## Describe the performance problem
<!--- A clear and concise description of what the performance problem is. Links to any charts, logs, or dashboards that describe the anomaly. (Please provide textual output rather than screenshots, if possible.) -->

/label bug
/label ~"group::platform insights"
/label ~"backend"
/confidential