.PHONY: lint-modules
# lint terraform modules and avoid linting in .terraform directories.
lint-modules: TF_MODULES=$(shell find ./modules -type f -name '*.tf' -not -path "**/.terraform/*" -exec dirname {} \; | sort | uniq)
lint-modules:
	@echo "Linting terraform modules:"
	@for d in ${TF_MODULES}; do \
		echo "--- linting $$d"; \
		(cd $$d && terraform init -backend=false && terraform validate); \
	done
	cd modules && terraform fmt -check -diff -recursive

.PHONY: lint-terragrunt
# lint terragrunt environments.
lint-terragrunt:
	cd environments/test-terragrunt && \
	terragrunt run-all validate && \
	terragrunt validate-inputs && \
	terragrunt hclfmt --terragrunt-check

.PHONY: format
format:
	cd modules && terraform fmt -recursive
	cd environments/test-terragrunt && terragrunt hclfmt