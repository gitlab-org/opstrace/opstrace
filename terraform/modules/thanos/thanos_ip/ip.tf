
resource "google_compute_address" "traefik-address" {
  name    = "${var.gke_cluster_name}-traefik-ip"
  region  = var.gke_cluster_region
  project = var.project_id
}

resource "google_compute_address" "ing-address" {
  name    = "${var.gke_cluster_name}-nginx-ingress-ip"
  region  = var.gke_cluster_region
  project = var.project_id
}