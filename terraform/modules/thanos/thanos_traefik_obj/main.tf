locals {
  allowed_ips = split(",", var.source_ip_ranges_allowed_access)
}

resource "kubernetes_manifest" "traefik-source-ip-whitelist" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "Middleware"

    "metadata" = {
      "name"      = "source-ip-whitelist-traefik",
      "namespace" = var.namespace,
    },

    "spec" = {
      "ipWhiteList" : {
        "sourceRange" : [for ip in local.allowed_ips : ip]
      }
    }
  }
}

# IngressRoute objects that expose store API of thanos-receive and thanos-store
# Note that this won't be available as ingress but rather CRD of type ingress route
resource "kubernetes_manifest" "traefik-ingressroute-store" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "IngressRoute"

    "metadata" = {
      "name"      = "thanos-store-plain",
      "namespace" = var.namespace,
    },

    "spec" = {
      "entryPoints" = [
        "web",
      ]
      "routes" = [
        {
          "kind"  = "Rule",
          "match" = "Host(`${var.thanos_store_domain}`)",
          "middlewares" = [
            {
              "name"      = "source-ip-whitelist-traefik",
              "namespace" = var.namespace,
            },
          ],
          "services" = [
            {
              "kind"      = "Service",
              "name"      = "thanos-store",
              "namespace" = var.namespace,
              "port"      = "10901",
              "scheme"    = "h2c",
            },
          ],
        },
      ],
    }
  }
}

resource "kubernetes_manifest" "traefik-ingressroute-receive" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "IngressRoute"

    "metadata" = {
      "name"      = "thanos-receive-plain",
      "namespace" = var.namespace,
    },

    "spec" = {
      "entryPoints" = [
        "web",
      ]
      "routes" = [
        {
          "kind"  = "Rule",
          "match" = "Host(`${var.thanos_receive_store_api_domain}`)",
          "middlewares" = [
            {
              "name"      = "source-ip-whitelist-traefik",
              "namespace" = var.namespace,
            },
          ],
          "services" = [
            {
              "kind"      = "Service",
              "name"      = "thanos-receive",
              "namespace" = var.namespace,
              "port"      = "10901",
              "scheme"    = "h2c",
            },
          ],
        },
      ],
    }
  }
}

resource "kubernetes_manifest" "traefik-ingressroute-query" {
  manifest = {
    "apiVersion" = "traefik.containo.us/v1alpha1"
    "kind"       = "IngressRoute"

    "metadata" = {
      "name"      = "thanos-query-plain",
      "namespace" = var.namespace,
    },

    "spec" = {
      "entryPoints" = [
        "web",
      ]
      "routes" = [
        {
          "kind"  = "Rule",
          "match" = "Host(`${var.thanos_query_domain}`)",
          "middlewares" = [
            {
              "name"      = "source-ip-whitelist-traefik",
              "namespace" = var.namespace,
            },
          ],
          "services" = [
            {
              "kind"      = "Service",
              "name"      = "thanos-query",
              "namespace" = var.namespace,
              "port"      = "10901",
              "scheme"    = "h2c",
            },
          ],
        },
      ],
    }
  }
}
