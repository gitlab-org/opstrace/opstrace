variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for gke_auth, one must be set
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "gke_cluster_name" {
  type        = string
  description = "Name of the GKE cluster"
}

variable "namespace" {
  type        = string
  default     = "thanos"
  description = "Namespace for thanos components"
}

variable "thanos_bucket_name" {
  type        = string
  description = "GCS bucket name where thanos will store the data"
}

variable "bucket_location" {
  type        = string
  description = "Location for the GCS bucket"
}

variable "gke_vpc_name" {
  type        = string
  description = "VPC name of the Cluster where the thanos components will be installed"
}

variable "opstrace_cluster_name" {
  type        = string
  description = "Cluster/Instance name of the GKE cluster where opstrace is available"
}

variable "thanos_query_sd_domains" {
  type        = list(string)
  default     = []
  description = "List of domains(addr:port) to add to the store sd config of thanos-query. Make sure that the stores are reachable from the cluster."
}

variable "bucket_force_destroy" {
  type        = bool
  default     = false
  description = "Indicates if a bucket with data should be deleted during a destroy operation "
}