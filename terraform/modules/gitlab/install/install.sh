#!/bin/bash

set -e

echo "starting registry login with --username ${1} --password xxxxxx"
# Make sure we're logged in to registry.gitlab.com to pull registry images want to pull something like:
# registry.gitlab.com/gitlab-org/build/omnibus-gitlab-mirror/gitlab-ee:8652627bf86d02d20599b1e0caad1465bb545481
docker login registry.gitlab.com --username $1 --password $2

CONTAINER_ID=$(docker ps -aqf "name=gitlab")
if [ -z "$CONTAINER_ID" ]
then
      echo "nothing previously running"
else
      echo "stopping existing container ${CONTAINER_ID}"
      docker rm $CONTAINER_ID -f
fi

echo "starting docker container for domain: ${3}, with image: ${4}"

docker run --detach \
  --env GITLAB_OMNIBUS_CONFIG="external_url 'https://${3}/'; letsencrypt['enable'] = true; letsencrypt['contact_emails'] = ['${1}@gitlab.com']" \
  --hostname $3 \
  --publish 443:443 --publish 80:80 \
  --name gitlab \
  --restart always \
  --volume /tmp/gitlab/config:/etc/gitlab \
  --volume /tmp/gitlab/logs:/var/log/gitlab \
  --volume /tmp/gitlab/data:/var/opt/gitlab \
  --shm-size 256m \
  $4

echo -n "waiting for gitlab instance to be ready..."
while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' https://${3}/users/sign_in)" != "200" ]]; do sleep 5 ; echo -n "waiting for gitlab instance to be ready..." ; done
echo "seems to be ready"

# Remove "Password: " from the line and write to another file
docker exec -i gitlab grep 'Password:' /etc/gitlab/initial_root_password | sed 's/^.\{10\}//' > /tmp/gitlab_initial_root_password
ROOT_USER_PASS=$(cat /tmp/gitlab_initial_root_password)
# Create a personal access token: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token-programmatically
# Unlike with passwords, we just create a new one with each run.
# 20 random letters
ROOT_AUTH_TOKEN="$(dd if=/dev/urandom bs=64 count=1 2>/dev/null | base64 | tr -dc 'a-zA-Z' | cut -c1-20)"
# The tokens do not need to have unique names, but this may help with tracing across multiple runs
TOKEN_NAME="API auth token $(date +%H:%M:%S)"
echo "$(date +%H:%M:%S) Adding Personal Access Token for root user, this can take around 20 seconds"
while [ 1 ]; do
    docker exec -i gitlab gitlab-rails runner "token = User.find_by_username('root').personal_access_tokens.create(scopes: [:api], name: '${TOKEN_NAME}', expires_at: 2.day.from_now); token.set_token('${ROOT_AUTH_TOKEN}'); token.save!"
    RESULT=$?
    if [ $RESULT -eq 0 ]; then
        break
    fi
    echo "$(date +%H:%M:%S) gitlab-rails command failed, trying again in 10s..."
    sleep 10
done
echo "$(date +%H:%M:%S) Added Personal Access Token with name=\"$TOKEN_NAME\""

# Create oauth application: https://docs.gitlab.com/ee/api/applications.html#create-an-application
# TODO: find a way to make this application trusted. Doesn't seem that it's supported via API
echo "Adding oauth application"
OAUTH_CONFIG=$(
    curl --silent \
        -H "Authorization: Bearer $ROOT_AUTH_TOKEN" \
        "https:/${3}/api/v4/applications" \
        --data "name=opstrace&redirect_uri=https://${5}/v1/auth/callback&scopes=api"
)
# Add the instance URL and auth token to file as well
echo $OAUTH_CONFIG | jq --arg rootUserPassword "$ROOT_USER_PASS" '. += {rootUserPassword: $rootUserPassword}' | jq --arg authToken "$ROOT_AUTH_TOKEN" '. += {adminAuthToken: $authToken}' | jq --arg url "https://${3}" '. += {gitlabURL: $url}' > /tmp/opstrace-config.json
echo "Wrote config to /tmp/opstrace-config.json"
