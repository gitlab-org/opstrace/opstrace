---
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  labels:
    tenant: system
  name: prometheus-k8s-rules
spec:
  groups:
    - name: k8s.rules
      rules:
        - expr: |
            sum(rate(container_cpu_usage_seconds_total{job="kubelet", image!="", container!="POD"}[5m])) by (namespace)
          record: namespace:container_cpu_usage_seconds_total:sum_rate

        - expr: |
            sum by (namespace, pod, container) (
              rate(container_cpu_usage_seconds_total{job="kubelet", image!="", container!="POD"}[5m])
            ) * on (namespace, pod) group_left(node) max by(namespace, pod, node) (kube_pod_info)
          record: node_namespace_pod_container:container_cpu_usage_seconds_total:sum_rate

        - expr: |
            container_memory_working_set_bytes{job="kubelet", image!=""}
            * on (namespace, pod) group_left(node) max by(namespace, pod, node) (kube_pod_info)
          record: node_namespace_pod_container:container_memory_working_set_bytes

        - expr: |
            container_memory_rss{job="kubelet", image!=""}
            * on (namespace, pod) group_left(node) max by(namespace, pod, node) (kube_pod_info)
          record: node_namespace_pod_container:container_memory_rss

        - expr: |
            container_memory_cache{job="kubelet", image!=""}
            * on (namespace, pod) group_left(node) max by(namespace, pod, node) (kube_pod_info)
          record: node_namespace_pod_container:container_memory_cache

        - expr: |
            container_memory_swap{job="kubelet", image!=""}
            * on (namespace, pod) group_left(node) max by(namespace, pod, node) (kube_pod_info)
          record: node_namespace_pod_container:container_memory_swap

        - expr: |
            sum(container_memory_usage_bytes{job="kubelet", image!="", container!="POD"}) by (namespace)
          record: namespace:container_memory_usage_bytes:sum

        - expr: |
            sum by (namespace, label_name, resource, unit) (
                sum(kube_pod_container_resource_requests{job="kube-state-metrics"} * on (endpoint, instance, job, namespace, pod, service) group_left(phase) (kube_pod_status_phase{phase=~"Pending|Running"} == 1)) by (namespace, pod, resource, unit)
              * on (namespace, pod)
                group_left(label_name) kube_pod_labels{job="kube-state-metrics"}
            )
          record: namespace:kube_pod_container_resource_requests:sum

        - expr: |
            sum(
              label_replace(
                label_replace(
                  kube_pod_owner{job="kube-state-metrics", owner_kind="ReplicaSet"},
                  "replicaset", "$1", "owner_name", "(.*)"
                ) * on(replicaset, namespace) group_left(owner_name) kube_replicaset_owner{job="kube-state-metrics"},
                "workload", "$1", "owner_name", "(.*)"
              )
            ) by (namespace, workload, pod)
          labels:
            workload_type: deployment
          record: mixin_pod_workload

        - expr: |
            sum(
              label_replace(
                kube_pod_owner{job="kube-state-metrics", owner_kind="DaemonSet"},
                "workload", "$1", "owner_name", "(.*)"
              )
            ) by (namespace, workload, pod)
          labels:
            workload_type: daemonset
          record: mixin_pod_workload

        - expr: |
            sum(
              label_replace(
                kube_pod_owner{job="kube-state-metrics", owner_kind="StatefulSet"},
                "workload", "$1", "owner_name", "(.*)"
              )
            ) by (namespace, workload, pod)
          labels:
            workload_type: statefulset
          record: mixin_pod_workload
    - name: node.rules
      rules:
        - expr: sum(min(kube_pod_info) by (node))
          record: ':kube_pod_info_node_count:'

        - expr: |
            max(label_replace(kube_pod_info{job="kube-state-metrics"}, "pod", "$1", "pod", "(.*)")) by (node, namespace, pod)
          record: 'node_namespace_pod:kube_pod_info:'

        - expr: |
            count by (node) (sum by (node, cpu) (
              node_cpu_seconds_total{job="node-exporter"}
            * on (namespace, pod) group_left(node)
              node_namespace_pod:kube_pod_info:
            ))
          record: node:node_num_cpu:sum

        - expr: |
            sum(
              node_memory_MemAvailable_bytes{job="node-exporter"} or
              (
                node_memory_Buffers_bytes{job="node-exporter"} +
                node_memory_Cached_bytes{job="node-exporter"} +
                node_memory_MemFree_bytes{job="node-exporter"} +
                node_memory_Slab_bytes{job="node-exporter"}
              )
            )
          record: :node_memory_MemAvailable_bytes:sum

    - name: kubernetes-resources.alerts
      rules:
        - alert: KubeCPUOvercommit
          annotations:
            title: Cluster CPU Overcommitted
            description: Cluster has overcommitted CPU resource requests for Pods and cannot tolerate node failure.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            sum(namespace:kube_pod_container_resource_requests:sum{resource="cpu", unit="core"})
              /
            sum(node:node_num_cpu:sum)
              >
            (count(node:node_num_cpu:sum)-1) / count(node:node_num_cpu:sum)
          for: 5m
          labels:
            alertname: KubeCPUOvercommit
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeMemOvercommit
          annotations:
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
            title: Cluster CPU Overcommitted
            description: Cluster has overcommitted CPU resource requests for Pods and cannot tolerate node failure.
          expr: |-
            sum(namespace:kube_pod_container_resource_requests:sum{resource="memory", unit="byte"})
              /
            sum(node_memory_MemTotal_bytes)
              >
            (count(node:node_num_cpu:sum)-1)
              /
            count(node:node_num_cpu:sum)
          for: 5m
          labels:
            alertname: KubeMemOvercommit
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeQuotaExceeded
          annotations:
            title: Kube Quota has been exceeded
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
            description: Namespace {{ $labels.namespace }} is using {{ printf "%0.0f" $value }}% of its {{ $labels.resource }} quota.
          expr: |-
            100 * kube_resourcequota{job="kube-state-metrics", type="used"}
              / ignoring(instance, job, type)
            (kube_resourcequota{job="kube-state-metrics", type="hard"} > 0)
              > 90
          for: 15m
          labels:
            alertname: KubeQuotaExceeded
            type: kube
            severity: s3
            alert_type: cause

        - alert: CPUThrottlingHigh
          annotations:
            title: CPU throttling is High
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
            description: '{{ printf "%0.0f" $value }}% throttling of CPU in namespace {{ $labels.namespace }} for pod {{ $labels.pod }}.'
          # container-watcher pod belongs to GKE, we do not controll it and it
          # seems to often be throttled.
          expr: |-
            100 * sum(increase(container_cpu_cfs_throttled_periods_total{pod !~ "container-watcher-.*"}[5m])) by (pod, namespace) / sum(increase(container_cpu_cfs_periods_total{pod !~ "container-watcher-.*"}[5m])) by (pod, namespace) > 10
          for: 15m
          labels:
            alertname: CPUThrottlingHigh
            type: kube
            severity: s3
            alert_type: cause

        # NOTE(prozlach): the alerts around Inodes are not 100% bulletproof,
        # for some reason e.g. clickhouse volumes do not show up. Still - they
        # aready give some coverage and we can iterate on them later on.
        - alert: ContainerVolumeInodesUsage
          annotations:
            title: Container Volume has less than 15% of Inodes left
            description: Container Volume Inodes usage {{ printf "%.2f" $value }}% is above 85% for instance {{ $labels.instance }}
          expr: |-
            (1 - (sum(container_fs_inodes_free) BY (instance) / sum(container_fs_inodes_total) BY (instance))) * 100 > 85
          for: 3m
          labels:
            alertname: ContainerVolumeInodesUsage
            type: kube
            severity: s3
            alert_type: cause

        - alert: NodeFilesystemFilesFillingUp
          annotations:
            title: Filesystem is predicted to run out of inodes within the next 24 hours.
            description: Filesystem on {{ $labels.device }}, mounted on {{ $labels.mountpoint }}, at {{ $labels.instance }} has only {{ printf "%.2f" $value }}% available inodes left and is filling up.
          expr: |-
            (
              node_filesystem_files_free{job="node-exporter",fstype!="",mountpoint!=""} / node_filesystem_files{job="node-exporter",fstype!="",mountpoint!=""} * 100 < 40
            and
              predict_linear(node_filesystem_files_free{job="node-exporter",fstype!="",mountpoint!=""}[6h], 24*60*60) < 0
            and
              node_filesystem_readonly{job="node-exporter",fstype!="",mountpoint!=""} == 0
            )
          for: 1h
          labels:
            alertname: NodeFilesystemFilesFillingUp
            type: kube
            severity: s3
            alert_type: cause

        - alert: NodeFilesystemAlmostOutOfFiles15
          annotations:
            title: Filesystem has less than 15% inodes left.
            description: Filesystem on {{ $labels.device }}, mounted on {{ $labels.mountpoint }}, at {{ $labels.instance }} has only {{ printf "%.2f" $value }}% available inodes left.
          expr: |-
            (
              node_filesystem_files_free{job="node-exporter",fstype!="",mountpoint!=""} / node_filesystem_files{job="node-exporter",fstype!="",mountpoint!=""} * 100 < 15
            and
              node_filesystem_readonly{job="node-exporter",fstype!="",mountpoint!=""} == 0
            )
          for: 1h
          labels:
            alertname: NodeFilesystemAlmostOutOfFiles15
            type: kube
            severity: s3
            alert_type: cause

        - alert: NodeFilesystemAlmostOutOfFiles5
          annotations:
            title: Filesystem has less than 5% inodes left.
            description: Filesystem on {{ $labels.device }}, mounted on {{ $labels.mountpoint }}, at {{ $labels.instance }} has only {{ printf "%.2f" $value }}% available inodes left.
          expr: |-
            (
              node_filesystem_files_free{job="node-exporter",fstype!="",mountpoint!=""} / node_filesystem_files{job="node-exporter",fstype!="",mountpoint!=""} * 100 < 5
            and
              node_filesystem_readonly{job="node-exporter",fstype!="",mountpoint!=""} == 0
            )
          for: 1h
          labels:
            alertname: NodeFilesystemAlmostOutOfFiles5
            type: kube
            severity: s2
            alert_type: cause

    - name: kubernetes-services.alerts
      rules:
        - alert: KubeStateMetricsDown
          annotations:
            title: KubeStateMetrics is down
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
            description: KubeStateMetrics has disappeared from Prometheus target discovery.
          expr: |
            absent(up{job="kube-state-metrics"} == 1)
          for: 15m
          labels:
            alertname: KubeStateMetricsDown
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeletDown
          annotations:
            title: Kubelet is down
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
            description: Kubelet has disappeared from Prometheus target discovery.
          expr: |
            absent(up{job="kubelet"} == 1)
          for: 15m
          labels:
            alertname: KubeletDown
            type: kube
            severity: s3
            alert_type: cause

    - name: kubernetes-storage.alerts
      rules:
        - alert: KubePersistentVolumeUsageCritical
          annotations:
            title: Persisten Volume usage is Critical
            description: The PersistentVolume claimed by {{ $labels.persistentvolumeclaim }} in Namespace {{ $labels.namespace }} is only {{ printf "%0.2f" $value }}% free.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            100 * kubelet_volume_stats_available_bytes{job="kubelet"}
              /
            kubelet_volume_stats_capacity_bytes{job="kubelet"}
              < 3
          for: 1m
          labels:
            alertname: KubePersistentVolumeUsageCritical
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubePersistentVolumeFullSoon
          annotations:
            title: Persistent Volume will be full in 7 days
            description: Based on recent sampling, the PersistentVolume claimed by {{ $labels.persistentvolumeclaim }} in Namespace {{ $labels.namespace }} is expected to fill up within 7 days. Currently {{ printf "%0.2f" $value }}% is available.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            100 * (
              kubelet_volume_stats_available_bytes{job="kubelet"}
                /
              kubelet_volume_stats_capacity_bytes{job="kubelet"}
            ) < 20
            and
            predict_linear(kubelet_volume_stats_available_bytes{job="kubelet"}[2h], 7 * 24 * 3600) < 0
          for: 1d
          labels:
            alertname: KubePersistentVolumeFullInFourDays
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubePersistentVolumeErrors
          annotations:
            title: Persistent Volume in bad state.
            description: The persistent volume {{ $labels.persistentvolume }} has status {{ $labels.phase }}.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: kube_persistentvolume_status_phase{phase=~"Failed|Pending",job="kube-state-metrics"} > 0
          for: 5m
          labels:
            alertname: KubePersistentVolumeErrors
            type: kube
            severity: s3
            alert_type: cause

    - name: kubernetes-system.alerts
      rules:
        - alert: KubeNodeNotReady
          annotations:
            title: Node not ready
            description: '{{ $labels.node }} has been unready for more than an hour.'
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: kube_node_status_condition{job="kube-state-metrics",condition="Ready",status="true"} == 0
          for: 1h
          labels:
            alertname: KubeNodeNotReady
            type: kube
            severity: s4
            alert_type: cause

        - alert: KubeVersionMismatch
          annotations:
            title: Differing versions of Kubernetes components
            description: There are {{ $value }} different semantic versions of Kubernetes components running.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: count(count by (gitVersion) (label_replace(kubernetes_build_info{job!="kube-dns"},"gitVersion","$1","gitVersion","(v[0-9]*.[0-9]*.[0-9]*).*"))) > 1
          for: 1h
          labels:
            alertname: KubeVersionMismatch
            type: kube
            severity: s4
            alert_type: cause

        - alert: KubeClientErrors
          annotations:
            title: Kubernetes API error rate is high
            description: Kubernetes API server client '{{ $labels.job }}/{{ $labels.instance }}' is experiencing {{ printf "%0.0f" $value }}% errors.'
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            (sum(rate(rest_client_requests_total{code=~"5.."}[5m])) by (instance, job)
              /
            sum(rate(rest_client_requests_total[5m])) by (instance, job))
            * 100 > 1
          for: 15m
          labels:
            alertname: KubeClientErrors
            type: kube
            severity: s4
            alert_type: cause

        - alert: KubeletTooManyPods
          annotations:
            title: Node running too many Pods
            description: Kubelet {{ $labels.instance }} is running {{ $value }} Pods, close to the limit of 110.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: kubelet_running_pod_count{job="kubelet"} > 110 * 0.9
          for: 15m
          labels:
            alertname: KubeletTooManyPods
            type: kube
            severity: s4
            alert_type: cause

    - name: kubernetes-workloads.alerts
      rules:
        - alert: KubeReplicasSetPodMismatch
          annotations:
            title: Deployment Replicas Mismatch
            description: Deployment {{ $labels.namespace }}/{{ $labels.replicaset }} has not matched the expected number of replicas
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            kube_replicaset_spec_replicas{job="kube-state-metrics", replicaset!~"jaeger.*"}
            !=
            kube_replicaset_status_ready_replicas{job="kube-state-metrics", replicaset!~"jaeger.*"}
          for: 1h
          labels:
            alertname: KubeReplicasSetPodMismatch
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubePodNotReady
          annotations:
            title: Pod not healthy
            description: Pod {{ $labels.namespace }}/{{ $labels.pod }} is in a non-ready state
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: sum by (namespace, pod, env, stage) (kube_pod_status_phase{job="kube-state-metrics", phase!~"(Running|Succeeded)"}) > 0
          for: 15m
          labels:
            alertname: KubePodNotReady
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubePodRestarting
          annotations:
            title: Pod Restarting Often
            description: Pod {{ $labels.namespace }}/{{ $labels.pod }} ({{ $labels.container }}) is restarting {{ printf "%.2f" $value }} times / 5 minutes.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: rate(kube_pod_container_status_restarts_total{job="kube-state-metrics", pod!~"jaeger.*"}[15m]) * 60 * 5 > 0
          for: 1h
          labels:
            alertname: KubePodRestarting
            type: kube
            severity: s3
            alert_type: cause

        - alert: PodOOMkilled
          annotations:
            title: Pod OOMKilled
            description: Pod {{ $labels.namespace }}/{{ $labels.pod }} ({{ $labels.container }}) has been OOMKilled
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |
            sum by (pod, container, namespace) (kube_pod_container_status_last_terminated_reason{reason="OOMKilled"}) * on (pod,container) group_left
            sum by (pod, container) (changes(kube_pod_container_status_restarts_total{}[1m])) > 0
          labels:
            alertname: KubePodOOMKilled
            type: kube
            severity: s2
            alert_type: cause

        - alert: KubePodCrashLooping
          annotations:
            title: Pod CrashLooping
            description: Pod {{ $labels.namespace }}/{{ $labels.pod }} ({{ $labels.container }}) is reporting CrashLoopBackOff restarts.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |
            sum by (environment,type,stage,tier,deployment) (kube_pod_container_status_waiting_reason:labeled{reason="CrashLoopBackOff"} > 0)
              /
            sum by (environment,type,stage,tier,deployment) (kube_pod_container_status_waiting_reason:labeled > 0)
              > 0.5
          for: 5m
          labels:
            alertname: KubePodCrashLooping
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeDeploymentReplicasMismatch
          annotations:
            title: Deployment Replicas Mismatch
            description: Deployment {{ $labels.namespace }}/{{ $labels.deployment }} has not matched the expected number of replicas
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |
            kube_deployment_spec_replicas{job="kube-state-metrics", deployment!~"jaeger.*"}
              !=
            kube_deployment_status_replicas_available{job="kube-state-metrics", deployment!~"jaeger.*"}
          for: 1h
          labels:
            alertname: KubeDeploymentReplicasMismatch
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeDeploymentGenerationMismatch
          annotations:
            title: Duplicate Deployments detected
            description: Deployment generation for {{ $labels.namespace }}/{{ $labels.deployment }} does not match, this indicates that the Deployment has failed but has not been rolled back.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            kube_deployment_status_observed_generation{job="kube-state-metrics"}
              !=
            kube_deployment_metadata_generation{job="kube-state-metrics"}
          for: 15m
          labels:
            alertname: KubeDeploymentGenerationMismatch
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeStatefulSetReplicasMismatch
          annotations:
            title: StatefulSet Replicas Mismatch
            description: StatefulSet {{ $labels.namespace }}/{{ $labels.statefulset }} has not matched the expected number of replicas
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            kube_statefulset_status_replicas_ready{job="kube-state-metrics"}
              !=
            kube_statefulset_status_replicas{job="kube-state-metrics"}
          for: 6h
          labels:
            alertname: KubeStatefulSetReplicasMismatch
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeStatefulSetGenerationMismatch
          annotations:
            title: Duplicate StatefulSets detected
            description: StatefulSet generation for {{ $labels.namespace }}/{{ $labels.statefulset }} does not match, this indicates that the StatefulSet has failed but has not been rolled back.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            kube_statefulset_status_observed_generation{job="kube-state-metrics"}
              !=
            kube_statefulset_metadata_generation{job="kube-state-metrics"}
          for: 15m
          labels:
            alertname: KubeStatefulSetGenerationMismatch
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeStatefulSetUpdateNotRolledOut
          annotations:
            title: StatefulSet update incomplete
            description: StatefulSet {{ $labels.namespace }}/{{ $labels.statefulset }} update has not been rolled out.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            max without (revision) (
              kube_statefulset_status_current_revision{job="kube-state-metrics"}
                unless
              kube_statefulset_status_update_revision{job="kube-state-metrics"}
            )
              *
            (
              kube_statefulset_replicas{job="kube-state-metrics"}
                !=
              kube_statefulset_status_replicas_updated{job="kube-state-metrics"}
            )
          for: 15m
          labels:
            alertname: KubeStatefulSetUpdateNotRolledOut
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeDaemonSetRolloutStuck
          annotations:
            title: Daemonset Rollout incomplete
            description: Only {{ $value }}% of the desired Pods of DaemonSet {{ $labels.namespace }}/{{ $labels.daemonset }} are scheduled and ready.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            kube_daemonset_status_number_ready{job="kube-state-metrics"}
              /
            kube_daemonset_status_desired_number_scheduled{job="kube-state-metrics"} * 100 < 100
          for: 15m
          labels:
            alertname: KubeDaemonSetRolloutStuck
            type: kube
            severity: s3
            alert_type: cause

        - alert: KubeDaemonSetNotScheduled
          annotations:
            title: DaemonSet Pod(s) unable to be scheduled
            description: '{{ $value }} Pods of DaemonSet {{ $labels.namespace }}/{{ $labels.daemonset }} are not scheduled.'
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: |-
            kube_daemonset_status_desired_number_scheduled{job="kube-state-metrics"}
              -
            kube_daemonset_status_current_number_scheduled{job="kube-state-metrics"} > 0
          for: 10m
          labels:
            alertname: KubeDaemonSetNotScheduled
            type: kube
            severity: s4
            alert_type: cause

        - alert: KubeDaemonSetMisScheduled
          annotations:
            title: DaemonSet Pod(s) unable to be scheduled
            description: '{{ $value }} Pods of DaemonSet {{ $labels.namespace }}/{{ $labels.daemonset }} are running where they are not supposed to run.'
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: kube_daemonset_status_number_misscheduled{job="kube-state-metrics"} > 0
          for: 10m
          labels:
            alertname: KubeDaemonSetMisScheduled
            type: kube
            severity: s4
            alert_type: cause

        - alert: KubeCronJobRunning
          annotations:
            title: CronJob Running too long
            description: CronJob {{ $labels.namespace }}/{{ $labels.cronjob }} is taking more than 1h to complete.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: time() - kube_cronjob_next_schedule_time{job="kube-state-metrics"} > 3600
          for: 1h
          labels:
            alertname: KubeCronJobRunning
            type: kube
            severity: s4
            alert_type: cause

        - alert: KubeJobCompletion
          annotations:
            title: CronJob Running too long
            description: Job {{ $labels.namespace }}/{{ $labels.job_name }} is taking more than one hour to complete.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: kube_job_spec_completions{job="kube-state-metrics"} - kube_job_status_succeeded{job="kube-state-metrics"}  > 0
          for: 1h
          labels:
            alertname: KubeJobCompletion
            type: kube
            severity: s4
            alert_type: cause

        - alert: KubeJobFailed
          annotations:
            title: Job failed
            description: Job {{ $labels.namespace }}/{{ $labels.job_name }} failed to complete.
            link: https://gitlab.com/gitlab-org/opstrace/opstrace/-/tree/main/docs/guides/administrator
            grafana_dashboard_link: https://dashboards.gitlab.net/dashboards/f/observability/observability
          expr: kube_job_status_failed{job="kube-state-metrics"}  > 0
          for: 1h
          labels:
            alertname: KubeJobFailed
            type: kube
            severity: s4
            alert_type: cause
