variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for google provider, see also https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#provider-default-values-configuration
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "scheduler_image" {
  type    = string
  default = "SCHEDULER_IMAGE_NAME"
}

variable "global_labels" {
  default     = {}
  description = "Map consisting key=value pairs added as labels to all resources provisioned by this module"
  type        = map(any)
}

variable "gke_cluster_name" {
  type        = string
  description = "Name of the cluster to which the nodes should be attached to"
}

variable "provision_clickhouse_node_pool" {
  default     = true
  type        = bool
  description = "flag to provision dedicated node pool for clickhouse"
}

variable "ch_nodepool_nodes_number" {
  description = "Number of nodes the dedicated clikhouse node pool should have"
  type        = number
  # minimum quorum size is 3
  default = 3
}

variable "ch_nodepool_machine_type" {
  description = "GCP machine type to use ClickHouse node pools"
  type        = string
  default     = "c2d-highcpu-16"
}

# See: https://cloud.google.com/kubernetes-engine/docs/reference/rest/v1beta1/UpgradeSettings#BlueGreenSettings
# for the following blue-green upgrade settings
variable "ch_nodepool_blue_green_batch_node_count" {
  description = "Number of blue nodes to drain in a batch"
  type        = number
  default     = 1
}

variable "ch_nodepool_blue_green_batch_soak_duration" {
  description = "Number of seconds to wait after each batch gets drained"
  type        = string
  default     = "7200s" # 2 hours
}

variable "ch_nodepool_blue_green_node_pool_soak_duration" {
  description = "Time needed after draining the entire blue pool, after which it's cleaned up"
  type        = string
  default     = "1800s" # 30 mins
}

variable "ratelimits_config" {
  type        = string
  default     = ""
  description = "Error tracking rate limits config. If not specified the scheduler/config/rate-limits/limits.yaml file is used"
}

variable "pool_auto_repair" {
  type        = bool
  default     = true
  description = "Enable auto-repair of nodes"
}

# Option added here only to make overriding this parameter in e2e tests suite
# easy and simple, without complex logic. It's main purpose is to dive the
# `auto_upgrade` setting.
variable "release_channel" {
  type        = string
  default     = "REGULAR"
  description = "Release channel specified for managing upgrades for GKE cluster"
}
