variable "project_id" {
  type        = string
  description = "project id"
}

# region and zone used for gke_auth, one must be set
variable "region" {
  type        = string
  default     = ""
  description = "The region to manage resources in. If not set, the zone will be used instead."
}

variable "zone" {
  type        = string
  default     = ""
  description = "The zone referencing the region to manage resources in. If not set, the region will be used instead."
}

variable "gke_cluster_name" {
  type        = string
  description = "Name of the GKE cluster"
}

variable "backup_secret_name" {
  type    = string
  default = "clickhouse-backup-secret"
}

variable "gcs_access_key_id" {
  type      = string
  sensitive = true
  default   = "sample"
}

variable "gcs_access_key_secret" {
  type      = string
  sensitive = true
  default   = "samplesecret"
}

variable "backup_gcs_bucket_url" {
  type    = string
  default = "https//storage.googleapis.com/clickhouse/backups"
}

variable "clickhouse_dsn" {
  type      = string
  sensitive = true
  default   = "tcp://localhost:9000"
}

variable "clickhouse_backup_image" {
  description = "Image for ClickHouse backup Utility"
  type        = string
  default     = "registry.gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-backup:latest"
}