resource "aws_s3_bucket" "clickhouse-s3-bucket" {
  bucket = var.clickhouse_s3_bucket
}

resource "aws_s3_bucket_server_side_encryption_configuration" "clickhouse-s3-bucket-encryption" {
  bucket = aws_s3_bucket.clickhouse-s3-bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_acl" "clickhouse-s3-bucket-acl" {
  bucket = aws_s3_bucket.clickhouse-s3-bucket.id
  acl    = "private"
}

resource "aws_iam_policy" "clickhouse-s3-access-policy" {
  name        = "clickhouse-s3-access-policy"
  description = "IAM policy to be used by Clickhouse that grants access over the bucket"
  policy      = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:AbortMultipartUpload",
        "s3:ListBucket",
        "s3:GetObject",
        "s3:DeleteObject",
        "s3:PutObject",
        "s3:ListMultipartUploadParts"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.clickhouse-s3-bucket.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:CreateBucket",
        "s3:DeleteBucket",
        "s3:GetBucketLocation",
        "s3:ListBucket",
        "s3:ListBucketMultipartUploads"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.clickhouse-s3-bucket.arn}*"
      ]
    }
  ]
}
POLICY
}

resource "aws_s3_object" "root-bucket" {
  bucket = aws_s3_bucket.clickhouse-s3-bucket.id
  key    = var.clickhouse_s3_bucket_root
  acl    = "private"
}

resource "aws_iam_user" "clickhouse-s3-user" {
  name = var.clickhouse_s3_user_name
}

resource "aws_iam_user_policy_attachment" "clickhouse-s3-user-policy-attach" {
  policy_arn = aws_iam_policy.clickhouse-s3-access-policy.arn
  user       = aws_iam_user.clickhouse-s3-user.name
}

resource "aws_s3_bucket_public_access_block" "clickhouse-s3-block-public-access" {
  bucket = aws_s3_bucket.clickhouse-s3-bucket.id
  # Block public ACLs for this bucket.
  block_public_acls = true
  # Block public bucket policies for this bucket.
  block_public_policy = true
  # Ignore public ACLs for this bucket.
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_iam_access_key" "clickhouse-s3-user-key" {
  user = aws_iam_user.clickhouse-s3-user.name
}

resource "kubernetes_secret" "clickhouse-s3-user-secret" {
  metadata {
    generate_name = "clickhouse-s3-secret"
    namespace     = var.s3_secret_namespace
  }
  data = {
    accessKeyID     = aws_iam_access_key.clickhouse-s3-user-key.id
    accessKeySecret = aws_iam_access_key.clickhouse-s3-user-key.secret
  }
}
