provider "google" {
  project = var.project_id
  region  = var.region
  zone    = var.zone
}

module "gke_auth" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  version = "v25.0.0"

  project_id   = var.project_id
  cluster_name = google_container_cluster.primary.name
  location     = coalesce(var.zone, var.region)

  depends_on = [google_container_cluster.primary]
}
