resource "local_sensitive_file" "kubeconfig" {
  filename = var.kubeconfig_path
  content  = module.gke_auth.kubeconfig_raw

  depends_on = [google_container_cluster.primary]
}
