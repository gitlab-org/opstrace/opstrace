# Opstrace Terraform

## Requirements

* Tested with terraform version >= 1.4.x
* Google GCP credentials

## What to Expect

The `environments/test-terragrunt` terraform environment will create an instance of GitLab on GCP, create an instance-wide OAuth Application and then create an Opstrace instance on GCP configured with the OAuth Application. In other words, it will deploy GitLab and Opstrace together and automatically configure their connection.

This environment serves primarily for testing, either in CI or by developers and could serve as a future reference for automating the connection
between GitLab/Opstrace when Opstrace is packages with GitLab.

## Terraform Outputs

For each module in `./modules` you can check the outputs documented in each `outputs.tf` file.

## Instructions

[Set up GCP credentials](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/getting_started#adding-credentials).

Ensure you have two [DNS zones in GCP set up](https://cloud.google.com/dns/docs/zones), one each for Opstrace and GitLab. These will represent
the DNS for Opstrace and GitLab and are required inputs in the terraform variables below.

Then move to the test directory to set up a test instance:

```bash
cd environments/test-terragrunt
```

Copy the `env.hcl` to `custom.hcl` and update the variables for your GCP project.

Initialize terragrunt setup:

```bash
terragrunt run-all init
```

Set the following TF_VARs:

```bash
export TF_VAR_registry_auth_token = "your_gitlab_personal_token"
export TF_VAR_scheduler_image = "scheduler_image"
```

Check the plan:

```bash
terragrunt run-all plan
```

Proceed to install and configure GitLab, Opstrace infrastructure, install the Scheduler and set the Cluster custom resource:

```bash
terragrunt run-all apply
```
```

## Cleanup

To destroy, run:

```bash
terragrunt run-all destroy
```

## Linting and Formatting

Run `make lint-modules` to lint the modules. Run `make lint-terragrunt` to lint the terragrunt example.

Use `make format` to format the code.
