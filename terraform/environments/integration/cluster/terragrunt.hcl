terraform {
  source = "${get_terragrunt_dir()}/../../../modules//opstracecluster"
}

include {
  path = find_in_parent_folders()
}

dependency "gke" {
  config_path = "../gke"
  mock_outputs = {
    kubernetes_cluster_name = "mock"
  }
  mock_outputs_allowed_terraform_commands = ["validate", "init"]
}

dependency "opstrace" {
  config_path  = "../opstrace"
  skip_outputs = true
}

inputs = {
  project_id                           = get_env("TF_VAR_project_id")
  zone                                 = get_env("TF_VAR_location")
  gke_cluster_name                     = dependency.gke.outputs.kubernetes_cluster_name
  cluster_secret_name                  = get_env("TF_VAR_cluster_secret_name")
  cluster_secret_namespace             = get_env("TF_VAR_cluster_secret_namespace")
  gitlab_oauth_client_id               = get_env("TF_VAR_oauth_client_id")
  gitlab_oauth_client_secret           = get_env("TF_VAR_oauth_client_secret")
  gitlab_otel_endpoint                 = get_env("TF_VAR_gitlab_otel_endpoint")
  gitlab_otel_endpoint_private_token   = get_env("TF_VAR_gitlab_otel_endpoint_private_token")
  internal_endpoint_token              = get_env("TF_VAR_internal_endpoint_token")
  disable_cluster_creation             = true
}
