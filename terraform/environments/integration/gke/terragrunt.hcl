terraform {
  source = "${get_terragrunt_dir()}/../../../modules//gke"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  instance_name                  = get_env("TF_VAR_instance_name")
  project_id                     = get_env("TF_VAR_project_id")
  zone                           = get_env("TF_VAR_location")
  gke_machine_type               = get_env("TF_VAR_gke_machine_type")
  kubeconfig_path                = "${get_terragrunt_dir()}/../.kubeconfig"
  provision_clickhouse_node_pool = true
}