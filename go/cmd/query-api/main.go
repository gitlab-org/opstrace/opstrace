package main

import (
	"context"
	"crypto/rsa"
	"flag"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/go-logr/zapr"
	_ "go.uber.org/automaxprocs"
	"go.uber.org/zap"

	alertsw "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	authproxy "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/auth-proxy"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/analytics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.opentelemetry.io/otel"
)

var (
	metricsAddr string
	serviceAddr string
	logLevel    string

	clickHouseDSN string

	clickHouseCloudDSN string

	otlpEndpoint        string
	otlpCACertificate   string
	otlpTokenSecretFile string

	gitlabOidcProviders arrayFlags
	rsaPrivateKey       *rsa.PrivateKey

	redisAddr               string
	redisPassword           string
	useRedisSentinel        bool
	redisConnectionPoolSize = 100
	rateLimitConfigFilePath string
)

type arrayFlags []string

func (i *arrayFlags) String() string {
	return strings.Join(*i, ", ")
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func parseCmdline() {
	flag.StringVar(&metricsAddr,
		"metrics-bind-address", ":8081", "The address the metric endpoint binds to.")
	flag.StringVar(&serviceAddr,
		"service-bind-address", ":8080", "The address the service endpoint binds to.")
	flag.StringVar(&logLevel,
		"log-level", "info", "The log-level to use.")
	flag.StringVar(&clickHouseDSN,
		"clickhouse-dsn", "tcp://localhost:9000", "clickhouse addr")
	flag.StringVar(&clickHouseCloudDSN,
		"clickhouse-cloud-dsn", "", "clickhouse cloud addr")
	flag.StringVar(&otlpEndpoint,
		"otlp-endpoint", "", "otlp collector endpoint, without `/1/traces` or `/v1/metrics` suffix")
	flag.StringVar(&otlpCACertificate,
		"otlp-ca-cert", "", "otlp CA certificate to use while connecting over HTTPs")
	flag.StringVar(&otlpTokenSecretFile,
		"otlp-token-file", "", "file that contains Access Token to use when connecting to otel collector")
	flag.Var(&gitlabOidcProviders,
		"gitlab-oidc-provider", "GitLab OIDC provider endpoint.")
	flag.StringVar(&redisAddr, "redis-address", "", "address for redis server, used for ratelimiting")
	flag.StringVar(&redisPassword, "redis-password", "", "password for redis server")
	flag.BoolVar(&useRedisSentinel, "use-redis-sentinel", true, "use sentinel for redis HA")
	flag.StringVar(&rateLimitConfigFilePath, "rate-limit-config-path", "", "file path for rate limit config")

	flag.Parse()
}

func main() {
	os.Exit(runQueryAPI())
}

//nolint:funlen,cyclop
func runQueryAPI() int {
	parseCmdline()

	logger, _, _ := instrumentation.SetupLogger(logLevel)
	// NOTE(prozlach): There is a tricky bug/feature that will prevent otel
	// logs from showing, unless you manually set the verbosity level to numerical
	// value when setting up zap. This is described here:
	//
	// https://github.com/go-logr/zapr?tab=readme-ov-file#increasing-verbosity
	//
	// It essentially boils down to zap log level debug reflecting `V(1)` level
	// for loggr. Unless you do the hack below when setting up the zap, entries
	// `V(2)` and above will not be visible and thus you will not get any logs
	// from otel.
	//
	// loggerCfg := zap.Config{
	//   ...
	//   Level:             zap.NewAtomicLevelAt(zapcore.Level(-6)),
	//   ...
	// }
	otel.SetLogger(zapr.NewLogger(logger.Desugar()))

	ctx, cancelFunc := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancelFunc()

	metricsQuerier, err := metrics.NewQuerier(clickHouseDSN, clickHouseCloudDSN, nil, logger.Desugar())
	if err != nil {
		logger.Errorw("initializing metrics querier", zap.Error(err))
		return 1
	}

	logsQuerier, err := logs.NewQuerier(
		clickHouseDSN, clickHouseCloudDSN,
		constants.LoggingCHQueryTimeout,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		logger.Errorw("initializing logs querier", zap.Error(err))
		return 1
	}

	metricsRegistry, routerMetrics := instrumentation.ConstructPromMeterTooling()
	analyticsQuerier, err := analytics.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		logger.Errorw("initializing analytics querier", zap.Error(err))
		return 1
	}
	alertsQuerier, err := alerts.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		logger.Errorw("initializing alerts querier", zap.Error(err))
		return 1
	}

	tracesQuerier, err := traces.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		logger.Errorw("initializing traces querier", zap.Error(err))
		return 1
	}

	errorsQuerier, err := errortracking.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		logger.Errorw("initializing errors querier", zap.Error(err))
		return 1
	}

	queryAPI := core.NewQueryAPI(
		logger.Desugar(),
		metricsQuerier,
		logsQuerier,
		analyticsQuerier,
		alertsQuerier,
		tracesQuerier,
		errorsQuerier,
	)

	resource, err := instrumentation.ConstructOTELResource(
		ctx,
		constants.QueryAPIName,
		constants.DockerImageTag,
	)
	if err != nil {
		logger.Errorw("defining otel resource failed", zap.Error(err))
		return 1
	}

	meterProvider, meterProviderDoneF, err := instrumentation.ConstructOTELMeterProvider(
		ctx,
		resource,
		metricsRegistry,
		otlpEndpoint, otlpCACertificate, otlpTokenSecretFile,
	)
	if err != nil {
		logger.Errorw("constructing otel meter provider failed", zap.Error(err))
		return 1
	}
	defer func() {
		err := meterProviderDoneF()
		if err != nil {
			logger.Errorw("shutting down meter provider failed", zap.Error(err))
		}
	}()
	defaultMeter := instrumentation.NewDefaultMeter(constants.QueryAPIName, meterProvider)

	tracerProvider, tracePropagator, traceProviderDoneF, err := instrumentation.ConstructOTELTracingTools(
		ctx,
		resource,
		otlpEndpoint, otlpCACertificate, otlpTokenSecretFile,
	)
	if err != nil {
		logger.Errorw("constructing otel trace provider failed", zap.Error(err))
		return 1
	}
	defer func() {
		err := traceProviderDoneF()
		if err != nil {
			logger.Errorw("shutting down trace provider failed", zap.Error(err))
		}
	}()

	redisClient := common.GetRedis(redisAddr, redisPassword, useRedisSentinel, redisConnectionPoolSize)
	rateLimiter, stopFn, err := common.GetRateLimiter(logger.Desugar(), rateLimitConfigFilePath, redisClient)
	defer stopFn()
	if err != nil {
		logger.Errorw("creating rate limiter: ", zap.Error(err))
		return 1
	}

	var alertsWriter alertsw.AlertWriter
	db, err := common.GetDB(clickHouseDSN, clickHouseCloudDSN, logger.Desugar())
	if err != nil {
		logger.Errorw("get db connection", zap.Error(err))
		return 1
	}
	alertsWriter = alertsw.NewAlertWriter(db, logger.Desugar())

	if len(gitlabOidcProviders) == 0 {
		logger.Errorw("no gitlabOidcProviders provided")
		return 1
	}

	rsaPrivateKey, err = common.GetRSAPrivateKeyFromEnv()
	if err != nil {
		logger.Errorw("getting rsa private key", zap.Error(err))
		return 1
	}
	if rsaPrivateKey == nil {
		logger.Info("no rsa private key via OIDC_PRIVATE_KEY_PEM provided")
	}
	authenticator, err := authproxy.NewAuthenticator(gitlabOidcProviders, rsaPrivateKey, logger)
	if err != nil {
		logger.Errorw("creating authenticator", zap.Error(err))
		return 1
	}

	if logLevel == "debug" {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	routerMain := gin.New()
	//  set ContextWithFallback=true so that gin.Context() can be directly used
	//  by otel to fetch current span
	routerMain.ContextWithFallback = true
	routerMain.Use(
		ginzap.GinzapWithConfig(
			logger.Desugar(),
			&ginzap.Config{
				TimeFormat: time.RFC3339,
				UTC:        true,
				SkipPaths:  []string{"/readyz"},
			},
		),
	)
	routerMain.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))
	routerMain.Use(common.RouteMetricsOtel(defaultMeter))

	routerMain.Use(
		otelgin.Middleware(
			constants.QueryAPIName,
			otelgin.WithPropagators(tracePropagator),
			otelgin.WithTracerProvider(tracerProvider),
		),
	)

	routerMain.Use(authproxy.RateLimitingHandler(logger, rateLimiter, alertsWriter))
	routerMain.Use(core.OIDCAuthMiddleware(logger.Desugar(), authenticator))

	queryAPI.SetRoutes(routerMain)

	logger.Infof("log level: %s", logLevel)
	logger.Infow("starting main HTTP server", "address", serviceAddr)
	logger.Infow("starting metrics HTTP server", "address", metricsAddr)

	metricServer := &http.Server{
		Addr:              metricsAddr,
		Handler:           routerMetrics,
		ReadHeaderTimeout: time.Second * 3,
	}

	mainServer := &http.Server{
		Addr:              serviceAddr,
		Handler:           routerMain,
		ReadHeaderTimeout: time.Second * 3,
	}

	err = common.ServeWithGracefulShutdown(ctx, logger, mainServer, metricServer)
	if err != nil {
		logger.Errorf("shutting down http servers: %w", err)
		return 1
	}

	logger.Info("server shutdown is complete")
	return 0
}
