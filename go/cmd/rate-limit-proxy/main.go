package main

import (
	"context"
	"flag"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/go-logr/zapr"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.opentelemetry.io/otel"
	_ "go.uber.org/automaxprocs"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	authproxy "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/auth-proxy"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
)

var (
	target                  string
	metricsAddr             string
	serviceAddr             string
	logLevel                string
	otlpEndpoint            string
	otlpCACertificate       string
	otlpTokenSecretFile     string
	redisAddr               string
	redisPassword           string
	useRedisSentinel        bool
	redisConnectionPoolSize = 100
	rateLimitConfigFilePath string
	clickhouseDSN           string
	clickhouseCloudDSN      string
)

func parseCmdline() {
	flag.StringVar(&target,
		"target", "http://localhost:8080", "The address to proxy authenticated requests to.")
	flag.StringVar(&metricsAddr,
		"metrics-bind-address", ":9081", "The address the metric endpoint binds to.")
	flag.StringVar(&serviceAddr,
		"service-bind-address", ":9080", "The address the service endpoint binds to.")
	flag.StringVar(&logLevel,
		"log-level", "info", "The log-level to use.")
	flag.StringVar(&otlpEndpoint,
		"otlp-endpoint", "", "otlp collector endpoint, without `/v1/traces` or `/v1/metrics` suffix")
	flag.StringVar(&otlpCACertificate,
		"otlp-ca-cert", "", "otlp CA certificate to use while connecting over HTTPs")
	flag.StringVar(&otlpTokenSecretFile,
		"otlp-token-file", "", "file that contains Access Token to use when connecting to otel collector")
	flag.StringVar(&redisAddr, "redis-address", "", "address for redis server, used for ratelimiting")
	flag.StringVar(&redisPassword, "redis-password", "", "password for redis server")
	flag.BoolVar(&useRedisSentinel, "use-redis-sentinel", true, "use sentinel for redis HA")
	flag.StringVar(&rateLimitConfigFilePath, "rate-limit-config-path", "", "file path for rate limit config")
	flag.StringVar(&clickhouseDSN, "clickhouse-dsn", "", "connection DSN for clickhouse")
	flag.StringVar(&clickhouseCloudDSN, "clickhouse-cloud-dsn", "", "connection DSN clickhouse Cloud")
	flag.Parse()
}

func main() {
	os.Exit(runRateLimitProxy())
}

//nolint:funlen
func runRateLimitProxy() int {
	parseCmdline()

	logger, _, _ := instrumentation.SetupLogger(logLevel)
	// NOTE(prozlach): There is a tricky bug/feature that will prevent otel
	// logs from showing, unless you manually set the verbosity level to numerical
	// value when setting up zap. This is described here:
	//
	// https://github.com/go-logr/zapr?tab=readme-ov-file#increasing-verbosity
	//
	// It essentially boils down to zap log level debug reflecting `V(1)` level
	// for loggr. Unless you do the hack below when setting up the zap, entries
	// `V(2)` and above will not be visible and thus you will not get any logs
	// from otel.
	//
	// loggerCfg := zap.Config{
	//   ...
	//   Level:             zap.NewAtomicLevelAt(zapcore.Level(-6)),
	//   ...
	// }
	otel.SetLogger(zapr.NewLogger(logger.Desugar()))

	ctx, cancelFunc := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancelFunc()

	metricsRegistry, routerMetrics := instrumentation.ConstructPromMeterTooling()

	resource, err := instrumentation.ConstructOTELResource(
		ctx,
		constants.RateLimitProxyName,
		constants.DockerImageTag,
	)
	if err != nil {
		logger.Errorw("defining otel resource failed", zap.Error(err))
		return 1
	}

	meterProvider, meterProviderDoneF, err := instrumentation.ConstructOTELMeterProvider(
		ctx,
		resource,
		metricsRegistry,
		otlpEndpoint, otlpCACertificate, otlpTokenSecretFile,
	)
	if err != nil {
		logger.Errorw("constructing otel meter provider failed", zap.Error(err))
		return 1
	}
	defer func() {
		err := meterProviderDoneF()
		if err != nil {
			logger.Errorw("shutting down meter provider failed", zap.Error(err))
		}
	}()
	defaultMeter := instrumentation.NewDefaultMeter(constants.RateLimitProxyName, meterProvider)

	tracerProvider, tracePropagator, traceProviderDoneF, err := instrumentation.ConstructOTELTracingTools(
		ctx,
		resource,
		otlpEndpoint, otlpCACertificate, otlpTokenSecretFile,
	)
	if err != nil {
		logger.Errorw("constructing otel trace provider failed", zap.Error(err))
		return 1
	}
	defer func() {
		err := traceProviderDoneF()
		if err != nil {
			logger.Errorw("shutting down trace provider failed", zap.Error(err))
		}
	}()

	if logLevel == "debug" {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	redisClient := common.GetRedis(redisAddr, redisPassword, useRedisSentinel, redisConnectionPoolSize)
	rateLimiter, stopFn, err := common.GetRateLimiter(logger.Desugar(), rateLimitConfigFilePath, redisClient)
	defer stopFn()
	if err != nil {
		logger.Errorw("creating rate limiter: ", zap.Error(err))
		return 1
	}

	var alertsWriter alerts.AlertWriter
	if clickhouseDSN != "" || clickhouseCloudDSN != "" {
		db, err := common.GetDB(clickhouseDSN, clickhouseCloudDSN, logger.Desugar())
		if err != nil {
			logger.Errorw("get db connection", zap.Error(err))
		}
		alertsWriter = alerts.NewAlertWriter(db, logger.Desugar())
	}

	routerMain := gin.New()
	//  set ContextWithFallback=true so that gin.Context() can be directly used
	//  by otel to fetch current span
	routerMain.ContextWithFallback = true
	routerMain.Use(
		ginzap.GinzapWithConfig(
			logger.Desugar(),
			&ginzap.Config{
				TimeFormat: time.RFC3339,
				UTC:        true,
				SkipPaths:  []string{"/readyz"},
			},
		),
	)
	routerMain.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))
	routerMain.Use(common.RouteMetricsOtel(defaultMeter))

	routerMain.Use(
		otelgin.Middleware(
			constants.RateLimitProxyName,
			otelgin.WithPropagators(tracePropagator),
			otelgin.WithTracerProvider(tracerProvider),
		),
	)
	routerMain.Use(authproxy.RateLimitingHandler(logger, rateLimiter, alertsWriter))

	upstream, err := url.Parse(target)
	if err != nil {
		logger.Errorw("parsing target URL failed", zap.Error(err))
		return 1
	}

	proxy := httputil.NewSingleHostReverseProxy(upstream)
	routerMain.Any("/*path", func(ctx *gin.Context) {
		if ctx.Request.URL.Path == "/readyz" {
			ctx.Status(http.StatusOK)
			return
		}
		logger.Debug("allowed from sidecar")
		proxy.ServeHTTP(ctx.Writer, ctx.Request)
	})

	logger.Infof("log level: %s", logLevel)
	logger.Infow("starting main HTTP server", "address", serviceAddr)
	logger.Infow("starting metrics HTTP server", "address", metricsAddr)

	metricServer := &http.Server{
		Addr:              metricsAddr,
		Handler:           routerMetrics,
		ReadHeaderTimeout: time.Second * 3,
	}

	mainServer := &http.Server{
		Addr:              serviceAddr,
		Handler:           routerMain,
		ReadHeaderTimeout: time.Second * 3,
	}

	err = common.ServeWithGracefulShutdown(ctx, logger, mainServer, metricServer)
	if err != nil {
		logger.Errorf("shutting down http servers: %w", err)
		return 1
	}

	logger.Info("server shutdown is complete")
	return 0
}
