# An Open API based rest API for GitLab's Error Tracking

## Generation

[go-swagger](https://goswagger.io/) is installed as a tool and used for code generation.

Use `make regenerate` in the `go/` directory to regenerate the code.
