package main

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/analytics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
	"go.uber.org/zap"
)

// ReplacePathRouter strips urls to be served via query API.
// All query API requests have to be modified as the binary would be often running without an ingress.
type ReplacePathRouter struct {
	*gin.Engine
	logger *zap.Logger
}

func (r *ReplacePathRouter) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if strings.HasPrefix(req.URL.Path, "/observability/v1") {
		req.RequestURI = strings.Replace(
			req.RequestURI,
			"/observability/v1",
			"/v4/query",
			1,
		)
		req.URL.Path = strings.Replace(
			req.URL.Path,
			"/observability/v1",
			"/v4/query",
			1,
		)
	}

	r.Engine.ServeHTTP(w, req)
}

func setupQueryAPI(
	logger *zap.SugaredLogger,
	routerMain *gin.Engine,
) error {
	logger = logger.With("component", "query-api")

	metricsQuerier, err := metrics.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		return fmt.Errorf("initializing metrics querier: %w", err)
	}

	logsQuerier, err := logs.NewQuerier(
		clickHouseDSN, clickHouseCloudDSN,
		constants.LoggingCHQueryTimeout,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		return fmt.Errorf("initializing logs querier: %w", err)
	}

	analyticsQuerier, err := analytics.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		return fmt.Errorf("initializing analytics querier: %w", err)
	}

	alertsQuerier, err := alerts.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		return fmt.Errorf("initializing alerts querier; %w", err)
	}

	tracesQuerier, err := traces.NewQuerier(
		clickHouseDSN,
		clickHouseCloudDSN,
		nil,
		logger.Desugar(),
	)
	if err != nil {
		return fmt.Errorf("initializing traces querier: %w", err)
	}

	queryAPI := core.NewQueryAPI(
		logger.Desugar(),
		metricsQuerier,
		logsQuerier,
		analyticsQuerier,
		alertsQuerier,
		tracesQuerier,
		nil,
	)

	queryAPI.SetRoutes(routerMain)
	return nil
}
