# GOB Single Binary

This setup allows all Platform Insights features to be run within a single-binary.

## Development

To setup the stack locally, do the following:

### Setup JWT token signing key for OIDC authentication to work
```
make generate_key
```
### Build all dependencies
```
make build
```
### Run the stack via docker-compose
```
make start
```
### When done, you can delete everything with
```
make stop
```

## Testing

We also run an E2E test-suite to ensure the correctness of the features integrated via the single-binary.

To run the tests, ensure you have the stack running locally as described above.

Run the suite from inside the `test/agent` directory. From the project root:
```
cd test/agent
go test -v ./...
```