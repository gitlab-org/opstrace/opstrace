package main

import (
	"context"
	"crypto/rsa"
	"fmt"
	"net/http"
	"net/http/httputil"
	"os"
	"os/signal"
	"syscall"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/go-logr/zapr"
	natsserver "github.com/nats-io/nats-server/v2/server"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/spf13/cobra"
	natsinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/nats"
	snowplowconfig "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/config"
	clickhouseexporter "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/exporter/clickhouse"
	natssink "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/sinks/nats"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.opentelemetry.io/otel"
	_ "go.uber.org/automaxprocs"
	"go.uber.org/zap"

	alerts "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	authproxy "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/auth-proxy"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher"
)

var (
	// servers
	logLevel               string
	metricsAddr            string
	queryServiceAddr       string
	snowplowAddr           string
	snowplowConfigFilePath string
	enricherConfigFilePath string
	igluConfigFilePath     string
	igluEnrichmentsDir     string

	// database
	clickHouseDSN      string
	clickHouseCloudDSN string
	selfManagedMode    bool

	// instrumentation
	otlpEndpoint        string
	otlpCACertificate   string
	otlpTokenSecretFile string
	metricsRegistry     *prometheus.Registry
	routerMetrics       *http.ServeMux

	// middleware
	redisAddr               string
	redisPassword           string
	useRedisSentinel        bool
	redisConnectionPoolSize = 100
	rateLimitConfigFilePath string
	gitlabOidcProviders     []string
	privateKeyFile          string

	// nats
	natsAddr string

	// clickhouse exporter
	flushBatchSize int
)

var (
	rootCmd = &cobra.Command{
		Use:   "gob",
		Short: "A Gitlab Observability Backend all-in-one binary",
		Long: `A single binary that provides Gitlab observability ingestion via
			OpenTelemetry compliant collector and a Query API.
			See Documentation at https://docs.gitlab.com/ee/operations/.`,
		Version: constants.DockerImageTag,
		RunE:    runGOB,
	}

	otelCommand *cobra.Command
)

func setupFlags() {
	rootCmd.Flags().StringVar(&logLevel, "log-level", "info", "Log Level")
	rootCmd.Flags().StringVar(&clickHouseDSN, "clickhouse-dsn", "tcp://localhost:9000", "ClickHouse DSN")
	rootCmd.Flags().StringVar(&clickHouseCloudDSN, "clickhouse-cloud-dsn", "", "ClickHouse Cloud DSN")
	rootCmd.Flags().BoolVar(&selfManagedMode, "self-managed", false, "Enable self-managed mode")

	rootCmd.Flags().StringVar(&otlpEndpoint, "otlp-endpoint", "", "OTLP endpoint")
	rootCmd.Flags().StringVar(&otlpCACertificate, "otlp-ca-certificate", "", "OTLP CA certificate")
	rootCmd.Flags().StringVar(&otlpTokenSecretFile, "otlp-token-secret-file", "", "OTLP token secret file")

	rootCmd.Flags().StringArrayVar(&gitlabOidcProviders, "gitlab-oidc-provider", []string{}, "OIDC Providers for Gitlab")
	rootCmd.Flags().StringVar(&privateKeyFile, "private-key-file", "", "file path to OIDC private key file")
	rootCmd.Flags().StringVar(&redisAddr, "redis-address", "", "Redis address")
	rootCmd.Flags().StringVar(&redisPassword, "redis-password", "", "Redis password")
	rootCmd.Flags().BoolVar(&useRedisSentinel, "use-redis-sentinel", true, "Use Redis Sentinel")
	rootCmd.Flags().StringVar(&rateLimitConfigFilePath, "rate-limit-config-path", "", "file path for rate limit config")

	rootCmd.Flags().StringVar(&natsAddr, "nats-addr", "", "client address for NATS instance")

	rootCmd.Flags().StringVar(&metricsAddr, "metrics-bind-address", ":8081", "metrics addr")
	rootCmd.Flags().StringVar(&queryServiceAddr, "query-bind-address", ":8080", "query API addr")
	rootCmd.Flags().StringVar(&snowplowAddr, "snowplow-address", ":8082", "snowplow ingester addr")
	rootCmd.Flags().StringVar(&snowplowConfigFilePath, "snowplow-config", "", "filepath for snowplow ingester config")
	rootCmd.Flags().StringVar(&enricherConfigFilePath, "enricher-config", "", "filepath for snowplow enricher config")
	rootCmd.Flags().StringVar(&igluConfigFilePath, "iglu-config", "", "filepath for snowplow iglu repository config")
	rootCmd.Flags().StringVar(
		&igluEnrichmentsDir, "enrichments", "", "directory path for snowplow enrichment config JSONs",
	)

	rootCmd.Flags().IntVar(&flushBatchSize, "flush-batch-size", 1, "batch size to flush into ClickHouse")
}

func main() {
	otelCommand = setupOTELCommand()
	rootCmd.Flags().AddFlagSet(otelCommand.Flags())

	setupFlags()
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

//nolint:funlen,cyclop
func runGOB(cmd *cobra.Command, args []string) error {
	logger, _, _ := instrumentation.SetupLogger(logLevel)
	// NOTE(prozlach): There is a tricky bug/feature that will prevent otel
	// logs from showing, unless you manually set the verbosity level to numerical
	// value when setting up zap. This is described here:
	//
	// https://github.com/go-logr/zapr?tab=readme-ov-file#increasing-verbosity
	//
	// It essentially boils down to zap log level debug reflecting `V(1)` level
	// for loggr. Unless you do the hack below when setting up the zap, entries
	// `V(2)` and above will not be visible and thus you will not get any logs
	// from otel.
	//
	// loggerCfg := zap.Config{
	//   ...
	//   Level:             zap.NewAtomicLevelAt(zapcore.Level(-6)),
	//   ...
	// }
	otel.SetLogger(zapr.NewLogger(logger.Desugar()))

	ctx, cancelFunc := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancelFunc()

	err := setupMigrations(cmd.Context(), clickHouseDSN, clickHouseCloudDSN, logger.Desugar())
	if err != nil {
		return err
	}

	if logLevel == "debug" {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	logger.Infof("set log level: %s", logLevel)

	// unless provided one, setup a local NATS server
	if natsAddr == "" {
		ns, err := natsinternal.NewLocalServer(&natsserver.Options{Port: 4222}, 5*time.Second)
		if err != nil {
			return fmt.Errorf("setting-up local server: %w", err)
		}
		natsAddr = ns.ClientURL()
	}

	// setup initial NATS stream
	if err := setupNATSStreams(ctx, natsAddr); err != nil {
		logger.Errorf("setting up NATS streams: %v", err)
		return err
	}

	routerMain := gin.New()

	if err := setupInstrumentation(ctx, logger, routerMain); err != nil {
		logger.Errorf("setting up instrumentation: %v", err)
		return err
	}

	if err := setupMiddleware(logger, routerMain); err != nil {
		logger.Errorf("setting up middleware: %v", err)
		return err
	}

	if err := setupQueryAPI(logger, routerMain); err != nil {
		logger.Errorf("setting up query API: %v", err)
		return err
	}

	opts := clickhouseexporter.Options{
		FlushBatchSize: flushBatchSize,
	}
	if err := setupClickHouseExportController(ctx, logger.Desugar(), opts); err != nil {
		logger.Errorf("setting up CH export controller: %v", err)
		return err
	}

	if err := setupEnrichmentController(ctx, logger.Desugar()); err != nil {
		logger.Errorf("setting up enrichment controller: %v", err)
		return err
	}

	routerSnowplow := gin.New()
	if logLevel == "debug" {
		routerSnowplow.Use(RequestLogger(logger.Desugar()))
	}

	if err := setupSnowplowIngester(ctx, logger, routerSnowplow); err != nil {
		logger.Errorf("setting up snowplow ingestion: %v", err)
		return err
	}

	var servers []*http.Server
	if routerMetrics != nil {
		logger.Infow("starting metrics HTTP server", "address", metricsAddr)
		metricServer := &http.Server{
			Addr:              metricsAddr,
			Handler:           routerMetrics,
			ReadHeaderTimeout: time.Second * 3,
		}
		servers = append(servers, metricServer)
	}

	logger.Infow("starting snowplow ingestion server", "address", snowplowAddr)
	snowplowServer := &http.Server{
		Addr:              snowplowAddr,
		Handler:           routerSnowplow,
		ReadHeaderTimeout: time.Second * 3,
	}
	servers = append(servers, snowplowServer)

	logger.Infow("starting query API server", "address", queryServiceAddr)
	mainServer := &http.Server{
		Addr: queryServiceAddr,
		Handler: &ReplacePathRouter{
			Engine: routerMain,
			logger: logger.Desugar(),
		},
		ReadHeaderTimeout: time.Second * 3,
	}
	servers = append(servers, mainServer)

	go func() {
		err := common.ServeWithGracefulShutdown(ctx, logger, servers...)
		if err != nil {
			logger.Errorf("shutting down http servers: %w", err)
		}
		logger.Info("server shutdown is complete")
	}()

	if err := otelCommand.RunE(cmd, args); err != nil {
		return fmt.Errorf("running OTEL collector: %w", err)
	}

	return nil
}

func setupInstrumentation(
	ctx context.Context,
	logger *zap.SugaredLogger,
	routerMain *gin.Engine,
) error {
	// set ContextWithFallback=true so that gin.Context() can be directly used
	// by OTEL to fetch current span
	routerMain.ContextWithFallback = true

	resource, err := instrumentation.ConstructOTELResource(
		ctx,
		constants.AllInOneName,
		constants.DockerImageTag,
	)
	if err != nil {
		return fmt.Errorf("defining otel resource failed: %w", err)
	}

	metricsRegistry, routerMetrics = instrumentation.ConstructPromMeterTooling()
	metricsRegistry.MustRegister(
		gitlabobservabilityexporter.ConfiguredCollectors()...,
	)

	meterProvider, meterProviderDoneF, err := instrumentation.ConstructOTELMeterProvider(
		ctx,
		resource,
		metricsRegistry,
		otlpEndpoint, otlpCACertificate, otlpTokenSecretFile,
	)
	if err != nil {
		return fmt.Errorf("constructing otel meter provider failed: %w", err)
	}
	defer func() {
		err := meterProviderDoneF()
		if err != nil {
			logger.Errorw("shutting down meter provider failed", zap.Error(err))
		}
	}()
	defaultMeter := instrumentation.NewDefaultMeter(constants.AllInOneName, meterProvider)

	tracerProvider, tracePropagator, traceProviderDoneF, err := instrumentation.ConstructOTELTracingTools(
		ctx,
		resource,
		otlpEndpoint, otlpCACertificate, otlpTokenSecretFile,
	)
	if err != nil {
		return fmt.Errorf("constructing otel trace provider failed: %w", err)
	}
	defer func() {
		err := traceProviderDoneF()
		if err != nil {
			logger.Errorw("shutting down trace provider failed", zap.Error(err))
		}
	}()

	routerMain.Use(
		ginzap.GinzapWithConfig(
			logger.Desugar(),
			&ginzap.Config{
				TimeFormat: time.RFC3339,
				UTC:        true,
				SkipPaths:  []string{"/readyz"},
			},
		),
	)
	routerMain.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))
	routerMain.Use(common.RouteMetricsOtel(defaultMeter)) //nolint:contextcheck

	routerMain.Use(
		otelgin.Middleware(
			constants.AllInOneName,
			otelgin.WithPropagators(tracePropagator),
			otelgin.WithTracerProvider(tracerProvider),
		),
	)

	return nil
}

func setupMiddleware(
	logger *zap.SugaredLogger,
	routerMain *gin.Engine,
) error {
	if len(gitlabOidcProviders) == 0 {
		return fmt.Errorf("no gitlabOidcProviders provided")
	}

	var (
		rsaPrivateKey *rsa.PrivateKey
		err           error
	)
	if privateKeyFile != "" {
		rsaPrivateKey, err = common.GetRSAPrivateKeyFromFile(privateKeyFile)
	} else {
		rsaPrivateKey, err = common.GetRSAPrivateKeyFromEnv()
	}
	if err != nil {
		return fmt.Errorf("getting rsa private key: %w", err)
	}
	if rsaPrivateKey == nil {
		logger.Info("no rsa private key via OIDC_PRIVATE_KEY_PEM provided")
	}

	redisClient := common.GetRedis(redisAddr, redisPassword, useRedisSentinel, redisConnectionPoolSize)
	rateLimiter, stopFn, err := common.GetRateLimiter(logger.Desugar(), rateLimitConfigFilePath, redisClient)
	if err != nil {
		return fmt.Errorf("creating rate limiter: %w", err)
	}
	defer stopFn()

	db, err := common.GetDB(clickHouseDSN, clickHouseCloudDSN, logger.Desugar())
	if err != nil {
		return fmt.Errorf("get db connection: %w", err)
	}
	alertsWriter := alerts.NewAlertWriter(db, logger.Desugar())
	routerMain.Use(authproxy.RateLimitingHandler(logger, rateLimiter, alertsWriter))

	authenticator, err := authproxy.NewAuthenticator(gitlabOidcProviders, rsaPrivateKey, logger)
	if err != nil {
		return fmt.Errorf("creating authenticator: %w", err)
	}
	routerMain.Use(core.OIDCAuthMiddleware(logger.Desugar(), authenticator))

	return nil
}

func setupNATSStreams(ctx context.Context, natsAddr string) error {
	js, err := natsinternal.NewJetstream(natsAddr)
	if err != nil {
		return fmt.Errorf("setting up NATS Jetstream API: %w", err)
	}

	_, err = js.CreateOrUpdateStream(ctx, utils.CommonStreamConfig("RAW_EVENTS", []string{"raw_events.>"}))
	if err != nil {
		return fmt.Errorf("setting up raw events stream: %w", err)
	}

	_, err = js.CreateOrUpdateStream(ctx, utils.CommonStreamConfig("ENRICHED_EVENTS", []string{"enriched_events.>"}))
	if err != nil {
		return fmt.Errorf("setting up enriched events stream: %w", err)
	}

	return nil
}

func setupSnowplowIngester(
	_ context.Context,
	logger *zap.SugaredLogger,
	routerSnowplow *gin.Engine,
) error {
	config := snowplowconfig.NewConfig()
	if snowplowConfigFilePath != "" {
		if err := config.LoadFrom(snowplowConfigFilePath); err != nil {
			logger.Errorf("reading config from %s: %v", snowplowConfigFilePath, err)
			return err
		}
	}

	// if the collector config specifies a different NATS instance, use that one instead
	var (
		goodSink snowplow.Sinker
		badSink  snowplow.Sinker
	)
	if config.Streams.Good.URL != "" {
		natsAddr = config.Streams.Good.URL
		sink, err := natssink.New(logger.Desugar(), natsAddr, &config.Streams)
		if err != nil {
			return fmt.Errorf("instantiating good sink: %w", err)
		}
		goodSink = sink
	}
	if config.Streams.Bad.URL != "" {
		natsAddr = config.Streams.Bad.URL
		sink, err := natssink.New(logger.Desugar(), natsAddr, &config.Streams)
		if err != nil {
			return fmt.Errorf("instantiating bad sink: %w", err)
		}
		badSink = sink
	}

	appInfo := &snowplow.AppInfo{Name: "agent", Version: constants.DockerImageTag}
	snowplowSvc := snowplow.NewService(logger.Desugar(), config, goodSink, badSink, appInfo)
	snowplowSvc.SetRoutes(routerSnowplow) //nolint:contextcheck

	return nil
}

func setupEnrichmentController(ctx context.Context, logger *zap.Logger) error {
	//nolint:contextcheck
	enricher, err := enricher.NewController(
		natsAddr,
		enricherConfigFilePath,
		igluConfigFilePath,
		igluEnrichmentsDir,
		logger,
	)
	if err != nil {
		return err
	}

	errCh := make(chan error)
	go func() {
		errCh <- enricher.Start(ctx)
	}()

	waitCh := time.After(5 * time.Second)
	select {
	case err := <-errCh:
		return err
	case <-waitCh:
		return nil // if it hasn't returned until now, it should be good to roll
	}
}

func setupClickHouseExportController(ctx context.Context, logger *zap.Logger, opts clickhouseexporter.Options) error {
	chExporter, err := clickhouseexporter.NewController(natsAddr, clickHouseDSN, logger, opts)
	if err != nil {
		return err
	}

	errCh := make(chan error)
	go func() {
		errCh <- chExporter.Start(ctx)
	}()

	waitCh := time.After(5 * time.Second)
	select {
	case err := <-errCh:
		return err
	case <-waitCh:
		return nil // if it hasn't returned until now, it should be good to roll
	}
}

func RequestLogger(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		dump, err := httputil.DumpRequest(c.Request, true)
		if err != nil {
			panic(err)
		}
		logger.Debug("request", zap.String("dumped", string(dump)))
		c.Next()
	}
}
