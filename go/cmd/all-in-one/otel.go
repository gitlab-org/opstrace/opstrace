package main

import (
	"fmt"

	"github.com/open-telemetry/opentelemetry-collector-contrib/extension/pprofextension"
	"github.com/open-telemetry/opentelemetry-collector-contrib/processor/attributesprocessor"
	"github.com/open-telemetry/opentelemetry-collector-contrib/receiver/jaegerreceiver"
	"github.com/spf13/cobra"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/extensions/gitlaboidcauthextension"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/confmap"
	"go.opentelemetry.io/collector/confmap/provider/envprovider"
	"go.opentelemetry.io/collector/confmap/provider/fileprovider"
	"go.opentelemetry.io/collector/confmap/provider/yamlprovider"
	"go.opentelemetry.io/collector/exporter"
	"go.opentelemetry.io/collector/exporter/loggingexporter"
	"go.opentelemetry.io/collector/extension"
	"go.opentelemetry.io/collector/extension/ballastextension"
	"go.opentelemetry.io/collector/extension/zpagesextension"
	"go.opentelemetry.io/collector/otelcol"
	"go.opentelemetry.io/collector/processor"
	"go.opentelemetry.io/collector/processor/batchprocessor"
	"go.opentelemetry.io/collector/processor/memorylimiterprocessor"
	"go.opentelemetry.io/collector/receiver"
	"go.opentelemetry.io/collector/receiver/otlpreceiver"
)

func setupOTELCommand() *cobra.Command {
	return otelcol.NewCommand(
		otelcol.CollectorSettings{
			BuildInfo: component.BuildInfo{
				Command:     "collector",
				Description: "OpenTelemetry Collector for GOB",
				Version:     constants.DockerImageTag,
			},
			Factories: getComponents,
			ConfigProviderSettings: otelcol.ConfigProviderSettings{
				ResolverSettings: confmap.ResolverSettings{
					// ConfigProviderSettings allows configuring the way the Collector retrieves its configuration
					ProviderFactories: []confmap.ProviderFactory{
						envprovider.NewFactory(),
						fileprovider.NewFactory(),
						yamlprovider.NewFactory(),
					},
				},
			},
		},
	)
}

func getComponents() (otelcol.Factories, error) {
	var err error
	factories := otelcol.Factories{}

	factories.Extensions, err = extension.MakeFactoryMap(
		// TODO: ballastextension has been deprecated in favor or GOMEMLIMIT env variable. We need to use that.
		ballastextension.NewFactory(),
		pprofextension.NewFactory(),
		zpagesextension.NewFactory(),
		gitlaboidcauthextension.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("extension factory: %w", err)
	}

	factories.Receivers, err = receiver.MakeFactoryMap(
		otlpreceiver.NewFactory(),
		jaegerreceiver.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("receiver factory: %w", err)
	}

	factories.Processors, err = processor.MakeFactoryMap(
		attributesprocessor.NewFactory(),
		memorylimiterprocessor.NewFactory(),
		batchprocessor.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("processor factory: %w", err)
	}

	factories.Exporters, err = exporter.MakeFactoryMap(
		gitlabobservabilityexporter.NewFactory(),
		loggingexporter.NewFactory(),
	)
	if err != nil {
		return otelcol.Factories{}, fmt.Errorf("exporter factory: %w", err)
	}

	return factories, nil
}
