package main

import (
	"context"
	"fmt"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
	alertsmigrations "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/alerts"
	analyticsmigrations "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/analytics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/logging"
	metricsmigrations "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	snowplowmigrations "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/snowplow"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
	"go.uber.org/zap"
)

func setupMigrations(ctx context.Context, clickHouseDSN, clickHouseCloudDSN string, logger *zap.Logger) error {
	//nolint:contextcheck
	conn, err := common.GetDB(clickHouseDSN, clickHouseCloudDSN, logger)
	if err != nil {
		return fmt.Errorf("get connection: %w", err)
	}
	for _, bundle := range []struct {
		database      string
		migrationsFun func(bool, bool) ([]*goose.Migration, error)
	}{
		{
			constants.ErrorTrackingAPIDatabaseName,
			errortracking.SetupMigrations,
		},
		{
			constants.TracingDatabaseName,
			tracing.SetupMigrations,
		},
		{
			constants.MetricsDatabaseName,
			metricsmigrations.SetupMigrations,
		},
		{
			constants.LoggingDatabaseName,
			logging.SetupMigrations,
		},
		{
			constants.AlertsDatabaseName,
			alertsmigrations.SetupMigrations,
		},
		{
			constants.AnalyticsDatabaseName,
			analyticsmigrations.SetupMigrations,
		},
		{
			constants.SnowplowDatabaseName,
			snowplowmigrations.SetupMigrations,
		},
	} {
		err = migrations.CreateDB(ctx, conn, bundle.database)
		if err != nil {
			return fmt.Errorf("create db for %v : %w", bundle.database, err)
		}

		// NOTE: Due to underlying complexity of managing table engines - development mode is mutually exclusive of
		// self-managed mode which require replication support.
		migrs, err := bundle.migrationsFun(selfManagedMode, !selfManagedMode)
		if err != nil {
			return fmt.Errorf("setting migrations for %v: %w", bundle.database, err)
		}
		dsn := clickHouseDSN
		if clickHouseCloudDSN != "" {
			dsn = clickHouseCloudDSN
		}
		err = migrations.RunGooseMigrations(ctx, dsn, bundle.database, migrs, true)
		if err != nil {
			return fmt.Errorf("running migrations for %v: %w", bundle.database, err)
		}
	}

	return nil
}
