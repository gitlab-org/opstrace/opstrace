package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var (
	logLevel           string
	port               int
	privateKeyFilePath string
)

func setupFlags() {
	flag.IntVar(&port, "listening-port", 18080, "listening-port")
	flag.StringVar(&privateKeyFilePath, "private-key-file", "", "private-key-file")
	flag.Parse()
}

func main() {
	setupFlags()

	logger, _, _ := instrumentation.SetupLogger(logLevel)

	server, err := testutils.NewOIDCServer(&port, &privateKeyFilePath)
	if err != nil {
		logger.Errorw("create oidc server", zap.Error(err))
		os.Exit(1)
	}
	logger.Infow("sample oidc server at: ", zap.String("url", server.URL))

	ctx, cancelFunc := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer cancelFunc()

	// wait for interrupt
	<-ctx.Done()
	server.Close()
}
