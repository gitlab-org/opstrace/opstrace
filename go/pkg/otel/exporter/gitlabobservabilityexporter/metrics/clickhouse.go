package metrics

import (
	"context"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"
)

type ClickHouseIngester struct {
	db           clickhouse.Conn
	logger       *zap.Logger
	ingesters    map[pmetric.MetricType]MetricsModel
	writeTimeout time.Duration
}

func NewClickHouseIngester(db clickhouse.Conn, logger *zap.Logger) *ClickHouseIngester {
	ingester := &ClickHouseIngester{
		db:           db,
		logger:       logger,
		ingesters:    make(map[pmetric.MetricType]MetricsModel),
		writeTimeout: time.Second * 5,
	}
	ingester.setupIngesters()
	return ingester
}

func (c *ClickHouseIngester) setupIngesters() {
	c.ingesters = map[pmetric.MetricType]MetricsModel{
		pmetric.MetricTypeGauge:                &gaugeMetrics{},
		pmetric.MetricTypeSum:                  &sumMetrics{},
		pmetric.MetricTypeHistogram:            &histogramMetrics{},
		pmetric.MetricTypeExponentialHistogram: &exponentialHistogramMetrics{},
	}
}

func (c *ClickHouseIngester) Add(
	resAttr map[string]string,
	resURL string,
	scopeInstr pcommon.InstrumentationScope,
	scopeURL string,
	m pmetric.Metric,
) {
	if _, supported := c.ingesters[m.Type()]; !supported {
		panic("unsupported metric type not caught upstream, programming error likely")
	}
	c.ingesters[m.Type()].Add(resAttr, resURL, scopeInstr, scopeURL, m)
}

func (c *ClickHouseIngester) Write(ctx context.Context, ingestionTimestamp time.Time) error {
	ctx, cancel := context.WithTimeout(ctx, c.writeTimeout)
	defer cancel()

	for _, m := range c.ingesters {
		if err := m.write(ctx, c.db, c.logger, ingestionTimestamp); err != nil {
			return err
		}
	}

	return nil
}

func (c *ClickHouseIngester) SetWriteTimeout(t time.Duration) {
	c.writeTimeout = t
}
