package gitlabobservabilityexporter

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/prometheus/common/model"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/pdata/plog"
	conventions "go.opentelemetry.io/collector/semconv/v1.25.0"
	"go.uber.org/zap"

	pcommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/instrumentation"
)

type logsExporter struct {
	db     *database
	logger *zap.Logger
	cfg    *config
}

func newLogsExporter(logger *zap.Logger, cfg *config) (*logsExporter, error) {
	var (
		db  *database
		err error
	)
	if cfg.ClickHouseCloudDSN != "" {
		db, err = newCloudDB(cfg)
		if err != nil {
			return nil, err
		}
	} else {
		// if cloud db is not provided, use self-hosted instance
		db, err = newDB(cfg)
		if err != nil {
			return nil, err
		}
	}
	return &logsExporter{
		db:     db,
		logger: logger,
		cfg:    cfg,
	}, nil
}

func (e *logsExporter) start(ctx context.Context, _ component.Host) error {
	pingCtx, cancel := context.WithTimeout(ctx, 5*time.Second) // arbitrary timeout for now
	defer cancel()

	if err := e.db.conn.Ping(pingCtx); err != nil {
		return fmt.Errorf("pinging clickhouse backend failed: %w", err)
	}

	return nil
}

func (e *logsExporter) shutdown(_ context.Context) error {
	if e.db != nil && e.db.conn != nil {
		if err := e.db.conn.Close(); err != nil {
			return fmt.Errorf("closing underlying connection: %w", err)
		}
	}
	return nil
}

const (
	insertLogsSQL = `
INSERT INTO ` + constants.LoggingDatabaseName + `.` + constants.LoggingTableName + ` (
    ProjectId,
    NamespaceId,
    TenantId,
    Fingerprint,
    Timestamp,
    ObservedTimestamp,
    TraceId,
    SpanId,
    TraceFlags,
    SeverityText,
    SeverityNumber,
    ServiceName,
    Body,
    ResourceAttributes,
    LogAttributes
)`
)

//nolint:funlen // (prozlach): 50% of this function are indendted function calls
func (e *logsExporter) pushLogsData(ctx context.Context, ld plog.Logs) error {
	m := &plog.ProtoMarshaler{}

	var (
		batchSize float64
		batch     driver.Batch
		err       error
		// Default to configured TenantID which is configured
		// in the per-tenant deployment. This will be "" in the
		// centralized service. We override this below
		// if there is a tenantID in the resource
		tenantID = e.cfg.TenantID
	)
	batch, err = e.db.conn.PrepareBatch(ctx, insertLogsSQL, driver.WithReleaseConnection())
	if err != nil {
		return fmt.Errorf("preparing logs batch for clickhouse backend: %w", err)
	}

	instrumentedBatch := instrumentation.NewInstrumentedBatch("logs.main", e.logger, e.db.conn, batch)

	logs := ld.ResourceLogs()
	for i := 0; i < logs.Len(); i++ {
		scopeLogs := logs.At(i)
		scopeLogsResource := scopeLogs.Resource()
		scopeLogsAttributes := common.AttributesToMap(scopeLogsResource.Attributes())

		var serviceName string
		if v, ok := scopeLogsResource.Attributes().Get(conventions.AttributeServiceName); ok {
			serviceName = v.Str()
		}

		scopeLogsSlice := scopeLogs.ScopeLogs()
		for j := 0; j < scopeLogsSlice.Len(); j++ {
			logRecords := scopeLogsSlice.At(j).LogRecords()

			for k := 0; k < logRecords.Len(); k++ {
				logRecord := logRecords.At(k)

				logRecordAttributes := common.AttributesToMap(logRecord.Attributes())
				// The centralized ingest service relies on the tenantID being
				// set as an attribute, like the projectID and namespaceID.
				tid := common.GetTenantID(logRecordAttributes)
				if tid != "" {
					tenantID = tid
				}
				projectID := common.GetProjectID(logRecordAttributes)
				namespaceID, err := common.GetNamespaceID(logRecordAttributes)
				if err != nil {
					e.logger.Error("collecting namespaceID from log attributes: ", zap.Error(err))
				}
				logRecordBody := logRecord.Body().Str()
				fingerprint := getLogFingerprint(
					logRecordBody,
					logRecord.SpanID().String(),
					scopeLogsAttributes,
					logRecordAttributes,
				)

				delete(logRecordAttributes, common.NamespaceIDHeader)
				delete(logRecordAttributes, common.ProjectIDHeader)
				delete(logRecordAttributes, common.TenantIDHeader)

				severityNumber := logRecord.SeverityNumber()
				if severityNumber == plog.SeverityNumberUnspecified {
					severityNumber = pcommon.SeverityTextToNumber(logRecord.SeverityText())
				} // otherwise we assume that ServerityNumber and SeverityText are in sync.

				e.logger.Debug("append log record",
					zap.String("ProjectId", projectID),
					zap.String("TenantId", tenantID),
					zap.Int64("NamespaceId", namespaceID),
					zap.String("Fingerprint", fingerprint),
					zap.Time("Timestamp", logRecord.Timestamp().AsTime()),
					zap.Time("ObservedTimestamp", time.Now()),
					zap.String("TraceId", logRecord.TraceID().String()),
					zap.String("SpanId", logRecord.SpanID().String()),
					zap.Uint32("TraceFlags", uint32(logRecord.Flags())),
					zap.String("SeverityText", logRecord.SeverityText()),
					zap.Int32("SeverityNumber", int32(severityNumber)),
					zap.String("ServiceName", serviceName),
					zap.String("Body", logRecordBody),
					zap.Reflect("ResourceAttributes", scopeLogsAttributes),
					zap.Reflect("LogAttributes", logRecordAttributes),
				)

				if err := instrumentedBatch.Append(
					// identify which tenant/project the write belongs to
					tenantID,
					projectID,
					// actual arguments to persist
					projectID,
					namespaceID,
					tenantID,
					fingerprint,
					logRecord.Timestamp().AsTime(),
					time.Now(),
					common.TraceIDToFixedString(logRecord.TraceID()),
					common.SpanIDToFixedString(logRecord.SpanID()),
					uint32(logRecord.Flags()),
					logRecord.SeverityText(),
					severityNumber,
					serviceName,
					logRecordBody,
					scopeLogsAttributes,
					logRecordAttributes,
				); err != nil {
					e.logger.Warn("appending log entry failed", zap.Error(err))
					continue
				}
				batchSize++
			}
			logsReceivedCounter.WithLabelValues(tenantID).Add(float64(logRecords.Len()))
		}
	}
	logsdataSizeBytes.WithLabelValues(tenantID).Add(float64(m.LogsSize(ld)))
	logsIngestedCounter.WithLabelValues(tenantID).Add(batchSize)

	if err := instrumentedBatch.Send(ctx, time.Now()); err != nil {
		return fmt.Errorf("sending logs batch to clickhouse backend: %w", err)
	}
	return nil
}

func getLogFingerprint(
	body, spanID string, resourceAttributes, logAttributes map[string]string,
) string {
	labelSet := model.LabelSet{}
	for k, v := range resourceAttributes {
		labelSet[model.LabelName("resAttr-"+k)] = model.LabelValue(v)
	}
	for k, v := range logAttributes {
		labelSet[model.LabelName("logAttr-"+k)] = model.LabelValue(v)
	}
	labelSet[model.LabelName("__log_body")] = model.LabelValue(body)
	labelSet[model.LabelName("__log_spanid")] = model.LabelValue(spanID)
	return labelSet.Fingerprint().String()
}
