package gitlabobservabilityexporter

import "github.com/prometheus/client_golang/prometheus"

var (
	// tracing
	tracedataSizeBytes = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "traces_size_bytes",
			Help:      "size of traces received in bytes",
		},
		[]string{"tenant"},
	)
	spansReceivedCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "spans_received",
			Help:      "number of spans received per tenant",
		},
		[]string{"tenant"},
	)
	spansIngestedCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "spans_ingested",
			Help:      "number of spans ingested per tenant",
		},
		[]string{"tenant"},
	)

	// logs
	logsdataSizeBytes = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "logs_size_bytes",
			Help:      "size of logs received in bytes",
		},
		[]string{"tenant"},
	)
	logsReceivedCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "logs_received",
			Help:      "number of logs received per tenant",
		},
		[]string{"tenant"},
	)
	logsIngestedCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: "custom",
			Name:      "logs_ingested",
			Help:      "number of logs ingested per tenant",
		},
		[]string{"tenant"},
	)
)

func ConfiguredCollectors() []prometheus.Collector {
	return []prometheus.Collector{
		tracedataSizeBytes,
		spansReceivedCounter,
		spansIngestedCounter,
		logsdataSizeBytes,
		logsReceivedCounter,
		logsIngestedCounter,
	}
}
