package instrumentation

import (
	"context"
	"fmt"
	"reflect"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"go.uber.org/zap"
)

type InstrumentedBatch struct {
	dimension string
	logger    *zap.Logger
	db        clickhouse.Conn
	batch     driver.Batch
	telemetry map[string]telemetry
}

type telemetry struct {
	tenantID   string
	projectID  string
	dimension  string
	eventCount int64
	byteCount  int64
}

func NewInstrumentedBatch(
	dimension string,
	logger *zap.Logger,
	db clickhouse.Conn,
	batch driver.Batch,
) *InstrumentedBatch {
	return &InstrumentedBatch{
		dimension: dimension,
		logger:    logger,
		db:        db,
		batch:     batch,
		telemetry: make(map[string]telemetry),
	}
}

func (b *InstrumentedBatch) updateTelemetry(tenantID, projectID string, size int64) {
	key := fmt.Sprintf("%s:%s", tenantID, projectID)
	temp := telemetry{}
	if _, ok := b.telemetry[key]; ok {
		temp = b.telemetry[key]
	}
	temp.tenantID = tenantID
	temp.projectID = projectID
	temp.byteCount += size
	temp.eventCount += 1
	temp.dimension = b.dimension
	b.telemetry[key] = temp
}

func (b *InstrumentedBatch) Append(tenantID, projectID string, v ...any) error {
	// generate instrumentation for this batch
	var writeSize int64
	for _, t := range v {
		size, err := computeSize(reflect.ValueOf(t))
		// fmt.Printf("estimated size of %v, type: %v ---> %d\n", t, reflect.TypeOf(t), size)
		if err != nil {
			b.logger.Error("estimating size of", zap.Any("value", t), zap.Error(err))
			continue
		}
		writeSize += size
	}
	// bookkeep telemetry generated so far
	b.updateTelemetry(tenantID, projectID, writeSize)
	// append slice to internal batch
	if err := b.batch.Append(v...); err != nil {
		return fmt.Errorf("appending to data batch: %w", err)
	}
	return nil
}

const (
	insertAnalyticsIngestionTableSQL = `INSERT INTO %s.%s (
		TenantId,
		ProjectId,
		Dimension,
		AggTime,
		EventsCount,
		BytesCount
	)`
)

func (b *InstrumentedBatch) Send(ctx context.Context, ingestionTimestamp time.Time) error {
	b.logger.Debug("sending data from instrumented batch")
	if err := b.batch.Send(); err != nil {
		return fmt.Errorf("sending data batch: %w", err)
	}
	// persist instrumentation collected for this batch
	b.logger.Debug("sending telemetry from instrumented batch")
	telemetryBatch, err := b.db.PrepareBatch(
		ctx,
		fmt.Sprintf(insertAnalyticsIngestionTableSQL, constants.AnalyticsDatabaseName, constants.AnalyticsIngestionTableName),
		driver.WithReleaseConnection(),
	)
	if err != nil {
		return fmt.Errorf("preparing telemetry batch: %w", err)
	}
	// ingest telemetry aggregated to the hour
	refTime := ingestionTimestamp.UTC()
	refTimeHour := time.Date(
		refTime.Year(), refTime.Month(), refTime.Day(),
		refTime.Hour(), 0, 0, 0,
		refTime.Location(),
	)
	for _, v := range b.telemetry {
		if err := telemetryBatch.Append(
			v.tenantID,
			v.projectID,
			v.dimension,
			refTimeHour,
			v.eventCount,
			v.byteCount,
		); err != nil {
			b.logger.Error("appending to telemetry batch", zap.Error(err))
			continue
		}
	}
	// send telemetry batch with retries, only log errors to prevent double counting
	err = b.retriable(3, 1, func() error {
		return telemetryBatch.Send()
	})
	if err != nil {
		b.logger.Error("sending telemetry batch", zap.Error(err))
	}
	return nil
}

func (b *InstrumentedBatch) retriable(count int, wait int, f func() error) error {
	var err error
	for i := 0; i < count; i++ {
		if i > 0 {
			b.logger.Debug("retrying after error, in", zap.Error(err), zap.Int("seconds", wait))
			time.Sleep(time.Duration(wait) * time.Second)
			wait *= 2
		}
		if err = f(); err == nil {
			return nil
		}
	}
	return fmt.Errorf("after %d attempts, last error: %w", count, err)
}

var (
	timeType time.Time
)

//nolint:cyclop
func computeSize(rValue reflect.Value) (int64, error) {
	var rv reflect.Value
	// check if we received a pointer
	if rValue.Kind() == reflect.Pointer {
		rv = rValue.Elem()
	} else {
		rv = rValue
	}
	// ensure rv represents a valid value
	if !rv.IsValid() {
		if rValue.Kind() == reflect.Pointer {
			return int64(rValue.Type().Size()), nil
		}
		return 0, nil
	}

	if rv.Type() == reflect.TypeOf(timeType) {
		return 8, nil // DateTime64 underneath
	}

	var (
		size      int64
		keySize   int64
		fieldSize int64
		err       error
	)

	//nolint:exhaustive
	switch rv.Kind() {
	case reflect.String:
		size += int64(len(rv.String()) + 1) // CH representation

	case reflect.Slice:
		if rv.Len() > 0 {
			for i := 0; i < rv.Len(); i++ {
				temp := rv.Index(i)
				// handling []any
				if temp.Kind() == reflect.Interface {
					temp = temp.Elem()
				}
				fieldSize, err = computeSize(temp)
				if err != nil {
					continue
				}
				size += fieldSize
			}
		}

	case reflect.Map:
		iter := rv.MapRange()
		for iter.Next() {
			keySize, err = computeSize(iter.Key())
			if err != nil {
				continue
			}
			fieldSize, err = computeSize(iter.Value())
			if err != nil {
				continue
			}
			size += (keySize + fieldSize)
		}
		size += 8 // CH representation

	case reflect.Struct:
		size = 0
		for i := 0; i < rv.NumField(); i++ {
			fieldRv := rv.Field(i)
			if !fieldRv.IsValid() {
				continue
			}
			fieldSize, err = computeSize(fieldRv)
			if err != nil {
				continue
			}
			size += fieldSize
		}

	default:
		size += int64(rv.Type().Size())
	}

	return size, err
}
