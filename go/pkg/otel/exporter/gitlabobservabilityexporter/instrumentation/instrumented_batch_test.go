package instrumentation

import (
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestIntrumentedBatch(t *testing.T) {
	test := func(t *testing.T, v any) int64 {
		t.Helper()
		size, err := computeSize(reflect.ValueOf(v))
		require.NoError(t, err)
		return size
	}

	assert.Equal(t, test(t, bool(true)), int64(1))

	assert.Equal(t, test(t, "foobar"), int64(7)) // n+1

	assert.Equal(t, test(t, uint(1)), int64(8))
	assert.Equal(t, test(t, int(1)), int64(8))

	assert.Equal(t, test(t, uint8(1)), int64(1))
	assert.Equal(t, test(t, uint16(1)), int64(2))
	assert.Equal(t, test(t, uint32(1)), int64(4))
	assert.Equal(t, test(t, uint64(1)), int64(8))

	assert.Equal(t, test(t, int8(1)), int64(1))
	assert.Equal(t, test(t, int16(1)), int64(2))
	assert.Equal(t, test(t, int32(1)), int64(4))
	assert.Equal(t, test(t, int64(1)), int64(8))

	assert.Equal(t, test(t, byte(1)), int64(1))

	assert.Equal(t, test(t, rune(1)), int64(4))

	assert.Equal(t, test(t, float32(1)), int64(4))
	assert.Equal(t, test(t, float64(1)), int64(8))

	// struct
	type fooT struct {
		a int64   // 8 bytes
		b float64 // 8 bytes
		c string  // n+1 bytes
	}
	fooStruct := fooT{int64(1), float64(2.0), "foo"}
	assert.Equal(t, test(t, fooStruct), int64(20))

	// slice
	fooSlice := []int32{}
	assert.Equal(t, test(t, fooSlice), int64(0))
	fooSlice = []int32{1, 2, 3}
	assert.Equal(t, test(t, fooSlice), int64(12))

	// array
	fooArray := [3]int32{1, 2, 3}
	assert.Equal(t, test(t, fooArray), int64(12))

	// map
	fooMap := make(map[int32]int32)
	fooMap[1] = 2
	assert.Equal(t, test(t, fooMap), int64(16))

	// time.Time
	fooTime := time.Now()
	assert.Equal(t, test(t, fooTime), int64(8))
}
