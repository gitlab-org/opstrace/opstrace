package gitlaboidcauthextension

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	"strings"

	"go.opentelemetry.io/collector/client"
	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/extension/auth"
	"go.uber.org/zap"

	authproxy "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/auth-proxy"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

type gitlabOIDCExtension struct {
	cfg *Config

	logger        *zap.Logger
	authenticator *authproxy.Authenticator
	privateKey    *rsa.PrivateKey
}

var (
	errNotAuthenticated                  = errors.New("authentication didn't succeed")
	errInvalidAuthenticationHeaderFormat = errors.New("invalid authorization header format")
	errNoOIDCProviders                   = errors.New("no OIDC providers specified")
	errBearerTokenNotFound               = errors.New("bearer token not found")
)

func newExtension(_ context.Context, cfg *Config, logger *zap.Logger) (auth.Server, error) {
	if cfg.Attribute == "" {
		cfg.Attribute = defaultAttribute
	}

	// validate config
	err := cfg.Validate()
	if err != nil {
		return nil, err
	}

	privateKey, err := common.GetRSAPrivateKeyFromEnv()
	if err != nil {
		return nil, fmt.Errorf("load RSA private key: %w", err)
	}

	oe := &gitlabOIDCExtension{
		cfg:        cfg,
		logger:     logger,
		privateKey: privateKey,
	}

	return auth.NewServer(
		auth.WithServerStart(oe.start),
		auth.WithServerAuthenticate(oe.authenticate),
		auth.WithServerShutdown(oe.Shutdown),
	), nil
}

func (e *gitlabOIDCExtension) start(context.Context, component.Host) error {
	//nolint:contextcheck
	authenticator, err := authproxy.NewAuthenticator(e.cfg.OIDCProviders, e.privateKey, e.logger.Sugar())
	if err != nil {
		return fmt.Errorf("authenticator: %w", err)
	}

	e.authenticator = authenticator
	e.logger.Info("starting gitlaboidc extension with providers:", zap.Strings("urls", e.cfg.OIDCProviders))
	return nil
}

// authenticate checks whether the given context contains valid auth data.
// Successfully authenticated calls will always return a nil error and a context with the auth data.
func (e *gitlabOIDCExtension) authenticate(ctx context.Context, headers map[string][]string) (context.Context, error) {
	var authHeaders []string
	for k, v := range headers {
		if strings.EqualFold(k, e.cfg.Attribute) {
			authHeaders = v
			break
		}
	}
	if len(authHeaders) == 0 {
		e.logger.Error(
			"no auth attribute headers found",
			zap.String("attribute", e.cfg.Attribute),
			zap.Error(errNotAuthenticated),
		)
		return ctx, errNotAuthenticated
	}

	// we only use the first header, if multiple values exist

	if !strings.HasPrefix(authHeaders[0], "Bearer ") {
		e.logger.Error("bearer token not found: ", zap.Error(errBearerTokenNotFound))
		return ctx, errBearerTokenNotFound
	}

	parts := strings.Split(authHeaders[0], " ")
	if len(parts) != 2 {
		e.logger.Error("headers: ", zap.Error(errInvalidAuthenticationHeaderFormat))
		return ctx, errInvalidAuthenticationHeaderFormat
	}

	tokenStr := parts[1]
	token, ok, errs := e.authenticator.AuthenticateFromToken(ctx, tokenStr)
	if !ok {
		e.logger.Error("auth denied: ", zap.Errors("errors", errs))
		return ctx, fmt.Errorf("%w : %w", errNotAuthenticated, errors.Join(errs...))
	}

	claims, err := authproxy.GetClaims(token)
	if err != nil {
		e.logger.Error("claims: ", zap.Error(err))
		return ctx, fmt.Errorf("%w - claims: %w", errNotAuthenticated, err)
	}

	if err := authproxy.ValidateRequest(headers, claims); err != nil {
		e.logger.Error("headers mismatch with claims: ", zap.Error(err))
		return ctx, fmt.Errorf("%w - headers mismatch with claims: %w", errNotAuthenticated, err)
	}

	headers = common.RewriteHeaders(headers)

	projectID := common.GetGOBProjectIDHeader(headers)
	tenantID := common.GetGOBTenantIDHeader(headers)
	namespaceID := common.GetGOBNamespaceIDHeader(headers)
	cl := client.FromContext(ctx)
	cl.Auth = &authData{
		raw:               tokenStr,
		subject:           claims.Subject,
		realm:             claims.Realm,
		gitlabNamespaceID: namespaceID,
		projectID:         projectID,
		tenantID:          tenantID,
	}

	e.logger.Info(
		"auth succeeded for: ",
		zap.String("projectID", projectID),
		zap.String("tenantID", tenantID),
		zap.String("namespaceID", namespaceID),
	)
	return client.NewContext(ctx, cl), nil
}

func (e *gitlabOIDCExtension) Shutdown(_ context.Context) error {
	e.authenticator.Cancel()
	return nil
}
