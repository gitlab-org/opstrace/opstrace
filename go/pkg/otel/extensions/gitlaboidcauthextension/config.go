package gitlaboidcauthextension

// Config has the configuration for the OIDC Authenticator extension.
type Config struct {
	// OIDC provider URLs
	// Required.
	OIDCProviders []string `mapstructure:"oidc_providers"`

	// The attribute (header name) to look for auth data. Optional, default value: "authorization".
	Attribute string `mapstructure:"attribute"`
}

func (c *Config) Validate() error {
	if len(c.OIDCProviders) == 0 {
		return errNoOIDCProviders
	}

	return nil
}
