package gitlaboidcauthextension

import (
	"context"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/extension"
)

const (
	Type                         = "gitlaboidc"
	GitlabOIDCExtensionStability = component.StabilityLevelAlpha
	defaultAttribute             = "authorization"
)

// NewFactory creates a factory for the OIDC Authenticator extension.
func NewFactory() extension.Factory {
	return extension.NewFactory(
		component.MustNewType(Type),
		createDefaultConfig,
		createExtension,
		GitlabOIDCExtensionStability,
	)
}

func createDefaultConfig() component.Config {
	return &Config{
		Attribute: defaultAttribute,
	}
}

func createExtension(ctx context.Context, set extension.Settings, cfg component.Config) (extension.Extension, error) {
	return newExtension(ctx, cfg.(*Config), set.Logger)
}
