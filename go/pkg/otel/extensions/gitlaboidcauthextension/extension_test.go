package gitlaboidcauthextension

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.opentelemetry.io/collector/component/componenttest"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

func TestOIDCAuthenticationSucceeded(t *testing.T) {
	// prepare
	oidcServer, err := testutils.NewOIDCServer(nil, nil)
	require.NoError(t, err)
	defer oidcServer.Close()

	config := &Config{
		OIDCProviders: []string{oidcServer.URL},
	}
	p, err := newExtension(context.Background(), config, zap.NewNop())
	require.NoError(t, err)

	err = p.Start(context.Background(), componenttest.NewNopHost())
	require.NoError(t, err)

	payload := map[string]string{
		"gitlab_realm":        "self-managed",
		"gitlab_namespace_id": "22",
		"sub":                 "instance1",
	}
	token, err := oidcServer.Token(payload)
	require.NoError(t, err)

	// test
	ctx, err := p.Authenticate(
		context.Background(), map[string][]string{
			"X-Gitlab-Realm":        {"self-managed"},
			"X-Gitlab-Instance-Id":  {"instance1"},
			"X-GitLab-Namespace-id": {"22"},
			"X-GitLab-Project-id":   {"11"},
			"authorization":         {fmt.Sprintf("Bearer %s", token)},
		})

	// verify
	assert.NoError(t, err)
	assert.NotNil(t, ctx)

	// test, upper-case header
	ctx, err = p.Authenticate(context.Background(), map[string][]string{
		"Authorization":         {fmt.Sprintf("Bearer %s", token)},
		"X-Gitlab-Realm":        {"self-managed"},
		"X-Gitlab-Instance-Id":  {"instance1"},
		"X-GitLab-Namespace-id": {"22"},
		"X-GitLab-Project-id":   {"11"},
	})

	// verify
	assert.NoError(t, err)
	assert.NotNil(t, ctx)
}

func TestNoOIDCProvider(t *testing.T) {
	config := &Config{}
	p, err := newExtension(context.Background(), config, zap.NewNop())
	assert.Equal(t, errNoOIDCProviders, err)
	assert.Nil(t, p)
}

func TestOIDCInvalidAuthHeader(t *testing.T) {
	// prepare
	config := &Config{
		OIDCProviders: []string{"example-provider.com"},
	}
	p, err := newExtension(context.Background(), config, zap.NewNop())
	require.NoError(t, err)

	// test
	ctx, err := p.Authenticate(context.Background(), map[string][]string{"authorization": {"some-value"}})

	// verify
	assert.Equal(t, errBearerTokenNotFound, err)
	assert.NotNil(t, ctx)
}

func TestOIDCNotAuthenticated(t *testing.T) {
	// prepare
	config := &Config{
		OIDCProviders: []string{"example-provider.com"},
	}
	p, err := newExtension(context.Background(), config, zap.NewNop())
	require.NoError(t, err)

	// test
	ctx, err := p.Authenticate(context.Background(), make(map[string][]string))

	// verify
	assert.Equal(t, errNotAuthenticated, err)
	assert.NotNil(t, ctx)
}

func TestProviderNotReachable(t *testing.T) {
	config := &Config{
		OIDCProviders: []string{"https://unreachable"},
	}
	p, err := newExtension(context.Background(), config, zap.NewNop())
	require.NoError(t, err)

	err = p.Start(context.Background(), componenttest.NewNopHost())
	// verify
	assert.Error(t, err)
}

func TestFailedToVerifyToken(t *testing.T) {
	// prepare
	oidcServer, err := testutils.NewOIDCServer(nil, nil)
	require.NoError(t, err)
	defer oidcServer.Close()

	config := &Config{
		OIDCProviders: []string{oidcServer.URL},
	}
	p, err := newExtension(context.Background(), config, zap.NewNop())
	require.NoError(t, err)

	err = p.Start(context.Background(), componenttest.NewNopHost())
	require.NoError(t, err)

	// test
	ctx, err := p.Authenticate(context.Background(), map[string][]string{"authorization": {"Bearer some-token"}})

	// verify
	assert.True(t, errors.Is(err, errNotAuthenticated))
	assert.NotNil(t, ctx)
}

func TestShutdown(t *testing.T) {
	// prepare
	oidcServer, err := testutils.NewOIDCServer(nil, nil)
	require.NoError(t, err)
	defer oidcServer.Close()

	config := &Config{
		OIDCProviders: []string{oidcServer.URL},
	}
	p, err := newExtension(context.Background(), config, zap.NewNop())
	require.NoError(t, err)
	require.NotNil(t, p)
	err = p.Start(context.Background(), componenttest.NewNopHost())
	require.NoError(t, err)

	// test
	err = p.Shutdown(context.Background())

	// verify
	assert.NoError(t, err)
}
