package gitlaboidcauthextension

import "go.opentelemetry.io/collector/client"

// Assert interface on auth data
var _ client.AuthData = (*authData)(nil)

type authData struct {
	raw               string
	realm             string
	subject           string
	gitlabNamespaceID string
	tenantID          string
	projectID         string
}

func (a *authData) GetAttribute(name string) any {
	switch name {
	case "subject":
		return a.subject
	case "namespaceid":
		return a.gitlabNamespaceID
	case "projectid":
		return a.projectID
	case "tenantid":
		return a.tenantID
	case "raw":
		return a.raw
	case "realm":
		return a.realm
	default:
		return nil
	}
}

func (*authData) GetAttributeNames() []string {
	return []string{"subject", "namespaceid", "projectid", "tenantid", "raw", "realm"}
}
