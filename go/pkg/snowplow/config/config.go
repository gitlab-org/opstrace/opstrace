package config

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/go-akka/configuration"
)

type Config struct {
	Interface        string
	Port             int32
	HSTS             HSTSConfig
	Networking       NetworkingConfig
	Cookie           CookieConfig
	CookieBounce     CookieBounceConfig
	DoNotTrackCookie DoNotTrackCookieConfig
	Streams          StreamsConfig
}

func NewConfig() *Config {
	return &Config{
		Interface: "0.0.0.0",
		Port:      8080,
		Cookie: CookieConfig{
			Enabled:    true,
			Expiration: 365 * 24 * time.Hour, // 1 year
			Name:       "sp",
			Secure:     true,
			HTTPOnly:   true,
			SameSite:   http.SameSiteNoneMode,
		},
		CookieBounce: CookieBounceConfig{
			Enabled:                 false,
			Name:                    "n3pc",
			FallbackNetworkUserID:   "00000000-0000-4000-A000-000000000000",
			ForwardedProtocolHeader: "X-Forwarded-Proto",
		},
		Streams: StreamsConfig{
			Good:                SinkConfig{Name: "good"},
			Bad:                 SinkConfig{Name: "bad"},
			UseIPAsPartitionKey: false,
		},
		HSTS: HSTSConfig{
			Enable: true,
			MaxAge: 24 * time.Hour,
		},
	}
}

func (c *Config) LoadFrom(path string) error {
	contents, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("reading filepath: %w", err)
	}
	config := configuration.ParseString(string(contents))
	c.Interface = config.GetString("collector.interface", "127.0.0.1")
	c.Port = config.GetInt32("config.port", 8080)
	c.Cookie = readCookieConfig(config)
	c.DoNotTrackCookie = readDoNotTrackCookieConfig(config)
	c.Streams = readStreamsConfig(config)
	return nil
}

type StreamsConfig struct {
	Good                SinkConfig
	Bad                 SinkConfig
	UseIPAsPartitionKey bool
}

type SinkConfig struct {
	Name   string
	URL    string
	Config struct {
		MaxBytes int64
	}
	Buffer struct {
		ByteLimit   int64
		RecordLimit int64
		TimeLimit   int64
	}
}

func readStreamsConfig(config *configuration.Config) StreamsConfig {
	s := StreamsConfig{}
	s.UseIPAsPartitionKey = config.GetBoolean("collector.streams.useIpAddressAsPartitionKey", false)

	s.Good.Name = config.GetString("collector.streams.good.name", "good")
	s.Good.URL = config.GetString("collector.streams.good.url", "nats://0.0.0.0:4222")
	s.Good.Config.MaxBytes = config.GetInt64("collector.streams.good.maxBytes", 1000000)

	s.Bad.Name = config.GetString("collector.streams.bad.name", "bad")
	s.Bad.URL = config.GetString("collector.streams.bad.url", "nats://0.0.0.0:4222")
	s.Bad.Config.MaxBytes = config.GetInt64("collector.streams.bad.maxBytes", 1000000)

	return s
}

type CookieConfig struct {
	Name           string
	Enabled        bool
	Expiration     time.Duration // Time until expiration
	Domains        []string
	FallbackDomain string
	SameSite       http.SameSite
	Secure         bool
	HTTPOnly       bool
}

func readCookieConfig(config *configuration.Config) CookieConfig {
	c := CookieConfig{}
	c.Name = config.GetString("collector.cookie.name")
	c.Enabled = config.GetBoolean("collector.cookie.enabled")
	c.Expiration = config.GetTimeDuration("collector.cookie.expiration")
	c.Domains = config.GetStringList("collector.cookie.domains")
	c.FallbackDomain = config.GetString("collector.cookie.domains")
	sameSite := config.GetString("collector.cookie.sameSite")
	if sameSite != "" {
		switch sameSite {
		case "None":
			c.SameSite = http.SameSiteNoneMode
		case "Strict":
			c.SameSite = http.SameSiteStrictMode
		}
	}
	c.Secure = config.GetBoolean("collector.cookie.secure")
	c.HTTPOnly = config.GetBoolean("collector.cookie.httpOnly")
	return c
}

type CookieBounceConfig struct {
	Enabled                 bool
	Name                    string
	FallbackNetworkUserID   string
	ForwardedProtocolHeader string
}

type HSTSConfig struct {
	Enable bool
	MaxAge time.Duration
}

type NetworkingConfig struct {
	IdleTimeout           time.Duration
	ResponseHeaderTimeout time.Duration
	MaxConnections        int
	MaxRequestLineLength  int
	MaxHeadersLength      int
}

type DoNotTrackCookieConfig struct {
	Enabled bool
	Name    string
	Value   string
}

func readDoNotTrackCookieConfig(config *configuration.Config) DoNotTrackCookieConfig {
	c := DoNotTrackCookieConfig{}
	c.Enabled = config.GetBoolean("collector.doNotTrackCookie.enabled")
	c.Name = config.GetString("collector.doNotTrackCookie.name")
	c.Value = config.GetString("collector.doNotTrackCookie.value")
	return c
}
