package config

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadFrom(t *testing.T) {
	testfile := filepath.Join("testdata", "example.hocon")

	cfg := NewConfig()
	cfg.LoadFrom(testfile)

	assert.Equal(t, "0.0.0.0", cfg.Interface)
	assert.Equal(t, int32(8080), cfg.Port)
	assert.Equal(t, "good", cfg.Streams.Good.Name)
	assert.Equal(t, "bad", cfg.Streams.Bad.Name)
	assert.Equal(t, false, cfg.Streams.UseIPAsPartitionKey)
}
