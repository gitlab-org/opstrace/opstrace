package snowplow

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/config"
)

func extractIP(r *http.Request, spAnonymous *string) string {
	if spAnonymous == nil {
		if ip := r.Header.Get("X-Forwarded-For"); ip != "" {
			return ip
		}
		if ip := r.RemoteAddr; ip != "" {
			return ip
		}
	}
	return ""
}

const (
	spAnonymousNuid string = "00000000-0000-0000-0000-000000000000"
)

func getNetworkUserID(r *http.Request, cookie string, spAnonymous *string) string {
	// if spAnonymous is not nil, return the anonymous network user ID.
	if spAnonymous != nil {
		return spAnonymousNuid
	}
	// check query params otherwise
	queryParams, err := url.ParseQuery(r.URL.RawQuery)
	if err == nil {
		if nuid := queryParams.Get("nuid"); nuid != "" {
			return nuid
		}
	}
	// if "nuid" is not in query params too, fall back to the cookie if present
	if cookie != "" {
		return cookie
	}
	return ""
}

// bounceLocationHeaders builds a location header redirecting to itself to check if third-party cookies are blocked.
func bounceLocationHeader(
	conf config.CookieBounceConfig,
	shouldBounce bool,
	r *http.Request,
) *http.Header {
	if shouldBounce {
		redirectURI := r.URL
		redirectQuery := redirectURI.Query()
		redirectQuery.Set(conf.Name, "true")
		redirectURI.RawQuery = redirectQuery.Encode()

		// extract the forwarded scheme from the request headers
		forwardedScheme := ""
		if h := r.Header.Get(conf.ForwardedProtocolHeader); h != "" {
			// validate if the scheme is either "http" or "https"
			if h == "http" || h == "https" {
				forwardedScheme = h
			}
		}
		if forwardedScheme != "" {
			redirectURI.Scheme = forwardedScheme
		}

		locationHeader := http.Header{}
		locationHeader.Set("Location", redirectURI.String())
		return &locationHeader
	}
	return nil
}

func computePartitionKey(ip string, ipAsPartitionKey bool) (string, string) {
	if ip == "" {
		ip = "unknown"
	}
	if ipAsPartitionKey {
		return ip, ip
	}
	return ip, uuid.New().String()
}

func getFilteredHeaders(r *http.Request, spAnonymous *string) []string {
	var headers []string

	for name, values := range r.Header {
		// filter out specific headers if spAnonymous is present.
		if spAnonymous != nil && (strings.EqualFold(name, "X-Forwarded-For") ||
			strings.EqualFold(name, "X-Real-IP") ||
			strings.EqualFold(name, "Cookie")) {
			continue
		}
		// format the headers as "Header-Name: Header-Value".
		for _, value := range values {
			headers = append(headers, fmt.Sprintf("%s: %s", name, value))
		}
	}
	return headers
}
