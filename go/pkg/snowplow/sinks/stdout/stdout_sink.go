package stdout

import (
	"context"

	"go.uber.org/zap"
)

type StdoutSink struct {
	logger *zap.Logger
}

func New(logger *zap.Logger) *StdoutSink {
	return &StdoutSink{
		logger: logger,
	}
}

func (s *StdoutSink) IsHealthy() bool {
	return true
}

//nolint:unparam
func (s *StdoutSink) StoreRawEvents(_ context.Context, events [][]byte, key string) error {
	for _, e := range events {
		s.logger.Sugar().Infof("%s\n", string(e))
	}
	return nil
}
