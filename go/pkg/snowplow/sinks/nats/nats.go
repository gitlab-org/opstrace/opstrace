package nats

import (
	"context"
	"errors"
	"fmt"

	"github.com/nats-io/nats.go/jetstream"
	"go.uber.org/zap"

	natsinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/nats"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/config"
)

type NatsSink struct {
	logger *zap.Logger
	js     jetstream.JetStream
}

func New(logger *zap.Logger, addr string, config *config.StreamsConfig) (*NatsSink, error) {
	logger.Info(
		"connecting to NATS sink",
		zap.String("addr", addr),
		zap.Any("config", config),
	)

	js, err := natsinternal.NewJetstream(addr)
	if err != nil {
		return nil, fmt.Errorf("setting up stream: %w", err)
	}

	return &NatsSink{
		logger: logger,
		js:     js,
	}, nil
}

func (s *NatsSink) IsHealthy() bool {
	return true
}

func (s *NatsSink) StoreRawEvents(ctx context.Context, events [][]byte, key string) error {
	var publishErrs []error
	for _, e := range events {
		subject := "raw_events." + key
		_, err := s.js.Publish(ctx, subject, e)
		if err != nil {
			publishErrs = append(publishErrs, err)
		}
	}
	return errors.Join(publishErrs...)
}
