package inmemory

import (
	"context"

	"go.uber.org/zap"
)

type Message []byte

type InMemorySink struct {
	logger *zap.Logger
	events []Message
}

func New(logger *zap.Logger) *InMemorySink {
	return &InMemorySink{
		logger: logger,
		events: make([]Message, 0),
	}
}

func (s *InMemorySink) IsHealthy() bool {
	return true
}

//nolint:unparam
func (s *InMemorySink) StoreRawEvents(_ context.Context, events [][]byte, _ string) error {
	for _, e := range events {
		s.events = append(s.events, e)
	}
	return nil
}

func (s *InMemorySink) GetEvents() []Message {
	return s.events
}
