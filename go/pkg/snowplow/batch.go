package snowplow

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"go.uber.org/zap"

	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	sv "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/schemas/com.snowplowanalytics.snowplow.badrows/sizeviolation"
	th "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/thrift/gen-go/collector_payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

type SplitBatch struct {
	logger         *zap.Logger
	appInfo        *AppInfo
	serializerPool *utils.TSerializerPool
}

func NewSplitBatch(logger *zap.Logger, appInfo *AppInfo, sp *utils.TSerializerPool) *SplitBatch {
	return &SplitBatch{
		logger:         logger,
		appInfo:        appInfo,
		serializerPool: sp,
	}
}

// EventSerializeResult represents the outcome of the serialization and split operations
type EventSerializeResult struct {
	GoodSerialized [][]byte
	BadEvents      [][]byte
}

// SplitAndSerializePayload attempts to split and serialize the CollectorPayload
//
// Snowplow-tracked payloads can contain one or more event objects within them, with each
// event object defining/tracking a given event in time. When persisting these payloads, we
// must ensure that we only ingest a collection of events within a maximum byte-size threshold
// to account for backing storage limits
//
// Any events that are over the given threshold are rejected & ingested truncated with a special
// marker called "oversizedPayload" inside a bad partition - which allows the user to still process
// them later as needed
//
// EventSerializeResult as a response describes this segregation logically
func (sb *SplitBatch) SplitAndSerializePayload(
	ctx context.Context,
	event *th.CollectorPayload,
	maxBytes int,
) (EventSerializeResult, error) {
	tser := sb.serializerPool.Get()
	defer sb.serializerPool.Put(tser)

	serialized, err := tser.Write(ctx, event)
	if err != nil {
		return EventSerializeResult{}, fmt.Errorf("serializing event: %w", err)
	}

	wholeEventBytes := len(serialized)
	sb.logger.Debug("serialized event bytes", zap.Int("len", wholeEventBytes))
	if wholeEventBytes < maxBytes {
		return EventSerializeResult{
			GoodSerialized: [][]byte{serialized},
			BadEvents:      nil,
		}, nil
	}

	body := event.GetBody()
	if body == "" {
		// oversized GET CollectorPayloads must be rejected with a size violation
		badrow := oversizedPayload(event, wholeEventBytes, maxBytes, "GET requests cannot be split")
		return EventSerializeResult{
			GoodSerialized: nil,
			BadEvents:      [][]byte{badrow},
		}, nil
	}

	if !json.Valid([]byte(body)) {
		// oversize POST CollectorPayloads without valid JSON body must be rejected
		badrow := oversizedPayload(event, wholeEventBytes, maxBytes, "cannot split POST requests which are not JSON")
		return EventSerializeResult{
			GoodSerialized: nil,
			BadEvents:      [][]byte{badrow},
		}, nil
	}

	sdd := core.NewSelfDescribingData()
	if err := sdd.Parse([]byte(body)); err != nil {
		return EventSerializeResult{}, fmt.Errorf(
			"cannot split POST requests which are not self-describing JSON: %v", err.Code(),
		)
	}
	sb.logger.Debug("self describing data", zap.Any("object", sdd))

	initialBodyDataBytes := 0
	for _, e := range sdd.Data {
		initialBodyDataBytes += len(e)
	}
	sb.logger.Debug("self describing data", zap.Int("len", initialBodyDataBytes))

	if wholeEventBytes-initialBodyDataBytes >= maxBytes {
		msg := "cannot split POST request because event without data is still too big"
		badrow := oversizedPayload(event, wholeEventBytes, maxBytes, msg)
		return EventSerializeResult{
			GoodSerialized: nil,
			BadEvents:      [][]byte{badrow},
		}, nil
	}

	splitResult := sb.split(sdd.Data, maxBytes-(wholeEventBytes-initialBodyDataBytes), 1)
	goodSerialized := sb.serializeBatch(ctx, event, splitResult.GoodBatches, sdd.SchemaKey)

	badEvents := make([][]byte, 0)
	if len(splitResult.FailedBigEvents) > 0 {
		msg := "this POST request split is still too large"
		for _, e := range splitResult.FailedBigEvents {
			badEvents = append(badEvents,
				oversizedPayload(event, len(e), maxBytes, msg),
			)
		}
	}

	return EventSerializeResult{
		GoodSerialized: goodSerialized,
		BadEvents:      badEvents,
	}, nil
}

// SplitBatchResult stores the results of the split operation
type SplitBatchResult struct {
	GoodBatches     [][]json.RawMessage
	FailedBigEvents []json.RawMessage
}

// Split a list of strings into batches, none of them exceeding a given size and return
// a SplitBatchResult containing a list of good batches and the list of events that were
// too big. Input strings exceeding the given size end up in the FailedBigEvents field of
// the result object.
//
// input: List of strings
// maximum: No good batch can exceed this size
// joinSize: Constant to add to the size of the string representing the additional comma
// needed to join separate event JSONs in a single array
func (sb *SplitBatch) split(input []json.RawMessage, maximum, joinSize int) SplitBatchResult {
	var (
		acc             [][]json.RawMessage
		failedBigEvents []json.RawMessage
		currentBatch    []json.RawMessage
		currentTotal    int
	)

	for _, head := range input {
		headSize := len(head)
		sb.logger.Debug("head event", zap.Int("len", headSize))
		//nolint:gocritic
		if headSize+joinSize > maximum {
			failedBigEvents = append(failedBigEvents, head)
		} else if currentTotal+headSize+joinSize > maximum {
			acc = append(acc, currentBatch)
			currentBatch = []json.RawMessage{head}
			currentTotal = (headSize + joinSize)
		} else {
			currentBatch = append(currentBatch, head)
			currentTotal += (headSize + joinSize)
		}
	}

	if len(currentBatch) > 0 {
		acc = append(acc, currentBatch)
	}

	return SplitBatchResult{GoodBatches: acc, FailedBigEvents: failedBigEvents}
}

// serializeBatch serializes batches into JSON format with schema included
func (sb *SplitBatch) serializeBatch(
	ctx context.Context,
	event *th.CollectorPayload,
	batches [][]json.RawMessage,
	schema *core.SchemaKey,
) [][]byte {
	var multierr error

	goodSerialized := make([][]byte, 0)
	for _, batch := range batches {
		batchStrArr := make([]string, 0)
		for _, b := range batch {
			batchStrArr = append(batchStrArr, string(b))
		}

		payload := event                                                                        // FIXME: deep copy
		batchData, _ := core.NewSelfDescribingDataFromParamsArr(schema, batchStrArr).AsString() //nolint:errcheck
		payload.Body = utils.StrPtr(batchData)

		tser := sb.serializerPool.Get()
		serialized, err := tser.Write(ctx, payload)
		sb.serializerPool.Put(tser)

		if err != nil {
			if multierr == nil {
				multierr = errors.New(err.Error())
			} else {
				errors.Join(multierr, err) //nolint:errcheck
			}
		} else {
			goodSerialized = append(goodSerialized, serialized)
		}
	}

	if multierr != nil {
		sb.logger.Error("serializing batch", zap.Error(multierr))
	}
	return goodSerialized
}

func firstN(s string, n int) string {
	r := []rune(s)
	if len(r) > n {
		return string(r[:n])
	}
	return s
}

func oversizedPayload(event *th.CollectorPayload, size, maxSize int, msg string) []byte {
	sv := sv.SizeViolation{
		Payload: firstN(event.GetBody(), maxSize/10),
		Failure: sv.Failure{
			Timestamp:               time.Now().UTC(),
			MaximumAllowedSizeBytes: maxSize,
			ActualSizeBytes:         size,
			Expectation:             msg,
		},
		Processor: sv.Processor{},
	}
	//nolint:errcheck
	svJSON, _ := json.Marshal(sv)
	return svJSON
}
