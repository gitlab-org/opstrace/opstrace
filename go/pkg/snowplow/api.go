package snowplow

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// SetRoutes initializes all the necessary routes
func (s *SnowplowService) SetRoutes(router *gin.Engine) {
	router.NoRoute(func(ginCtx *gin.Context) {
		ginCtx.AbortWithStatus(http.StatusNotFound)
	})

	mainGroup := router.Group("/:vendor/:version")
	{
		mainGroup.POST("", func(ctx *gin.Context) {
			s.Ingest(ctx, false)
		})

		mainGroup.GET("", func(ctx *gin.Context) {
			s.Ingest(ctx, true)
		})

		mainGroup.HEAD("", func(ctx *gin.Context) {
			s.Ingest(ctx, true)
		})
	}

	router.GET("/health", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "ok")
	})

	router.GET("/sink-health", func(ctx *gin.Context) {
		if s.SinksHealthy() {
			ctx.String(http.StatusOK, "ok")
		} else {
			ctx.AbortWithStatus(http.StatusServiceUnavailable)
		}
	})

	router.GET("/robots.txt", func(ctx *gin.Context) {
		//nolint:lll
		robots := "User-agent: *\nDisallow: /\n\nUser-agent: Googlebot\nDisallow: /\n\nUser-agent: AdsBot-Google\nDisallow: /\n"
		ctx.String(http.StatusOK, robots)
	})

	router.OPTIONS("/", func(ctx *gin.Context) {
		origin := ctx.Request.Header.Get("Origin")
		ctx.Header("Access-Control-Allow-Origin", origin)
		ctx.Header("Access-Control-Allow-Credentials", "true")
		ctx.Header("Access-Control-Allow-Headers", "Content-Type, SP-Anonymous")
		ctx.Header("Access-Control-Max-Age", strconv.Itoa(1))
		ctx.Status(http.StatusOK)
	})

	{
		router.GET("/ice.png", func(ctx *gin.Context) {
			s.Ingest(ctx, true)
		})

		router.HEAD("/ice.png", func(ctx *gin.Context) {
			s.Ingest(ctx, true)
		})
	}

	// Redirect Route
	// if !enableDefaultRedirect {
	// 	router.GET("/r/*", func(ctx *gin.Context) {
	// 		ctx.AbortWithStatus(http.StatusNotFound)
	// 	})
	// }

	// Root Route
	// if enableRootResponse {
	// 	router.GET("/", func(ctx *gin.Context) {
	// 		// s.RootResponse
	// 	})
	// }

	// Crossdomain Route
	// if enableCrossdomainTracking {
	// 	router.GET("/crossdomain.xml", func(ctx *gin.Context) {
	// 		domains := []string{}
	// 		secure := true
	// 		crossdomainResponse(domains, secure)
	// 	})
	// }
}
