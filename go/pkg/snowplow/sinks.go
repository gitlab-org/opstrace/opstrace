package snowplow

import (
	"context"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/sinks/nats"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/sinks/stdout"
)

type Sinker interface {
	IsHealthy() bool
	StoreRawEvents(context.Context, [][]byte, string) error
}

var _ Sinker = (*stdout.StdoutSink)(nil)
var _ Sinker = (*nats.NatsSink)(nil)
