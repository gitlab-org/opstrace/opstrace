package loaders

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
)

const APIPathRegex string = `^[/]?([^/]+)/([^/]+)[/]?$`

type CollectorPayloadAPI struct {
	Vendor  string
	Version string
}

func NewCollectorPayloadAPI() *CollectorPayloadAPI {
	return &CollectorPayloadAPI{}
}

func NewCollectorPayloadAPIFromParams(vendor, version string) *CollectorPayloadAPI {
	return &CollectorPayloadAPI{
		Vendor:  vendor,
		Version: version,
	}
}

func NewCollectorPayloadAPIFromPath(path string) (*CollectorPayloadAPI, error) {
	r := regexp.MustCompile(APIPathRegex)
	if r.Match([]byte(path)) {
		comps := r.FindStringSubmatch(path)
		if len(comps) != 3 {
			return nil, fmt.Errorf("parsing vendor/version from path: %s", path)
		}
		return NewCollectorPayloadAPIFromParams(comps[1], comps[2]), nil
	}
	if isIceRequest(path) {
		return NewCollectorPayloadAPIFromParams("com.snowplowanalytics.snowplow", "tp1"), nil
	}
	return nil, fmt.Errorf("path format violation")
}

func isIceRequest(path string) bool {
	return strings.HasPrefix(path, "/ice.png") || // legacy name for /i
		strings.EqualFold(path, "/i") || // legacy name for /com.snowplowanalytics.snowplow/tp1
		strings.HasPrefix(path, "/i?")
}

func (api *CollectorPayloadAPI) AsString() string {
	if api.Vendor == "com.snowplowanalytics.snowplow" && api.Version == "tp1" {
		return "/i"
	}
	return fmt.Sprintf("%s/%s", api.Vendor, api.Version)
}

type Source struct {
	Name     string
	Encoding string
	Hostname *string
}

func NewSource(name string, encoding string, hostname *string) *Source {
	return &Source{
		Name:     name,
		Encoding: encoding,
		Hostname: hostname,
	}
}

type Context struct {
	Timestamp  time.Time
	IPAddress  *string
	UserAgent  *string
	RefererURI *string
	Headers    []string
	UserID     *string
}

func NewContext(ts time.Time, ipAddress, userAgent, refererURI *string, headers []string, uid *string) *Context {
	return &Context{
		Timestamp:  ts,
		IPAddress:  ipAddress,
		UserAgent:  userAgent,
		RefererURI: refererURI,
		Headers:    headers,
		UserID:     uid,
	}
}

type CollectorPayload struct {
	API         *CollectorPayloadAPI
	QueryString []payload.NVP
	ContentType *string
	Body        *string
	Source      Source
	Context     Context
}

func NewCollectorPayload() *CollectorPayload {
	return &CollectorPayload{}
}

func NewCollectorPayloadFromParams(
	api *CollectorPayloadAPI,
	queryString []payload.NVP,
	contentType *string,
	body *string,
	source Source,
	context Context,
) *CollectorPayload {
	return &CollectorPayload{
		API:         api,
		QueryString: queryString,
		ContentType: contentType,
		Body:        body,
		Source:      source,
		Context:     context,
	}
}

func (cp *CollectorPayload) ToRaw() string {
	bts, _ := json.Marshal(cp) //nolint:errcheck
	return string(bts)
}
