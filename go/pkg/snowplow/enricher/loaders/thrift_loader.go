package loaders

import (
	"context"
	"fmt"
	"net/url"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/thrift/gen-go/collector_payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func BuildCollectorPayloadFromThriftPayload(
	ctx context.Context,
	bytes []byte,
) (*CollectorPayload, error) {
	tdeserPool := utils.NewTDeserializerPool()

	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	if err := tdeser.Read(ctx, cp, bytes); err != nil {
		return nil, fmt.Errorf("deserializing bytes: %w", err)
	}

	api, err := NewCollectorPayloadAPIFromPath(cp.GetPath())
	if err != nil {
		return nil, fmt.Errorf("parsing API for collector payload: %w", err)
	}

	return NewCollectorPayloadFromParams(
		api,
		ParseQueryString(cp.Querystring),
		cp.ContentType,
		cp.Body,
		Source{
			Name:     cp.Collector,
			Encoding: cp.Encoding,
			Hostname: cp.Hostname,
		},
		Context{
			Timestamp:  time.UnixMilli(cp.GetTimestamp()).UTC(),
			IPAddress:  utils.StrPtr(cp.IpAddress),
			RefererURI: cp.RefererUri,
			Headers:    cp.Headers,
			UserID:     cp.NetworkUserId,
			UserAgent:  cp.UserAgent,
		},
	), nil
}

func ParseQueryString(qs *string) []payload.NVP {
	nvps := make([]payload.NVP, 0)
	if qs == nil {
		return nvps
	}
	qcomps := strings.Split(*qs, "&")
	for _, comp := range qcomps {
		t := strings.SplitN(comp, "=", 2)
		if len(t) != 2 {
			continue
		}
		k, _ := url.QueryUnescape(t[0]) //nolint:errcheck
		v, _ := url.QueryUnescape(t[1]) //nolint:errcheck
		nvps = append(nvps, payload.NVP{
			Name:  k,
			Value: &v,
		})
	}
	return nvps
}
