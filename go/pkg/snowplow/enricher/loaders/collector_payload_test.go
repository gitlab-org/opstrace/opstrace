package loaders

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsIceRequest(t *testing.T) {
	for _, test := range []struct {
		path     string
		expected bool
	}{
		{path: "/i", expected: true},
		{path: "/ice.png", expected: true},
		{path: "/i?foo=1&bar=2", expected: true},
		{path: "/blah/i", expected: false},
		{path: "i", expected: false},
	} {
		result := isIceRequest(test.path)
		assert.Equal(t, result, test.expected)
	}
}
