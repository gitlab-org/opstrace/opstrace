package loaders

import (
	"context"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gin-gonic/gin"
	storagememory "github.com/snowplow/snowplow-golang-tracker/v3/pkg/storage/memory"
	sp "github.com/snowplow/snowplow-golang-tracker/v3/tracker"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

func TestBuildCollectorPayloadFromThriftPayload(t *testing.T) {
	gin.SetMode(gin.TestMode)

	server := testutils.NewTestServer()

	r := gin.Default()
	r.POST("/com.snowplowanalytics.snowplow/tp2", server.Handler)

	ts := httptest.NewServer(r)
	defer ts.Close()

	tsURL, _ := url.Parse(ts.URL)

	emitter := sp.InitEmitter(
		sp.RequireCollectorUri(tsURL.Host),
		sp.RequireStorage(*storagememory.Init()),
		sp.OptionRequestType("POST"),
		sp.OptionProtocol("http"),
		sp.OptionSendLimit(4),
	)
	subject := sp.InitSubject()
	subject.SetLanguage("en")
	tracker := sp.InitTracker(
		sp.RequireEmitter(emitter),
		sp.OptionSubject(subject),
	)

	data := map[string]interface{}{"targetUrl": "https://foobar.com"}
	sdj := sp.InitSelfDescribingJson("iglu:com.snowplowanalytics.snowplow/link_click/jsonschema/1-0-1", data)
	sde := sp.SelfDescribingEvent{Event: sdj}

	tracker.TrackSelfDescribingEvent(sde)
	tracker.Emitter.Stop()
	tracker.BlockingFlush(5, 1000)

	events := server.GoodSink.GetEvents()
	require.Len(t, events, 1)

	cp, err := BuildCollectorPayloadFromThriftPayload(context.TODO(), events[0])
	require.NoError(t, err)

	assert.Equal(t, "com.snowplowanalytics.snowplow/tp2", cp.API.AsString())
	assert.Equal(t, "application/json", *cp.ContentType)
	assert.Equal(t, "Go-http-client/1.1", *cp.Context.UserAgent)
}

func TestParseQueryString(t *testing.T) {
	for _, test := range []struct {
		name        string
		queryString *string
		expected    []payload.NVP
	}{
		{
			name:        "simple query string #1",
			queryString: utils.StrPtr("e=pv&dtm=1376487150616&tid=483686"),
			expected: payload.ToNameValuePairs(map[string]*string{
				"e":   utils.StrPtr("pv"),
				"dtm": utils.StrPtr("1376487150616"),
				"tid": utils.StrPtr("483686"),
			}),
		},
		{
			name:        "simple query string #2",
			queryString: utils.StrPtr("page=Celestial%2520Tarot%2520-%2520Psychic%2520Bazaar&vp=1097x482&ds=1097x1973"),
			expected: payload.ToNameValuePairs(map[string]*string{
				"page": utils.StrPtr("Celestial%20Tarot%20-%20Psychic%20Bazaar"),
				"vp":   utils.StrPtr("1097x482"),
				"ds":   utils.StrPtr("1097x1973"),
			}),
		},
		{
			name:        "superfluous ? ends up in first param's name",
			queryString: utils.StrPtr("?e=pv&dtm=1376487150616&tid=483686"),
			expected: payload.ToNameValuePairs(map[string]*string{
				"?e":  utils.StrPtr("pv"),
				"dtm": utils.StrPtr("1376487150616"),
				"tid": utils.StrPtr("483686"),
			}),
		},
		{
			name:        "empty querystring",
			queryString: nil,
			expected:    payload.ToNameValuePairs(map[string]*string{}),
		},
	} {
		t.Run(test.name, func(t *testing.T) {
			result := ParseQueryString(test.queryString)
			assert.ElementsMatch(t, test.expected, result)
		})
	}
}
