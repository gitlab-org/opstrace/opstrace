package enrichments

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows"
	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

var failureSchemaKey = core.NewSchemaKeyFromParams(
	"com.snowplowanalytics.snowplow", "failure", "jsonschema", core.NewSchemaVerFull(1, 0, 0),
)

type FailureContext struct {
	FailureType      string
	Errors           []string
	Schema           *core.SchemaKey
	Data             *string
	Timestamp        time.Time
	ComponentName    string
	ComponentVersion string
}

func (fc *FailureContext) AsJSON() string {
	bts, _ := json.Marshal(fc) //nolint:errcheck
	return string(bts)
}

type Failure interface {
	GetTimestamp() time.Time
	ToSDJ(processor *badrows.Processor) *core.SelfDescribingData
}

var _ Failure = (*SchemaViolation)(nil)
var _ Failure = (*EnrichmentFailure)(nil)

// schema-specific error
type SchemaViolation struct {
	SchemaViolation fd.SchemaViolationDetail
	Source          string
	Data            string
	Timestamp       time.Time
}

func NewSchemaViolation(
	v fd.SchemaViolationDetail,
	source string,
	data string,
	timestamp time.Time,
) *SchemaViolation {
	return &SchemaViolation{
		SchemaViolation: v,
		Source:          source,
		Data:            data,
		Timestamp:       timestamp,
	}
}

func (v *SchemaViolation) GetTimestamp() time.Time {
	return v.Timestamp
}

func (v *SchemaViolation) ToSDJ(processor *badrows.Processor) *core.SelfDescribingData {
	failureContext := fromSchemaViolation(v, v.Timestamp, processor)
	return core.NewSelfDescribingDataFromParams(failureSchemaKey, failureContext.AsJSON())
}

func fromSchemaViolation(
	sv *SchemaViolation,
	timestamp time.Time,
	processor *badrows.Processor,
) *FailureContext {
	var (
		failureType string
		errors      []string
		schema      *core.SchemaKey
		data        *string
	)
	switch v := sv.SchemaViolation.(type) {
	case *fd.SchemaViolationNotJSON:
		failureType = "NotJSON"
		errors = []string{
			utils.JSON(
				map[string]string{
					"message": v.Error,
					"source":  sv.Source,
				},
			),
		}
		schema = nil
		data = utils.StrPtr(utils.JSON(map[string]string{sv.Source: sv.Data}))
	case *fd.SchemaViolationNotIglu:
		failureType = "NotIglu"
		var message string
		switch v.Error.(type) {
		case *core.InvalidSchemaVer:
			message = "Invalid schema version"
		case *core.InvalidIgluURI:
			message = "Invalid Iglu URI"
		case *core.InvalidData:
			message = "Invalid data payload"
		case *core.InvalidSchema:
			message = "Invalid schema"
		case *core.InvalidMetaSchema:
			message = "Invalid metaschema"
		}
		errors = []string{
			utils.JSON(
				map[string]string{
					"message": message,
					"source":  sv.Source,
				},
			),
		}
		schema = nil
		data = utils.StrPtr(sv.Data)
	case *fd.SchemaViolationCriterionMismatch:
		failureType = "CriterionMismatch"
		message := fmt.Sprintf("Unexpected schema: %s does not match the criterion", v.SchemaKey.AsString())
		errors = []string{
			utils.JSON(
				map[string]string{
					"message":   message,
					"source":    sv.Source,
					"criterion": v.SchemaCriterion.AsString(),
				},
			),
		}
		schema = v.SchemaKey
		data = utils.StrPtr(sv.Data)
	}
	return &FailureContext{
		FailureType:      failureType,
		Errors:           errors,
		Schema:           schema,
		Data:             data,
		Timestamp:        timestamp,
		ComponentName:    processor.Artifact,
		ComponentVersion: processor.Version,
	}
}

// enrichment-specific error
type EnrichmentFailure struct {
	Failure   *fd.EnrichmentFailure
	Timestamp time.Time
}

func NewEnrichmentFailure(failure *fd.EnrichmentFailure, timestamp time.Time) *EnrichmentFailure {
	return &EnrichmentFailure{
		Failure:   failure,
		Timestamp: timestamp,
	}
}

func (v *EnrichmentFailure) GetTimestamp() time.Time {
	return v.Timestamp
}

func (v *EnrichmentFailure) ToSDJ(processor *badrows.Processor) *core.SelfDescribingData {
	failureContext := fromEnrichmentFailure(v, v.Timestamp, processor)
	return core.NewSelfDescribingDataFromParams(failureSchemaKey, failureContext.AsJSON())
}

func fromEnrichmentFailure(
	ef *EnrichmentFailure,
	timestamp time.Time,
	processor *badrows.Processor,
) *FailureContext {
	failureType := fmt.Sprintf("EnrichmentError: %s", ef.Failure.Information.Identifier)
	schemaKey := ef.Failure.Information.SchemaKey
	var (
		errors []string
		data   *string
	)
	switch v := ef.Failure.Message.(type) {
	case *fd.EnrichmentFailureInputData:
		errors = []string{
			utils.JSON(map[string]string{
				"message": fmt.Sprintf("%s - %s", v.Field, v.Expectation),
				"source":  v.Field,
			}),
		}
		data = utils.StrPtr(utils.JSON(map[string]string{v.Field: *v.Value}))
	case *fd.EnrichmentFailureSimple:
		errors = []string{
			utils.JSON(map[string]string{
				"message": v.Error,
			}),
		}
	case *fd.EnrichmentFailureIgluError:
		errors = []string{
			utils.JSON(map[string]string{
				"message": v.Error,
			}),
		}
	}
	return &FailureContext{
		FailureType:      failureType,
		Errors:           errors,
		Schema:           schemaKey,
		Data:             data,
		Timestamp:        timestamp,
		ComponentName:    processor.Artifact,
		ComponentVersion: processor.Version,
	}
}
