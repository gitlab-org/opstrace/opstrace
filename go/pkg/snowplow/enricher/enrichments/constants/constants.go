package constants

const (
	EventTypeStruct          = "struct"
	EventTypeUnstruct        = "unstruct"
	EventTypeAdImpression    = "ad_impression"
	EventTypeTransaction     = "transaction"
	EventTypeTransactionItem = "transaction_item"
	EventTypePageView        = "page_view"
	EventTypePagePing        = "page_ping"
)
