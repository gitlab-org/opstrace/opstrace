package transform

import (
	"strconv"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	ce "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/client_enrichments"
	ee "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/event_enrichments"
	me "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/misc_enrichments"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
	"go.uber.org/zap"
)

//nolint:errcheck,funlen,gocyclo,cyclop
func FirstPassTransform(raw *types.RawEvent, en *outputs.EnrichedEvent, logger *zap.Logger) {
	logger.Debug("first pass transform", zap.Any("raw event", raw))

	if raw == nil || raw.Parameters == nil {
		return
	}

	if v, ok := raw.Parameters["e"]; ok && v != nil {
		en.Event, _ = ee.ExtractEventType(*v)
	}

	if v, ok := raw.Parameters["ip"]; ok && v != nil {
		en.UserIPAddress = me.ExtractIP(*v)
	}

	if v, ok := raw.Parameters["aid"]; ok && v != nil {
		en.AppID = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["p"]; ok && v != nil {
		en.Platform, _ = me.ExtractPlatform(*v)
	}

	if v, ok := raw.Parameters["tid"]; ok && v != nil {
		value, _ := strconv.Atoi(*v)
		en.TxnID = &value
	}

	if v, ok := raw.Parameters["uid"]; ok && v != nil {
		en.UserID = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["duid"]; ok && v != nil {
		en.DomainUserid = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["nuid"]; ok && v != nil {
		en.NetworkUserid = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["ua"]; ok && v != nil {
		en.UserAgent = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["fp"]; ok && v != nil {
		en.UserFingerprint = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["vid"]; ok && v != nil {
		value, _ := strconv.Atoi(*v)
		en.DomainSessionIdx = &value
	}

	if v, ok := raw.Parameters["sid"]; ok && v != nil {
		en.DomainUserid, _ = utils.ValidateUUID(*v)
	}

	if v, ok := raw.Parameters["dtm"]; ok && v != nil {
		t, _ := ee.ExtractTimestamp(*v)
		en.DvceCreatedTstamp = utils.StrPtr(t)
	}

	if v, ok := raw.Parameters["ttm"]; ok && v != nil {
		t, _ := ee.ExtractTimestamp(*v)
		en.TrueTstamp = utils.StrPtr(t)
	}

	if v, ok := raw.Parameters["stm"]; ok && v != nil {
		t, _ := ee.ExtractTimestamp(*v)
		en.DvceSentTstamp = utils.StrPtr(t)
	}

	if v, ok := raw.Parameters["tna"]; ok && v != nil {
		en.NameTracker = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["tv"]; ok && v != nil {
		en.VTracker = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["cv"]; ok && v != nil {
		en.VCollector = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["lang"]; ok && v != nil {
		en.BrLang = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["f_pdf"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesPDF = t
	}

	if v, ok := raw.Parameters["f_fla"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesFlash = t
	}

	if v, ok := raw.Parameters["f_java"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesJava = t
	}

	if v, ok := raw.Parameters["f_dir"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesDirector = t
	}

	if v, ok := raw.Parameters["f_qt"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesQuicktime = t
	}

	if v, ok := raw.Parameters["f_realp"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesRealPlayer = t
	}

	if v, ok := raw.Parameters["f_wma"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesWindowsMedia = t
	}

	if v, ok := raw.Parameters["f_gears"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesGears = t
	}

	if v, ok := raw.Parameters["f_ag"]; ok && v != nil {
		t, _ := utils.StringToBooleanLikeByte(*v)
		en.BrFeaturesSilverlight = t
	}

	if v, ok := raw.Parameters["res"]; ok && v != nil {
		width, height, _ := ce.ExtractViewDimensions(*v)
		en.DvceScreenWidth = width
		en.DvceScreenHeight = height
	}

	if v, ok := raw.Parameters["cd"]; ok && v != nil {
		en.BrColorDepth = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["tz"]; ok && v != nil {
		en.OsTimezone = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["refr"]; ok && v != nil {
		en.PageReferrer = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["url"]; ok && v != nil {
		en.PageURL = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["page"]; ok && v != nil {
		en.PageTitle = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["cs"]; ok && v != nil {
		en.DocCharset = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["ds"]; ok && v != nil {
		width, height, _ := ce.ExtractViewDimensions(*v)
		en.DocWidth = width
		en.DocHeight = height
	}

	if v, ok := raw.Parameters["vp"]; ok && v != nil {
		width, height, _ := ce.ExtractViewDimensions(*v)
		en.BrViewWidth = width
		en.BrViewHeight = height
	}

	if v, ok := raw.Parameters["eid"]; ok && v != nil {
		en.EventID, _ = utils.ValidateUUID(*v)
	}

	// custom contexts
	if v, ok := raw.Parameters["co"]; ok && v != nil {
		en.Contexts, _ = utils.ExtractUnencodedJSON(*v)
	}

	if v, ok := raw.Parameters["cx"]; ok && v != nil {
		en.Contexts, _ = utils.ExtractBase64EncodedJSON(*v)
	}

	// custom unstructured events
	if v, ok := raw.Parameters["ue_pr"]; ok && v != nil {
		en.UnstructEvent, _ = utils.ExtractUnencodedJSON(*v)
	}

	if v, ok := raw.Parameters["ue_px"]; ok && v != nil {
		en.UnstructEvent, _ = utils.ExtractBase64EncodedJSON(*v)
	}

	if v, ok := raw.Parameters["tr_id"]; ok && v != nil {
		en.TrOrderID = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["tr_af"]; ok && v != nil {
		en.TrAffiliation = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["tr_tt"]; ok && v != nil {
		temp, _ := utils.StringToDouble(*v)
		en.TrTotal = utils.FloatPtr(temp)
	}

	if v, ok := raw.Parameters["tr_tx"]; ok && v != nil {
		temp, _ := utils.StringToDouble(*v)
		en.TrTax = utils.FloatPtr(temp)
	}

	if v, ok := raw.Parameters["tr_sh"]; ok && v != nil {
		temp, _ := utils.StringToDouble(*v)
		en.TrShipping = utils.FloatPtr(temp)
	}

	if v, ok := raw.Parameters["tr_ci"]; ok && v != nil {
		en.TrCity = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["tr_st"]; ok && v != nil {
		en.TrState = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["tr_co"]; ok && v != nil {
		en.TrCountry = me.ToTSVSafe(*v)
	}

	// ecommerce transaction items
	if v, ok := raw.Parameters["ti_id"]; ok && v != nil {
		en.TiOrderID = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["ti_sk"]; ok && v != nil {
		en.TiSKU = me.ToTSVSafe(*v)
	}

	// error in tracker protocol
	if v, ok := raw.Parameters["ti_na"]; ok && v != nil {
		en.TiName = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["ti_nm"]; ok && v != nil {
		en.TiName = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["ti_ca"]; ok && v != nil {
		en.TiCategory = me.ToTSVSafe(*v)
	}

	//   ("ti_pr", (CU.stringToJBigDecimal2, "ti_price")),
	//   ("ti_qu", (CU.stringToJInteger2, "ti_quantity")),

	// Page pings
	// ("pp_mix", (CU.stringToJInteger2, "pp_xoffset_min")),
	// ("pp_max", (CU.stringToJInteger2, "pp_xoffset_max")),
	// ("pp_miy", (CU.stringToJInteger2, "pp_yoffset_min")),
	// ("pp_may", (CU.stringToJInteger2, "pp_yoffset_max")),

	// currency
	if v, ok := raw.Parameters["tr_cu"]; ok && v != nil {
		en.TrCurrency = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["ti_cu"]; ok && v != nil {
		en.TiCurrency = me.ToTSVSafe(*v)
	}

	// custom structured events
	if v, ok := raw.Parameters["se_ca"]; ok && v != nil {
		en.SeCategory = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["se_ac"]; ok && v != nil {
		en.SeAction = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["se_la"]; ok && v != nil {
		en.SeLabel = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["se_pr"]; ok && v != nil {
		en.SeProperty = me.ToTSVSafe(*v)
	}

	if v, ok := raw.Parameters["se_va"]; ok && v != nil {
		temp, _ := strconv.ParseFloat(*v, 64)
		en.SeValue = &temp
	}
}
