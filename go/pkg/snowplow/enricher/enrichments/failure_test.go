package enrichments

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows"
	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	iglucore "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/testutils"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

var (
	testTimestamp = time.Now().UTC()
	testProcessor = badrows.NewProcessor("unit tests SCE", "v42")
	testSchemaKey = iglucore.NewSchemaKeyFromParams(
		"com.snowplowanalytics", "test", "jsonschema", iglucore.NewSchemaVerFull(1, 0, 0),
	)
)

func TestFailureContextAgainstSchema(t *testing.T) {
	generatedJSON := []string{
		`{}`,
		`{"test1": "value1"}`,
		`{"test1": "value1", "test2": "value2"}`,
		`{"test1": "value1", "test2": "value2", "test3": "value3"}`,
	}
	failureType := "randomFailureType"
	randomIndex := 0
	fc := FailureContext{
		FailureType:      failureType,
		Errors:           []string{generatedJSON[randomIndex]},
		Schema:           testSchemaKey,
		Data:             utils.StrPtr(generatedJSON[randomIndex]),
		Timestamp:        time.Now().UTC(),
		ComponentName:    testProcessor.Artifact,
		ComponentVersion: testProcessor.Version,
	}

	sdj := iglucore.NewSelfDescribingDataFromParams(failureSchemaKey, fc.AsJSON())

	igluClient, err := testutils.NewIgluClient()
	require.NoError(t, err)

	clientErr := igluClient.Check(sdj)
	require.NotNil(t, clientErr)
}

func TestParsingNotJSONFromSchemaViolation(t *testing.T) {
	sv := NewSchemaViolation(
		fd.NewSchemaViolationNotJSON("testField", utils.StrPtr("testValue"), "testError"),
		"testSource",
		"testData",
		testTimestamp,
	)

	result := fromSchemaViolation(sv, testTimestamp, testProcessor)
	expected := &FailureContext{
		FailureType: "NotJSON",
		Errors: []string{
			utils.JSON(
				map[string]string{
					"message": "testError",
					"source":  "testSource",
				},
			),
		},
		Schema:           nil,
		Data:             utils.StrPtr(utils.JSON(map[string]string{"testSource": "testData"})),
		Timestamp:        testTimestamp,
		ComponentName:    testProcessor.Artifact,
		ComponentVersion: testProcessor.Version,
	}

	assert.Equal(t, expected, result)
}

func TestParsingSimpleFromEnrichmentFailure(t *testing.T) {
	ef := NewEnrichmentFailure(
		fd.NewEnrichmentFailure(
			fd.NewEnrichmentInformation(testSchemaKey, "enrichmentId"),
			fd.NewEnrichmentFailureSimple("testError"),
		),
		testTimestamp,
	)

	result := fromEnrichmentFailure(ef, testTimestamp, testProcessor)
	expected := &FailureContext{
		FailureType: "EnrichmentError: enrichmentId",
		Errors: []string{
			utils.JSON(
				map[string]string{
					"message": "testError",
				},
			),
		},
		Schema:           testSchemaKey,
		Data:             nil,
		Timestamp:        testTimestamp,
		ComponentName:    testProcessor.Artifact,
		ComponentVersion: testProcessor.Version,
	}

	assert.Equal(t, expected, result)
}

func TestParsingInputDataFromEnrichmentFailure(t *testing.T) {
	ef := NewEnrichmentFailure(
		fd.NewEnrichmentFailure(
			fd.NewEnrichmentInformation(testSchemaKey, "enrichmentId"),
			fd.NewEnrichmentFailureInputData(
				"testField",
				utils.StrPtr("testValue"),
				"testExpectation",
			),
		),
		testTimestamp,
	)

	result := fromEnrichmentFailure(ef, testTimestamp, testProcessor)
	expected := &FailureContext{
		FailureType: "EnrichmentError: enrichmentId",
		Errors: []string{
			utils.JSON(map[string]string{
				"message": "testField - testExpectation",
				"source":  "testField",
			}),
		},
		Schema:           testSchemaKey,
		Data:             utils.StrPtr(utils.JSON(map[string]string{"testField": "testValue"})),
		Timestamp:        testTimestamp,
		ComponentName:    testProcessor.Artifact,
		ComponentVersion: testProcessor.Version,
	}

	assert.Equal(t, expected, result)
}
