package eventenrichments

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

/** A Redshift-compatible timestamp format */
const tsFormat string = "2006-01-02 15:04:05.000"

func GenerateEventID() string {
	return uuid.New().String()
}

func ToTimestamp(ts time.Time) string {
	return ts.UTC().Format(tsFormat)
}

func FromTimestamp(ts string) (time.Time, error) {
	t, err := time.Parse(tsFormat, ts)
	if err != nil {
		return time.Time{}, fmt.Errorf("parsing %s as time: %w", ts, err)
	}
	return t, nil
}

func FormatCollectorTimestamp(tstamp time.Time) (string, error) {
	formattedTimestamp := ToTimestamp(tstamp)
	if strings.HasPrefix(formattedTimestamp, "-") || tstamp.Year() > 9999 || tstamp.Year() < 0 {
		return "", fmt.Errorf("timestamp is not Redshift-compatible")
	}
	return formattedTimestamp, nil
}

var errInvalidEventType = errors.New("not a valid event type")

func ExtractEventType(t string) (string, error) {
	switch t {
	case "se":
		return constants.EventTypeStruct, nil
	case "ev":
		return constants.EventTypeStruct, nil
	case "ue":
		return constants.EventTypeUnstruct, nil
	case "ad":
		return constants.EventTypeAdImpression, nil
	case "tr":
		return constants.EventTypeTransaction, nil
	case "ti":
		return constants.EventTypeTransactionItem, nil
	case "pv":
		return constants.EventTypePageView, nil
	case "pp":
		return constants.EventTypePagePing, nil
	default:
		return "", errInvalidEventType
	}
}

func ExtractTimestamp(value string) (string, error) {
	i, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		return "", fmt.Errorf("parsing %s as int: %w", value, err)
	}
	t := time.UnixMilli(i)
	tString := ToTimestamp(t)
	if strings.HasPrefix(tString, "-") || t.Year() > 9999 || t.Year() < 0 {
		return "", fmt.Errorf("timestamp is not Redshift-compatible")
	}
	return tString, nil
}

func ExtractDerivedTimestamp(
	dvceSentTimestamp *string,
	dvceCreatedTimestamp *string,
	collectorTimestamp *string,
	trueTimestamp *string,
) (*string, *fd.EnrichmentFailure) {
	if trueTimestamp != nil {
		return trueTimestamp, nil
	}

	if dvceSentTimestamp == nil || dvceCreatedTimestamp == nil || collectorTimestamp == nil {
		return collectorTimestamp, nil
	}

	startTimestamp, err := FromTimestamp(*dvceCreatedTimestamp)
	if err != nil {
		return nil, fd.NewEnrichmentFailure(
			fd.NewEnrichmentInformation(nil, "dvce_created_timestamp"),
			fd.NewEnrichmentFailureSimple(fmt.Sprintf("Exception calculating derived timestamp: %s", err.Error())),
		)
	}

	endTimestamp, err := FromTimestamp(*dvceSentTimestamp)
	if err != nil {
		return nil, fd.NewEnrichmentFailure(
			fd.NewEnrichmentInformation(nil, "dvce_sent_timestamp"),
			fd.NewEnrichmentFailureSimple(fmt.Sprintf("Exception calculating derived timestamp: %s", err.Error())),
		)
	}

	if startTimestamp.Before(endTimestamp) {
		temp, err := FromTimestamp(*collectorTimestamp)
		if err != nil {
			return nil, fd.NewEnrichmentFailure(
				fd.NewEnrichmentInformation(nil, "collector_timestamp"),
				fd.NewEnrichmentFailureSimple(fmt.Sprintf("Exception calculating derived timestamp: %s", err.Error())),
			)
		}
		duration := endTimestamp.Sub(startTimestamp)
		derivedTimestamp := ToTimestamp(temp.Add(-1 * duration))
		return utils.StrPtr(derivedTimestamp), nil
	}

	return collectorTimestamp, nil
}
