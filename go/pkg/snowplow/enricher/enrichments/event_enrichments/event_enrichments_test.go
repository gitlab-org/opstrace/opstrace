package eventenrichments

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func TestExtractEventType(t *testing.T) {
	for _, test := range []struct {
		specName string
		input    string
		expected string
	}{
		{specName: "transaction", input: "tr", expected: constants.EventTypeTransaction},
		{specName: "transaction item", input: "ti", expected: constants.EventTypeTransactionItem},
		{specName: "page view", input: "pv", expected: constants.EventTypePageView},
		{specName: "page ping", input: "pp", expected: constants.EventTypePagePing},
		{specName: "unstructured event", input: "ue", expected: constants.EventTypeUnstruct},
		{specName: "structured event", input: "se", expected: constants.EventTypeStruct},
		{specName: "structured event (legacy)", input: "ev", expected: constants.EventTypeStruct},
		{specName: "ad impression (legacy)", input: "ad", expected: constants.EventTypeAdImpression},
	} {
		t.Run(test.specName, func(t *testing.T) {
			result, err := ExtractEventType(test.input)
			require.NoError(t, err)
			require.Equal(t, test.expected, result)
		})
	}
}

func TestExtractEventTypeFailures(t *testing.T) {
	for _, test := range []struct {
		specName    string
		input       string
		expectedErr error
	}{
		{specName: "null", input: "", expectedErr: errInvalidEventType},
		{specName: "unrecognized", input: "e", expectedErr: errInvalidEventType},
	} {
		t.Run(test.specName, func(t *testing.T) {
			_, err := ExtractEventType(test.input)
			if assert.Error(t, err) {
				assert.Equal(t, err, test.expectedErr)
			}
		})
	}
}

func TestExtractDerivedTimestamp(t *testing.T) {
	for _, test := range []struct {
		specName                 string
		dvceCreatedTimestamp     *string
		dvceSentTimestamp        *string
		collectorTimestamp       *string
		trueTimestamp            *string
		expectedDerivedTimestamp *string
	}{
		{
			specName:                 "no device sent timestamp",
			dvceCreatedTimestamp:     utils.StrPtr("2014-04-29 12:00:54.555"),
			dvceSentTimestamp:        nil,
			collectorTimestamp:       utils.StrPtr("2014-04-29 09:00:54.000"),
			trueTimestamp:            nil,
			expectedDerivedTimestamp: utils.StrPtr("2014-04-29 09:00:54.000"),
		},
		{
			specName:                 "no device created timestamp",
			dvceCreatedTimestamp:     nil,
			dvceSentTimestamp:        nil,
			collectorTimestamp:       utils.StrPtr("2014-04-29 09:00:54.000"),
			trueTimestamp:            nil,
			expectedDerivedTimestamp: utils.StrPtr("2014-04-29 09:00:54.000"),
		},
		{
			specName:                 "no collector timestamp",
			dvceCreatedTimestamp:     nil,
			dvceSentTimestamp:        nil,
			collectorTimestamp:       nil,
			trueTimestamp:            nil,
			expectedDerivedTimestamp: nil,
		},
		{
			specName:                 "dvce_sent_tstamp before dvce_created_tstamp",
			dvceCreatedTimestamp:     utils.StrPtr("2014-04-29 09:00:54.001"),
			dvceSentTimestamp:        utils.StrPtr("2014-04-29 09:00:54.000"),
			collectorTimestamp:       utils.StrPtr("2014-04-29 09:00:54.000"),
			trueTimestamp:            nil,
			expectedDerivedTimestamp: utils.StrPtr("2014-04-29 09:00:54.000"),
		},
		{
			specName:                 "dvce_sent_tstamp after dvce_created_tstamp",
			dvceCreatedTimestamp:     utils.StrPtr("2014-04-29 09:00:54.000"),
			dvceSentTimestamp:        utils.StrPtr("2014-04-29 09:00:54.001"),
			collectorTimestamp:       utils.StrPtr("2014-04-29 09:00:54.000"),
			trueTimestamp:            nil,
			expectedDerivedTimestamp: utils.StrPtr("2014-04-29 09:00:53.999"),
		},
		{
			specName:                 "true_tstamp override",
			dvceCreatedTimestamp:     utils.StrPtr("2014-04-29 09:00:54.001"),
			dvceSentTimestamp:        utils.StrPtr("2014-04-29 09:00:54.000"),
			collectorTimestamp:       utils.StrPtr("2014-04-29 09:00:54.000"),
			trueTimestamp:            utils.StrPtr("2000-01-01 00:00:00.000"),
			expectedDerivedTimestamp: utils.StrPtr("2000-01-01 00:00:00.000"),
		},
	} {
		t.Run(test.specName, func(t *testing.T) {
			result, err := ExtractDerivedTimestamp(
				test.dvceSentTimestamp,
				test.dvceCreatedTimestamp,
				test.collectorTimestamp,
				test.trueTimestamp,
			)
			require.Nil(t, err)
			require.Equal(t, test.expectedDerivedTimestamp, result)
		})
	}
}
