package schemaenrichments

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

var (
	signupFormSubmitted       = `{"schema":"iglu:com.snowplowanalytics.snowplow-website/signup_form_submitted/jsonschema/1-0-0","data":{"name":"Χαριτίνη NEW Unicode test","email":"alex+test@snowplowanalytics.com","company":"SP","eventsPerMonth":"< 1 million","serviceType":"unsure"}}`
	signupFormSubmittedSDJ, _ = jsonStringToSDJ(signupFormSubmitted)
)

func TestExtractSchema(t *testing.T) {
	for _, test := range []struct {
		specName       string
		event          *outputs.EnrichedEvent
		ue             *core.SelfDescribingData
		expectedSchema *core.SchemaKey
	}{
		{
			specName:       "page view",
			event:          buildEvent(constants.EventTypePageView),
			ue:             nil,
			expectedSchema: core.NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "page_view", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		},
		{
			specName:       "page ping",
			event:          buildEvent(constants.EventTypePagePing),
			ue:             nil,
			expectedSchema: core.NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "page_ping", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		},
		{
			specName:       "transaction",
			event:          buildEvent(constants.EventTypeTransaction),
			ue:             nil,
			expectedSchema: core.NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "transaction", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		},
		{
			specName:       "transaction item",
			event:          buildEvent(constants.EventTypeTransactionItem),
			ue:             nil,
			expectedSchema: core.NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "transaction_item", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		},
		{
			specName:       "struct event",
			event:          buildEvent(constants.EventTypeStruct),
			ue:             nil,
			expectedSchema: core.NewSchemaKeyFromParams("com.google.analytics", "event", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		},
		{
			specName: "unstruct event",
			event:    buildUnstructEvent(signupFormSubmitted),
			ue:       signupFormSubmittedSDJ,
			expectedSchema: core.NewSchemaKeyFromParams(
				"com.snowplowanalytics.snowplow-website",
				"signup_form_submitted",
				"jsonschema",
				core.NewSchemaVerFull(1, 0, 0),
			),
		},
	} {
		t.Run(test.specName, func(t *testing.T) {
			result, failure := ExtractSchema(test.event, test.ue)
			require.Nil(t, failure)
			require.Equal(t, test.expectedSchema, result)
		})
	}
}

func buildEvent(eventType string) *outputs.EnrichedEvent {
	event := outputs.NewEnrichedEvent()
	event.Event = eventType
	return event
}

func buildUnstructEvent(ue string) *outputs.EnrichedEvent {
	event := outputs.NewEnrichedEvent()
	event.Event = constants.EventTypeUnstruct
	event.UnstructEvent = ue
	return event
}

func jsonStringToSDJ(data string) (*core.SelfDescribingData, error) {
	if !json.Valid([]byte(data)) {
		return nil, fmt.Errorf("invalid json data")
	}

	sd := core.NewSelfDescribingData()
	if err := sd.Parse([]byte(data)); err != nil {
		return nil, fmt.Errorf("parsing json data as self-describing json: %s", err.Code())
	}

	return sd, nil
}
