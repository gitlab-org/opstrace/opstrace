package schemaenrichments

import (
	"fmt"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

const (
	Vendor string = "com.snowplowanalytics.snowplow"
	Format string = "jsonschema"
)

var (
	SchemaVersion         = core.NewSchemaVerFull(1, 0, 0)
	PageViewSchema        = core.NewSchemaKeyFromParams(Vendor, "page_view", Format, SchemaVersion)
	PagePingSchema        = core.NewSchemaKeyFromParams(Vendor, "page_ping", Format, SchemaVersion)
	TransactionSchema     = core.NewSchemaKeyFromParams(Vendor, "transaction", Format, SchemaVersion)
	TransactionItemSchema = core.NewSchemaKeyFromParams(Vendor, "transaction_item", Format, SchemaVersion)
	StructSchema          = core.NewSchemaKeyFromParams("com.google.analytics", "event", Format, SchemaVersion)
)

func ExtractSchema(event *outputs.EnrichedEvent, ue *core.SelfDescribingData) (*core.SchemaKey, *fd.EnrichmentFailure) {
	switch event.Event {
	case constants.EventTypePageView:
		return PageViewSchema, nil
	case constants.EventTypePagePing:
		return PagePingSchema, nil
	case constants.EventTypeStruct:
		return StructSchema, nil
	case constants.EventTypeTransaction:
		return TransactionSchema, nil
	case constants.EventTypeTransactionItem:
		return TransactionItemSchema, nil
	case constants.EventTypeUnstruct:
		if ue != nil {
			return ue.SchemaKey, nil
		}
		return nil, nil
	default:
		f := fd.NewEnrichmentFailureInputData(
			"event",
			utils.StrPtr(event.Event),
			//nolint:lll
			fmt.Sprintf("trying to extract the schema of the enriched event but event type %s doesn't match any of page_view, page_ping, struct, transaction, transaction_item and unstruct", event.Event),
		)
		return nil, fd.NewEnrichmentFailure(nil, f)
	}
}
