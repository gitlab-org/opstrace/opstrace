package miscenrichments

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/require"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

func toJSON(payload map[string]interface{}) string {
	bts, _ := json.Marshal(payload)
	return string(bts)
}

func TestFormatContexts(t *testing.T) {
	contexts := make([]*core.SelfDescribingData, 0)
	contexts = append(contexts, core.NewSelfDescribingDataFromParams(
		core.NewSchemaKeyFromParams("com.acme", "user", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		toJSON(map[string]interface{}{"type": "tester", "name": "bethany"})),
	)
	contexts = append(contexts, core.NewSelfDescribingDataFromParams(
		core.NewSchemaKeyFromParams("com.acme", "design", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		toJSON(map[string]interface{}{"color": "red", "fontSize": 14})),
	)

	expected := `{
  "schema":"iglu:com.snowplowanalytics.snowplow/contexts/jsonschema/1-0-1",
  "data": [
	{
	  "schema":"iglu:com.acme/user/jsonschema/1-0-0",
	  "data": {
	    "type":"tester",
		"name":"bethany"
	  }
	},
	{
	  "schema":"iglu:com.acme/design/jsonschema/1-0-0",
	  "data":{
	    "color":"red",
		"fontSize":14
	  }
	}
  ]
}`

	result := FormatContexts(contexts)
	require.JSONEq(t, expected, result)
}

func TestFormatUnstructuredEvents(t *testing.T) {
	ue := core.NewSelfDescribingDataFromParams(
		core.NewSchemaKeyFromParams("com.acme", "design", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
		toJSON(map[string]interface{}{"color": "red", "fontSize": 14}),
	)

	expected := `{
  "schema":"iglu:com.snowplowanalytics.snowplow/unstruct_event/jsonschema/1-0-0",
  "data": {
	"schema":"iglu:com.acme/design/jsonschema/1-0-0",
	"data":{
	  "color":"red",
	  "fontSize":14
	}
  }
}`
	result := FormatUnstructEvent(ue)
	require.JSONEq(t, expected, result)
}
