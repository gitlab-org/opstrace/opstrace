package miscenrichments

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

var (
	ContextsSchema = core.NewSchemaKeyFromParams(
		"com.snowplowanalytics.snowplow", "contexts", "jsonschema", core.NewSchemaVerFull(1, 0, 1),
	)
	UnstructEventSchema = core.NewSchemaKeyFromParams(
		"com.snowplowanalytics.snowplow", "unstruct_event", "jsonschema", core.NewSchemaVerFull(1, 0, 0),
	)
)

// The X-Forwarded-For header can contain a comma-separated list of IPs especially if it has
// gone through multiple load balancers.
// Here we retrieve the first one as it is supposed to be the client one, c.f.
// https://en.m.wikipedia.org/wiki/X-Forwarded-For#Format
func ExtractIP(ipAddrs string) string {
	ips := strings.Split(ipAddrs, ",")
	return utils.FixTabsNewlines(ips[0])
}

func ToTSVSafe(s string) string {
	return utils.FixTabsNewlines(s)
}

func ExtractPlatform(s string) (string, error) {
	switch s {
	case "web":
		return "web", nil // Web, including Mobile Web
	case "iot":
		return "iot", nil // Internet of Things (e.g. Arduino tracker)
	case "app":
		return "app", nil // General App
	case "mob":
		return "mob", nil // Mobile / Tablet
	case "pc":
		return "pc", nil // Desktop / Laptop / Netbook
	case "cnsl":
		return "cnsl", nil // Games Console
	case "tv":
		return "tv", nil // Connected TV
	case "srv":
		return "srv", nil // Server-side App
	case "headset":
		return "headset", nil // AR/VR Headset
	default:
		return "", fmt.Errorf("not a valid platform")
	}
}

func ETLVersion(processor *badrows.Processor) string {
	return fmt.Sprintf("%s-%s", processor.Artifact, processor.Version)
}

func FormatContexts(contexts []*core.SelfDescribingData) string {
	if len(contexts) == 0 {
		return ""
	}

	allPayloads := make([]string, 0)
	for _, context := range contexts {
		temp, _ := context.AsString() //nolint:errcheck
		allPayloads = append(allPayloads, temp)
	}

	sdj := core.NewSelfDescribingDataFromParamsArr(ContextsSchema, allPayloads)
	sdjStr, _ := sdj.AsString() //nolint:errcheck
	return sdjStr
}

func FormatUnstructEvent(unstructEvent *core.SelfDescribingData) string {
	if unstructEvent == nil {
		return ""
	}
	payload, _ := unstructEvent.AsString() //nolint:errcheck
	sdj := core.NewSelfDescribingDataFromParams(UnstructEventSchema, payload)
	sdjStr, _ := sdj.AsString() //nolint:errcheck
	return sdjStr
}
