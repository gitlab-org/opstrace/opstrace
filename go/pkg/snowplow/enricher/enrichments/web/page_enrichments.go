package web

import (
	"errors"
	"fmt"
	"net/url"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
)

// extract Page URI from event referer or provided tracker variable
func ExtractPageURI(fromReferer *string, fromTracker *string) (*url.URL, *fd.EnrichmentFailure) {
	var (
		result *url.URL
		err    error
	)

	switch {
	case fromReferer != nil && fromTracker == nil:
		result, err = parse(*fromReferer)
	case fromReferer == nil && fromTracker != nil:
		result, err = parse(*fromTracker)
	case fromReferer != nil && fromTracker != nil:
		result, err = parse(*fromTracker) // tracker URL takes precedence
	default:
		return nil, nil // no page URI available, not a failure
	}

	if err != nil {
		return nil, fd.NewEnrichmentFailure(nil, fd.NewEnrichmentFailureSimple(err.Error()))
	}
	return result, nil
}

func parse(input string) (*url.URL, error) {
	if input == "" {
		return nil, errors.New("empty string cannot be converted to URI")
	}
	uri, err := url.Parse(input)
	if err != nil {
		return nil, fmt.Errorf("invalid URI: %w", err)
	}
	return uri, nil
}
