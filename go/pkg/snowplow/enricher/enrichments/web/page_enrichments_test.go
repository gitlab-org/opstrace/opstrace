package web

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func TestExtractPageURIWhenNoneProvided(t *testing.T) {
	result, failure := ExtractPageURI(nil, nil)
	assert.Nil(t, failure)
	assert.Nil(t, result)
}

func TestExtractPageURIPreferTracker(t *testing.T) {
	original := "http://www.mysite.com/shop/session/_internal/checkout"
	originalURI, _ := url.Parse(original)

	custom := "http://www.mysite.com/shop/checkout"
	customURI, _ := url.Parse(custom)

	for _, test := range []struct {
		referer  *string
		tracker  *string
		expected *url.URL
	}{
		{referer: utils.StrPtr(original), tracker: utils.StrPtr(original), expected: originalURI}, // both URIs match, majority case
		{referer: utils.StrPtr(original), tracker: nil, expected: originalURI},                    // tracker didn't send URI
		{referer: nil, tracker: utils.StrPtr(original), expected: originalURI},                    // collector didn't record referer
		{referer: utils.StrPtr(original), tracker: utils.StrPtr(custom), expected: customURI},     // collector & tracker URIs differ
	} {
		result, failure := ExtractPageURI(test.referer, test.tracker)
		assert.Nil(t, failure)
		assert.Equal(t, test.expected, result)
	}
}

func TestExtractPageURIWithTruncatedURL(t *testing.T) {
	original := "http://www.mysite.com/shop/session/_internal/checkout"
	truncated := original[:len(original)-5]
	truncatedURI, _ := url.Parse(truncated)

	result, failure := ExtractPageURI(utils.StrPtr(original), utils.StrPtr(truncated))
	assert.Nil(t, failure)
	assert.Equal(t, truncatedURI, result)
}
