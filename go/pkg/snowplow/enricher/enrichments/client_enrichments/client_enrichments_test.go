package clientenrichments

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func TestExtractViewDimensions(t *testing.T) {
	for _, test := range []struct {
		specName        string
		inputResolution string
		expectedWidth   int
		expectedHeight  int
		expectedError   error
	}{
		{
			specName:        "valid desktop",
			inputResolution: "1200x800",
			expectedWidth:   1200,
			expectedHeight:  800,
		},
		{
			specName:        "valid mobile",
			inputResolution: "76x128",
			expectedWidth:   76,
			expectedHeight:  128,
		},
		{
			specName:        "invalid empty",
			inputResolution: "",
			expectedError:   errInvalidFormat,
		},
		{
			specName:        "invalid hex",
			inputResolution: "76xEE",
			expectedError:   errInvalidFormat,
		},
		{
			specName:        "invalid negative",
			inputResolution: "1200x-17",
			expectedError:   errInvalidFormat,
		},
		{
			specName:        "Arabic",
			inputResolution: "٤٥٦٧x680",
			expectedError:   errInvalidFormat,
		},
		{
			specName:        "conversion to int # 1",
			inputResolution: "760x3389336768",
			expectedError:   errInvalidValue,
		},
		{
			specName:        "conversion to int # 2",
			inputResolution: "9989336768x1200",
			expectedError:   errInvalidValue,
		},
	} {
		t.Run(test.specName, func(t *testing.T) {
			resultWidth, resultHeight, err := ExtractViewDimensions(test.inputResolution)
			if test.expectedError != nil {
				require.Error(t, err)
				require.EqualError(t, test.expectedError, err.Error())
			} else {
				require.NoError(t, err)
				require.Equal(t, utils.IntPtr(test.expectedWidth), resultWidth)
				require.Equal(t, utils.IntPtr(test.expectedHeight), resultHeight)
			}
		})
	}
}
