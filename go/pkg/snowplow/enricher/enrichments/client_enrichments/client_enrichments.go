package clientenrichments

import (
	"errors"
	"regexp"
	"strconv"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

var (
	errInvalidFormat = errors.New("resolution does not conform to acceptable format")
	errInvalidValue  = errors.New("passed value cannot be converted to integer")
)

const resolutionStrRegex string = "(\\d+)x(\\d+)"

func ExtractViewDimensions(resolution string) (*int, *int, error) {
	r := regexp.MustCompile(resolutionStrRegex)
	if r.Match([]byte(resolution)) {
		comps := r.FindStringSubmatch(resolution)
		if len(comps) != 3 {
			return nil, nil, errInvalidFormat
		}
		width, err := strconv.ParseInt(comps[1], 10, 32)
		if err != nil {
			return nil, nil, errInvalidValue
		}
		height, err := strconv.ParseInt(comps[2], 10, 32)
		if err != nil {
			return nil, nil, errInvalidValue
		}
		return utils.IntPtr(int(width)), utils.IntPtr(int(height)), nil
	}
	return nil, nil, errInvalidFormat
}
