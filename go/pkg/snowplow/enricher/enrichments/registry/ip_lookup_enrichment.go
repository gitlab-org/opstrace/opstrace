package registry

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/url"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/oschwald/geoip2-golang"
	iplookups "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/schemas/com.snowplowanalytics.snowplow/ip-lookups"
	"go.uber.org/zap"
)

const localDBPath string = "/tmp/GeoLite2-City.mmdb" // temporary local storage

type IPLookupEnrichment struct {
	logger *zap.Logger
	db     *geoip2.Reader
	client *s3.Client
}

type IPLookupConfig struct {
	iplookups.A200
}

func NewIPLookupEnrichment(logger *zap.Logger, configJSON []byte) (*IPLookupEnrichment, error) {
	var ipConfig IPLookupConfig
	if err := json.Unmarshal(configJSON, &ipConfig); err != nil {
		return nil, fmt.Errorf("unmarshaling config: %w", err)
	}

	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		return nil, fmt.Errorf("loading AWS config: %w", err)
	}

	enrichment := &IPLookupEnrichment{
		logger: logger,
		client: s3.NewFromConfig(cfg),
	}

	bucket, key, err := parseS3URI(ipConfig.Parameters.Geo.Uri)
	if err != nil {
		return nil, fmt.Errorf("parsing S3 URI: %w", err)
	}

	if err := enrichment.downloadFromS3(bucket, key); err != nil {
		return nil, fmt.Errorf("downloading from S3: %w", err)
	}

	db, err := geoip2.Open(localDBPath)
	if err != nil {
		return nil, fmt.Errorf("opening configured db: %w", err)
	}
	enrichment.db = db

	return enrichment, nil
}

type IPLookupResult struct {
	Country        string
	Region         string
	RegionName     string
	City           string
	ZipCode        string
	Latitude       float64
	Longitude      float64
	TimeZone       string
	IPISP          string
	IPOrganization string
	IPDomain       string
	IPNetspeed     string
}

func (e *IPLookupEnrichment) ExtractIPInformation(ipStr string) (*IPLookupResult, error) {
	ip := net.ParseIP(ipStr)
	if ip == nil {
		return nil, fmt.Errorf("invalid IP address: %s", ipStr)
	}
	// perform lookup(s)
	record, err := e.db.City(ip)
	if err != nil {
		e.logger.Warn("performing city lookup", zap.Error(err))
		return nil, fmt.Errorf("performing city lookup: %w", err)
	}

	result := &IPLookupResult{
		Country:   record.Country.Names["en"],
		City:      record.City.Names["en"],
		ZipCode:   record.Postal.Code,
		Latitude:  record.Location.Latitude,
		Longitude: record.Location.Longitude,
		TimeZone:  record.Location.TimeZone,
	}
	if len(record.Subdivisions) > 0 {
		result.Region = record.Subdivisions[0].IsoCode
		result.RegionName = record.Subdivisions[0].Names["en"]
	}
	return result, nil
}

func parseS3URI(s3URI string) (bucket string, key string, err error) {
	if !strings.HasPrefix(s3URI, "s3://") {
		return "", "", fmt.Errorf("invalid S3 URI: %s", s3URI)
	}

	parsedURL, err := url.Parse(s3URI)
	if err != nil {
		return "", "", fmt.Errorf("instantiating url.URL from S3 URI: %w", err)
	}

	bucket = parsedURL.Host
	key = strings.TrimPrefix(parsedURL.Path, "/") // remove leading slash

	return bucket, key, nil
}

func (e *IPLookupEnrichment) downloadFromS3(bucket, key string) error {
	resp, err := e.client.GetObject(context.TODO(), &s3.GetObjectInput{
		Bucket: &bucket,
		Key:    &key,
	})
	if err != nil {
		return fmt.Errorf("downloading file from S3: %w", err)
	}
	defer resp.Body.Close()

	file, err := os.Create(localDBPath)
	if err != nil {
		return fmt.Errorf("creating local file: %w", err)
	}
	defer file.Close()

	_, err = file.ReadFrom(resp.Body)
	if err != nil {
		return fmt.Errorf("writing local file: %w", err)
	}

	e.logger.Sugar().Infof("S3 file downloaded from %s:%s", bucket, key)
	return nil
}
