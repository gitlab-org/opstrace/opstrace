package registry

import (
	"github.com/ua-parser/uap-go/uaparser"
)

type ClientAttributes struct {
	BrowserName         string
	BrowserFamily       string
	BrowserVersion      string
	BrowserType         string
	BrowserRenderEngine string
	OSName              string
	OSFamily            string
	OSManufacturer      string
	DeviceType          string
	DeviceIsMobile      bool
}

type UserAgentUtilsEnrichment struct{}

func NewUserAgentUtilsEnrichment() *UserAgentUtilsEnrichment {
	return &UserAgentUtilsEnrichment{}
}

func (u *UserAgentUtilsEnrichment) ExtractClientAttributes(ua string) *ClientAttributes {
	parser := uaparser.NewFromSaved()

	parsed := parser.Parse(ua)

	return &ClientAttributes{
		BrowserName:    parsed.UserAgent.Family + " " + parsed.UserAgent.Major,
		BrowserFamily:  parsed.UserAgent.Family,
		BrowserVersion: parsed.UserAgent.ToVersionString(),
		OSName:         parsed.Os.ToString(),
		OSFamily:       parsed.Os.Family,
		OSManufacturer: parsed.Os.Family,
		DeviceType:     parsed.Device.ToString(),
		DeviceIsMobile: false,
	}
}
