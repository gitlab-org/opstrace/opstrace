package registry

import (
	"fmt"
	"os"
	"testing"

	"github.com/oschwald/geoip2-golang"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

func TestParseS3URI(t *testing.T) {
	for _, test := range []struct {
		specName       string
		s3URI          string
		expectedBucket string
		expectedKey    string
		shouldFail     bool
	}{
		{"simple", "s3://my-bucket/myfile.mmdb", "my-bucket", "myfile.mmdb", false},
		{"multipath", "s3://another-bucket/path/to/file.mmdb", "another-bucket", "path/to/file.mmdb", false},
		{"invalid http", "http://example.com/file.mmdb", "", "", true},
	} {
		t.Run(test.specName, func(t *testing.T) {
			bucket, key, err := parseS3URI(test.s3URI)
			if test.shouldFail {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, test.expectedBucket, bucket)
				require.Equal(t, test.expectedKey, key)
			}
		})
	}
}

func TestExtractIPInformation(t *testing.T) {
	// ensure test database exists before running this test
	testDBPath := "testdata/GeoLite2-City.mmdb"
	if _, err := os.Stat(testDBPath); os.IsNotExist(err) {
		t.Skip("skipping test: database not found")
	}

	db, err := geoip2.Open(testDBPath)
	require.NoError(t, err)

	enrichment := &IPLookupEnrichment{db: db, logger: zap.NewNop()}
	result, err := enrichment.ExtractIPInformation("8.8.8.8")
	require.NoError(t, err)

	fmt.Printf("result: %+v", result)
}
