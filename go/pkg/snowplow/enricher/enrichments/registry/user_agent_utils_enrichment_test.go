package registry

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func TestExtractClientAttributes(t *testing.T) {
	for _, test := range []struct {
		specName               string
		inputUA                string
		browserName            string
		browserFamily          string
		browserVersion         *string
		browserType            string
		browserRenderingEngine string
		osFields               []string
		deviceType             string
		deviceIsMobile         bool
	}{
		{
			specName:               "Safari spec",
			inputUA:                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36",
			browserName:            "Chrome 33",
			browserFamily:          "Chrome",
			browserVersion:         utils.StrPtr("33.0.1750"),
			browserType:            "",
			browserRenderingEngine: "",
			osFields:               []string{"Mac OS X 10.9.1", "Mac OS X", "Mac OS X"},
			deviceType:             "Mac",
			deviceIsMobile:         false,
		},
		{
			specName:               "IE spec",
			inputUA:                "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0",
			browserName:            "IE 11",
			browserFamily:          "IE",
			browserVersion:         utils.StrPtr("11.0"),
			browserType:            "",
			browserRenderingEngine: "",
			osFields:               []string{"Windows 7", "Windows", "Windows"},
			deviceType:             "Other",
			deviceIsMobile:         false,
		},
	} {
		t.Run(test.specName, func(t *testing.T) {
			enrichment := NewUserAgentUtilsEnrichment()
			result := enrichment.ExtractClientAttributes(test.inputUA)

			expected := &ClientAttributes{
				BrowserName:         test.browserName,
				BrowserFamily:       test.browserFamily,
				BrowserVersion:      *test.browserVersion,
				BrowserType:         test.browserType,
				BrowserRenderEngine: test.browserRenderingEngine,
				OSName:              test.osFields[0],
				OSFamily:            test.osFields[1],
				OSManufacturer:      test.osFields[2],
				DeviceType:          test.deviceType,
				DeviceIsMobile:      test.deviceIsMobile,
			}
			assert.Equal(t, expected, result)
		})
	}
}
