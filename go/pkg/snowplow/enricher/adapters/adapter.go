package adapters

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/snowplow"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/loaders"
	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	"go.uber.org/zap"
)

const (
	VendorSnowplow = "com.snowplowanalytics.snowplow"
	VendorIglu     = "com.snowplowanalytics.iglu"
)

func GetRawEvents(
	logger *zap.Logger,
	payload *loaders.CollectorPayload,
	client *client.Client,
) ([]*types.RawEvent, error) {
	adapter, err := getAdapter(logger, payload)
	if err != nil {
		return nil, fmt.Errorf("getting adapter: %w", err)
	}
	events, failures := adapter.GetRawEvents(payload, client)
	if failures != nil {
		for _, f := range failures {
			fmt.Printf("failure: %v", f)
		}
		return nil, nil // return a bad row instead
	}
	return events, nil
}

func getAdapter(logger *zap.Logger, payload *loaders.CollectorPayload) (types.Adapter, error) {
	vendor := payload.API.Vendor
	version := payload.API.Version
	logger.Debug("parsed from payload",
		zap.String("vendor", vendor),
		zap.String("version", version),
	)

	//nolint:gocritic,staticcheck
	if vendor == VendorSnowplow && version == "tp1" {
		// return snowplow.NewTP1Adapter(logger), nil
	} else if vendor == VendorSnowplow && version == "tp2" {
		return snowplow.NewTP2Adapter(logger), nil
	} else if vendor == VendorIglu && version == "v1" {
		// return iglu.NewIgluAdapter(logger), nil
	}

	return nil, fmt.Errorf("adapter for vendor: %s, version: %s is not supported", vendor, version)
}
