package snowplow

import (
	"encoding/json"
	"fmt"
	"slices"
	"strings"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/loaders"
	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
	"go.uber.org/zap"
)

var supportedContentTypes = []string{
	"application/json",
	"application/json; charset=utf-8",
	"application/json; charset=UTF-8",
}

var payloadDataSchema = core.NewSchemaCriterionFromParams(
	"com.snowplowanalytics.snowplow", "payload_data", "jsonschema", utils.IntPtr(1), utils.IntPtr(0), nil,
)

type TP2Adapter struct {
	logger *zap.Logger
}

func NewTP2Adapter(l *zap.Logger) *TP2Adapter {
	return &TP2Adapter{
		logger: l,
	}
}

var _ types.Adapter = (*TP2Adapter)(nil) // ascertain interface

func (tp2 *TP2Adapter) GetRawEvents(
	payload *loaders.CollectorPayload,
	client *client.Client,
) ([]*types.RawEvent, []fd.TrackerProtocolViolation) {
	qsParams := types.ToRawEventParameter(payload.QueryString)

	validatedParamsNel := func() ([]types.RawEventParameters, []fd.TrackerProtocolViolation) {
		failures := make([]fd.TrackerProtocolViolation, 0)

		// it's ok if only the query string was populated
		if payload.Body == nil && payload.ContentType == nil && qsParams != nil {
			return []types.RawEventParameters{qsParams}, nil
		}

		if payload.Body == nil {
			failures = append(failures,
				fd.NewTrackerProtocolViolationInputData(
					"body", nil, "empty body: not a valid tracker protocol event",
				),
			)
			if len(qsParams) == 0 {
				failures = append(failures,
					fd.NewTrackerProtocolViolationInputData(
						"querystring", nil, "empty querystring: not a valid tracker protocol event",
					),
				)
			}
		} else {
			if payload.ContentType == nil {
				failures = append(failures,
					fd.NewTrackerProtocolViolationInputData(
						"contentType",
						nil,
						fmt.Sprintf("expected one of %s", strings.Join(supportedContentTypes, ", ")),
					),
				)
			} else if !slices.Contains(supportedContentTypes, *payload.ContentType) {
				failures = append(failures,
					fd.NewTrackerProtocolViolationInputData(
						"contentType",
						payload.ContentType,
						fmt.Sprintf("expected one of %s", strings.Join(supportedContentTypes, ", ")),
					),
				)
			}
		}

		if len(failures) > 0 {
			return nil, failures
		}

		paramsJSON, failures := extractAndValidateJSON(payloadDataSchema, *payload.Body, client)
		if failures != nil {
			return nil, failures
		}
		tp2.logger.Debug("post extraction & validation", zap.String("params json", paramsJSON))

		params, failures := toParametersNel(paramsJSON, qsParams)
		if failures != nil {
			return nil, failures
		}
		tp2.logger.Debug("constructing parameters NEL", zap.Any("params", params))

		return params, nil
	}

	params, failures := validatedParamsNel()
	if failures != nil {
		return nil, failures
	}

	events := make([]*types.RawEvent, 0)
	for _, p := range params {
		events = append(events, types.NewRawEvent(
			payload.API,
			p,
			payload.ContentType,
			payload.Source,
			payload.Context,
		))
	}

	return events, nil
}

func toParametersNel(
	instance string,
	mergeWith types.RawEventParameters,
) ([]types.RawEventParameters, []fd.TrackerProtocolViolation) {
	var dataArr []interface{}
	if err := json.Unmarshal([]byte(instance), &dataArr); err != nil {
		return nil, []fd.TrackerProtocolViolation{
			fd.NewTrackerProtocolViolationInputData("body", utils.StrPtr(instance), "JSON is not an array"),
		}
	}

	// we have an array of parameters
	reps := make([]types.RawEventParameters, 0)
	for _, p := range dataArr {
		temp := make(map[string]string)
		if pMap, ok := p.(map[string]interface{}); ok {
			for k, v := range pMap {
				temp[k] = fmt.Sprintf("%s", v)
			}
		}
		// merge
		for k, v := range mergeWith {
			temp[k] = *v
		}
		reps = append(reps, types.ToRawEventParameterFromAnotherMap(temp))
	}

	return reps, nil
}

func extractAndValidateJSON(
	schemaCriterion *core.SchemaCriterion,
	instance string,
	client *client.Client,
) (string, []fd.TrackerProtocolViolation) {
	if !json.Valid([]byte(instance)) {
		return "", []fd.TrackerProtocolViolation{
			fd.NewTrackerProtocolViolationNotJSON("body", utils.StrPtr(instance), "invalid json"),
		}
	}

	sd := core.NewSelfDescribingData()
	if err := sd.Parse([]byte(instance)); err != nil {
		return "", []fd.TrackerProtocolViolation{
			fd.NewTrackerProtocolViolationNotIglu(instance, err),
		}
	}

	if !schemaCriterion.Matches(sd.SchemaKey) {
		return "", []fd.TrackerProtocolViolation{
			fd.NewTrackerProtocolViolationCriterionMismatch(sd.SchemaKey, schemaCriterion),
		}
	}

	if err := client.Check(sd); err != nil {
		return "", []fd.TrackerProtocolViolation{
			fd.NewTrackerProtocolViolationIgluError(sd.SchemaKey, err),
		}
	}

	return sd.Payload(), nil
}
