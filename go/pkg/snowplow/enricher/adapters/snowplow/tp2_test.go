package snowplow

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/loaders"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/testutils"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func buildAPI(version string) *loaders.CollectorPayloadAPI {
	return loaders.NewCollectorPayloadAPIFromParams("com.snowplowanalytics.snowplow", version)
}

var TP1 *loaders.CollectorPayloadAPI = buildAPI("tp1")
var TP2 *loaders.CollectorPayloadAPI = buildAPI("tp2")
var ApplicationJSON = "application/json"
var ApplicationJSONWithCharset = "application/json; charset=utf-8"
var ApplicationJSONWithCapitalCharset = "application/json; charset=UTF-8"

var TSource = loaders.Source{
	Name:     "go-tests",
	Encoding: "UTF-8",
	Hostname: nil,
}

var TContext = loaders.Context{
	Timestamp:  time.Now().UTC(),
	IPAddress:  utils.StrPtr("37.157.33.123"),
	UserAgent:  nil,
	RefererURI: nil,
	Headers:    nil,
	UserID:     nil,
}

func TestRawEventFromQueryString(t *testing.T) {
	client, err := testutils.NewIgluClient()
	require.NoError(t, err)

	adapter := NewTP2Adapter(zap.NewNop())
	nvps := payload.ToNameValuePairs(map[string]*string{
		"aid": utils.StrPtr("tp2"),
		"e":   utils.StrPtr("se"),
	})
	payload := loaders.NewCollectorPayloadFromParams(TP2, nvps, nil, nil, TSource, TContext)
	rawEvents, failures := adapter.GetRawEvents(payload, client)
	require.Nil(t, failures)
	assert.Len(t, rawEvents, 1)

	rep := types.ToRawEventParameter(nvps)
	expected := types.NewRawEvent(TP2, rep, nil, TSource, TContext)
	assert.Equal(t, expected, rawEvents[0])
}

func TestRawEventsFromBody(t *testing.T) {
	data := []map[string]interface{}{
		{"tv": "ios-0.1.0", "p": "mob", "e": "se"},
	}
	dataBytes, _ := json.Marshal(data)

	body, _ := json.Marshal(core.SelfDescribingDataRaw{
		Schema: "iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-0",
		Data:   dataBytes,
	})

	cp := loaders.NewCollectorPayloadFromParams(
		TP2, nil, utils.StrPtr(ApplicationJSONWithCharset), utils.StrPtr(string(body)), TSource, TContext,
	)

	client, err := testutils.NewIgluClient()
	require.NoError(t, err)

	adapter := NewTP2Adapter(zap.NewNop())
	rawEvents, failures := adapter.GetRawEvents(cp, client)
	require.Nil(t, failures)
	assert.Len(t, rawEvents, 1)

	rep0 := types.ToRawEventParameterFromAnotherMap(map[string]string{"tv": "ios-0.1.0", "p": "mob", "e": "se"})
	expected0 := types.NewRawEvent(TP2, rep0, utils.StrPtr(ApplicationJSONWithCharset), TSource, TContext)
	assert.Equal(t, expected0, rawEvents[0])
}

func TestBuildingMultipleRawEvents(t *testing.T) {
	data := []map[string]interface{}{
		{"tv": "1", "p": "1", "e": "1"},
		{"tv": "1", "p": "2", "e": "2"},
		{"tv": "1", "p": "3", "e": "3"},
	}
	dataBytes, _ := json.Marshal(data)

	body, _ := json.Marshal(core.SelfDescribingDataRaw{
		Schema: "iglu:com.snowplowanalytics.snowplow/payload_data/jsonschema/1-0-0",
		Data:   dataBytes,
	})

	nvps := payload.ToNameValuePairs(map[string]*string{
		"tv":   utils.StrPtr("0"),
		"nuid": utils.StrPtr("123"),
	})
	cp := loaders.NewCollectorPayloadFromParams(
		TP2, nvps, utils.StrPtr(ApplicationJSONWithCapitalCharset), utils.StrPtr(string(body)), TSource, TContext,
	)

	client, err := testutils.NewIgluClient()
	require.NoError(t, err)

	adapter := NewTP2Adapter(zap.NewNop())
	rawEvents, failures := adapter.GetRawEvents(cp, client)
	require.Nil(t, failures)
	assert.Len(t, rawEvents, 3)

	rep0 := types.ToRawEventParameterFromAnotherMap(map[string]string{"tv": "0", "p": "1", "e": "1", "nuid": "123"})
	expected0 := types.NewRawEvent(TP2, rep0, utils.StrPtr(ApplicationJSONWithCapitalCharset), TSource, TContext)
	assert.Equal(t, expected0, rawEvents[0])

	rep1 := types.ToRawEventParameterFromAnotherMap(map[string]string{"tv": "0", "p": "2", "e": "2", "nuid": "123"})
	expected1 := types.NewRawEvent(TP2, rep1, utils.StrPtr(ApplicationJSONWithCapitalCharset), TSource, TContext)
	assert.Equal(t, expected1, rawEvents[1])

	rep2 := types.ToRawEventParameterFromAnotherMap(map[string]string{"tv": "0", "p": "3", "e": "3", "nuid": "123"})
	expected2 := types.NewRawEvent(TP2, rep2, utils.StrPtr(ApplicationJSONWithCapitalCharset), TSource, TContext)
	assert.Equal(t, expected2, rawEvents[2])
}

func TestBuildRawEventsWithMismatchingRequestEntities(t *testing.T) {
	for _, test := range []struct {
		specName        string
		queryString     []payload.NVP
		contentType     *string
		body            *string
		expectedFailure []fd.TrackerProtocolViolation
	}{
		{
			specName:    "Invalid content type",
			queryString: nil,
			contentType: utils.StrPtr("text/plain"),
			body:        utils.StrPtr("body"),
			expectedFailure: []fd.TrackerProtocolViolation{
				fd.NewTrackerProtocolViolationInputData(
					"contentType",
					utils.StrPtr("text/plain"),
					"expected one of application/json, application/json; charset=utf-8, application/json; charset=UTF-8",
				),
			},
		},
		{
			specName:    "Neither query string nor body populated",
			queryString: nil,
			contentType: nil,
			body:        nil,
			expectedFailure: []fd.TrackerProtocolViolation{
				fd.NewTrackerProtocolViolationInputData(
					"body",
					nil,
					"empty body: not a valid tracker protocol event",
				),
				fd.NewTrackerProtocolViolationInputData(
					"querystring",
					nil,
					"empty querystring: not a valid tracker protocol event",
				),
			},
		},
		{
			specName:    "Body populated but content-type missing",
			queryString: nil,
			contentType: nil,
			body:        utils.StrPtr("body"),
			expectedFailure: []fd.TrackerProtocolViolation{
				fd.NewTrackerProtocolViolationInputData(
					"contentType",
					nil,
					"expected one of application/json, application/json; charset=utf-8, application/json; charset=UTF-8",
				),
			},
		},
		{
			specName: "Content-type populated but body type missing",
			queryString: payload.ToNameValuePairs(
				map[string]*string{
					"a": utils.StrPtr("b"),
				},
			),
			contentType: utils.StrPtr(ApplicationJSONWithCharset),
			body:        nil,
			expectedFailure: []fd.TrackerProtocolViolation{
				fd.NewTrackerProtocolViolationInputData(
					"body",
					nil,
					"empty body: not a valid tracker protocol event",
				),
			},
		},
		{
			specName: "Body is not JSON",
			queryString: payload.ToNameValuePairs(
				map[string]*string{
					"a": utils.StrPtr("b"),
				},
			),
			contentType: utils.StrPtr(ApplicationJSON),
			body:        utils.StrPtr("body"),
			expectedFailure: []fd.TrackerProtocolViolation{
				fd.NewTrackerProtocolViolationNotJSON(
					"body",
					utils.StrPtr("body"),
					"invalid json",
				),
			},
		},
	} {
		// do something with the test
		t.Run(test.specName, func(t *testing.T) {
			cp := loaders.NewCollectorPayloadFromParams(TP2, test.queryString, test.contentType, test.body, TSource, TContext)

			client, err := testutils.NewIgluClient()
			require.NoError(t, err)

			adapter := NewTP2Adapter(zap.NewNop())
			rawEvents, failures := adapter.GetRawEvents(cp, client)
			require.Nil(t, rawEvents)
			require.NotNil(t, failures)

			assert.Equal(t, failures, test.expectedFailure)
		})
	}
}

func TestBuildRawEventsWithNonSelfDescribingJSONBody(t *testing.T) {
	jsonBody := `{"not":"self-desc"}`

	cp := loaders.NewCollectorPayloadFromParams(
		TP2,
		payload.ToNameValuePairs(
			map[string]*string{
				"a": utils.StrPtr("b"),
			},
		),
		utils.StrPtr(ApplicationJSON),
		utils.StrPtr(jsonBody),
		TSource,
		TContext,
	)

	client, err := testutils.NewIgluClient()
	require.NoError(t, err)

	adapter := NewTP2Adapter(zap.NewNop())
	rawEvents, failures := adapter.GetRawEvents(cp, client)
	require.Nil(t, rawEvents)
	require.NotNil(t, failures)

	assert.Equal(t, failures, []fd.TrackerProtocolViolation{
		fd.NewTrackerProtocolViolationNotIglu(
			jsonBody,
			&core.InvalidData{},
		),
	})
}

func TestBuildRawEventsWithBodyIsNotInPayloadDataJSONSchema(t *testing.T) {
	data := []map[string]interface{}{
		{"longitude": "20.1234"},
	}
	dataBytes, _ := json.Marshal(data)

	body, _ := json.Marshal(core.SelfDescribingDataRaw{
		Schema: "iglu:com.snowplowanalytics.snowplow/geolocation_context/jsonschema/1-0-0",
		Data:   dataBytes,
	})

	cp := loaders.NewCollectorPayloadFromParams(TP2, nil, utils.StrPtr(ApplicationJSON), utils.StrPtr(string(body)), TSource, TContext)

	client, err := testutils.NewIgluClient()
	require.NoError(t, err)

	adapter := NewTP2Adapter(zap.NewNop())
	rawEvents, failures := adapter.GetRawEvents(cp, client)
	require.Nil(t, rawEvents)
	require.NotNil(t, failures)

	assert.Equal(t, failures, []fd.TrackerProtocolViolation{
		fd.NewTrackerProtocolViolationCriterionMismatch(
			core.NewSchemaKeyFromParams(
				"com.snowplowanalytics.snowplow",
				"geolocation_context",
				"jsonschema",
				core.NewSchemaVerFull(1, 0, 0),
			),
			core.NewSchemaCriterionFromParams(
				"com.snowplowanalytics.snowplow",
				"payload_data",
				"jsonschema",
				utils.IntPtr(1),
				utils.IntPtr(0),
				nil,
			),
		),
	})
}
