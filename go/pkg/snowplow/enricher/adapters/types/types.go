package types

import (
	"encoding/json"
	"maps"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/loaders"
	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

type Adapter interface {
	GetRawEvents(
		payload *loaders.CollectorPayload,
		client *client.Client,
	) ([]*RawEvent, []fd.TrackerProtocolViolation)
}

type RawEventParameters map[string]*string

type RawEvent struct {
	API         *loaders.CollectorPayloadAPI
	Parameters  RawEventParameters
	ContentType *string
	Source      loaders.Source
	Context     loaders.Context
}

func NewRawEvent(
	api *loaders.CollectorPayloadAPI,
	rep RawEventParameters,
	contentType *string,
	source loaders.Source,
	context loaders.Context,
) *RawEvent {
	re := &RawEvent{
		API:         api,
		Parameters:  make(map[string]*string),
		ContentType: contentType,
		Source:      source,
		Context:     context,
	}
	maps.Copy(re.Parameters, rep)
	return re
}

func (re *RawEvent) AsString() string {
	bts, _ := json.Marshal(re) //nolint:errcheck
	return string(bts)
}

func ToPayloadRawEvent(re *RawEvent) *payload.RawEvent {
	nvps := make([]payload.NVP, 0)
	for k, v := range re.Parameters {
		nvps = append(nvps, payload.NVP{Name: k, Value: v})
	}

	return payload.NewRawEvent(
		re.API.Vendor,
		re.API.Vendor,
		nvps,
		re.ContentType,
		re.Source.Name,
		re.Source.Encoding,
		re.Source.Hostname,
		re.Context.Timestamp,
		re.Context.IPAddress,
		re.Context.UserAgent,
		re.Context.RefererURI,
		re.Context.Headers,
		re.Context.UserID,
	)
}

func ToRawEventParameter(parameters []payload.NVP) RawEventParameters {
	if parameters == nil {
		return nil
	}
	rep := make(map[string]*string)
	for _, nvp := range parameters {
		rep[nvp.Name] = nvp.Value // key collision?
	}
	return rep
}

func ToRawEventParameterFromAnotherMap(raw map[string]string) RawEventParameters {
	rep := make(map[string]*string)
	for k, v := range raw {
		rep[k] = utils.StrPtr(v)
	}
	return rep
}
