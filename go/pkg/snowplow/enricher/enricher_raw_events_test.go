package enricher

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/loaders"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/testutils"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

var (
	api     = loaders.NewCollectorPayloadAPIFromParams("com.snowplowanalytics.snowplow", "tp2")
	tsource = loaders.Source{
		Name:     "go-tests",
		Encoding: "UTF-8",
		Hostname: nil,
	}
	tcontext = loaders.Context{
		Timestamp:  time.Now().UTC(),
		IPAddress:  utils.StrPtr("0.0.0.0"),
		UserAgent:  nil,
		RefererURI: nil,
		Headers:    nil,
		UserID:     nil,
	}
	tprocessor = badrows.NewProcessor("manager-tests", "0.0.0")
	tregistry  = &Registry{}
)

func TestEnrichEventWithInvalidContext(t *testing.T) {
	params := types.ToRawEventParameterFromAnotherMap(map[string]string{
		"e":  "pp",
		"tv": "js-0.13.1",
		"p":  "web",
		"co": `{
          "schema": "iglu:com.snowplowanalytics.snowplow/contexts/jsonschema/1-0-0",
          "data": [
            {
              "schema":"iglu:com.acme/email_sent/jsonschema/1-0-0",
              "data": {
                "foo": "hello@world.com",
                "emailAddress2": "foo@bar.org"
              }
            }
          ]
        }`,
	})

	client, err := testutils.NewIgluClient()
	require.NoError(t, err)

	rawEvent := types.NewRawEvent(api, params, nil, tsource, tcontext)
	_, failure := NewEnricher(
		zap.NewNop(), client, tprocessor, tregistry,
	).enrichRawEvent(context.TODO(), client, rawEvent, time.Now().UTC(), tprocessor)
	require.NotNil(t, failure)
}

func TestEnrichEventWithInvalidUnstructuredEvent(t *testing.T) {
	params := types.ToRawEventParameterFromAnotherMap(map[string]string{
		"e":  "pp",
		"tv": "js-0.13.1",
		"p":  "web",
		"co": `{
          "schema": "iglu:com.snowplowanalytics.snowplow/unstruct_event/jsonschema/1-0-0",
          "data": {
            "schema":"iglu:com.acme/email_sent/jsonschema/1-0-0",
            "data": {
              "emailAddress": "hello@world.com",
              "emailAddress2": "foo@bar.org",
              "unallowedAdditionalField": "foo@bar.org"
            }
          }
        }`,
	})

	client, err := testutils.NewIgluClient()
	require.NoError(t, err)

	rawEvent := types.NewRawEvent(api, params, nil, tsource, tcontext)
	_, failure := NewEnricher(
		zap.NewNop(), client, tprocessor, tregistry,
	).enrichRawEvent(context.TODO(), client, rawEvent, time.Now().UTC(), tprocessor)
	require.NotNil(t, failure)
}
