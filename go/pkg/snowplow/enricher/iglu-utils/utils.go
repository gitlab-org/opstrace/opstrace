package igluutils

import (
	"encoding/json"
	"time"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
	"go.uber.org/zap"
)

var (
	contextsSchemaCriterion = core.NewSchemaCriterionFromParams(
		"com.snowplowanalytics.snowplow", "contexts", "jsonschema", utils.IntPtr(1), utils.IntPtr(0), nil,
	)
	unstructSchemaCriterion = core.NewSchemaCriterionFromParams(
		"com.snowplowanalytics.snowplow", "unstruct_event", "jsonschema", utils.IntPtr(1), utils.IntPtr(0), nil,
	)
)

type EventExtractResult struct {
	Contexts               []*core.SelfDescribingData
	UnstructEvent          *core.SelfDescribingData
	ValidationInfoContexts []*core.SelfDescribingData
}

func ExtractAndValidateInputJSONs(
	logger *zap.Logger,
	enriched *outputs.EnrichedEvent,
	client *client.Client,
	timestamp time.Time,
) (*EventExtractResult, []*enrichments.SchemaViolation) {
	contexts, failures := ExtractInputContexts(logger, enriched, "contexts", client, timestamp)
	if len(failures) > 0 {
		logger.Debug("extracting input contexts", zap.Any("failure", failures))
		return nil, failures
	}

	unstructEvent, failures := ExtractUnstructEvent(logger, enriched, "unstruct", client, timestamp)
	if len(failures) > 0 {
		logger.Debug("extracting unstruct event", zap.Any("failure", failures))
		return nil, failures
	}

	return &EventExtractResult{
		Contexts:               contexts,
		UnstructEvent:          unstructEvent,
		ValidationInfoContexts: []*core.SelfDescribingData{}, // gather from errors above
	}, nil
}

func ExtractInputContexts(
	logger *zap.Logger,
	enriched *outputs.EnrichedEvent,
	field string,
	client *client.Client,
	timestamp time.Time,
) ([]*core.SelfDescribingData, []*enrichments.SchemaViolation) {
	if enriched.Contexts == "" {
		return nil, nil
	}

	contextsJSON, failure := ExtractInputData(logger, enriched.Contexts, field, contextsSchemaCriterion, client, timestamp)
	if failure != nil {
		logger.Debug("extracing input data from context", zap.Any("failure", failure))
		return nil, []*enrichments.SchemaViolation{failure}
	}

	var contextsArr []map[string]interface{}
	if err := json.Unmarshal([]byte(contextsJSON), &contextsArr); err != nil {
		logger.Debug("list of contexts is invalid JSON")
		return nil, []*enrichments.SchemaViolation{
			enrichments.NewSchemaViolation(
				fd.NewSchemaViolationNotJSON(field, utils.StrPtr(contextsJSON), "invalid JSON"),
				field,
				contextsJSON,
				timestamp,
			),
		}
	}

	result := make([]*core.SelfDescribingData, 0)
	for _, context := range contextsArr {
		contextJSON, _ := json.Marshal(context) //nolint:errcheck
		// parse and validate SDJ and merge errors if any
		sd := core.NewSelfDescribingData()
		if err := sd.Parse(contextJSON); err != nil {
			logger.Debug("context could not be parsed into SDJ", zap.Any("parse error", err))
			return nil, []*enrichments.SchemaViolation{
				enrichments.NewSchemaViolation(
					fd.NewSchemaViolationNotIglu(string(contextJSON), err),
					field,
					string(contextJSON),
					timestamp,
				),
			}
		}

		if err := client.Check(sd); err != nil {
			logger.Debug("context failed Iglu client validation", zap.Any("client error", err))
			return nil, []*enrichments.SchemaViolation{
				enrichments.NewSchemaViolation(
					fd.NewSchemaViolationIgluError(sd.SchemaKey, err),
					field,
					string(contextJSON),
					timestamp,
				),
			}
		}

		result = append(result, sd)
	}

	return result, nil
}

func ExtractUnstructEvent(
	logger *zap.Logger,
	enriched *outputs.EnrichedEvent,
	field string,
	client *client.Client,
	timestamp time.Time,
) (*core.SelfDescribingData, []*enrichments.SchemaViolation) {
	if enriched.UnstructEvent == "" {
		return nil, nil
	}

	rawJSON := enriched.UnstructEvent
	unstruct, failure := ExtractInputData(logger, rawJSON, field, unstructSchemaCriterion, client, timestamp)
	if failure != nil {
		logger.Debug("extracting input data from ue", zap.Any("failure", failure))
		return nil, []*enrichments.SchemaViolation{failure}
	}

	sd := core.NewSelfDescribingData()
	if err := sd.Parse([]byte(unstruct)); err != nil {
		logger.Debug("unstruct event could not be parsed into SDJ", zap.Any("parse error", err))
		return nil, []*enrichments.SchemaViolation{
			enrichments.NewSchemaViolation(
				fd.NewSchemaViolationNotIglu(rawJSON, err),
				field,
				rawJSON,
				timestamp,
			),
		}
	}

	if err := client.Check(sd); err != nil {
		logger.Debug("unstruct event failed Iglu client validation", zap.Any("client error", err))
		return nil, []*enrichments.SchemaViolation{
			enrichments.NewSchemaViolation(
				fd.NewSchemaViolationIgluError(sd.SchemaKey, err),
				field,
				rawJSON,
				timestamp,
			),
		}
	}

	return sd, nil
}

func ExtractInputData(
	logger *zap.Logger,
	rawJSON string,
	field string,
	expectedCriterion *core.SchemaCriterion,
	client *client.Client,
	timestamp time.Time,
) (string, *enrichments.SchemaViolation) {
	sd := core.NewSelfDescribingData()
	if err := sd.Parse([]byte(rawJSON)); err != nil {
		switch err.(type) {
		case *core.InvalidJSON:
			logger.Debug("input data raw JSON is not valid JSON")
			return "", enrichments.NewSchemaViolation(
				fd.NewSchemaViolationNotJSON(field, utils.StrPtr(rawJSON), "invalid JSON"),
				field,
				rawJSON,
				timestamp,
			)
		case *core.InvalidData:
			logger.Debug("input data raw JSON has invalid data")
			return "", enrichments.NewSchemaViolation(
				fd.NewSchemaViolationNotIglu(rawJSON, err),
				field,
				rawJSON,
				timestamp,
			)
		}
	}

	if !expectedCriterion.Matches(sd.SchemaKey) {
		logger.Debug("input data has a schema criterion mismatch")
		return "", enrichments.NewSchemaViolation(
			fd.NewSchemaViolationCriterionMismatch(sd.SchemaKey, expectedCriterion),
			field,
			rawJSON,
			timestamp,
		)
	}

	if err := client.Check(sd); err != nil {
		logger.Debug("input data failed Iglu client validation")
		return "", enrichments.NewSchemaViolation(
			fd.NewSchemaViolationIgluError(sd.SchemaKey, err),
			field,
			rawJSON,
			timestamp,
		)
	}

	return sd.Payload(), nil
}

func ValidateEnrichmentContexts(
	client *client.Client,
	sdjs []*core.SelfDescribingData,
) (contexts []*core.SelfDescribingData, errors []*enrichments.SchemaViolation) {
	for _, sdj := range sdjs {
		clientError := client.Check(sdj)
		if clientError != nil {
			errors = append(errors, enrichments.NewSchemaViolation(
				fd.NewSchemaViolationIgluError(sdj.SchemaKey, clientError),
				"derived_contexts",
				sdj.Payload(),
				time.Now().UTC(),
			))
		} else {
			contexts = append(contexts, sdj)
		}
	}
	return
}
