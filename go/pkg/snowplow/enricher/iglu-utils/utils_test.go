package igluutils

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	iglucore "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/testutils"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
	"go.uber.org/zap"
)

var (
	timestamp        = time.Now().UTC()
	notJSON   string = "foo"
	notIglu   string = `{"foo":"bar"}`
	noSchema         = `{"schema":"iglu:com.snowplowanalytics.snowplow/foo/jsonschema/1-0-0", "data": "{}"}`
)

var (
	unstructSchemaKey = iglucore.NewSchemaKeyFromParams(
		"com.snowplowanalytics.snowplow",
		"unstruct_event",
		"jsonschema",
		iglucore.NewSchemaVerFull(1, 0, 0),
	)
)

func buildUnstructEvent(t *testing.T, payload string) string {
	t.Helper()
	return fmt.Sprintf(`{"schema": "%s", "data": %s}`, unstructSchemaKey.AsString(), payload)
}

func TestExtractUnstructEventWhenPassedEmpty(t *testing.T) {
	enrichedEvent := outputs.NewEnrichedEvent()

	igluclient, err := testutils.NewIgluClient()
	require.NoError(t, err)

	sdj, failures := ExtractUnstructEvent(zap.NewNop(), enrichedEvent, "unstruct", igluclient, timestamp)
	require.Nil(t, failures)
	require.Nil(t, sdj)
}

func TestExtractUnstructEventWithoutValidJSON(t *testing.T) {
	enrichedEvent := outputs.NewEnrichedEvent()
	enrichedEvent.UnstructEvent = notJSON

	igluclient, err := testutils.NewIgluClient()
	require.NoError(t, err)

	result, failures := ExtractUnstructEvent(zap.NewNop(), enrichedEvent, "unstruct", igluclient, timestamp)
	require.Nil(t, result)
	expected := []*enrichments.SchemaViolation{
		enrichments.NewSchemaViolation(
			fd.NewSchemaViolationNotJSON("unstruct", utils.StrPtr(notJSON), "invalid JSON"),
			"unstruct",
			notJSON,
			timestamp,
		),
	}
	require.NotNil(t, failures)
	require.Equal(t, expected, failures)
}

func TestExtractUnstructEventWithNonSelfDescribingJSON(t *testing.T) {
	enrichedEvent := outputs.NewEnrichedEvent()
	enrichedEvent.UnstructEvent = notIglu

	igluclient, err := testutils.NewIgluClient()
	require.NoError(t, err)

	result, failures := ExtractUnstructEvent(zap.NewNop(), enrichedEvent, "unstruct", igluclient, timestamp)
	require.Nil(t, result)
	expected := []*enrichments.SchemaViolation{
		enrichments.NewSchemaViolation(
			fd.NewSchemaViolationNotIglu(notIglu, &iglucore.InvalidData{}),
			"unstruct",
			notIglu,
			timestamp,
		),
	}
	require.NotNil(t, failures)
	require.Equal(t, expected, failures)
}

func TestExtractUnstructEventWithInvalidSchema(t *testing.T) {
	enrichedEvent := outputs.NewEnrichedEvent()
	enrichedEvent.UnstructEvent = noSchema

	igluclient, err := testutils.NewIgluClient()
	require.NoError(t, err)

	expectedKey, _ := iglucore.NewSchemaKeyFromURI("iglu:com.snowplowanalytics.snowplow/foo/jsonschema/1-0-0")

	result, failures := ExtractUnstructEvent(zap.NewNop(), enrichedEvent, "unstruct", igluclient, timestamp)
	require.Nil(t, result)
	expected := []*enrichments.SchemaViolation{
		enrichments.NewSchemaViolation(
			fd.NewSchemaViolationCriterionMismatch(expectedKey, unstructSchemaCriterion),
			"unstruct",
			noSchema,
			timestamp,
		),
	}
	require.NotNil(t, failures)
	require.Equal(t, expected, failures)
}

func TestExtractUnstructEventWithInvalidPayload(t *testing.T) {
	ue := buildUnstructEvent(t, notJSON)
	enrichedEvent := outputs.NewEnrichedEvent()
	enrichedEvent.UnstructEvent = ue
	igluclient, err := testutils.NewIgluClient()
	require.NoError(t, err)

	result, failures := ExtractUnstructEvent(zap.NewNop(), enrichedEvent, "unstruct", igluclient, timestamp)
	require.Nil(t, result)
	expected := []*enrichments.SchemaViolation{
		enrichments.NewSchemaViolation(
			fd.NewSchemaViolationNotJSON("unstruct", utils.StrPtr(ue), "invalid JSON"),
			"unstruct",
			ue,
			timestamp,
		),
	}
	require.NotNil(t, failures)
	require.Equal(t, expected, failures)
}

func TestExtractUnstructEventWithInvalidSDJPayload(t *testing.T) {
	invalidEmailSentData := `{"emailAddress": "hello@world.com"}`
	ue := buildUnstructEvent(t, invalidEmailSentData)
	enrichedEvent := outputs.NewEnrichedEvent()
	enrichedEvent.UnstructEvent = ue
	igluclient, err := testutils.NewIgluClient()
	require.NoError(t, err)

	result, failures := ExtractUnstructEvent(zap.NewNop(), enrichedEvent, "unstruct", igluclient, timestamp)
	require.Nil(t, result)
	require.NotNil(t, failures)
}
