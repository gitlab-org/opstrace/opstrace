package enricher

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments"
	me "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/misc_enrichments"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/loaders"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"go.uber.org/zap"
)

var debugProcessor = badrows.NewProcessor("debugger", "0.0.1")

type Enricher struct {
	logger    *zap.Logger
	client    *client.Client
	processor *badrows.Processor
	registry  *Registry
}

func NewEnricher(
	logger *zap.Logger,
	client *client.Client,
	processor *badrows.Processor,
	registry *Registry,
) *Enricher {
	return &Enricher{
		logger:    logger,
		client:    client,
		processor: processor,
		registry:  registry,
	}
}

func (e *Enricher) Enrich(ctx context.Context, data []byte) ([]interface{}, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	payload, err := loaders.BuildCollectorPayloadFromThriftPayload(ctx, data)
	if err != nil {
		return nil, fmt.Errorf("building enrichment collector payload: %w", err)
	}

	rawEvents, err := adapters.GetRawEvents(e.logger, payload, e.client)
	if err != nil {
		return nil, fmt.Errorf("building raw events from payload: %w", err)
	}

	result := make([]interface{}, 0)
	for _, rawEvent := range rawEvents {
		enriched, badrow := e.enrichRawEvent(
			ctx,
			e.client,
			rawEvent,
			time.Now().UTC(),
			e.processor,
		)
		if badrow != nil {
			// result here will be a bad row, check if we have incomplete events
			// enabled. if yes, sink the bad row and return success instead
			e.logger.Debug("enriching raw event failed", zap.String("badrow", badrow.AsString()))
			result = append(result, badrow)
		} else {
			result = append(result, enriched)
		}
	}

	return result, nil
}

// EnrichedEvent if everything goes well
// BadRow if something went wrong and incomplete events are not enabled
// EnrichedEvent, BadRow if something went wrong and incomplete events are enabled
func (e *Enricher) enrichRawEvent(
	ctx context.Context,
	client *client.Client,
	raw *types.RawEvent,
	etlTimestamp time.Time,
	processor *badrows.Processor,
) (*outputs.EnrichedEvent, badrows.BadRow) {
	enrichedEvent := outputs.NewEnrichedEvent()
	enrichmentResult := e.enrich(ctx, client, raw, enrichedEvent, etlTimestamp, processor)
	if len(enrichmentResult.failures) > 0 {
		for _, f := range enrichmentResult.failures {
			temp, _ := f.ToSDJ(debugProcessor).AsString() //nolint:errcheck
			e.logger.Debug("enrich", zap.Any("failure", temp))
		}
	}

	setDerivedContexts(enrichedEvent, enrichmentResult, processor)
	if len(enrichmentResult.failures) > 0 {
		b := createBadRow(
			enrichmentResult.failures,
			outputs.ToPartiallyEnrichedEvent(enrichedEvent),
			types.ToPayloadRawEvent(raw),
			etlTimestamp,
			processor,
		)
		return nil, b
	}
	return enrichedEvent, nil
}

type enrichmentResult struct {
	failures []enrichments.Failure
	events   []*core.SelfDescribingData
}

func (e *Enricher) enrich(
	ctx context.Context,
	client *client.Client,
	raw *types.RawEvent,
	enriched *outputs.EnrichedEvent,
	etlTimestamp time.Time,
	processor *badrows.Processor,
) *enrichmentResult {
	extractResult, failures := mapAndValidateInput(e.logger, ctx, raw, enriched, etlTimestamp, processor, client)
	if len(failures) > 0 {
		for _, f := range failures {
			temp, _ := f.ToSDJ(debugProcessor).AsString() //nolint:errcheck
			e.logger.Debug("map and validate input", zap.Any("failure", temp))
		}
		return &enrichmentResult{failures: failures}
	}

	enriched.Contexts = me.FormatContexts(extractResult.Contexts)
	enriched.UnstructEvent = me.FormatUnstructEvent(extractResult.UnstructEvent)

	// run enrichments
	enrichmentContexts, enrichmentErrors := runEnrichments(
		e.registry, raw, enriched, extractResult.Contexts, extractResult.UnstructEvent, etlTimestamp,
	)
	if len(enrichmentErrors) > 0 {
		for _, f := range failures {
			temp, _ := f.ToSDJ(debugProcessor).AsString() //nolint:errcheck
			e.logger.Debug("run enrichments", zap.Any("failure", temp))
		}
		// return &enrichmentResult{failures: enrichmentErrors}
	}

	// validate enriched event
	validContexts, validErrors := validateEnriched(enriched, enrichmentContexts, client)
	if len(validErrors) > 0 {
		for _, f := range validErrors {
			temp, _ := f.ToSDJ(debugProcessor).AsString() //nolint:errcheck
			e.logger.Debug("validate enriched", zap.Any("failure", temp))
		}
		return &enrichmentResult{failures: validErrors}
	}

	validContexts = append(validContexts, extractResult.ValidationInfoContexts...)
	return &enrichmentResult{events: validContexts}
}
