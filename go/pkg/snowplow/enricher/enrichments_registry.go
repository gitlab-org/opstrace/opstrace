package enricher

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/registry"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"go.uber.org/zap"
)

type Registry struct {
	UserAgentUtilsEnrichment *registry.UserAgentUtilsEnrichment
	IPLookupEnrichment       *registry.IPLookupEnrichment
}

func NewEnrichmentsRegistry() *Registry {
	r := &Registry{
		UserAgentUtilsEnrichment: nil,
		IPLookupEnrichment:       nil,
	}
	return r
}

type ParseableConfig struct {
	// Enabled corresponds to the JSON schema field "enabled".
	Enabled bool `json:"enabled"`
	// Name corresponds to the JSON schema field "name".
	Name string `json:"name"`
	// Parameters corresponds to the JSON schema field "parameters".
	Parameters map[string]interface{} `json:"parameters,omitempty"`
	// Vendor corresponds to the JSON schema field "vendor".
	Vendor string `json:"vendor"`
}

func IsParseable(configJSON []byte) (*ParseableConfig, error) {
	var parseable ParseableConfig
	if err := json.Unmarshal(configJSON, &parseable); err != nil {
		return nil, fmt.Errorf("unmarshaling config JSON: %w", err)
	}
	return &parseable, nil
}

func ParseEnrichmentConfigs(
	logger *zap.Logger,
	enrichmentsDir string,
	enrichmentsRegistry *Registry,
) error {
	//nolint:wrapcheck
	return filepath.WalkDir(enrichmentsDir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		// skip directories inside
		if d.IsDir() {
			return nil
		}
		// process enrichment configurations
		content, err := os.ReadFile(path)
		if err != nil {
			logger.Warn("reading enrichment config", zap.Error(err))
			return nil // continue processing others
		}
		// unmarshal as SDJ and parse contents
		var sdj core.SelfDescribingData
		if err := sdj.Parse(content); err != nil {
			logger.Warn("parsing enrichment config", zap.Any("parse error", err))
			return nil // continue processing others
		}
		// check config
		parsed, err := IsParseable([]byte(sdj.Payload()))
		if err != nil {
			logger.Warn("checking enrichment config", zap.Error(err))
			return nil // continue processing others
		}
		// check if the enrichment is disabled
		if !parsed.Enabled {
			logger.Sugar().Infof("enrichment %s configured but disabled, skipping", parsed.Name)
			return nil // continue processing others
		}
		// instantiate enrichments
		switch parsed.Name {
		case "user_agent_utils_config":
			enrichmentsRegistry.UserAgentUtilsEnrichment = registry.NewUserAgentUtilsEnrichment()
		case "ip_lookups":
			t, err := registry.NewIPLookupEnrichment(logger, []byte(sdj.Payload()))
			if err != nil {
				logger.Warn("instantiating ip lookup enrichment", zap.Error(err))
				return err // return error
			}
			enrichmentsRegistry.IPLookupEnrichment = t
		}
		return nil
	})
}
