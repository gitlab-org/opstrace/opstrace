package enricher

import (
	"strconv"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	ee "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/event_enrichments"
	schemaenrichments "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/schema_enrichments"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/web"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

type Accumulation struct {
	event    *outputs.EnrichedEvent
	errors   []*fd.EnrichmentFailure
	contexts []*core.SelfDescribingData
}

func NewAccumulation(enrichedEvent *outputs.EnrichedEvent) *Accumulation {
	return &Accumulation{
		event:    enrichedEvent,
		errors:   make([]*fd.EnrichmentFailure, 0),
		contexts: make([]*core.SelfDescribingData, 0),
	}
}

func (a *Accumulation) Contexts() []*core.SelfDescribingData {
	return a.contexts
}

func (a *Accumulation) Errors() []*fd.EnrichmentFailure {
	return a.errors
}

func (a *Accumulation) accError(failure *fd.EnrichmentFailure) {
	a.errors = append(a.errors, failure)
}

// func (a *Accumulation) accContexts(contexts []*core.SelfDescribingData) {
// 	a.contexts = append(a.contexts, contexts...)
// }

func (a *Accumulation) Run(
	enrichmentRegistry *Registry,
	raw *types.RawEvent,
	_ []*core.SelfDescribingData,
	ue *core.SelfDescribingData,
	legacyOrder bool,
) {
	if legacyOrder {
		a.getCollectorVersionSet()
		a.getPageURI(raw.Context.RefererURI)
		a.getDerivedTimestamp()
		a.getUAUtils(enrichmentRegistry)
		a.extractSchemaFields(ue)
		a.geoLocation(enrichmentRegistry)
	}
}

func (a *Accumulation) getCollectorVersionSet() {
	if a.event.VCollector == "" {
		failure := fd.NewEnrichmentFailure(
			fd.NewEnrichmentInformation(nil, "some_identifier"),
			fd.NewEnrichmentFailureInputData("v_collector", nil, "should be set"))
		a.accError(failure)
	}
}

func (a *Accumulation) getPageURI(refererURI *string) {
	var fromTracker *string
	if a.event.PageURL != "" {
		fromTracker = utils.StrPtr(a.event.PageURL)
	}

	pageURI, failure := web.ExtractPageURI(refererURI, fromTracker)
	if failure != nil {
		a.accError(failure)
		return
	}

	// if none of refererURI or tracker URLs were available, pageURI would be nil
	// with a non-failure too. Should be good to return early
	if pageURI == nil {
		return
	}

	a.event.PageURL = pageURI.String()

	a.event.PageURLScheme = pageURI.Scheme
	a.event.RefrURLHost = pageURI.Host
	{
		port, _ := strconv.Atoi(pageURI.Port()) //nolint:errcheck
		a.event.PageURLPort = utils.IntPtr(port)
	}
	a.event.PageURLPath = pageURI.RawPath
	a.event.PageURLQuery = pageURI.RawQuery
	a.event.PageURLFragment = pageURI.RawFragment
}

func (a *Accumulation) getDerivedTimestamp() {
	derivedTimestamp, failure := ee.ExtractDerivedTimestamp(
		a.event.DvceSentTstamp,
		a.event.DvceCreatedTstamp,
		a.event.CollectorTstamp,
		a.event.TrueTstamp,
	)
	if failure != nil {
		a.accError(failure)
		return
	}
	a.event.DerivedTstamp = derivedTimestamp
}

func (a *Accumulation) getUAUtils(enrichmentRegistry *Registry) {
	if enrichmentRegistry == nil {
		return // enrichments not configured
	}
	enricher := enrichmentRegistry.UserAgentUtilsEnrichment
	if enricher == nil {
		return // user agent utils enrichment not configured
	}
	ca := enricher.ExtractClientAttributes(a.event.UserAgent)
	a.event.BrName = ca.BrowserName
	a.event.BrFamily = ca.BrowserFamily
	a.event.BrVersion = ca.BrowserVersion
	a.event.BrType = ca.BrowserType
	a.event.BrRenderEngine = ca.BrowserRenderEngine
	a.event.OsName = ca.OSName
	a.event.OsFamily = ca.OSFamily
	a.event.OsManufacturer = ca.OSManufacturer
	a.event.DvceType = ca.DeviceType
	a.event.DvceIsMobile = &ca.DeviceIsMobile
}

func (a *Accumulation) extractSchemaFields(ue *core.SelfDescribingData) {
	schemaKey, failure := schemaenrichments.ExtractSchema(a.event, ue)
	if failure != nil {
		a.accError(failure)
		return
	}
	a.event.EventVendor = schemaKey.Vendor
	a.event.EventName = schemaKey.Name
	a.event.EventFormat = schemaKey.Format
	a.event.EventVersion = schemaKey.Version.AsString()
}

func (a *Accumulation) geoLocation(enrichmentRegistry *Registry) {
	if enrichmentRegistry == nil {
		return // enrichments not configured
	}
	enricher := enrichmentRegistry.IPLookupEnrichment
	if enricher == nil {
		return // ip lookup enrichments not configured
	}
	result, err := enricher.ExtractIPInformation(a.event.UserIPAddress)
	if err != nil {
		// accumulate error?
		return
	}
	a.event.GeoCountry = result.Country
	a.event.GeoRegion = result.Region
	a.event.GeoCity = result.City
	a.event.GeoZipcode = result.ZipCode
	a.event.GeoLatitude = &result.Latitude
	a.event.GeoLongitude = &result.Longitude
	a.event.GeoRegionName = result.RegionName
	a.event.GeoTimeZone = result.TimeZone
	a.event.IPISP = result.IPISP
	a.event.IPOrganization = result.IPOrganization
	a.event.IPDomain = result.IPDomain
	a.event.IPNetspeed = result.IPNetspeed
}
