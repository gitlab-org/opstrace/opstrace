package enricher

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	"github.com/nats-io/nats.go/jetstream"
	"go.uber.org/zap"

	natsinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/nats"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

type Controller struct {
	logger           *zap.Logger
	serializerPool   *utils.TSerializerPool
	deserializerPool *utils.TDeserializerPool
	js               jetstream.JetStream
	enricher         *Enricher
}

func NewController(
	addr string,
	enricherConfigFile string,
	igluConfigFile string,
	enrichmentsDir string,
	logger *zap.Logger,
) (*Controller, error) {
	// ensure we have necessary enrichment configuration provided
	if enricherConfigFile == "" {
		return nil, fmt.Errorf("enricher config file not provided")
	}

	enricherConfigJSON, err := os.ReadFile(enricherConfigFile)
	if err != nil {
		return nil, fmt.Errorf("reading config file: %w", err)
	}
	logger.Sugar().Infof("enricher config: %s", string(enricherConfigJSON))

	if igluConfigFile == "" {
		return nil, fmt.Errorf("iglu config file not provided")
	}

	igluConfigJSON, err := os.ReadFile(igluConfigFile)
	if err != nil {
		return nil, fmt.Errorf("reading iglu config file: %w", err)
	}
	logger.Sugar().Infof("iglu config: %s", string(igluConfigJSON))

	// setup NATS/Jetstream
	js, err := natsinternal.NewJetstream(addr)
	if err != nil {
		return nil, fmt.Errorf("setting-up jetstream API: %w", err)
	}
	// setup Iglu client
	igluclient, err := client.NewClient(string(igluConfigJSON))
	if err != nil {
		return nil, fmt.Errorf("instantiating Iglu client: %w", err)
	}
	// setup event processor
	processor := badrows.NewProcessor("etl-processor", "0.0.1")

	c := &Controller{
		logger:           logger.Named("enricher"),
		serializerPool:   utils.NewTSerializerPool(),
		deserializerPool: utils.NewTDeserializerPool(),
		js:               js,
	}

	// check if we have any enrichments enabled/configured
	enrichmentRegistry := NewEnrichmentsRegistry()
	if enrichmentsDir != "" {
		if err := ParseEnrichmentConfigs(logger, enrichmentsDir, enrichmentRegistry); err != nil {
			return nil, fmt.Errorf("parsing enrichment configs from %s: %w", enrichmentsDir, err)
		}
	}
	c.enricher = NewEnricher(logger.Named("enricher"), igluclient, processor, enrichmentRegistry)

	return c, nil
}

func (c *Controller) Start(ctx context.Context) error {
	if err := c.startSource(ctx); err != nil {
		c.logger.Error("starting source", zap.Error(err))
		return err
	}
	c.logger.Info("enrichment controller started")
	return nil
}

func (c *Controller) startSource(ctx context.Context) error {
	streamName := "RAW_EVENTS"
	rawEventsStream, err := c.js.Stream(ctx, streamName)
	if err != nil {
		return fmt.Errorf("raw events stream not found: %w", err)
	}

	consumer, err := rawEventsStream.CreateOrUpdateConsumer(
		ctx, utils.CommonConsumerConfig(streamName, []string{"raw_events.>"}),
	)
	if err != nil {
		return fmt.Errorf("creating/updating raw events stream consumer: %w", err)
	}

	iter, err := consumer.Messages(jetstream.PullMaxMessages(100))
	if err != nil {
		return fmt.Errorf("failed setting up messages iterator: %w", err)
	}
	defer iter.Stop()

FETCHER:
	for {
		select {
		case <-ctx.Done():
			break FETCHER
		default:
			msg, err := iter.Next()
			if err != nil {
				c.logger.Warn("fetching messages", zap.Error(err))
				continue FETCHER
			}

			err = c.handleMessage(ctx, msg)
			if err != nil {
				c.logger.Warn("handling message", zap.Error(err))
				c.nackMessage(msg)
			} else {
				c.ackMessage(msg)
			}
		}
	}

	return nil
}

func (c *Controller) ackMessage(msg jetstream.Msg) {
	if err := msg.Ack(); err != nil {
		c.logger.Warn("acking message", zap.Error(err))
	} else {
		c.logger.Debug("acked message")
	}
}

func (c *Controller) nackMessage(msg jetstream.Msg) {
	if err := msg.Nak(); err != nil {
		c.logger.Warn("nacking message", zap.Error(err))
	} else {
		c.logger.Debug("nacked message")
	}
}

func (c *Controller) handleMessage(ctx context.Context, msg jetstream.Msg) error {
	perMsgCtx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	enrichedEvents, err := c.enricher.Enrich(perMsgCtx, msg.Data())
	if err != nil {
		return fmt.Errorf("enriching message: %w", err)
	}

	if err := c.publishEnrichmentResult(perMsgCtx, enrichedEvents); err != nil {
		return fmt.Errorf("publishing enriched message: %w", err)
	}

	return nil
}

func (c *Controller) publishEnrichmentResult(ctx context.Context, events []interface{}) error {
	var publishErrs []error
	for _, e := range events {
		switch v := e.(type) {
		case *outputs.EnrichedEvent:
			tsv := outputs.BuildTabSeparatedEnrichedEvent(v)
			// c.logger.Sugar().Debugf("enriched event: %s", tsv)
			_, err := c.js.Publish(ctx, "enriched_events.good", []byte(tsv))
			if err != nil {
				c.logger.Error("queueing enriched event", zap.Error(err))
				publishErrs = append(publishErrs, err)
			} else {
				c.logger.Debug("queued enriched event")
			}
		default:
			c.logger.Debug("unhandled post-enrichment result", zap.Any("object", e))
		}
	}
	return errors.Join(publishErrs...)
}
