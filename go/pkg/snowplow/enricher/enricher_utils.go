package enricher

import (
	"context"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows"
	ff "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure"
	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/adapters/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments"
	ee "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/event_enrichments"
	me "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/misc_enrichments"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/enrichments/transform"
	igluutils "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/iglu-utils"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
	"go.uber.org/zap"
)

func setDerivedContexts(
	enrichedEvent *outputs.EnrichedEvent,
	enrichmentResult *enrichmentResult,
	processor *badrows.Processor,
) {
	derivedContexts := make([]*core.SelfDescribingData, 0)
	if len(enrichmentResult.failures) > 0 {
		for _, failure := range enrichmentResult.failures {
			derivedContexts = append(derivedContexts, failure.ToSDJ(processor))
		}
	} else {
		derivedContexts = enrichmentResult.events
	}
	enrichedEvent.DerivedContexts = me.FormatContexts(derivedContexts)
}

func mapAndValidateInput(
	logger *zap.Logger,
	_ context.Context,
	raw *types.RawEvent,
	enriched *outputs.EnrichedEvent,
	etlTimestamp time.Time,
	processor *badrows.Processor,
	client *client.Client,
) (*igluutils.EventExtractResult, []enrichments.Failure) {
	setupEnrichedEvent(raw, enriched, etlTimestamp, processor, logger)
	result, schemaViolations := igluutils.ExtractAndValidateInputJSONs(logger, enriched, client, etlTimestamp)
	// map out structs to interface as necessary
	failures := make([]enrichments.Failure, 0)
	for _, sv := range schemaViolations {
		logger.Debug("extracting and validating input JSON", zap.Any("failure", sv))
		failures = append(failures, sv)
	}

	return result, failures
}

func setupEnrichedEvent(
	raw *types.RawEvent,
	enriched *outputs.EnrichedEvent,
	etlTimestamp time.Time,
	processor *badrows.Processor,
	logger *zap.Logger,
) {
	enriched.EventID = ee.GenerateEventID()
	enriched.VCollector = raw.Source.Name
	enriched.VEtl = me.ETLVersion(processor)
	enriched.EtlTstamp = ee.ToTimestamp(etlTimestamp)

	if raw.Context.UserID != nil {
		enriched.NetworkUserid = *raw.Context.UserID
	}
	if raw.Context.IPAddress != nil {
		enriched.UserIPAddress = me.ExtractIP(*raw.Context.IPAddress)
	}
	if raw.Context.UserAgent != nil {
		enriched.UserAgent = utils.FixTabsNewlines(*raw.Context.UserAgent)
	}
	if !raw.Context.Timestamp.IsZero() {
		t, _ := ee.FormatCollectorTimestamp(raw.Context.Timestamp) //nolint:errcheck
		enriched.CollectorTstamp = utils.StrPtr(t)
	}

	transform.FirstPassTransform(raw, enriched, logger)
}

func runEnrichments(
	enrichmentRegistry *Registry,
	raw *types.RawEvent,
	enriched *outputs.EnrichedEvent,
	inputContexts []*core.SelfDescribingData,
	unstructEvent *core.SelfDescribingData,
	timestamp time.Time,
) ([]*core.SelfDescribingData, []enrichments.Failure) {
	acc := NewAccumulation(enriched)
	acc.Run(enrichmentRegistry, raw, inputContexts, unstructEvent, true)

	errors := make([]enrichments.Failure, 0)
	accFailures := acc.Errors()
	if len(accFailures) > 0 {
		for _, f := range accFailures {
			errors = append(errors, enrichments.NewEnrichmentFailure(f, timestamp))
		}
	}

	return acc.Contexts(), errors
}

func validateEnriched(
	_ *outputs.EnrichedEvent,
	contexts []*core.SelfDescribingData,
	client *client.Client,
) ([]*core.SelfDescribingData, []enrichments.Failure) {
	contexts, failures := igluutils.ValidateEnrichmentContexts(client, contexts)

	errors := make([]enrichments.Failure, 0)
	if len(failures) > 0 {
		for _, f := range failures {
			errors = append(errors, f)
		}
	}
	// TODO: AtomicFieldsLengthValidator
	return contexts, errors
}

func createBadRow(
	failures []enrichments.Failure,
	pe *payload.PartiallyEnrichedEvent,
	re *payload.RawEvent,
	timestamp time.Time,
	processor *badrows.Processor,
) badrows.BadRow {
	firstFailure := failures[0]
	switch v := firstFailure.(type) {
	case *enrichments.SchemaViolation:
		return badrows.NewSchemaViolations(
			processor,
			ff.NewSchemaViolations(timestamp, []fd.SchemaViolationDetail{firstFailure}),
			payload.NewEnrichmentPayload(pe, re),
		)
	case *enrichments.EnrichmentFailure:
		return badrows.NewEnrichmentFailures(
			fd.NewEnrichmentInformation(v.Failure.Information.SchemaKey, v.Failure.Information.Identifier),
			ff.NewEnrichmentFailures(timestamp, []fd.SchemaViolationDetail{firstFailure}),
		)
	default:
		panic("received failure not supported")
	}
}
