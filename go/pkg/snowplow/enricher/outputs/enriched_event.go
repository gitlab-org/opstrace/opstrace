package outputs

import (
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
)

// https://docs.snowplow.io/docs/fundamentals/canonical-event/understanding-the-enriched-tsv-format/?fields=tsv
//
//nolint:lll
const ExpectedOrder string = "app_id	platform	etl_tstamp	collector_tstamp	dvce_created_tstamp	event	event_id	txn_id	name_tracker	v_tracker	v_collector	v_etl	user_id	user_ipaddress	user_fingerprint	domain_userid	domain_sessionidx	network_userid	geo_country	geo_region	geo_city	geo_zipcode	geo_latitude	geo_longitude	geo_region_name	ip_isp	ip_organization	ip_domain	ip_netspeed	page_url	page_title	page_referrer	page_urlscheme	page_urlhost	page_urlport	page_urlpath	page_urlquery	page_urlfragment	refr_urlscheme	refr_urlhost	refr_urlport	refr_urlpath	refr_urlquery	refr_urlfragment	refr_medium	refr_source	refr_term	mkt_medium	mkt_source	mkt_term	mkt_content	mkt_campaign	contexts	se_category	se_action	se_label	se_property	se_value	unstruct_event	tr_orderid	tr_affiliation	tr_total	tr_tax	tr_shipping	tr_city	tr_state	tr_country	ti_orderid	ti_sku	ti_name	ti_category	ti_price	ti_quantity	pp_xoffset_min	pp_xoffset_max	pp_yoffset_min	pp_yoffset_max	useragent	br_name	br_family	br_version	br_type	br_renderengine	br_lang	br_features_pdf	br_features_flash	br_features_java	br_features_director	br_features_quicktime	br_features_realplayer	br_features_windowsmedia	br_features_gears	br_features_silverlight	br_cookies	br_colordepth	br_viewwidth	br_viewheight	os_name	os_family	os_manufacturer	os_timezone	dvce_type	dvce_ismobile	dvce_screenwidth	dvce_screenheight	doc_charset	doc_width	doc_height	tr_currency	tr_total_base	tr_tax_base	tr_shipping_base	ti_currency	ti_price_base	base_currency	geo_timezone	mkt_clickid	mkt_network	etl_tags	dvce_sent_tstamp	refr_domain_userid	refr_device_tstamp	derived_contexts	domain_sessionid	derived_tstamp	event_vendor	event_name	event_format	event_version	event_fingerprint	true_tstamp"

// EnrichedEvent represents an enriched Snowplow analytics event.
// Source: https://docs.snowplow.io/docs/fundamentals/canonical-event/understanding-the-enriched-tsv-format/?fields=table
// Note: Keep the definition of this struct compliant with the above source at all times.
//
//nolint:lll
type EnrichedEvent struct {
	// Application info
	AppID    string `json:"app_id,omitempty"`
	Platform string `json:"platform,omitempty"`

	// Date/time
	EtlTstamp         string  `json:"etl_tstamp,omitempty"`
	CollectorTstamp   *string `json:"collector_tstamp,omitempty"`
	DvceCreatedTstamp *string `json:"dvce_created_tstamp,omitempty"`

	Event   string `json:"event,omitempty"`
	EventID string `json:"event_id,omitempty"`
	TxnID   *int   `json:"txn_id,omitempty"`

	// Versioning
	NameTracker string `json:"name_tracker,omitempty"`
	VTracker    string `json:"v_tracker,omitempty"`
	VCollector  string `json:"v_collector,omitempty"`
	VEtl        string `json:"v_etl,omitempty"`

	// User and visit
	UserID           string `json:"user_id,omitempty"`
	UserIPAddress    string `json:"user_ipaddress,omitempty"`
	UserFingerprint  string `json:"user_fingerprint,omitempty"`
	DomainUserid     string `json:"domain_userid,omitempty"`
	DomainSessionIdx *int   `json:"domain_sessionidx,omitempty"`
	NetworkUserid    string `json:"network_userid,omitempty"`

	// Location
	GeoCountry    string   `json:"geo_country,omitempty"`
	GeoRegion     string   `json:"geo_region,omitempty"`
	GeoCity       string   `json:"geo_city,omitempty"`
	GeoZipcode    string   `json:"geo_zipcode,omitempty"`
	GeoLatitude   *float64 `json:"geo_latitude,omitempty"`
	GeoLongitude  *float64 `json:"geo_longitude,omitempty"`
	GeoRegionName string   `json:"geo_region_name,omitempty"`

	// Other IP lookups
	IPISP          string `json:"ip_isp,omitempty"`
	IPOrganization string `json:"ip_organization,omitempty"`
	IPDomain       string `json:"ip_domain,omitempty"`
	IPNetspeed     string `json:"ip_netspeed,omitempty"`

	// Page
	PageURL      string `json:"page_url,omitempty"`
	PageTitle    string `json:"page_title,omitempty"`
	PageReferrer string `json:"page_referrer,omitempty"`

	// Page URL components
	PageURLScheme   string `json:"page_urlscheme,omitempty"`
	PageURLHost     string `json:"page_urlhost,omitempty"`
	PageURLPort     *int   `json:"page_urlport,omitempty"`
	PageURLPath     string `json:"page_urlpath,omitempty"`
	PageURLQuery    string `json:"page_urlquery,omitempty"`
	PageURLFragment string `json:"page_urlfragment,omitempty"`

	// Referrer URL components
	RefrURLScheme   string `json:"refr_urlscheme,omitempty"`
	RefrURLHost     string `json:"refr_urlhost,omitempty"`
	RefrURLPort     *int   `json:"refr_urlport,omitempty"`
	RefrURLPath     string `json:"refr_urlpath,omitempty"`
	RefrURLQuery    string `json:"refr_urlquery,omitempty"`
	RefrURLFragment string `json:"refr_urlfragment,omitempty"`

	// Referrer details
	RefrMedium string `json:"refr_medium,omitempty"`
	RefrSource string `json:"refr_source,omitempty"`
	RefrTerm   string `json:"refr_term,omitempty"`

	// Marketing
	MktMedium   string `json:"mkt_medium,omitempty"`
	MktSource   string `json:"mkt_source,omitempty"`
	MktTerm     string `json:"mkt_term,omitempty"`
	MktContent  string `json:"mkt_content,omitempty"`
	MktCampaign string `json:"mkt_campaign,omitempty"`

	// Custom contexts
	Contexts string `json:"contexts,omitempty"`

	// Structured Event
	SeCategory string   `json:"se_category,omitempty"`
	SeAction   string   `json:"se_action,omitempty"`
	SeLabel    string   `json:"se_label,omitempty"`
	SeProperty string   `json:"se_property,omitempty"`
	SeValue    *float64 `json:"se_value,omitempty"`

	// Unstructured Event
	UnstructEvent string `json:"unstruct_event,omitempty"`

	// E-commerce
	TrOrderID     string   `json:"tr_orderid,omitempty"`
	TrAffiliation string   `json:"tr_affiliation,omitempty"`
	TrTotal       *float64 `json:"tr_total,omitempty"`
	TrTax         *float64 `json:"tr_tax,omitempty"`
	TrShipping    *float64 `json:"tr_shipping,omitempty"`
	TrCity        string   `json:"tr_city,omitempty"`
	TrState       string   `json:"tr_state,omitempty"`
	TrCountry     string   `json:"tr_country,omitempty"`

	TiOrderID  string   `json:"ti_orderid,omitempty"`
	TiSKU      string   `json:"ti_sku,omitempty"`
	TiName     string   `json:"ti_name,omitempty"`
	TiCategory string   `json:"ti_category,omitempty"`
	TiPrice    *float64 `json:"ti_price,omitempty"`
	TiQuantity *int     `json:"ti_quantity,omitempty"`

	// Page Pings
	PpXOffsetMin *int `json:"pp_xoffset_min,omitempty"`
	PpXOffsetMax *int `json:"pp_xoffset_max,omitempty"`
	PpYOffsetMin *int `json:"pp_yoffset_min,omitempty"`
	PpYOffsetMax *int `json:"pp_yoffset_max,omitempty"`

	// User Agent and Browser Info
	UserAgent      string `json:"useragent,omitempty"`
	BrName         string `json:"br_name,omitempty"`
	BrFamily       string `json:"br_family,omitempty"`
	BrVersion      string `json:"br_version,omitempty"`
	BrType         string `json:"br_type,omitempty"`
	BrRenderEngine string `json:"br_renderengine,omitempty"`

	BrLang                 string `json:"br_lang,omitempty"`
	BrFeaturesPDF          byte   `json:"br_features_pdf,omitempty"`
	BrFeaturesFlash        byte   `json:"br_features_flash,omitempty"`
	BrFeaturesJava         byte   `json:"br_features_java,omitempty"`
	BrFeaturesDirector     byte   `json:"br_features_director,omitempty"`
	BrFeaturesQuicktime    byte   `json:"br_features_quicktime,omitempty"`
	BrFeaturesRealPlayer   byte   `json:"br_features_realplayer,omitempty"`
	BrFeaturesWindowsMedia byte   `json:"br_features_windowsmedia,omitempty"`
	BrFeaturesGears        byte   `json:"br_features_gears,omitempty"`
	BrFeaturesSilverlight  byte   `json:"br_features_silverlight,omitempty"`
	BrCookies              byte   `json:"br_cookies,omitempty"`
	BrColorDepth           string `json:"br_colordepth,omitempty"`
	BrViewWidth            *int   `json:"br_viewwidth,omitempty"`
	BrViewHeight           *int   `json:"br_viewheight,omitempty"`

	// OS
	OsName         string `json:"os_name,omitempty"`
	OsFamily       string `json:"os_family,omitempty"`
	OsManufacturer string `json:"os_manufacturer,omitempty"`
	OsTimezone     string `json:"os_timezone,omitempty"`

	// Device
	DvceType         string `json:"dvce_type,omitempty"`
	DvceIsMobile     *bool  `json:"dvce_ismobile,omitempty"`
	DvceScreenWidth  *int   `json:"dvce_screenwidth,omitempty"`
	DvceScreenHeight *int   `json:"dvce_screenheight,omitempty"`

	// Document
	DocCharset string `json:"doc_charset,omitempty"`
	DocWidth   *int   `json:"doc_width,omitempty"`
	DocHeight  *int   `json:"doc_height,omitempty"`

	// Currency
	TrCurrency     string   `json:"tr_currency,omitempty"`
	TrTotalBase    *float64 `json:"tr_total_base,omitempty"`
	TrTaxBase      *float64 `json:"tr_tax_base,omitempty"`
	TrShippingBase *float64 `json:"tr_shipping_base,omitempty"`
	TiCurrency     string   `json:"ti_currency,omitempty"`
	TiPriceBase    *float64 `json:"ti_price_base,omitempty"`
	BaseCurrency   string   `json:"base_currency,omitempty"`

	GeoTimeZone string `json:"geo_timezone,omitempty"`

	MktClickID string `json:"mkt_clickid,omitempty"`
	MktNetwork string `json:"mkt_network,omitempty"`

	// ETL Tags
	EtlTags string `json:"etl_tags,omitempty"`

	DvceSentTstamp *string `json:"dvce_sent_tstamp,omitempty"`

	RefrDomainUserID *string `json:"refr_domain_userid,omitempty"`
	RefrDeviceTstamp *string `json:"refr_device_tstamp,omitempty"`

	// Derived Contexts
	DerivedContexts string `json:"derived_contexts,omitempty"`

	DomainSessionID *int `json:"domain_sessionid,omitempty"`

	// Derived timestamp
	DerivedTstamp *string `json:"derived_tstamp,omitempty"`

	EventVendor  string `json:"event_vendor,omitempty"`
	EventName    string `json:"event_name,omitempty"`
	EventFormat  string `json:"event_format,omitempty"`
	EventVersion string `json:"event_version,omitempty"`

	// Event fingerprint
	EventFingerprint string `json:"event_fingerprint,omitempty"`

	// True timestamp
	TrueTstamp *string `json:"true_tstamp,omitempty"`
}

func NewEnrichedEvent() *EnrichedEvent {
	return &EnrichedEvent{}
}

func ToPartiallyEnrichedEvent(_ *EnrichedEvent) *payload.PartiallyEnrichedEvent {
	return payload.NewPartiallyEnrichedEvent()
}
