package outputs

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

const debug bool = false

//nolint:cyclop
func BuildTabSeparatedEnrichedEvent(enrichedEvent *EnrichedEvent) string {
	if enrichedEvent == nil {
		return ""
	}
	fields := getEnrichedEventFields(*enrichedEvent)

	v := reflect.ValueOf(*enrichedEvent)
	values := make([]string, 0)
	for _, f := range fields {
		temp := "" // start empty
		if debug {
			// when debugging, add field name to the value
			temp = fmt.Sprintf("%s:", f.Name)
		}

		value := v.FieldByName(f.Name).Interface()
		// handling all possible types within the enriched-event struct
		switch v := value.(type) {
		case byte:
			if v != 0 {
				temp += string(v)
			}
		case string:
			temp += v
		case *string:
			if v != nil {
				temp += *v
			}
		case *int:
			if v != nil {
				temp += fmt.Sprintf("%d", *v)
			}
		case *float64:
			if v != nil {
				temp += fmt.Sprintf("%f", *v)
			}
		case *bool:
			if v != nil {
				temp += strconv.FormatBool(*v)
			}
		default:
			fmt.Printf("fields mishandled: %s\n", f.Name)
		}
		values = append(values, temp)
	}

	return strings.Join(values, "\t")
}

func getEnrichedEventFields(event interface{}) []reflect.StructField {
	t := reflect.TypeOf(event)
	if t.Kind() != reflect.Struct {
		panic("not a struct type")
	}

	fields := make([]reflect.StructField, 0)
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		if f.Name == "pii" {
			continue
		}
		fields = append(fields, f)
	}

	return fields
}
