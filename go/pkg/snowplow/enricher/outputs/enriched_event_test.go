package outputs

import (
	"reflect"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestEnrichedEventFieldsOrdering(t *testing.T) {
	expected := strings.Split(ExpectedOrder, "\t")
	for i, part := range expected {
		expected[i] = strings.TrimSpace(part)
	}

	event := NewEnrichedEvent()
	eeType := reflect.TypeOf(*event)

	actual := make([]string, 0)
	for i := 0; i < eeType.NumField(); i++ {
		field := eeType.Field(i)                      // get field by index
		json := field.Tag.Get("json")                 // get JSON tag value
		json = strings.TrimSuffix(json, ",omitempty") // remove omitempty if present
		actual = append(actual, json)
	}

	// assert fields
	require.EqualValues(t, expected, actual)
	// assert order
	for i := range expected {
		require.Equal(t, expected[i], actual[i])
	}
}
