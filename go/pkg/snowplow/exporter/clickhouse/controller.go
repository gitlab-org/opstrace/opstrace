package clickhouse

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/nats-io/nats.go/jetstream"
	"go.uber.org/zap"

	"github.com/ClickHouse/clickhouse-go/v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	natsinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/nats"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

type Controller struct {
	logger           *zap.Logger
	js               jetstream.JetStream
	conn             clickhouse.Conn
	serializerPool   *utils.TSerializerPool
	deserializerPool *utils.TDeserializerPool
	opts             Options
}

type Options struct {
	FlushBatchSize int
}

func NewController(addr string, clickhouseDSN string, logger *zap.Logger, opts Options) (*Controller, error) {
	c := &Controller{
		logger:           logger.Named("chexporter"),
		opts:             opts,
		serializerPool:   utils.NewTSerializerPool(),
		deserializerPool: utils.NewTDeserializerPool(),
	}
	// setup NATS/Jetstream
	if addr != "" {
		js, err := natsinternal.NewJetstream(addr)
		if err != nil {
			return nil, fmt.Errorf("setting-up jetstream API: %w", err)
		}
		c.js = js
	}
	// setup ClickHouse
	if clickhouseDSN != "" {
		conn, err := NewDB(&Config{
			ClickHouseDSN:             clickhouseDSN,
			ClickHouseMaxOpenConns:    10,
			ClickHouseMaxIdleConns:    5,
			ClickHouseConnMaxLifetime: 30 * time.Second,
		})
		if err != nil {
			return nil, fmt.Errorf("setting-up clickhouse: %w", err)
		}
		c.conn = conn
	}
	return c, nil
}

func (c *Controller) Start(ctx context.Context) error {
	if err := c.startSource(ctx); err != nil {
		c.logger.Error("starting source", zap.Error(err))
		return err
	}
	c.logger.Info("exporter controller started")
	return nil
}

func (c *Controller) startSource(ctx context.Context) error {
	streamName := "ENRICHED_EVENTS"
	enrichedEventsStream, err := c.js.Stream(ctx, streamName)
	if err != nil {
		return fmt.Errorf("stream %s not found: %w", streamName, err)
	}

	consumer, err := enrichedEventsStream.CreateOrUpdateConsumer(
		ctx, utils.CommonConsumerConfig(streamName, []string{"enriched_events.>"}),
	)
	if err != nil {
		return fmt.Errorf("creating/updating enriched events stream consumer: %w", err)
	}

	iter, err := consumer.Messages(jetstream.PullMaxMessages(100))
	if err != nil {
		return fmt.Errorf("failed setting up messages iterator: %w", err)
	}
	defer iter.Stop()

	// used later for batching fetched messages
	messageBatch := make([]jetstream.Msg, 0)

FETCHER:
	for {
		select {
		case <-ctx.Done():
			break FETCHER
		default:
			msg, err := iter.Next()
			if err != nil {
				c.logger.Warn("fetching messages", zap.Error(err))
				continue FETCHER
			}

			messageBatch = append(messageBatch, msg)
			// if batching is requested and length of the current batch is
			// less than the threshold, continue accumulating messages
			if c.opts.FlushBatchSize >= 1 && len(messageBatch) < c.opts.FlushBatchSize {
				continue FETCHER
			}

			// persist batched messages
			err = c.persistMessageBatch(ctx, messageBatch)
			if err != nil {
				c.logger.Error("persisting message batch", zap.Error(err))
				// negative acknowledge the batch since persistence failed
				c.nackMessageBatch(messageBatch)
			} else {
				c.logger.Debug("persisted message batch")
				// acknowledge messages if the batch was persisted
				c.ackMessageBatch(messageBatch)
			}

			messageBatch = nil // clear the slice, allow GC
		}
	}

	return nil
}

const (
	insertEnrichedEventsSQL = `
INSERT INTO ` + constants.SnowplowDatabaseName + `.` + constants.SnowplowEnrichedEventsTableName + ` (
  TenantId,
  ProjectId,
  Message,
  IngestionTimestamp
)`
)

func (c *Controller) persistMessageBatch(ctx context.Context, batch []jetstream.Msg) error {
	payloads := make([]string, 0)
	for _, msg := range batch {
		payloads = append(payloads, string(msg.Data()))
	}
	return c.persistPayloads(ctx, payloads)
}

func (c *Controller) persistPayloads(ctx context.Context, payloads []string) error {
	batch, err := c.conn.PrepareBatch(ctx, insertEnrichedEventsSQL)
	if err != nil {
		return fmt.Errorf("preparing batch: %w", err)
	}

	tenantID := "tid"  // FIXME when auth lands
	projectID := "pid" // FIXME when auth lands

	for _, payload := range payloads {
		if err := batch.Append(
			tenantID,
			projectID,
			payload,
			time.Now().UTC(),
		); err != nil {
			c.logger.Warn("appending message to batch", zap.Error(err))
		}
	}

	if sendErr := batch.Send(); sendErr != nil {
		if abortErr := batch.Abort(); abortErr != nil {
			errors.Join(sendErr, abortErr) //nolint:errcheck
		}
		return fmt.Errorf("sending batch: %w", sendErr)
	}
	return nil
}

func (c *Controller) ackMessageBatch(batch []jetstream.Msg) {
	for msgIdx, msg := range batch {
		if err := msg.Ack(); err != nil {
			c.logger.Warn("acking message", zap.Int("index", msgIdx), zap.Error(err))
		} else {
			c.logger.Debug("acked message successfully", zap.Int("index", msgIdx))
		}
	}
}

func (c *Controller) nackMessageBatch(batch []jetstream.Msg) {
	for msgIdx, msg := range batch {
		if err := msg.Nak(); err != nil {
			c.logger.Warn("nacking message", zap.Int("index", msgIdx), zap.Error(err))
		} else {
			c.logger.Debug("nacked message successfully", zap.Int("index", msgIdx))
		}
	}
}
