package clickhouse

import (
	"fmt"
	"time"

	chgo "github.com/ClickHouse/clickhouse-go/v2"
)

type Config struct {
	ClickHouseDSN             string        `mapstructure:"clickhouse_dsn"`
	ClickHouseMaxOpenConns    int           `mapstructure:"clickhouse_max_open_conns"`
	ClickHouseMaxIdleConns    int           `mapstructure:"clickhouse_max_idle_conns"`
	ClickHouseConnMaxLifetime time.Duration `mapstructure:"clickhouse_conn_max_lifetime"`
}

func NewDB(cfg *Config) (chgo.Conn, error) {
	dbOpts, err := chgo.ParseDSN(cfg.ClickHouseDSN)
	if err != nil {
		return nil, fmt.Errorf("parsing clickhouse Cloud DSN: %w", err)
	}
	dbOpts.MaxOpenConns = cfg.ClickHouseMaxOpenConns
	dbOpts.MaxIdleConns = cfg.ClickHouseMaxIdleConns
	dbOpts.ConnMaxLifetime = cfg.ClickHouseConnMaxLifetime
	dbOpts.Compression = &chgo.Compression{Method: chgo.CompressionLZ4}

	conn, err := chgo.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse cloud db open: %w", err)
	}

	return conn, nil
}
