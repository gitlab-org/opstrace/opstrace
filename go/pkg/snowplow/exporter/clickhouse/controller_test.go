package clickhouse

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"go.uber.org/zap/zaptest"
)

type enrichedEventsRow struct {
	TenantID           string    `ch:"TenantId"`
	ProjectID          string    `ch:"ProjectId"`
	Message            string    `ch:"Message"`
	IngestionTimestamp time.Time `ch:"IngestionTimestamp"`
}

const readEnrichedEventsTmpl string = `
SELECT
  TenantId,
  ProjectId,
  Message,
  IngestionTimestamp
FROM
  %s.%s
`

func TestPushEnrichedEvents(t *testing.T) {
	// ensure the tests run within the stipulated timeout
	ctx, cancel := context.WithTimeout(context.Background(), 180*time.Second)
	defer cancel()

	testEnv, err := testutils.NewClickHouseServer(ctx)
	require.NoError(t, err)
	// ensure cleaning up the container afterwards
	defer testEnv.Terminate(ctx)

	dsn, err := testEnv.GetDSN(ctx, constants.SnowplowDatabaseName)
	require.NoError(t, err)

	err = testEnv.CreateDatabasesAndRunMigrations(ctx, constants.SnowplowDatabaseName)
	require.NoError(t, err)

	t.Run("test enriched events insertion", func(t *testing.T) {
		// setup exporter, configure database
		exporter, err := NewController("", dsn, zaptest.NewLogger(t), Options{})
		require.NoError(t, err)
		// ensure data was inserted correctly
		err = exporter.persistPayloads(ctx, generateEnrichedEventPayloads())
		require.NoError(t, err)
		// test table state/data
		conn, err := testEnv.GetConnection(ctx)
		require.NoError(t, err)
		rows, err := conn.Query(
			ctx,
			fmt.Sprintf(readEnrichedEventsTmpl, constants.SnowplowDatabaseName, constants.SnowplowEnrichedEventsTableName),
		)
		require.NoError(t, err)
		var rowsFound int
		for rows.Next() {
			rowsFound++
			var r enrichedEventsRow
			if err := rows.ScanStruct(&r); err != nil {
				t.Error(err)
			}
			assert.Equal(t, r.TenantID, "tid")
			assert.Equal(t, r.ProjectID, "pid")
			assert.Equal(t, r.Message, "hello world")
		}
		assert.Equal(t, 1, rowsFound)
	})
}

func generateEnrichedEventPayloads() []string {
	return []string{"hello world"}
}
