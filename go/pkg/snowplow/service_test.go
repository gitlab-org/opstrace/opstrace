package snowplow

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/config"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/sinks/inmemory"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/thrift/gen-go/collector_payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

var testAppInfo = &AppInfo{Name: "collector-test", Version: "test-version"}

func setSPAnonymousHeader(r *http.Request) {
	r.Header.Set("Sp-Anonymous", "*")
}

func setNetworkUserIDCookie(r *http.Request, cookieName string, cookieValue string) {
	r.AddCookie(&http.Cookie{
		Name:     cookieName,
		Value:    url.QueryEscape(cookieValue),
		MaxAge:   3600,
		Path:     "/",
		Domain:   "localhost",
		Secure:   false,
		HttpOnly: false,
	})
}

func TestCookieSetupWhenSPAnonymousIsPresent(t *testing.T) {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("GET", "/", nil)
	setSPAnonymousHeader(request)
	ctx.Request = request

	config := config.NewConfig()
	service := NewService(zap.NewNop(), config, nil, nil, testAppInfo)
	service.Ingest(ctx, false)

	assert.Empty(t, ctx.Writer.Header().Values("Set-Cookie"))
}

func TestNetworkUserIDSetupWhenSPAnonymousIsPresent(t *testing.T) {
	gin.SetMode(gin.TestMode)
	config := getTestConfig()

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("GET", "/", nil)
	setSPAnonymousHeader(request)
	setNetworkUserIDCookie(request, config.Cookie.Name, "test-nuid")
	ctx.Request = request

	goodSink := inmemory.New(zap.NewNop())
	service := NewService(zap.NewNop(), config, goodSink, nil, testAppInfo)
	service.Ingest(ctx, false)

	assert.Equal(t, ctx.Writer.Status(), http.StatusOK)

	goodEvents := goodSink.GetEvents()
	assert.Len(t, goodEvents, 1)

	tdeserPool := utils.NewTDeserializerPool()
	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	err := tdeser.Read(ctx, cp, goodEvents[0])
	require.NoError(t, err)

	assert.Equal(t, "00000000-0000-0000-0000-000000000000", *cp.NetworkUserId)
}

func TestNetworkUserIDSetupWhenSPAnonymousIsNotPresent(t *testing.T) {
	gin.SetMode(gin.TestMode)
	config := getTestConfig()

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("GET", "/", nil)
	setNetworkUserIDCookie(request, config.Cookie.Name, "test-nuid")
	ctx.Request = request

	goodSink := inmemory.New(zap.NewNop())
	service := NewService(zap.NewNop(), config, goodSink, nil, testAppInfo)
	service.Ingest(ctx, false)

	assert.Equal(t, ctx.Writer.Status(), http.StatusOK)

	goodEvents := goodSink.GetEvents()
	assert.Len(t, goodEvents, 1)

	tdeserPool := utils.NewTDeserializerPool()
	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	err := tdeser.Read(ctx, cp, goodEvents[0])
	require.NoError(t, err)

	assert.Equal(t, "test-nuid", *cp.NetworkUserId)
}

func TestIPAddressWithXForwardedForHeaderUsed(t *testing.T) {
	gin.SetMode(gin.TestMode)
	config := getTestConfig()

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("POST", "/", nil)
	request.Header.Set("X-Forwarded-For", "192.0.2.4")
	ctx.Request = request

	goodSink := inmemory.New(zap.NewNop())
	badSink := inmemory.New(zap.NewNop())
	service := NewService(zap.NewNop(), config, goodSink, badSink, testAppInfo)
	service.Ingest(ctx, false)

	assert.Equal(t, ctx.Writer.Status(), http.StatusOK)
	assert.Len(t, goodSink.GetEvents(), 1)
	assert.Len(t, badSink.GetEvents(), 0)

	tdeserPool := utils.NewTDeserializerPool()
	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	err := tdeser.Read(ctx, cp, goodSink.GetEvents()[0])
	require.NoError(t, err)

	assert.Equal(t, cp.IpAddress, "192.0.2.4")
}

func TestIPAddressWithXForwardedForHeaderNotUsed(t *testing.T) {
	gin.SetMode(gin.TestMode)
	config := getTestConfig()

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("POST", "/", nil)
	request.RemoteAddr = "192.0.2.2"
	ctx.Request = request

	goodSink := inmemory.New(zap.NewNop())
	badSink := inmemory.New(zap.NewNop())
	service := NewService(zap.NewNop(), config, goodSink, badSink, testAppInfo)
	service.Ingest(ctx, false)

	assert.Equal(t, ctx.Writer.Status(), http.StatusOK)
	assert.Len(t, goodSink.GetEvents(), 1)
	assert.Len(t, badSink.GetEvents(), 0)

	tdeserPool := utils.NewTDeserializerPool()
	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	err := tdeser.Read(ctx, cp, goodSink.GetEvents()[0])
	require.NoError(t, err)

	assert.Equal(t, cp.IpAddress, "192.0.2.2")
}

func TestIPAddressWhenSpAnonymousIsPresent(t *testing.T) {
	gin.SetMode(gin.TestMode)
	config := getTestConfig()

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("POST", "/", nil)
	setSPAnonymousHeader(request)
	ctx.Request = request

	goodSink := inmemory.New(zap.NewNop())
	badSink := inmemory.New(zap.NewNop())
	service := NewService(zap.NewNop(), config, goodSink, badSink, testAppInfo)
	service.Ingest(ctx, false)

	assert.Equal(t, ctx.Writer.Status(), http.StatusOK)
	assert.Len(t, goodSink.GetEvents(), 1)
	assert.Len(t, badSink.GetEvents(), 0)

	tdeserPool := utils.NewTDeserializerPool()
	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	err := tdeser.Read(ctx, cp, goodSink.GetEvents()[0])
	require.NoError(t, err)

	assert.Equal(t, cp.IpAddress, "unknown")
}

func TestIngest(t *testing.T) {
	gin.SetMode(gin.TestMode)
	config := getTestConfig()

	nuid := "dfdb716e-ecf9-4d00-8b10-44edfbc8a108"
	testHeaders := map[string]string{
		"User-Agent":                       "testUserAgent",
		"Content-Type":                     "application/json",
		"X-Forwarded-For":                  "192.0.2.3",
		"Access-Control-Allow-Credentials": "*",
		"Referer":                          "example.com",
	}

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("POST", "http://example.com/p?a=b", strings.NewReader("b"))
	request.RemoteAddr = "192.168.1.1:12345"
	for k, v := range testHeaders {
		request.Header.Set(k, v)
	}
	setNetworkUserIDCookie(request, config.Cookie.Name, nuid)
	ctx.Request = request

	goodSink := inmemory.New(zap.NewNop())
	badSink := inmemory.New(zap.NewNop())
	service := NewService(zap.NewNop(), config, goodSink, badSink, testAppInfo)
	service.Ingest(ctx, false)

	assert.Equal(t, ctx.Writer.Status(), http.StatusOK)
	assert.Len(t, goodSink.GetEvents(), 1)
	assert.Len(t, badSink.GetEvents(), 0)

	tdeserPool := utils.NewTDeserializerPool()
	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	err := tdeser.Read(ctx, cp, goodSink.GetEvents()[0])
	require.NoError(t, err)

	assert.Equal(t, cp.Schema, CollectorThriftSchema)
	assert.Equal(t, cp.IpAddress, "192.0.2.3")
	assert.Equal(t, cp.Encoding, "UTF-8")
	assert.Equal(t, cp.Collector, fmt.Sprintf("%s-%s", testAppInfo.Name, testAppInfo.Version))
	assert.Equal(t, *cp.Querystring, "a=b")
	assert.Equal(t, *cp.Body, "b")
	assert.Equal(t, cp.Path, "p")
	assert.Equal(t, *cp.UserAgent, "testUserAgent")
	assert.Equal(t, *cp.RefererUri, "example.com")
	assert.Equal(t, *cp.Hostname, "example.com")
	assert.Equal(t, *cp.NetworkUserId, nuid)

	expectedHeaders := make([]string, 0)
	expectedHeaders = append(expectedHeaders, fmt.Sprintf("Cookie: sp=%s", nuid)) // add cookie explicitly
	for k, v := range testHeaders {
		expectedHeaders = append(expectedHeaders, fmt.Sprintf("%s: %s", k, v))
	}
	assert.ElementsMatch(t, expectedHeaders, cp.Headers)
}

func TestHeadersWhenSPAnonymousIsPresent(t *testing.T) {
	gin.SetMode(gin.TestMode)
	config := getTestConfig()

	testHeaders := map[string]string{
		"User-Agent":                       "testUserAgent",
		"Content-Type":                     "application/json",
		"X-Forwarded-For":                  "192.0.2.3",
		"Access-Control-Allow-Credentials": "*",
		"Referer":                          "example.com",
	}

	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)

	request, _ := http.NewRequest("POST", "http://example.com/p?a=b", strings.NewReader("b"))
	for k, v := range testHeaders {
		request.Header.Set(k, v)
	}
	setSPAnonymousHeader(request)
	ctx.Request = request

	goodSink := inmemory.New(zap.NewNop())
	badSink := inmemory.New(zap.NewNop())
	service := NewService(zap.NewNop(), config, goodSink, badSink, testAppInfo)
	service.Ingest(ctx, false)

	assert.Equal(t, ctx.Writer.Status(), http.StatusOK)
	assert.Len(t, goodSink.GetEvents(), 1)
	assert.Len(t, badSink.GetEvents(), 0)

	tdeserPool := utils.NewTDeserializerPool()
	tdeser := tdeserPool.Get()
	defer tdeserPool.Put(tdeser)

	cp := collector_payload.NewCollectorPayload()
	err := tdeser.Read(ctx, cp, goodSink.GetEvents()[0])
	require.NoError(t, err)

	expectedHeaders := make([]string, 0)
	expectedHeaders = append(expectedHeaders, "Sp-Anonymous: *")
	for k, v := range testHeaders {
		if k == "X-Forwarded-For" || k == "X-Real-IP" || k == "Cookie" {
			continue // remove filtered headers
		}
		expectedHeaders = append(expectedHeaders, fmt.Sprintf("%s: %s", k, v))
	}
	assert.ElementsMatch(t, expectedHeaders, cp.Headers)
}

func getTestConfig() *config.Config {
	testConfig := &config.Config{
		Interface: "0.0.0.0",
		Port:      8080,
		Cookie: config.CookieConfig{
			Enabled:    true,
			Name:       "sp",
			Expiration: 365 * 24 * time.Hour, // 1 year
		},
		DoNotTrackCookie: config.DoNotTrackCookieConfig{
			Enabled: false,
			Name:    "",
			Value:   "",
		},
		CookieBounce: config.CookieBounceConfig{
			Enabled:                 false,
			Name:                    "n3pc",
			FallbackNetworkUserID:   "00000000-0000-4000-A000-000000000000",
			ForwardedProtocolHeader: "",
		},
		Streams: config.StreamsConfig{
			Good: config.SinkConfig{
				Name: "raw",
			},
			Bad: config.SinkConfig{
				Name: "bad",
			},
			UseIPAsPartitionKey: false,
		},
	}
	return testConfig
}
