package testutils

import (
	"fmt"
	"path"
	"path/filepath"
	"runtime"

	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
)

const testIgluClientConfigJSONTmpl string = `{
  "schema": "iglu:com.snowplowanalytics.iglu/resolver-config/jsonschema/1-0-1",
  "data": {
    "cacheSize": 500,
    "cacheTtl": 600,
    "repositories": [
      {
		"name": "Iglu tests embedded",
		"priority": 0,
		"vendorPrefixes": [ "com.snowplowanalytics" ],
		"connection": {
		  "embedded": {
		    "path": "%s"
		  }
		}
	  }
	]
  }
}`

func NewIgluClient() (*client.Client, error) {
	//nolint:dogsled
	_, b, _, _ := runtime.Caller(0)
	basePath := filepath.Dir(b)

	embeddedSchemasPath := path.Join(basePath, "/embedded/iglu-schemas")
	testIgluClientConfigJSON := fmt.Sprintf(testIgluClientConfigJSONTmpl, embeddedSchemasPath)
	return client.NewClient(testIgluClientConfigJSON)
}
