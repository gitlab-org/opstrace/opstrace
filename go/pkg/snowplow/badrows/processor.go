package badrows

type Processor struct {
	Artifact string
	Version  string
}

func NewProcessor(artifact, version string) *Processor {
	return &Processor{
		Artifact: artifact,
		Version:  version,
	}
}
