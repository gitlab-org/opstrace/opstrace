package badrows

import (
	"encoding/json"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	fp "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/payload"
)

type BadRow interface {
	AsString() string
}

var _ BadRow = (*SchemaViolations)(nil)
var _ BadRow = (*EnrichmentFailures)(nil)

type SchemaViolations struct {
	Processor *Processor
	Failure   fd.SchemaViolationDetail
	Payload   *fp.EnrichmentPayload
}

func NewSchemaViolations(
	processor *Processor,
	failure fd.SchemaViolationDetail,
	payload *fp.EnrichmentPayload,
) *SchemaViolations {
	return &SchemaViolations{
		Processor: processor,
		Failure:   failure,
		Payload:   payload,
	}
}

func (sv *SchemaViolations) AsString() string {
	bts, _ := json.Marshal(sv) //nolint:errcheck
	return string(bts)
}

type EnrichmentFailures struct {
	Enrichment *fd.EnrichmentInformation
	Message    fd.EnrichmentFailureDetail
}

func NewEnrichmentFailures(
	enrichment *fd.EnrichmentInformation,
	failure fd.EnrichmentFailureDetail,
) *EnrichmentFailures {
	return &EnrichmentFailures{
		Enrichment: enrichment,
		Message:    failure,
	}
}

func (e *EnrichmentFailures) AsString() string {
	bts, _ := json.Marshal(e) //nolint:errcheck
	return string(bts)
}
