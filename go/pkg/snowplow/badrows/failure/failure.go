package failure

import (
	"encoding/json"
	"time"

	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
)

type Failure interface {
	AsJSON() string
}

var _ Failure = (*SchemaViolations)(nil)
var _ Failure = (*EnrichmentFailures)(nil)

type SchemaViolations struct {
	Timestamp time.Time                  `json:"timestamp"`
	Messages  []fd.SchemaViolationDetail `json:"messages"`
}

func NewSchemaViolations(ts time.Time, messages []fd.SchemaViolationDetail) Failure {
	return &SchemaViolations{
		Timestamp: ts,
		Messages:  messages,
	}
}

func (s *SchemaViolations) AsJSON() string {
	bts, _ := json.Marshal(s) //nolint:errcheck
	return string(bts)
}

type EnrichmentFailures struct{}

func NewEnrichmentFailures(timestamp time.Time, messages []fd.SchemaViolationDetail) Failure {
	return &EnrichmentFailures{}
}

func (s *EnrichmentFailures) AsJSON() string {
	bts, _ := json.Marshal(s) //nolint:errcheck
	return string(bts)
}
