package failure

import (
	"testing"

	"github.com/stretchr/testify/assert"
	fd "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/badrows/failure_details"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func TestEncodeSchemaViolation(t *testing.T) {
	failureDetail := fd.NewSchemaViolationNotIglu(utils.JSON(map[string]string{}), &core.InvalidData{})
	messages := []fd.SchemaViolationDetail{failureDetail}
	ts := utils.ToEpochMilli(1000)
	f := NewSchemaViolations(ts, messages)
	expectedJSON := `{
  "timestamp" : "1970-01-01T00:00:01Z",
  "messages" : [
	{
	  "json" : {},
	  "error" : "INVALID_DATA_PAYLOAD"
	}
  ]
}`
	assert.JSONEq(t, expectedJSON, f.AsJSON())
}
