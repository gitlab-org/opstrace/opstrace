package payload

import (
	"time"
)

type NVP struct {
	Name  string
	Value *string
}

func ToNameValuePairs(pairs map[string]*string) []NVP {
	nvps := make([]NVP, 0)
	for k, v := range pairs {
		nvps = append(nvps, NVP{
			Name:  k,
			Value: v,
		})
	}
	return nvps
}

type RawEvent struct {
	vendor      string
	version     string
	parameters  []NVP
	contentType *string
	loaderName  string
	encoding    string
	hostname    *string
	timestamp   time.Time
	ipAddress   *string
	userAgent   *string
	refererURI  *string
	headers     []string
	userID      *string
}

func NewRawEvent(
	vendor string,
	version string,
	parameters []NVP,
	contentType *string,
	loaderName string,
	encoding string,
	hostname *string,
	timestamp time.Time,
	ipAddress *string,
	userAgent *string,
	refererURI *string,
	headers []string,
	userID *string,
) *RawEvent {
	return &RawEvent{
		vendor:      vendor,
		version:     version,
		parameters:  parameters,
		contentType: contentType,
		loaderName:  loaderName,
		encoding:    encoding,
		hostname:    hostname,
		timestamp:   timestamp,
		ipAddress:   ipAddress,
		userAgent:   userAgent,
		refererURI:  refererURI,
		headers:     headers,
		userID:      userID,
	}
}

//nolint:unused
type PartiallyEnrichedEvent struct {
	appID                  *string
	platform               *string
	ETLTstamp              *string
	collectorTstamp        *string
	dvceCreatedTstamp      *string
	event                  *string
	eventID                *string
	txnID                  *string
	nameTracker            *string
	vTracker               *string
	vCollector             *string
	vETL                   *string
	userID                 *string
	userIPaddress          *string
	userFingerprint        *string
	domainUserID           *string
	domainSessionIdx       *int
	networkUserID          *string
	geoCountry             *string
	geoRegion              *string
	geoCity                *string
	geoZipcode             *string
	geoLatitude            *float64
	geoLongitude           *float64
	geoRegionName          *string
	ipISP                  *string
	ipOrganization         *string
	ipDomain               *string
	ipNetspeed             *string
	pageURL                *string
	pageTitle              *string
	pageReferrer           *string
	pageURLScheme          *string
	pageURLHost            *string
	pageURLPort            *int
	pageURLPath            *string
	pageURLQuery           *string
	pageURLFragment        *string
	refrURLScheme          *string
	refrURLHost            *string
	refrURLPort            *int
	refrURLPath            *string
	refrURLQuery           *string
	refrURLFragment        *string
	refrMedium             *string
	refrSource             *string
	refrTerm               *string
	mktMedium              *string
	mktSource              *string
	mktTerm                *string
	mktContent             *string
	mktCampaign            *string
	contexts               *string
	seCategory             *string
	seAction               *string
	seLabel                *string
	seProperty             *string
	seValue                *string
	unstructEvent          *string
	trOrderid              *string
	trAffiliation          *string
	trTotal                *string
	trTax                  *string
	trShipping             *string
	trCity                 *string
	trState                *string
	trCountry              *string
	tiOrderID              *string
	tiSKU                  *string
	tiName                 *string
	tiCategory             *string
	tiPrice                *string
	tiQuantity             *int
	ppXoffsetMin           *int
	ppXoffsetMax           *int
	ppYoffsetMin           *int
	ppYoffsetMax           *int
	useragent              *string
	brName                 *string
	brFamily               *string
	brVersion              *string
	brType                 *string
	brRenderEngine         *string
	brLang                 *string
	brFeaturesPDF          []byte
	brFeaturesFlash        []byte
	brFeaturesJava         []byte
	brFeaturesDirector     []byte
	brFeaturesQuicktime    []byte
	brFeaturesRealplayer   []byte
	brFeaturesWindowsMedia []byte
	brFeaturesGears        []byte
	brFeaturesSilverlight  []byte
	brCookies              []byte
	brColorDepth           *string
	brViewwidth            *int
	brViewHeight           *int
	osName                 *string
	osFamily               *string
	osManufacturer         *string
	osTimezone             *string
	dvceType               *string
	dvceIsMobile           []byte
	dvceScreenWidth        *int
	dvceScreenHeight       *int
	docCharset             *string
	docWidth               *int
	docHeight              *int
	trCurrency             *string
	trTotalBase            *string
	trTaxBase              *string
	trShippingBase         *string
	tiCurrency             *string
	tiPriceBase            *string
	baseCurrency           *string
	geoTimezone            *string
	mktClickid             *string
	mktNetwork             *string
	etlTags                *string
	dvceSentTstamp         *string
	refrDomainUserID       *string
	refrDvceTstamp         *string
	derivedContexts        *string
	domainSessionID        *string
	derivedTstamp          *string
	eventVendor            *string
	eventName              *string
	eventFormat            *string
	eventVersion           *string
	eventFingerprint       *string
	trueTstamp             *string
}

func NewPartiallyEnrichedEvent() *PartiallyEnrichedEvent {
	return &PartiallyEnrichedEvent{}
}

type EnrichmentPayload struct {
	Enriched *PartiallyEnrichedEvent
	Raw      *RawEvent
}

func NewEnrichmentPayload(
	enriched *PartiallyEnrichedEvent,
	raw *RawEvent,
) *EnrichmentPayload {
	return &EnrichmentPayload{
		Enriched: enriched,
		Raw:      raw,
	}
}
