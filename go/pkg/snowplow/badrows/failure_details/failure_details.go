package failuredetails

import (
	"encoding/json"

	client "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

type SchemaViolationDetail interface{}

var _ SchemaViolationDetail = (*SchemaViolationNotJSON)(nil)
var _ SchemaViolationDetail = (*SchemaViolationNotIglu)(nil)
var _ SchemaViolationDetail = (*SchemaViolationIgluError)(nil)
var _ SchemaViolationDetail = (*SchemaViolationCriterionMismatch)(nil)

type SchemaViolationNotJSON struct {
	Field string  `json:"field"`
	Value *string `json:"value"`
	Error string  `json:"error"`
}

func NewSchemaViolationNotJSON(field string, value *string, err string) *SchemaViolationNotJSON {
	return &SchemaViolationNotJSON{
		Field: field,
		Value: value,
		Error: err,
	}
}

type SchemaViolationNotIglu struct {
	JSON  map[string]interface{} `json:"json"`
	Error core.ParseError        `json:"error"`
}

func NewSchemaViolationNotIglu(p string, err core.ParseError) *SchemaViolationNotIglu {
	params := make(map[string]interface{})
	if err := json.Unmarshal([]byte(p), &params); err != nil {
		panic(err)
	}

	return &SchemaViolationNotIglu{
		JSON:  params,
		Error: err,
	}
}

func (f *SchemaViolationNotIglu) MarshalJSON() ([]byte, error) {
	type Alias SchemaViolationNotIglu
	//nolint:wrapcheck
	return json.Marshal(&struct {
		Error string `json:"error"`
		*Alias
	}{
		Error: f.Error.Code(),
		Alias: (*Alias)(f),
	})
}

type SchemaViolationIgluError struct {
	SchemaKey *core.SchemaKey    `json:"schema"`
	Error     client.ClientError `json:"error"`
}

func NewSchemaViolationIgluError(schemaKey *core.SchemaKey, err client.ClientError) *SchemaViolationIgluError {
	return &SchemaViolationIgluError{
		SchemaKey: schemaKey,
		Error:     err,
	}
}

type SchemaViolationCriterionMismatch struct {
	SchemaKey       *core.SchemaKey       `json:"schema"`
	SchemaCriterion *core.SchemaCriterion `json:"criterion"`
}

func NewSchemaViolationCriterionMismatch(
	key *core.SchemaKey, crit *core.SchemaCriterion,
) *SchemaViolationCriterionMismatch {
	return &SchemaViolationCriterionMismatch{
		SchemaKey:       key,
		SchemaCriterion: crit,
	}
}

type EnrichmentInformation struct {
	SchemaKey  *core.SchemaKey
	Identifier string
}

func NewEnrichmentInformation(key *core.SchemaKey, identifier string) *EnrichmentInformation {
	return &EnrichmentInformation{
		SchemaKey:  key,
		Identifier: identifier,
	}
}

type EnrichmentFailure struct {
	Information *EnrichmentInformation
	Message     EnrichmentFailureDetail
}

func NewEnrichmentFailure(enrichment *EnrichmentInformation, message EnrichmentFailureDetail) *EnrichmentFailure {
	return &EnrichmentFailure{
		Information: enrichment,
		Message:     message,
	}
}

type EnrichmentFailureDetail interface{}

var _ EnrichmentFailureDetail = (*EnrichmentFailureSimple)(nil)
var _ EnrichmentFailureDetail = (*EnrichmentFailureInputData)(nil)
var _ EnrichmentFailureDetail = (*EnrichmentFailureIgluError)(nil)

type EnrichmentFailureSimple struct {
	Error string
}

func NewEnrichmentFailureSimple(err string) *EnrichmentFailureSimple {
	return &EnrichmentFailureSimple{Error: err}
}

type EnrichmentFailureInputData struct {
	Field       string
	Value       *string
	Expectation string
}

func NewEnrichmentFailureInputData(
	field string,
	value *string,
	expectation string,
) *EnrichmentFailureInputData {
	return &EnrichmentFailureInputData{
		Field:       field,
		Value:       value,
		Expectation: expectation,
	}
}

type EnrichmentFailureIgluError struct {
	Error string
}

//nolint:unparam
func NewEnrichmentFailureIgluError(key *core.SchemaKey, err client.ClientError) *EnrichmentFailureIgluError {
	return &EnrichmentFailureIgluError{
		Error: "",
	}
}

type TrackerProtocolViolation interface{}

var _ TrackerProtocolViolation = (*TrackerProtocolViolationInputData)(nil)
var _ TrackerProtocolViolation = (*TrackerProtocolViolationNotJSON)(nil)
var _ TrackerProtocolViolation = (*TrackerProtocolViolationNotIglu)(nil)
var _ TrackerProtocolViolation = (*TrackerProtocolViolationIgluError)(nil)
var _ TrackerProtocolViolation = (*TrackerProtocolViolationCriterionMismatch)(nil)

var _ TrackerProtocolViolation = (*AdapterFailureNotJSON)(nil)
var _ TrackerProtocolViolation = (*AdapterFailureNotIglu)(nil)
var _ TrackerProtocolViolation = (*AdapterFailureInputData)(nil)
var _ TrackerProtocolViolation = (*AdapterFailureSchemaMapping)(nil)

type TrackerProtocolViolationInputData struct {
	Field       string
	Value       *string
	Expectation string
}

func NewTrackerProtocolViolationInputData(
	field string, value *string, expectation string,
) *TrackerProtocolViolationInputData {
	return &TrackerProtocolViolationInputData{
		Field:       field,
		Value:       value,
		Expectation: expectation,
	}
}

type TrackerProtocolViolationNotJSON struct {
	Field string
	Value *string
	Error string
}

func NewTrackerProtocolViolationNotJSON(field string, value *string, err string) *TrackerProtocolViolationNotJSON {
	return &TrackerProtocolViolationNotJSON{
		Field: field,
		Value: value,
		Error: err,
	}
}

type TrackerProtocolViolationNotIglu struct {
	JSON  string
	Error core.ParseError
}

func NewTrackerProtocolViolationNotIglu(json string, err core.ParseError) *TrackerProtocolViolationNotIglu {
	return &TrackerProtocolViolationNotIglu{
		JSON:  json,
		Error: err,
	}
}

type TrackerProtocolViolationIgluError struct {
	SchemaKey *core.SchemaKey
	Error     client.ClientError
}

func NewTrackerProtocolViolationIgluError(
	key *core.SchemaKey, err client.ClientError,
) *TrackerProtocolViolationIgluError {
	return &TrackerProtocolViolationIgluError{
		SchemaKey: key,
		Error:     err,
	}
}

type TrackerProtocolViolationCriterionMismatch struct {
	SchemaKey       *core.SchemaKey
	SchemaCriterion *core.SchemaCriterion
}

func NewTrackerProtocolViolationCriterionMismatch(
	key *core.SchemaKey, criterion *core.SchemaCriterion,
) *TrackerProtocolViolationCriterionMismatch {
	return &TrackerProtocolViolationCriterionMismatch{
		SchemaKey:       key,
		SchemaCriterion: criterion,
	}
}

type AdapterFailureNotJSON struct {
	Field string
	Value *string
	Error string
}

func NewAdapterFailureNotJSON(field string, value *string, err string) *AdapterFailureNotJSON {
	return &AdapterFailureNotJSON{
		Field: field,
		Value: value,
		Error: err,
	}
}

type AdapterFailureNotIglu struct {
	JSON  string
	Error *core.ParseError
}

func NewAdapterFailureNotIglu(json string, err *core.ParseError) *AdapterFailureNotIglu {
	return &AdapterFailureNotIglu{
		JSON:  json,
		Error: err,
	}
}

type AdapterFailureInputData struct {
	Field       string
	Value       *string
	Expectation string
}

func NewAdapterFailureInputData(field string, value *string, expectation string) *AdapterFailureInputData {
	return &AdapterFailureInputData{
		Field:       field,
		Value:       value,
		Expectation: expectation,
	}
}

type AdapterFailureSchemaMapping struct {
	Actual          *string
	ExpectedMapping map[string]*core.SchemaKey
	Expectation     string
}

func NewAdapterFailureSchemaMapping(
	actual *string, m map[string]*core.SchemaKey, e string,
) *AdapterFailureSchemaMapping {
	return &AdapterFailureSchemaMapping{
		Actual:          actual,
		ExpectedMapping: m,
		Expectation:     e,
	}
}
