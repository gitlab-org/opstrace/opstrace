package snowplow

import (
	"context"
	"encoding/json"
	"strings"
	"testing"

	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	sv "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/schemas/com.snowplowanalytics.snowplow.badrows/sizeviolation"
	th "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/thrift/gen-go/collector_payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"go.uber.org/zap"
)

var (
	logger                    = zap.NewNop()
	serializerPool            = utils.NewTSerializerPool()
	sizeViolationSchemaKey, _ = core.NewSchemaKeyFromURI("iglu:com.snowplowanalytics.snowplow.badrows/size_violation/jsonschema/1-0-0")
)

type RandomEvent struct {
	E  string `json:"e"`
	Tv string `json:"tv"`
}

func TestSplittingListOfStrings(t *testing.T) {
	events := make([]json.RawMessage, 0)
	for _, e := range []string{
		"123456778901", "123456789", "12345678", "1234567", "123456", "12345", "1234", "123", "12", "1",
	} {
		eJSON, err := json.Marshal(e)
		require.NoError(t, err)
		events = append(events, json.RawMessage(eJSON))
	}
	result := NewSplitBatch(logger, testAppInfo, serializerPool).split(events, 13, 0)
	/*
		expected good batches:
		("1", "12", "123"),
		("1234", "12345"),
		("123456"),
		("1234567"),
		("12345678"),
		("123456789")
	*/
	assert.Equal(t, len(result.GoodBatches), 6)
	/*
		expected bad batches:
		("123456778901")
	*/
	assert.Equal(t, len(result.FailedBigEvents), 1)
}

func TestSplittingOversizedGETPayloads(t *testing.T) {
	invalidQueryString := strings.Repeat("x", 1000)
	payload := &th.CollectorPayload{
		Querystring: utils.StrPtr(invalidQueryString),
	}
	result, err := NewSplitBatch(logger, testAppInfo, serializerPool).SplitAndSerializePayload(context.TODO(), payload, 100)
	require.NoError(t, err)
	assert.Equal(t, 1, len(result.BadEvents))

	parsed := core.NewSelfDescribingDataFromParams(sizeViolationSchemaKey, string(result.BadEvents[0])).Payload()

	var bad sv.SizeViolation
	err = json.Unmarshal([]byte(parsed), &bad)
	require.NoError(t, err)

	assert.Equal(t, 100, bad.Failure.MaximumAllowedSizeBytes)

	failureMsg := bad.Failure.Expectation
	assert.Equal(t, strings.Contains(failureMsg, "GET requests cannot be split"), true)
}

func TestRejectingOversizedPOSTPayloadsWithUnparseableBody(t *testing.T) {
	unparseableBody := strings.Repeat("x", 1000)
	payload := &th.CollectorPayload{
		Body: utils.StrPtr(unparseableBody),
	}
	result, err := NewSplitBatch(logger, testAppInfo, serializerPool).SplitAndSerializePayload(context.TODO(), payload, 100)
	require.NoError(t, err)
	assert.Equal(t, 1, len(result.BadEvents))

	parsed := core.NewSelfDescribingDataFromParams(sizeViolationSchemaKey, string(result.BadEvents[0])).Payload()

	var bad sv.SizeViolation
	err = json.Unmarshal([]byte(parsed), &bad)
	require.NoError(t, err)

	assert.Equal(t, 100, bad.Failure.MaximumAllowedSizeBytes)

	failureMsg := bad.Failure.Expectation
	assert.Equal(t, strings.Contains(failureMsg, "cannot split POST requests which are not JSON"), true)
}

func TestRejectingPOSTPayloadWithOversizedNonData(t *testing.T) {
	events := make([]string, 0)
	for _, e := range []RandomEvent{
		{E: "se", Tv: "js"},
		{E: "se", Tv: "js"},
	} {
		eJSON, err := json.Marshal(e)
		require.NoError(t, err)
		events = append(events, string(eJSON))
	}

	schemaKey := core.NewSchemaKeyFromParams("v", "n", "f", core.NewSchemaVerFull(1, 0, 0))
	data, err := core.NewSelfDescribingDataFromParamsArr(schemaKey, events).AsString()
	require.NoError(t, err)

	payload := &th.CollectorPayload{
		Path: strings.Repeat("p", 1000),
		Body: utils.StrPtr(data),
	}

	result, err := NewSplitBatch(logger, testAppInfo, serializerPool).SplitAndSerializePayload(context.TODO(), payload, 1000)
	require.NoError(t, err)
	assert.Equal(t, 1, len(result.BadEvents))

	parsed := core.NewSelfDescribingDataFromParams(sizeViolationSchemaKey, string(result.BadEvents[0])).Payload()

	var bad sv.SizeViolation
	err = json.Unmarshal([]byte(parsed), &bad)
	require.NoError(t, err)

	assert.Equal(t, 1000, bad.Failure.MaximumAllowedSizeBytes)
}

func TestSplitAndSerializePayload(t *testing.T) {
	events := make([]string, 0)
	for _, e := range []RandomEvent{
		{E: "se", Tv: strings.Repeat("x", 600)},
		{E: "se", Tv: strings.Repeat("x", 5)},
		{E: "se", Tv: strings.Repeat("x", 600)},
		{E: "se", Tv: strings.Repeat("y", 1000)},
		{E: "se", Tv: strings.Repeat("y", 1000)},
		{E: "se", Tv: strings.Repeat("y", 1000)},
		{E: "se", Tv: strings.Repeat("y", 1000)},
	} {
		eJSON, err := json.Marshal(e)
		require.NoError(t, err)
		events = append(events, string(eJSON))
	}

	schemaKey := core.NewSchemaKeyFromParams("vendor", "name", "format", core.NewSchemaVerFull(1, 0, 0))
	data, err := core.NewSelfDescribingDataFromParamsArr(schemaKey, events).AsString()
	require.NoError(t, err)

	payload := &th.CollectorPayload{
		Body: utils.StrPtr(data),
	}
	result, err := NewSplitBatch(logger, testAppInfo, serializerPool).SplitAndSerializePayload(context.TODO(), payload, 1000)
	require.NoError(t, err)

	assert.Equal(t, 2, len(result.GoodSerialized))
	assert.Equal(t, 4, len(result.BadEvents))
}
