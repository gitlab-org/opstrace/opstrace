package snowplow

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/config"
	th "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/thrift/gen-go/collector_payload"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

const (
	CollectorThriftSchema  string = "iglu:com.snowplowanalytics.snowplow/CollectorPayload/thrift/1-0-0"
	DefaultPayloadEncoding string = "UTF-8"
)

var (
	errReadingRequestBody = errors.New("couldn't read body")
)

type AppInfo struct {
	Name    string
	Version string
}

type SnowplowService struct {
	logger         *zap.Logger
	config         *config.Config
	goodSink       Sinker
	badSink        Sinker
	appInfo        *AppInfo
	serializerPool *utils.TSerializerPool
}

func NewService(
	logger *zap.Logger,
	config *config.Config,
	goodSink, badSink Sinker,
	appInfo *AppInfo,
) *SnowplowService {
	pool := utils.NewTSerializerPool()
	return &SnowplowService{
		config:         config,
		goodSink:       goodSink,
		badSink:        badSink,
		logger:         logger,
		appInfo:        appInfo,
		serializerPool: pool,
	}
}

func (s *SnowplowService) SinksHealthy() bool {
	return s.goodSink.IsHealthy() && s.badSink.IsHealthy()
}

func readHeader(ctx *gin.Context, header string) *string {
	if v := ctx.GetHeader(header); v != "" {
		return utils.StrPtr(v)
	}
	return nil
}

func (s *SnowplowService) Ingest(ctx *gin.Context, pixelExpected bool) {
	r := ctx.Request

	var (
		body []byte
		err  error
	)
	if r.Body != nil {
		body, err = io.ReadAll(r.Body)
		if err != nil {
			ctx.AbortWithError(http.StatusInternalServerError, errReadingRequestBody)
			return
		}
		defer r.Body.Close()
	}

	redirect := strings.HasPrefix(r.URL.Path, "/r/")
	useragent := readHeader(ctx, "User-Agent")
	refererURI := readHeader(ctx, "Referer")
	spAnonymous := readHeader(ctx, "SP-Anonymous")
	contentType := utils.StrPtr(strings.ToLower(ctx.ContentType()))

	cookieBounceEnabled := s.config.CookieBounce.Enabled
	queryContainsCookieBounce := strings.Contains(r.URL.RawQuery, s.config.CookieBounce.Name)
	alreadyBouncing := cookieBounceEnabled && queryContainsCookieBounce

	cookie, _ := ctx.Cookie(s.config.Cookie.Name) //nolint:errcheck
	networkUserID := getNetworkUserID(r, cookie, spAnonymous)
	if networkUserID == "" {
		if alreadyBouncing {
			networkUserID = s.config.CookieBounce.FallbackNetworkUserID
		} else {
			networkUserID = uuid.New().String()
		}
	}

	doNotTrack := checkDoNotTrackCookie(r, &s.config.DoNotTrackCookie)
	setCookie := setupCookie(
		&(s.config.Cookie),
		networkUserID,
		doNotTrack,
		spAnonymous,
		time.Now().UTC(),
	)
	ctx.Header("Set-Cookie", setCookie)
	ctx.Header("Access-Control-Allow-Origin", "*")
	ctx.Header("Access-Control-Allow-Credentials", "true")
	if pixelExpected {
		ctx.Header("Cache-Control", "public, max-age=31536000")
	}

	shouldBounce := cookieBounceEnabled && networkUserID == "" && !alreadyBouncing && !redirect && pixelExpected
	b := bounceLocationHeader(s.config.CookieBounce, shouldBounce, r)
	if b != nil {
		ctx.Header("Location", b.Get("Location"))
	}

	if !doNotTrack && !shouldBounce {
		ip := extractIP(r, spAnonymous)
		ipAddress, partitionKey := computePartitionKey(ip, s.config.Streams.UseIPAsPartitionKey)

		event := &th.CollectorPayload{
			Body:          utils.StrPtr(string(body)),
			Schema:        CollectorThriftSchema,
			IpAddress:     ipAddress,
			Timestamp:     time.Now().UnixNano() / int64(time.Millisecond), // Current time in milliseconds
			Encoding:      DefaultPayloadEncoding,
			Collector:     fmt.Sprintf("%s-%s", s.appInfo.Name, s.appInfo.Version),
			Path:          strings.TrimPrefix(r.URL.Path, "/"),
			NetworkUserId: utils.StrPtr(networkUserID),
			Headers:       getFilteredHeaders(r, spAnonymous),
			Querystring:   utils.StrPtr(r.URL.RawQuery),
			RefererUri:    refererURI,
			UserAgent:     useragent,
			Hostname:      utils.StrPtr(r.URL.Host),
			ContentType:   contentType,
		}

		if err := s.sinkEvent(ctx, event, partitionKey); err != nil {
			s.logger.Sugar().Errorf("sinking event: %v", err)
			ctx.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}

	ctx.Status(http.StatusOK)
}

func (s *SnowplowService) sinkEvent(
	ctx context.Context,
	payload *th.CollectorPayload,
	partitionKey string,
) error {
	maxBytes := 100000000
	result, err := NewSplitBatch(
		s.logger,
		s.appInfo,
		s.serializerPool,
	).SplitAndSerializePayload(ctx, payload, maxBytes)
	if err != nil {
		return fmt.Errorf("splitting payload: %w", err)
	}

	if s.goodSink != nil && result.GoodSerialized != nil {
		s.logger.Sugar().Infof("storing %d good events", len(result.GoodSerialized))
		if err := s.goodSink.StoreRawEvents(ctx, result.GoodSerialized, partitionKey); err != nil {
			return err
		}
	}

	if s.badSink != nil && result.BadEvents != nil {
		s.logger.Sugar().Infof("storing %d bad events", len(result.BadEvents))
		if err := s.badSink.StoreRawEvents(ctx, result.BadEvents, partitionKey); err != nil {
			return err
		}
	}

	return nil
}
