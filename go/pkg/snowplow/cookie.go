package snowplow

import (
	"net/http"
	"regexp"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/config"
)

// checkDoNotTrackCookie checks for a "Do Not Track" cookie in the request
func checkDoNotTrackCookie(r *http.Request, config *config.DoNotTrackCookieConfig) bool {
	if !config.Enabled {
		return false
	}
	cookie, err := r.Cookie(config.Name)
	if err != nil {
		return false
	}
	matched, _ := regexp.MatchString(config.Value, cookie.Value) //nolint:errcheck
	return matched
}

// setupCookie builds a Set-Cookie header with the network user ID as value
func setupCookie(
	conf *config.CookieConfig,
	networkUserID string,
	doNotTrack bool,
	spAnonymous *string,
	now time.Time,
) string {
	if doNotTrack || !conf.Enabled || spAnonymous != nil {
		return "" // Return empty string if any condition to not set the cookie is met
	}
	cookie := http.Cookie{
		Name:     conf.Name,
		Value:    networkUserID,
		Expires:  now.Add(conf.Expiration),
		Domain:   cookieDomain(conf.Domains, conf.FallbackDomain),
		Path:     "/",
		SameSite: conf.SameSite,
		Secure:   conf.Secure,
		HttpOnly: conf.HTTPOnly,
	}
	return cookie.String()
}

func cookieDomain(domains []string, fallbackDomain string) string {
	if len(domains) > 0 {
		return domains[0]
	}
	return fallbackDomain
}
