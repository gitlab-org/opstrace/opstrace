package utils

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
)

// ExtractUnencodedJson validates a string as correct JSON.
func ExtractUnencodedJSON(str string) (string, error) {
	if str == "" {
		return "", nil
	}
	if json.Valid([]byte(str)) {
		return str, nil
	}
	return "", errors.New("invalid json")
}

// DecodeBase64Url decodes a Base64 URL-safe encoded string.
func DecodeBase64URL(str string) (string, error) {
	if str == "" {
		return "", nil
	}
	decoded, err := base64.URLEncoding.DecodeString(str)
	if err != nil {
		return "", fmt.Errorf("decoding as base64: %w", err)
	}
	return string(decoded), nil
}

// ExtractBase64EncodedJson decodes Base64 and validates JSON.
func ExtractBase64EncodedJSON(str string) (string, error) {
	if str == "" {
		return "", nil
	}
	decoded, err := DecodeBase64URL(str)
	if err != nil {
		return "", fmt.Errorf("decoding passed str: %w", err)
	}
	return ExtractUnencodedJSON(decoded)
}
