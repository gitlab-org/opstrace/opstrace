package utils

import (
	"time"

	"github.com/nats-io/nats.go/jetstream"
)

func CommonStreamConfig(stream string, subjects []string) jetstream.StreamConfig {
	return jetstream.StreamConfig{
		Name:              stream,
		Subjects:          subjects,
		MaxAge:            24 * time.Hour, // keep messages for a day
		Storage:           jetstream.FileStorage,
		Retention:         jetstream.LimitsPolicy,
		MaxConsumers:      -1,
		MaxMsgsPerSubject: -1,
	}
}

func CommonConsumerConfig(stream string, subjects []string) jetstream.ConsumerConfig {
	return jetstream.ConsumerConfig{
		Durable:        stream,
		Name:           stream,
		FilterSubjects: subjects,
		DeliverPolicy:  jetstream.DeliverLastPolicy,
		AckPolicy:      jetstream.AckExplicitPolicy,
	}
}
