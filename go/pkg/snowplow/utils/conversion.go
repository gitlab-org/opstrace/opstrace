package utils

import (
	"encoding/json"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
)

func JSON(params map[string]string) string {
	bts, _ := json.Marshal(params) //nolint:errcheck
	return string(bts)
}

func IntPtr(a int) *int {
	return &a
}

func StrPtr(a string) *string {
	return &a
}

func TimePtr(a time.Time) *time.Time {
	return &a
}

func FloatPtr(a float64) *float64 {
	return &a
}

// FixTabsNewlines replaces tabs with four spaces and removes control characters, including newlines.
// Returns a pointer to the modified string if non-empty, otherwise nil.
func FixTabsNewlines(str string) string {
	// replace tabs with four spaces
	fixed := strings.ReplaceAll(str, "\t", "    ")

	// remove all control characters (Unicode category Cntrl)
	fixed = regexp.MustCompile(`\p{C}`).ReplaceAllString(fixed, "")

	return fixed
}

func ValidateUUID(s string) (string, error) {
	_, err := uuid.Parse(s)
	if err != nil {
		return "", fmt.Errorf("parsing %s as uuid: %w", s, err)
	}
	return strings.ToLower(s), nil
}

func StringToBooleanLikeByte(str string) (byte, error) {
	switch str {
	case "1":
		return 1, nil
	case "0":
		return 0, nil
	default:
		return 0, fmt.Errorf("cannot be converted to boolean-like byte")
	}
}

func StringToDouble(str string) (float64, error) {
	return strconv.ParseFloat(str, 64) //nolint:wrapcheck
}

func ParseURLEncodedForm(s string) (map[string]*string, error) {
	parsed, err := url.ParseQuery(s)
	if err != nil {
		return nil, fmt.Errorf("parsing query: %w", err)
	}
	result := make(map[string]*string)
	for k, v := range parsed {
		if len(v) == 0 {
			result[k] = nil
		} else {
			result[k] = &(v[0])
		}
	}
	return result, nil
}
