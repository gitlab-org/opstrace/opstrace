package utils

import "time"

func ToEpochMilli(millis int64) time.Time {
	secs := millis / 1000                // seconds since epoch
	nanos := (millis % 1000) * 1_000_000 // remaining milliseconds converted to nanoseconds
	return time.Unix(secs, nanos).UTC()  // create time.Time and ensure UTC
}
