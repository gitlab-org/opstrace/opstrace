package utils

import (
	"sync"

	"github.com/apache/thrift/lib/go/thrift"
)

type TSerializerPool struct {
	pool *sync.Pool
}

func NewTSerializerPool() *TSerializerPool {
	return &TSerializerPool{
		pool: &sync.Pool{
			New: func() interface{} {
				t := thrift.NewTMemoryBuffer()
				p := thrift.NewTBinaryProtocolFactoryConf(&thrift.TConfiguration{}).GetProtocol(t)
				return &thrift.TSerializer{Transport: t, Protocol: p}
			},
		},
	}
}

func (t *TSerializerPool) Get() *thrift.TSerializer {
	return t.pool.Get().(*thrift.TSerializer)
}

func (t *TSerializerPool) Put(ts *thrift.TSerializer) {
	// reset transport
	ts.Transport.Close()
	// reset buffer
	ts.Transport = thrift.NewTMemoryBuffer()
	// reset protocol
	ts.Protocol = thrift.NewTBinaryProtocolFactoryConf(&thrift.TConfiguration{}).GetProtocol(ts.Transport)
	t.pool.Put(ts)
}

type TDeserializerPool struct {
	pool *sync.Pool
}

func NewTDeserializerPool() *TDeserializerPool {
	return &TDeserializerPool{
		pool: &sync.Pool{
			New: func() interface{} {
				t := thrift.NewTMemoryBuffer()
				p := thrift.NewTBinaryProtocolFactoryConf(&thrift.TConfiguration{}).GetProtocol(t)
				return &thrift.TDeserializer{Transport: t, Protocol: p}
			},
		},
	}
}

func (t *TDeserializerPool) Get() *thrift.TDeserializer {
	return t.pool.Get().(*thrift.TDeserializer)
}

func (t *TDeserializerPool) Put(ts *thrift.TDeserializer) {
	// reset transport
	ts.Transport.Close()
	// reset buffer
	ts.Transport = thrift.NewTMemoryBuffer()
	// reset protocol
	ts.Protocol = thrift.NewTBinaryProtocolFactoryConf(&thrift.TConfiguration{}).GetProtocol(ts.Transport)
	t.pool.Put(ts)
}
