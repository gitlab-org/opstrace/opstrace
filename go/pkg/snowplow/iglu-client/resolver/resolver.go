package resolver

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registry"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	resolver_config "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/schemas/com.snowplowanalytics.iglu/resolver-config"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

type Resolver interface {
	Init(cacheSize int, cacheTTL time.Duration, refs ...registry.Registry)
	Prioritize(vendor string) []registry.Registry
	LookupSchema(schemaKey *core.SchemaKey) (string, error)

	GetRepositories() []registry.Registry
}

type DefaultResolver struct {
	repositories []registry.Registry
}

var _ Resolver = (*DefaultResolver)(nil)

func NewDefaultResolver() Resolver {
	d := &DefaultResolver{
		repositories: make([]registry.Registry, 0),
	}
	return d
}

func NewDefaultResolverFromConfig(configJSON string) (Resolver, error) {
	d := &DefaultResolver{
		repositories: make([]registry.Registry, 0),
	}

	config, err := NewResolverConfigFromJSON([]byte(configJSON))
	if err != nil {
		return nil, fmt.Errorf("parsing registry config: %w", err)
	}

	for _, repo := range config.Repositories {
		repoJSON, err := json.Marshal(repo)
		if err != nil {
			return nil, fmt.Errorf("serializing repository config: %w", err)
		}
		registry, err := registry.Parse(string(repoJSON))
		if err != nil {
			return nil, err
		}

		d.repositories = append(d.repositories, registry)
	}

	return d, nil
}

func (d *DefaultResolver) Init(cacheSize int, cacheTTL time.Duration, refs ...registry.Registry) {}

func (d *DefaultResolver) Prioritize(vendor string) []registry.Registry {
	return nil
}

func (d *DefaultResolver) LookupSchema(key *core.SchemaKey) (string, error) {
	for _, repo := range d.repositories {
		schema, err := repo.LookupSchema(key)
		if err == nil {
			return schema, nil
		}
	}
	return "", fmt.Errorf("schema: %s not found", key.AsString())
}

func (d *DefaultResolver) GetRepositories() []registry.Registry {
	return d.repositories
}

type ResolverConfig struct {
	resolver_config.A103
}

var configurationSchema = core.NewSchemaCriterionFromParams(
	"com.snowplowanalytics.iglu", "resolver-config", "jsonschema", utils.IntPtr(1), utils.IntPtr(0), nil,
)

func NewResolverConfigFromJSON(configJSON []byte) (*ResolverConfig, error) {
	configSDJ := core.NewSelfDescribingData()
	if err := configSDJ.Parse(configJSON); err != nil {
		return nil, fmt.Errorf("parsing JSON as resolver config: %s", err.Code())
	}

	if !configurationSchema.Matches(configSDJ.SchemaKey) {
		return nil, fmt.Errorf(
			"schema %s does not match criterion %s", configSDJ.SchemaKey.AsString(), configurationSchema.AsString(),
		)
	}

	var c resolver_config.A103
	if err := json.Unmarshal([]byte(configSDJ.Payload()), &c); err != nil {
		return nil, fmt.Errorf("parsing registry config from JSON: %w", err)
	}
	return &ResolverConfig{c}, nil
}
