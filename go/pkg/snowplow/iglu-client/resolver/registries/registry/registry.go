package registry

import (
	"encoding/json"
	"fmt"
	"net/url"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/embedded"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/http"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registryconfig"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

type Registry interface {
	LookupSchema(schemaKey *core.SchemaKey) (string, error)
}

func Parse(configJSON string) (Registry, error) {
	registryConfig, err := registryconfig.NewRegistryConfigFromJSON(configJSON)
	if err != nil {
		return nil, err
	}

	var raw map[string]interface{}
	if err := json.Unmarshal([]byte(configJSON), &raw); err != nil {
		return nil, fmt.Errorf("deserializing config JSON: %w", err)
	}

	//nolint:nestif
	if connectionRaw, exists := raw["connection"]; exists {
		if connectionData, existsAsMap := connectionRaw.(map[string]interface{}); existsAsMap {
			if embeddedRaw, isEmbedded := connectionData["embedded"]; isEmbedded {
				if embeddedData, isEmbeddedMap := embeddedRaw.(map[string]interface{}); isEmbeddedMap {
					return buildEmbeddedRegistry(registryConfig, embeddedData)
				}
			}
			if httpRaw, isHTTP := connectionData["http"]; isHTTP {
				if httpData, ok := httpRaw.(map[string]interface{}); ok {
					return buildHTTPRegistry(registryConfig, httpData)
				}
			}
		}
	}
	return nil, fmt.Errorf("invalid config")
}

func buildEmbeddedRegistry(conf *registryconfig.RegistryConfig, params map[string]interface{}) (Registry, error) {
	if _, ok := params["path"]; !ok {
		return nil, fmt.Errorf("missing embed path")
	}
	embedPath := fmt.Sprintf("%s", params["path"])
	return embedded.NewEmbeddedRegistry(conf, embedPath), nil
}

func buildHTTPRegistry(conf *registryconfig.RegistryConfig, params map[string]interface{}) (Registry, error) {
	if _, ok := params["uri"]; !ok {
		return nil, fmt.Errorf("missing URI")
	}
	u, err := url.Parse(fmt.Sprintf("%s", params["uri"]))
	if err != nil {
		return nil, fmt.Errorf("parsing registry URI: %w", err)
	}
	httpConnection := http.NewHTTPConnection(u, nil) // fix passing API key
	return http.NewHTTPRegistry(conf, httpConnection), nil
}
