package registryconfig

import (
	"encoding/json"
	"fmt"
	"strings"
)

type RegistryConfig struct {
	Name             string   `json:"name"`
	InstancePriority float64  `json:"priority"`
	VendorPrefixes   []string `json:"vendorPrefixes"`
}

func NewRegistryConfig(name string, priority float64, prefixes []string) *RegistryConfig {
	return &RegistryConfig{
		Name:             name,
		InstancePriority: priority,
		VendorPrefixes:   prefixes,
	}
}

func NewRegistryConfigFromJSON(configJSON string) (*RegistryConfig, error) {
	var temp RegistryConfig
	if err := json.Unmarshal([]byte(configJSON), &temp); err != nil {
		return nil, fmt.Errorf("deserializing passed config JSON: %w", err)
	}
	return &temp, nil
}

func (c *RegistryConfig) VendorMatched(vendor string) bool {
	for _, pre := range c.VendorPrefixes {
		if strings.HasPrefix(pre, vendor) {
			return true
		}
	}
	return false
}
