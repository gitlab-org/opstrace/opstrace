package registrylookup

import (
	"fmt"
	"strings"

	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

func ToPath(prefix string, key *core.SchemaKey) string {
	prefix = strings.TrimSuffix(prefix, "/")
	return fmt.Sprintf("%s/schemas/%s", prefix, key.AsPath())
}
