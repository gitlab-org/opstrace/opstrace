package embedded

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registryconfig"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

var testEmbeddedRegistry = EmbeddedRegistry{
	Config: registryconfig.NewRegistryConfig("Iglu Test Embedded", 0, []string{"com.snowplowanalytics"}),
	Path:   filepath.Join("testdata", "iglu-test-embedded"),
}

func TestRetrievingExistingJSONSchema(t *testing.T) {
	expected := `
{
  "$schema":"http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
  "description":"Test schema",
  "self":{
	"vendor":"com.snowplowanalytics.iglu-test",
	"name":"stock-item",
	"format":"jsonschema",
	"version":"1-0-0"
  },
  "type":"object",
  "properties":{
	"id":{
	  "type":"string"
	},
	"name":{
	  "type":"string"
	},
	"price":{
	  "type":"number"
	}
  },
  "required":["id","name","price"],
  "additionalProperties":false
}`

	schemaKey := core.NewSchemaKeyFromParams("com.snowplowanalytics.iglu-test", "stock-item", "jsonschema", core.NewSchemaVerFull(1, 0, 0))
	schema, err := testEmbeddedRegistry.LookupSchema(schemaKey)
	require.NoError(t, err)
	assert.JSONEq(t, expected, schema)
}

func TestRetrievingNonExistingJSONSchema(t *testing.T) {
	schemaKey := core.NewSchemaKeyFromParams("com.snowplowanalytics.n-a", "null", "jsonschema", core.NewSchemaVerFull(1, 0, 0))
	_, err := testEmbeddedRegistry.LookupSchema(schemaKey)
	if assert.Error(t, err) {
		assert.Equal(t, errSchemaNotFound, err)
	}
}

func TestRetrievingCorruptedJSONSchema(t *testing.T) {
	schemaKey := core.NewSchemaKeyFromParams("com.snowplowanalytics.iglu-test", "corrupted_schema", "jsonschema", core.NewSchemaVerFull(1, 0, 0))
	_, err := testEmbeddedRegistry.LookupSchema(schemaKey)
	if assert.Error(t, err) {
		assert.Equal(t, errInvalidSchema, err)
	}
}
