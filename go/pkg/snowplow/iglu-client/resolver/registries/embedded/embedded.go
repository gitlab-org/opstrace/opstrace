package embedded

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registryconfig"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

var (
	errSchemaNotFound = errors.New("schema not found")
	errInvalidSchema  = errors.New("schema contains invalid json")
)

type EmbeddedRegistry struct {
	Config *registryconfig.RegistryConfig
	Path   string
}

func NewEmbeddedRegistry(config *registryconfig.RegistryConfig, path string) *EmbeddedRegistry {
	return &EmbeddedRegistry{
		Config: config,
		Path:   path,
	}
}

func (e *EmbeddedRegistry) LookupSchema(schemaKey *core.SchemaKey) (string, error) {
	paths, err := e.walkBasepath()
	if err != nil {
		return "", fmt.Errorf("walking base path: %w", err)
	}

	// trim iglu: from the schema key structure and look it up
	var schemaPath string
	schemaStr := strings.TrimLeft(schemaKey.AsString(), "iglu:")
	for _, path := range paths {
		if strings.Contains(path, schemaStr) {
			schemaPath = path
			break
		}
	}
	// read matched path and return file contents if applicable
	if schemaPath == "" {
		return "", errSchemaNotFound
	}

	schemaJSON, err := os.ReadFile(schemaPath)
	if err != nil {
		return "", fmt.Errorf("reading JSON file: %w", err)
	}

	if !json.Valid(schemaJSON) {
		return "", errInvalidSchema
	}

	return string(schemaJSON), nil
}

func (e *EmbeddedRegistry) walkBasepath() ([]string, error) {
	filepaths := make([]string, 0)
	err := filepath.WalkDir(e.Path, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() {
			filepaths = append(filepaths, path)
		}
		return nil
	})
	if err != nil {
		return filepaths, fmt.Errorf("traversing basepath: %w", err)
	}
	return filepaths, nil
}
