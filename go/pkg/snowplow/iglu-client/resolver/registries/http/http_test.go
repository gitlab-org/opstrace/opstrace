package http

import (
	"encoding/json"
	"fmt"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registryconfig"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registrylookup"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

const AcmeConfig string = `
{
  "name": "Acme Iglu Repo",
  "priority": 5,
  "vendorPrefixes": [ "com.acme" ],
  "connection": {
    "http": {
      "uri": "http://iglu.acme.com"
    }
  }
}`

const AcmeConfigWithAuth string = `
{
  "name": "Acme Iglu Repo",
  "priority": 5,
  "vendorPrefixes": [ "com.acme" ],
  "connection": {
    "http": {
      "apikey": "de305d54-75b4-431b-adb2-eb6b9e546014",
      "uri": "http://iglu.acme.com"
    }
  }
}`

func TestToPathStrippingUnnecessaryTrailingSlash(t *testing.T) {
	resultWithSlash := registrylookup.ToPath(
		"http://iglucentral.com/api/",
		core.NewSchemaKeyFromParams("com.snowplow", "event", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
	)
	resultWithoutSlash := registrylookup.ToPath(
		"http://iglucentral.com/api",
		core.NewSchemaKeyFromParams("com.snowplow", "event", "jsonschema", core.NewSchemaVerFull(1, 0, 0)),
	)

	expected := "http://iglucentral.com/api/schemas/com.snowplow/event/jsonschema/1-0-0"
	assert.Equal(t, expected, resultWithSlash)
	assert.Equal(t, expected, resultWithoutSlash)
}

func TestConstructingRepositoryRefFromJSONConfig(t *testing.T) {
	url, err := url.Parse("http://iglu.acme.com")
	require.NoError(t, err)

	expected := NewHTTPRegistry(
		registryconfig.NewRegistryConfig("Acme Iglu Repo", 5, []string{"com.acme"}),
		NewHTTPConnection(url, nil),
	)

	result, err := parseConfig(AcmeConfig)
	require.NoError(t, err)
	assert.Equal(t, expected, result)
}

func getIgluCentralRegistry() *HTTPRegistry {
	url, err := url.Parse("http://iglucentral.com")
	if err != nil {
		panic(err)
	}

	return NewHTTPRegistry(
		registryconfig.NewRegistryConfig("Iglu Central", 0, []string{"com.snowplowanalytics"}),
		NewHTTPConnection(url, nil),
	)
}

func TestSchemaLookupFromValidPath(t *testing.T) {
	schemaKey := core.NewSchemaKeyFromParams(
		"com.snowplowanalytics.snowplow",
		"link_click",
		"jsonschema",
		core.NewSchemaVerFull(1, 0, 0),
	)

	expected := `{
  "$schema": "http://iglucentral.com/schemas/com.snowplowanalytics.self-desc/schema/jsonschema/1-0-0#",
  "description": "Schema for a link click event",
  "self": {
	"vendor": "com.snowplowanalytics.snowplow",
	"name": "link_click",
	"format": "jsonschema",
	"version": "1-0-0"
  },
  "type": "object",
  "properties": {
	"elementId": {
	  "type": "string"
	},
	"elementClasses": {
	  "type": "array",
	  "items": {
		"type": "string"
	  }
	},
	"elementTarget": {
	  "type": "string"
	},
	"targetUrl": {
	  "type": "string",
	  "minLength": 1
	}
  },
  "required": ["targetUrl"],
  "additionalProperties": false
}`

	result, err := getIgluCentralRegistry().LookupSchema(schemaKey)
	require.NoError(t, err)
	require.NotEmpty(t, result)
	assert.JSONEq(t, expected, result)
}

func TestSchemaLookupFromNonExistentSchema(t *testing.T) {
	schemaKey := core.NewSchemaKeyFromParams(
		"de.ersatz.n-a",
		"null",
		"jsonschema",
		core.NewSchemaVerFull(1, 0, 0),
	)

	result, err := getIgluCentralRegistry().LookupSchema(schemaKey)
	require.Error(t, err) // TODO: add support for RegistryError
	require.Empty(t, result)
}

func TestConstructingRepositoryRefFromJSONConfigWithAPIKey(t *testing.T) {
	apiKey := "de305d54-75b4-431b-adb2-eb6b9e546014" //nolint:gosec

	url, err := url.Parse("http://iglu.acme.com")
	require.NoError(t, err)

	expected := NewHTTPRegistry(
		registryconfig.NewRegistryConfig("Acme Iglu Repo", 5, []string{"com.acme"}),
		NewHTTPConnection(url, utils.StrPtr(apiKey)),
	)

	result, err := parseConfig(AcmeConfigWithAuth)
	require.NoError(t, err)
	assert.Equal(t, expected, result)
}

func parseConfig(configJSON string) (*HTTPRegistry, error) {
	registryConfig, err := registryconfig.NewRegistryConfigFromJSON(configJSON)
	if err != nil {
		return nil, err
	}

	var raw map[string]interface{}
	if err := json.Unmarshal([]byte(configJSON), &raw); err != nil {
		return nil, err
	}

	//nolint:nestif
	if connectionRaw, ok := raw["connection"]; ok {
		if connectionData, ok := connectionRaw.(map[string]interface{}); ok {
			if httpRaw, ok := connectionData["http"]; ok {
				if httpData, ok := httpRaw.(map[string]interface{}); ok {
					return buildHTTPRegistry(registryConfig, httpData)
				}
			}
		}
	}
	return nil, fmt.Errorf("invalid config")
}

func buildHTTPRegistry(conf *registryconfig.RegistryConfig, params map[string]interface{}) (*HTTPRegistry, error) {
	if _, ok := params["uri"]; !ok {
		return nil, fmt.Errorf("missing URI")
	}
	u, err := url.Parse(fmt.Sprintf("%s", params["uri"]))
	if err != nil {
		return nil, err
	}

	var apiKey string
	if _, ok := params["apikey"]; ok {
		apiKey = fmt.Sprintf("%s", params["apikey"])
	}

	var httpConnection *HTTPConnection
	if apiKey != "" {
		httpConnection = NewHTTPConnection(u, utils.StrPtr(apiKey))
	} else {
		httpConnection = NewHTTPConnection(u, nil)
	}

	return NewHTTPRegistry(conf, httpConnection), nil
}
