package http

import (
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registryconfig"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registrylookup"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

var httpClient *http.Client

type HTTPConnection struct {
	URI    *url.URL
	APIKey *string
}

func NewHTTPConnection(uri *url.URL, apiKey *string) *HTTPConnection {
	httpClient = &http.Client{
		Transport: &http.Transport{
			DialContext: (&net.Dialer{Timeout: 1 * time.Second}).DialContext,
		},
		Timeout: 10 * time.Second,
	}

	return &HTTPConnection{
		URI:    uri,
		APIKey: apiKey,
	}
}

type HTTPRegistry struct {
	Config *registryconfig.RegistryConfig
	HTTP   *HTTPConnection
}

func NewHTTPRegistry(config *registryconfig.RegistryConfig, http *HTTPConnection) *HTTPRegistry {
	return &HTTPRegistry{
		Config: config,
		HTTP:   http,
	}
}

const (
	apiKeyHeaderName string = "apikey"
)

func (h *HTTPRegistry) LookupSchema(key *core.SchemaKey) (string, error) {
	schemaPath := registrylookup.ToPath(h.HTTP.URI.String(), key)

	lookupRequest, err := http.NewRequest("GET", schemaPath, nil)
	if err != nil {
		return "", fmt.Errorf("constructing lookup request: %w", err)
	}

	if h.HTTP.APIKey != nil {
		lookupRequest.Header.Add(apiKeyHeaderName, *h.HTTP.APIKey)
	}

	response, err := httpClient.Do(lookupRequest)
	if err != nil {
		return "", fmt.Errorf("making lookup request: %w", err)
	}
	defer response.Body.Close()

	if response.StatusCode >= 200 && response.StatusCode <= 299 {
		body, err := io.ReadAll(response.Body)
		if err != nil {
			return "", fmt.Errorf("reading response body: %w", err)
		}
		return string(body), nil
	}
	return "", fmt.Errorf("requesting schema path: %v", response.Status)
}
