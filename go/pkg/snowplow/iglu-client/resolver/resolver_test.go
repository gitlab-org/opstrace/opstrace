package resolver

import (
	"net/url"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/embedded"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/http"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registry"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver/registries/registryconfig"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

func buildEmbeddedRegistry(prefix string, priority float64) *embedded.EmbeddedRegistry {
	resolverConfig := registryconfig.NewRegistryConfig("An embedded repo", priority, []string{prefix})
	return embedded.NewEmbeddedRegistry(resolverConfig, "/embed-path")
}

var repoOne = buildEmbeddedRegistry("com.acme", 0)
var repoTwo = buildEmbeddedRegistry("de.acompany.snowplow", 40)
var repoThree = buildEmbeddedRegistry("de.acompany.snowplow", 100)

func TestBuildResolverFromValidConfig(t *testing.T) {
	config := `
{
  "schema": "iglu:com.snowplowanalytics.iglu/resolver-config/jsonschema/1-0-0",
  "data": {
	"cacheSize": 500,
	"repositories": [
	  {
		"name": "Iglu Central",
		"priority": 0,
		"vendorPrefixes": [ "com.snowplowanalytics" ],
		"connection": {
		  "http": {
			"uri": "http://iglucentral.com"
		  }
		}
	  },
	  {
		"name": "An embedded repo",
		"priority": 100,
		"vendorPrefixes": [ "de.acompany.snowplow" ],
		"connection": {
		  "embedded": {
			"path": "/embed-path"
		  }
		}
	  }
	]
  }
}`

	resolver, err := NewDefaultResolverFromConfig(config)
	require.NoError(t, err)
	require.NotNil(t, resolver)

	expected := []registry.Registry{getIgluCentralRegistry(), repoThree}
	assert.Equal(t, expected, resolver.GetRepositories())
}

func getIgluCentralRegistry() *http.HTTPRegistry {
	url, err := url.Parse("http://iglucentral.com")
	if err != nil {
		panic(err)
	}

	return http.NewHTTPRegistry(
		registryconfig.NewRegistryConfig("Iglu Central", 0, []string{"com.snowplowanalytics"}),
		http.NewHTTPConnection(url, nil),
	)
}

func TestRepositorySortOrder(t *testing.T) {
	t.Skip()
	resolver := NewDefaultResolver()
	resolver.Init(10, 1*time.Minute, repoOne, repoTwo, repoThree)

	schemaKey := core.NewSchemaKeyFromParams("de.acompany.snowplow", "mobile_context", "jsonschema", core.NewSchemaVerFull(1, 0, 0))
	expected := []registry.Registry{repoTwo, repoThree, repoOne}

	actual := resolver.Prioritize(schemaKey.Vendor)
	assert.Equal(t, expected, actual)
}
