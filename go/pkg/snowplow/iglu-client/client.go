package igluclient

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/resolver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-client/validator"
	core "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/iglu-core"
)

type Client struct {
	resolver  resolver.Resolver
	validator validator.Validator
}

func NewClient(configJSON string) (*Client, error) {
	resolver, err := resolver.NewDefaultResolverFromConfig(configJSON)
	if err != nil {
		return nil, fmt.Errorf("instantiating resolver: %w", err)
	}

	validator := validator.NewJSONSchemaValidator()

	c := &Client{
		resolver:  resolver,
		validator: validator,
	}
	return c, nil
}

func (c *Client) Check(instance *core.SelfDescribingData) ClientError {
	schema, err := c.resolver.LookupSchema(instance.SchemaKey)
	if err != nil {
		return err
	}

	if err := c.validator.ValidateSchema(schema); err != nil {
		return err
	}

	err = c.validator.Validate(schema, instance.Payload())
	if err != nil {
		return err
	}

	return nil
}
