package validator

import (
	"errors"
	"fmt"

	"github.com/xeipuuv/gojsonschema"
)

type Validator interface {
	ValidateSchema(schema string) error
	Validate(schema, data string) error
}

const (
	// Draft-04 meta-schema URL
	metaSchema string = `http://json-schema.org/draft-04/schema`
)

type JSONSchemaValidator struct{}

func NewJSONSchemaValidator() Validator {
	return &JSONSchemaValidator{}
}

func (v *JSONSchemaValidator) ValidateSchema(targetSchema string) error {
	metaSchemaLoader := gojsonschema.NewReferenceLoader(metaSchema)
	targetSchemaLoader := gojsonschema.NewStringLoader(targetSchema)

	result, err := gojsonschema.Validate(metaSchemaLoader, targetSchemaLoader)
	if err != nil {
		return fmt.Errorf("validating target schema against standard: %w", err)
	}

	if result.Valid() {
		return nil
	}

	var validationErr error
	for _, desc := range result.Errors() {
		validationErr = errors.Join(validationErr, fmt.Errorf("%s", desc.String()))
	}
	return validationErr
}

var ErrInvalidType = errors.New("invalid type")

func (v *JSONSchemaValidator) Validate(schema, data string) error {
	schemaLoader := gojsonschema.NewStringLoader(schema)
	dataLoader := gojsonschema.NewStringLoader(data)

	result, err := gojsonschema.Validate(schemaLoader, dataLoader)
	if err != nil {
		return fmt.Errorf("validating json schema: %w", err)
	}

	if result.Valid() {
		return nil
	}

	resultErrors := result.Errors()
	if resultErrors[0].Type() == "invalid_type" {
		return ErrInvalidType
	}

	var validationErr error
	for _, desc := range result.Errors() {
		validationErr = errors.Join(validationErr, fmt.Errorf("%s", desc.String()))
	}
	return validationErr
}
