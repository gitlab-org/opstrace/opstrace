package iglucore

import (
	"errors"
	"fmt"
	"regexp"
)

var ErrInvalidSchemaCriterion = errors.New("invalid schema criterion")
var criterionRegex = regexp.MustCompile("^iglu:" + // Protocol
	"([a-zA-Z0-9-_.]+)/" + // Vendor
	"([a-zA-Z0-9-_]+)/" + // Name
	"([a-zA-Z0-9-_]+)/" + // Format
	"([1-9][0-9]*|\\*)-" + // MODEL (cannot start with zero)
	"((?:0|[1-9][0-9]*)|\\*)-" + // REVISION
	"((?:0|[1-9][0-9]*)|\\*)$") // ADDITION

type SchemaCriterion struct {
	Vendor   string
	Name     string
	Format   string
	Model    *int
	Revision *int
	Addition *int
}

func NewSchemaCriterion() *SchemaCriterion {
	return &SchemaCriterion{}
}

func NewSchemaCriterionFromParams(vendor, name, format string, model, revision, addition *int) *SchemaCriterion {
	return &SchemaCriterion{
		Vendor:   vendor,
		Name:     name,
		Format:   format,
		Model:    model,
		Revision: revision,
		Addition: addition,
	}
}

func (sc *SchemaCriterion) AsString() string {
	return fmt.Sprintf("iglu:%s/%s/%s/%s-%s-%s",
		sc.Vendor,
		sc.Name,
		sc.Format,
		schemaVerComponentToString(sc.Model),
		schemaVerComponentToString(sc.Revision),
		schemaVerComponentToString(sc.Addition),
	)
}

func (sc *SchemaCriterion) Parse(schema string) error {
	if criterionRegex.Match([]byte(schema)) {
		comps := criterionRegex.FindStringSubmatch(schema)
		// fmt.Printf("regex match components: %+v\n", comps)
		sc.Vendor = comps[1]
		sc.Name = comps[2]
		sc.Format = comps[3]
		sc.Model = parseSchemaVerComponent(comps[4])
		sc.Revision = parseSchemaVerComponent(comps[5])
		sc.Addition = parseSchemaVerComponent(comps[6])
		return nil
	}
	return ErrInvalidSchemaCriterion
}

func (sc *SchemaCriterion) PickFrom(keys []*SchemaKey) []*SchemaKey {
	matched := make([]*SchemaKey, 0)
	for _, key := range keys {
		if sc.Matches(key) {
			matched = append(matched, key)
		}
	}
	return matched
}

func (sc *SchemaCriterion) Matches(key *SchemaKey) bool {
	return sc.prefixMatches(key) && sc.verMatch(key.Version)
}

func (sc *SchemaCriterion) prefixMatches(key *SchemaKey) bool {
	return key.Vendor == sc.Vendor &&
		key.Name == sc.Name &&
		key.Format == sc.Format
}

func (sc *SchemaCriterion) verMatch(version *SchemaVer) bool {
	return groupMatch(sc.Model, version.Model) &&
		groupMatch(sc.Revision, version.Revision) &&
		groupMatch(sc.Addition, version.Addition)
}
