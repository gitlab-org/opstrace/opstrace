package iglucore

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParse(t *testing.T) {
	for _, filepaths := range []string{
		filepath.Join("testdata", "sd1.json"),
		filepath.Join("testdata", "sd2.json"),
	} {
		json, err := os.ReadFile(filepaths)
		require.NoError(t, err)
		sd := &SelfDescribingData{}
		parseError := sd.Parse(json)
		require.Nil(t, parseError)
	}
}
