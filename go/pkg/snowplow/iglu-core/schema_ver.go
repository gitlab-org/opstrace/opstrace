package iglucore

import (
	"fmt"
	"regexp"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

type SchemaVer struct {
	Model    *int
	Revision *int
	Addition *int
}

func NewSchemaVer() *SchemaVer {
	return &SchemaVer{}
}

func NewSchemaVerFull(model, revision, addition int) *SchemaVer {
	return &SchemaVer{
		Model:    utils.IntPtr(model),
		Revision: utils.IntPtr(revision),
		Addition: utils.IntPtr(addition),
	}
}

func (ver *SchemaVer) AsString() string {
	return fmt.Sprintf("%s-%s-%s",
		schemaVerComponentToString(ver.Model),
		schemaVerComponentToString(ver.Revision),
		schemaVerComponentToString(ver.Addition),
	)
}

func (ver *SchemaVer) Apply(model, revision, addition *int) {
	ver.Model = model
	ver.Revision = revision
	ver.Addition = addition
}

func (ver *SchemaVer) Equals(expected *SchemaVer) bool {
	return groupMatch(ver.Model, expected.Model) &&
		groupMatch(ver.Revision, expected.Revision) &&
		groupMatch(ver.Addition, expected.Addition)
}

func groupMatch(a, b *int) bool {
	if a != nil && b != nil {
		return *a == *b
	}
	return true
}

func (ver *SchemaVer) Parse(version string) ParseError {
	if err := ver.parseFull(version); err != nil {
		if schemaVerPartialRegex.Match([]byte(version)) {
			comps := schemaVerPartialRegex.FindStringSubmatch(version)
			ver.Model = parseSchemaVerComponent(comps[1])
			ver.Revision = parseSchemaVerComponent(comps[2])
			ver.Addition = parseSchemaVerComponent(comps[3])
			return nil
		}
		return &InvalidSchemaVer{}
	}
	return nil
}

var schemaVerFullRegex = regexp.MustCompile("^([1-9][0-9]*)-(0|[1-9][0-9]*)-(0|[1-9][0-9]*)$")

var schemaVerPartialRegex = regexp.MustCompile("^([1-9][0-9]*|\\?)-" + // MODEL (cannot start with zero)
	"((?:0|[1-9][0-9]*)|\\?)-" + // REVISION
	"((?:0|[1-9][0-9]*)|\\?)$") // ADDITION

func (ver *SchemaVer) parseFull(version string) ParseError {
	if schemaVerFullRegex.Match([]byte(version)) {
		comps := schemaVerFullRegex.FindStringSubmatch(version)

		ver.Model = parseSchemaVerComponent(comps[1])
		if ver.Model == nil {
			return &InvalidSchemaVer{}
		}

		ver.Revision = parseSchemaVerComponent(comps[2])
		if ver.Revision == nil {
			return &InvalidSchemaVer{}
		}

		ver.Addition = parseSchemaVerComponent(comps[3])
		if ver.Addition == nil {
			return &InvalidSchemaVer{}
		}
	}

	return &InvalidSchemaVer{}
}
