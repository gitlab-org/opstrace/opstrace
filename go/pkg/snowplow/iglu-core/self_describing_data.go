package iglucore

import (
	"encoding/json"
	"fmt"

	"github.com/go-playground/validator/v10"
)

const (
	payloadTypeArray = iota
	payloadTypeMap
	payloadTypeRaw
)

type SelfDescribingData struct {
	SchemaKey *SchemaKey        `json:"schema"`
	Data      []json.RawMessage `json:"data"`

	payloadType int
}

func NewSelfDescribingData() *SelfDescribingData {
	return &SelfDescribingData{}
}

func NewSelfDescribingDataFromParams(key *SchemaKey, data string) *SelfDescribingData {
	return &SelfDescribingData{
		SchemaKey:   key,
		Data:        []json.RawMessage{[]byte(data)},
		payloadType: payloadTypeMap,
	}
}

func NewSelfDescribingDataFromParamsArr(key *SchemaKey, dataArr []string) *SelfDescribingData {
	bytesArr := make([]json.RawMessage, 0)
	for _, data := range dataArr {
		bytesArr = append(bytesArr, []byte(data))
	}
	return &SelfDescribingData{
		SchemaKey:   key,
		Data:        bytesArr,
		payloadType: payloadTypeArray,
	}
}

func (sdd *SelfDescribingData) Payload() string {
	if sdd.Data == nil {
		return ""
	}

	var bts []byte
	switch sdd.payloadType {
	case payloadTypeArray:
		bts, _ = json.Marshal(sdd.Data) //nolint:errcheck
	case payloadTypeMap, payloadTypeRaw:
		bts = sdd.Data[0]
	}
	return string(bts)
}

type SelfDescribingDataRaw struct {
	Schema string          `json:"schema" validate:"required"`
	Data   json.RawMessage `json:"data" validate:"required"`
}

func (sdd *SelfDescribingData) Parse(instance []byte) ParseError {
	if !json.Valid(instance) {
		return &InvalidJSON{}
	}

	var raw SelfDescribingDataRaw
	if err := json.Unmarshal(instance, &raw); err != nil {
		return &InvalidData{}
	}

	// validate fields
	validate := validator.New()
	if err := validate.Struct(raw); err != nil {
		return &InvalidData{}
	}

	if !json.Valid(raw.Data) {
		return &InvalidJSON{}
	}

	// parse schema
	key, err := NewSchemaKeyFromURI(raw.Schema)
	if err != nil {
		return &InvalidSchema{}
	}
	sdd.SchemaKey = key

	// parse data, account for top-level array construction
	sdd.Data = make([]json.RawMessage, 0)
	var arrData []interface{}
	if err := json.Unmarshal(raw.Data, &arrData); err == nil {
		sdd.payloadType = payloadTypeArray
		for _, d := range arrData {
			bts, _ := json.Marshal(d) //nolint:errcheck
			sdd.Data = append(sdd.Data, bts)
		}
		return nil
	}

	var mapData map[string]interface{}
	if err := json.Unmarshal(raw.Data, &mapData); err == nil {
		sdd.payloadType = payloadTypeMap
		sdd.Data = append(sdd.Data, raw.Data)
		return nil
	}

	sdd.payloadType = payloadTypeRaw
	sdd.Data = append(sdd.Data, raw.Data)
	return nil
}

func (sdd *SelfDescribingData) AsString() (string, error) {
	var raw SelfDescribingDataRaw
	raw.Schema = sdd.SchemaKey.AsString()
	raw.Data = json.RawMessage(sdd.Payload())

	bts, err := json.Marshal(raw)
	if err != nil {
		return "", fmt.Errorf("stringifying sdj: %w", err)
	}
	return string(bts), nil
}

//nolint:wrapcheck
func (sdd *SelfDescribingData) MarshalJSON() ([]byte, error) {
	type Alias SelfDescribingData
	return json.Marshal(&struct {
		SchemaKey string `json:"schema"`
		*Alias
	}{
		SchemaKey: sdd.SchemaKey.AsString(),
		Alias:     (*Alias)(sdd),
	})
}
