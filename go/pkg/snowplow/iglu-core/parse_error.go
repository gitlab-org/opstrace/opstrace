package iglucore

import "fmt"

type ParseError interface {
	Code() string
	Message(str string) string
}

var _ ParseError = (*InvalidSchemaVer)(nil)
var _ ParseError = (*InvalidIgluURI)(nil)
var _ ParseError = (*InvalidData)(nil)
var _ ParseError = (*InvalidJSON)(nil)
var _ ParseError = (*InvalidSchema)(nil)
var _ ParseError = (*InvalidMetaSchema)(nil)

type InvalidSchemaVer struct{}

func (v *InvalidSchemaVer) Code() string {
	return "INVALID_SCHEMAVER"
}

func (v *InvalidSchemaVer) Message(str string) string {
	return fmt.Sprintf("Invalid schema version: %s, code: %s", str, v.Code())
}

type InvalidIgluURI struct{}

func (v *InvalidIgluURI) Code() string {
	return "INVALID_IGLUURI"
}

func (v *InvalidIgluURI) Message(str string) string {
	return fmt.Sprintf("Invalid Iglu URI: %s, code: %s", str, v.Code())
}

type InvalidData struct{}

func (v *InvalidData) Code() string {
	return "INVALID_DATA_PAYLOAD"
}

func (v *InvalidData) Message(str string) string {
	return fmt.Sprintf("Invalid data payload: %s, code: %s", str, v.Code())
}

type InvalidJSON struct{}

func (v *InvalidJSON) Code() string {
	return "INVALID_JSON"
}

func (v *InvalidJSON) Message(str string) string {
	return fmt.Sprintf("Invalid json: %s, code: %s", str, v.Code())
}

type InvalidSchema struct{}

func (v *InvalidSchema) Code() string {
	return "INVALID_SCHEMA"
}

func (v *InvalidSchema) Message(str string) string {
	return fmt.Sprintf("Invalid schema: %s, code: %s", str, v.Code())
}

type InvalidMetaSchema struct{}

func (v *InvalidMetaSchema) Code() string {
	return "INVALID_METASCHEMA"
}

func (v *InvalidMetaSchema) Message(str string) string {
	return fmt.Sprintf("Invalid metaschema: %s, code: %s", str, v.Code())
}
