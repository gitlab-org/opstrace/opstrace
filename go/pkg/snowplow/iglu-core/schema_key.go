package iglucore

import (
	"fmt"
	"regexp"
)

var schemaURIRigidRegex = regexp.MustCompile("^iglu:" + // Protocol
	"([a-zA-Z0-9-_.]+)/" + // Vendor
	"([a-zA-Z0-9-_]+)/" + // Name
	"([a-zA-Z0-9-_]+)/" + // Format
	"([0-9]*(?:-(?:[0-9]*)){2})$") // SchemaVer

type SchemaKey struct {
	Vendor  string
	Name    string
	Format  string
	Version *SchemaVer
}

func NewSchemaKey() *SchemaKey {
	return &SchemaKey{}
}

func NewSchemaKeyFromParams(vendor, name, format string, version *SchemaVer) *SchemaKey {
	return &SchemaKey{
		Vendor:  vendor,
		Name:    name,
		Format:  format,
		Version: version,
	}
}

func NewSchemaKeyFromURI(uri string) (*SchemaKey, ParseError) {
	key := NewSchemaKey()
	if err := key.fromURI(uri); err != nil {
		return nil, err
	}
	return key, nil
}

func (key *SchemaKey) Apply(vendor, name, format string, version *SchemaVer) {
	key.Vendor = vendor
	key.Name = name
	key.Format = format
	key.Version = version
}

func (key *SchemaKey) AsPath() string {
	return fmt.Sprintf(
		"%s/%s/%s/%s-%s-%s",
		key.Vendor,
		key.Name,
		key.Format,
		schemaVerComponentToString(key.Version.Model),
		schemaVerComponentToString(key.Version.Revision),
		schemaVerComponentToString(key.Version.Addition),
	)
}

func (key *SchemaKey) AsString() string {
	return fmt.Sprintf(
		"iglu:%s/%s/%s/%s-%s-%s",
		key.Vendor,
		key.Name,
		key.Format,
		schemaVerComponentToString(key.Version.Model),
		schemaVerComponentToString(key.Version.Revision),
		schemaVerComponentToString(key.Version.Addition),
	)
}

func (key *SchemaKey) Equals(expectedKey *SchemaKey) bool {
	return key.Vendor == expectedKey.Vendor &&
		key.Name == expectedKey.Name &&
		key.Format == expectedKey.Format &&
		key.Version.Equals(expectedKey.Version)
}

func (key *SchemaKey) fromURI(uri string) ParseError {
	if schemaURIRigidRegex.Match([]byte(uri)) {
		comps := schemaURIRigidRegex.FindStringSubmatch(uri) // vnd, n, f, ver

		ver := NewSchemaVer()
		if err := ver.Parse(comps[4]); err != nil {
			return err
		}

		key.Vendor = comps[1]
		key.Name = comps[2]
		key.Format = comps[3]
		key.Version = ver

		return nil
	}
	return &InvalidIgluURI{}
}

func (key *SchemaKey) Extract(json string) error {
	return nil
}
