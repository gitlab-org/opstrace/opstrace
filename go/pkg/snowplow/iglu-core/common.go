package iglucore

import "strconv"

func parseSchemaVerComponent(a string) *int {
	v, err := strconv.Atoi(a)
	if err != nil {
		return nil
	}
	return &v
}

func schemaVerComponentToString(a *int) string {
	var model string
	if a != nil {
		model = strconv.Itoa(*a)
	} else {
		model = "*"
	}
	return model
}
