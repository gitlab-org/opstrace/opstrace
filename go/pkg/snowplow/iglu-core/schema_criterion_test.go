package iglucore

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/utils"
)

func TestParseSimpleCorrectCriterion(t *testing.T) {
	criterion := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/2-*-*"
	sc := NewSchemaCriterion()
	err := sc.Parse(criterion)
	require.NoError(t, err)

	assert.Equal(t, "com.snowplowanalytics.snowplow", sc.Vendor)
	assert.Equal(t, "mobile_context", sc.Name)
	assert.Equal(t, "jsonschema", sc.Format)
	assert.Equal(t, utils.IntPtr(2), sc.Model)
}

func TestParseCriterionWithoutSchemaVer(t *testing.T) {
	criterion := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/*-*-*"
	sc := NewSchemaCriterion()
	err := sc.Parse(criterion)
	require.NoError(t, err)

	assert.Equal(t, "com.snowplowanalytics.snowplow", sc.Vendor)
	assert.Equal(t, "mobile_context", sc.Name)
	assert.Equal(t, "jsonschema", sc.Format)
	assert.Nil(t, sc.Model)
	assert.Nil(t, sc.Revision)
	assert.Nil(t, sc.Addition)
}

func TestParseCriterionWithPartialSchemaVer(t *testing.T) {
	criterion := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/2-0-*"
	sc := NewSchemaCriterion()
	err := sc.Parse(criterion)
	require.NoError(t, err)

	assert.Equal(t, "com.snowplowanalytics.snowplow", sc.Vendor)
	assert.Equal(t, "mobile_context", sc.Name)
	assert.Equal(t, "jsonschema", sc.Format)
	assert.Equal(t, utils.IntPtr(2), sc.Model)
	assert.Equal(t, utils.IntPtr(0), sc.Revision)
	assert.Nil(t, sc.Addition)
}

func TestParseCriterionWithFullSchemaVer(t *testing.T) {
	criterion := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/1-0-0"
	sc := NewSchemaCriterion()
	err := sc.Parse(criterion)
	require.NoError(t, err)

	assert.Equal(t, "com.snowplowanalytics.snowplow", sc.Vendor)
	assert.Equal(t, "mobile_context", sc.Name)
	assert.Equal(t, "jsonschema", sc.Format)
	assert.Equal(t, utils.IntPtr(1), sc.Model)
	assert.Equal(t, utils.IntPtr(0), sc.Revision)
	assert.Equal(t, utils.IntPtr(0), sc.Addition)
}

func TestParseCriterionWithMissingAddition(t *testing.T) {
	criterion := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/*-*-"
	sc := NewSchemaCriterion()
	err := sc.Parse(criterion)
	if assert.Error(t, err) {
		assert.Equal(t, ErrInvalidSchemaCriterion, err)
	}
}

func TestParseCriterionWithZeroModel(t *testing.T) {
	criterion := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/0-*-*"
	sc := NewSchemaCriterion()
	err := sc.Parse(criterion)
	if assert.Error(t, err) {
		assert.Equal(t, ErrInvalidSchemaCriterion, err)
	}
}

func TestCriterionMatchByModel(t *testing.T) {
	sc := NewSchemaCriterionFromParams("com.snowplowanalytics.snowplow", "mobile_context", "jsonschema", utils.IntPtr(2), nil, nil)
	key := NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "mobile_context", "jsonschema", NewSchemaVerFull(2, 1, 0))
	assert.True(t, sc.Matches(key))
}

func TestCriterionMatchInitialSchema(t *testing.T) {
	sc := NewSchemaCriterionFromParams(
		"com.snowplowanalytics.snowplow",
		"mobile_context",
		"jsonschema",
		nil,
		utils.IntPtr(0),
		utils.IntPtr(0),
	)

	key := NewSchemaKeyFromParams(
		"com.snowplowanalytics.snowplow",
		"mobile_context",
		"jsonschema",
		NewSchemaVerFull(2, 0, 0),
	)

	assert.True(t, sc.Matches(key))
}

func TestCriterionNotMatchingKey(t *testing.T) {
	sc := NewSchemaCriterionFromParams(
		"com.snowplowanalytics.snowplow",
		"mobile_context",
		"jsonschema",
		nil,
		utils.IntPtr(0),
		utils.IntPtr(0),
	)

	key := NewSchemaKeyFromParams(
		"com.snowplowanalytics.snowplow",
		"mobile_context",
		"jsonschema",
		NewSchemaVerFull(2, 1, 0),
	)

	assert.False(t, sc.Matches(key))
}

func TestCriterionFilterCorrectEntries(t *testing.T) {
	keys := []*SchemaKey{
		NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "mobile_context", "jsonschema", NewSchemaVerFull(2, 1, 0)),
		NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "mobile_context", "jsonschema", NewSchemaVerFull(1, 0, 0)),
		NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "mobile_context", "avro", NewSchemaVerFull(2, 1, 0)),
		NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "mobile_context", "jsonschema", NewSchemaVerFull(2, 0, 0)),
		NewSchemaKeyFromParams("com.snowplowanalytics.snowplow", "mobile_context", "jsonschema", NewSchemaVerFull(1, 1, 1)),
	}

	sc := NewSchemaCriterionFromParams("com.snowplowanalytics.snowplow", "mobile_context", "jsonschema", nil, utils.IntPtr(0), utils.IntPtr(0))

	matched := sc.PickFrom(keys)
	assert.Len(t, matched, 2)
	assert.Equal(t, keys[1], matched[0])
	assert.Equal(t, keys[3], matched[1])
}
