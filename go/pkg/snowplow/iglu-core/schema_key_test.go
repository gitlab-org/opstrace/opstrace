package iglucore

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseSimpleKeyFromIgluURI(t *testing.T) {
	uri := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/1-0-0"
	key, err := NewSchemaKeyFromURI(uri)
	require.Nil(t, err)
	require.NotNil(t, key)
}

func TestParseComplexKeyFromIgluURI(t *testing.T) {
	uri := "iglu:uk.edu.acme.sub-division/second-event_complex/jsonschema/2-10-32"
	key, err := NewSchemaKeyFromURI(uri)
	require.Nil(t, err)
	require.NotNil(t, key)
}

func TestParseKeyWithPrecedingZero(t *testing.T) {
	uri := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/1-01-0"
	_, err := NewSchemaKeyFromURI(uri)
	if assert.NotNil(t, err) {
		assert.Equal(t, &InvalidSchemaVer{}, err)
	}
}

func TestParseKeyWithMissingRevision(t *testing.T) {
	uri := "iglu:com.snowplowanalytics.snowplow/mobile_context/jsonschema/1--0"
	_, err := NewSchemaKeyFromURI(uri)
	if assert.NotNil(t, err) {
		assert.Equal(t, &InvalidSchemaVer{}, err)
	}
}

func TestParseKeyWithInvalidName(t *testing.T) {
	uri := "iglu:com.snowplowanalytics.snowplow/mobile.context/jsonschema/1-2-0"
	_, err := NewSchemaKeyFromURI(uri)
	if assert.NotNil(t, err) {
		assert.Equal(t, &InvalidIgluURI{}, err)
	}
}

func TestParseKeyWithMissingAddition(t *testing.T) {
	uri := "iglu:com.snowplowanalytics.snowplow/mobile.context/jsonschema/1-2-"
	_, err := NewSchemaKeyFromURI(uri)
	if assert.NotNil(t, err) {
		assert.Equal(t, &InvalidIgluURI{}, err)
	}
}

func TestParseKeyWithPartialSchemaKey(t *testing.T) {
	uri := "iglu:com.snowplowanalytics.snowplow/mobile.context/jsonschema/1-2-?"
	_, err := NewSchemaKeyFromURI(uri)
	if assert.NotNil(t, err) {
		assert.Equal(t, &InvalidIgluURI{}, err)
	}
}

func TestParseKeyWithURLEncoding(t *testing.T) {
	uri := "iglu%3Auk.edu.acme.sub-division%2Fsecond-event_complex%2Fjsonschema%2F2-10-32"
	_, err := NewSchemaKeyFromURI(uri)
	if assert.NotNil(t, err) {
		assert.Equal(t, &InvalidIgluURI{}, err)
	}
}
