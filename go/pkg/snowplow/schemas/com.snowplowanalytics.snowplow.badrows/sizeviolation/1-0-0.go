package sizeviolation

import "time"

// Schema for a size violation bad row
type SizeViolation struct {
	// Information regarding the size violation
	Failure Failure `json:"failure"`

	// The truncated payload that resulted in a size violation
	Payload string `json:"payload"`

	// Information about the piece of software responsible for the creation of this
	// size violation
	Processor Processor `json:"processor"`
}

// Information regarding the size violation
type Failure struct {
	// Maximum payload size allowed by the current platform
	ActualSizeBytes int `json:"actualSizeBytes"`

	// Expectation which was not met
	Expectation string `json:"expectation,omitempty"`

	// Maximum payload size allowed by the current platform
	MaximumAllowedSizeBytes int `json:"maximumAllowedSizeBytes"`

	// Timestamp at which the failure occurred
	Timestamp time.Time `json:"timestamp"`
}

// Information about the piece of software responsible for the creation of this
// size violation
type Processor struct {
	// Artifact responsible for the creation of this size violation
	Artifact string `json:"artifact"`

	// Version of the artifact responsible for the creation of this size violation
	Version string `json:"version"`
}
