## Thrift generated code for Snowplow collector payload

Schema: https://github.com/snowplow/iglu-central/blob/master/schemas/com.snowplowanalytics.snowplow/CollectorPayload/thrift/1-0-0

Generated with:
```
thrift -gen go collector_payload.idl
```