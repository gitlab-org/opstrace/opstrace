package instrumentation

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/collectors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	jaeger_propagator "go.opentelemetry.io/contrib/propagators/jaeger"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	promexp "go.opentelemetry.io/otel/exporters/prometheus"
	"go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/propagation"
	metricsdk "go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	tracesdk "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.26.0"
	"go.opentelemetry.io/otel/trace"
	"go.opentelemetry.io/otel/trace/noop"
)

const (
	ScopeName = "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	tracerKey = "otel-gitlab-monitor-observability"
)

func NewDefaultMeter(serviceName string, mp *metricsdk.MeterProvider) metric.Meter {
	return mp.Meter(
		serviceName,
		metric.WithInstrumentationVersion(constants.DockerImageTag),
	)
}

func NewSubSpan(ctx context.Context, spanName string, spanKind trace.SpanKind) (context.Context, trace.Span) {
	span := trace.SpanFromContext(ctx)
	tracer := span.TracerProvider().Tracer(
		ScopeName,
		trace.WithInstrumentationVersion(constants.DockerImageTag),
	)
	newCtx, newSpan := tracer.Start(ctx, spanName, trace.WithSpanKind(spanKind))
	return newCtx, newSpan
}

func ConstructPromMeterTooling() (*prometheus.Registry, *http.ServeMux) {
	registry := prometheus.NewPedanticRegistry()
	registry.MustRegister(collectors.NewProcessCollector(collectors.ProcessCollectorOpts{}))
	registry.MustRegister(collectors.NewGoCollector())
	handler := promhttp.HandlerFor(registry, promhttp.HandlerOpts{
		ErrorHandling:     promhttp.HTTPErrorOnError,
		EnableOpenMetrics: true,
	})
	router := http.NewServeMux()
	router.Handle("/metrics", handler)
	return registry, router
}

func ConstructOTELResource(ctx context.Context, serviceName, version string) (*resource.Resource, error) {
	res, err := resource.New(
		ctx,
		resource.WithSchemaURL(semconv.SchemaURL),
		resource.WithAttributes(
			semconv.ServiceNameKey.String(serviceName),
			semconv.ServiceVersionKey.String(version),
		),
		resource.WithProcessRuntimeDescription(),
		resource.WithTelemetrySDK(),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create otel resource: %w", err)
	}
	return res, nil
}

func ConstructOTELMeterProvider(
	ctx context.Context,
	r *resource.Resource,
	reg prometheus.Registerer,
	otlpEndpoint, otlpCACertificate, otlpTokenSecretFile string,
) (*metricsdk.MeterProvider, func() error, error) {
	promExporter, err := promexp.New(promexp.WithRegisterer(reg))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create otel prometheus exporter: %w", err)
	}

	var mp *metricsdk.MeterProvider
	// Readers cannot be added after a MeterProvider is created, hence we
	// have two different NewMeterProvider calls
	if otlpEndpoint != "" {
		u, err := url.Parse(otlpEndpoint)
		if err != nil {
			return nil, nil, fmt.Errorf("parsing otel collector url %q failed: %w", otlpEndpoint, err)
		}

		var otelExporter metricsdk.Exporter
		switch u.Scheme {
		case "https":
			otelExporter, err = newHTTPSMetricsExporter(ctx, u, otlpCACertificate, otlpTokenSecretFile)
			if err != nil {
				return nil, nil, fmt.Errorf("unable to create new otel https metrics exporter: %w", err)
			}
		case "http":
			otelExporter, err = newHTTPMetricsExporter(ctx, u, otlpTokenSecretFile)
			if err != nil {
				return nil, nil, fmt.Errorf("unable to create new otel http metrics exporter: %w", err)
			}
		default:
			return nil, nil, fmt.Errorf("unsupported schema of metrics url %q, only `http` and `https` are supported", u.Scheme)
		}
		otelPeriodicExporter := metricsdk.NewPeriodicReader(
			otelExporter,
			metricsdk.WithInterval(30*time.Second),
		)

		mp = metricsdk.NewMeterProvider(
			metricsdk.WithResource(r),
			metricsdk.WithReader(promExporter),
			metricsdk.WithReader(otelPeriodicExporter),
		)
	} else {
		// Construct only via prometheus endpoint/format
		mp = metricsdk.NewMeterProvider(
			metricsdk.WithResource(r),
			metricsdk.WithReader(promExporter),
		)
	}

	//nolint:contextcheck // graceful shutdown
	mpStop := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		err := mp.Shutdown(ctx)
		if err != nil {
			return fmt.Errorf("meter-provider shutdown failed: %w", err)
		}
		return nil
	}
	return mp, mpStop, nil
}

func newHTTPSMetricsExporter(
	ctx context.Context,
	otlpEndpoint *url.URL,
	otlpCACertificate, otlpTokenSecretFile string,
) (metricsdk.Exporter, error) {
	opts := []otlpmetrichttp.Option{
		otlpmetrichttp.WithEndpoint(otlpEndpoint.Host),
		otlpmetrichttp.WithURLPath(otlpEndpoint.Path + "/v1/metrics"),
		otlpmetrichttp.WithTimeout(10 * time.Second),
		otlpmetrichttp.WithRetry(otlpmetrichttp.RetryConfig{
			Enabled:         true,
			InitialInterval: 1 * time.Second,
			MaxInterval:     10 * time.Second,
			MaxElapsedTime:  240 * time.Second,
		}),
	}

	headers, err := clientHTTPHeadersConfig(otlpTokenSecretFile)
	if err != nil {
		return nil, fmt.Errorf("unable to determine http headers config: %w", err)
	}
	opts = append(opts, otlpmetrichttp.WithHeaders(headers))

	tlsConfig, err := clientTLSConfig(otlpCACertificate)
	if err != nil {
		return nil, fmt.Errorf("unable to determine TLS config: %w", err)
	}
	opts = append(opts, otlpmetrichttp.WithTLSClientConfig(tlsConfig))

	me, err := otlpmetrichttp.New(ctx, opts...)
	if err != nil {
		return nil, fmt.Errorf("creating otel-metrics https exporter failed: %w", err)
	}
	return me, nil
}

func newHTTPMetricsExporter(
	ctx context.Context, otlpEndpoint *url.URL, otlpTokenSecretFile string,
) (metricsdk.Exporter, error) {
	opts := []otlpmetrichttp.Option{
		otlpmetrichttp.WithEndpoint(otlpEndpoint.Host),
		otlpmetrichttp.WithURLPath(otlpEndpoint.Path + "/v1/metrics"),
		otlpmetrichttp.WithInsecure(),
		otlpmetrichttp.WithTimeout(10 * time.Second),
		otlpmetrichttp.WithRetry(otlpmetrichttp.RetryConfig{
			Enabled:         true,
			InitialInterval: 1 * time.Second,
			MaxInterval:     10 * time.Second,
			MaxElapsedTime:  240 * time.Second,
		}),
	}

	headers, err := clientHTTPHeadersConfig(otlpTokenSecretFile)
	if err != nil {
		return nil, fmt.Errorf("unable to determine http headers config: %w", err)
	}
	opts = append(opts, otlpmetrichttp.WithHeaders(headers))

	me, err := otlpmetrichttp.New(ctx, opts...)
	if err != nil {
		return nil, fmt.Errorf("creating otel-metrics http exporter failed: %w", err)
	}
	return me, nil
}

func ConstructOTELTracingTools(
	ctx context.Context,
	r *resource.Resource,
	otlpEndpoint, otlpCACertificateFile, otlpTokenSecretFile string,
) (
	trace.TracerProvider,
	propagation.TextMapPropagator,
	func() error,
	error,
) {
	if otlpEndpoint == "" {
		return noop.NewTracerProvider(),
			propagation.NewCompositeTextMapPropagator(),
			func() error { return nil },
			nil
	}

	// Exporter must be constructed right before TracerProvider as it's started
	// implicitly so needs to be stopped, which TracerProvider does in its
	// Shutdown() method.
	exporter, err := constructTracingExporter(
		ctx,
		otlpEndpoint, otlpCACertificateFile, otlpTokenSecretFile,
	)
	if err != nil {
		return nil, nil, nil, err
	}

	traceProvider := tracesdk.NewTracerProvider(
		tracesdk.WithResource(r),
		tracesdk.WithBatcher(
			exporter,
			tracesdk.WithBatchTimeout(3*time.Second),
			tracesdk.WithMaxExportBatchSize(2048),
			tracesdk.WithMaxQueueSize(2048),
		),
		tracesdk.WithRawSpanLimits(tracesdk.SpanLimits{
			// Avoid misuse of attributes.
			AttributeValueLengthLimit: 3 * 1024, // 3KB
			// Based on the default values from the OpenTelemetry specification.
			AttributeCountLimit:         tracesdk.DefaultAttributeCountLimit,
			EventCountLimit:             tracesdk.DefaultEventCountLimit,
			LinkCountLimit:              tracesdk.DefaultLinkCountLimit,
			AttributePerEventCountLimit: tracesdk.DefaultEventCountLimit,
			AttributePerLinkCountLimit:  tracesdk.DefaultAttributePerLinkCountLimit,
		}),
	)
	// Apart form  W3C Trace Context, W3C Baggage, we need to support Jaeger
	// propagator as well, until we can migrate to Traefik v3.
	propagator := propagation.NewCompositeTextMapPropagator(
		jaeger_propagator.Jaeger{},
		propagation.TraceContext{},
		propagation.Baggage{},
	)
	//nolint:contextcheck // graceful shutdown
	traceProviderDoneF := func() error {
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		err := traceProvider.Shutdown(ctx)
		if err != nil {
			return fmt.Errorf("trace-provider shutdown failed: %w", err)
		}
		return nil
	}
	return traceProvider, propagator, traceProviderDoneF, nil
}

func constructTracingExporter(
	ctx context.Context,
	otlpEndpoint, otlpCACertificateFile, otlpTokenSecretFile string,
) (tracesdk.SpanExporter, error) {
	u, err := url.Parse(otlpEndpoint)
	if err != nil {
		return nil, fmt.Errorf("parsing tracing url %s failed: %w", otlpEndpoint, err)
	}

	switch u.Scheme {
	case "https":
		return newHTTPSTracingExporter(ctx, u, otlpCACertificateFile, otlpTokenSecretFile)
	case "http":
		return newHTTPTracingExporter(ctx, u, otlpTokenSecretFile)
	default:
		return nil, fmt.Errorf("unsupported schema of metrics url %q, only `http` and `https` are supported", u.Scheme)
	}
}

func newHTTPSTracingExporter(
	ctx context.Context,
	otlpEndpoint *url.URL,
	otlpCACertificateFile, otlpTokenSecretFile string,
) (tracesdk.SpanExporter, error) {
	opts := []otlptracehttp.Option{
		otlptracehttp.WithEndpoint(otlpEndpoint.Host),
		otlptracehttp.WithURLPath(otlpEndpoint.Path + "/v1/traces"),
		otlptracehttp.WithTimeout(10 * time.Second),
		otlptracehttp.WithRetry(otlptracehttp.RetryConfig{
			Enabled:         true,
			InitialInterval: 1 * time.Second,
			MaxInterval:     10 * time.Second,
			MaxElapsedTime:  240 * time.Second,
		}),
	}

	headers, err := clientHTTPHeadersConfig(otlpTokenSecretFile)
	if err != nil {
		return nil, fmt.Errorf("unable to determine http headers config: %w", err)
	}
	opts = append(opts, otlptracehttp.WithHeaders(headers))

	tlsConfig, err := clientTLSConfig(otlpCACertificateFile)
	if err != nil {
		return nil, fmt.Errorf("unable to determine TLS config: %w", err)
	}
	opts = append(opts, otlptracehttp.WithTLSClientConfig(tlsConfig))

	te, err := otlptracehttp.New(ctx, opts...)
	if err != nil {
		return nil, fmt.Errorf("creating https trace-exporter failed: %w", err)
	}
	return te, nil
}

func newHTTPTracingExporter(
	ctx context.Context,
	otlpEndpoint *url.URL,
	otlpTokenSecretFile string,
) (tracesdk.SpanExporter, error) {
	opts := []otlptracehttp.Option{
		otlptracehttp.WithEndpoint(otlpEndpoint.Host),
		otlptracehttp.WithURLPath(otlpEndpoint.Path + "/v1/traces"),
		otlptracehttp.WithInsecure(),
		otlptracehttp.WithTimeout(10 * time.Second),
		otlptracehttp.WithRetry(otlptracehttp.RetryConfig{
			Enabled:         true,
			InitialInterval: 1 * time.Second,
			MaxInterval:     10 * time.Second,
			MaxElapsedTime:  240 * time.Second,
		}),
	}

	headers, err := clientHTTPHeadersConfig(otlpTokenSecretFile)
	if err != nil {
		return nil, fmt.Errorf("unable to determine http headers config: %w", err)
	}
	opts = append(opts, otlptracehttp.WithHeaders(headers))

	te, err := otlptracehttp.New(ctx, opts...)
	if err != nil {
		return nil, fmt.Errorf("creating http trace-exporter failed: %w", err)
	}
	return te, nil
}

func clientHTTPHeadersConfig(tokenSecretFile string) (map[string]string, error) {
	headers := make(map[string]string)

	if tokenSecretFile != "" {
		token, err := os.ReadFile(tokenSecretFile)
		if err != nil {
			return nil, fmt.Errorf("unable to read OTLP token from %q: %w", tokenSecretFile, err)
		}
		token = bytes.TrimSpace(token)
		headers["Private-Token"] = string(token)
	}

	return headers, nil
}

func clientTLSConfig(caCertFile string) (*tls.Config, error) {
	tlsConfig := &tls.Config{
		CipherSuites: secureCipherSuites(),
		MinVersion:   tls.VersionTLS12,
	}
	if caCertFile != "" {
		certPool, err := loadCACert(caCertFile)
		if err != nil {
			return nil, fmt.Errorf("unable to load CA certificate file: %w", err)
		}
		tlsConfig.RootCAs = certPool
	}
	return tlsConfig, nil
}

func loadCACert(caCertFile string) (*x509.CertPool, error) {
	certPool, err := x509.SystemCertPool()
	if err != nil {
		return nil, fmt.Errorf("SystemCertPool: %w", err)
	}
	caCert, err := os.ReadFile(caCertFile)
	if err != nil {
		return nil, fmt.Errorf("CA certificate file: %w", err)
	}
	ok := certPool.AppendCertsFromPEM(caCert)
	if !ok {
		return nil, fmt.Errorf("AppendCertsFromPEM(%s) failed", caCertFile)
	}
	return certPool, nil
}

// https://ssl-config.mozilla.org/#server=go&version=1.21.0&config=intermediate&guideline=5.7
func secureCipherSuites() []uint16 {
	return []uint16{
		tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
		tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
		tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
		tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
	}
}
