package nats

import (
	"fmt"
	"time"

	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
)

func NewLocalServer(opts *server.Options, timeout time.Duration) (*server.Server, error) {
	ns, err := server.NewServer(opts)
	if err != nil {
		return nil, fmt.Errorf("instantiating nats server: %w", err)
	}
	go ns.Start()
	if !ns.ReadyForConnections(timeout) {
		return nil, fmt.Errorf("server not ready")
	}
	return ns, nil
}

func NewJetstream(addr string) (jetstream.JetStream, error) {
	nc, err := nats.Connect(addr)
	if err != nil {
		return nil, fmt.Errorf("connecting to NATS: %w", err)
	}

	js, err := jetstream.New(nc)
	if err != nil {
		return nil, fmt.Errorf("creating jetstream: %w", err)
	}

	return js, nil
}
