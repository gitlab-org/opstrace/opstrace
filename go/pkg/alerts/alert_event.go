package alerts

import "time"

type AlertEventType int

const (
	TraceRateLimitEvent AlertEventType = iota + 1
	MetricRateLimitEvent
	LogRateLimitEvent
	AuthLimitEvent
)

func (e AlertEventType) String() string {
	switch e {
	case TraceRateLimitEvent:
		return "TraceRateLimitEvent"
	case MetricRateLimitEvent:
		return "MetricRateLimitEvent"
	case LogRateLimitEvent:
		return "LogRateLimitEvent"
	case AuthLimitEvent:
		return "AuthLimitEvent"
	default:
		return ""
	}
}

type AlertEvent struct {
	ProjectID   string         `json:"project_id"`
	TenantID    string         `json:"tenant_id"`
	Description string         `json:"description"`
	Timestamp   time.Time      `json:"timestamp"`
	Reason      string         `json:"reason"`
	Type        AlertEventType `json:"type"`
	IsSaaSEvent int8           `json:"is_saas_event"`
}
