package alerts_test

import (
	"context"
	"testing"

	"github.com/ClickHouse/clickhouse-go/v2"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var logger *zap.SugaredLogger
var testEnv *testutils.ClickHouseServer
var conn clickhouse.Conn

func TestAlertWriter(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "Alert Writer Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func(ctx SpecContext) {
	config := zap.NewDevelopmentConfig()
	sink := zapcore.AddSync(GinkgoWriter)
	encoder := zapcore.NewConsoleEncoder(config.EncoderConfig)
	core := zapcore.NewCore(encoder, sink, zap.DebugLevel)
	log := zap.New(core).WithOptions(zap.ErrorOutput(sink))
	logger = log.Sugar()

	var err error
	testEnv, conn, err = testutils.NewClickHouseServerAndConnection(context.Background())
	Expect(err).ToNot(HaveOccurred())
	DeferCleanup(func(ctx SpecContext) {
		_ = testEnv.Terminate(ctx)
	})

	Expect(testEnv.CreateAllDatabasesAndRunMigrations(ctx)).To(Succeed())
})

var _ = AfterSuite(func() {
	err := logger.Sync()
	Expect(err).ToNot(HaveOccurred())
})
