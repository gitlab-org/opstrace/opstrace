//go:generate go run github.com/vektra/mockery/v2  --name AlertWriter --dir . --structname AlertWriterMock --output ./test/ --outpkg test --filename alert_writer.go
package alerts

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"go.uber.org/zap"
)

const writeTimout = 5 * time.Second

type AlertWriter interface {
	WriteAlertEvent(ctx context.Context, alert *AlertEvent) error
}

// NowFunc is only a helper to assist with testing timestamp on generated alert events.
// For advanced mocking of time related functions, a dedicated library will be more suitable.
type NowFunc func() time.Time

type writer struct {
	db     clickhouse.Conn
	logger *zap.Logger
	now    NowFunc
}

const insertAlertEvent = `
INSERT INTO {database:Identifier}.{table:Identifier} (
  TenantId,
  ProjectId,
  Timestamp,
  AlertType,
  Description,
  Reason,
  IsSaaS
) VALUES (
  '%s',
  '%s',
  '%s',
  '%s',
  '%s',
  '%s',
  '%s'
)
`

func (w *writer) WriteAlertEvent(ctx context.Context, alert *AlertEvent) error {
	if alert.Timestamp.IsZero() {
		alert.Timestamp = w.now()
	}

	newCtx, cancel := context.WithTimeout(ctx, writeTimout)
	defer cancel()
	chCtx := clickhouse.Context(newCtx, clickhouse.WithParameters(clickhouse.Parameters{
		"database": constants.AlertsDatabaseName,
		"table":    constants.AlertEventsTableName,
		"tId":      alert.TenantID,
		"pId":      alert.ProjectID,
		"ts":       strconv.FormatInt(alert.Timestamp.UnixNano(), 10),
		"aT":       alert.Type.String(),
		"desc":     alert.Description,
		"r":        alert.Reason,
	}))
	err := w.db.AsyncInsert(
		chCtx,
		fmt.Sprintf(
			insertAlertEvent,
			alert.TenantID,
			alert.ProjectID,
			strconv.FormatInt(alert.Timestamp.UnixNano(), 10),
			alert.Type.String(),
			alert.Description,
			alert.Reason,
			strconv.Itoa(int(alert.IsSaaSEvent)),
		),
		true, // wait for ack from db server
	)
	if err != nil {
		return fmt.Errorf("writing alert event: %w", err)
	}
	return nil
}

func NewAlertWriter(db clickhouse.Conn, logger *zap.Logger) AlertWriter {
	return &writer{
		db:     db,
		logger: logger,
		now:    time.Now,
	}
}
