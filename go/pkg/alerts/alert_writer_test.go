package alerts_test

import (
	"context"
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
)

const alertQuery = `
SELECT TenantId, ProjectId, AggTimestamp, AlertType
FROM %s.%s
GROUP BY TenantId, ProjectId, AggTimestamp, AlertType
`

type AlertResult struct {
	TenantID     string    `ch:"TenantId"`
	ProjectID    string    `ch:"ProjectId"`
	AggTimestamp time.Time `ch:"AggTimestamp"`
	AlertType    string    `ch:"AlertType"`
}

var _ = Describe("AlertWriter", func() {

	var (
		writer alerts.AlertWriter
		ctx    context.Context
		cancel context.CancelFunc
		err    error
	)

	BeforeEach(func() {
		writer = alerts.NewAlertWriter(conn, logger.Desugar())
	})

	Context("for writing", func() {
		It("alert events", func() {
			ctx, cancel = context.WithTimeout(context.Background(), time.Second*5)
			defer cancel()
			alertEvent := &alerts.AlertEvent{
				ProjectID:   "1",
				TenantID:    "1",
				Description: "description",
				Timestamp:   time.Now(),
				Reason:      "reason",
				Type:        alerts.TraceRateLimitEvent,
				IsSaaSEvent: 1,
			}
			err = writer.WriteAlertEvent(ctx, alertEvent)
			Expect(err).ToNot(HaveOccurred())

			alertEvent = &alerts.AlertEvent{
				ProjectID:   "1",
				TenantID:    "1",
				Description: "description",
				Timestamp:   time.Now(),
				Reason:      "reason",
				Type:        alerts.TraceRateLimitEvent,
				IsSaaSEvent: 1,
			}
			err = writer.WriteAlertEvent(ctx, alertEvent)
			Expect(err).ToNot(HaveOccurred())

			var res []AlertResult
			err = conn.Select(
				ctx,
				&res,
				fmt.Sprintf(alertQuery,
					constants.AlertsDatabaseName,
					constants.AlertsTableName,
				),
			)
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(1))
		})
	})
})
