// Code generated by mockery v2.36.0. DO NOT EDIT.

package test

import (
	context "context"

	alerts "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"

	mock "github.com/stretchr/testify/mock"
)

// AlertWriterMock is an autogenerated mock type for the AlertWriter type
type AlertWriterMock struct {
	mock.Mock
}

// WriteAlertEvent provides a mock function with given fields: ctx, alert
func (_m *AlertWriterMock) WriteAlertEvent(ctx context.Context, alert *alerts.AlertEvent) error {
	ret := _m.Called(ctx, alert)

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, *alerts.AlertEvent) error); ok {
		r0 = rf(ctx, alert)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// NewAlertWriterMock creates a new instance of AlertWriterMock. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewAlertWriterMock(t interface {
	mock.TestingT
	Cleanup(func())
}) *AlertWriterMock {
	mock := &AlertWriterMock{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
