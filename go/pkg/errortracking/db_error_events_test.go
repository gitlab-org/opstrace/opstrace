package errortracking

import (
	"fmt"
	"testing"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
)

func TestBuildListEventsQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.ListEventsParams
		expected string
		args     clickhouse.Parameters
		err      error
	}{
		{
			name: "should build a simple query",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(20),
				Cursor:      nil,
				Sort:        stringPointer("occurred_at_desc"),
			},
			expected: `
SELECT project_id, fingerprint, name, actor, description, payload, platform, environment
FROM {errorEventsTable:Identifier} WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
ORDER BY occurred_at DESC LIMIT {lim:Int64} OFFSET {oft:Int64}`,
			args: clickhouse.Parameters{
				"errorEventsTable": constants.ErrorTrackingEventsTableName,
				"projectId":        "1",
				"fingerprint":      "1",
				"lim":              "20",
				"oft":              "0",
			},
			err: nil,
		},
		{
			name: "should build a query with custom sort",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(20),
				Cursor:      nil,
				Sort:        stringPointer("occurred_at_asc"),
			},
			expected: `
SELECT project_id, fingerprint, name, actor, description, payload, platform, environment
FROM {errorEventsTable:Identifier} WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
ORDER BY occurred_at ASC LIMIT {lim:Int64} OFFSET {oft:Int64}`,
			args: clickhouse.Parameters{
				"errorEventsTable": constants.ErrorTrackingEventsTableName,
				"projectId":        "1",
				"fingerprint":      "1",
				"lim":              "20",
				"oft":              "0",
			},
			err: nil,
		},
		{
			name: "should fail with invalid custom cursor parameter",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(10),
				Cursor:      stringPointer("foobar"),
				Sort:        stringPointer("occurred_at_desc"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected end of JSON input"),
		},
		{
			name: "should not fail with valid custom cursor parameter",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(9),
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: `
SELECT project_id, fingerprint, name, actor, description, payload, platform, environment
FROM {errorEventsTable:Identifier} WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
ORDER BY occurred_at DESC LIMIT {lim:Int64} OFFSET {oft:Int64}`,
			args: clickhouse.Parameters{
				"errorEventsTable": constants.ErrorTrackingEventsTableName,
				"projectId":        "1",
				"fingerprint":      "1",
				"lim":              "9",
				"oft":              "18",
			},
			err: nil,
		},
		{
			name: "should not fail with custom cursor parameter without page field",
			params: errors.ListEventsParams{
				ProjectID:   1,
				Fingerprint: 1,
				Limit:       int64Pointer(7),
				// cursor is the base64 encoded json string: {"foo":"bar"}
				Cursor: stringPointer("eyJmb28iOiJiYXIifQo="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: `
SELECT project_id, fingerprint, name, actor, description, payload, platform, environment
FROM {errorEventsTable:Identifier} WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
ORDER BY occurred_at DESC LIMIT {lim:Int64} OFFSET {oft:Int64}`,
			args: clickhouse.Parameters{
				"errorEventsTable": constants.ErrorTrackingEventsTableName,
				"projectId":        "1",
				"fingerprint":      "1",
				"lim":              "7",
				"oft":              "0",
			},
			err: nil,
		},
		{
			name: "should fail with custom cursor parameter with invalid json",
			params: errors.ListEventsParams{
				ProjectID: 1,
				Limit:     int64Pointer(7),
				// cursor is the base64 encoded json string: "foobar"
				Cursor: stringPointer("Zm9vYmFyCg=="),
				Sort:   stringPointer("occurred_at_desc"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("invalid character"),
		},
	} {
		t.Log(tc.name)
		qb, err := buildListEventsQuery(tc.params)
		if tc.err != nil {
			assert.Error(t, err)
			assert.Nil(t, qb)
			continue
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, qb.SQL())
		assert.EqualValues(t, tc.args, qb.StringifyParams())
	}
}

func TestDisambiguateEventSource(t *testing.T) {
	assert := assert.New(t)
	tests := []struct {
		payload string
	}{
		// documented example
		{
			payload: `{"event_id":"9ec79c33ec9942ab8353589fcb2e04dc","dsn":"https://e12d836b15bb49d7bbf99e64295d995b:@sentry.io/42"}
{"type":"attachment","length":10,"content_type":"text/plain","filename":"hello.txt"}
\xef\xbb\xbfHello\r\n
{"type":"event","length":41,"content_type":"application/json","filename":"application.log"}
{"message":"hello world","level":"error"}`,
		},
		// documented example
		{
			payload: "{\"event_id\":\"9ec79c33ec9942ab8353589fcb2e04dc\"}\n{\"type\":\"attachment\",\"length\":0}\n\n{\"type\":\"attachment\",\"length\":0}\n\n",
		},
		// documented example
		{
			payload: "{\"event_id\":\"9ec79c33ec9942ab8353589fcb2e04dc\"}\n{\"type\":\"attachment\"}\nhelloworld",
		},
		// documented example
		{
			payload: `{}
{"type":"session"}
{"started": "2020-02-07T14:16:00Z","attrs":{"release":"sentry-test@1.0.0"}}`,
		},
		// rust-generated envelope
		{
			payload: "{\"event_id\":\"6871f9e0-8c2f-4f4c-925b-2ca79fe623c4\"}\n{\"type\":\"event\",\"length\":4788}\n{\"event_id\":\"6871f9e08c2f4f4c925b2ca79fe623c4\",\"level\":\"fatal\",\"platform\":\"native\",\"timestamp\":1681225569.186447,\"server_name\":\"192.168.1.152\",\"release\":\"test-sentry-project@0.1.0\",\"environment\":\"development\",\"contexts\":{\"device\":{\"type\":\"device\",\"family\":\"MacBookPro\",\"model\":\"MacBookPro18,2\",\"arch\":\"aarch64\"},\"os\":{\"type\":\"os\",\"name\":\"Darwin\",\"version\":\"22.4.0\",\"kernel_version\":\"Darwin Kernel Version 22.4.0: Mon Mar  6 20:59:28 PST 2023; root:xnu-8796.101.5~3/RELEASE_ARM64_T6000\"},\"rust\":{\"type\":\"runtime\",\"name\":\"rustc\",\"version\":\"1.65.0\",\"channel\":\"stable\"}},\"exception\":{\"values\":[{\"type\":\"panic\",\"value\":\"test panic\",\"stacktrace\":{\"frames\":[{\"function\":\"_main\",\"in_app\":true,\"instruction_addr\":\"0x10213fc98\"},{\"function\":\"std::rt::lang_start\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":165,\"in_app\":false,\"instruction_addr\":\"0x102140994\"},{\"function\":\"std::rt::lang_start_internal\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":148,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panic::catch_unwind\",\"package\":\"std\",\"filename\":\"panic.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panic.rs\",\"lineno\":137,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":456,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try::do_call\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":492,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::rt::lang_start_internal::{{closure}}\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":148,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panic::catch_unwind\",\"package\":\"std\",\"filename\":\"panic.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panic.rs\",\"lineno\":137,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":456,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::panicking::try::do_call\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":492,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"core::ops::function::impls::<impl core::ops::function::FnOnce<A> for &F>::call_once\",\"package\":\"core\",\"filename\":\"function.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/core/src/ops/function.rs\",\"lineno\":283,\"in_app\":false,\"instruction_addr\":\"0x102876ee0\"},{\"function\":\"std::rt::lang_start::{{closure}}\",\"package\":\"std\",\"filename\":\"rt.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/rt.rs\",\"lineno\":166,\"in_app\":false,\"instruction_addr\":\"0x1021409cc\"},{\"function\":\"std::sys_common::backtrace::__rust_begin_short_backtrace\",\"package\":\"std\",\"filename\":\"backtrace.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/sys_common/backtrace.rs\",\"lineno\":122,\"in_app\":false,\"instruction_addr\":\"0x102142128\"},{\"function\":\"core::ops::function::FnOnce::call_once\",\"package\":\"core\",\"filename\":\"function.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/core/src/ops/function.rs\",\"lineno\":248,\"in_app\":false,\"instruction_addr\":\"0x102140a70\"},{\"function\":\"test_sentry_project::main\",\"package\":\"test_sentry_project\",\"filename\":\"main.rs\",\"abs_path\":\"/Users/drossetti/Developer/test-sentry-project/src/main.rs\",\"lineno\":36,\"in_app\":true,\"instruction_addr\":\"0x10213fc6c\"},{\"function\":\"std::panicking::begin_panic\",\"package\":\"std\",\"filename\":\"panicking.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/panicking.rs\",\"lineno\":616,\"in_app\":false,\"instruction_addr\":\"0x1028a3c4c\"},{\"function\":\"std::sys_common::backtrace::__rust_end_short_backtrace\",\"package\":\"std\",\"filename\":\"backtrace.rs\",\"abs_path\":\"/rustc/897e37553bba8b42751c67658967889d11ecd120/library/std/src/sys_common/backtrace.rs\",\"lineno\":138,\"in_app\":false,\"instruction_addr\":\"0x1025dec7c\"}]},\"mechanism\":{\"type\":\"panic\",\"handled\":false}}]},\"sdk\":{\"name\":\"sentry.rust\",\"version\":\"0.23.0\",\"integrations\":[\"attach-stacktrace\",\"contexts\",\"panic\",\"process-stacktrace\"],\"packages\":[{\"name\":\"cargo:sentry\",\"version\":\"0.23.0\"}]}}\n",
		},
		// non-envelope
		{
			payload: `{"contexts":{"device":{"arch":"amd64","num_cpu":2},"os":{"name":"linux"},"runtime":{"go_maxprocs":2,"go_numcgocalls":0,"go_numroutines":2,"name":"go","version":"go1.18.6"}},"event_id":"3b83a832a27e45b6bb6827704b3743a9","level":"error","platform":"go","sdk":{"name":"sentry.go","version":"0.13.0","integrations":["ContextifyFrames","Environment","IgnoreErrors","Modules"],"packages":[{"name":"sentry-go","version":"0.13.0"}]},"server_name":"6ed22a4deafc","user":{},"modules":{"":"","github.com/getsentry/sentry-go":"v0.13.0","golang.org/x/sys":"v0.0.0-20211007075335-d3039528d8ac"},"exception":[{"type":"*errors.errorString","value":"unsupported protocol scheme \"\""},{"type":"*url.Error","value":"Get \"fake-url\": unsupported protocol scheme \"\"","stacktrace":{"frames":[{"function":"main","module":"main","abs_path":"/go/src/gitlab.com/opstrace/error-tracking-test/go/main.go","lineno":66,"in_app":true}]}}],"timestamp":"2022-09-28T23:47:27.761226727Z"}`,
		},
	}
	for _, test := range tests {
		endpoint, sentryItem, err := DisambiguateEventSource([]byte(test.payload))
		if err != nil {
			t.Log(err)
		}
		// t.Log(sentryItem)
		if endpoint == "store" {
			event, ok := sentryItem.(*types.Event)
			assert.True(ok)
			assert.NotNil(event)
		}
		if endpoint == "envelope" {
			envelope, ok := sentryItem.(*types.Envelope)
			assert.True(ok)
			assert.NotNil(envelope)
		}
	}
}
