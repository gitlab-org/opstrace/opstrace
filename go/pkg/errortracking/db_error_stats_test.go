package errortracking

import (
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

func TestBuildTimeSeriesQuery(t *testing.T) {
	refTime := time.Now()
	for _, tc := range []struct {
		name          string
		statsPeriod   string
		expectedQuery string
		expectedArgs  clickhouse.Parameters
	}{
		{
			name:        "building a time window for the last 15m",
			statsPeriod: "15m",
			expectedQuery: `
WITH
    toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*15*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(time.Minute).UnixNano(), 10),
				"p2": "60",
			},
		},
		{
			name:        "building a time window for the last 30m",
			statsPeriod: "30m",
			expectedQuery: `
WITH
    toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*30*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(time.Minute).UnixNano(), 10),
				"p2": "60",
			},
		},
		{
			name:        "building a time window for the last 1h",
			statsPeriod: "1h",
			expectedQuery: `
WITH
    toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(time.Minute).UnixNano(), 10),
				"p2": "60",
			},
		},
		{
			name:        "building a time window for the last 24h",
			statsPeriod: "24h",
			expectedQuery: `
WITH
    toStartOfHour({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfHour({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*24*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(time.Hour).UnixNano(), 10),
				"p2": "3600",
			},
		},
		{
			name:        "building a time window for the last 7d",
			statsPeriod: "7d",
			expectedQuery: `
WITH
    toStartOfDay({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfDay({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*7*24*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(24*time.Hour).UnixNano(), 10),
				"p2": "86400",
			},
		},
		{
			name:        "building a time window for the last 14d",
			statsPeriod: "14d",
			expectedQuery: `
WITH
    toStartOfDay({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfDay({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*14*24*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(24*time.Hour).UnixNano(), 10),
				"p2": "86400",
			},
		},
		{
			name:        "building a time window for the last 30d",
			statsPeriod: "30d",
			expectedQuery: `
WITH
    toStartOfDay({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfDay({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*30*24*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(24*time.Hour).UnixNano(), 10),
				"p2": "86400",
			},
		},
		{
			name:        "building a time window for an unspecified period should return data for 24h",
			statsPeriod: "fooh",
			expectedQuery: `
WITH
    toStartOfHour({p0:DateTime64(9, 'UTC')}) AS start,
    toStartOfHour({p1:DateTime64(9, 'UTC')}) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), {p2:UInt64}))) AS t
`,
			expectedArgs: clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*24*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(time.Hour).UnixNano(), 10),
				"p2": "3600",
			},
		},
	} {
		t.Log(tc.name)
		qb := buildTimeSeriesQuery(refTime, tc.statsPeriod)
		assert.Equal(t, tc.expectedQuery, qb.SQL())
		assert.EqualValues(t, tc.expectedArgs, qb.StringifyParams())
	}
}

func TestBuildErrorStatsQuery(t *testing.T) {
	refTime := time.Now()
	fingerprints := []uint32{233069371, 1805903340} // random values
	projectID := "1"
	for _, tc := range []struct {
		name          string
		statsPeriod   string
		expectedQuery string
		expectedArgs  clickhouse.Parameters
	}{
		{
			name:        "querying error events for the last 15m",
			statsPeriod: "15m",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfMinute(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*15*time.Minute).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(time.Minute).UnixNano(), 10),
			},
		},
		{
			name:        "querying error events for the last 30m",
			statsPeriod: "30m",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfMinute(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*30*time.Minute).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(time.Minute).UnixNano(), 10),
			},
		},
		{
			name:        "querying error events for the last 1h",
			statsPeriod: "1h",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfMinute(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*60*time.Minute).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(time.Minute).UnixNano(), 10),
			},
		},
		{
			name:        "querying error events for the last 1d",
			statsPeriod: "1d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfHour(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*24*time.Hour).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(time.Hour).UnixNano(), 10),
			},
		},
		{
			name:        "querying error events for the last 7d",
			statsPeriod: "7d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfDay(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*7*24*time.Hour).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(24*time.Hour).UnixNano(), 10),
			},
		},
		{
			name:        "querying error events for the last 14d",
			statsPeriod: "14d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfDay(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*14*24*time.Hour).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(24*time.Hour).UnixNano(), 10),
			},
		},
		{
			name:        "querying error events for the last 30d",
			statsPeriod: "30d",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfDay(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*30*24*time.Hour).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(24*time.Hour).UnixNano(), 10),
			},
		},
		{
			name:        "querying error events for an unspecified period should return data for 24h",
			statsPeriod: "fooh",
			expectedQuery: `
SELECT
	fingerprint,
	toUInt64(toStartOfHour(occurred_at)) AS t,
	count() AS cnt
FROM gl_error_tracking_error_events
WHERE fingerprint IN {p0:Array(UInt32)} AND project_id = {p1:UInt64} AND t >= {p2:DateTime64(9, 'UTC')} AND t <= {p3:DateTime64(9, 'UTC')} GROUP BY fingerprint, t ORDER BY fingerprint ASC, t DESC`,
			expectedArgs: clickhouse.Parameters{
				"p0": clickhouse.Array[uint32](fingerprints).String(),
				"p1": projectID,
				"p2": strconv.FormatInt(refTime.Add(-1*24*time.Hour).UnixNano(), 10),
				"p3": strconv.FormatInt(refTime.Add(time.Hour).UnixNano(), 10),
			},
		},
	} {
		t.Log(tc.name)
		qb := buildErrorStatsQuery(fingerprints, refTime, tc.statsPeriod, 1)
		assert.Equal(t, tc.expectedQuery, qb.SQL())
		assert.EqualValues(t, tc.expectedArgs, qb.StringifyParams())
	}
}
