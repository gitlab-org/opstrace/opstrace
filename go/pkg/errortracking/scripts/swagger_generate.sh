#!/usr/bin/env bash

cd "$(dirname "$0")" || exit
if [ "$(uname)" == "Darwin" ]; then
  swaggerTmp="$(mktemp "$(uuidgen)".swagger.yaml)"
else
  swaggerTmp="$(mktemp --suffix=.swagger.yaml)"
fi
# remove security applied to every operation
# this is handled by gatekeeper but kept in for external client gen
sed '/^security:/,+1d' ../swagger.yaml >> "$swaggerTmp"

go run github.com/go-swagger/go-swagger/cmd/swagger generate server --target ../gen --name ErrorTracking --spec "$swaggerTmp" --principal interface{} --exclude-main