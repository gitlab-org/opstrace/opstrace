package errortracking

import (
	"context"
	"fmt"

	"github.com/go-openapi/strfmt"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.opentelemetry.io/otel/trace"
)

// InsertErrorTrackingMessageEvent inserts the given message event in the
// error_tracking_message_event table in the clickhouse database.
func (db *database) InsertErrorTrackingMessageEvent(ctx context.Context, e *et.ErrorTrackingMessageEvent) error {
	var span trace.Span
	_, span = instrumentation.NewSubSpan(ctx, "InsertErrorTrackingMessageEvent", trace.SpanKindClient)
	defer span.End()
	err := db.insert(ctx, e)
	if err != nil {
		return fmt.Errorf("inserting error tracking message event: %w", err)
	}

	return nil
}

func (db *database) ListMessages(params messages.ListMessagesParams) ([]*models.MessageEvent, error) {
	var result []*models.MessageEvent

	qb := buildListMessageQuery(params)

	rows, err := db.conn.Query(
		qb.Context(params.HTTPRequest.Context()),
		qb.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to list messages: %w", err)
	}
	for rows.Next() {
		e := &et.ErrorTrackingMessageEvent{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingMessageEvent: %w", err)
		}

		result = append(result, &models.MessageEvent{
			Environment: e.Environment,
			EventID:     e.EventID,
			Level:       e.Level,
			Message:     e.Message,
			Platform:    e.Platform,
			ProjectID:   e.ProjectID,
			Release:     e.Release,
			Timestamp:   strfmt.DateTime(e.Timestamp),
		})
	}
	err = rows.Close()
	if err != nil {
		return nil, fmt.Errorf("failed to close connection while reading from clickhouse: %w", err)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events from clickhouse: %w", err)
	}
	return result, nil
}

const baseQueryListMessages = `SELECT
	event_id,
	project_id,
	timestamp,
	environment,
	level,
	message,
	actor,
	platform,
	release,
	server_name
FROM gl_error_tracking_message_events
`

func buildListMessageQuery(params messages.ListMessagesParams) *clickhouse.QueryBuilder {
	q := clickhouse.NewQueryBuilder()
	q.Build(baseQueryListMessages)

	q.Build("WHERE project_id IN ( ? )", params.ProjectID)

	if params.Limit != nil {
		q.Build("LIMIT ?", *params.Limit)
	} else {
		q.Build("LIMIT ?", int64(defaultListIssueLimit))
	}

	return q
}
