-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_error_status_v2_local ON CLUSTER '{cluster}'
(
    event_id UUID,
    project_id UInt64,
    timestamp DateTime64(6, 'UTC'), -- Precision till microseconds
    is_deleted UInt8,
    fingerprint FixedString(32),

    title Nullable(String),
    actor Nullable(String),
    user_id Nullable(String),
    status Nullable(UInt8), -- 0 UNRESOLVED , 1 RESOLVED, 2 IGNORED

    updated_by UInt8, -- 0 status changed by user, 1 status changed by system (new event happened after resolve)
    updated_at DateTime64(6, 'UTC')
)
ENGINE = ReplicatedReplacingMergeTree('/clickhouse/{cluster}/tables/{shard}/errortracking_api.gl_error_tracking_error_status_v2_local', '{replica}', updated_at)
ORDER BY (project_id, timestamp, fingerprint, event_id);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
