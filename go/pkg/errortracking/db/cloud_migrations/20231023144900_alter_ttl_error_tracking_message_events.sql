-- +goose Up
-- +goose StatementBegin
ALTER TABLE gl_error_tracking_message_events MODIFY TTL toDateTime(timestamp) + INTERVAL 90 day ;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
