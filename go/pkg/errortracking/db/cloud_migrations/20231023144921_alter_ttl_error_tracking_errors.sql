-- +goose Up
-- +goose StatementBegin
ALTER TABLE gl_error_tracking_errors MODIFY TTL toDateTime(first_seen_at) + INTERVAL 90 day ;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
