-- +goose Up
-- +goose StatementBegin
ALTER TABLE gl_error_tracking_ignored_errors MODIFY TTL toDateTime(updated_at) + INTERVAL 90 day ;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
