-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_ignored_errors
(
  project_id UInt64,
  fingerprint UInt32,
  user_id Nullable(UInt64) DEFAULT NULL,
  updated_at DateTime64(6, 'UTC')
)
ENGINE = ReplicatedReplacingMergeTree
ORDER BY (project_id, fingerprint);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
