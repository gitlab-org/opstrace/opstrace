-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS gl_error_tracking_error_events (
  project_id UInt64,
  fingerprint UInt32,
  name String,
  description String,
  actor String,
  environment LowCardinality(String),
  platform String,
  level LowCardinality(String),
  user_identifier String,
  payload String CODEC(LZ4HC(9)), -- AVG 20K bytes
  occurred_at DateTime64(6, 'UTC') -- Precision till microseconds
)
ENGINE = ReplicatedMergeTree
PARTITION BY toYYYYMM(occurred_at)
ORDER BY (project_id, fingerprint, occurred_at);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
-- +goose StatementEnd
