# Migrations for Error tracking

We use our [fork](https://gitlab.com/gitlab-org/opstrace/goose/) of [pressly/goose](https://github.com/pressly/goose) to help with migrations.

The reason to fork is to add replication support for ClickHouse driver so that the version table can be stored on a cluster.

The custom binary is available via the pipeline and can be downloaded as an [artifact](https://gitlab.com/gitlab-org/opstrace/goose/-/jobs/artifacts/master/download?job=build).

In order to get familiarity with the library it is recommended to go through the [docs](https://github.com/pressly/goose#migrations).

## Creating a new migration
To create a new migration, you can use:

```bash
$ goose -dir db/gcsmigrations create alter_storage_policy_sessions_local sql
```

This will create a new migration prefixed with the timestamp. Please adjust the content with the actual SQL.

We haven't yet added any `down` migrations as the tables have only been added.

Generally the idea is to only move forward and in case of any issues, fix it with a new migration instead of trying to rollback.
However, if the rollback is desired it can be added via the down migration.

Migrations are applied whenever the application binary is loaded, and they are supposed to be idempotent.

Please make sure that the migrations always remain as such as application can be restarted for many reasons.
We also use `ON CLUSTER` as a way to ensure that the migrations are applied to the whole cluster no matter where they run.

This also assumes a presence of `{cluster}` macro in ClickHouse config so if you are running the migrations locally,
please make sure of that.

## Checking migration status

Goose keeps the migration state in the table called `goose_db_version`, which can be used to see executed migrations.
Note that in our cases, this table is replicated so all the changes once executed can be seen on any node.

This helps us to avoid worrying about where the migration is running as our application replica sets can be stick to any node.
