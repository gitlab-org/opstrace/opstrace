package errortracking

import (
	"context"
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"go.opentelemetry.io/otel/trace"

	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
)

// InsertErrorTrackingSession inserts the given session in the
// error_tracking_session table in the clickhouse database.
func (db *database) InsertErrorTrackingSession(ctx context.Context, e *et.ErrorTrackingSession) error {
	var span trace.Span
	_, span = instrumentation.NewSubSpan(ctx, "InsertErrorTrackingSession", trace.SpanKindClient)
	defer span.End()
	err := db.insert(ctx, e)
	if err != nil {
		return fmt.Errorf("inserting error tracking session: %w", err)
	}

	return nil
}
