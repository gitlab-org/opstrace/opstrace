package errortracking

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.opentelemetry.io/otel/trace"
)

// InsertErrorTrackingErrorEvent inserts the given error event in the
// error_tracking_error_event table in the clickhouse database. It then proceeds
// to upsert gl_error_tracking_error_status as well.
func (db *database) InsertErrorTrackingErrorEvent(ctx context.Context, e *et.ErrorTrackingErrorEvent) error {
	var span trace.Span
	_, span = instrumentation.NewSubSpan(ctx, "InsertErrorTrackingErrorEvent", trace.SpanKindClient)
	defer span.End()
	err := db.insert(ctx, e)
	if err != nil {
		return fmt.Errorf("inserting error tracking error event: %w", err)
	}

	s := &et.ErrorTrackingErrorStatus{
		ProjectID:   e.ProjectID,
		Fingerprint: e.Fingerprint,
		Status:      uint8(errorUnresolved),
		UserID:      uint64(0),
		Actor:       uint8(2),
		UpdatedAt:   time.Now(),
	}

	err = db.insert(ctx, s)
	if err != nil {
		return fmt.Errorf("inserting error tracking error status: %w", err)
	}

	return nil
}

func (db *database) ListEvents(params errors.ListEventsParams) ([]*models.ErrorEvent, error) {
	ctx := context.Background()

	qb, err := buildListEventsQuery(params)
	if err != nil {
		return nil, err
	}

	rows, err := db.conn.Query(
		qb.Context(ctx),
		qb.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to list events: %w", err)
	}

	var result []*models.ErrorEvent
	for rows.Next() {
		// Scan the row as a ErrorTrackingErrorEvent struct
		e := &et.ErrorTrackingErrorEvent{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingErrorEvent: %w", err)
		}

		endpoint, sentryItem, err := DisambiguateEventSource([]byte(e.Payload))
		if err != nil {
			// log error
			continue
		}

		var payload string
		if endpoint == StoreEndpoint {
			// keep current behavior intact
			payload = e.Payload
		} else if endpoint == EnvelopeEndpoint {
			// for payloads received via the /envelope endpoint, extract
			// out the event-type item and corresponding payload to return
			// in our response
			envelope, ok := sentryItem.(*types.Envelope)
			if !ok {
				return nil, fmt.Errorf("failed to parse envelope payload")
			}
			// as documented in https://develop.sentry.dev/sdk/envelopes/#data-model,
			// an Event item type may occur at most once per Envelope. The following
			// check sanity checks we did receive a valid event before adding it to
			// the response downstream
			if len(envelope.Items) != 1 {
				continue
			}
			payload = string(envelope.Items[0].Payload)
		}
		// Convert the ErrorTrackingErrorEvent struct to a models.ErrorEvent object
		// to make ListEvent response uniform regardless of the payload being parsed
		result = append(result, &models.ErrorEvent{
			Actor:       e.Actor,
			Description: e.Description,
			Environment: e.Environment,
			Fingerprint: e.Fingerprint,
			Name:        e.Name,
			Payload:     payload,
			Platform:    e.Platform,
			ProjectID:   e.ProjectID,
		})
	}
	rows.Close()

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events from clickhouse: %w", err)
	}
	return result, nil
}

func buildListEventsQuery(params errors.ListEventsParams) (*clickhouse.QueryBuilder, error) {
	// Note(Arun): Omitting level, user identifier from columns fetched since models.ErrorEvent don't use them yet.
	const query = `
SELECT project_id, fingerprint, name, actor, description, payload, platform, environment
FROM {errorEventsTable:Identifier} WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
`
	q := clickhouse.NewQueryBuilder().WithParams(map[string]clickhouse.Literal{
		"projectId":        clickhouse.Value(params.ProjectID),
		"fingerprint":      clickhouse.Value(params.Fingerprint),
		"errorEventsTable": clickhouse.Identifier(constants.ErrorTrackingEventsTableName),
	}).Build(query)

	// Sort default value is last_seen_desc so we can skip the nil check.
	orderBy := "ORDER BY"
	switch *params.Sort {
	case "occurred_at_asc":
		orderBy += " occurred_at ASC"
	case "occurred_at_desc":
		orderBy += " occurred_at DESC"
	default:
		panic(fmt.Sprintf("unsupport sort option %s", *params.Sort))
	}
	q.Build(orderBy)
	q.Build("LIMIT {lim:Int64}")
	q.WithParam("lim", clickhouse.Value(*params.Limit))

	page, err := decodePage(params.Cursor)
	if err != nil {
		return nil, err
	}
	// Limit default value is 20 so we can skip the nil check.
	offset := (page - 1) * int(*params.Limit)
	q.Build("OFFSET {oft:Int64}")
	q.WithParam("oft", clickhouse.Value(offset))

	return q, nil
}

const (
	StoreEndpoint    string = "store"
	EnvelopeEndpoint string = "event"
	UnknownEndpoint  string = "unknown"
)

func DisambiguateEventSource(payload []byte) (string, interface{}, error) {
	var (
		item interface{}
		err  error
	)
	item, err = types.NewEventFrom(payload)
	if err == nil {
		return StoreEndpoint, item, nil
	}
	item, err = types.NewEnvelopeFrom(payload)
	if err == nil {
		return EnvelopeEndpoint, item, nil
	}
	return UnknownEndpoint, nil, fmt.Errorf("unknown payload type")
}
