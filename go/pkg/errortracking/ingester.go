package errortracking

import (
	"context"
	"encoding/json"
	"fmt"

	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/persistentqueue"
)

type Ingester interface {
	IngestEventData(context.Context, uint64, string, []byte, *types.Event) error
	IngestMessageData(context.Context, uint64, string, []byte, *types.Event) error
	IngestSessionData(context.Context, uint64, string, []byte, *types.Session) error
}

type ClickhouseIngester struct {
	db Database
}

func NewClickhouseIngester(db Database) Ingester {
	return &ClickhouseIngester{db: db}
}

func (c *ClickhouseIngester) IngestEventData(
	ctx context.Context, projectID uint64, _ string, payload []byte, event *types.Event) error {
	err := c.db.InsertErrorTrackingErrorEvent(ctx, et.NewErrorTrackingErrorEvent(projectID, event, payload))
	if err != nil { // failure to insert
		return fmt.Errorf("failed to insert error event: %w", err)
	}
	return nil
}

func (c *ClickhouseIngester) IngestMessageData(
	ctx context.Context, projectID uint64, _ string, payload []byte, event *types.Event) error {
	err := c.db.InsertErrorTrackingMessageEvent(ctx, et.NewErrorTrackingMessageEvent(projectID, event, payload))
	if err != nil {
		return fmt.Errorf("failed to insert message event to the database: %w", err)
	}

	return nil
}

func (c *ClickhouseIngester) IngestSessionData(
	ctx context.Context, projectID uint64, _ string, payload []byte, session *types.Session) error {
	err := c.db.InsertErrorTrackingSession(ctx, et.NewErrorTrackingSession(projectID, session, payload))
	if err != nil {
		return fmt.Errorf("failed to insert session to the database: %w", err)
	}
	return nil
}

type QueueIngester struct {
	queue persistentqueue.IQueue
}

func NewQueueIngester(queue *persistentqueue.Queue) Ingester {
	return &QueueIngester{queue: queue}
}

func (q *QueueIngester) IngestEventData(
	ctx context.Context, projectID uint64, payloadType string, payload []byte, _ *types.Event) error {
	return q.ingestData(ctx, projectID, payloadType, payload)
}

func (q *QueueIngester) IngestMessageData(
	ctx context.Context, projectID uint64, payloadType string, payload []byte, _ *types.Event) error {
	return q.ingestData(ctx, projectID, payloadType, payload)
}

func (q *QueueIngester) IngestSessionData(
	ctx context.Context, projectID uint64, payloadType string, payload []byte, session *types.Session) error {
	return q.ingestData(ctx, projectID, payloadType, payload)
}

func (q *QueueIngester) ingestData(_ context.Context, projectID uint64, payloadType string, payload []byte) error {
	var b []byte
	b, err := json.Marshal(QueueItem{
		Type:      payloadType,
		Payload:   payload,
		ProjectID: projectID,
	})
	if err != nil {
		return fmt.Errorf("marshaling queue item: %w", err)
	}
	err = q.queue.Insert(b)
	if err != nil {
		return fmt.Errorf("enqueueing event: %w", err)
	}
	return nil
}
