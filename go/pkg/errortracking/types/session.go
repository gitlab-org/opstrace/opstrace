package types

import (
	"encoding/json"
	"fmt"
	"time"
)

// Use a map for finding keys fast.
var statusValues = map[string]uint8{
	"ok":       uint8(1),
	"exited":   uint8(2),
	"crashed":  uint8(3),
	"abnormal": uint8(4),
}

type Session struct {
	SessionID  string            `json:"sid,omitempty"`
	UserID     string            `json:"did,omitempty"`
	Init       bool              `json:"init,omitempty"`
	Started    UnixOrRFC3339Time `json:"started,omitempty"`
	OccurredAt UnixOrRFC3339Time `json:"timestamp,omitempty"`
	Duration   float64           `json:"duration,omitempty"`
	Status     string            `json:"status,omitempty"`
	Attributes SessionAttributes `json:"attrs,omitempty"`
}

type SessionAttributes struct {
	Release     string `json:"release,omitempty"`
	Environment string `json:"environment,omitempty"`
}

func NewSessionFrom(payload []byte) (*Session, error) {
	s := &Session{}
	if err := json.Unmarshal(payload, s); err != nil {
		return nil, fmt.Errorf("unmarshal session: %w", err)
	}

	if s.OccurredAt.Time().IsZero() {
		s.OccurredAt = UnixOrRFC3339Time(time.Now().UTC())
	}

	return s, nil
}

func (s *Session) ValidateStatus() error {
	_, ok := statusValues[s.Status]
	if ok {
		return nil
	}
	return fmt.Errorf("session status unknown: %s", s.Status)
}

func (s *Session) Validate() error {
	if s.SessionID == "" || s.Attributes.Release == "" {
		return fmt.Errorf("validation failed: session is missing required filed Release or SessionID")
	}

	if err := s.ValidateStatus(); err != nil {
		return fmt.Errorf("validation failed: %w", err)
	}

	return nil
}
