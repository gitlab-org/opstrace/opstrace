package types

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewEventFrom(t *testing.T) {
	type args struct {
		payload []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *Event
		wantErr bool
	}{
		{
			"broken json should return error",
			args{
				[]byte(`{`),
			},
			nil,
			true,
		},
		{
			"empty object should return zero values expect current timestamp",
			args{
				[]byte(`{}`),
			},
			&Event{
				Timestamp: UnixOrRFC3339Time(time.Now().UTC()),
			},
			false,
		},
		{
			"a RFC3339 timestamp is parsed",
			args{
				[]byte(`{"timestamp": "2020-01-01T00:00:00Z"}`),
			},
			&Event{
				Timestamp: UnixOrRFC3339Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
			},
			false,
		},
		{
			"a unix timestamp is parsed",
			args{
				[]byte(`{"timestamp": 1498827360}`),
			},
			&Event{
				Timestamp: UnixOrRFC3339Time(time.Unix(1498827360, 0)),
			},
			false,
		},
		{
			"message can be a plain string",
			args{
				[]byte(`{"message": "hello"}`),
			},
			&Event{
				Message:   Message("hello"),
				Timestamp: UnixOrRFC3339Time(time.Now().UTC()),
			},
			false,
		},
		{
			"message can be an object with a 'formatted' key",
			args{
				[]byte(`{"message": {"formatted": "hello"}}`),
			},
			&Event{
				Message:   Message("hello"),
				Timestamp: UnixOrRFC3339Time(time.Now().UTC()),
			},
			false,
		},
		{
			"example with all fields set",
			args{
				[]byte(`{
					"dist": "foo",
					"environment": "bar",
					"event_id": "baz",
					"level": "error",
					"timestamp": "2020-01-01T00:00:00Z",
					"message": {
						"formatted": "hello"
					},
					"platform": "go",
					"release": "1.0.0",
					"server_name": "server",
					"transaction": "transaction",
					"exception": {
						"values": [
							{
								"stacktrace": {
									"frames": [
										{
											"filename": "main.go"
										}
									]
								}
							}
						]
					},
					"user": {
						"id": "1",
						"ip_address": "127.0.0.1",
						"email": "foo@example.com",
						"username": "user"
					}
				}`),
			},
			&Event{
				Dist:        "foo",
				Environment: "bar",
				EventID:     "baz",
				Level:       "error",
				Message:     Message("hello"),
				Platform:    "go",
				Release:     "1.0.0",
				ServerName:  "server",
				Transaction: "transaction",
				Timestamp:   UnixOrRFC3339Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
				Exception: Exceptions([]*Exception{
					{
						Stacktrace: &Stacktrace{
							Frames: []Frame{
								{
									Filename: "main.go",
								},
							},
						},
					},
				}),
				User: User{
					ID:        "1",
					IPAddress: "127.0.0.1",
					Email:     "foo@example.com",
					Username:  "user",
				},
			},
			false,
		},
		{
			"user id can be an integer",
			args{
				[]byte(`{
					"timestamp": "2020-01-01T00:00:00Z",
					"user": {
						"id": 100
					}
				}`),
			},
			&Event{
				Timestamp: UnixOrRFC3339Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
				User: User{
					ID: "100",
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewEventFrom(tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewEventFrom() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil {
				assert.Nil(t, got)
				return
			}

			// timestamp is set to current if not present in the payload
			// check tolerance is within a second
			assert.WithinDuration(t, tt.want.Timestamp.Time(), got.Timestamp.Time(), time.Second)
			tt.want.Timestamp = got.Timestamp

			assert.EqualExportedValues(t, *tt.want, *got, "new event")
		})
	}
}
