package types

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

// Event is a representation of https://develop.sentry.dev/sdk/event-payloads/.
type Event struct {
	Dist        string            `json:"dist,omitempty"`
	Environment string            `json:"environment,omitempty"`
	EventID     string            `json:"event_id,omitempty"`
	Level       string            `json:"level,omitempty"`
	Message     Message           `json:"message,omitempty"`
	Platform    string            `json:"platform,omitempty"`
	Release     string            `json:"release,omitempty"`
	Timestamp   UnixOrRFC3339Time `json:"timestamp"`
	Transaction string            `json:"transaction,omitempty"`
	Exception   Exceptions        `json:"exception,omitempty"`
	// The fields below are only relevant for transactions.
	Type      string    `json:"type,omitempty"`
	StartTime time.Time `json:"start_timestamp"`
	Spans     []*Span   `json:"spans,omitempty"`
	// This will hold a pointer to  the first Exception that has a stacktrace
	// since the first Exception may not provide adequate context (e.g. in the
	// Go SDK).
	// Or, if there are no stacktraces it's just the first Exception.
	// Or, nil if there are no Exceptions at all (i.e. it's a message or something else).
	primaryException *Exception `json:"-"`

	ServerName string `json:"server_name,omitempty"`

	User User `json:"user,omitempty"`
}

// Message aliased for custom json marshaling
// message can be an object or a string. See https://develop.sentry.dev/sdk/event-payloads/message/.
type Message string

func (m *Message) UnmarshalJSON(b []byte) error {
	var formatted struct {
		Formatted string `json:"formatted"`
	}

	if err := json.Unmarshal(b, &formatted); err == nil {
		*m = Message(formatted.Formatted)
		return nil
	}

	var v string
	if err := json.Unmarshal(b, &v); err != nil {
		return fmt.Errorf("expected a string or an object: %w", err)
	}
	*m = Message(v)
	return nil
}

// User implements user interface - https://develop.sentry.dev/sdk/event-payloads/user/
type User struct {
	ID        IntOrString `json:"id,omitempty"`
	Username  string      `json:"username,omitempty"`
	Email     string      `json:"email,omitempty"`
	IPAddress string      `json:"ip_address,omitempty"`
}

// Identifier returns id in order of preference: email, username, id, ip_address.
// May return an empty string.
func (u *User) Identifier() string {
	if u.Email != "" {
		return u.Email
	}
	if u.Username != "" {
		return u.Username
	}
	if u.ID != "" {
		return string(u.ID)
	}
	return u.IPAddress
}

type IntOrString string

func (is *IntOrString) UnmarshalJSON(value []byte) error {
	if value[0] == '"' {
		var s string
		if err := json.Unmarshal(value, &s); err != nil {
			return fmt.Errorf("unmarshal string: %w", err)
		}
		*is = IntOrString(s)
		return nil
	}

	i := 0
	if err := json.Unmarshal(value, &i); err != nil {
		return fmt.Errorf("unmarshall int: %w", err)
	}

	*is = IntOrString(strconv.Itoa(i))
	return nil
}

// Exceptions holds one or more exceptions.
// Can be an object with the attribute values [] or a flat list of objects.
// See https://develop.sentry.dev/sdk/event-payloads/exception/.
type Exceptions []*Exception

func (e *Exceptions) UnmarshalJSON(b []byte) error {
	var v []*Exception
	if err := json.Unmarshal(b, &v); err == nil {
		*e = v
		return nil
	}

	var vs struct {
		Values []*Exception `json:"values"`
	}

	if err := json.Unmarshal(b, &vs); err != nil {
		return fmt.Errorf("expected an array or an object: %w", err)
	}
	*e = vs.Values

	return nil
}

// Exception holds core attributes. See https://develop.sentry.dev/sdk/event-payloads/exception/#attributes.
type Exception struct {
	Type       string      `json:"type,omitempty"`
	Value      string      `json:"value,omitempty"`
	Module     string      `json:"module,omitempty"`
	ThreadID   interface{} `json:"thread_id,omitempty"`
	Stacktrace *Stacktrace `json:"stacktrace,omitempty"`
}

type Stacktrace struct {
	Frames        []Frame `json:"frames,omitempty"`
	FramesOmitted []uint  `json:"frames_omitted,omitempty"`
}

type Frame struct {
	Function    string                 `json:"function,omitempty"`
	Symbol      string                 `json:"symbol,omitempty"`
	Module      string                 `json:"module,omitempty"`
	Package     string                 `json:"package,omitempty"`
	Filename    string                 `json:"filename,omitempty"`
	AbsPath     string                 `json:"abs_path,omitempty"`
	Lineno      int                    `json:"lineno,omitempty"`
	Colno       int                    `json:"colno,omitempty"`
	PreContext  []string               `json:"pre_context,omitempty"`
	ContextLine string                 `json:"context_line,omitempty"`
	PostContext []string               `json:"post_context,omitempty"`
	InApp       bool                   `json:"in_app,omitempty"`
	Vars        map[string]interface{} `json:"vars,omitempty"`
}

type Span struct {
	TraceID      string                 `json:"trace_id"`
	SpanID       string                 `json:"span_id"`
	ParentSpanID string                 `json:"parent_span_id"`
	Op           string                 `json:"op,omitempty"`
	Description  string                 `json:"description,omitempty"`
	Status       string                 `json:"status,omitempty"`
	Tags         map[string]string      `json:"tags,omitempty"`
	StartTime    time.Time              `json:"start_timestamp"`
	EndTime      time.Time              `json:"timestamp"`
	Data         map[string]interface{} `json:"data,omitempty"`
}

// NewEventFrom parses a payload request to an Event object.
// Note: Currently we don't have limits on payload size when ingesting via this method.
func NewEventFrom(payload []byte) (*Event, error) {
	e := &Event{}

	if err := json.Unmarshal(payload, e); err != nil {
		return nil, fmt.Errorf("unmarshal event: %w", err)
	}

	// In case where `timestamp` key is missing in the payload, fill it on server side
	if e.Timestamp.Time().IsZero() {
		log.Info("missing 'timestamp' column in exception event payload, filling it on server")
		e.Timestamp = UnixOrRFC3339Time(time.Now().UTC())
	}

	e.locatePrimaryException()

	return e, nil
}

func (e *Event) locatePrimaryException() {
	// Find pointer to the first exception that has a stacktrace since the first
	// exception may not provide adequate context (e.g. in the Go SDK).
	// Original ruby code:
	//nolint:lll
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L41-48
	// - https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L56-67
	for i, ex := range e.Exception {
		if ex.Stacktrace == nil {
			continue
		}
		e.primaryException = e.Exception[i]
		return
	}

	// If there are no stacktraces it's just the first Exception.
	if len(e.Exception) > 0 {
		e.primaryException = e.Exception[0]
	}
}

func (e *Event) ExtractDataItemType() string {
	if e.primaryException != nil {
		return SupportedTypeException
	}
	if e.Message != "" {
		return SupportedTypeMessage
	}
	return UnsupportedDataItem
}

func (e *Event) ValidateException() error {
	if e.primaryException == nil {
		return fmt.Errorf("invalid error event of exception type; exception key not present")
	}

	if e.Name() == "" {
		return fmt.Errorf("invalid error event of exception type: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event of exception type: description is not set")
	}

	if e.ExceptionActor() == "" {
		return fmt.Errorf("invalid error event of exception type: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event of exception type: platform is not set")
	}

	return nil
}

// Actor for message returns first item in stacktrace that has a function and module name
// if stacktraces are not available then an empty string is returned.
// TODO: We need to implement message actor by parsing the stacktrace for various SDKs
// for now we will not be handling stacktraces

func (e Event) MessageActor(payload []byte) string {
	return ""
}

func (e *Event) ValidateMessage() error {
	if e.Platform == "" {
		return fmt.Errorf("invalid error event of message type: platform is not set")
	}

	if e.Message == "" {
		return fmt.Errorf("invalid error event of message type: message is not set")
	}
	return nil
}

// Validate Event has required fields set.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/models/error_tracking/error.rb#L20-25
func (e *Event) Validate() error {
	// Note(Arun): Currently exception is a mandatory field as per our prior implementation.
	//nolint:lll
	// See https://gitlab.com/gitlab-org/gitlab/-/blob/308867c700f2b614e2ee29c8fae8fd8a698e8203/app/validators/json_schemas/error_tracking_event_payload.json#L4 for the list of required fields.
	if e.primaryException == nil {
		return fmt.Errorf("invalid error event; exception key not present")
	}

	if e.Name() == "" {
		return fmt.Errorf("invalid error event: name is not set")
	}

	if e.Description() == "" {
		return fmt.Errorf("invalid error event: description is not set")
	}

	if e.ExceptionActor() == "" {
		return fmt.Errorf("invalid error event: actor is not set")
	}

	if e.Platform == "" {
		return fmt.Errorf("invalid error event: platform is not set")
	}

	return nil
}

// Actor returns the transaction name. If the error does not have one set it
// returns the first item in stacktrace that has a function and module name.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L11
// - https://gitlab.com/gitlab-org/gitlab/-/blob/daa5796df89d775c635dc900a532efb657877b75/app/services/error_tracking/collect_error_service.rb#L66
//
//nolint:lll
func (e Event) ExceptionActor() string {
	if e.Transaction != "" {
		return e.Transaction
	}

	// If no exception is present return early
	if e.primaryException == nil {
		return ""
	}

	gotTraces := e.primaryException.Stacktrace != nil && len(e.primaryException.Stacktrace.Frames) > 0
	if !gotTraces {
		// TODO(joe): we could use Threads stacks here to build a better actor.
		// This may work for rust when the error is not an uncaught panic.
		return fmt.Sprintf("%s(%s)", e.primaryException.Type, e.primaryException.Module)
	}

	// Some SDKs do not have a transaction attribute. So we build it by
	// combining function name and module name from the last item in stacktrace.
	//nolint:lll
	// https://gitlab.com/gitlab-org/gitlab/-/blob/8565b9d4770baafa560915b6b06da6026ecab41c/app/services/error_tracking/collect_error_service.rb#L62

	f := e.primaryException.Stacktrace.Frames[len(e.primaryException.Stacktrace.Frames)-1]
	return fmt.Sprintf("%s(%s)", f.Function, f.Module)
}

// Name returns the exception type.
// Original ruby code:
// https://gitlab.com/gitlab-org/gitlab/-/blob/cb84795fae22fa8d87bddda8810e636cd729030d/app/services/error_tracking/collect_error_service.rb#L9
//
//nolint:lll
func (e Event) Name() string {
	if e.primaryException != nil {
		return e.primaryException.Type
	}
	return ""
}

// Description returns the exception value.
// Original ruby code:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/90b6bea21c74ccc98205aff69e0e0f2acfc7993f/app/services/error_tracking/collect_error_service.rb#L10
//
//nolint:lll
func (e Event) Description() string {
	if e.primaryException != nil {
		return e.primaryException.Value
	}
	return ""
}
