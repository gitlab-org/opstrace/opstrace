package types

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewSessionFrom(t *testing.T) {
	type args struct {
		payload []byte
	}
	tests := []struct {
		name    string
		args    args
		want    *Session
		wantErr bool
	}{
		{
			"broken json should return error",
			args{
				[]byte(`{`),
			},
			nil,
			true,
		},
		{
			"empty object should return zero values expect current timestamp",
			args{
				[]byte(`{}`),
			},
			&Session{
				OccurredAt: UnixOrRFC3339Time(time.Now().UTC()),
			},
			false,
		},
		{
			"a RFC3339 timestamp is parsed",
			args{
				[]byte(`{"timestamp": "2020-01-01T00:00:00Z"}`),
			},
			&Session{
				OccurredAt: UnixOrRFC3339Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
			},
			false,
		},
		{
			"example with all fields set",
			args{
				[]byte(`{
					"sid": "sid",
					"did": "did",
					"init": true,
					"started": "2020-01-01T00:00:00Z",
					"timestamp": "2020-01-01T00:00:00Z",
					"duration": 1.0,
					"status": "ok",
					"attrs": {
						"release": "release",
						"environment": "environment"
					}
				}`),
			},
			&Session{
				SessionID:  "sid",
				UserID:     "did",
				Init:       true,
				Started:    UnixOrRFC3339Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
				OccurredAt: UnixOrRFC3339Time(time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)),
				Duration:   1.0,
				Status:     "ok",
				Attributes: SessionAttributes{
					Release:     "release",
					Environment: "environment",
				},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewSessionFrom(tt.args.payload)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewSessionFrom() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				assert.Nil(t, got)
				return
			}

			// timestamp is set to current if not present in the payload
			// check it's within a millisecond
			assert.WithinDuration(t, tt.want.OccurredAt.Time(), got.OccurredAt.Time(), time.Millisecond)
			tt.want.OccurredAt = got.OccurredAt

			assert.EqualExportedValues(t, *tt.want, *got, "new session")
		})
	}
}
