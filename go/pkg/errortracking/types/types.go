package types

const (
	SupportedTypeException = "exception"
	SupportedTypeMessage   = "message"
	UnsupportedDataItem    = "unknown"

	EventType   = "event"
	SessionType = "session"
)
