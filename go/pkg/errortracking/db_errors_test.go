package errortracking

import (
	"fmt"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
)

// used as a placeholder for request contexts
var httpRequest = httptest.NewRequest("GET", "/test", nil)

type testcase struct {
	name     string
	params   errors.ListErrorsParams
	expected string
	args     clickhouse.Parameters
	err      error
}

func TestBuildListErrorsQuery(t *testing.T) {
	// reference time to test time-based filtering against
	refTime := time.Now()
	defaultStartTime := refTime.Add(-1 * 30 * 24 * time.Hour).UnixNano()
	defaultEndTime := refTime.Add(1 * 24 * time.Hour).UnixNano()
	testcases := []testcase{
		{
			name: "should build a simple query",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(20),
				Query:       nil,
				Cursor:      nil,
				Sort:        stringPointer("last_seen_desc"),
				Status:      stringPointer("unresolved"),
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}
AND COALESCE(ignored_errors.ignored, FALSE) = FALSE AND errors_status.status = {eStatus:UInt8}
ORDER BY last_seen_at DESC, fingerprint DESC LIMIT {lim:Int64} OFFSET {oft:Int64} 
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"startTime":          strconv.FormatInt(defaultStartTime, 10),
				"endTime":            strconv.FormatInt(defaultEndTime, 10),
				"eStatus":            "0",
				"lim":                "20",
				"oft":                "0",
			},
			err: nil,
		},
		{
			name: "should build a query with custom sort",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(20),
				Query:       nil,
				Cursor:      nil,
				Sort:        stringPointer("first_seen_desc"),
				Status:      stringPointer("unresolved"),
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}
AND COALESCE(ignored_errors.ignored, FALSE) = FALSE AND errors_status.status = {eStatus:UInt8}
ORDER BY first_seen_at DESC, fingerprint DESC LIMIT {lim:Int64} OFFSET {oft:Int64} 
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"startTime":          strconv.FormatInt(defaultStartTime, 10),
				"endTime":            strconv.FormatInt(defaultEndTime, 10),
				"eStatus":            "0",
				"lim":                "20",
				"oft":                "0",
			},
			err: nil,
		},
		{
			name: "should build a query with custom status",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(20),
				Query:       nil,
				Cursor:      nil,
				Sort:        stringPointer("first_seen_desc"),
				Status:      stringPointer("ignored"),
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}
AND COALESCE(ignored_errors.ignored, FALSE) = TRUE
ORDER BY first_seen_at DESC, fingerprint DESC LIMIT {lim:Int64} OFFSET {oft:Int64} 
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"startTime":          strconv.FormatInt(defaultStartTime, 10),
				"endTime":            strconv.FormatInt(defaultEndTime, 10),
				"lim":                "20",
				"oft":                "0",
			},
			err: nil,
		},
		{
			name: "should build a query with custom query parameter",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(20),
				Query:       stringPointer("foo"),
				Cursor:      nil,
				Sort:        stringPointer("first_seen_desc"),
				Status:      stringPointer("ignored"),
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}
AND COALESCE(ignored_errors.ignored, FALSE) = TRUE
AND (errors.name ILIKE {p0:String} OR errors.description ILIKE {p1:String}) ORDER BY first_seen_at DESC, fingerprint DESC LIMIT {lim:Int64} OFFSET {oft:Int64} 
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"startTime":          strconv.FormatInt(defaultStartTime, 10),
				"endTime":            strconv.FormatInt(defaultEndTime, 10),
				"p0":                 "%foo%",
				"p1":                 "%foo%",
				"lim":                "20",
				"oft":                "0",
			},
			err: nil,
		},
		{
			name: "should fail with invalid custom cursor parameter",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(10),
				Query:       nil,
				Cursor:      stringPointer("foobar"),
				Sort:        stringPointer("frequency_desc"),
				Status:      stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected end of JSON input"),
		},
		{
			name: "should not fail with valid custom cursor parameter",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(9),
				Query:       nil,
				// cursor is the base64 encoded json string: {"page":3}
				Cursor: stringPointer("eyJwYWdlIjozfQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}
AND COALESCE(ignored_errors.ignored, FALSE) = FALSE AND errors_status.status = {eStatus:UInt8}
ORDER BY event_count DESC, fingerprint DESC LIMIT {lim:Int64} OFFSET {oft:Int64} 
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"startTime":          strconv.FormatInt(defaultStartTime, 10),
				"endTime":            strconv.FormatInt(defaultEndTime, 10),
				"eStatus":            "1",
				"lim":                "9",
				"oft":                "18",
			},
			err: nil,
		},
		{
			name: "should not fail with custom cursor parameter without page field",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(7),
				Query:       nil,
				// cursor is the base64 encoded json string: {"foo":"bar"}
				Cursor: stringPointer("eyJmb28iOiJiYXIifQo="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}
AND COALESCE(ignored_errors.ignored, FALSE) = FALSE AND errors_status.status = {eStatus:UInt8}
ORDER BY event_count DESC, fingerprint DESC LIMIT {lim:Int64} OFFSET {oft:Int64} 
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"startTime":          strconv.FormatInt(defaultStartTime, 10),
				"endTime":            strconv.FormatInt(defaultEndTime, 10),
				"eStatus":            "1",
				"lim":                "7",
				"oft":                "0",
			},
			err: nil,
		},
		{
			name: "should fail with custom cursor parameter with invalid json",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(7),
				Query:       nil,
				// cursor is the base64 encoded json string: "foobar"
				Cursor: stringPointer("Zm9vYmFyCg=="),
				Sort:   stringPointer("frequency_desc"),
				Status: stringPointer("resolved"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("invalid character"),
		},
		{
			name: "should fail with unexpected status",
			params: errors.ListErrorsParams{
				HTTPRequest: httpRequest,
				ProjectID:   1,
				Limit:       int64Pointer(20),
				Query:       nil,
				Cursor:      nil,
				Sort:        stringPointer("first_seen_desc"),
				Status:      stringPointer("_ignored"),
			},
			expected: "",
			args:     nil,
			err:      fmt.Errorf("unexpected error status _ignored"),
		},
	}
	// end of base testcases
	// now, add time-filter specific testcases
	for _, queryPeriod := range supportedTimePeriods {
		var (
			startTime, endTime int64
		)
		switch queryPeriod {
		case period15m:
			startTime = refTime.Add(-1 * 15 * time.Minute).UnixNano()
			endTime = refTime.Add(1 * time.Minute).UnixNano()
		case period30m:
			startTime = refTime.Add(-1 * 30 * time.Minute).UnixNano()
			endTime = refTime.Add(time.Minute).UnixNano()
		case period1h:
			startTime = refTime.Add(-1 * 60 * time.Minute).UnixNano()
			endTime = refTime.Add(time.Minute).UnixNano()
		case period24h:
			startTime = refTime.Add(-1 * 24 * time.Hour).UnixNano()
			endTime = refTime.Add(time.Hour).UnixNano()
		case period7d:
			startTime = refTime.Add(-1 * 7 * 24 * time.Hour).UnixNano()
			endTime = refTime.Add(24 * time.Hour).UnixNano()
		case period14d:
			startTime = refTime.Add(-1 * 14 * 24 * time.Hour).UnixNano()
			endTime = refTime.Add(24 * time.Hour).UnixNano()
		case period30d:
			startTime = refTime.Add(-1 * 30 * 24 * time.Hour).UnixNano()
			endTime = refTime.Add(24 * time.Hour).UnixNano()
		}

		testcases = append(testcases, testcase{
			name: fmt.Sprintf("should build a simple query with data filtered for: %s", queryPeriod),
			params: errors.ListErrorsParams{
				ProjectID:   1,
				Limit:       int64Pointer(20),
				Query:       nil,
				Cursor:      nil,
				Sort:        stringPointer("last_seen_desc"),
				Status:      stringPointer("unresolved"),
				QueryPeriod: stringPointer(queryPeriod),
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}
AND COALESCE(ignored_errors.ignored, FALSE) = FALSE AND errors_status.status = {eStatus:UInt8}
ORDER BY last_seen_at DESC, fingerprint DESC LIMIT {lim:Int64} OFFSET {oft:Int64} 
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"startTime":          strconv.FormatInt(startTime, 10),
				"endTime":            strconv.FormatInt(endTime, 10),
				"eStatus":            "0",
				"lim":                "20",
				"oft":                "0",
			},
			err: nil,
		})
	}
	// now run all testcases
	for _, tc := range testcases {
		t.Log(tc.name)
		qb, err := buildListErrorsQuery(tc.params, refTime)
		if tc.err != nil {
			assert.Error(t, err)
			assert.Nil(t, qb)
			continue
		} else {
			assert.Nil(t, err)
		}
		assert.Equal(t, tc.expected, qb.SQL())
		assert.EqualValues(t, tc.args, qb.StringifyParams())
	}
}

func TestBuildGetErrorQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   errors.GetErrorParams
		expected string
		args     clickhouse.Parameters
	}{
		{
			name: "should build a simple query",
			params: errors.GetErrorParams{
				ProjectID:   1,
				Fingerprint: 1,
			},
			expected: `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}

SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`,
			args: clickhouse.Parameters{
				"errorsTable":        constants.ErrorTrackingErrorsTableName,
				"errorsStatusTable":  constants.ErrorTrackingErrorStatusTableName,
				"errorsIgnoredTable": constants.ErrorTrackingErrorIgnoreTableName,
				"projectId":          "1",
				"fingerprint":        "1",
			},
		},
	} {
		t.Log(tc.name)
		qb := buildGetErrorQuery(tc.params)
		assert.Equal(t, tc.expected, qb.SQL())
		assert.EqualValues(t, tc.args, qb.StringifyParams())
	}
}
