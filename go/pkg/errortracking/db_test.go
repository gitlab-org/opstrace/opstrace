package errortracking

import (
	"context"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/stretchr/testify/mock"
)

func stringPointer(s string) *string {
	return &s
}

func int64Pointer(i int64) *int64 {
	return &i
}

type MockClickhouseConn struct {
	mock.Mock
	driver.Conn
}

func (m *MockClickhouseConn) Query(ctx context.Context, query string, args ...interface{}) (driver.Rows, error) {
	var listOfArguments []interface{}
	listOfArguments = append(listOfArguments, ctx)
	listOfArguments = append(listOfArguments, query)
	listOfArguments = append(listOfArguments, args...)
	arguments := m.Called(listOfArguments...)
	return arguments.Get(0).(driver.Rows), arguments.Error(1)
}

type MockClickhouseRows struct {
	mock.Mock
	driver.Rows
}

func (m *MockClickhouseRows) Next() bool {
	args := m.Called()
	return args.Bool(0)
}

func (m *MockClickhouseRows) ScanStruct(dest interface{}) error {
	args := m.Called(dest)
	return args.Error(0)
}

func (m *MockClickhouseRows) Close() error {
	args := m.Called()
	return args.Error(0)
}

func (m *MockClickhouseRows) Err() error {
	args := m.Called()
	return args.Error(0)
}
