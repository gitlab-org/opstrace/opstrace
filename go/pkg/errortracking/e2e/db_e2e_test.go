package e2e

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

func TestDBMigrationsE2E(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	ch, err := testutils.NewClickHouseServer(ctx)
	require.NoError(t, err)
	dsn, err := ch.GetDSN(ctx, "")
	require.NoError(t, err)
	_, err = errortracking.NewDB(dsn, &errortracking.DatabaseOptions{UseRemoteStorage: "gcp"})
	require.NoError(t, err)
}

func TestDBMigrationsMissingE2E(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	ch, err := testutils.NewClickHouseServer(ctx)
	require.NoError(t, err)
	dsn, err := ch.GetDSN(ctx, "")
	require.NoError(t, err)
	_, err = errortracking.NewDB(dsn, &errortracking.DatabaseOptions{})
	require.NoError(t, err)

	// GCS specific migrations should be older than the general migrations.
	// This should verify that they are applied correctly.
	_, err = errortracking.NewDB(dsn, &errortracking.DatabaseOptions{
		UseRemoteStorage: "gcp",
	})
	require.NoError(t, err)

	// Re-run the migrations to make sure that we don't re-apply the migrations
	_, err = errortracking.NewDB(dsn, &errortracking.DatabaseOptions{
		UseRemoteStorage: "gcp",
	})
	require.NoError(t, err)
}
