package errortracking

import (
	"context"
	"database/sql"
	"embed"
	"fmt"
	"io/fs"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/store"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/errors"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/projects"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	ch "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

type errorStatus uint8

const (
	errorUnresolved errorStatus = iota
	errorResolved
	errorIgnored
)

type errorStatusStr string

const (
	errorUnresolvedStr = "unresolved"
	errorResolvedStr   = "resolved"
	errorIgnoredStr    = "ignored"
)

var errorStatusToInt = map[errorStatusStr]errorStatus{
	errorUnresolvedStr: errorUnresolved,
	errorResolvedStr:   errorResolved,
	errorIgnoredStr:    errorIgnored,
}

var errorStatusToStr = map[errorStatus]errorStatusStr{
	errorUnresolved: errorUnresolvedStr,
	errorResolved:   errorResolvedStr,
	errorIgnored:    errorIgnoredStr,
}

const defaultListIssueLimit = 1000

// Embed all the files in the migrations directory.
//
//go:embed db/*
var dbMigrations embed.FS

type Insertable interface {
	AsInsertStmt(tz *time.Location) string
}

type Database interface {
	GetConn() (*clickhouse.Conn, error)
	GetTZ() (*time.Location, error)

	InsertErrorTrackingErrorEvent(ctx context.Context, e *et.ErrorTrackingErrorEvent) error
	InsertErrorTrackingMessageEvent(ctx context.Context, e *et.ErrorTrackingMessageEvent) error
	InsertErrorTrackingSession(ctx context.Context, e *et.ErrorTrackingSession) error

	ListErrors(params errors.ListErrorsParams) ([]*models.Error, error)
	ListEvents(params errors.ListEventsParams) ([]*models.ErrorEvent, error)
	ListMessages(params messages.ListMessagesParams) ([]*models.MessageEvent, error)

	GetError(params errors.GetErrorParams) (*models.Error, error)
	UpdateError(params errors.UpdateErrorParams) (*models.Error, error)
	DeleteProject(params projects.DeleteProjectParams) error

	Close() error
}

// Implements the Database interface.
type database struct {
	// db holds the clickhouse connection
	conn clickhouse.Conn
	tz   *time.Location
}

type DatabaseOptions struct {
	MaxOpenConns     int
	MaxIdleConns     int
	UseRemoteStorage string
	Debug            bool
}

// NOTE: This clickhouse database initialization procedure should move to a new
// location so we can handle migrations.
func NewDB(clickHouseDsn string, opts *DatabaseOptions) (Database, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	dbOpts.MaxOpenConns = opts.MaxOpenConns
	dbOpts.MaxIdleConns = opts.MaxIdleConns
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}
	dbOpts.Debug = opts.Debug

	// Requirement per
	// https://gitlab.com/ahegyi/error-tracking-data-generator#example-queries
	dbOpts.Settings["join_use_nulls"] = 1

	var db *sql.DB
	db, err = sql.Open("clickhouse", clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	clickhouseStore := &store.ClickHouseStore{
		TName: "goose_db_version_v2",
	}

	clickhouseStore.AttachOptions(map[string]string{
		"ON_CLUSTER": "true",
	})

	sqlFS, err := fs.Sub(dbMigrations, "db/migrations")
	if err != nil {
		return nil, fmt.Errorf("sql fs sub: %w", err)
	}

	provider, err := goose.NewProvider(
		"",
		db,
		sqlFS,
		goose.WithAllowOutofOrder(true),
		goose.WithStore(clickhouseStore),
	)
	if err != nil {
		return nil, fmt.Errorf("goose/clickhouse provider: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()
	if _, err := provider.Up(ctx); err != nil {
		return nil, fmt.Errorf("goose/clickhouse migrations: %w", err)
	}

	// If remote storage is enabled, also execute specific migrations
	if opts.UseRemoteStorage == string(common.GCP) {
		gcsSQLFS, err := fs.Sub(dbMigrations, "db/gcsmigrations")
		if err != nil {
			return nil, fmt.Errorf("sql fs sub: %w", err)
		}

		// NOTE: It would be better to put these migrations in the same directory as the DDLs. But due to possibility
		// of running without a remote storage backend this has to be made conditional.
		// We assume that the actual tables were create beforehand on the specific database.
		anotherProvider, err := goose.NewProvider(
			"",
			db,
			gcsSQLFS,
			goose.WithAllowOutofOrder(true),
			goose.WithStore(clickhouseStore),
		)
		if err != nil {
			return nil, fmt.Errorf("goose/clickhouse provider: %w", err)
		}

		ctx2, cancel2 := context.WithTimeout(context.Background(), time.Minute*3)
		defer cancel2()
		if _, err := anotherProvider.Up(ctx2); err != nil {
			return nil, fmt.Errorf("goose/clickhouse gcs specific migrations: %w", err)
		}
	}

	conn, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	params, err := conn.ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("getting server version: %w", err)
	}

	return &database{
		conn: conn,
		tz:   params.Timezone,
	}, nil
}

func NewCloudDB(clickHouseDsn string, opts *DatabaseOptions) (Database, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to parse clickhouse DSN: %w", err)
	}
	dbOpts.MaxOpenConns = opts.MaxOpenConns
	dbOpts.MaxIdleConns = opts.MaxIdleConns
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{Method: clickhouse.CompressionLZ4}
	dbOpts.Debug = opts.Debug

	// Requirement per
	// https://gitlab.com/ahegyi/error-tracking-data-generator#example-queries
	dbOpts.Settings["join_use_nulls"] = 1
	// higher dial timeout as cloud db connection can show variable latencies
	dbOpts.DialTimeout = time.Second * 60

	var db *sql.DB
	db, err = sql.Open("clickhouse", clickHouseDsn)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	clickhouseStore := &store.ClickHouseStore{
		TName: "goose_db_version_v2",
	}

	clickhouseStore.AttachOptions(map[string]string{
		"ON_CLUSTER": "true",
	})

	sqlFS, err := fs.Sub(dbMigrations, "db/cloud_migrations")
	if err != nil {
		return nil, fmt.Errorf("sql fs sub: %w", err)
	}

	provider, err := goose.NewProvider(
		"",
		db,
		sqlFS,
		goose.WithAllowOutofOrder(true),
		goose.WithStore(clickhouseStore),
	)
	if err != nil {
		return nil, fmt.Errorf("goose/clickhouse provider: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*3)
	defer cancel()
	if _, err := provider.Up(ctx); err != nil {
		return nil, fmt.Errorf("failed to run migrations: %w", err)
	}

	conn, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("clickhouse open: %w", err)
	}

	ctx2, cancel2 := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel2()
	err = conn.Ping(ctx2)
	if err != nil {
		return nil, fmt.Errorf("failed to ping cloud db %w", err)
	}

	params, err := conn.ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("getting server version: %w", err)
	}

	return &database{
		conn: conn,
		tz:   params.Timezone,
	}, nil
}

// helper function to insert the given value
// Currently, it is the responsibility of the caller to properly sanitize the query string.
// Check db.conn.AsyncInsert but handle SQL injection or find an alternative.
func (db *database) insert(ctx context.Context, e Insertable) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	// Default max_data_size is 100000 bytes = 100KB.
	// We use 1 MB as the async buffer size but, it has to be set in server configuration.
	// See https://clickhouse.com/docs/en/operations/settings/settings#async-insert-max-data-size for details.
	// async_insert_busy_timeout_ms also controls flush behavior which is 200 ms by default.

	// Note: Currently the insert call is set to fire and forget and does not wait for acknowledgement if the insert has
	// succeeded or not.
	//nolint:wrapcheck
	return db.conn.AsyncInsert(ctx, e.AsInsertStmt(db.tz), false)
}

func (db *database) GetConn() (*clickhouse.Conn, error) {
	return &db.conn, nil
}

func (db *database) Close() error {
	err := db.conn.Close()
	if err != nil {
		return fmt.Errorf("failed to close underlying db conn: %w", err)
	}
	return nil
}

func (db *database) GetTZ() (*time.Location, error) {
	return db.tz, nil
}

func (db *database) DeleteProject(params projects.DeleteProjectParams) error {
	runDeleteQuery := func(table string) error {
		q := ch.NewQueryBuilder()
		q.Build(
			"DELETE FROM ? ON CLUSTER '{cluster}' WHERE project_id = ?",
			ch.Identifier(table), int64(params.ID),
		)

		err := db.conn.Exec(
			q.Context(params.HTTPRequest.Context()),
			q.SQL(),
		)
		if err != nil {
			return fmt.Errorf("failed to delete project table %s: %w", table, err)
		}
		return nil
	}
	err := runDeleteQuery("gl_error_tracking_error_events_local")
	if err != nil {
		return err
	}
	err = runDeleteQuery("gl_error_tracking_error_status_local")
	if err != nil {
		return err
	}

	return err
}

const (
	period15m = "15m" // 15 minutes
	period30m = "30m" // 30 minutes
	period1h  = "1h"  // 1 hour
	period24h = "24h" // 24 hours
	period7d  = "7d"  // 7 days
	period14d = "14d" // 14 days
	period30d = "30d" // 30 days
)

var supportedTimePeriods = [7]string{
	period15m, period30m, period1h,
	period24h, period7d, period14d, period30d,
}
