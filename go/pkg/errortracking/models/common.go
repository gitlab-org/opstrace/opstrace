package models

import (
	"fmt"
	"strings"
	"time"
)

func quote(v string) string {
	return "'" + strings.NewReplacer(`\`, `\\`, `'`, `\'`).Replace(v) + "'"
}

func formatTime(value time.Time, tz *time.Location) string {
	switch value.Location().String() {
	case "Local":
		return fmt.Sprintf("toDateTime64('%d', 6)", value.UnixMicro())
	case tz.String():
		return fmt.Sprintf("toDateTime64('%s', 6,'%s')", value.Format("2006-01-02 15:04:05.000000"), tz.String())
	}
	return fmt.Sprintf("toDateTime64('%s', 6, '%s')",
		value.Format("2006-01-02 15:04:05.000000"),
		value.Location().String(),
	)
}
