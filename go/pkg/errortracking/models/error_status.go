package models

import (
	"fmt"
	"time"
)

// ErrorTrackingErrorStatus maps to the corresponding table in the clickhouse
// database.
type ErrorTrackingErrorStatus struct {
	ProjectID   uint64 `ch:"project_id"`
	Fingerprint uint32 `ch:"fingerprint"`
	// Status is a code:
	//   0 - unresolved
	//   1 - resolved
	Status uint8  `ch:"status"`
	UserID uint64 `ch:"user_id"`
	// Actor is a code:
	//   0 - status changed by user
	//   1 - status changed by system (new event happened after resolve)
	//   2 - status changed by computer (not a user)
	Actor     uint8     `ch:"actor"`
	UpdatedAt time.Time `ch:"updated_at"`
}

func (e ErrorTrackingErrorStatus) AsInsertStmt(tz *time.Location) string {
	return fmt.Sprintf("INSERT INTO gl_error_tracking_error_status VALUES (%d,%d,%d,%d,%d,%s)",
		e.ProjectID,
		e.Fingerprint,
		e.Status,
		e.UserID,
		e.Actor,
		formatTime(e.UpdatedAt, tz),
	)
}
