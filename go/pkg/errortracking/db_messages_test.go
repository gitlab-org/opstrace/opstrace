package errortracking

import (
	"context"
	e "errors"
	"fmt"
	"testing"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/restapi/operations/messages"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
)

func TestBuildListMessageQuery(t *testing.T) {
	for _, tc := range []struct {
		name     string
		params   messages.ListMessagesParams
		expected string
		args     clickhouse.Parameters
	}{
		{
			name: "should build a query with specific limit",
			params: messages.ListMessagesParams{
				ProjectID: 1,
				Limit:     int64Pointer(20),
			},
			expected: `SELECT
	event_id,
	project_id,
	timestamp,
	environment,
	level,
	message,
	actor,
	platform,
	release,
	server_name
FROM gl_error_tracking_message_events
WHERE project_id IN ( {p0:UInt64} ) LIMIT {p1:Int64}`,
			args: clickhouse.Parameters{
				"p0": "1",
				"p1": "20",
			},
		},
		{
			name: "should build a query with default limit",
			params: messages.ListMessagesParams{
				ProjectID: 1,
			},
			expected: `SELECT
	event_id,
	project_id,
	timestamp,
	environment,
	level,
	message,
	actor,
	platform,
	release,
	server_name
FROM gl_error_tracking_message_events
WHERE project_id IN ( {p0:UInt64} ) LIMIT {p1:Int64}`,
			args: clickhouse.Parameters{
				"p0": "1",
				"p1": "1000",
			},
		},
	} {
		t.Log(tc.name)
		qb := buildListMessageQuery(tc.params)
		assert.Equal(t, tc.expected, qb.SQL())
		assert.EqualValues(t, tc.args, qb.StringifyParams())
	}
}

func TestDbListMessages(t *testing.T) {
	assert := assert.New(t)
	expectedQuery := "SELECT\n\tevent_id,\n\tproject_id,\n\ttimestamp,\n\tenvironment,\n\tlevel,\n\tmessage,\n\tactor,\n\tplatform,\n\trelease,\n\tserver_name\nFROM gl_error_tracking_message_events\nWHERE project_id IN ( {p0:UInt64} ) LIMIT {p1:Int64}"
	dbMock := &MockClickhouseConn{}
	nilResults := []*models.MessageEvent(nil)
	mockErr := e.New("mock")
	database := database{
		conn: dbMock,
	}

	t.Run("Fails if Query returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to list messages: mock")
		dbMock.On("Query",
			clickhouse.Context(
				context.Background(),
				clickhouse.WithParameters(
					clickhouse.Parameters{
						"p0": "1",
						"p1": "20",
					},
				),
			),
			expectedQuery,
		).
			Times(1).Return(&MockClickhouseRows{}, mockErr)

		res, err := database.ListMessages(messages.ListMessagesParams{
			HTTPRequest: httpRequest,
			ProjectID:   1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Fails if ScanStruct returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to scan struct into ErrorTrackingMessageEvent: mock")
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*models.ErrorTrackingMessageEvent")).Return(mockErr)
		dbMock.On("Query",
			clickhouse.Context(
				context.Background(),
				clickhouse.WithParameters(
					clickhouse.Parameters{
						"p0": "1",
						"p1": "20",
					},
				),
			),
			expectedQuery,
		).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			HTTPRequest: httpRequest,
			ProjectID:   1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Fails if Close returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to close connection while reading from clickhouse: mock")
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Once()
		rows.On("Next").Times(1).Return(false).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*models.ErrorTrackingMessageEvent")).Return(nil)
		rows.On("Close").Times(1).Return(mockErr)
		dbMock.On("Query",
			clickhouse.Context(
				context.Background(),
				clickhouse.WithParameters(
					clickhouse.Parameters{
						"p0": "1",
						"p1": "20",
					},
				),
			),
			expectedQuery,
		).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			HTTPRequest: httpRequest,
			ProjectID:   1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Fails if rows.Err returns an error", func(t *testing.T) {
		expectedErr := fmt.Errorf("failed to read events from clickhouse: mock")
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Once()
		rows.On("Next").Times(1).Return(false).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*models.ErrorTrackingMessageEvent")).Return(nil)
		rows.On("Close").Times(1).Return(nil)
		rows.On("Err").Times(1).Return(mockErr)
		dbMock.On("Query",
			clickhouse.Context(
				context.Background(),
				clickhouse.WithParameters(
					clickhouse.Parameters{
						"p0": "1",
						"p1": "20",
					},
				),
			),
			expectedQuery,
		).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			HTTPRequest: httpRequest,
			ProjectID:   1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Equal(expectedErr.Error(), err.Error())
		assert.Equal(nilResults, res)
	})

	t.Run("Succeeds with 2 messages", func(t *testing.T) {
		rows := &MockClickhouseRows{}
		rows.On("Next").Times(1).Return(true).Twice()
		rows.On("Next").Times(1).Return(false).Once()
		rows.On("ScanStruct",
			mock.AnythingOfType("*models.ErrorTrackingMessageEvent")).Return(nil).Twice().Run(func(args mock.Arguments) {
			e := args.Get(0).(*et.ErrorTrackingMessageEvent)
			e.Environment = "dev"
			e.Level = "error"
		})
		rows.On("Close").Times(1).Return(nil)
		rows.On("Err").Times(1).Return(nil)
		dbMock.On("Query",
			clickhouse.Context(
				context.Background(),
				clickhouse.WithParameters(
					clickhouse.Parameters{
						"p0": "1",
						"p1": "20",
					},
				),
			),
			expectedQuery,
		).
			Times(1).Return(rows, nil)

		res, err := database.ListMessages(messages.ListMessagesParams{
			HTTPRequest: httpRequest,
			ProjectID:   1, Limit: int64Pointer(20),
		})

		dbMock.AssertExpectations(t)
		assert.Nil(err)
		assert.Equal(2, len(res))
		for i := 0; i < 2; i++ {
			assert.Equal(models.MessageEvent{Environment: "dev", Level: "error"}, *res[0])
		}
	})
}
