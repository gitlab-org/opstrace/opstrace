package testutils

import (
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/config"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/sinks/inmemory"
	"go.uber.org/zap"
)

type testServer struct {
	AppInfo  *snowplow.AppInfo
	GoodSink *inmemory.InMemorySink
	BadSink  *inmemory.InMemorySink
	Config   *config.Config
}

func NewTestServer() *testServer {
	return &testServer{
		AppInfo:  &snowplow.AppInfo{Name: "collector-test", Version: "test-version"},
		GoodSink: inmemory.New(zap.NewNop()),
		BadSink:  inmemory.New(zap.NewNop()),
		Config:   GetTestConfig(),
	}
}

func (ts *testServer) Handler(c *gin.Context) {
	service := snowplow.NewService(zap.NewNop(), ts.Config, ts.GoodSink, ts.BadSink, ts.AppInfo)
	service.Ingest(c, true)
}

func GetTestConfig() *config.Config {
	testConfig := &config.Config{
		Interface: "0.0.0.0",
		Port:      8080,
		Cookie: config.CookieConfig{
			Enabled:    true,
			Name:       "sp",
			Expiration: 365 * 24 * time.Hour, // 1 year
		},
		DoNotTrackCookie: config.DoNotTrackCookieConfig{
			Enabled: false,
			Name:    "",
			Value:   "",
		},
		CookieBounce: config.CookieBounceConfig{
			Enabled:                 false,
			Name:                    "n3pc",
			FallbackNetworkUserID:   "00000000-0000-4000-A000-000000000000",
			ForwardedProtocolHeader: "",
		},
		Streams: config.StreamsConfig{
			Good: config.SinkConfig{
				Name: "raw",
			},
			Bad: config.SinkConfig{
				Name: "bad",
			},
			UseIPAsPartitionKey: false,
		},
	}
	return testConfig
}
