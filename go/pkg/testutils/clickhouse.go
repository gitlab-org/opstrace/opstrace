package testutils

import (
	"context"
	"fmt"
	"path"
	"path/filepath"
	"runtime"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/docker/go-connections/nat"
	"github.com/testcontainers/testcontainers-go"
	testcontainersCH "github.com/testcontainers/testcontainers-go/modules/clickhouse"
	"github.com/testcontainers/testcontainers-go/wait"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/test"
)

type ClickHouseServer struct {
	*testcontainersCH.ClickHouseContainer
}

func NewClickHouseServerAndConnection(ctx context.Context) (*ClickHouseServer, driver.Conn, error) {
	// create test server
	testEnv, err := NewClickHouseServer(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("initializing CH server: %w", err)
	}

	defer func() {
		if err != nil {
			//nolint:errcheck // terminate the container on error if we can
			_ = testEnv.Terminate(ctx)
		}
	}()
	// test connection
	conn, err := testEnv.GetConnection(ctx)
	if err != nil {
		return nil, nil, fmt.Errorf("getting CH connection: %w", err)
	}
	pingCtx, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()
	if err = conn.Ping(pingCtx); err != nil {
		return nil, nil, fmt.Errorf("pinging CH server: %w", err)
	}
	return testEnv, conn, nil
}

func NewClickHouseServer(ctx context.Context) (*ClickHouseServer, error) {
	//nolint:dogsled
	_, b, _, _ := runtime.Caller(0)
	basePath := filepath.Dir(b)

	container, err := testcontainersCH.RunContainer(ctx,
		//nolint:staticcheck
		testcontainers.WithImage(constants.OpstraceImages().ClickHouseImage),
		testcontainersCH.WithDatabase(""),
		testcontainersCH.WithConfigFile(path.Join(basePath, "./clickhouse-resources/config.xml")),
		testcontainers.WithWaitStrategyAndDeadline(120*time.Second,
			wait.ForLog("Ready for connections").WithStartupTimeout(120*time.Second),
			wait.NewHTTPStrategy("/").WithPort(nat.Port("8123/tcp")).WithStatusCodeMatcher(func(status int) bool {
				return status == 200
			}).WithStartupTimeout(120*time.Second)))
	if err != nil {
		return nil, fmt.Errorf("running clickhouse container: %w", err)
	}

	return &ClickHouseServer{
		ClickHouseContainer: container,
	}, nil
}

func (c *ClickHouseServer) GetDSN(ctx context.Context, dbName string) (string, error) {
	dsn, err := c.ConnectionString(ctx)
	if err != nil {
		return "", fmt.Errorf("get connection string: %w", err)
	}
	return dsn + dbName, nil
}

func (c *ClickHouseServer) GetConnection(ctx context.Context) (driver.Conn, error) {
	dsn, err := c.GetDSN(ctx, "")
	if err != nil {
		return nil, err
	}

	opts, err := clickhouse.ParseDSN(dsn)
	if err != nil {
		return nil, fmt.Errorf("parse dsn: %w", err)
	}
	opts.DialTimeout = 10 * time.Second

	conn, err := clickhouse.Open(opts)
	if err != nil {
		return nil, fmt.Errorf("opening clickhouse connection: %w", err)
	}
	return conn, nil
}

func (c *ClickHouseServer) CreateDatabase(ctx context.Context, dbName string) error {
	conn, err := c.GetConnection(ctx)
	if err != nil {
		return err
	}
	defer func() {
		_ = conn.Close()
	}()

	if err := conn.Exec(
		ctx,
		fmt.Sprintf("CREATE DATABASE IF NOT EXISTS `%s`", dbName),
	); err != nil {
		return fmt.Errorf("creating database %s: %w", dbName, err)
	}
	return nil
}

func (c *ClickHouseServer) DropDatabase(ctx context.Context, dbName string) error {
	conn, err := c.GetConnection(ctx)
	if err != nil {
		return err
	}
	defer func() {
		_ = conn.Close()
	}()

	if err := conn.Exec(
		ctx,
		fmt.Sprintf("DROP DATABASE IF EXISTS `%s`", dbName),
	); err != nil {
		return fmt.Errorf("dropping database %s: %w", dbName, err)
	}
	return nil
}

func (c *ClickHouseServer) Exec(ctx context.Context, sql string) error {
	conn, err := c.GetConnection(ctx)
	if err != nil {
		return err
	}
	defer func() {
		_ = conn.Close()
	}()

	if err := conn.Exec(ctx, sql); err != nil {
		return fmt.Errorf("executing sql: %w", err)
	}
	return nil
}

var dbMigrations = map[string]func(context.Context, string) error{
	constants.ErrorTrackingAPIDatabaseName: test.RunErrorTrackingMigrations,
	constants.TracingDatabaseName:          test.RunTracingMigrations,
	constants.MetricsDatabaseName:          test.RunMetricsMigrations,
	constants.LoggingDatabaseName:          test.RunLoggingMigrations,
	constants.AnalyticsDatabaseName:        test.RunAnalyticsMigrations,
	constants.AlertsDatabaseName:           test.RunAlertsMigrations,
	constants.SnowplowDatabaseName:         test.RunSnowplowMigrations,
}

func (c *ClickHouseServer) CreateDatabasesAndRunMigrations(ctx context.Context, databases ...string) error {
	for _, dbName := range databases {
		mFun, ok := dbMigrations[dbName]
		if !ok {
			return fmt.Errorf("unknown database %s", dbName)
		}
		if err := c.CreateDatabase(ctx, dbName); err != nil {
			return err
		}
		dsn, err := c.GetDSN(ctx, dbName)
		if err != nil {
			return err
		}
		if err := mFun(ctx, dsn); err != nil {
			return err
		}
	}
	return nil
}

func (c *ClickHouseServer) CreateAllDatabasesAndRunMigrations(ctx context.Context) error {
	return c.CreateDatabasesAndRunMigrations(ctx,
		constants.ErrorTrackingAPIDatabaseName,
		constants.TracingDatabaseName,
		constants.MetricsDatabaseName,
		constants.LoggingDatabaseName,
		constants.AnalyticsDatabaseName,
		constants.AlertsDatabaseName,
		constants.SnowplowDatabaseName,
	)
}
