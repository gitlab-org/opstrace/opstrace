package testutils

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"net/http/httptest"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

type OidcServer struct {
	*httptest.Server
	key    *jwk.Key
	rsaKey *rsa.PrivateKey
}

//nolint:errcheck
func NewOIDCServer(listeningPort *int, privateKeyFilePath *string) (*OidcServer, error) {
	var (
		privateKey *rsa.PrivateKey
		err        error
	)
	if privateKeyFilePath != nil {
		privateKey, err = common.GetRSAPrivateKeyFromFile(*privateKeyFilePath)
	} else {
		privateKey, err = rsa.GenerateKey(rand.Reader, 2048)
	}
	if err != nil {
		return nil, fmt.Errorf("generate private key: %w", err)
	}
	// This is the key we will use to sign
	realKey, err := jwk.FromRaw(privateKey)
	if err != nil {
		return nil, fmt.Errorf("create JWK: %w", err)
	}
	err = realKey.Set(jwk.KeyIDKey, `key`)
	if err != nil {
		return nil, fmt.Errorf("set key ID: %w", err)
	}
	err = realKey.Set(jwk.AlgorithmKey, jwa.RS256)
	if err != nil {
		return nil, fmt.Errorf("set algorithm: %w", err)
	}

	// Now create a key set that tests will use to verity the signed serialized against
	// Normally these keys are available somewhere like https://www.googleapis.com/oauth2/v3/certs
	// or in GitLab's case, they are available at mygitlabinstance.com/oauth/discovery/keys,
	// e.g. https://gitlab.com/oauth/discovery/keys

	// We can use the jwk.PublicSetOf() utility to get a JWKS
	// of all the public keys.
	privateSet := jwk.NewSet()
	_ = privateSet.AddKey(realKey)
	publicSet, err := jwk.PublicSetOf(privateSet)
	if err != nil {
		return nil, fmt.Errorf("create public JWKS: %w", err)
	}
	wellKnownResponse, err := json.Marshal(publicSet)
	if err != nil {
		return nil, fmt.Errorf("JSON encode public keyset: %w", err)
	}
	oidcProvider := httptest.NewUnstartedServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/token" {
			payload := map[string]string{
				"gitlab_realm":        "self-managed",
				"gitlab_namespace_id": "22",
				"sub":                 "instance1",
			}
			token, err := token(payload, privateKey)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write([]byte(token))
			return
		}

		if r.URL.Path != "/oauth/discovery/keys" {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(wellKnownResponse)
	}))
	// if a custom listening port was provided, replace the listener
	if listeningPort != nil {
		listener, err := net.Listen("tcp", fmt.Sprintf(":%d", *listeningPort))
		if err != nil {
			return nil, fmt.Errorf("initializing custom listener: %w", err)
		}
		oidcProvider.Listener.Close()
		oidcProvider.Listener = listener
	}
	// start the server
	oidcProvider.Start()
	return &OidcServer{
		Server: oidcProvider,
		key:    &realKey,
		rsaKey: privateKey,
	}, nil
}

func (s *OidcServer) Close() {
	s.Server.Close()
}

//nolint:errcheck
func (s *OidcServer) Token(claims map[string]string) (string, error) {
	token := jwt.New()
	for k, v := range claims {
		_ = token.Set(k, v)
	}

	signed, err := jwt.Sign(token, jwt.WithKey(jwa.RS256, s.rsaKey))
	if err != nil {
		return "", fmt.Errorf("sign token: %w", err)
	}
	return string(signed), nil
}

//nolint:errcheck
func token(claims map[string]string, key *rsa.PrivateKey) (string, error) {
	token := jwt.New()
	for k, v := range claims {
		_ = token.Set(k, v)
	}

	signed, err := jwt.Sign(token, jwt.WithKey(jwa.RS256, key))
	if err != nil {
		return "", fmt.Errorf("sign token: %w", err)
	}
	return string(signed), nil
}
