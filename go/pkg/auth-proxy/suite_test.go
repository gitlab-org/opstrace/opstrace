package authproxy_test

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	uberzap "go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
)

type provider struct {
	server *httptest.Server
	url    string
	key1   *jwk.Key
	key2   *jwk.Key
}

type upstreamResponse struct {
	Path    string            `json:"path"`
	Headers map[string]string `json:"headers"`
}

var (
	logger        *uberzap.SugaredLogger
	upstream      *httptest.Server
	upstreamURL   *url.URL
	provider1     provider
	provider2     provider
	rsaPrivateKey *rsa.PrivateKey
	privateKey    *jwk.Key
)

func TestAuthProxyAPI(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "Auth Proxy API Test Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func() {
	By("setting up logger")
	setTimeEncoderOpt := func(o *zap.Options) {
		o.TimeEncoder = zapcore.RFC3339TimeEncoder
	}
	logger = zap.NewRaw(
		zap.WriteTo(GinkgoWriter),
		zap.UseDevMode(true),
		setTimeEncoderOpt,
	).Sugar()

	By("setting up upstream server")
	upstream = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// write received path and headers to response so we can verify it later
		resp := upstreamResponse{Path: r.URL.Path, Headers: getHeaders(r)}
		jsonBytes, err := json.Marshal(resp)
		Expect(err).ToNot(HaveOccurred())
		// w.Header().Set("Content-Type", "application/json")
		w.Write(jsonBytes)
	}))
	upstreamURL, _ = url.Parse(upstream.URL)

	By("setting up oidc provider 1 server")
	p1k1, p1k2, p1, err := createOIDCProvider()
	Expect(err).ToNot(HaveOccurred())
	provider1 = provider{
		server: p1,
		url:    p1.URL,
		key1:   p1k1,
		key2:   p1k2,
	}

	By("setting up oidc provider 2 server")
	p2k1, p2k2, p2, err := createOIDCProvider()
	Expect(err).ToNot(HaveOccurred())
	provider2 = provider{
		server: p2,
		url:    p2.URL,
		key1:   p2k1,
		key2:   p2k2,
	}

	rsaPrivateKey, err = rsa.GenerateKey(rand.Reader, 2048)
	Expect(err).ToNot(HaveOccurred())

	k, err := jwk.FromRaw(rsaPrivateKey)
	Expect(err).ToNot(HaveOccurred())

	k.Set(jwk.AlgorithmKey, jwa.RS256)

	privateKey = &k
})

var _ = AfterSuite(func() {
	By("tearing down the test environment")
	upstream.Close()
	provider1.server.Close()
	provider2.server.Close()
})

// createOIDCProvider is a test utility to return a fake OIDC provider with three keys.
// Two valid RS256 keys can be used to sign JWTs and the other is just a bogus key to test our code
// handles it.
func createOIDCProvider() (*jwk.Key, *jwk.Key, *httptest.Server, error) {
	privKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to generate private key: %w", err)
	}
	// This is the key we will use to sign
	realKey, err := jwk.FromRaw(privKey)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create JWK: %w", err)
	}
	realKey.Set(jwk.KeyIDKey, `key1`)
	realKey.Set(jwk.AlgorithmKey, jwa.RS256)

	privKey2, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to generate private key: %w", err)
	}
	// This is the key we will use to sign
	realKey2, err := jwk.FromRaw(privKey2)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create JWK: %w", err)
	}
	realKey2.Set(jwk.KeyIDKey, `key2`)
	realKey2.Set(jwk.AlgorithmKey, jwa.RS256)

	// For demonstration purposes, we also create a bogus key
	bogusKey, err := jwk.FromRaw([]byte("bogus"))
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create bogus JWK: %w", err)
	}
	bogusKey.Set(jwk.AlgorithmKey, jwa.NoSignature)
	bogusKey.Set(jwk.KeyIDKey, "boguskey")

	// Now create a key set that tests will use to verity the signed serialized against
	// Normally these keys are available somewhere like https://www.googleapis.com/oauth2/v3/certs
	// or in GitLab's case, they are available at mygitlabinstance.com/oauth/discovery/keys,
	// e.g. https://gitlab.com/oauth/discovery/keys
	// This key set contains two keys, the first one is the correct one

	// We can use the jwk.PublicSetOf() utility to get a JWKS
	// all of the public keys
	privateSet := jwk.NewSet()
	privateSet.AddKey(realKey)
	privateSet.AddKey(realKey2)
	privateSet.AddKey(bogusKey)
	publicSet, err := jwk.PublicSetOf(privateSet)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to create public JWKS: %w", err)
	}
	wellKnownResponse, err := json.Marshal(publicSet)
	if err != nil {
		return nil, nil, nil, fmt.Errorf("failed to JSON encode public keyset: %w", err)
	}
	oidcProvider := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/oauth/discovery/keys" {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write(wellKnownResponse)
	}))

	return &realKey, &realKey2, oidcProvider, nil
}

func getHeaders(r *http.Request) map[string]string {
	headers := make(map[string]string)

	for key, values := range r.Header {
		headers[key] = values[0]
	}

	return headers
}
