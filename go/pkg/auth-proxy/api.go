package authproxy

import (
	"crypto/rsa"
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

type AuthProxy struct {
	gitlabOidcProviders []string
	logger              *zap.SugaredLogger
	authenticator       *Authenticator
	upstream            *httputil.ReverseProxy
}

func NewAuthProxy(
	target *url.URL,
	gitlabOidcProviders []string,
	rsaPrivateKey *rsa.PrivateKey,
	logger *zap.SugaredLogger) (*AuthProxy, error) {
	if len(gitlabOidcProviders) == 0 {
		return nil, fmt.Errorf("no gitlabOidcProviders provided")
	}

	authenticator, err := NewAuthenticator(gitlabOidcProviders, rsaPrivateKey, logger)
	if err != nil {
		return nil, err
	}

	proxy := httputil.NewSingleHostReverseProxy(target)
	previous := proxy.Director
	proxy.Director = func(r *http.Request) {
		previous(r)
		proxyRewrite(r)
	}
	// TODO: add roundTripper and a buffer pool
	// p.reverseProxy.Transport = roundTripper
	// p.reverseProxy.BufferPool = pool

	return &AuthProxy{
		authenticator:       authenticator,
		gitlabOidcProviders: gitlabOidcProviders,
		logger:              logger,
		upstream:            proxy,
	}, nil
}

func (k *AuthProxy) SetRoutes(router *gin.Engine) {
	router.Any("/*path", k.handler)
}

func (k *AuthProxy) handler(x *gin.Context) {
	// check if readyz request and return early if so
	if x.Request.URL.Path == "/readyz" {
		x.Status(http.StatusOK)
		return
	}
	logger := k.logger.With(
		zap.String("method", x.Request.Method),
		zap.String("host", x.Request.Host),
		zap.String("path", x.Request.URL.Path),
		zap.String("X-Gitlab-Realm", x.GetHeader("X-Gitlab-Realm")),
		zap.String("X-Gitlab-Host-Name", x.GetHeader("X-Gitlab-Host-Name")),
		zap.String("X-Gitlab-Version", x.GetHeader("X-Gitlab-Version")),
		zap.String("X-Gitlab-Instance-Id", x.GetHeader("X-Gitlab-Instance-Id")),
		zap.String("X-Gitlab-Global-User-Id", x.GetHeader("X-Gitlab-Global-User-Id")),
		zap.String("X-GitLab-Namespace-id", x.GetHeader("X-GitLab-Namespace-id")),
		zap.String("X-GitLab-Project-id", x.GetHeader("X-GitLab-Project-id")),
		zap.String("client-ip", x.GetHeader("X-Forwarded-For")),
		zap.String("user-agent", x.GetHeader("User-Agent")))

	ok, errors := k.authenticator.Authenticate(x.Request)
	if ok {
		logger.Info("allow")
		k.upstream.ServeHTTP(x.Writer, x.Request)
	} else {
		logger.Error("deny", zap.Errors("reasons -->", errors))
		x.AbortWithStatus(http.StatusUnauthorized)
	}
}

func (k *AuthProxy) Shutdown() {
	k.authenticator.Cancel()
}

// proxyRewrite ensures Project IDs are unique across all saas and self-managed users.
func proxyRewrite(r *http.Request) {
	instanceID := r.Header.Get(constants.InstanceIDHeader)
	namespaceID := r.Header.Get(constants.NamespaceIDHeader)
	projectID := r.Header.Get(constants.ProjectIDHeader)
	realm := r.Header.Get(constants.RealmHeader)

	if realm == constants.GitlabRealmSaas {
		r.Header.Set(gobTenantIDHeader, namespaceID)
		r.Header.Set(gobNamespaceIDHeader, namespaceID)
		r.Header.Set(gobProjectIDHeader, projectID)
	}

	if realm == constants.GitlabRealmSelfManaged {
		smProjectID := fmt.Sprintf("%s_%s", instanceID, projectID)

		r.Header.Set(gobTenantIDHeader, instanceID)
		r.Header.Set(gobNamespaceIDHeader, namespaceID)
		// Bake the tenantID into the projectID to maintain compatibility with
		// existing primary indexes and search functionality.
		r.Header.Set(gobProjectIDHeader, smProjectID)
	}
	// Delete the headers that are no longer needed to ensure they are not
	// accidentally used upstream for data access.
	r.Header.Del(constants.InstanceIDHeader)
	r.Header.Del(constants.NamespaceIDHeader)
	r.Header.Del(constants.ProjectIDHeader)
}
