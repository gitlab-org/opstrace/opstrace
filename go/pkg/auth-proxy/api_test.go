package authproxy_test

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis_rate/v10"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/mock"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	alertstest "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts/test"
	authproxy "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/auth-proxy"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
	ratelimting_test "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting/test"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type TestResponseRecorder struct {
	*httptest.ResponseRecorder
	closeChannel chan bool
}

func (r *TestResponseRecorder) CloseNotify() <-chan bool {
	return r.closeChannel
}

func (r *TestResponseRecorder) closeClient() {
	r.closeChannel <- true
}

func CreateTestResponseRecorder() *TestResponseRecorder {
	return &TestResponseRecorder{
		httptest.NewRecorder(),
		make(chan bool, 1),
	}
}

type AuthRequestTestCase struct {
	description string
	// request path
	path string
	// gitlabOidcProviders
	providers []string
	// signing key from one of the providers
	signingKey string
	// jwt claims
	claims  map[string]string
	headers map[string]string
	// upstream request path
	expectUpstreamPath string
	// headers sent to upstream
	upstreamHeaders map[string]string
	// expected status code
	expectStatus int
	logContains  string
	tokenExpires time.Time
}

var _ = Context("api test", func() {
	var (
		router       *gin.Engine
		testRecorder *TestResponseRecorder
		logOutput    *testutils.SyncBuffer
	)

	BeforeEach(func() {

		gin.SetMode(gin.DebugMode)
		gin.DefaultWriter = GinkgoWriter

		// router setup
		// https://stackoverflow.com/questions/70717858/gin-reverse-proxy-tests-failing-for-interface-conversion-httptest-responsereco
		testRecorder = CreateTestResponseRecorder()
		_, router = gin.CreateTestContext(testRecorder)
		router.Use(
			ginzap.GinzapWithConfig(
				logger.Desugar(),
				&ginzap.Config{
					TimeFormat: time.RFC3339,
					UTC:        true,
				},
			),
		)
		router.Use(ginzap.RecoveryWithZap(logger.Desugar(), true))

		// logs capture
		logOutput = new(testutils.SyncBuffer)
		GinkgoWriter.TeeTo(logOutput)
	})

	AfterEach(func() {
		GinkgoWriter.ClearTeeWriters()
		testRecorder.closeClient()
	})

	Describe("initialization", func() {
		It("returns error when gitlabOidcProviders is empty", func() {
			_, err := authproxy.NewAuthProxy(upstreamURL, []string{}, nil, logger)
			Expect(err).To(HaveOccurred())
		})
		It("returns error when gitlabOidcProvider is not a valid url", func() {
			_, err := authproxy.NewAuthProxy(upstreamURL, []string{"blah"}, nil, logger)
			Expect(err).To(HaveOccurred())
		})
		It("returns error when gitlabOidcProvider is not available", func() {
			_, err := authproxy.NewAuthProxy(upstreamURL, []string{"https://myunavailableprovider.com"}, nil, logger)
			Expect(err).To(HaveOccurred())
		})
		It("accepts multiple available gitlabOidcProviders", func() {
			_, err := authproxy.NewAuthProxy(upstreamURL, []string{provider1.url, provider2.url}, nil, logger)
			Expect(err).ToNot(HaveOccurred())
		})
	})

	Context("request", func() {
		It("should serve /readyz without authentication and without proxying to upstream", func() {
			proxy, err := authproxy.NewAuthProxy(upstreamURL, []string{provider1.url}, nil, logger)
			Expect(err).ToNot(HaveOccurred())
			proxy.SetRoutes(router)

			// If this request is passed to the upstream it will return
			testReq, err := http.NewRequest(http.MethodGet, "/readyz", nil)
			Expect(err).NotTo(HaveOccurred())

			router.ServeHTTP(testRecorder, testReq)
			resp := testRecorder.Result()
			body, err := io.ReadAll(resp.Body)
			Expect(err).NotTo(HaveOccurred())

			// upstream will write the path to the response. If the body
			// doesn't contain the requested path then the proxy served
			// the response.
			Expect(string(body)).ToNot(ContainSubstring("/readyz"))
			Expect(testRecorder.Code).To(Equal(http.StatusOK))
		})

		// Run all tests over the following HTTP methods
		for _, method := range []string{
			http.MethodGet,
			http.MethodHead,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
		} {
			When(method, func() {

				It("should return 401 when request has no auth token", func() {
					proxy, err := authproxy.NewAuthProxy(upstreamURL, []string{provider1.url}, nil, logger)
					Expect(err).ToNot(HaveOccurred())
					proxy.SetRoutes(router)

					// If this request is passed to the upstream it will return
					testReq, err := http.NewRequest(method, "/test", nil)
					Expect(err).NotTo(HaveOccurred())

					router.ServeHTTP(testRecorder, testReq)
					resp := testRecorder.Result()
					body, err := io.ReadAll(resp.Body)
					Expect(err).NotTo(HaveOccurred())

					// upstream will write the path to the response. If the body
					// doesn't contain the requested path then the proxy served
					// the response.
					Expect(string(body)).ToNot(ContainSubstring("/test"))
					Expect(testRecorder.Code).To(Equal(http.StatusUnauthorized))
				})

				It("should return 401 when request has Authorization header without Bearer prefix", func() {
					proxy, err := authproxy.NewAuthProxy(upstreamURL, []string{provider1.url}, nil, logger)
					Expect(err).ToNot(HaveOccurred())
					proxy.SetRoutes(router)

					// If this request is passed to the upstream it will return
					testReq, err := http.NewRequest(method, "/test", nil)
					Expect(err).NotTo(HaveOccurred())

					testReq.Header.Add("Authorization", "blah")

					router.ServeHTTP(testRecorder, testReq)
					resp := testRecorder.Result()
					body, err := io.ReadAll(resp.Body)
					Expect(err).NotTo(HaveOccurred())

					// upstream will write the path to the response. If the body
					// doesn't contain the requested path then the proxy served
					// the response.
					Expect(string(body)).ToNot(ContainSubstring("/test"))
					Expect(testRecorder.Code).To(Equal(http.StatusUnauthorized))
				})

				It("should return 401 when request has Authorization header with Bearer prefix and malformed token", func() {
					proxy, err := authproxy.NewAuthProxy(upstreamURL, []string{provider1.url}, nil, logger)
					Expect(err).ToNot(HaveOccurred())
					proxy.SetRoutes(router)

					// If this request is passed to the upstream it will return
					testReq, err := http.NewRequest(method, "/test", nil)
					Expect(err).NotTo(HaveOccurred())

					testReq.Header.Add("Authorization", "Bearer blah")

					router.ServeHTTP(testRecorder, testReq)
					resp := testRecorder.Result()
					body, err := io.ReadAll(resp.Body)
					Expect(err).NotTo(HaveOccurred())

					// upstream will write the path to the response. If the body
					// doesn't contain the requested path then the proxy served
					// the response.
					Expect(string(body)).ToNot(ContainSubstring("/test"))
					Expect(testRecorder.Code).To(Equal(http.StatusUnauthorized))
				})

				for _, testCase := range []AuthRequestTestCase{
					{
						description: "succeed with first key from first provider",
						providers:   []string{"p1"},
						signingKey:  "p1k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "allow",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       200,
					},
					{
						description: "succeed with second key from first provider",
						providers:   []string{"p1"},
						signingKey:  "p1k2",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "allow",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       200,
					},
					{
						description: "fail with key not from provider",
						providers:   []string{"p1"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains: "deny{reasons --> 1 0  [" +
							"parsing token using provider={P1} and KID=key1: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P1} and KID=key2: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P1} and KID=boguskey: could not verify message using any of the signatures or keys" +
							"]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					{
						description: "succeed with key from second provider",
						providers:   []string{"p1", "p2"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "allow",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       200,
					},
					{
						description: "fail with expired token",
						providers:   []string{"p1", "p2"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						tokenExpires:       time.Now().AddDate(0, 0, -1),
						logContains:        "KID=key1: \"exp\" not satisfied parsing token using provider={P2}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					// Test missing headers
					{
						description: "fail with missing X-Gitlab-Realm header",
						providers:   []string{"p1", "p2"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "deny{reasons --> 1 0  [empty realm, X-Gitlab-Realm not found]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					{
						description: "fail with missing X-Gitlab-Instance-Id header",
						providers:   []string{"p1", "p2"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "deny{reasons --> 1 0  [empty instanceID, X-Gitlab-Instance-Id not found]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					// Test invalid headers
					{
						description: "fail with mismatching X-Gitlab-Realm header",
						providers:   []string{"p1"},
						signingKey:  "p1k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "self-managed",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "deny{reasons --> 1 0  [invalid realm, X-Gitlab-Realm=self-managed jwt.claims.Realm=saas]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					{
						description: "fail with mismatching X-Gitlab-Namespace-id header for SaaS realm",
						providers:   []string{"p1"},
						signingKey:  "p1k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "2",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "deny{reasons --> 1 0  [invalid namespaceID, X-GitLab-Namespace-id=2 jwt.claims.GitLabNamespaceID=1]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					{
						description: "fail with mismatching X-Gitlab-Instance-id header for self-managed realm",
						providers:   []string{"p1", "p2"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "self-managed",
							"gitlab_namespace_id": "22",
							"sub":                 "instance2",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "self-managed",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "22",
							"X-GitLab-Project-id":   "11",
						},
						logContains:        "deny{reasons --> 1 0  [invalid instanceID, X-Gitlab-Instance-Id=instance1 jwt.claims.Subject=instance2]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					// Test invalid claims
					{
						description: "fail with invalid X-Gitlab-Realm header",
						providers:   []string{"p1"},
						signingKey:  "p1k1",
						claims: map[string]string{
							"gitlab_realm":        "foobar",
							"gitlab_namespace_id": "1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "foobar",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "1",
							"X-GitLab-Project-id":   "1",
						},
						logContains:        "deny{reasons --> 1 0  [unsupported realm, jwt.claims.Realm=foobar]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						expectStatus:       401,
					},
					// Test headers sent to upstream
					{
						description: "succeed with correct upstream headers for SaaS realm",
						providers:   []string{"p1", "p2"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "22",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "22",
							"X-GitLab-Project-id":   "11",
						},
						logContains:        "allow",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						upstreamHeaders: map[string]string{
							"X-Target-Tenantid":    "22",
							"X-Target-Namespaceid": "22",
							"X-Target-Projectid":   "11",
							"X-Gitlab-Realm":       "saas",
							// all below headers should be deleted to ensure they aren't accidentally
							// relied upon for upstream data access
							"X-Gitlab-Instance-Id":  "",
							"X-GitLab-Namespace-id": "",
							"X-GitLab-Project-id":   "",
						},
						expectStatus: 200,
					},
					{
						description: "succeed with correct upstream headers for self-managed realm",
						providers:   []string{"p1", "p2"},
						signingKey:  "p2k1",
						claims: map[string]string{
							"gitlab_realm":        "self-managed",
							"gitlab_namespace_id": "22",
							"sub":                 "instance1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "self-managed",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "22",
							"X-GitLab-Project-id":   "11",
						},
						logContains:        "allow",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						upstreamHeaders: map[string]string{
							"X-Target-Tenantid":    "instance1",
							"X-Target-Namespaceid": "22",
							"X-Target-Projectid":   "instance1_11",
							"X-Gitlab-Realm":       "self-managed",
							// all below headers should be deleted to ensure they aren't accidentally
							// relied upon for upstream data access
							"X-Gitlab-Instance-Id":  "",
							"X-GitLab-Namespace-id": "",
							"X-GitLab-Project-id":   "",
						},
						expectStatus: 200,
					},
					{
						description: "succeed with privateKey for SaaS realm",
						providers:   []string{"p1", "p2"},
						signingKey:  "privateKey",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "22",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "22",
							"X-GitLab-Project-id":   "11",
						},
						logContains:        "allow",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						upstreamHeaders: map[string]string{
							"X-Target-Tenantid":    "22",
							"X-Target-Namespaceid": "22",
							"X-Target-Projectid":   "11",
							"X-Gitlab-Realm":       "saas",
							// all below headers should be deleted to ensure they aren't accidentally
							// relied upon for upstream data access
							"X-Gitlab-Instance-Id":  "",
							"X-GitLab-Namespace-id": "",
							"X-GitLab-Project-id":   "",
						},
						expectStatus: 200,
					},
					{
						description: "succeed with privateKey for self-managed realm",
						providers:   []string{"p1", "p2"},
						signingKey:  "privateKey",
						claims: map[string]string{
							"gitlab_realm":        "self-managed",
							"gitlab_namespace_id": "22",
							"sub":                 "instance1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "self-managed",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "22",
							"X-GitLab-Project-id":   "11",
						},
						logContains:        "allow",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						upstreamHeaders: map[string]string{
							"X-Target-Tenantid":    "instance1",
							"X-Target-Namespaceid": "22",
							"X-Target-Projectid":   "instance1_11",
							"X-Gitlab-Realm":       "self-managed",
							// all below headers should be deleted to ensure they aren't accidentally
							// relied upon for upstream data access
							"X-Gitlab-Instance-Id":  "",
							"X-GitLab-Namespace-id": "",
							"X-GitLab-Project-id":   "",
						},
						expectStatus: 200,
					},
					{
						description: "fail with otherPrivateKey for SaaS realm",
						providers:   []string{"p1", "p2"},
						signingKey:  "otherPrivateKey",
						claims: map[string]string{
							"gitlab_realm":        "saas",
							"gitlab_namespace_id": "22",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "saas",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "22",
							"X-GitLab-Project-id":   "11",
						},
						logContains: "deny{reasons --> 1 0  [" +
							"parsing token using provider={P1} and KID=key1: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P1} and KID=key2: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P1} and KID=boguskey: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P2} and KID=key1: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P2} and KID=key2: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P2} and KID=boguskey: could not verify message using any of the signatures or keys " +
							"parsing token using provider=rsa-private-key and KID=: could not verify message using any of the signatures or keys]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						upstreamHeaders: map[string]string{
							"X-Target-Tenantid":    "22",
							"X-Target-Namespaceid": "22",
							"X-Target-Projectid":   "11",
							"X-Gitlab-Realm":       "saas",
							// all below headers should be deleted to ensure they aren't accidentally
							// relied upon for upstream data access
							"X-Gitlab-Instance-Id":  "",
							"X-GitLab-Namespace-id": "",
							"X-GitLab-Project-id":   "",
						},
						expectStatus: 401,
					},
					{
						description: "fail with otherPrivateKey for self-managed realm",
						providers:   []string{"p1", "p2"},
						signingKey:  "otherPrivateKey",
						claims: map[string]string{
							"gitlab_realm":        "self-managed",
							"gitlab_namespace_id": "22",
							"sub":                 "instance1",
						},
						headers: map[string]string{
							"X-Gitlab-Realm":        "self-managed",
							"X-Gitlab-Instance-Id":  "instance1",
							"X-GitLab-Namespace-id": "22",
							"X-GitLab-Project-id":   "11",
						},
						logContains: "deny{reasons --> 1 0  [" +
							"parsing token using provider={P1} and KID=key1: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P1} and KID=key2: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P1} and KID=boguskey: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P2} and KID=key1: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P2} and KID=key2: could not verify message using any of the signatures or keys " +
							"parsing token using provider={P2} and KID=boguskey: could not verify message using any of the signatures or keys " +
							"parsing token using provider=rsa-private-key and KID=: could not verify message using any of the signatures or keys]}",
						path:               "/foobar",
						expectUpstreamPath: "/foobar",
						upstreamHeaders: map[string]string{
							"X-Target-Tenantid":    "instance1",
							"X-Target-Namespaceid": "22",
							"X-Target-Projectid":   "instance1_11",
							"X-Gitlab-Realm":       "self-managed",
							// all below headers should be deleted to ensure they aren't accidentally
							// relied upon for upstream data access
							"X-Gitlab-Instance-Id":  "",
							"X-GitLab-Namespace-id": "",
							"X-GitLab-Project-id":   "",
						},
						expectStatus: 401,
					},
				} {
					It(testCase.description,
						func() {
							var rsaKey *rsa.PrivateKey
							// Provider variables are dynamically generated in the BeforeSuite
							// so we need to access them inside the test case to avoid getting
							// the nil default.
							var signingKey *jwk.Key
							switch testCase.signingKey {
							case "p1k1":
								signingKey = provider1.key1
							case "p1k2":
								signingKey = provider1.key2
							case "p2k1":
								signingKey = provider2.key1
							case "p2k2":
								signingKey = provider2.key2
							case "privateKey":
								rsaKey = rsaPrivateKey
								signingKey = privateKey
							case "otherPrivateKey":
								otherKey, err := rsa.GenerateKey(rand.Reader, 2048)
								Expect(err).ToNot(HaveOccurred())
								rsaKey = otherKey
								signingKey = privateKey
							default:
								panic("invalid signing key")

							}
							var providers []string
							for _, provider := range testCase.providers {
								switch provider {
								case "p1":
									providers = append(providers, provider1.url)
								case "p2":
									providers = append(providers, provider2.url)
								default:
									panic("invalid provider")

								}
							}

							token := jwt.New()
							for k, v := range testCase.claims {
								token.Set(k, v)
							}
							if testCase.tokenExpires != (time.Time{}) {
								token.Set(jwt.ExpirationKey, testCase.tokenExpires)
							}
							signed, err := jwt.Sign(token, jwt.WithKey(jwa.RS256, *signingKey))
							Expect(err).NotTo(HaveOccurred())

							proxy, err := authproxy.NewAuthProxy(upstreamURL, providers, rsaKey, logger)
							Expect(err).ToNot(HaveOccurred())
							proxy.SetRoutes(router)

							// If this request is passed to the upstream it will return
							Expect(testCase.path).ToNot(BeEmpty())
							testReq, err := http.NewRequest(method, testCase.path, nil)
							Expect(err).NotTo(HaveOccurred())

							testReq.Header.Add("Authorization", fmt.Sprintf("Bearer %s", signed))
							for k, v := range testCase.headers {
								testReq.Header.Add(k, v)
							}

							router.ServeHTTP(testRecorder, testReq)
							resp := testRecorder.Result()
							body, err := io.ReadAll(resp.Body)
							Expect(err).NotTo(HaveOccurred())

							Expect(resp.StatusCode).To(Equal(testCase.expectStatus))
							// unmarshall upstream response and expect an error to occur if non 200 status code,
							// or validate upstream response if status code is 200
							var u upstreamResponse
							err = json.Unmarshal(body, &u)

							if testCase.expectStatus == 200 && method != http.MethodHead {
								Expect(err).NotTo(HaveOccurred())
								Expect(u.Path).To(Equal(testCase.expectUpstreamPath))

								for k, v := range testCase.upstreamHeaders {
									h := u.Headers[k]
									Expect(h).To(Equal(v))
								}
							} else {
								Expect(err).To(HaveOccurred())
							}

							// substitute dynamically generated provider endpoints
							l := strings.ReplaceAll(
								strings.ReplaceAll(testCase.logContains, "{P1}", provider1.url+"/oauth/discovery/keys"),
								"{P2}", provider2.url+"/oauth/discovery/keys")

							Expect(logOutput.String()).To(ContainSubstring(l))
						},
					)
				}
			})
		}
	})

	Context("rate limiter", func() {
		var (
			proxy  *authproxy.AuthProxy
			err    error
			signed []byte
		)

		BeforeEach(func() {
			signingKey := provider2.key1
			providers := []string{provider1.url, provider2.url}
			proxy, err = authproxy.NewAuthProxy(upstreamURL, providers, nil, logger)
			Expect(err).ToNot(HaveOccurred())
			token := jwt.New()
			token.Set("gitlab_realm", "saas")
			token.Set("gitlab_namespace_id", "22")
			signed, err = jwt.Sign(token, jwt.WithKey(jwa.RS256, *signingKey))
			Expect(err).NotTo(HaveOccurred())
		})

		It("returns 200 if no limits are set", func() {
			headers := map[string]string{
				"X-Gitlab-Realm":        "saas",
				"X-Gitlab-Instance-Id":  "instance1",
				"X-GitLab-Namespace-id": "22",
				"X-GitLab-Project-id":   "11",
			}

			testReq, err := http.NewRequest(http.MethodGet, "/foobar", nil)
			Expect(err).NotTo(HaveOccurred())
			testReq.Header.Add("Authorization", fmt.Sprintf("Bearer %s", signed))
			for k, v := range headers {
				testReq.Header.Add(k, v)
			}

			router.Use(authproxy.RateLimitingHandler(logger, ratelimiting.NewNullRateLimiter(), nil))
			proxy.SetRoutes(router)

			router.ServeHTTP(testRecorder, testReq)
			resp := testRecorder.Result()

			Expect(resp.StatusCode).To(Equal(200))
		})

		It("returns 403 is error occurred during rate limiting", func() {
			headers := map[string]string{
				"X-Gitlab-Realm":        "saas",
				"X-Gitlab-Instance-Id":  "instance1",
				"X-GitLab-Namespace-id": "22",
				"X-GitLab-Project-id":   "11",
			}
			rlMock := ratelimting_test.NewRateLimiterMock(GinkgoT())
			rlMock.On(
				"IsAllowed", mock.Anything, "22", ratelimiting.AuthLimit, uint64(1),
			).Once().Return(nil, fmt.Errorf("some error"))
			router.Use(authproxy.RateLimitingHandler(logger, rlMock, nil))
			proxy.SetRoutes(router)

			testReq, err := http.NewRequest(http.MethodGet, "/foobar", nil)
			Expect(err).NotTo(HaveOccurred())

			testReq.Header.Add("Authorization", fmt.Sprintf("Bearer %s", signed))
			for k, v := range headers {
				testReq.Header.Add(k, v)
			}

			router.ServeHTTP(testRecorder, testReq)
			resp := testRecorder.Result()

			Expect(resp.StatusCode).To(Equal(403))
		})

		It("returns 200 if within limits", func() {
			headers := map[string]string{
				"X-Gitlab-Realm":        "saas",
				"X-Gitlab-Instance-Id":  "instance1",
				"X-GitLab-Namespace-id": "22",
				"X-GitLab-Project-id":   "11",
			}

			res := &redis_rate.Result{
				Allowed: 5000,
			}
			rlMock := ratelimting_test.NewRateLimiterMock(GinkgoT())
			rlMock.On(
				"IsAllowed", mock.Anything, "22", ratelimiting.AuthLimit, uint64(1),
			).Once().Return(res, nil)

			router.Use(authproxy.RateLimitingHandler(logger, rlMock, nil))
			proxy.SetRoutes(router)

			testReq, err := http.NewRequest(http.MethodGet, "/foobar", nil)
			Expect(err).NotTo(HaveOccurred())

			testReq.Header.Add("Authorization", fmt.Sprintf("Bearer %s", signed))
			for k, v := range headers {
				testReq.Header.Add(k, v)
			}

			router.ServeHTTP(testRecorder, testReq)
			resp := testRecorder.Result()
			Expect(resp.StatusCode).To(Equal(200))
		})

		It("returns 429 if outside limits", func() {
			headers := map[string]string{
				"X-Gitlab-Realm":        "saas",
				"X-Gitlab-Instance-Id":  "instance1",
				"X-GitLab-Namespace-id": "22",
				"X-GitLab-Project-id":   "11",
			}

			res := &redis_rate.Result{
				Allowed: 0,
				Limit: redis_rate.Limit{
					Rate: 100,
				},
			}
			rlMock := ratelimting_test.NewRateLimiterMock(GinkgoT())
			rlMock.On(
				"IsAllowed", mock.Anything, "22", ratelimiting.AuthLimit, uint64(1),
			).Once().Return(res, nil)
			awMock := alertstest.NewAlertWriterMock(GinkgoT())
			awMock.On(
				"WriteAlertEvent",
				mock.Anything,
				&alerts.AlertEvent{
					ProjectID:   "11",
					TenantID:    "22",
					Description: "rate limit exceeded on path: /foobar with method: GET",
					Timestamp:   time.Time{},
					Reason:      "value of 1 goes over the limit set to 100",
					Type:        alerts.AuthLimitEvent,
					IsSaaSEvent: 1,
				},
			).Once().Return(nil)

			router.Use(authproxy.RateLimitingHandler(logger, rlMock, awMock))
			proxy.SetRoutes(router)

			testReq, err := http.NewRequest(http.MethodGet, "/foobar", nil)
			Expect(err).NotTo(HaveOccurred())

			testReq.Header.Add("Authorization", fmt.Sprintf("Bearer %s", signed))
			for k, v := range headers {
				testReq.Header.Add(k, v)
			}

			router.ServeHTTP(testRecorder, testReq)
			resp := testRecorder.Result()
			Expect(resp.StatusCode).To(Equal(429))
		})

		It("returns 429 - for traces if outside limits", func() {
			headers := map[string]string{
				"X-Gitlab-Realm":        "saas",
				"X-Gitlab-Instance-Id":  "instance1",
				"X-GitLab-Namespace-id": "22",
				"X-GitLab-Project-id":   "11",
				"X-Content-Length":      "1000",
				"X-Forwarded-Method":    http.MethodPost,
			}

			res := &redis_rate.Result{
				Allowed: 0,
				Limit: redis_rate.Limit{
					Rate: 100,
				},
			}
			rlMock := ratelimting_test.NewRateLimiterMock(GinkgoT())
			rlMock.On(
				"IsAllowed", mock.Anything, "22", ratelimiting.TracesWritesLimit, uint64(1000),
			).Once().Return(res, nil)
			awMock := alertstest.NewAlertWriterMock(GinkgoT())
			awMock.On(
				"WriteAlertEvent",
				mock.Anything,
				&alerts.AlertEvent{
					ProjectID:   "11",
					TenantID:    "22",
					Description: "rate limit exceeded on path: /v1/traces with method: POST",
					Timestamp:   time.Time{},
					Reason:      "value of 1000 goes over the limit set to 100",
					Type:        alerts.TraceRateLimitEvent,
					IsSaaSEvent: 1,
				},
			).Once().Return(nil)

			router.Use(authproxy.RateLimitingHandler(logger, rlMock, awMock))
			proxy.SetRoutes(router)

			testReq, err := http.NewRequest(http.MethodPost, "/v1/traces", nil)
			Expect(err).NotTo(HaveOccurred())

			testReq.Header.Add("Authorization", fmt.Sprintf("Bearer %s", signed))
			for k, v := range headers {
				testReq.Header.Add(k, v)
			}

			router.ServeHTTP(testRecorder, testReq)
			resp := testRecorder.Result()
			Expect(resp.StatusCode).To(Equal(429))

		})
	})
})
