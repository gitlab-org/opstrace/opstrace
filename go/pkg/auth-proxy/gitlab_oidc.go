package authproxy

import (
	"context"
	"crypto/rsa"
	"crypto/tls"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
)

const (
	gobTenantIDHeader    = "x-target-tenantid"
	gobNamespaceIDHeader = "x-target-namespaceid"
	gobProjectIDHeader   = "x-target-projectid"
)

type Claims struct {
	Realm             string
	Subject           string
	GitLabNamespaceID string
}

type Authenticator struct {
	gitlabOidcProviders []string
	// Passing in private keys enables API testing without relying on a gitlabOidcProvider.
	// It also allows GitLab instances (GDK/GCK) to connect to this backend without having a public JWKS endpoint,
	// by passing the same private key in to the `<path-to-gitlab-rails>/config/secrets.yml` file under the key
	// openid_connect_signing_key.
	rsaPrivateKeySet jwk.Set
	cancel           context.CancelFunc
	logger           *zap.SugaredLogger
	ar               *jwk.Cache
	alg              jwa.SignatureAlgorithm
}

func NewAuthenticator(
	gitlabOidcProviders []string,
	rsaPrivateKey *rsa.PrivateKey,
	logger *zap.SugaredLogger) (*Authenticator, error) {
	ctx, cancel := context.WithCancel(context.Background())
	ar := jwk.NewCache(ctx)
	op := []string{}

	for _, provider := range gitlabOidcProviders {
		keysEndpoint, err := url.Parse(provider + "/oauth/discovery/keys")
		if err != nil {
			defer cancel()
			return nil, fmt.Errorf("generating JWKS endpoint for provider %s: %w", provider, err)
		}
		p := keysEndpoint.String()
		op = append(op, p)
		// Only want to refresh this JWKS
		// when it needs to (based on Cache-Control or Expires header from
		// the HTTP response). If the calculated minimum refresh interval is less
		// than 15 minutes, don't go refreshing any earlier than 15 minutes.
		err = ar.Register(p, jwk.WithMinRefreshInterval(15*time.Minute), jwk.WithHTTPClient(getHTTPClient()))
		if err != nil {
			defer cancel()
			return nil, fmt.Errorf("registering JWKS endpoint for %s: %w", p, err)
		}
		// Fetch the JWKS once to check if the JWKS is available before we start.
		_, err = ar.Refresh(ctx, p)
		if err != nil {
			defer cancel()
			return nil, fmt.Errorf("fetching JWKS for %s: %w", p, err)
		}
	}

	set := jwk.NewSet()
	if rsaPrivateKey != nil {
		pubkey, err := jwk.PublicRawKeyOf(rsaPrivateKey)
		if err != nil {
			defer cancel()
			return nil, fmt.Errorf("generating public key from private key: %w", err)
		}
		k, err := jwk.FromRaw(pubkey)
		if err != nil {
			defer cancel()
			return nil, fmt.Errorf("generating jwk.Key from public key: %w", err)
		}

		err = k.Set(jwk.AlgorithmKey, jwa.RS256)
		if err != nil {
			defer cancel()
			return nil, fmt.Errorf("setting algorithm in jwk.Key: %w", err)
		}
		err = set.AddKey(k)
		if err != nil {
			defer cancel()
			return nil, fmt.Errorf("addning jwk.Key to set: %w", err)
		}
	}

	p := &Authenticator{
		gitlabOidcProviders: op,
		rsaPrivateKeySet:    set,
		cancel:              cancel,
		logger:              logger,
		ar:                  ar,
		alg:                 jwa.RS256, // All GitLab OIDC keys are RS256
	}

	return p, nil
}

func (p *Authenticator) Cancel() {
	p.cancel()
}

func (p *Authenticator) Authenticate(r *http.Request) (bool, []error) {
	ctx, span := instrumentation.NewSubSpan(r.Context(), "authenticator.Authenticate", trace.SpanKindServer)
	defer span.End()
	tokenStr, found := getBearerToken(r)
	if !found {
		span.SetStatus(codes.Error, "bearer token not found")
		return false, []error{fmt.Errorf("bearer token not found")}
	}

	token, ok, errors := p.AuthenticateFromToken(ctx, tokenStr)
	if !ok {
		return false, errors
	}

	claims, err := GetClaims(token)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		return false, []error{fmt.Errorf("GetClaims: %w", err)}
	}

	if err := ValidateRequest(r.Header, claims); err != nil {
		span.SetStatus(codes.Error, err.Error())
		return false, []error{err}
	}

	return true, []error{}
}

func (p *Authenticator) AuthenticateFromToken(ctx context.Context, tokenStr string) (jwt.Token, bool, []error) {
	errors := []error{}
	// Try with gitlabOidcProviders first
	for _, provider := range p.gitlabOidcProviders {
		token, errs := p.authenticateWithProvider(ctx, tokenStr, provider)
		if token != nil && errs == nil {
			return *token, true, errors
		}
		errors = append(errors, errs...)
	}
	if p.rsaPrivateKeySet.Len() > 0 {
		// Try with RSA private key.
		token, errs := p.authenticateWithKeySet(ctx, tokenStr, "rsa-private-key", p.rsaPrivateKeySet)
		if token != nil && errs == nil {
			return *token, true, errors
		}
		errors = append(errors, errs...)
	}

	return nil, false, errors
}

// authenticateWithProvider authenticates with the given provider using the provided token string.
// It will Parse and validate the JWT and verify the JWT's signature.
// It returns a jwt.Token and an error.
func (p *Authenticator) authenticateWithProvider(ctx context.Context, tokenStr, provider string) (*jwt.Token, []error) {
	ctx, span := instrumentation.NewSubSpan(ctx, "authenticator.authenticateWithProvider", trace.SpanKindClient)
	defer span.End()
	set, err := p.ar.Get(ctx, provider)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
		return nil, []error{fmt.Errorf("fetching JWKS: %w", err)}
	}

	return p.authenticateWithKeySet(ctx, tokenStr, provider, set)
}

func (p *Authenticator) authenticateWithKeySet(
	ctx context.Context,
	tokenStr, provider string,
	set jwk.Set) (*jwt.Token, []error) {
	ctx, span := instrumentation.NewSubSpan(ctx, "authenticator.authenticateWithKeySet", trace.SpanKindClient)
	defer span.End()
	var errors []error
	if set.Len() == 0 {
		span.SetStatus(codes.Error, "")
		errors = append(errors, fmt.Errorf("no keys found for provider %s", provider))
	}

	for it := set.Keys(ctx); it.Next(ctx); {
		pair := it.Pair()
		key, ok := pair.Value.(jwk.Key)
		if !ok {
			span.SetStatus(codes.Error, "")
			errors = append(errors, fmt.Errorf("invalid key type for provider %s", provider))
			continue
		}

		tok, err := jwt.Parse(
			[]byte(tokenStr),
			jwt.WithKey(p.alg, key),
			jwt.WithValidate(true),
		)
		if tok != nil && err == nil {
			return &tok, nil
		}
		span.SetStatus(codes.Error, "")
		errors = append(
			errors,
			fmt.Errorf("parsing token using provider=%s and KID=%s: %w", provider, key.KeyID(), err),
		)
	}
	return nil, errors
}

func GetClaims(token jwt.Token) (*Claims, error) {
	var realm, subject, namespaceID string
	// Tokens minted by GitLab.com and customersDot have the realm embedded as a custom
	// claim so we know if the request comes from GitLab.com or a self-managed instance
	r, ok := token.Get("gitlab_realm")
	if ok {
		realm, ok = r.(string)
		if !ok {
			return nil, fmt.Errorf("invalid realm claim")
		}
	}
	// GitLab.com issues tokens gitlab_namespace_id embedded as a custom claim. This
	// is the top-level-namespace-ID.
	n, ok := token.Get("gitlab_namespace_id")
	if ok {
		namespaceID, ok = n.(string)
		if !ok {
			return nil, fmt.Errorf("invalid namespace claim")
		}
	}
	// Self-managed instance tokens issued by customersDot have the instanceID embedded
	// in the subject claim
	s, ok := token.Get("sub")
	if ok {
		subject, ok = s.(string)
		if !ok {
			return nil, fmt.Errorf("invalid subject claim")
		}
	}

	return &Claims{Realm: realm, GitLabNamespaceID: namespaceID, Subject: subject}, nil
}

// getBearerToken extracts the Bearer token from the Authorization header of the request.
// It returns the token as a string and a boolean indicating whether a token was found.
func getBearerToken(r *http.Request) (string, bool) {
	// Retrieve the Authorization header from the request
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return "", false
	}
	// Check if the Authorization header starts with "Bearer"
	if !strings.HasPrefix(authHeader, "Bearer ") {
		return "", false
	}
	// Extract the token from the Authorization header
	// The token should follow the "Bearer" keyword and a space
	token := strings.TrimPrefix(authHeader, "Bearer ")

	return token, true
}

// ValidateRequest validates the headers against the jwt claims.
// It returns an error if the headers are invalid for the associated
// JWT claims.
func ValidateRequest(headers http.Header, claims *Claims) error {
	instanceID := headers.Get(constants.InstanceIDHeader)
	namespaceID := headers.Get(constants.NamespaceIDHeader)
	realm := headers.Get(constants.RealmHeader)

	if instanceID == "" {
		return fmt.Errorf("empty instanceID, %s not found", constants.InstanceIDHeader)
	}
	if realm == "" {
		return fmt.Errorf("empty realm, %s not found", constants.RealmHeader)
	}

	// Ensure the realm in the claim matches the realm header
	if claims.Realm != realm {
		return fmt.Errorf(
			"invalid realm, %s=%s jwt.claims.Realm=%s",
			constants.RealmHeader,
			realm,
			claims.Realm,
		)
	}
	// Ensure the realm is valid
	if claims.Realm != constants.GitlabRealmSaas && claims.Realm != constants.GitlabRealmSelfManaged {
		return fmt.Errorf("unsupported realm, jwt.claims.Realm=%s", claims.Realm)
	}
	// In SaaS we map the top level namespace ID to the tenant.
	// Validate the namespaceID in the claim matches the namespaceID header
	if namespaceID != "" && (claims.Realm == constants.GitlabRealmSaas && claims.GitLabNamespaceID != namespaceID) {
		return fmt.Errorf(
			"invalid namespaceID, %s=%s jwt.claims.GitLabNamespaceID=%s",
			constants.NamespaceIDHeader,
			namespaceID,
			claims.GitLabNamespaceID,
		)
	}
	// In self-managed we map the instance ID to the tenant.
	// Validate the Subject in the claim matches the instanceID header.
	// https://gitlab.com/gitlab-org/customers-gitlab-com/-/merge_requests/9491
	if claims.Realm == constants.GitlabRealmSelfManaged && claims.Subject != instanceID {
		return fmt.Errorf(
			"invalid instanceID, %s=%s jwt.claims.Subject=%s",
			constants.InstanceIDHeader,
			instanceID,
			claims.Subject,
		)
	}

	return nil
}

func getHTTPClient() *http.Client {
	if common.LookupEnvOrBool("TLS_SKIP_INSECURE_VERIFY", false) {
		transport := &http.Transport{
			// #nosec
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		return &http.Client{Transport: transport}
	}
	return http.DefaultClient
}
