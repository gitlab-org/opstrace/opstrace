package authproxy

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
)

func RateLimitingHandler(
	logger *zap.SugaredLogger,
	limiter ratelimiting.RateLimiter,
	aw alerts.AlertWriter,
) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var namespaceID string
		realm := ctx.GetHeader(constants.RealmHeader)

		if realm == constants.GitlabRealmSaas {
			namespaceID = ctx.GetHeader(constants.NamespaceIDHeader)
		}
		if realm == constants.GitlabRealmSelfManaged {
			// For self-managed, use combination of namespaceID and instanceID for rate limiting
			instanceID := ctx.GetHeader(constants.InstanceIDHeader)
			nsID := ctx.GetHeader(constants.NamespaceIDHeader)
			namespaceID = fmt.Sprintf("%s_%s", instanceID, nsID)
		}
		ctx.Set(ratelimiting.RootNamespaceKey, namespaceID)

		limitID := getLimitIDFromURL(ctx.Request.URL, ctx.Request.Method)
		if limitID == "" {
			limitID = ratelimiting.AuthLimit
		}
		ratelimiting.DoRateLimiting(ctx, logger.Desugar(), limiter, limitID, aw)
	}
}

// Instead of injecting headers and enforce an action on ingress, we inspect URLs to get LimitID
func getLimitIDFromURL(url *url.URL, method string) string {
	if strings.Contains(url.Path, "traces") && method == http.MethodPost {
		return ratelimiting.TracesWritesLimit
	}
	if strings.Contains(url.Path, "metrics") && method == http.MethodPost {
		return ratelimiting.MetricsWritesLimit
	}
	if strings.Contains(url.Path, "logs") && method == http.MethodPost {
		return ratelimiting.LogsWritesLimit
	}
	return ""
}
