package ratelimiting

import (
	"os"
	"path"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var _ = Context("rate limiter config load", func() {
	var tmpDir string
	var tmpFile string

	BeforeEach(func() {
		var err error
		tmpDir, err = os.MkdirTemp("", "FileObserverTestFiles")
		Expect(err).NotTo(HaveOccurred())

		tmpFile = path.Join(tmpDir, "target")
	})

	AfterEach(func() {
		// Tmp dir is different on every run, we do not need to worry about errors
		// here.
		_ = os.RemoveAll(tmpDir)
	})

	It("fails if the config does not exist", func() {
		_, err := ReadConfig("/tmp/foobar.yaml")
		Expect(err).To(HaveOccurred())
	})

	It("fails if the config is not a valid yaml", func() {
		err := os.WriteFile(tmpFile, []byte("'asf2341--asdf"), 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).To(HaveOccurred())
	})

	It("succeeds if the configuration is valid", func() {
		conf := []byte(`
---
api_version: 1
limits:
  errortracking_api_reads: 1001
  errortracking_api_writes: 999
  tracing_bytes_write_limit: 1500
  auth_proxy_limit: 100
overrides:
 groups:
   22:
    errortracking_api_reads: 10012
    errortracking_api_writes: 9991
    tracing_bytes_write_limit: 1500
    auth_proxy_limit: 100
 projects:
   1:
    errortracking_api_reads: 1001
    errortracking_api_writes: 999
    tracing_bytes_write_limit: 1500
    auth_proxy_limit: 100
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		res, err := ReadConfig(tmpFile)
		Expect(err).ToNot(HaveOccurred())
		Expect(res.Overrides.Groups["22"]).To(Equal(Limits{
			ETAPIReadsLimit:   10012,
			ETAPIWritesLimit:  9991,
			TRBytesWriteLimit: 1500,
			AuthProxyLimit:    100,
		}))
		Expect(res.Overrides.Projects["1"]).To(Equal(Limits{
			ETAPIReadsLimit:   1001,
			ETAPIWritesLimit:  999,
			TRBytesWriteLimit: 1500,
			AuthProxyLimit:    100,
		}))
		Expect(res.Limits.ETAPIReadsLimit).To(Equal(int64(1001)))
		Expect(res.Limits.ETAPIWritesLimit).To(Equal(int64(999)))
		Expect(res.Limits.TRBytesWriteLimit).To(Equal(int64(1500)))
	})

	It("fails if the config has invalid api version", func() {
		conf := []byte(`
---
api_version: 2
limits:
  errortracking_api_reads: 1001
  errortracking_api_writes: 999
  tracing_bytes_write_limit: 1500
  auth_proxy_limit: 100
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})

	It("fails if the config does not contain default limtis", func() {
		conf := []byte(`
---
api_version: 1
limits:
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})

	It("fails if the config has incomplete limits", func() {
		conf := []byte(`
---
api_version: 1
limits:
  tracing_bytes_write_limit: 1500
  auth_proxy_limit: 100
`)
		err := os.WriteFile(tmpFile, conf, 0600)
		Expect(err).NotTo(HaveOccurred())

		_, err = ReadConfig(tmpFile)
		Expect(err).Should(MatchError(ErrInvalidLimitsConfiguration))
	})

})
