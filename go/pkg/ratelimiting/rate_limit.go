//go:generate go run github.com/vektra/mockery/v2  --name RateLimiter --dir . --structname RateLimiterMock --output ./test/ --outpkg test --filename rate_limiter.go
package ratelimiting

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis_rate/v10"
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

type RateLimiter interface {
	IsAllowed(context.Context, string, string, uint64) (*redis_rate.Result, error)
	SetLimits(*LimitsConfig)
}

type RateLimiterBackend interface {
	AllowN(context.Context, string, redis_rate.Limit, int) (*redis_rate.Result, error)
}

func NewNullRateLimiter() RateLimiter {
	return &nullRateLimiter{}
}

type nullRateLimiter struct{}

func (n *nullRateLimiter) IsAllowed(context.Context, string, string, uint64) (*redis_rate.Result, error) {
	return nil, nil
}
func (n *nullRateLimiter) SetLimits(*LimitsConfig) {}

func NewRateLimiter(limiter RateLimiterBackend) RateLimiter {
	res := &rateLimiter{}

	res.ratelimitCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_rate_limit_total",
			Help: "number of requests by status code, method and path",
		},
		[]string{"id", "endpointName", "allowed"},
	)
	prometheus.MustRegister(res.ratelimitCounter)

	res.limiter = limiter

	return res
}

const (
	ETReadsLimit       = "et_read"
	ETWritesLimit      = "et_write"
	TracesWritesLimit  = "tr_write"
	LogsWritesLimit    = "log_write"
	MetricsWritesLimit = "metrics_write"
	AuthLimit          = "auth_limit"
	RootNamespaceKey   = "rootNamespaceKey"
)

type rateLimiter struct {
	ratelimitCounter *prometheus.CounterVec
	limiter          RateLimiterBackend
	config           *LimitsConfig
	configMutex      sync.RWMutex
}

func (r *rateLimiter) SetLimits(cfg *LimitsConfig) {
	r.configMutex.Lock()
	defer r.configMutex.Unlock()

	r.config = cfg
}
func (r *rateLimiter) IsAllowed(
	pCtx context.Context,
	rootNamespaceID string,
	limitID string,
	n uint64,
) (*redis_rate.Result, error) {
	ctx, cancel := context.WithTimeout(pCtx, 5*time.Second)
	defer cancel()

	perMinuteLimit, err := r.getLimit(rootNamespaceID, limitID)
	if err != nil {
		return nil, fmt.Errorf("get limit for %s : %w", limitID, err)
	}
	limitRedisKey := fmt.Sprintf("%s:%s", rootNamespaceID, limitID)
	result, err := r.limiter.AllowN(ctx, limitRedisKey, redis_rate.PerMinute(int(perMinuteLimit)), int(n))
	if err != nil {
		return nil, fmt.Errorf("unable to determine if rate per minute quota is exceeded: %w", err)
	}
	r.ratelimitCounter.WithLabelValues(
		rootNamespaceID,
		limitID,
		fmt.Sprintf("%v", result.Allowed > 0),
	)

	return result, nil
}

func (r *rateLimiter) getLimit(rootNamespaceID string, limitID string) (int64, error) {
	if r.config == nil {
		return 0, fmt.Errorf("config is not set")
	}
	r.configMutex.Lock()
	defer r.configMutex.Unlock()

	limits := Limits{}
	if override, ok := r.config.Overrides.Groups[rootNamespaceID]; ok {
		limits = override
	} else {
		limits = r.config.Limits
	}
	switch limitID {
	case ETReadsLimit:
		return limits.ETAPIReadsLimit, nil
	case ETWritesLimit:
		return limits.ETAPIWritesLimit, nil
	case TracesWritesLimit, MetricsWritesLimit, LogsWritesLimit:
		//NOTE: currently there are no explicit rate limit values for Metrics and logs.
		return limits.TRBytesWriteLimit, nil
	case AuthLimit:
		return limits.AuthProxyLimit, nil
	default:
		return 0, fmt.Errorf("unknown limit id %s", limitID)
	}
}

//nolint:funlen,nestif,cyclop
func DoRateLimiting(
	ctx *gin.Context,
	logger *zap.Logger,
	limiter RateLimiter,
	limitID string,
	alertWriter alerts.AlertWriter,
) {
	rootNamespaceID, ok := ctx.MustGet(RootNamespaceKey).(string)
	if !ok {
		panic("root namespace not found in context")
	}
	n := uint64(0)

	switch limitID {
	case LogsWritesLimit, TracesWritesLimit, MetricsWritesLimit:
		// For tracing, metrics, logs we are calculating the number of bytes ingested
		tmpN := ctx.GetHeader("X-Content-Length")
		origMethod := ctx.GetHeader("X-Forwarded-Method")
		// If forwarded-method is method (in case of auth-proxy) use the method of request itself
		if origMethod == "" {
			origMethod = ctx.Request.Method
		}
		var (
			nTmp int
			err  error
		)
		// If X-Content-Length is unavailable, directly use request content length
		if tmpN == "" {
			nTmp = int(ctx.Request.ContentLength)
		} else {
			nTmp, err = strconv.Atoi(tmpN)
		}

		if origMethod != http.MethodPost || err != nil {
			ctx.AbortWithError(
				http.StatusForbidden, fmt.Errorf(
					"unsupported HTTP request method (%s!=%s) or content-length header missing",
					origMethod, http.MethodPost,
				),
			)
			return
		}
		n = uint64(nTmp)
	case AuthLimit:
		n = 1
	default:
		panic(fmt.Sprintf("unrecognized limitID %s", limitID))
	}

	limitsData, err := limiter.IsAllowed(ctx, rootNamespaceID, limitID, n)
	if err != nil {
		ctx.AbortWithError(
			http.StatusForbidden,
			fmt.Errorf("rate-limiting failed: %w", err),
		)
		return
	}
	if limitsData == nil {
		// There are no limits set, treat it as if rate-limiting was disabled,
		// and do not set any HTTP headers.
		return
	}
	if limitsData.Allowed == 0 {
		logger.With(
			zap.String("method", ctx.Request.Method),
			zap.String("host", ctx.Request.Host),
			zap.String("path", ctx.Request.URL.Path),
			zap.String("X-Gitlab-Realm", ctx.GetHeader("X-Gitlab-Realm")),
			zap.String("X-Gitlab-Host-Name", ctx.GetHeader("X-Gitlab-Host-Name")),
			zap.String("X-Gitlab-Version", ctx.GetHeader("X-Gitlab-Version")),
			zap.String("X-Gitlab-Instance-Id", ctx.GetHeader("X-Gitlab-Instance-Id")),
			zap.String("X-Gitlab-Global-User-Id", ctx.GetHeader("X-Gitlab-Global-User-Id")),
			zap.String("X-GitLab-Namespace-id", ctx.GetHeader("X-GitLab-Namespace-id")),
			zap.String("X-GitLab-Project-id", ctx.GetHeader("X-GitLab-Project-id")),
			zap.String("client-ip", ctx.GetHeader("X-Forwarded-For")),
			zap.String("user-agent", ctx.GetHeader("User-Agent")),
			zap.Float64("retryAfter", limitsData.ResetAfter.Seconds()),
			zap.Int("limitRate", limitsData.Limit.Rate),
			zap.Int64("limitReset", time.Now().Add(limitsData.ResetAfter).Unix()),
			zap.String("limitID", limitID),
			zap.String("rootNamespaceID", rootNamespaceID),
		).Info("request rejected, limits exceeded")

		realm := ctx.GetHeader(constants.RealmHeader)
		tenantID := ""
		if realm != "" {
			tenantID = getTenantID(ctx, realm)
		} else {
			// If realm header is missing, use root namespaceID as tenant.
			// This can happen in cases when traffic is served by gatekeeper.
			tenantID = rootNamespaceID
		}
		alertType := getAlertType(limitID)
		description := fmt.Sprintf(
			"rate limit exceeded on path: %s with method: %s",
			ctx.Request.URL.Path,
			ctx.Request.Method,
		)
		reason := fmt.Sprintf("value of %d goes over the limit set to %d",
			n,
			limitsData.Limit.Rate,
		)
		isSaaS := int8(0)
		if ctx.GetHeader(constants.RealmHeader) == constants.GitlabRealmSaas {
			isSaaS = 1
		}
		if alertWriter != nil {
			alertEvent := &alerts.AlertEvent{
				ProjectID:   ctx.GetHeader(constants.ProjectIDHeader),
				TenantID:    tenantID,
				Description: description,
				Reason:      reason,
				Type:        alertType,
				IsSaaSEvent: isSaaS,
			}

			err = alertWriter.WriteAlertEvent(ctx, alertEvent)
			if err != nil {
				logger.Error("failed to write alert event", zap.Error(err))
			}
		}

		// Follow the spec from:
		//
		// https://opentelemetry.io/docs/specs/otlp/#failures-1
		// https://opentelemetry.io/docs/specs/otlp/#otlphttp-throttling
		//
		//nolint:lll
		// https://github.com/open-telemetry/opentelemetry-collector/blob/744eb93307a6607ebeb1e3da53365ab3995dc6cc/exporter/otlphttpexporter/otlp.go#L162-L167
		retryAfter := int64(limitsData.ResetAfter.Seconds())
		ctx.Header("Retry-After", strconv.FormatInt(retryAfter, 10))
		ctx.AbortWithStatus(http.StatusTooManyRequests)
	}
}

func getTenantID(ctx *gin.Context, realm string) string {
	if realm == constants.GitlabRealmSaas {
		return ctx.GetHeader(constants.NamespaceIDHeader)
	}
	if realm == constants.GitlabRealmSelfManaged {
		return ctx.GetHeader(constants.InstanceIDHeader)
	}
	return ""
}

func getAlertType(limitID string) alerts.AlertEventType {
	switch limitID {
	case TracesWritesLimit:
		return alerts.TraceRateLimitEvent
	case LogsWritesLimit:
		return alerts.LogRateLimitEvent
	case MetricsWritesLimit:
		return alerts.MetricRateLimitEvent
	case AuthLimit:
		return alerts.AuthLimitEvent
	default:
		return 0
	}
}
