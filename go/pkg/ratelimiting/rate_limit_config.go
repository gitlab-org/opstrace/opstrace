package ratelimiting

import (
	"errors"
	"fmt"
	"os"

	"sigs.k8s.io/yaml"
)

/*
---
ApiVersion: 1
limits:
  errortracking_api_reads: <uint64>
  errortracking_api_writes: <uint64>
  tracing_bytes_write_limit: <uint64>
  auth_proxy_limit: <uint64>
    ...
overrides:
  groups:
    22:
      errortracking_api_reads: <uint64>
      errortracking_api_writes: <uint64>
      tracing_bytes_write: <uint64>
  projects:
    1:
      errortracking_api_reads: <uint64>
      errortracking_api_writes: <uint64>
      tracing_bytes_write: <uint64>
*/

// Limits are set per minute
type Limits struct {
	ETAPIReadsLimit  int64 `json:"errortracking_api_reads"`
	ETAPIWritesLimit int64 `json:"errortracking_api_writes"`

	TRBytesWriteLimit int64 `json:"tracing_bytes_write_limit"`
	AuthProxyLimit    int64 `json:"auth_proxy_limit"`
}

type LimitsConfig struct {
	APIVersion uint64 `json:"api_version"`
	Limits     Limits `json:"limits"`
	Overrides  struct {
		Groups   map[string]Limits `json:"groups"`
		Projects map[string]Limits `json:"projects"`
	} `json:"overrides"`
}

var ErrInvalidLimitsConfiguration = errors.New("limits configuration is invalid")

//nolint:cyclop
func verifyConfig(in *LimitsConfig) error {
	if in.APIVersion != 1 {
		return fmt.Errorf("%w: usupported format of the limits config: %d", ErrInvalidLimitsConfiguration, in.APIVersion)
	}

	if in.Limits.ETAPIReadsLimit == 0 ||
		in.Limits.ETAPIWritesLimit == 0 ||
		in.Limits.TRBytesWriteLimit == 0 ||
		in.Limits.AuthProxyLimit == 0 {
		return fmt.Errorf("%w: empty limits", ErrInvalidLimitsConfiguration)
	}

	for k, v := range in.Overrides.Groups {
		if v.TRBytesWriteLimit == 0 || v.ETAPIWritesLimit == 0 || v.ETAPIReadsLimit == 0 || v.AuthProxyLimit == 0 {
			return fmt.Errorf("%w: empty limits for group: %s", ErrInvalidLimitsConfiguration, k)
		}
	}

	for k, v := range in.Overrides.Projects {
		if v.TRBytesWriteLimit == 0 || v.ETAPIWritesLimit == 0 || v.ETAPIReadsLimit == 0 || v.AuthProxyLimit == 0 {
			return fmt.Errorf("%w: empty limits for project: %s", ErrInvalidLimitsConfiguration, k)
		}
	}

	return nil
}

func ReadConfig(path string) (*LimitsConfig, error) {
	blob, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("error while reading limits file: %w", err)
	}
	res := new(LimitsConfig)

	err = yaml.Unmarshal(blob, res)
	if err != nil {
		return nil, fmt.Errorf("error occurred while parsing limits yaml: %w", err)
	}

	err = verifyConfig(res)
	if err != nil {
		return nil, fmt.Errorf("limits configuration is invalid: %w", err)
	}

	return res, nil
}
