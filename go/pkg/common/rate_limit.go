package common

import (
	"fmt"

	"github.com/go-redis/redis_rate/v10"
	"github.com/redis/go-redis/v9"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/fileobserver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
	"go.uber.org/zap"
)

func GetRedis(
	redisAddr, redisPassword string,
	useRedisSentinel bool,
	redisConnectionPoolSize int,
) *redis.Client {
	var redisClient *redis.Client
	if useRedisSentinel {
		redisClient = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName:    "mymaster",
			SentinelAddrs: []string{redisAddr},
			Password:      redisPassword,
			DB:            0, // use default DB
			PoolSize:      redisConnectionPoolSize,
		})
	} else {
		redisClient = redis.NewClient(&redis.Options{
			Addr:     redisAddr,
			Password: redisPassword,
			DB:       0,
			PoolSize: redisConnectionPoolSize,
		})
	}

	return redisClient
}

func GetRateLimiter(
	logger *zap.Logger,
	rateLimitingConfigFilePath string,
	redisClient *redis.Client,
) (ratelimiting.RateLimiter, func(), error) {
	var rateLimiter ratelimiting.RateLimiter

	if rateLimitingConfigFilePath != "" {
		redisRateLimiter := redis_rate.NewLimiter(redisClient)
		rateLimiter = ratelimiting.NewRateLimiter(redisRateLimiter)

		actionFn := func(_ chan struct{}) error {
			newLimits, err := ratelimiting.ReadConfig(rateLimitingConfigFilePath)
			if err != nil {
				return fmt.Errorf(
					"unable to load rate-limiting config %s: %w", rateLimitingConfigFilePath, err)
			}
			logger.Info("new rate limits have been set")
			rateLimiter.SetLimits(newLimits)
			return nil
		}

		fo := fileobserver.NewFileObserver(rateLimitingConfigFilePath, actionFn)
		err := fo.Run()
		if err != nil {
			return nil, func() {}, fmt.Errorf("fileobserver: %w", err)
		}
		return rateLimiter, func() { fo.Stop() }, nil
	} else {
		return ratelimiting.NewNullRateLimiter(), func() {}, nil
	}
}
