package common

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"math/big"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	clickhouse "github.com/ClickHouse/clickhouse-go/v2"
	jsonpatch "github.com/evanphx/json-patch/v5"
	"go.opentelemetry.io/collector/pdata/plog"
	"go.uber.org/zap"
	rbac "k8s.io/api/rbac/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	chinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

func generateRandomBytes(n int) []byte {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		panic(err)
	}
	return b
}

func RandStringRunes(s int) string {
	b := generateRandomBytes(s)
	return base64.URLEncoding.EncodeToString(b)
}

func RandStringRunesRaw(s int) string {
	b := generateRandomBytes(s)
	return base64.RawStdEncoding.EncodeToString(b)
}

var (
	specialChars = []string{"@", "!", "#", "$", "^", "(", ")", "_"}
)

// ClickHouseCloudPassword generates ClickHouse Cloud specific password which requires special characters.
// We already use secure random generated string which is then appended by special chars.
func ClickHouseCloudPassword(s int) string {
	bts := generateRandomBytes(s)
	charIndex, err := rand.Int(rand.Reader, big.NewInt(int64(len(specialChars))))
	if err != nil {
		panic(err)
	}
	b := url.QueryEscape(specialChars[charIndex.Int64()]) + base64.RawURLEncoding.EncodeToString(bts)
	return b
}

func ParseFeatureAsBool(features map[string]string, k string) (bool, error) {
	if features != nil {
		if _, ok := features[k]; ok {
			parsed, err := strconv.ParseBool(features[k])
			if err != nil {
				return false, fmt.Errorf("parsing feature as bool: %w", err)
			}
			return parsed, nil
		}
	}
	return false, nil
}

func MergeMap(base map[string]string, overrides map[string]string) map[string]string {
	merged := map[string]string{}
	if overrides == nil {
		if base == nil {
			return map[string]string{}
		}
		return base
	}

	for k, v := range base {
		merged[k] = v
	}
	for k, v := range overrides {
		merged[k] = v
	}
	return merged
}

func ValidateServiceName(string string) bool {
	// a DNS-1035 label must consist of lower case alphanumeric
	//    characters or '-', start with an alphabetic character, and end with an
	//    alphanumeric character (e.g. 'my-name',  or 'abc-123', regex used for
	//    validation is '[a-z]([-a-z0-9]*[a-z0-9])?
	b, err := regexp.MatchString("[a-z]([-a-z0-9]*[a-z0-9])?", string)
	if err != nil {
		return false
	}
	return b
}

func PatchObject(base, overrides interface{}) error {
	if overrides == nil {
		return nil
	}
	overridesSpec, err := json.Marshal(overrides)
	if err != nil {
		// TODO: Add error wrapping to satisfy wrapcheck
		//nolint:wrapcheck
		return err
	}

	baseBytes, err := json.Marshal(base)
	if err != nil {
		//nolint:wrapcheck
		return err
	}
	// Turn the patch into a patch that can be applied to the defaults
	patch, err := jsonpatch.CreateMergePatch([]byte(`{}`), overridesSpec)
	if err != nil {
		//nolint:wrapcheck
		return err
	}
	// Apply the overrides patch to the base
	withOverrides, err := jsonpatch.MergePatch(baseBytes, patch)
	if err != nil {
		//nolint:wrapcheck
		return err
	}
	// Unmarshall into original base
	if err := json.Unmarshal(withOverrides, base); err != nil {
		//nolint:wrapcheck
		return err
	}

	return nil
}

// patch the default spec (as defined in code) with the overrides (as provided by an SRE).
func PatchBytes(defaults, overrides []byte) ([]byte, error) {
	if overrides == nil {
		return defaults, nil
	}
	// Turn the patch into a patch that can be applied to the defaults
	patch, err := jsonpatch.CreateMergePatch([]byte(`{}`), overrides)
	if err != nil {
		// TODO: Add error wrapping to satisfy wrapcheck
		//nolint:wrapcheck
		return defaults, err
	}
	// Apply the overrides patch to the base
	// TODO: Add error wrapping to satisfy wrapcheck
	//nolint:wrapcheck
	return jsonpatch.MergePatch(defaults, patch)
}

func RBACObjectMutator(current, desired client.Object) error {
	switch o := current.(type) {
	case *rbac.Role:
		o.Rules = desired.(*rbac.Role).Rules
	case *rbac.RoleBinding:
		o.RoleRef = desired.(*rbac.RoleBinding).RoleRef
		o.Subjects = desired.(*rbac.RoleBinding).Subjects
	case *rbac.ClusterRole:
		o.Rules = desired.(*rbac.ClusterRole).Rules
	case *rbac.ClusterRoleBinding:
		o.RoleRef = desired.(*rbac.ClusterRoleBinding).RoleRef
		o.Subjects = desired.(*rbac.ClusterRoleBinding).Subjects
	default:
		return fmt.Errorf("unsuported kind: %s", o.GetObjectKind().GroupVersionKind().Kind)
	}
	return nil
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandStringASCIIBytes(n int) (string, error) {
	totalNum := int64(len(letterBytes))
	b := make([]byte, n)
	for i := range b {
		r, err := rand.Int(rand.Reader, big.NewInt(totalNum))
		if err != nil {
			return "", fmt.Errorf("failed to read random integers %w", err)
		}
		b[i] = letterBytes[r.Int64()]
	}

	return string(b), nil
}

func GetEnv(key, fallback string) string {
	v := os.Getenv(key)
	if len(v) == 0 {
		return fallback
	}
	return v
}

// simpleLogger is a temporary crutch until we unify our logging libraries on
// uber.Zap
type simpleLogger interface {
	Errorf(string, ...any)
	Error(...any)
	Infof(string, ...any)
	Info(...any)
}

func ServeWithGracefulShutdown(
	ctx context.Context,
	logger simpleLogger,
	srvs ...*http.Server,
) error {
	var errs []error

	for _, srv := range srvs {
		srv := srv
		go func() {
			// service connections
			if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
				logger.Errorf("failed to start server: %s\n", err)
			}
		}()
	}

	// Wait for interrupt signal to gracefully shutdown the server
	<-ctx.Done()

	logger.Info("shutting down gracefully")

	// Allow 10 seconds to flush existing connections
	ctx, cancel := context.WithTimeout(ctx, 10*time.Second)
	defer cancel()

	wg := sync.WaitGroup{}
	wg.Add(len(srvs))
	for _, srv := range srvs {
		srv := srv
		go func() {
			if err := srv.Shutdown(ctx); err != nil {
				errs = append(errs, fmt.Errorf("server shutdown error: %w", err))
			}
			wg.Done()
		}()
	}
	wg.Wait()

	logger.Info("http servers have exited")

	if len(errs) > 0 {
		return errors.Join(errs...)
	}

	return nil
}

func Ptr[T any](v T) *T {
	return &v
}

//nolint:funlen,cyclop // It is just a long switch statement
func SeverityTextToNumber(severityText string) plog.SeverityNumber {
	switch strings.ToLower(severityText) {
	case "trace":
		return plog.SeverityNumberTrace
	case "trace2":
		return plog.SeverityNumberTrace2
	case "trace3":
		return plog.SeverityNumberTrace3
	case "trace4":
		return plog.SeverityNumberTrace4
	case "debug":
		return plog.SeverityNumberDebug
	case "debug2":
		return plog.SeverityNumberDebug2
	case "debug3":
		return plog.SeverityNumberDebug3
	case "debug4":
		return plog.SeverityNumberDebug4
	case "info":
		return plog.SeverityNumberInfo
	case "info2":
		return plog.SeverityNumberInfo2
	case "info3":
		return plog.SeverityNumberInfo3
	case "info4":
		return plog.SeverityNumberInfo4
	case "warn":
		return plog.SeverityNumberWarn
	case "warn2":
		return plog.SeverityNumberWarn2
	case "warn3":
		return plog.SeverityNumberWarn3
	case "warn4":
		return plog.SeverityNumberWarn4
	case "error":
		return plog.SeverityNumberError
	case "error2":
		return plog.SeverityNumberError2
	case "error3":
		return plog.SeverityNumberError3
	case "error4":
		return plog.SeverityNumberError4
	case "fatal":
		return plog.SeverityNumberFatal
	case "fatal2":
		return plog.SeverityNumberFatal2
	case "fatal3":
		return plog.SeverityNumberFatal3
	case "fatal4":
		return plog.SeverityNumberFatal4
	default:
		// NOTE(prozlach): Not sure about this, but OTOH there is no other way
		// to represent malformed SeverityText field. Dunno if spec forsees
		// such a case.
		return plog.SeverityNumberUnspecified
	}
}

// GetDB returns the database handler for clickhouse cloud if cloud DSN is not empty, otherwise defaults to self-hosted
func GetDB(clickhouseDSN, cloudDSN string, logger *zap.Logger) (clickhouse.Conn, error) {
	if cloudDSN != "" {
		db, err := chinternal.GetDatabaseConnection(cloudDSN, nil, logger, 30*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting cloud database handle: %w", err)
		}
		return db, nil
	}

	db, err := chinternal.GetDatabaseConnection(clickhouseDSN, nil, logger, 3*time.Second)
	if err != nil {
		return nil, fmt.Errorf("getting database handle: %w", err)
	}
	return db, nil
}

// GetRSAPrivateKeyFromEnv returns a parsed rsa.PrivateKey from a given file path
func GetRSAPrivateKeyFromFile(path string) (*rsa.PrivateKey, error) {
	// Read the file
	keyData, err := os.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("reading key file: %w", err)
	}
	pemBlock, _ := pem.Decode(keyData)
	if pemBlock == nil {
		return nil, fmt.Errorf("failed to decode PEM block containing the private key")
	}
	privateKey, err := x509.ParsePKCS1PrivateKey(pemBlock.Bytes)
	if err != nil {
		return nil, fmt.Errorf("failed to parse RSA private key: %w", err)
	}
	return privateKey, nil
}

// GetRSAPrivateKeyFromEnv returns a parsed rsa.PrivateKey if a private key is provided in `OIDC_PRIVATE_KEY_PEM`
func GetRSAPrivateKeyFromEnv() (*rsa.PrivateKey, error) {
	// Check if a rsa private key has been provided
	data := GetEnv("OIDC_PRIVATE_KEY_PEM", "")
	if data == "" {
		return nil, nil
	}
	pemBlock, _ := pem.Decode([]byte(data))
	if pemBlock == nil {
		return nil, fmt.Errorf("parsing OIDC_PRIVATE_KEY_PEM returned a nil pem block")
	}
	privateKey, err := x509.ParsePKCS1PrivateKey(pemBlock.Bytes)
	if err != nil {
		return nil, fmt.Errorf("parsing OIDC_PRIVATE_KEY_PEM returned a nil pem block %w", err)
	}
	return privateKey, nil
}
