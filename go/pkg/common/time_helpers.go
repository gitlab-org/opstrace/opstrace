package common

import (
	"time"
)

const (
	Period1m  = "1m"  // 1 minute
	Period5m  = "5m"  // 5 minutes
	Period15m = "15m" // 15 minutes
	Period30m = "30m" // 30 minutes
	Period1h  = "1h"  // 1 hour
	Period4h  = "4h"  // 4 hours
	Period12h = "12h" // 12 hours
	Period24h = "24h" // 24 hours
	Period7d  = "7d"  // 7 days
	Period14d = "14d" // 14 days
	Period30d = "30d" // 30 days
)

const (
	nrSecondsMinute = 60
	nrSecondsHour   = 60 * 60
	nrSecondsDay    = 60 * 60 * 24
)

// ClickHouse-specific date-time function-names.
// See: https://clickhouse.com/docs/en/sql-reference/functions/date-time-functions
const (
	timeFuncStartMinute string = "toStartOfMinute"
	timeFuncStartHour   string = "toStartOfHour"
	timeFuncStartDay    string = "toStartOfDay"
)

//nolint:funlen
func InferTimelines(refTime time.Time, statsPeriod string) (int64, int64, int64, string) {
	var (
		delta       time.Duration
		startTime   int64
		endTime     int64
		stepSeconds int64
		timeFunc    string
	)
	switch statsPeriod {
	case Period1m:
		delta = 1 * time.Minute
		endTime = refTime.Add(time.Minute).UnixNano()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period5m:
		delta = 5 * time.Minute
		endTime = refTime.Add(time.Minute).UnixNano()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period15m:
		delta = 15 * time.Minute
		endTime = refTime.Add(time.Minute).UnixNano()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period30m:
		delta = 30 * time.Minute
		endTime = refTime.Add(time.Minute).UnixNano()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period1h:
		delta = time.Hour
		endTime = refTime.Add(time.Minute).UnixNano()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period4h:
		delta = 4 * time.Hour
		endTime = refTime.Add(time.Minute).UnixNano()
		timeFunc = timeFuncStartMinute
		stepSeconds = nrSecondsMinute
	case Period12h:
		delta = 12 * time.Hour
		endTime = refTime.Add(time.Hour).UnixNano()
		timeFunc = timeFuncStartHour
		stepSeconds = nrSecondsHour
	case Period24h:
		delta = 24 * time.Hour
		endTime = refTime.Add(time.Hour).UnixNano()
		timeFunc = timeFuncStartHour
		stepSeconds = nrSecondsHour
	case Period7d:
		delta = 7 * 24 * time.Hour
		endTime = refTime.Add(24 * time.Hour).UnixNano()
		timeFunc = timeFuncStartDay
		stepSeconds = nrSecondsDay
	case Period14d:
		delta = 14 * 24 * time.Hour
		endTime = refTime.Add(24 * time.Hour).UnixNano()
		timeFunc = timeFuncStartDay
		stepSeconds = nrSecondsDay
	case Period30d:
		delta = 30 * 24 * time.Hour
		endTime = refTime.Add(24 * time.Hour).UnixNano()
		timeFunc = timeFuncStartDay
		stepSeconds = nrSecondsDay
	default: // to 24h
		delta = 24 * time.Hour
		endTime = refTime.Add(time.Hour).UnixNano()
		timeFunc = timeFuncStartHour
		stepSeconds = nrSecondsHour
	}

	startTime = refTime.Add(-1 * delta).UnixNano()
	return startTime, endTime, stepSeconds, timeFunc
}

// NOTE(prozlach): Interval calculation in CH for second, millisecond and
// nanosecond intervals starts from 1970-01-01 00:00:00. Zero time in
// Golang starts with zero time which is `January 1, year 1,
// 00:00:00.000000000 UTC` hence we can't simply use time.Truncate function
// as leap-seconds calculation plus difference in start times will make
// things tricky. We workaround this by simply calculating absolute
// duration between CH epoch start and the startTIme, truncate it to
// multiple of interval, and re-add it to CH epoch start.
//
// See also https://clickhouse.com/docs/en/sql-reference/functions/date-time-functions#tostartofinterval
func TruncateTimeUsingCHLogic(t time.Time, interval time.Duration) time.Time {
	chEpochStart := time.Date(1970, time.January, 1, 0, 0, 0, 0, time.UTC)

	roundedDuration := t.Sub(chEpochStart).Truncate(interval)
	res := chEpochStart.Add(roundedDuration)

	return res
}
