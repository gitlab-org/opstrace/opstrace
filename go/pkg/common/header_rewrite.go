package common

import (
	"fmt"
	"net/http"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

// RewriteHeaders ensures Project IDs are unique across all saas and self-managed users.
func RewriteHeaders(headers http.Header) http.Header {
	instanceID := headers.Get(constants.InstanceIDHeader)
	namespaceID := headers.Get(constants.NamespaceIDHeader)
	projectID := headers.Get(constants.ProjectIDHeader)
	realm := headers.Get(constants.RealmHeader)

	if realm == constants.GitlabRealmSaas {
		headers.Set(constants.GobTenantIDHeader, namespaceID)
		headers.Set(constants.GobNamespaceIDHeader, namespaceID)
		headers.Set(constants.GobProjectIDHeader, projectID)
	}

	if realm == constants.GitlabRealmSelfManaged {
		smProjectID := fmt.Sprintf("%s_%s", instanceID, projectID)

		headers.Set(constants.GobTenantIDHeader, instanceID)
		headers.Set(constants.GobNamespaceIDHeader, namespaceID)
		// Bake the tenantID into the projectID to maintain compatibility with
		// existing primary indexes and search functionality.
		headers.Set(constants.GobProjectIDHeader, smProjectID)
	}
	// Delete the headers that are no longer needed to ensure they are not
	// accidentally used upstream for data access.
	headers.Del(constants.InstanceIDHeader)
	headers.Del(constants.NamespaceIDHeader)
	headers.Del(constants.ProjectIDHeader)
	return headers
}

func GetGOBProjectIDHeader(headers http.Header) string {
	return headers.Get(constants.GobProjectIDHeader)
}

func GetGOBTenantIDHeader(headers http.Header) string {
	return headers.Get(constants.GobTenantIDHeader)
}

func GetGOBNamespaceIDHeader(headers http.Header) string {
	return headers.Get(constants.GobNamespaceIDHeader)
}
