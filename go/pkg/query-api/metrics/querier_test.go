package metrics

import (
	"context"
	"strconv"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Context("metrics", func() {
	var (
		defaultProjectID  = "12345"
		defaultMetricName = "cpu_seconds_total"
		refTime           = time.Now().UTC()
		defaultStartTime  = refTime.Add(-1 * 24 * time.Hour)
		defaultEndTime    = refTime
	)

	DescribeTable(
		"validating metrics queries",
		func(pid string, startTS, endTS time.Time, q *Query, expectedErr error) {
			err := validateMetricQuery(pid, startTS, endTS, q)
			if expectedErr == nil {
				Expect(err).ToNot(HaveOccurred())
			} else {
				Expect(err).To(Equal(expectedErr))
			}
		},
		Entry(
			"valid query",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			nil,
		),
		Entry(
			"query with project ID unset",
			"",
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			common.ErrUnknownProjectID,
		),
		Entry(
			"query with metric name missing",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetType: pmetric.MetricTypeGauge.String(),
			},
			errUnknownMetricName,
		),
		Entry(
			"query with metric type missing",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
			},
			errUnknownMetricType,
		),
		Entry(
			"query with unsupported metric type",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   "foo",
			},
			errUnsupportedMetricType,
		),
		Entry(
			"query with metric type cased differently",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   strings.ToUpper(pmetric.MetricTypeGauge.String()),
			},
			nil,
		),
	)

	DescribeTable(
		"building metrics queries",
		func(pid string, startTS, endTS time.Time, query *Query, expectedQuery string, args clickhouse.Parameters) {
			mtype := getMetricTypeFromString(query.TargetType)
			Expect(mtype).ToNot(Equal(pmetric.MetricTypeEmpty))

			mHandler := getMetricHandlerForMetricType(mtype)
			Expect(mHandler).ToNot(BeNil())

			ctx := context.Background()
			qb, err := mHandler.buildQuery(ctx, pid, startTS, endTS, query)
			Expect(err).ToNot(HaveOccurred())
			err = testutils.CompareSQLStrings(qb.SQL(), expectedQuery)
			Expect(err).ToNot(HaveOccurred())
			Expect(qb.StringifyParams()).To(Equal(args))
		},
		Entry(
			"to fetch sample data for a sample gauge metric",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			`
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  groupArray(AggTimeUnix) AS Times,
  groupArray(AggValue) AS Values,
  any(AggAttributes) AS FilteredAttributes FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  any(Attributes) AS AggAttributes,
  AggTimeUnix,
  Fingerprint,
  sum(SumValue)/sum(CountValue) AS AggValue FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  maxMapMerge(Attributes) AS Attributes,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  minMerge(MinValueState) AS MinValue,
  maxMerge(MaxValueState) AS MaxValue,
  anyLastMerge(LastValueState) AS LastValue,
  AggTimeUnix
FROM metrics.metrics_main_gauge_1h_v2
WHERE
  ProjectId = {p0:String}
  AND MetricName = {p1:String}
  AND AggTimeUnix >= toStartOfHour({p2:DateTime64(9, 'UTC')})
  AND AggTimeUnix <= toStartOfHour({p3:DateTime64(9, 'UTC')})
  AND (1 = 1)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint
ORDER BY MetricName, Fingerprint`,
			clickhouse.Parameters{
				"p0": defaultProjectID,
				"p1": defaultMetricName,
				"p2": strconv.FormatInt(defaultStartTime.UnixNano(), 10),
				"p3": strconv.FormatInt(defaultEndTime.UnixNano(), 10),
			},
		),
		Entry(
			"to fetch sample data for a sample gauge metric with filter-attributes",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters: []Filter{
					{
						Key: "cpu", Operator: "eq", Value: "1",
					},
					{
						Key: "state", Operator: "eq", Value: "idle",
					},
				},
			},
			`
WITH matched_fingerprints AS (
SELECT
  DISTINCT Fingerprint
FROM (
  SELECT
    ProjectId,
    MetricName,
    Fingerprint,
    maxMapMerge(Attributes) AS Attributes
  FROM
    metrics.metrics_main_gauge_1h_v2
  WHERE
    ProjectId = {p0:String}
    AND MetricName = {p1:String}
    AND AggTimeUnix >= toStartOfHour({p2:DateTime64(9, 'UTC')})
    AND AggTimeUnix <= toStartOfHour({p3:DateTime64(9, 'UTC')})
  GROUP BY ProjectId, MetricName, Fingerprint, AggTimeUnix
)
WHERE ProjectId = {p4:String} AND MetricName = {p5:String} AND Attributes[{p6:String}] = {p7:String} AND Attributes[{p8:String}] = {p9:String} )
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  groupArray(AggTimeUnix) AS Times,
  groupArray(AggValue) AS Values,
  any(AggAttributes) AS FilteredAttributes FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  any(Attributes) AS AggAttributes,
  AggTimeUnix,
  Fingerprint,
  sum(SumValue)/sum(CountValue) AS AggValue FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  maxMapMerge(Attributes) AS Attributes,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  minMerge(MinValueState) AS MinValue,
  maxMerge(MaxValueState) AS MaxValue,
  anyLastMerge(LastValueState) AS LastValue,
  AggTimeUnix
FROM metrics.metrics_main_gauge_1h_v2
WHERE
  ProjectId = {p10:String}
  AND MetricName = {p11:String}
  AND AggTimeUnix >= toStartOfHour({p12:DateTime64(9, 'UTC')})
  AND AggTimeUnix <= toStartOfHour({p13:DateTime64(9, 'UTC')})
  AND Fingerprint IN matched_fingerprints
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint
ORDER BY MetricName, Fingerprint`,
			clickhouse.Parameters{
				"p0":  defaultProjectID,
				"p1":  defaultMetricName,
				"p2":  strconv.FormatInt(defaultStartTime.UnixNano(), 10),
				"p3":  strconv.FormatInt(defaultEndTime.UnixNano(), 10),
				"p4":  defaultProjectID,
				"p5":  defaultMetricName,
				"p6":  "cpu",
				"p7":  "1",
				"p8":  "state",
				"p9":  "idle",
				"p10": defaultProjectID,
				"p11": defaultMetricName,
				"p12": strconv.FormatInt(defaultStartTime.UnixNano(), 10),
				"p13": strconv.FormatInt(defaultEndTime.UnixNano(), 10),
			},
		),
		Entry(
			"to fetch sample data for a gauge metric for last 1h",
			defaultProjectID,
			refTime.Add(-1*time.Hour),
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			`
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  groupArray(AggTimeUnix) AS Times,
  groupArray(AggValue) AS Values,
  any(AggAttributes) AS FilteredAttributes FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  any(Attributes) AS AggAttributes,
  AggTimeUnix,
  Fingerprint,
  sum(SumValue)/sum(CountValue) AS AggValue FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  maxMapMerge(Attributes) AS Attributes,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  minMerge(MinValueState) AS MinValue,
  maxMerge(MaxValueState) AS MaxValue,
  anyLastMerge(LastValueState) AS LastValue,
  AggTimeUnix
FROM metrics.metrics_main_gauge_1m_v2
WHERE
  ProjectId = {p0:String}
  AND MetricName = {p1:String}
  AND AggTimeUnix >= toStartOfMinute({p2:DateTime64(9, 'UTC')})
  AND AggTimeUnix <= toStartOfMinute({p3:DateTime64(9, 'UTC')})
  AND (1 = 1)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint
ORDER BY MetricName, Fingerprint`,
			clickhouse.Parameters{
				"p0": defaultProjectID,
				"p1": defaultMetricName,
				"p2": strconv.FormatInt(refTime.Add(-1*time.Hour).UnixNano(), 10),
				"p3": strconv.FormatInt(defaultEndTime.UnixNano(), 10),
			},
		),
		Entry(
			"to fetch sample data for a sum metric for last 73 hours",
			defaultProjectID,
			refTime.Add(-73*time.Hour),
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeSum.String(),
			},
			`
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  groupArray(AggTimeUnix) AS Times,
  groupArray(AggValue) AS Values,
  any(AggAttributes) AS FilteredAttributes FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  any(Attributes) AS AggAttributes,
  AggTimeUnix,
  Fingerprint,
  sum(SumValue) AS AggValue FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  maxMapMerge(Attributes) AS Attributes,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  minMerge(MinValueState) AS MinValue,
  maxMerge(MaxValueState) AS MaxValue,
  AggTimeUnix
FROM metrics.metrics_main_sum_1d_v2
WHERE
  ProjectId = {p0:String}
  AND MetricName = {p1:String}
  AND AggTimeUnix >= toStartOfDay({p2:DateTime64(9, 'UTC')})
  AND AggTimeUnix <= toStartOfDay({p3:DateTime64(9, 'UTC')})
  AND (1 = 1)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint
ORDER BY MetricName, Fingerprint`,
			clickhouse.Parameters{
				"p0": defaultProjectID,
				"p1": defaultMetricName,
				"p2": strconv.FormatInt(refTime.Add(-73*time.Hour).UnixNano(), 10),
				"p3": strconv.FormatInt(defaultEndTime.UnixNano(), 10),
			},
		),
		Entry(
			"to fetch sample data for a sample sum metric with filter-attributes",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeSum.String(),
				Filters: []Filter{
					{
						Key: "cpu", Operator: "eq", Value: "1",
					},
					{
						Key: "state", Operator: "eq", Value: "idle",
					},
				},
			},
			`
WITH matched_fingerprints AS (
SELECT
  DISTINCT Fingerprint
FROM (
  SELECT
    ProjectId,
    MetricName,
    Fingerprint,
    maxMapMerge(Attributes) AS Attributes
  FROM
    metrics.metrics_main_sum_1h_v2
  WHERE
    ProjectId = {p0:String}
    AND MetricName = {p1:String}
    AND AggTimeUnix >= toStartOfHour({p2:DateTime64(9, 'UTC')})
    AND AggTimeUnix <= toStartOfHour({p3:DateTime64(9, 'UTC')})
  GROUP BY ProjectId, MetricName, Fingerprint, AggTimeUnix
)
WHERE ProjectId = {p4:String} AND MetricName = {p5:String} AND Attributes[{p6:String}] = {p7:String} AND Attributes[{p8:String}] = {p9:String} )
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  groupArray(AggTimeUnix) AS Times,
  groupArray(AggValue) AS Values,
  any(AggAttributes) AS FilteredAttributes FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  any(Attributes) AS AggAttributes,
  AggTimeUnix,
  Fingerprint,
  sum(SumValue) AS AggValue FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  maxMapMerge(Attributes) AS Attributes,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  minMerge(MinValueState) AS MinValue,
  maxMerge(MaxValueState) AS MaxValue,
  AggTimeUnix
FROM metrics.metrics_main_sum_1h_v2
WHERE
  ProjectId = {p10:String}
  AND MetricName = {p11:String}
  AND AggTimeUnix >= toStartOfHour({p12:DateTime64(9, 'UTC')})
  AND AggTimeUnix <= toStartOfHour({p13:DateTime64(9, 'UTC')})
  AND Fingerprint IN matched_fingerprints
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Fingerprint
ORDER BY MetricName, Fingerprint`,
			clickhouse.Parameters{
				"p0":  defaultProjectID,
				"p1":  defaultMetricName,
				"p2":  strconv.FormatInt(defaultStartTime.UnixNano(), 10),
				"p3":  strconv.FormatInt(defaultEndTime.UnixNano(), 10),
				"p4":  defaultProjectID,
				"p5":  defaultMetricName,
				"p6":  "cpu",
				"p7":  "1",
				"p8":  "state",
				"p9":  "idle",
				"p10": defaultProjectID,
				"p11": defaultMetricName,
				"p12": strconv.FormatInt(defaultStartTime.UnixNano(), 10),
				"p13": strconv.FormatInt(defaultEndTime.UnixNano(), 10),
			},
		),
		Entry(
			"to fetch sample data for a sample histogram metric",
			defaultProjectID,
			defaultStartTime,
			defaultEndTime,
			&Query{
				TargetMetric: defaultMetricName,
				TargetType:   pmetric.MetricTypeHistogram.String(),
			},
			`
SELECT
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  any(Attributes) AS Attributes,
  groupArray(Avg) AS Values,
  groupArray(TimeUnix) AS Times
FROM
(
  SELECT
    MetricName,
    MetricDescription,
    MetricUnit,
    Fingerprint,
    Attributes,
    (Sum/Count) AS Avg,
    TimeUnix
  FROM metrics.metrics_main_histogram
  WHERE
	ProjectId = {p0:String}
	AND MetricName = {p1:String}
	AND TimeUnix >= {p2:DateTime64(9, 'UTC')}
	AND TimeUnix < {p3:DateTime64(9, 'UTC')}
	AND (1 = 1)
  ORDER BY MetricName, Fingerprint, TimeUnix
)
GROUP BY Fingerprint
`,
			clickhouse.Parameters{
				"p0": defaultProjectID,
				"p1": defaultMetricName,
				"p2": strconv.FormatInt(defaultStartTime.UnixNano(), 10),
				"p3": strconv.FormatInt(defaultEndTime.UnixNano(), 10),
			},
		),
	)
})
