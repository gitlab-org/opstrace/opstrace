package metrics

import (
	"context"
	"fmt"
	"maps"
	"sort"
	"sync"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	chinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
)

type chQuerier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

// ascertain interface implementation
var _ Querier = (*chQuerier)(nil)

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		err error
	)
	if clickHouseCloudDSN != "" {
		db, err = chinternal.GetDatabaseConnection(clickHouseCloudDSN, opts, logger, 30*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting cloud database handle: %w", err)
		}
	} else {
		db, err = chinternal.GetDatabaseConnection(clickHouseDSN, opts, logger, 3*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting database handle: %w", err)
		}
	}

	return &chQuerier{
		db:     db,
		logger: logger,
	}, nil
}

// GetMetricNames returns a MetricNameResponse which contains a list of all
// distinct metric-names ingested into the metrics subsystem.
//
// Within our current design/implementation, we query data from multiple metric
// schemas in a concurrent manner to optimize for query-latency. `chQuerier.getSources`
// internally returns a map of all registered table-names to their corresponding
// fetcher methods, which are then invoked inside a separate goroutine per table-name
// concurrently. Once all queries are complete, the results are clubbed together and
// filtered as needed, before returning within the response.

//nolint:funlen,cyclop
func (q *chQuerier) GetMetricNames(
	ctx context.Context, projectID string, filters *MetricNameFilters,
) (*MetricNameResponse, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.GetMetricNames", trace.SpanKindClient)
	defer span.End()

	// ensure empty responses when applicable
	metrics := make([]MetricName, 0)
	attributes := make(map[string]interface{})
	correlationMetadata := make(map[string]map[string]time.Time)

	// guards metrics, attributes & correlationMetadata above
	var metricsGuard sync.Mutex

	// allows querying all registered metrics sources
	errGroup := new(errgroup.Group)

	// when filtering with trace IDs is requested, we perform that lookup first
	// to ensure we can narrow down the response as early as possible
	filteredNames := make(map[string]struct{})
	if filters.TraceID != "" {
		for mtype, getter := range q.getCorrelationMetadataSources(projectID, filters) {
			q.logger.Debug("querying correlation metadata sources", zap.String("metric type", mtype))
			mtype := mtype
			getter := getter
			errGroup.Go(
				func() error {
					queryCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
					defer cancel()

					var getterSpan trace.Span
					queryCtx, getterSpan = instrumentation.NewSubSpan(
						queryCtx,
						fmt.Sprintf("chQuerier.CorrelationMetadataGetter.%s", mtype),
						trace.SpanKindClient,
					)
					defer getterSpan.End()

					m, err := getter(queryCtx)
					if err != nil {
						q.logger.Error("querying for metric correlation metadata", zap.Error(err))
						return err
					}

					if _, ok := correlationMetadata[mtype]; !ok {
						correlationMetadata[mtype] = make(map[string]time.Time)
					}
					metricsGuard.Lock()
					maps.Copy(correlationMetadata[mtype], m)
					metricsGuard.Unlock()

					// keep a record of all metrics collected during this search
					for mname := range m {
						filteredNames[mname] = struct{}{}
					}
					return nil
				},
			)
		}
	}

	// wait for all metrics sources to have been queried
	if err := errGroup.Wait(); err != nil {
		return nil, fmt.Errorf("querying for metric names and attributes: %w", err)
	}

	// reset errGroup before the other searches are performed
	errGroup = new(errgroup.Group)

	// when filtering by trace_id is requested and no corresponding metrics are found,
	// we can skip searching for their dimensions/metadata
	lookupByTraceIDEmpty := filters.TraceID != "" && len(filteredNames) == 0

	if !lookupByTraceIDEmpty {
		// only when the query hasn't explicitly asked for searching a specific
		// metric by name, we can use the filtered names from the trace ID lookup
		exactMatchesRequested := filters.ExactMatch != nil && len(filters.ExactMatch) > 0

		if !exactMatchesRequested {
			if len(filteredNames) > 0 {
				filters.ExactMatch = make([]string, 0)
				for m := range filteredNames {
					filters.ExactMatch = append(filters.ExactMatch, m)
				}
			}
		}
		// search for metric names and related metadata
		for k, v := range q.getSources(projectID, filters) {
			q.logger.Debug("querying name getter", zap.String("tablename", k))
			v := v // construct loop variable
			k := k
			errGroup.Go(
				func() error {
					queryCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
					defer cancel()

					var getterSpan trace.Span
					queryCtx, getterSpan = instrumentation.NewSubSpan(
						queryCtx,
						fmt.Sprintf("chQuerier.NameGetter.%s", k),
						trace.SpanKindClient,
					)
					defer getterSpan.End()

					m, err := v(queryCtx)
					if err != nil {
						q.logger.Error("querying metric table for distinct names", zap.Error(err))
						return err
					}

					metricsGuard.Lock()
					defer metricsGuard.Unlock()
					metrics = append(metrics, m...)
					return nil
				},
			)
		}
	}

	// for metric attributes
	for k, v := range q.getAttributesSources(projectID) {
		q.logger.Debug("querying attributes getter", zap.String("tablename", k))
		v := v // construct loop variable
		k := k
		errGroup.Go(
			func() error {
				queryCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
				defer cancel()

				var getterSpan trace.Span
				queryCtx, getterSpan = instrumentation.NewSubSpan(
					queryCtx,
					fmt.Sprintf("chQuerier.AttributesGetter.%s", k),
					trace.SpanKindClient,
				)
				defer getterSpan.End()

				m, err := v(queryCtx)
				if err != nil {
					q.logger.Error("querying metric table for attributes", zap.Error(err))
					return err
				}

				metricsGuard.Lock()
				defer metricsGuard.Unlock()
				for attr := range m {
					attributes[attr] = struct{}{}
				}
				return nil
			},
		)
	}

	// wait for all metrics sources to have been queried
	if err := errGroup.Wait(); err != nil {
		return nil, fmt.Errorf("querying for metric names and attributes: %w", err)
	}

	// sort results since they collect metrics of multiple types/sources
	sort.Sort(ByMetricName(metrics))

	// stitch correlation metadata with the searched metrics
	for idx, m := range metrics {
		if _, ok := correlationMetadata[m.Type]; ok {
			if _, ok := correlationMetadata[m.Type][m.Name]; ok {
				metrics[idx].TraceIDOccurence = correlationMetadata[m.Type][m.Name].UnixNano()
			}
		}
	}

	// limit the response as was requested
	if filters.Limit > 0 && filters.Limit <= len(metrics) {
		metrics = metrics[0:filters.Limit]
	}

	// unroll all collected attributes into a slice
	allAttributes := make([]string, 0)
	for attr := range attributes {
		allAttributes = append(allAttributes, attr)
	}
	sort.Strings(allAttributes)

	return &MetricNameResponse{
		Metrics:    metrics,
		Attributes: allAttributes,
	}, nil
}

// GetMetrics returns a response specific to the type of metric being queried,
// which contains data ingested into the metrics subsystem that matches a given
// search criteria, which is internally modeled via a QueryContext object.
func (q *chQuerier) GetMetrics(ctx context.Context, queryContext *QueryContext) (interface{}, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.GetMetrics", trace.SpanKindClient)
	defer span.End()

	if queryContext == nil {
		return nil, nil // nothing to do
	}

	if queryContext.ProjectID == "" {
		return nil, common.ErrUnknownProjectID
	}

	if len(queryContext.Queries) > 1 {
		panic("multiple queries with expressions are not supported yet")
	}

	for alias, query := range queryContext.Queries {
		q.logger.Debug(
			"processing query",
			zap.String("query alias", alias),
			zap.Any("query", query),
		)

		mtype := getMetricTypeFromString(query.TargetType)
		if mtype == pmetric.MetricTypeEmpty {
			return nil, errUnknownMetricType
		}

		mHandler := getMetricHandlerForMetricType(mtype)
		if mHandler == nil {
			return nil, fmt.Errorf("couldn't get a metric handler for type: %s", mtype.String())
		}
		if err := mHandler.setup(q.db, q.logger); err != nil {
			return nil, fmt.Errorf("setting up metric handler for type: %s: %w", mtype.String(), err)
		}

		response, err := mHandler.executeQuery(
			ctx,
			queryContext.ProjectID,
			queryContext.StartTimestamp,
			queryContext.EndTimestamp,
			query,
		)
		if err != nil {
			return nil, fmt.Errorf("executing query for type: %s: %w", mtype.String(), err)
		}

		return response, nil
	}

	return nil, nil
}

// GetSearchMetadata returns search metadata specific to the query being performed which is
// used by components of the query builder to populate attributes, supported aggregations etc.
func (q *chQuerier) GetSearchMetadata(ctx context.Context, queryContext *QueryContext) (interface{}, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.GetSearchMetadata", trace.SpanKindClient)
	defer span.End()

	if queryContext == nil {
		return nil, nil // nothing to do
	}

	if queryContext.ProjectID == "" {
		return nil, common.ErrUnknownProjectID
	}

	if len(queryContext.Queries) > 1 {
		panic("multiple queries via expressions are not supported yet")
	}

	for alias, query := range queryContext.Queries {
		q.logger.Debug(
			"processing search metadata for query",
			zap.String("query alias", alias),
			zap.Any("query", query),
		)

		mtype := getMetricTypeFromString(query.TargetType)
		if mtype == pmetric.MetricTypeEmpty {
			return nil, errUnknownMetricType
		}

		mHandler := getMetricHandlerForMetricType(mtype)
		if mHandler == nil {
			return nil, fmt.Errorf("couldn't get a metric handler for type: %s", mtype.String())
		}
		if err := mHandler.setup(q.db, q.logger); err != nil {
			return nil, fmt.Errorf("setting up metric handler for type: %s: %w", mtype.String(), err)
		}

		response, err := mHandler.getSearchMetadata(
			ctx,
			queryContext.ProjectID,
			query,
		)
		if err != nil {
			return nil, fmt.Errorf("executing search metadata query for type: %s: %w", mtype.String(), err)
		}
		return response, nil
	}

	return nil, nil
}
