package metrics

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"
)

const rawGaugeSearchTmpl string = `
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  any(Attributes) AS Attributes,
  toStartOfSecond(TimeUnix) AS AggTimeUnix,
  sum(Value) AS SumValue,
  count(Value) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  min(Value) AS MinValue,
  max(Value) AS MaxValue,
  arraySort(
    (x) -> (x.1),
    groupArray(
      (TimeUnix,Value)
    )
  )[-1].2 AS LastValue,
  groupArray(
	(TimeUnix, arrayMap((x) -> UUIDNumToString(x), Exemplars.TraceId))
  ) AS AggExemplarsTraceIdTuples
FROM %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
  AND AggTimeUnix >= %s(?)
  AND AggTimeUnix <= %s(?)
  AND %s
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
`

const aggGaugeSearchTmpl string = `
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  maxMapMerge(Attributes) AS Attributes,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  minMerge(MinValueState) AS MinValue,
  maxMerge(MaxValueState) AS MaxValue,
  anyLastMerge(LastValueState) AS LastValue,
  AggTimeUnix
FROM %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
  AND AggTimeUnix >= %s(?)
  AND AggTimeUnix <= %s(?)
  AND %s
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
`

type gaugeHandler struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

type GaugeMetricsResponse struct {
	StartTimestamp int64       `json:"start_ts"`
	EndTimestamp   int64       `json:"end_ts"`
	Results        []QueryData `json:"results"`
}

type GaugeRow struct {
	MetricName        string                `ch:"MetricName"`
	MetricDescription string                `ch:"MetricDescription"`
	MetricUnit        string                `ch:"MetricUnit"`
	Fingerprint       string                `ch:"Fingerprint"`
	Attributes        map[string]string     `ch:"FilteredAttributes"`
	Values            []float64             `ch:"Values"`
	LastValues        []float64             `ch:"LastValues"`
	Times             []time.Time           `ch:"Times"`
	TraceIds          []AggExemplarTraceIds `ch:"TraceIds"`
}

var _ metricHandler = (*gaugeHandler)(nil)

func (g *gaugeHandler) setup(db clickhouse.Conn, logger *zap.Logger) error {
	g.db = db
	g.logger = logger
	return nil
}

func (g *gaugeHandler) getSearchMetadata(
	ctx context.Context,
	projectID string,
	query *Query,
) (interface{}, error) {
	return getCounterSearchMetadata(ctx, g.logger, g.db, projectID, query)
}

//nolint:funlen,cyclop
func (g *gaugeHandler) buildQuery(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (*clickhouse.QueryBuilder, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	period := endTimestamp.Sub(startTimestamp)
	aggregateBucket, timeFunc := getAggregateBucket(period)
	tableName, err := getAggTableNameForMetricType(mtype, aggregateBucket)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	builder := clickhouse.NewQueryBuilder()
	//
	// we add all passed group-by attributes to the builder upfront to
	// be able to use them as named parameters within the constructed
	// query downstream
	//
	if len(query.GroupBy.Attributes) > 0 {
		builder.WithParams(aggregationAttributesToParams(query.GroupBy.Attributes))
	}

	var useFilteredFingerprints string
	if len(query.Filters) > 0 {
		builder, err = buildFilteredMetricsQuery(
			ctx,
			projectID,
			startTimestamp,
			endTimestamp,
			query,
			builder,
		)
		if err != nil {
			return nil, fmt.Errorf("building filters for query: %w", err)
		}
		useFilteredFingerprints = matchedFingerprintsQueryQualifier
	} else {
		useFilteredFingerprints = noFiltersQueryQualifier // default state for when query contains no filters
	}

	var searchExemplars bool
	var filterQueryStr string
	if aggregateBucket == Raw {
		//
		// right now, we want to add support for querying exemplars within metrics
		// ingested when there's no aggregation yet, i.e. aggregateBucket is raw.
		//
		searchExemplars = true
		filterQueryStr = fmt.Sprintf(
			rawGaugeSearchTmpl,
			constants.MetricsDatabaseName,
			tableName,
			timeFuncStartSecond, timeFuncStartSecond,
			useFilteredFingerprints,
		)
	} else {
		filterQueryStr = fmt.Sprintf(
			aggGaugeSearchTmpl,
			constants.MetricsDatabaseName,
			tableName,
			timeFunc, timeFunc,
			useFilteredFingerprints,
		)
	}

	// ascertain grouping type
	groupingType := query.GetGroupingType()
	defaultColumnsToSelect := `
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  groupArray(AggTimeUnix) AS Times,
  groupArray(AggValue) AS Values,
`
	if searchExemplars {
		defaultColumnsToSelect += "  arraySort((x)->(x.1), groupArray(ExemplarsTraceIdTuples)).2 AS TraceIds,\n"
	}

	switch groupingType {
	case GroupingOverAttributes, GroupingOverMetricName:
		defaultColumnsToSelect += "  mapFilter((k,v)->(k IN ?), any(AggAttributes)) AS FilteredAttributes"
		builder.Build(
			defaultColumnsToSelect,
			clickhouse.Array[string](query.GroupBy.Attributes),
		)
	case GroupingOverNone:
		defaultColumnsToSelect += "  any(AggAttributes) AS FilteredAttributes"
		builder.Build(defaultColumnsToSelect)
	}
	builder.Build("FROM")
	builder.Build("(")
	//
	// aggregation subquery
	//
	{
		aggregationSubQuery := `
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  any(Attributes) AS AggAttributes,
  AggTimeUnix,
`
		if searchExemplars {
			aggregationSubQuery += "  groupArray(AggExemplarsTraceIdTuples) AS ExemplarsTraceIdTuples,\n"
		}

		switch groupingType {
		case GroupingOverAttributes, GroupingOverMetricName:
			//
			// add corresponding aggregation when one is requested
			//
			aggCol, err := getAggregationColumn(pmetric.MetricTypeGauge, query.GroupBy.Function)
			if err != nil {
				return nil, fmt.Errorf("getting aggregation column: %w", err)
			}
			aggregationSubQuery += fmt.Sprintf("  %s AS AggValue", aggCol)
		case GroupingOverNone:
			//
			// when no aggregation is requested, we group by all fingerprints
			// which is why we need to select `Fingerprint` in the subquery
			//
			aggregationSubQuery += "  Fingerprint,\n"
			//
			// we use "avg" as the default aggregation unless specified otherwise
			//
			aggregationSubQuery += "  sum(SumValue)/sum(CountValue) AS AggValue"
		}
		builder.Build(aggregationSubQuery)
		builder.Build("FROM")
		builder.Build("(")
		//
		// filtering subquery
		//
		{
			builder.Build(
				filterQueryStr,
				projectID,
				query.TargetMetric,
				startTimestamp,
				endTimestamp,
			)
		}
		builder.Build(")")

		groupAttrs := []string{"MetricName"} // default groupby attribute
		switch groupingType {
		case GroupingOverAttributes:
			for idx := range query.GroupBy.Attributes {
				groupAttrs = append(groupAttrs, fmt.Sprintf("Attributes[{attr_%d:String}]", idx))
			}
		case GroupingOverNone:
			groupAttrs = append(groupAttrs, "Fingerprint")
		}
		groupAttrs = append(groupAttrs, "AggTimeUnix")
		groupStr := strings.Join(groupAttrs, ", ")
		builder.Build(fmt.Sprintf("\nGROUP BY %s", groupStr))
		builder.Build(fmt.Sprintf("\nORDER BY %s\n", groupStr))
	}
	builder.Build(")")

	groupAttrs := []string{"MetricName"} // default groupby attribute
	switch groupingType {
	case GroupingOverAttributes:
		for idx := range query.GroupBy.Attributes {
			groupAttrs = append(groupAttrs, fmt.Sprintf("AggAttributes[{attr_%d:String}]", idx))
		}
	case GroupingOverNone:
		groupAttrs = append(groupAttrs, "Fingerprint")
	}
	groupStr := strings.Join(groupAttrs, ", ")
	builder.Build(fmt.Sprintf("\nGROUP BY %s", groupStr))
	builder.Build(fmt.Sprintf("\nORDER BY %s", groupStr))
	return builder, nil
}

func (g *gaugeHandler) executeQuery(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (interface{}, error) {
	if err := validateMetricQuery(projectID, startTimestamp, endTimestamp, query); err != nil {
		return nil, fmt.Errorf("validating query: %w", err)
	}

	qb, err := g.buildQuery(ctx, projectID, startTimestamp, endTimestamp, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	sql := qb.SQL()
	g.logger.Debug(
		"executing query",
		zap.String("query string", sql),
		zap.Any("query args", qb.Params()),
	)

	var rows []GaugeRow
	if err := g.db.Select(
		qb.Context(ctx),
		&rows,
		sql,
	); err != nil {
		return nil, fmt.Errorf("executing metrics query: %w", err)
	}

	m := GaugeMetricsResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]QueryData, 0),
	}
	for _, r := range rows {
		qd := QueryData{
			MetricName:        r.MetricName,
			MetricDescription: r.MetricDescription,
			MetricUnit:        r.MetricUnit,
			MetricType:        query.TargetType,
			Attributes:        r.Attributes,
			Values:            make([]QueryPoint, 0),
		}
		for idx := 0; idx < len(r.Times); idx++ {
			exemplars := make([]string, 0)
			if r.TraceIds != nil && len(r.TraceIds[idx]) > 0 {
				// grouped during time aggregation
				for _, t := range r.TraceIds[idx] {
					// grouped during function aggregation
					for _, a := range t {
						// grouped as stored in CH
						exemplars = append(exemplars, a...)
					}
				}
			}
			qd.Values = append(qd.Values, QueryPoint{
				Time:             strconv.FormatInt(r.Times[idx].UnixNano(), 10),
				Value:            formatMetricValue(r.Values[idx]),
				ExemplarTraceIds: exemplars,
			})
		}
		m.Results = append(m.Results, qd)
	}
	return m, nil
}
