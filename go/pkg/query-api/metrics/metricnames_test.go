package metrics

import (
	"github.com/ClickHouse/clickhouse-go/v2"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Context("metrics", func() {
	DescribeTable(
		"building metric name source queries",
		func(filters *MetricNameFilters, expectedQuery string, args clickhouse.Parameters) {
			m := metricTable{
				tableName: "metrics_table",
				typeName:  "metrics_type",
			}
			q, err := buildQuery(m, RandomProjectID, filters)
			Expect(err).ToNot(HaveOccurred())

			err = testutils.CompareSQLStrings(q.SQL(), expectedQuery)
			Expect(err).ToNot(HaveOccurred())
			Expect(q.StringifyParams()).To(Equal(args))
		},
		Entry(
			"list all metrics",
			nil,
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
			},
		),
		Entry(
			"list metrics with starts_with filter",
			&MetricNameFilters{StartsWith: "foo"},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} AND MetricName LIKE {p1:String} GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "foo%",
			},
		),
		Entry(
			"list metrics with limit filter",
			&MetricNameFilters{Limit: 10},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName LIMIT {p1:Int64}`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "10",
			},
		),
		Entry(
			"list metrics with multiple filters",
			&MetricNameFilters{StartsWith: "foo", Limit: 10},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} AND MetricName LIKE {p1:String} GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName LIMIT {p2:Int64}`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "foo%",
				"p2": "10",
			},
		),
		Entry(
			"list metrics with multiple filters including a search term with multiple search strings",
			&MetricNameFilters{Search: []string{"foo", "bar"}, Limit: 10},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} AND ( MetricName LIKE {p1:String} OR MetricName LIKE {p2:String} ) GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName LIMIT {p3:Int64}`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "%foo%",
				"p2": "%bar%",
				"p3": "10",
			},
		),
		Entry(
			"list metrics with multiple filters including a search term",
			&MetricNameFilters{Search: []string{"foo"}, Limit: 10},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} AND ( MetricName LIKE {p1:String} ) GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName LIMIT {p2:Int64}`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "%foo%",
				"p2": "10",
			},
		),
		Entry(
			"list metrics with multiple filters including an empty search term",
			&MetricNameFilters{Search: []string{}, Limit: 10},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName LIMIT {p1:Int64}`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "10",
			},
		),
		Entry(
			"list metrics with multiple filters including attributes",
			&MetricNameFilters{
				StartsWith: "foo",
				Attributes: []string{"attr-1", "attr-2"},
				Limit:      10,
			},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} AND MetricName LIKE {p1:String} GROUP BY ProjectId, MetricName HAVING hasAll(Attributes, {p2:Array(String)}) ORDER BY ProjectId, MetricName LIMIT {p3:Int64}`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "foo%",
				"p2": "['attr-1','attr-2']",
				"p3": "10",
			},
		),
		Entry(
			"list metrics with filters including exact name search",
			&MetricNameFilters{
				ExactMatch: []string{"foo", "bar"},
			},
			`
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_table_metadata
WHERE ProjectId = {p0:String} AND ( MetricName = {p1:String} OR MetricName = {p2:String} ) GROUP BY ProjectId, MetricName ORDER BY ProjectId, MetricName`,
			clickhouse.Parameters{
				"p0": RandomProjectID,
				"p1": "foo",
				"p2": "bar",
			},
		),
	)
})
