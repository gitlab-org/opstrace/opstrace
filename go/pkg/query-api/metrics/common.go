package metrics

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

const debugTests bool = false

type AggregationInterval string

const (
	AggregationInterval1m = "1m" // minutely
	AggregationInterval1h = "1h" // hourly
	AggregationInterval1d = "1d" // daily
)

var SupportedTimeAggregations = []string{
	AggregationInterval1m,
	AggregationInterval1h,
	AggregationInterval1d,
}

type AggregateFunction string

const (
	AggregateFunctionUnknown AggregateFunction = "unknown"
	AggregateFunctionLast    AggregateFunction = "last"
	AggregateFunctionSum     AggregateFunction = "sum"
	AggregateFunctionAvg     AggregateFunction = "avg"
	AggregateFunctionMin     AggregateFunction = "min"
	AggregateFunctionMax     AggregateFunction = "max"
	AggregateFunctionCount   AggregateFunction = "count"
	AggregateFunctionP50     AggregateFunction = "p50"
	AggregateFunctionP75     AggregateFunction = "p75"
	AggregateFunctionP90     AggregateFunction = "p90"
	AggregateFunctionP95     AggregateFunction = "p95"
	AggregateFunctionP99     AggregateFunction = "p99"
)

func GetAggregationFunctionFromString(fn string) AggregateFunction {
	fn = strings.ToLower(fn) // make case-insensitive
	switch fn {
	case "last":
		return AggregateFunctionLast
	case "sum":
		return AggregateFunctionSum
	case "avg":
		return AggregateFunctionAvg
	case "min":
		return AggregateFunctionMin
	case "max":
		return AggregateFunctionMax
	case "count":
		return AggregateFunctionCount
	case "p50":
		return AggregateFunctionP50
	case "p75":
		return AggregateFunctionP75
	case "p90":
		return AggregateFunctionP90
	case "p95":
		return AggregateFunctionP95
	case "p99":
		return AggregateFunctionP99
	default:
		return AggregateFunctionUnknown
	}
}

const (
	GroupByAttributesAll    string = "*"
	GroupByAttributesBucket string = "bucket"
)

var (
	errInvalidQueryTime      = errors.New("invalid query time")
	errUnknownMetricType     = errors.New("unknown metric type")
	errUnsupportedMetricType = errors.New("unsupported metric type")
	errUnknownMetricName     = errors.New("unknown metric name")
)

// OTEL metrics
type metricTable struct {
	tableName string
	typeName  string
}

var registeredOTELMetricTables = []metricTable{
	{
		tableName: constants.MetricsGaugeTableName,
		typeName:  pmetric.MetricTypeGauge.String(),
	},
	{
		tableName: constants.MetricsSumTableName,
		typeName:  pmetric.MetricTypeSum.String(),
	},
	{
		tableName: constants.MetricsHistogramTableName,
		typeName:  pmetric.MetricTypeHistogram.String(),
	},
	{
		tableName: constants.MetricsExponentialHistogramTableName,
		typeName:  pmetric.MetricTypeExponentialHistogram.String(),
	},
}

var RawMetricTables = map[pmetric.MetricType]string{
	pmetric.MetricTypeGauge:                constants.MetricsGaugeTableName,
	pmetric.MetricTypeSum:                  constants.MetricsSumTableName,
	pmetric.MetricTypeHistogram:            constants.MetricsHistogramTableName,
	pmetric.MetricTypeExponentialHistogram: constants.MetricsExponentialHistogramTableName,
}

var MinutelyMetricTables = map[pmetric.MetricType]string{
	pmetric.MetricTypeGauge:     constants.MetricsGauge1mTableNameV2,
	pmetric.MetricTypeSum:       constants.MetricsSum1mTableNameV2,
	pmetric.MetricTypeHistogram: constants.MetricsHistogram1mTableName,
}

var HourlyMetricTables = map[pmetric.MetricType]string{
	pmetric.MetricTypeGauge:     constants.MetricsGauge1hTableNameV2,
	pmetric.MetricTypeSum:       constants.MetricsSum1hTableNameV2,
	pmetric.MetricTypeHistogram: constants.MetricsHistogram1hTableName,
}

var DailyMetricTables = map[pmetric.MetricType]string{
	pmetric.MetricTypeGauge:     constants.MetricsGauge1dTableNameV2,
	pmetric.MetricTypeSum:       constants.MetricsSum1dTableNameV2,
	pmetric.MetricTypeHistogram: constants.MetricsHistogram1dTableName,
}

func getMetricTypeFromString(t string) pmetric.MetricType {
	switch strings.ToLower(t) {
	case "gauge":
		return pmetric.MetricTypeGauge
	case "sum":
		return pmetric.MetricTypeSum
	case "histogram":
		return pmetric.MetricTypeHistogram
	case "exponentialhistogram":
		return pmetric.MetricTypeExponentialHistogram
	default:
		return pmetric.MetricTypeEmpty
	}
}

func getTableForMetricType(mtype pmetric.MetricType) (string, error) {
	for _, t := range registeredOTELMetricTables {
		if strings.EqualFold(t.typeName, mtype.String()) {
			return t.tableName, nil
		}
	}
	return "", errUnsupportedMetricType
}

func getMetricHandlerForMetricType(mtype pmetric.MetricType) metricHandler {
	var mHandler metricHandler
	//nolint:exhaustive
	switch mtype {
	case pmetric.MetricTypeGauge:
		mHandler = &gaugeHandler{}
	case pmetric.MetricTypeSum:
		mHandler = &sumHandler{}
	case pmetric.MetricTypeHistogram, pmetric.MetricTypeExponentialHistogram:
		mHandler = &histogramHandler{}
	}
	return mHandler
}

func validateMetricQuery(
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) error {
	if projectID == "" {
		return common.ErrUnknownProjectID
	}

	if startTimestamp.IsZero() || endTimestamp.IsZero() {
		return errInvalidQueryTime
	}

	if query.TargetMetric == "" {
		return errUnknownMetricName
	}

	if query.TargetType == "" {
		return errUnknownMetricType
	}

	var typeKnown bool
	for _, t := range registeredOTELMetricTables {
		if strings.EqualFold(t.typeName, query.TargetType) {
			typeKnown = true
			break
		}
	}
	if !typeKnown {
		return errUnsupportedMetricType
	}

	return nil
}

type AggregateBucket string

const (
	Raw                 AggregateBucket = "main"
	Secondly            AggregateBucket = "1s"
	Minutely            AggregateBucket = "1m"
	Hourly              AggregateBucket = "1h"
	Daily               AggregateBucket = "1d"
	timeFuncStartSecond string          = "toStartOfSecond"
	timeFuncStartMinute string          = "toStartOfMinute"
	timeFuncStartHour   string          = "toStartOfHour"
	timeFuncStartDay    string          = "toStartOfDay"
)

func getAggregateBucket(period time.Duration) (AggregateBucket, string) {
	switch {
	case period <= time.Minute*30:
		return Raw, ""
	case period > time.Minute*30 && period <= time.Hour:
		return Minutely, timeFuncStartMinute
	case period > time.Hour && period <= time.Hour*72:
		return Hourly, timeFuncStartHour
	default:
		return Daily, timeFuncStartDay
	}
}

func getAggTableNameForMetricType(mtype pmetric.MetricType, bucket AggregateBucket) (string, error) {
	//nolint:exhaustive
	switch bucket {
	case Raw:
		tableName, ok := RawMetricTables[mtype]
		if !ok {
			return "", errUnsupportedMetricType
		}
		return tableName, nil
	case Minutely:
		tableName, ok := MinutelyMetricTables[mtype]
		if !ok {
			return "", errUnsupportedMetricType
		}
		return tableName, nil
	case Hourly:
		tableName, ok := HourlyMetricTables[mtype]
		if !ok {
			return "", errUnsupportedMetricType
		}
		return tableName, nil
	case Daily:
		tableName, ok := DailyMetricTables[mtype]
		if !ok {
			return "", errUnsupportedMetricType
		}
		return tableName, nil
	default:
		return "", errUnsupportedMetricType
	}
}

func formatMetricValue(value float64) string {
	return fmt.Sprintf("%.6f", value)
}

// groupUniqInOrder generates a string slice with unique elements
// from the passed slice while retaining their original order
func groupUniqInOrder(elements []string) []string {
	if len(elements) == 0 {
		return []string{}
	}
	elementsSorted := make([]string, 0)
	elementsMap := make(map[string]struct{})
	for _, e := range elements {
		if _, ok := elementsMap[e]; !ok {
			elementsMap[e] = struct{}{}
			elementsSorted = append(elementsSorted, e)
		}
	}
	return elementsSorted
}
