package metrics

import (
	"context"
	"fmt"
	"sort"
	"strconv"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics/instruments"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type HistogramDistributionRowRaw struct {
	ProjectID         string      `ch:"ProjectId"`
	MetricName        string      `ch:"MetricName"`
	MetricDescription string      `ch:"MetricDescription"`
	MetricUnit        string      `ch:"MetricUnit"`
	Fingerprint       string      `ch:"Fingerprint"`
	AggTimeUnix       time.Time   `ch:"AggTimeUnix"`
	AggTemp           int32       `ch:"AggTemp"`
	ArrBounds         [][]float64 `ch:"ArrBounds"`
	ArrBoundsHash     []uint64    `ch:"ArrBoundsHash"`
	ArrBucketCounts   [][]uint64  `ch:"ArrBucketCounts"`
	TraceIds          []string    `ch:"TraceIds"`
}

const baseHistogramDistributionRawTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  toStartOfMinute(TimeUnix) AS AggTimeUnix,
  any(AggTemp) AS AggTemp,
  groupArray(Bounds) AS ArrBounds,
  groupArray(BoundsHash) AS ArrBoundsHash,
  groupArray(BucketCounts) AS ArrBucketCounts,
  flatten(groupArrayArray(TraceIds)) AS TraceIds
FROM
(
  SELECT
    ProjectId,
    MetricName,
    MetricDescription,
    MetricUnit,
    Fingerprint,
    TimeUnix,
    AggTemp,
    arrayPushBack(ExplicitBounds, +inf) AS Bounds,
    sipHash64(Bounds) AS BoundsHash,
    BucketCounts,
    arrayMap((x) -> UUIDNumToString(x), Exemplars.TraceId) AS TraceIds
  FROM
    %s.%s
  WHERE
    ProjectId = ?
    AND MetricName = ?
    AND toStartOfMinute(TimeUnix) >= toStartOfMinute(?)
    AND toStartOfMinute(TimeUnix) <= toStartOfMinute(?)
    AND %s
  ORDER BY TimeUnix
)
GROUP BY ProjectId, MetricName, Fingerprint, AggTimeUnix
ORDER BY ProjectId, MetricName, Fingerprint, AggTimeUnix
`

func (h *histogramHandler) computeDistributionDataRaw(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (*HistogramDistributionResponse, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.histogram.rawdistribution", trace.SpanKindClient)
	defer span.End()

	qb, err := h.buildQuery(ctx, projectID, startTimestamp, endTimestamp, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	sql := qb.SQL()
	h.logger.Debug(
		"executing query",
		zap.String("query string", sql),
		zap.Any("query args", qb.Params()),
	)

	var rows []HistogramDistributionRowRaw
	if err := h.db.Select(
		qb.Context(ctx),
		&rows,
		sql,
	); err != nil {
		return nil, fmt.Errorf("executing metrics query: %w", err)
	}

	if h.logger.Level() == zapcore.DebugLevel {
		for _, r := range rows {
			h.logger.Debug(
				"query complete",
				zap.String("fingerprint", r.Fingerprint),
				zap.Time("aggtime", r.AggTimeUnix),
				zap.Any("aggtemp", r.AggTemp),
				zap.Any("counts", r.ArrBucketCounts),
			)
		}
	}

	m := &HistogramDistributionResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]HistogramDistributionData, 0),
	}

	if len(rows) == 0 {
		return m, nil // no data found
	}

	latestRow := rows[len(rows)-1]
	dpCount := len(latestRow.ArrBoundsHash)
	latestBounds := latestRow.ArrBounds[dpCount-1]
	latestBoundsHash := latestRow.ArrBoundsHash[dpCount-1]
	// initialize the histogram
	hgram := instruments.NewHistogram(
		latestRow.MetricName,
		latestRow.MetricDescription,
		latestRow.MetricUnit,
		query.TargetType,
		latestRow.Fingerprint,
		latestRow.AggTemp,
		latestBoundsHash,
		latestBounds,
	)
	// process queried data
	for _, r := range rows {
		datapoints := len(r.ArrBucketCounts)
		if datapoints == 0 {
			continue // no data for this aggregated time
		}
		//
		// for each datapoint aggregated within this time-window, add it to
		// the histogram instance initialized above, accounting for the
		// aggregation temporality used
		//
		aggregator := NewAggregator(latestBounds)
		for dpIdx := 0; dpIdx < datapoints; dpIdx++ {
			bucketCount := r.ArrBucketCounts[dpIdx]
			if r.AggTemp == int32(pmetric.AggregationTemporalityDelta) {
				aggregator.AddDelta(bucketCount)
			} else {
				aggregator.AddCumulative(bucketCount)
			}
		}
		//
		// add constructed data to the histogram
		//
		for bucketIdx, count := range aggregator.GetData() {
			hgram.AddDatapoint(bucketIdx, r.AggTimeUnix.UnixNano(), float64(count), r.TraceIds)
		}
	}
	// compute distribution for the histogram built above
	d := computeHistogramDistribution(hgram)
	m.Results = append(m.Results, *d)
	return m, nil
}

func computeHistogramDistribution(hgram *instruments.Histogram) *HistogramDistributionData {
	d := HistogramDistributionData{
		MetricName:        hgram.MetricName,
		MetricDescription: hgram.MetricDescription,
		MetricUnit:        hgram.MetricUnit,
		MetricType:        hgram.MetricType,
		Fingerprint:       "",
		Data:              make([]PerBucketDistribution, 1),
	}
	hBuckets := hgram.BucketsAsString()
	hDistribution := make([][]QueryPoint, len(hBuckets))
	for bucketIdx, bucketData := range hgram.Data {
		// sort data in time
		tss := make([]int64, 0)
		for ts := range bucketData {
			tss = append(tss, ts)
		}
		sort.Slice(tss, func(i, j int) bool { return tss[i] < tss[j] })
		for _, ts := range tss {
			hDistribution[bucketIdx] = append(hDistribution[bucketIdx], QueryPoint{
				Time:             strconv.FormatInt(ts, 10),
				Value:            formatMetricValue(bucketData[ts].Value),
				ExemplarTraceIds: groupUniqInOrder(bucketData[ts].TraceIDs),
			})
		}
	}
	d.Data[0] = PerBucketDistribution{
		BucketsHash:  hgram.BucketsHash,
		Buckets:      hBuckets,
		Distribution: hDistribution,
	}
	return &d
}

type Aggregator struct {
	Data           []uint64
	Accumulator    []uint64
	DatapointCount int64
}

func NewAggregator(buckets []float64) *Aggregator {
	return &Aggregator{
		Data:           make([]uint64, len(buckets)),
		Accumulator:    make([]uint64, len(buckets)),
		DatapointCount: 0,
	}
}

func (a *Aggregator) AddDelta(bucketCount []uint64) {
	for bucketIdx, count := range bucketCount {
		a.Data[bucketIdx] += count
	}
}

func (a *Aggregator) AddCumulative(bucketCount []uint64) {
	a.DatapointCount++
	switch a.DatapointCount {
	case 1:
		// first datapoint
		for bucketIdx, count := range bucketCount {
			a.Data[bucketIdx] = count // handles when there is only one dp
			a.Accumulator[bucketIdx] = count
		}
	case 2:
		// second datapoint
		for bucketIdx, count := range bucketCount {
			delta := count - a.Accumulator[bucketIdx]
			a.Data[bucketIdx] = delta // begin accumulating
			a.Accumulator[bucketIdx] = count
		}
	default:
		// all successive datapoints
		for bucketIdx, count := range bucketCount {
			delta := count - a.Accumulator[bucketIdx]
			a.Data[bucketIdx] += delta // continue accumulating
			a.Accumulator[bucketIdx] = count
		}
	}
}

func (a *Aggregator) GetData() []uint64 {
	return a.Data
}
