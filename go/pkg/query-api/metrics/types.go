package metrics

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.uber.org/zap"
)

type QueryContext struct {
	ProjectID string `json:"project_id"`

	Queries    map[string]*Query `json:"queries,omitempty"`
	Expression string            `json:"expression,omitempty"`

	StartTimestamp time.Time `json:"start_time"`
	EndTimestamp   time.Time `json:"end_time"`
}

type Filter struct {
	Key      string
	Value    string
	Operator string
}

type GroupBy struct {
	Attributes []string          `json:"attribute"`
	Function   AggregateFunction `json:"function"`
}

type Correlate struct {
	TraceID string `json:"trace_id"`
}

type Query struct {
	TargetMetric string    `json:"mname"`
	TargetType   string    `json:"mtype"`
	TargetVisual string    `json:"mvisual"`
	Filters      []Filter  `json:"filters"`
	GroupBy      GroupBy   `json:"groupby"`
	Correlate    Correlate `json:"correlate"`
}

const (
	GroupingOverAttributes int = iota
	GroupingOverMetricName
	GroupingOverNone
)

func (q *Query) GetGroupingType() int {
	if q.GroupBy.Function != "" {
		if len(q.GroupBy.Attributes) > 0 {
			return GroupingOverAttributes
		} else {
			return GroupingOverMetricName
		}
	}
	return GroupingOverNone
}

type Querier interface {
	GetMetricNames(context.Context, string, *MetricNameFilters) (*MetricNameResponse, error)
	GetMetrics(context.Context, *QueryContext) (interface{}, error)
	GetSearchMetadata(context.Context, *QueryContext) (interface{}, error)
}

type metricHandler interface {
	setup(clickhouse.Conn, *zap.Logger) error
	buildQuery(context.Context, string, time.Time, time.Time, *Query) (*clickhouse.QueryBuilder, error)
	executeQuery(context.Context, string, time.Time, time.Time, *Query) (interface{}, error)
	getSearchMetadata(context.Context, string, *Query) (interface{}, error)
}

type MetricNameFilters struct {
	ExactMatch []string `form:"exact_match"`
	StartsWith string   `form:"starts_with"`
	Search     []string `form:"search"`
	Limit      int      `form:"limit" binding:"numeric"`
	Attributes []string `form:"attributes"`
	TraceID    string   `form:"trace_id"`
}

type MetricNameResponse struct {
	Metrics    []MetricName `json:"metrics"`
	Attributes []string     `json:"all_available_attributes"`
}

type MetricName struct {
	Name             string   `json:"name"`
	Description      string   `json:"description"`
	Attributes       []string `json:"attributes"`
	Type             string   `json:"type"`
	LastIngestedAt   int64    `json:"last_ingested_at"`
	TraceIDOccurence int64    `json:"timestamp_of_datapoint_with_traceId,omitempty"`
}

type ByMetricName []MetricName

func (s ByMetricName) Len() int           { return len(s) }
func (s ByMetricName) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s ByMetricName) Less(i, j int) bool { return s[i].Name < s[j].Name }

type MetricNameGetFn func(context.Context) ([]MetricName, error)
type MetricAttributesGetFn func(context.Context) (map[string]interface{}, error)
type MetricCorrelationMetadataGetFn func(context.Context) (map[string]time.Time, error)

type MetricsResponse struct {
	StartTimestamp int64 `json:"start_ts"`
	EndTimestamp   int64 `json:"end_ts"`

	Results []QueryData `json:"results"`
}

type QueryPoint struct {
	Time             string
	Value            string
	ExemplarTraceIds []string
}

func (qp *QueryPoint) MarshalJSON() ([]byte, error) {
	out, err := json.Marshal([]any{
		qp.Time,
		qp.Value,
		qp.ExemplarTraceIds,
	})
	if err != nil {
		return nil, fmt.Errorf("marshaling QueryPoint for response: %w", err)
	}
	return out, nil
}

func (qp *QueryPoint) UnmarshalJSON(data []byte) error {
	if data == nil {
		return nil
	}
	var t []any
	if err := json.Unmarshal(data, &t); err != nil {
		return fmt.Errorf("unmarshaling data for QueryPoint: %w", err)
	}
	if len(t) != 3 {
		return fmt.Errorf("bad data")
	}
	if v, ok := t[0].(string); ok {
		qp.Time = v
	}
	if v, ok := t[1].(string); ok {
		qp.Value = v
	}
	if v, ok := t[2].([]string); ok {
		qp.ExemplarTraceIds = v
	}
	return nil
}
