package metrics

import (
	"context"
	"fmt"
	"sort"
	"strconv"
	"time"

	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics/instruments"
)

const targetVisualHeatmap = "heatmap"

type histogramHandler struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

var _ metricHandler = (*histogramHandler)(nil)

func (h *histogramHandler) setup(db clickhouse.Conn, logger *zap.Logger) error {
	h.db = db
	h.logger = logger
	return nil
}

const baseHistogramSearchTmpl string = `
SELECT
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  any(Attributes) AS Attributes,
  groupArray(Avg) AS Values,
  groupArray(TimeUnix) AS Times
FROM
(
  SELECT
    MetricName,
    MetricDescription,
    MetricUnit,
    Fingerprint,
    Attributes,
    (Sum/Count) AS Avg,
    TimeUnix
  FROM %s.%s
  WHERE
    ProjectId = ?
    AND MetricName = ?
    AND TimeUnix >= ?
    AND TimeUnix < ?
    AND %s
  ORDER BY MetricName, Fingerprint, TimeUnix
)
GROUP BY Fingerprint
`

const baseHistogramDistributionTmpl string = `
SELECT
  ProjectId,
  MetricName,
  MetricDescription,
  MetricUnit,
  Fingerprint,
  StartTimeUnix,
  AggTimeUnix,
  AggTemp,
  Min,
  Max,
  Count,
  Sum,
  Bounds,
  BoundsHash,
  SamplesCount,
  CountsMax[-1].2 - CountsMin[1].2 AS Delta,
  CountsSummed AS Cumulative
FROM
(
  SELECT
    ProjectId,
    MetricName,
    any(MetricDescription) AS MetricDescription,
    any(MetricUnit) AS MetricUnit,
    Fingerprint,
    StartTimeUnix,
    AggTimeUnix,
    any(AggTemp) AS AggTemp,
    minMerge(MinState) AS Min,
    maxMerge(MaxState) AS Max,
    sumMerge(CountState) AS Count,
    sumMerge(SumState) AS Sum,
    any(Bounds) AS Bounds,
    sipHash64(Bounds) AS BoundsHash,
    sumMerge(SamplesCountState) AS SamplesCount,
    arraySort((x) -> (x.1), groupArrayMerge(BucketCountsMinState)) AS CountsMin,
    arraySort((x) -> (x.1), groupArrayMerge(BucketCountsMaxState)) AS CountsMax,
    sumForEachMerge(BucketCountsSummedState) AS CountsSummed
  FROM
    %s.%s
  WHERE
    ProjectId = ?
    AND MetricName = ?
    AND AggTimeUnix >= %s(?)
    AND AggTimeUnix <= %s(?)
    AND %s
  GROUP BY ProjectId, MetricName, Fingerprint, StartTimeUnix, AggTimeUnix
  ORDER BY ProjectId, MetricName, Fingerprint, StartTimeUnix, AggTimeUnix
)
ORDER BY ProjectId, MetricName, Fingerprint, StartTimeUnix, AggTimeUnix
`

func (h *histogramHandler) buildQuery(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (*clickhouse.QueryBuilder, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	tableName, err := getTableForMetricType(mtype)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	period := endTimestamp.Sub(startTimestamp)
	aggregateBucket, timeFunc := getAggregateBucket(period)
	aggregatedTableName, err := getAggTableNameForMetricType(mtype, aggregateBucket)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	//
	// When computing distribution data for histograms or exponential histograms over
	// aggregated data, we always want to filter down to a single timeseries, i.e. a
	// single fingerprint.
	// For this, we add a noop filter to ensure the rest of our filtering machinery
	// works as intended without having to add any exceptions within that logic,
	// then handle the histogram-type(s) as needed.
	//
	if mtype == pmetric.MetricTypeHistogram &&
		query.TargetVisual == targetVisualHeatmap &&
		aggregateBucket != Raw {
		query.Filters = append(query.Filters, Filter{Operator: string(FilterOpNoOp)})
	}

	var useFilteredFingerprints string
	builder := clickhouse.NewQueryBuilder()
	if len(query.Filters) > 0 {
		builder, err = buildFilteredMetricsQuery(
			ctx,
			projectID,
			startTimestamp,
			endTimestamp,
			query,
			builder,
		)
		if err != nil {
			return nil, fmt.Errorf("building filters for query: %w", err)
		}
		useFilteredFingerprints = matchedFingerprintsQueryQualifier
	} else {
		useFilteredFingerprints = noFiltersQueryQualifier // default state for when query contains no filters
	}

	var queryStr string
	if mtype == pmetric.MetricTypeHistogram && query.TargetVisual == targetVisualHeatmap {
		if aggregateBucket == Raw {
			queryStr = fmt.Sprintf(
				baseHistogramDistributionRawTmpl,
				constants.MetricsDatabaseName,
				tableName,
				useFilteredFingerprints,
			)
		} else {
			queryStr = fmt.Sprintf(
				baseHistogramDistributionTmpl,
				constants.MetricsDatabaseName,
				aggregatedTableName,
				timeFunc, timeFunc,
				useFilteredFingerprints,
			)
		}
	} else {
		queryStr = fmt.Sprintf(
			baseHistogramSearchTmpl,
			constants.MetricsDatabaseName,
			tableName,
			useFilteredFingerprints,
		)
	}

	builder.Build(
		queryStr,
		projectID,
		query.TargetMetric,
		startTimestamp,
		endTimestamp,
	)
	return builder, nil
}

type PerBucketDistribution struct {
	BucketsHash  uint64         `json:"bucketsHash"`
	Buckets      []string       `json:"buckets"`
	Distribution [][]QueryPoint `json:"distribution"`
}

type HistogramDistributionResponse struct {
	StartTimestamp int64                       `json:"start_ts"`
	EndTimestamp   int64                       `json:"end_ts"`
	Results        []HistogramDistributionData `json:"results"`
}

type HistogramDistributionData struct {
	MetricName        string                  `json:"name"`
	MetricDescription string                  `json:"description"`
	MetricUnit        string                  `json:"unit"`
	MetricType        string                  `json:"type"`
	Fingerprint       string                  `json:"fingerprint"`
	Data              []PerBucketDistribution `json:"data"`
}

type HistogramDistributionRow struct {
	ProjectID         string    `ch:"ProjectId"`
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	MetricUnit        string    `ch:"MetricUnit"`
	Fingerprint       string    `ch:"Fingerprint"`
	StartTimeUnix     time.Time `ch:"StartTimeUnix"`
	AggTimeUnix       time.Time `ch:"AggTimeUnix"`
	AggTemp           int32     `ch:"AggTemp"`
	Min               float64   `ch:"Min"`
	Max               float64   `ch:"Max"`
	Count             uint64    `ch:"Count"`
	Sum               float64   `ch:"Sum"`
	Bounds            []float64 `ch:"Bounds"`
	BoundsHash        uint64    `ch:"BoundsHash"`
	SamplesCount      uint64    `ch:"SamplesCount"`
	Delta             []uint64  `ch:"Delta"`
	Cumulative        []uint64  `ch:"Cumulative"`
}

type HistogramMetricRow struct {
	MetricName        string            `ch:"MetricName"`
	MetricDescription string            `ch:"MetricDescription"`
	MetricUnit        string            `ch:"MetricUnit"`
	Fingerprint       string            `ch:"Fingerprint"`
	Attributes        map[string]string `ch:"Attributes"`
	Values            []float64         `ch:"Values"`
	Times             []time.Time       `ch:"Times"`
}

func (h *histogramHandler) executeQuery(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (interface{}, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	period := endTimestamp.Sub(startTimestamp)
	aggregateBucket, _ := getAggregateBucket(period)

	if mtype == pmetric.MetricTypeHistogram && query.TargetVisual == targetVisualHeatmap {
		if aggregateBucket == Raw {
			return h.computeDistributionDataRaw(
				ctx,
				projectID,
				startTimestamp,
				endTimestamp,
				query,
			)
		}
		return h.computeDistributionData(
			ctx,
			projectID,
			startTimestamp,
			endTimestamp,
			query,
		)
	}

	return h.computeAverageData(
		ctx,
		projectID,
		startTimestamp,
		endTimestamp,
		query,
	)
}

func (h *histogramHandler) computeAverageData(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (*CounterMetricsResponse, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.CHQuery.average", trace.SpanKindClient)
	defer span.End()

	qb, err := h.buildQuery(ctx, projectID, startTimestamp, endTimestamp, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	sql := qb.SQL()
	h.logger.Debug(
		"executing query",
		zap.String("query string", sql),
		zap.Any("query args", qb.Params()),
	)

	var rows []HistogramMetricRow
	if err := h.db.Select(
		qb.Context(ctx),
		&rows,
		sql,
	); err != nil {
		return nil, fmt.Errorf("executing metrics query: %w", err)
	}

	m := &CounterMetricsResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]QueryData, 0),
	}
	for _, r := range rows {
		qd := QueryData{
			MetricName:        r.MetricName,
			MetricDescription: r.MetricDescription,
			MetricUnit:        r.MetricUnit,
			MetricType:        query.TargetType,
			Attributes:        r.Attributes,
			Values:            make([]QueryPoint, 0),
		}
		for idx := 0; idx < len(r.Times); idx++ {
			qd.Values = append(qd.Values, QueryPoint{
				Time:  strconv.FormatInt(r.Times[idx].UnixNano(), 10),
				Value: formatMetricValue(r.Values[idx]),
			})
		}
		m.Results = append(m.Results, qd)
	}
	return m, nil
}

//nolint:funlen
func (h *histogramHandler) computeDistributionData(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
) (*HistogramDistributionResponse, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.CHQuery.distribution", trace.SpanKindClient)
	defer span.End()

	// constructing distribution data
	qb, err := h.buildQuery(ctx, projectID, startTimestamp, endTimestamp, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	sql := qb.SQL()
	h.logger.Debug(
		"executing query",
		zap.String("query string", sql),
		zap.Any("query args", qb.Params()),
	)

	var rows []HistogramDistributionRow
	if err := h.db.Select(
		qb.Context(ctx),
		&rows,
		sql,
	); err != nil {
		return nil, fmt.Errorf("executing metrics query: %w", err)
	}

	m := &HistogramDistributionResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]HistogramDistributionData, 0),
	}

	if len(rows) == 0 {
		return m, nil // no data found
	}

	histograms := make(map[string]*instruments.Histogram)
	for _, r := range rows {
		if _, ok := histograms[r.Fingerprint]; !ok {
			h := instruments.NewHistogram(
				r.MetricName,
				r.MetricDescription,
				r.MetricUnit,
				query.TargetType,
				r.Fingerprint,
				r.AggTemp,
				r.BoundsHash,
				r.Bounds,
			)
			histograms[r.Fingerprint] = h
		}
		// account for aggregation temporality
		var data []uint64
		if r.AggTemp == int32(pmetric.AggregationTemporalityDelta) {
			data = r.Cumulative
		} else {
			// when recording samples with cumulative aggregation temporality, if
			// we only aggregate a single sample within the aggregation window, it'd
			// be okay to use the cumulative value instead, since the delta value
			// would essentially be all zeroes.
			if r.SamplesCount == 1 {
				data = r.Cumulative
			} else {
				data = r.Delta
			}
		}
		// add queried datapoints to the corresponding histogram
		h := histograms[r.Fingerprint]
		for bucketIdx, count := range data {
			h.AddDatapoint(bucketIdx, r.AggTimeUnix.UnixNano(), float64(count), nil)
		}
	}
	// add all histograms to results
	for f, h := range histograms {
		d := HistogramDistributionData{
			MetricName:        h.MetricName,
			MetricDescription: h.MetricDescription,
			MetricUnit:        h.MetricUnit,
			MetricType:        h.MetricType,
			Fingerprint:       f,
			Data:              make([]PerBucketDistribution, 1),
		}
		hBuckets := h.BucketsAsString()
		hDistribution := make([][]QueryPoint, len(hBuckets))
		for bucketIdx, bucketData := range h.Data {
			// sort data in time
			tss := make([]int64, 0)
			for ts := range bucketData {
				tss = append(tss, ts)
			}
			sort.Slice(tss, func(i, j int) bool { return tss[i] < tss[j] })
			for _, ts := range tss {
				hDistribution[bucketIdx] = append(hDistribution[bucketIdx], QueryPoint{
					Time:             strconv.FormatInt(ts, 10),
					Value:            formatMetricValue(bucketData[ts].Value),
					ExemplarTraceIds: groupUniqInOrder(bucketData[ts].TraceIDs),
				})
			}
		}
		d.Data[0] = PerBucketDistribution{
			BucketsHash:  h.BucketsHash,
			Buckets:      hBuckets,
			Distribution: hDistribution,
		}
		m.Results = append(m.Results, d)
	}
	return m, nil
}

func (h *histogramHandler) getSearchMetadata(ctx context.Context, projectID string, query *Query) (interface{}, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.SearchMetadataQuery", trace.SpanKindClient)
	defer span.End()

	qb, err := buildSearchMetadataQuery(projectID, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	sql := qb.SQL()
	h.logger.Debug(
		"executing search metadata query",
		zap.String("query string", sql),
		zap.Any("query args", qb.Params()),
	)

	var rows []MetricSearchMetadataRow
	if err := h.db.Select(
		qb.Context(ctx),
		&rows,
		sql,
	); err != nil {
		return nil, fmt.Errorf("executing metrics search metadata query: %w", err)
	}

	response := MetricSearchMetadataResponse{}
	for _, r := range rows {
		response.MetricName = r.MetricName
		response.MetricType = query.TargetType
		response.MetricDescription = r.MetricDescription
		response.AttributeKeys = r.AttributeKeys
		response.LastIngestedAt = r.LastIngestedAt.UnixNano()
	}
	response.SupportedAggregations = SupportedTimeAggregations
	response.SupportedFunctions = []string{
		string(AggregateFunctionP50),
		string(AggregateFunctionP75),
		string(AggregateFunctionP90),
		string(AggregateFunctionP95),
		string(AggregateFunctionP99),
		string(AggregateFunctionMin),
		string(AggregateFunctionMax),
	}
	response.DefaultGroupByAttributes = []string{GroupByAttributesBucket}
	response.DefaultGroupByFunction = string(AggregateFunctionCount)
	return response, nil
}
