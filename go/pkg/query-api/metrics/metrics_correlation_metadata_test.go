package metrics

import (
	"context"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type correlationMetadataRow struct {
	ProjectID               string    `ch:"ProjectId"`
	MetricName              string    `ch:"MetricName"`
	ExemplarTraceID         string    `ch:"ExemplarTraceId"`
	LatestDatapointTimeUnix time.Time `ch:"LatestDatapointTimeUnix"`
}

func testQueryingCorrelationMetadata(
	ctx context.Context,
	conn driver.Conn,
	mtype pmetric.MetricType,
	traceID string,
	refTime time.Time,
) {
	tableName, err := getTableForMetricType(mtype)
	Expect(err).ToNot(HaveOccurred())

	qb := buildMetricsCorrelationMetadataQuery(
		metricTable{
			tableName: tableName,
			typeName:  mtype.String(),
		},
		common.RandomProjectID,
		&MetricNameFilters{
			TraceID: traceID,
		},
	)

	var rows []correlationMetadataRow
	err = conn.Select(qb.Context(ctx), &rows, qb.SQL())
	Expect(err).ToNot(HaveOccurred())
	Expect(rows).To(HaveLen(1))
	for _, r := range rows {
		GinkgoWriter.Println("row: ", r)
	}
	Expect(rows[0].LatestDatapointTimeUnix.UnixNano()).To(Equal(refTime.UnixNano()))
}

var _ = Describe("metrics correlation metadata", Ordered, Serial, func() {
	var (
		ctx     context.Context
		cancel  context.CancelFunc
		testEnv *testutils.ClickHouseServer
		conn    driver.Conn
		err     error
	)

	BeforeAll(func() {
		// ensure the tests run within the stipulated timeout
		ctx, cancel = context.WithTimeout(context.Background(), 120*time.Second)
		// create test CH server
		testEnv, conn, err = testutils.NewClickHouseServerAndConnection(ctx)
		Expect(err).ToNot(HaveOccurred())
		DeferCleanup(func(ctx SpecContext) {
			testEnv.Terminate(ctx)
		})

		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.MetricsDatabaseName)).To(Succeed())
		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.AnalyticsDatabaseName)).To(Succeed())
	})

	AfterAll(func() {
		cancel()
	})

	DescribeTable(
		"querying correlation metadata via materialized view",
		func(mtype pmetric.MetricType) {
			traceID, err := GenerateRandomTraceID()
			Expect(err).ToNot(HaveOccurred())
			traceIDUUID, err := uuid.FromBytes(traceID)
			Expect(err).ToNot(HaveOccurred())

			// insert datapoint, then query correlation data back via the MV
			refTime := time.Now().UTC()
			m := common.GenerateOTELMetrics(mtype, refTime, refTime, map[string]string{
				"keyone": "valone",
				"keytwo": "valtwo",
			}, 10, traceID)
			err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, refTime)
			Expect(err).ToNot(HaveOccurred())
			testQueryingCorrelationMetadata(ctx, conn, mtype, traceIDUUID.String(), refTime)

			// insert another datapoint with the same trace ID, test change in
			// latest datapoint timestamp
			refTime = refTime.Add(1 * time.Minute).UTC()
			m = common.GenerateOTELMetrics(mtype, refTime, refTime, map[string]string{
				"keyone": "valone",
				"keytwo": "valtwo",
			}, 5, traceID)
			err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, refTime)
			Expect(err).ToNot(HaveOccurred())
			testQueryingCorrelationMetadata(ctx, conn, mtype, traceIDUUID.String(), refTime)
		},
		Entry("for sum metrics", pmetric.MetricTypeSum),
		Entry("for gauge metrics", pmetric.MetricTypeGauge),
		Entry("for histogram metrics", pmetric.MetricTypeHistogram),
		Entry("for exponential histogram metrics", pmetric.MetricTypeExponentialHistogram),
	)
})
