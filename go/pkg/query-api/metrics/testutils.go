package metrics

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

var sampleTimestamp = time.Unix(1701264345, 0).UTC()

var corpus = []MetricName{
	{
		Name:           "foo",
		Description:    "description for foo",
		Attributes:     []string{"foo", "bar"},
		Type:           pmetric.MetricTypeGauge.String(),
		LastIngestedAt: sampleTimestamp.UnixNano(),
	},
	{
		Name:           "bar",
		Description:    "description for bar",
		Attributes:     []string{"foo", "bar"},
		Type:           pmetric.MetricTypeGauge.String(),
		LastIngestedAt: sampleTimestamp.UnixNano(),
	},
	{
		Name:           "baz",
		Description:    "description for baz",
		Attributes:     []string{"foo", "bar"},
		Type:           pmetric.MetricTypeHistogram.String(),
		LastIngestedAt: sampleTimestamp.UnixNano(),
	},
}

var corpusAttributes = []string{"bar", "foo"}

var sampleData = CounterMetricsResponse{
	StartTimestamp: sampleTimestamp.UnixNano(),
	EndTimestamp:   sampleTimestamp.UnixNano(),

	Results: []QueryData{
		{
			MetricName:        "cpu_seconds_total",
			MetricDescription: "random description",
			MetricUnit:        "foo",
			MetricType:        pmetric.MetricTypeGauge.String(),
			Attributes: map[string]string{
				"foo": "bar",
				"baz": "blah",
			},
			Values: []QueryPoint{
				{
					Time:             strconv.FormatInt(sampleTimestamp.UnixNano(), 10),
					Value:            formatMetricValue(float64(12345)),
					ExemplarTraceIds: []string{"06e7bf985b0f1a67af1181e5361f79bf"},
				},
			},
		},
	},
}

var searchMetadataResponse = MetricSearchMetadataResponse{
	MetricName:               "cpu_seconds_total",
	MetricType:               pmetric.MetricTypeGauge.String(),
	MetricDescription:        "random description",
	AttributeKeys:            []string{"foo", "bar"},
	LastIngestedAt:           sampleTimestamp.UnixNano(),
	SupportedFunctions:       []string{"sum", "count"},
	SupportedAggregations:    []string{"1m", "1h"},
	DefaultGroupByAttributes: []string{"*"},
	DefaultGroupByFunction:   "sum",
}

type MockQuerier struct{}

var _ Querier = (*MockQuerier)(nil)

func (*MockQuerier) GetMetricNames(_ context.Context, _ string, _ *MetricNameFilters) (*MetricNameResponse, error) {
	return &MetricNameResponse{
		Metrics:    corpus,
		Attributes: corpusAttributes,
	}, nil
}

func (*MockQuerier) GetMetrics(_ context.Context, _ *QueryContext) (interface{}, error) {
	return &sampleData, nil
}

func (*MockQuerier) GetSearchMetadata(_ context.Context, _ *QueryContext) (interface{}, error) {
	return &searchMetadataResponse, nil
}

type testPeriod struct {
	startTimestamp, endTimestamp time.Time
}

func getTestIntervals() []testPeriod {
	refTime := time.Now()
	testPeriods := make([]testPeriod, 0)
	for _, interval := range []time.Duration{
		10 * time.Second, // 10 seconds
		1 * time.Hour,    // 1 hour
		2 * time.Hour,    // 2 hour
		96 * time.Hour,   // 96 hour
	} {
		testPeriods = append(testPeriods, testPeriod{
			startTimestamp: refTime.Add(-1 * interval).UTC(),
			endTimestamp:   refTime.UTC(),
		})
	}
	return testPeriods
}

func truncateOTELSumTables(ctx context.Context, testEnv *testutils.ClickHouseServer) error {
	for _, tableName := range []string{
		constants.MetricsSumTableName,
		constants.MetricsSum1mTableNameV2,
		constants.MetricsSum1hTableNameV2,
		constants.MetricsSum1dTableNameV2,
	} {
		if err := testEnv.Exec(
			ctx,
			fmt.Sprintf("TRUNCATE TABLE %s.%s", constants.MetricsDatabaseName, tableName),
		); err != nil {
			return err
		}
	}
	return nil
}

func truncateOTELGaugeTables(ctx context.Context, testEnv *testutils.ClickHouseServer) error {
	for _, tableName := range []string{
		constants.MetricsGaugeTableName,
		constants.MetricsGauge1mTableNameV2,
		constants.MetricsGauge1hTableNameV2,
		constants.MetricsGauge1dTableNameV2,
	} {
		if err := testEnv.Exec(
			ctx,
			fmt.Sprintf("TRUNCATE TABLE %s.%s", constants.MetricsDatabaseName, tableName),
		); err != nil {
			return err
		}
	}
	return nil
}

func truncateOTELHistogramTables(ctx context.Context, testEnv *testutils.ClickHouseServer) error {
	for _, tableName := range []string{
		constants.MetricsHistogramTableName,
	} {
		if err := testEnv.Exec(
			ctx,
			fmt.Sprintf("TRUNCATE TABLE %s.%s", constants.MetricsDatabaseName, tableName),
		); err != nil {
			return err
		}
	}
	return nil
}
