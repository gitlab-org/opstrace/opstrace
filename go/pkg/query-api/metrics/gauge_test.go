package metrics

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"os"
	"strconv"
	"time"

	"github.com/olekukonko/tablewriter"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

func generateRandomGaugeMetricsData(
	ctx context.Context,
	d *DataGenerator,
	startTimestamp time.Time,
	endTimestamp time.Time,
) error {
	// machine 1
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, time.Now(), MetricOptions{
		MetricName: targetMetric,
		MetricType: pmetric.MetricTypeGauge.String(),
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-001",
			"host": "ams4-host-001",
		},
	}); err != nil {
		return err
	}
	// machine 2
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, time.Now(), MetricOptions{
		MetricName: targetMetric,
		MetricType: pmetric.MetricTypeGauge.String(),
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-001",
			"host": "ams4-host-002",
		},
	}); err != nil {
		return err
	}
	// machine 3
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, time.Now(), MetricOptions{
		MetricName: targetMetric,
		MetricType: pmetric.MetricTypeGauge.String(),
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-002",
			"host": "ams4-host-003",
		},
	}); err != nil {
		return err
	}
	return nil
}

func reportGaugeMetricsResponse(computed, expected GaugeMetricsResponse) {
	computedRows := 0
	for _, ts := range computed.Results {
		computedRows += len(ts.Values)
	}
	fmt.Printf("computed rows: %d\n", computedRows)

	expectedRows := 0
	for _, ts := range expected.Results {
		expectedRows += len(ts.Values)
	}
	fmt.Printf("expected rows: %d\n", expectedRows)

	tableRows := max(computedRows, expectedRows)

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{
		"EXPECTED ATTRIBUTES", "EXPECTED TS", "EXPECTED VAL", "EXPECTED EXEMPLARS",
		"ACTUAL ATTRIBUTES", "ACTUAL TS", "ACTUAL VAL", "ACTUAL EXEMPLARS",
	})
	tableData := make([][]string, tableRows)

	tableRow := 0
	for _, ts := range computed.Results {
		attrs, _ := json.Marshal(ts.Attributes)
		for _, dp := range ts.Values {
			tableData[tableRow] = append(tableData[tableRow], string(attrs))
			tableData[tableRow] = append(tableData[tableRow], dp.Time)
			tableData[tableRow] = append(tableData[tableRow], dp.Value)
			tableData[tableRow] = append(tableData[tableRow], fmt.Sprintf("%v", dp.ExemplarTraceIds))
			tableRow++
		}
	}
	tableRow = 0
	for _, ts := range expected.Results {
		attrs, _ := json.Marshal(ts.Attributes)
		for _, dp := range ts.Values {
			tableData[tableRow] = append(tableData[tableRow], string(attrs))
			tableData[tableRow] = append(tableData[tableRow], dp.Time)
			tableData[tableRow] = append(tableData[tableRow], dp.Value)
			tableData[tableRow] = append(tableData[tableRow], fmt.Sprintf("%v", dp.ExemplarTraceIds))
			tableRow++
		}
	}
	for _, v := range tableData {
		table.Append(v)
	}
	table.Render()
}

//nolint:cyclop
func computeGaugeMetricsResponse(
	startTimestamp time.Time,
	endTimestamp time.Time,
	d *DataGenerator,
	q *Query,
) GaugeMetricsResponse {
	computed := GaugeMetricsResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]QueryData, 0),
	}
	aggregated := d.RawDataAggregated(q)
	if len(aggregated) == 0 {
		return computed // no data received
	}
	for _, ts := range aggregated {
		qd := QueryData{
			Values: make([]QueryPoint, 0),
		}
		for _, aggTime := range ts.Timestamps() {
			var (
				sumValue   float64
				maxValue   float64 = 0
				minValue   float64 = math.MaxFloat64
				countValue float64
				lastValue  float64
				exemplars  = make([]string, 0)
			)
			datapoints := ts.Datapoints[aggTime]
			for idx, m := range datapoints {
				if qd.MetricName == "" {
					qd.MetricName = m.MetricName
					qd.MetricDescription = m.MetricDescription
					qd.MetricType = pmetric.MetricTypeGauge.String()
					qd.MetricUnit = m.MetricUnit
					qd.Attributes = m.FilteredAttributes
				}
				countValue++
				sumValue += m.Value
				if m.Value > maxValue {
					maxValue = m.Value
				}
				if m.Value < minValue {
					minValue = m.Value
				}
				if idx == len(datapoints)-1 {
					lastValue = m.Value
				}
				exemplars = append(exemplars, m.ExemplarTraceIds...)
			}
			var qp QueryPoint
			//nolint:exhaustive
			switch q.GroupBy.Function {
			case AggregateFunctionSum:
				qp = QueryPoint{
					Time:             strconv.FormatInt(aggTime, 10),
					Value:            formatMetricValue(sumValue),
					ExemplarTraceIds: exemplars,
				}
			case AggregateFunctionAvg:
				qp = QueryPoint{
					Time:             strconv.FormatInt(aggTime, 10),
					Value:            formatMetricValue(sumValue / countValue),
					ExemplarTraceIds: exemplars,
				}
			case AggregateFunctionMax:
				qp = QueryPoint{
					Time:             strconv.FormatInt(aggTime, 10),
					Value:            formatMetricValue(maxValue),
					ExemplarTraceIds: exemplars,
				}
			case AggregateFunctionMin:
				qp = QueryPoint{
					Time:             strconv.FormatInt(aggTime, 10),
					Value:            formatMetricValue(minValue),
					ExemplarTraceIds: exemplars,
				}
			case AggregateFunctionCount:
				qp = QueryPoint{
					Time:             strconv.FormatInt(aggTime, 10),
					Value:            formatMetricValue(countValue),
					ExemplarTraceIds: exemplars,
				}
			case AggregateFunctionLast:
				qp = QueryPoint{
					Time:             strconv.FormatInt(aggTime, 10),
					Value:            formatMetricValue(lastValue),
					ExemplarTraceIds: exemplars,
				}
			default:
				qp = QueryPoint{
					Time:             strconv.FormatInt(aggTime, 10),
					Value:            formatMetricValue(sumValue / countValue),
					ExemplarTraceIds: exemplars,
				}
			}
			qd.Values = append(qd.Values, qp)
		}
		computed.Results = append(computed.Results, qd)
	}
	return computed
}

var _ = Context("gauge metrics", Serial, Ordered, func() {
	var (
		ctx     context.Context
		cancel  context.CancelFunc
		testEnv *testutils.ClickHouseServer
		conn    driver.Conn
		err     error
	)

	BeforeAll(func() {
		// ensure the tests run within the stipulated timeout
		ctx, cancel = context.WithTimeout(context.Background(), 120*time.Second)
		// create test CH server
		testEnv, conn, err = testutils.NewClickHouseServerAndConnection(ctx)
		Expect(err).ToNot(HaveOccurred())
		DeferCleanup(func(ctx SpecContext) {
			testEnv.Terminate(ctx)
		})

		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.MetricsDatabaseName)).To(Succeed())
		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.AnalyticsDatabaseName)).To(Succeed())
	})

	AfterAll(func() {
		// cancel context
		cancel()
	})

	DescribeTable(
		"search",
		func(q *Query, genTestData genTestDataFn, expectedErr error) {
			testPeriods := getTestIntervals()
			for _, tp := range testPeriods {
				// truncate all tables before each test
				err = truncateOTELGaugeTables(ctx, testEnv)
				Expect(err).ToNot(HaveOccurred())
				// create a data generator
				d := NewDataGenerator(conn, logger.Desugar())
				// generate test data
				err = genTestData(ctx, d, tp.startTimestamp, tp.endTimestamp)
				Expect(err).ToNot(HaveOccurred())
				// compute expected data
				computed := computeGaugeMetricsResponse(tp.startTimestamp, tp.endTimestamp, d, q)
				computedJSON, err := json.Marshal(computed)
				Expect(err).ToNot(HaveOccurred())
				handler := gaugeHandler{db: conn, logger: logger.Desugar()}
				if debugTests {
					qb, err := handler.buildQuery(
						ctx,
						targetProjectID,
						tp.startTimestamp,
						tp.endTimestamp,
						q,
					)
					Expect(err).ToNot(HaveOccurred())
					fmt.Printf("%s\n", qb.SQL())
					fmt.Printf("%+v\n", qb.Params())
				}
				expectedRaw, err := handler.executeQuery(
					ctx,
					targetProjectID,
					tp.startTimestamp,
					tp.endTimestamp,
					q,
				)
				Expect(err).ToNot(HaveOccurred())
				if debugTests {
					reportGaugeMetricsResponse(computed, expectedRaw.(GaugeMetricsResponse))
				}

				expectedJSON, err := json.Marshal(expectedRaw)
				Expect(err).ToNot(HaveOccurred())
				// compare outputs
				Expect(expectedJSON).To(Equal(computedJSON))
			}
		},
		Entry(
			"filtering by one attribute",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters: []Filter{
					{
						Key:      "dc",
						Operator: string(FilterOpEqual),
						Value:    "ams4",
					},
				},
				GroupBy: GroupBy{
					Attributes: []string{},
					Function:   "",
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"filtering by one or more attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters: []Filter{
					{
						Key:      "host",
						Operator: string(FilterOpEqual),
						Value:    "ams4-host-001",
					},
					{
						Key:      "rack",
						Operator: string(FilterOpEqual),
						Value:    "ams4-rack-001",
					},
				},
				GroupBy: GroupBy{
					Attributes: []string{},
					Function:   "",
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"grouping with last by one or more attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters:      nil,
				GroupBy: GroupBy{
					Attributes: []string{"host", "rack"},
					Function:   AggregateFunctionLast,
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"grouping with sum by one or more attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters:      nil,
				GroupBy: GroupBy{
					Attributes: []string{"host", "rack"},
					Function:   AggregateFunctionSum,
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"grouping with count by one or more attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters:      nil,
				GroupBy: GroupBy{
					Attributes: []string{"host", "rack"},
					Function:   AggregateFunctionCount,
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"grouping with avg with no attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters:      nil,
				GroupBy: GroupBy{
					Attributes: []string{},
					Function:   AggregateFunctionAvg,
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"grouping with avg by one or more attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters:      nil,
				GroupBy: GroupBy{
					Attributes: []string{"host", "rack"},
					Function:   AggregateFunctionAvg,
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"grouping with max by one or more attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters:      nil,
				GroupBy: GroupBy{
					Attributes: []string{"host", "rack"},
					Function:   AggregateFunctionMax,
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
		Entry(
			"grouping with min by one or more attributes",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters:      nil,
				GroupBy: GroupBy{
					Attributes: []string{"host", "rack"},
					Function:   AggregateFunctionMin,
				},
			},
			generateRandomGaugeMetricsData,
			nil, // expectedErr
		),
	)

	DescribeTable(
		"building queries",
		func(projectID string, q *Query, expectedQuery string, expectedErr error) {
			handler := gaugeHandler{}

			qb, err := handler.buildQuery(
				context.TODO(),
				targetProjectID,
				time.Now().Add(-1*36*time.Hour),
				time.Now(),
				q,
			)
			if expectedErr != nil {
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(expectedErr))
			} else {
				Expect(err).ToNot(HaveOccurred())
				err = testutils.CompareSQLStrings(qb.SQL(), expectedQuery)
				Expect(err).ToNot(HaveOccurred())
			}
		},
		Entry(
			"querying data for a random gauge metric",
			targetProjectID,
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
				Filters: []Filter{
					{Key: "cpu", Operator: string(FilterOpEqual), Value: "1"},
					{Key: "state", Operator: string(FilterOpNotEqual), Value: "idle"},
					{Key: "", Operator: string(FilterOpHasAttribute), Value: "cpu"},
				},
				GroupBy: GroupBy{
					Function:   AggregateFunctionAvg,
					Attributes: []string{"cpu"},
				},
			},
			`
WITH matched_fingerprints AS (
SELECT
  DISTINCT Fingerprint
FROM (
  SELECT
    ProjectId,
    MetricName,
    Fingerprint,
    maxMapMerge(Attributes) AS Attributes
  FROM
    metrics.metrics_main_gauge_1h_v2
  WHERE
    ProjectId = {p0:String}
    AND MetricName = {p1:String}
    AND AggTimeUnix >= toStartOfHour({p2:DateTime64(9, 'UTC')})
    AND AggTimeUnix <= toStartOfHour({p3:DateTime64(9, 'UTC')})
  GROUP BY ProjectId, MetricName, Fingerprint, AggTimeUnix
)
WHERE ProjectId = {p4:String} AND MetricName = {p5:String} AND Attributes[{p6:String}] = {p7:String} AND Attributes[{p8:String}] != {p9:String} AND mapContains(Attributes, {p10:String}) )
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  groupArray(AggTimeUnix) AS Times,
  groupArray(AggValue) AS Values,
  mapFilter((k,v)->(k IN {p11:Array(String)}), any(AggAttributes)) AS FilteredAttributes FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  any(Attributes) AS AggAttributes,
  AggTimeUnix,
  sum(SumValue)/sum(CountValue) AS AggValue FROM (
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  maxMapMerge(Attributes) AS Attributes,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue,
  minMerge(MinValueState) AS MinValue,
  maxMerge(MaxValueState) AS MaxValue,
  anyLastMerge(LastValueState) AS LastValue,
  AggTimeUnix
FROM metrics.metrics_main_gauge_1h_v2
WHERE
  ProjectId = {p12:String}
  AND MetricName = {p13:String}
  AND AggTimeUnix >= toStartOfHour({p14:DateTime64(9, 'UTC')})
  AND AggTimeUnix <= toStartOfHour({p15:DateTime64(9, 'UTC')})
  AND Fingerprint IN matched_fingerprints
GROUP BY MetricName, Fingerprint, AggTimeUnix
ORDER BY MetricName, Fingerprint, AggTimeUnix
)
GROUP BY MetricName, Attributes[{attr_0:String}], AggTimeUnix
ORDER BY MetricName, Attributes[{attr_0:String}], AggTimeUnix
)
GROUP BY MetricName, AggAttributes[{attr_0:String}]
ORDER BY MetricName, AggAttributes[{attr_0:String}]`,
			nil,
		),
	)
})
