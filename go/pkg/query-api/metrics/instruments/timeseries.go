package instruments

import (
	"sort"
	"time"

	"go.opentelemetry.io/collector/pdata/pmetric"
)

type TimeSeries struct {
	Fingerprint string
	Datapoints  map[int64][]Datapoint
}

type Datapoint struct {
	AggTimeUnix        time.Time
	TimeUnix           time.Time
	MetricName         string
	MetricDescription  string
	MetricUnit         string
	Attributes         map[string]string
	FilteredAttributes map[string]string
	Fingerprint        string
	Value              float64
	AggTemp            pmetric.AggregationTemporality
	ExemplarTraceIds   []string
}

func NewTimeSeries(fingerprint string) *TimeSeries {
	return &TimeSeries{
		Fingerprint: fingerprint,
		Datapoints:  make(map[int64][]Datapoint),
	}
}

func (ts *TimeSeries) Timestamps() []int64 {
	timestamps := make([]int64, 0)
	for t := range ts.Datapoints {
		timestamps = append(timestamps, t)
	}
	sort.Slice(timestamps, func(i, j int) bool {
		return timestamps[i] < timestamps[j]
	})
	return timestamps
}

func (ts *TimeSeries) AddDatapoints(data map[int64][]Datapoint) {
	for t, dp := range data {
		if _, ok := ts.Datapoints[t]; !ok {
			ts.Datapoints[t] = make([]Datapoint, 0)
		}
		ts.Datapoints[t] = append(ts.Datapoints[t], dp...)
	}
}
