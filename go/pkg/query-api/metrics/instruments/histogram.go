package instruments

import "fmt"

type HDatapoint struct {
	Value    float64
	TraceIDs []string
}

type BucketData map[int64]HDatapoint

type Histogram struct {
	MetricName             string
	MetricDescription      string
	MetricUnit             string
	MetricType             string
	Fingerprint            string
	AggregationTemporality int32
	BucketsHash            uint64
	Buckets                []float64
	Data                   []BucketData
}

func NewHistogram(
	metricName string,
	metricDescription string,
	metricUnit string,
	metricType string,
	fingerprint string,
	aggTemp int32,
	bucketsHash uint64,
	buckets []float64,
) *Histogram {
	h := Histogram{
		MetricName:             metricName,
		MetricDescription:      metricDescription,
		MetricUnit:             metricUnit,
		MetricType:             metricType,
		Fingerprint:            fingerprint,
		AggregationTemporality: aggTemp,
		BucketsHash:            bucketsHash,
		Buckets:                buckets,
		Data:                   make([]BucketData, len(buckets)),
	}
	for idx := range buckets {
		h.Data[idx] = make(map[int64]HDatapoint)
	}
	return &h
}

func (h *Histogram) BucketsAsString() []string {
	response := make([]string, 0)
	for _, b := range h.Buckets {
		response = append(response, fmt.Sprintf("%.3f", b))
	}
	return response
}

func (h *Histogram) AddDatapoint(bucketIdx int, ts int64, value float64, tids []string) {
	// get
	if _, ok := h.Data[bucketIdx][ts]; !ok {
		h.Data[bucketIdx][ts] = HDatapoint{
			Value:    0,
			TraceIDs: make([]string, 0),
		}
	}
	hdp := h.Data[bucketIdx][ts]
	// update
	hdp.Value += value
	if tids != nil {
		hdp.TraceIDs = append(hdp.TraceIDs, tids...)
	}
	// set
	h.Data[bucketIdx][ts] = hdp
}
