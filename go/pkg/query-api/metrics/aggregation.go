package metrics

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

func getAggregationColumn(p pmetric.MetricType, fn AggregateFunction) (string, error) {
	//nolint:exhaustive
	switch p {
	case pmetric.MetricTypeSum, pmetric.MetricTypeGauge:
		return getCounterAggregationColumn(fn)
	}
	return "", fmt.Errorf("unhandled aggregation function:%s for type:%s", string(fn), p)
}

func getCounterAggregationColumn(fn AggregateFunction) (string, error) {
	//nolint:exhaustive
	switch fn {
	case AggregateFunctionLast:
		return "anyLast(LastValue)", nil
	case AggregateFunctionSum:
		return "sum(SumValue)", nil
	case AggregateFunctionAvg:
		return "sum(SumValue)/sum(CountValue)", nil
	case AggregateFunctionCount:
		return "sum(CountValue)", nil
	case AggregateFunctionMax:
		return "max(MaxValue)", nil
	case AggregateFunctionMin:
		return "min(MinValue)", nil
	default:
		return "", fmt.Errorf("unsupported aggregation function: %s", string(fn))
	}
}

func aggregationAttributesToParams(attrs []string) map[string]clickhouse.Literal {
	ls := make(map[string]clickhouse.Literal, len(attrs))
	for i, a := range attrs {
		key := fmt.Sprintf("attr_%d", i)
		ls[key] = clickhouse.Value(a)
	}
	return ls
}
