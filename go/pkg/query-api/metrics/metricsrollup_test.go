package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type gaugeRollupRow struct {
	ProjectID         string    `ch:"ProjectId"`
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	LastIngestedAt    time.Time `ch:"LastIngestedAt"`
	SumValue          float64   `ch:"SumValue"`
	CountValue        uint64    `ch:"CountValue"`
	AvgValue          float64   `ch:"AvgValue"`
}

type sumRollupRow struct {
	ProjectID         string    `ch:"ProjectId"`
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	LastIngestedAt    time.Time `ch:"LastIngestedAt"`
	SumValue          float64   `ch:"SumValue"`
}

const sumRollupQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  max(LastIngestedAt) AS LastIngestedAt,
  sumMerge(SumValueState) AS SumValue
FROM
  %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
GROUP BY ProjectId, MetricName
`

const gaugeRollupQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  max(LastIngestedAt) AS LastIngestedAt,
  sumMerge(SumValueState) AS SumValue,
  countMerge(CountValueState) AS CountValue,
  (SumValue/CountValue) AS AvgValue
FROM
  %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
GROUP BY ProjectId, MetricName
`

var _ = Describe("metrics rollup", Ordered, Serial, func() {
	var (
		ctx     context.Context
		cancel  context.CancelFunc
		testEnv *testutils.ClickHouseServer
		conn    driver.Conn
	)

	BeforeAll(func() {
		ctx, cancel = context.WithTimeout(context.Background(), 180*time.Second)
		DeferCleanup(cancel)
	})

	BeforeEach(func() {
		// create test CH server
		var err error
		testEnv, conn, err = testutils.NewClickHouseServerAndConnection(ctx)
		Expect(err).ToNot(HaveOccurred())
		DeferCleanup(func(ctx SpecContext) {
			testEnv.Terminate(ctx)
		})

		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.MetricsDatabaseName)).To(Succeed())
		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.AnalyticsDatabaseName)).To(Succeed())
	})

	Context("for gauges", func() {
		DescribeTable(
			"querying metrics via materialized view",
			func(mtype pmetric.MetricType, aggregateBucket AggregateBucket) {
				// insert data into raw table, then query it back via the MV
				m := common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 10, common.RandomTraceID[:])
				err := common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())

				tableName, err := getTableForMetricType(mtype)
				Expect(err).ToNot(HaveOccurred())

				aggTableName := fmt.Sprintf("%s_%s_v2", tableName, aggregateBucket)
				query := fmt.Sprintf(gaugeRollupQueryTmpl, constants.MetricsDatabaseName, aggTableName)

				var rows []gaugeRollupRow
				err = conn.Select(ctx, &rows, query, common.RandomProjectID, common.RandomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.AvgValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{10}
					Expect(values).To(Equal(expectedValues))
				}
				// Generate the same value and test average
				m = common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 5, common.RandomTraceID[:])
				err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())
				m = common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 10, common.RandomTraceID[:])
				err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())
				m = common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 5, common.RandomTraceID[:])
				err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())
				rows = []gaugeRollupRow{}
				err = conn.Select(ctx, &rows, query, common.RandomProjectID, common.RandomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.AvgValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{7.5}
					Expect(values).To(Equal(expectedValues))
				}
			},
			Entry("using aggregation: 1m", pmetric.MetricTypeGauge, Minutely),
			Entry("using aggregation: 1h", pmetric.MetricTypeGauge, Hourly),
			Entry("using aggregation: 1d", pmetric.MetricTypeGauge, Daily),
		)
	})

	Context("for sums", func() {
		DescribeTable(
			"querying metrics via materialized view",
			func(mtype pmetric.MetricType, aggregateBucket AggregateBucket) {
				// insert data into raw table, then query it back via the MV
				m := common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 10, common.RandomTraceID[:])
				err := common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())

				tableName, err := getTableForMetricType(mtype)
				Expect(err).ToNot(HaveOccurred())

				aggTableName := fmt.Sprintf("%s_%s_v2", tableName, aggregateBucket)
				query := fmt.Sprintf(sumRollupQueryTmpl, constants.MetricsDatabaseName, aggTableName)

				var rows []sumRollupRow
				err = conn.Select(ctx, &rows, query, common.RandomProjectID, common.RandomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.SumValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{10}
					Expect(values).To(Equal(expectedValues))
				}
				// Generate the same value and test average
				m = common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
					"keyone": "valone",
					"keytwo": "valtwo",
				}, 5, common.RandomTraceID[:])
				err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())
				rows = []sumRollupRow{}
				err = conn.Select(ctx, &rows, query, common.RandomProjectID, common.RandomMetricName)
				Expect(err).ToNot(HaveOccurred())
				{
					values := make([]float64, 0)
					for _, r := range rows {
						GinkgoWriter.Println("row: ", r)
						values = append(values, r.SumValue)
					}
					Expect(values).To(HaveLen(1))
					expectedValues := []float64{15}
					Expect(values).To(Equal(expectedValues))
				}
			},
			Entry("using aggregation: 1m", pmetric.MetricTypeSum, Minutely),
			Entry("using aggregation: 1h", pmetric.MetricTypeSum, Hourly),
			Entry("using aggregation: 1d", pmetric.MetricTypeSum, Daily),
		)
	})

	Context("for histograms with cumulative aggregation", func() {
		DescribeTable(
			"querying metrics via materialized view",
			func(mtype pmetric.MetricType, aggregation string) {
				// insert data into raw table, then query it back via the MV
				startTimestamp := time.Now()
				m := common.GenerateOTELHistogramMetrics(
					startTimestamp,
					time.Now(),
					map[string]string{
						"keyone": "valone",
						"keytwo": "valtwo",
					},
					[]float64{1.0, 2.0, 3.0, 4.0, 5.0},
					[]uint64{1, 1, 1, 1, 1},
					common.RandomTraceID[:],
				)
				err := common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())

				tableName, err := getTableForMetricType(mtype)
				Expect(err).ToNot(HaveOccurred())
				query := fmt.Sprintf(
					baseHistogramDistributionTmpl,
					constants.MetricsDatabaseName,
					tableName+"_"+aggregation,
					timeFuncStartMinute, timeFuncStartMinute,
					"(1 = 1)",
				)

				var rows []HistogramDistributionRow
				err = conn.Select(
					ctx,
					&rows,
					query,
					common.RandomProjectID,
					common.RandomMetricName,
					time.Now().Add(-1*time.Minute),
					time.Now().Add(1*time.Minute),
				)
				Expect(err).ToNot(HaveOccurred())
				{
					Expect(rows).To(HaveLen(1))
					aggData := rows[0]
					Expect(aggData.Delta).To(Equal([]uint64{0, 0, 0, 0, 0}))
					Expect(aggData.Cumulative).To(Equal([]uint64{1, 1, 1, 1, 1}))
				}
				// Generate new values and test distribution again
				m = common.GenerateOTELHistogramMetrics(
					startTimestamp,
					time.Now(),
					map[string]string{
						"keyone": "valone",
						"keytwo": "valtwo",
					},
					[]float64{1.0, 2.0, 3.0, 4.0, 5.0},
					[]uint64{2, 2, 2, 2, 2},
					common.RandomTraceID[:],
				)
				err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, time.Now())
				Expect(err).ToNot(HaveOccurred())

				rows = []HistogramDistributionRow{}
				err = conn.Select(
					ctx,
					&rows,
					query,
					common.RandomProjectID,
					common.RandomMetricName,
					time.Now().Add(-1*time.Minute),
					time.Now().Add(1*time.Minute),
				)
				Expect(err).ToNot(HaveOccurred())
				{
					Expect(rows).To(HaveLen(1))
					aggData := rows[0]
					Expect(aggData.Delta).To(Equal([]uint64{1, 1, 1, 1, 1}))
					Expect(aggData.Cumulative).To(Equal([]uint64{3, 3, 3, 3, 3}))
				}
			},
			Entry("using aggregation:1m",
				pmetric.MetricTypeHistogram,
				"1m",
			),
		)
	})
})
