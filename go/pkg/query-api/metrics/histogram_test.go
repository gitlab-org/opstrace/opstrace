package metrics

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"os"
	"sort"
	"time"

	"github.com/olekukonko/tablewriter"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics/instruments"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

func generateRandomHistogramMetricsData(
	ctx context.Context,
	d *HistogramDataGenerator,
	startTimestamp time.Time,
	endTimestamp time.Time,
) error {
	refTime := time.Now()
	// machine 1
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, refTime, MetricOptions{
		MetricName:             targetMetric,
		MetricType:             pmetric.MetricTypeHistogram.String(),
		AggregationTemporality: pmetric.AggregationTemporalityCumulative,
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-001",
			"host": "ams4-host-001",
		},
	}); err != nil {
		return err
	}
	// machine 2
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, refTime, MetricOptions{
		MetricName:             targetMetric,
		MetricType:             pmetric.MetricTypeHistogram.String(),
		AggregationTemporality: pmetric.AggregationTemporalityCumulative,
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-002",
			"host": "ams4-host-002",
		},
	}); err != nil {
		return err
	}
	return nil
}

var _ = Context("histogram metrics", Serial, Ordered, func() {
	var (
		ctx     context.Context
		cancel  context.CancelFunc
		testEnv *testutils.ClickHouseServer
		conn    driver.Conn
		err     error
	)

	BeforeAll(func() {
		// ensure the tests run within the stipulated timeout
		ctx, cancel = context.WithTimeout(context.Background(), 120*time.Second)
		// create test CH server
		testEnv, conn, err = testutils.NewClickHouseServerAndConnection(ctx)
		Expect(err).ToNot(HaveOccurred())
		DeferCleanup(func(ctx SpecContext) {
			testEnv.Terminate(ctx)
		})

		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.MetricsDatabaseName)).To(Succeed())
		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.AnalyticsDatabaseName)).To(Succeed())
	})

	AfterAll(func() {
		// cancel context
		cancel()
	})

	DescribeTable(
		"search",
		func(q *Query) {
			refTime := time.Now()
			testPeriods := []testPeriod{
				{
					startTimestamp: refTime.Add(-1 * 5 * time.Minute).UTC(),
					endTimestamp:   refTime.UTC(),
				},
			}
			for _, tp := range testPeriods {
				// truncate all tables before each test
				err = truncateOTELHistogramTables(ctx, testEnv)
				Expect(err).ToNot(HaveOccurred())
				// create a data generator
				d := NewHistogramDataGenerator(conn, logger.Desugar())
				// generate test data
				err = generateRandomHistogramMetricsData(ctx, d, tp.startTimestamp, tp.endTimestamp)
				Expect(err).ToNot(HaveOccurred())
				// compute expected data
				computed, err := computeHistogramDistributionResponseRaw(tp.startTimestamp, tp.endTimestamp, d, q)
				Expect(err).ToNot(HaveOccurred())
				// compute expected data
				handler := histogramHandler{db: conn, logger: logger.Desugar()}
				if debugTests {
					qb, err := handler.buildQuery(
						ctx,
						targetProjectID,
						tp.startTimestamp,
						tp.endTimestamp,
						q,
					)
					Expect(err).ToNot(HaveOccurred())
					fmt.Printf("%s\n", qb.SQL())
					fmt.Printf("%+v\n", qb.StringifyParams())
				}
				expectedRaw, err := handler.executeQuery(
					ctx,
					targetProjectID,
					tp.startTimestamp,
					tp.endTimestamp,
					q,
				)
				Expect(err).ToNot(HaveOccurred())
				if debugTests {
					expected, ok := expectedRaw.(*HistogramDistributionResponse)
					Expect(ok).To(BeTrue())
					if ok {
						reportHistogramMetricsResponse(computed, expected)
					}
				}

				computedJSON, err := json.Marshal(computed)
				Expect(err).ToNot(HaveOccurred())
				expectedJSON, err := json.Marshal(expectedRaw)
				Expect(err).ToNot(HaveOccurred())
				// compare outputs
				Expect(expectedJSON).To(Equal(computedJSON))
			}
		},
		Entry(
			"default distribution data",
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeHistogram.String(),
				TargetVisual: targetVisualHeatmap,
				Filters:      []Filter{},
			},
		),
	)
})

func computeHistogramDistributionResponseRaw(
	startTimestamp time.Time,
	endTimestamp time.Time,
	hdg *HistogramDataGenerator,
	query *Query,
) (*HistogramDistributionResponse, error) {
	aggData := hdg.Raw()

	m := &HistogramDistributionResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),
		Results:        make([]HistogramDistributionData, 0),
	}

	if len(aggData) == 0 {
		return m, nil
	}

	latestRow := aggData[len(aggData)-1]
	dpCount := len(latestRow.datapoints)
	latestDP := latestRow.datapoints[dpCount-1]
	latestBounds := latestDP.ExplicitBounds
	// compute bounds hash, also add +Inf like we do when querying the DB
	latestBoundsHash, err := computeBoundsHash(context.TODO(), hdg.db, latestBounds)
	if err != nil {
		return m, err
	}
	latestBounds = append(latestBounds, math.Inf(1))

	// initialize the histogram
	hgram := instruments.NewHistogram(
		latestDP.MetricName,
		latestDP.MetricDescription,
		latestDP.MetricUnit,
		query.TargetType,
		latestDP.Fingerprint,
		latestDP.AggTemp,
		latestBoundsHash,
		latestBounds,
	)
	// process queried data
	for _, r := range aggData {
		traces := make([]string, 0)
		if len(r.datapoints) == 0 {
			continue // no data for this aggregated time
		}
		//
		// for each datapoint aggregated within this time-window, add it to
		// the histogram instance initialized above
		//
		aggregator := NewAggregator(latestBounds)
		for _, dp := range r.datapoints {
			traces = append(traces, dp.ExemplarTraceIds.Strings()...)
			if dp.AggTemp == int32(pmetric.AggregationTemporalityDelta) {
				aggregator.AddDelta(dp.BucketCounts)
			} else {
				aggregator.AddCumulative(dp.BucketCounts)
			}
		}
		//
		// add constructed data to the histogram
		//
		for bucketIdx, count := range aggregator.GetData() {
			hgram.AddDatapoint(bucketIdx, r.pivot.UnixNano(), float64(count), traces)
		}
	}
	// compute distribution for the histogram built above
	d := computeHistogramDistribution(hgram)
	m.Results = append(m.Results, *d)
	return m, nil
}

func reportHistogramMetricsResponse(computed, expected *HistogramDistributionResponse) {
	type formattedData struct {
		eBuckets      []string
		eDistribution []string
		eTraces       []string
		cBuckets      []string
		cDistribution []string
		cTraces       []string
	}

	heatmap := make(map[string]formattedData)

	for _, distributionData := range computed.Results {
		for _, data := range distributionData.Data {
			for _, qps := range data.Distribution {
				for _, qp := range qps {
					if _, ok := heatmap[qp.Time]; !ok {
						heatmap[qp.Time] = formattedData{
							eDistribution: make([]string, 0),
							cDistribution: make([]string, 0),
						}
					}
					entry := heatmap[qp.Time]
					entry.cBuckets = data.Buckets
					entry.cDistribution = append(entry.cDistribution, qp.Value)
					entry.cTraces = qp.ExemplarTraceIds
					heatmap[qp.Time] = entry
				}
			}
		}
	}

	for _, distributionData := range expected.Results {
		for _, data := range distributionData.Data {
			for _, qps := range data.Distribution {
				for _, qp := range qps {
					if _, ok := heatmap[qp.Time]; !ok {
						heatmap[qp.Time] = formattedData{
							eDistribution: make([]string, 0),
							cDistribution: make([]string, 0),
						}
					}
					entry := heatmap[qp.Time]
					entry.eBuckets = data.Buckets
					entry.eDistribution = append(entry.eDistribution, qp.Value)
					entry.eTraces = qp.ExemplarTraceIds
					heatmap[qp.Time] = entry
				}
			}
		}
	}

	tss := make([]string, 0)
	for ts := range heatmap {
		tss = append(tss, ts)
	}
	sort.Slice(tss, func(i, j int) bool {
		return tss[i] < tss[j]
	})

	table := tablewriter.NewWriter(os.Stdout)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetAutoWrapText(false)
	table.SetHeader([]string{
		"TIMESTAMP",
		"COMPUTED BUCKETS", "COMPUTED DISTRIBUTION", "COMPUTED TRACES",
		"EXPECTED BUCKETS", "EXPECTED DISTRIBUTION", "EXPECTED TRACES",
	})
	for _, ts := range tss {
		fdata := heatmap[ts]
		table.Append([]string{
			ts,
			fmt.Sprintf("%v", fdata.cBuckets),
			fmt.Sprintf("%v", fdata.cDistribution),
			fmt.Sprintf("%v", fdata.cTraces),
			fmt.Sprintf("%v", fdata.eBuckets),
			fmt.Sprintf("%v", fdata.eDistribution),
			fmt.Sprintf("%v", fdata.eTraces),
		})
	}
	table.Render()
}
