package metrics

import (
	"fmt"
	"math/rand/v2"
	"sort"
	"time"
)

type GeneratedTimestamp struct {
	Pivot     time.Time
	Generated []time.Time
}

func GenerateTimestamps(startTimestamp, endTimestamp time.Time, count int) ([]GeneratedTimestamp, error) {
	period := endTimestamp.Sub(startTimestamp)
	aggregateBucket, _ := getAggregateBucket(period)

	//nolint:exhaustive
	switch aggregateBucket {
	case Raw:
		return GenerateTimestampsAggregatedPerMinute(startTimestamp, endTimestamp, count), nil
	case Minutely:
		return GenerateTimestampsAggregatedPerMinute(startTimestamp, endTimestamp, count), nil
	case Hourly:
		return GenerateTimestampsAggregatedPerHour(startTimestamp, endTimestamp, count), nil
	case Daily:
		return GenerateTimestampsAggregatedPerDay(startTimestamp, endTimestamp, count), nil
	default:
		return nil, fmt.Errorf("unhandled bucket when generating sample data")
	}
}

// GenerateTimestampsAggregatedPerSecond generates a slice of "count" random timestamps
// grouped per second within a given time-interval: [start, end].
func GenerateTimestampsAggregatedPerSecond(startTimestamp, endTimestamp time.Time, count int) []GeneratedTimestamp {
	aggregatedData := make([]GeneratedTimestamp, 0)
	for second := 0; ; second++ {
		refTime := startTimestamp.Add(time.Duration(second) * time.Second)
		targetSecond := refTime.Second()
		targetTime := time.Date(
			refTime.Year(), refTime.Month(), refTime.Day(),
			refTime.Hour(), refTime.Minute(), targetSecond, 0,
			refTime.Location(),
		)
		if targetTime.After(endTimestamp) {
			break // we're done here
		}
		randomTimestamps := GenerateRandomTimestamps(Secondly, endTimestamp, targetTime, count)
		aggregatedData = append(aggregatedData, GeneratedTimestamp{
			Pivot:     targetTime,
			Generated: randomTimestamps,
		})
	}
	return aggregatedData
}

// GenerateTimestampsAggregatedPerMinute generates a slice of "count" random timestamps
// grouped per minute within a given time-interval: [start, end].
func GenerateTimestampsAggregatedPerMinute(startTimestamp, endTimestamp time.Time, count int) []GeneratedTimestamp {
	aggregatedData := make([]GeneratedTimestamp, 0)
	for min := 0; ; min++ {
		refTime := startTimestamp.Add(time.Duration(min) * time.Minute)
		targetMinute := refTime.Minute()
		targetTime := time.Date(
			refTime.Year(), refTime.Month(), refTime.Day(),
			refTime.Hour(), targetMinute, 0, 0,
			refTime.Location(),
		)
		if targetTime.After(endTimestamp) {
			break // we're done here
		}
		randomTimestamps := GenerateRandomTimestamps(Minutely, endTimestamp, targetTime, count)
		aggregatedData = append(aggregatedData, GeneratedTimestamp{
			Pivot:     targetTime,
			Generated: randomTimestamps,
		})
	}
	return aggregatedData
}

// GenerateTimestampsAggregatedPerHour generates a slice of "count" random timestamps
// grouped per hour within a given time-interval: [start, end].
func GenerateTimestampsAggregatedPerHour(startTimestamp, endTimestamp time.Time, count int) []GeneratedTimestamp {
	aggregatedData := make([]GeneratedTimestamp, 0)
	for hour := 0; ; hour++ {
		refTime := startTimestamp.Add(time.Duration(hour) * time.Hour)
		targetHour := refTime.Hour()
		targetTime := time.Date(
			refTime.Year(), refTime.Month(), refTime.Day(),
			targetHour, 0, 0, 0,
			refTime.Location(),
		)
		if targetTime.After(endTimestamp) {
			break // we're done here
		}
		randomTimestamps := GenerateRandomTimestamps(Hourly, endTimestamp, targetTime, count)
		aggregatedData = append(aggregatedData, GeneratedTimestamp{
			Pivot:     targetTime,
			Generated: randomTimestamps,
		})
	}
	return aggregatedData
}

// GenerateTimestampsAggregatedPerDay generates a slice of "count" random timestamps
// grouped per day within a given time-interval: [start, end].
func GenerateTimestampsAggregatedPerDay(startTimestamp, endTimestamp time.Time, count int) []GeneratedTimestamp {
	aggregatedData := make([]GeneratedTimestamp, 0)
	for day := 0; ; day++ {
		refTime := startTimestamp.Add(time.Duration(day) * 24 * time.Hour)
		targetDay := refTime.Day()
		targetTime := time.Date(
			refTime.Year(), refTime.Month(), targetDay,
			0, 0, 0, 0,
			refTime.Location(),
		)
		if targetTime.After(endTimestamp) {
			break // we're done here
		}
		randomTimestamps := GenerateRandomTimestamps(Daily, endTimestamp, targetTime, count)
		aggregatedData = append(aggregatedData, GeneratedTimestamp{
			Pivot:     targetTime,
			Generated: randomTimestamps,
		})
	}
	return aggregatedData
}

// GenerateRandomTimestamps generates a slice of random timestamps within the given minute
func GenerateRandomTimestamps(
	aggregation AggregateBucket,
	endTimestamp, targetTime time.Time,
	numTimestamps int,
) []time.Time {
	source := rand.NewPCG(uint64(time.Now().Unix()), 0)
	rng := rand.New(source)
	var timestamps []time.Time
	for i := 0; i < numTimestamps; {
		var randomTimestamp time.Time
		//nolint:exhaustive
		switch aggregation {
		case Secondly:
			randomTimestamp = targetTime.Add(
				time.Duration(rng.IntN(100)) * time.Millisecond).Add(
				time.Duration(rng.IntN(100)) * time.Microsecond)
		case Minutely:
			randomTimestamp = targetTime.Add(
				time.Duration(rng.IntN(60)) * time.Second).Add(
				time.Duration(rng.IntN(100)) * time.Millisecond).Add(
				time.Duration(rng.IntN(100)) * time.Microsecond)
		case Hourly:
			randomTimestamp = targetTime.Add(
				time.Duration(rng.IntN(60)) * time.Minute).Add(
				time.Duration(rng.IntN(60)) * time.Second).Add(
				time.Duration(rng.IntN(100)) * time.Millisecond).Add(
				time.Duration(rng.IntN(100)) * time.Microsecond)
		case Daily:
			randomTimestamp = targetTime.Add(
				time.Duration(rng.IntN(24)) * time.Hour).Add(
				time.Duration(rng.IntN(60)) * time.Minute).Add(
				time.Duration(rng.IntN(60)) * time.Second).Add(
				time.Duration(rng.IntN(100)) * time.Millisecond).Add(
				time.Duration(rng.IntN(100)) * time.Microsecond)
		}
		if randomTimestamp.Before(endTimestamp) {
			timestamps = append(timestamps, randomTimestamp)
			i++ // generate timestamps strictly within the desired period
		}
	}
	// sort the timestamps, ordered in time
	sort.Slice(timestamps, func(i, j int) bool {
		return timestamps[i].Before(timestamps[j])
	})
	return timestamps
}
