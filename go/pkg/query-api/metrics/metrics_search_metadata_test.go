package metrics

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

var _ = Context("metrics search metadata", func() {
	var (
		targetProjectID = "12345"
		targetMetric    = "some_metric_name"
	)

	DescribeTable(
		"building queries",
		func(projectID string, q *Query, expectedQuery string, expectedErr error) {
			qb, err := buildSearchMetadataQuery(projectID, q)
			if expectedErr != nil {
				Expect(err).To(HaveOccurred())
				Expect(err).To(Equal(expectedErr))
			} else {
				Expect(err).ToNot(HaveOccurred())
				err = testutils.CompareSQLStrings(qb.SQL(), expectedQuery)
				Expect(err).ToNot(HaveOccurred())
			}
		},
		Entry(
			"query search metadata with missing metric name",
			targetProjectID,
			&Query{
				TargetType: pmetric.MetricTypeSum.String(),
			},
			"",
			errMissingTargetMetricName,
		),
		Entry(
			"query search metadata with missing metric type",
			targetProjectID,
			&Query{
				TargetMetric: targetMetric,
			},
			"",
			errMissingTargetMetricType,
		),
		Entry(
			"query search metadata for metric type: sum",
			targetProjectID,
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeSum.String(),
			},
			`
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS AttributeKeys,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_main_sum_metadata
WHERE
  ProjectId = {p0:String}
  AND MetricName = {p1:String}
GROUP BY ProjectId, MetricName`,
			nil,
		),
		Entry(
			"query search metadata for metric type: gauge",
			targetProjectID,
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeGauge.String(),
			},
			`
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS AttributeKeys,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_main_gauge_metadata
WHERE
  ProjectId = {p0:String}
  AND MetricName = {p1:String}
GROUP BY ProjectId, MetricName`,
			nil,
		),
		Entry(
			"query search metadata for metric type: histogram",
			targetProjectID,
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeHistogram.String(),
			},
			`
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS AttributeKeys,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_main_histogram_metadata
WHERE
  ProjectId = {p0:String}
  AND MetricName = {p1:String}
GROUP BY ProjectId, MetricName`,
			nil,
		),
		Entry(
			"query search metadata for metric type: exponential histogram",
			targetProjectID,
			&Query{
				TargetMetric: targetMetric,
				TargetType:   pmetric.MetricTypeExponentialHistogram.String(),
			},
			`
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS AttributeKeys,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  metrics.metrics_main_exp_histogram_metadata
WHERE
  ProjectId = {p0:String}
  AND MetricName = {p1:String}
GROUP BY ProjectId, MetricName`,
			nil,
		),
	)
})
