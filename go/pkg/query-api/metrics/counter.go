package metrics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
)

type CounterMetricsResponse struct {
	StartTimestamp int64       `json:"start_ts"`
	EndTimestamp   int64       `json:"end_ts"`
	Results        []QueryData `json:"results"`
}

type QueryData struct {
	MetricName        string            `json:"name"`
	MetricDescription string            `json:"description"`
	MetricUnit        string            `json:"unit"`
	MetricType        string            `json:"type"`
	Attributes        map[string]string `json:"attributes"`
	Values            []QueryPoint      `json:"values,omitempty"`
}

type MetricRow struct {
	MetricName        string                `ch:"MetricName"`
	MetricDescription string                `ch:"MetricDescription"`
	MetricUnit        string                `ch:"MetricUnit"`
	Fingerprint       string                `ch:"Fingerprint"`
	Attributes        map[string]string     `ch:"FilteredAttributes"`
	Values            []float64             `ch:"Values"`
	Times             []time.Time           `ch:"Times"`
	TraceIds          []AggExemplarTraceIds `ch:"TraceIds"`
}

// AggExemplarTraceIds -> Array(String) grouped by time, grouped by aggregation
type AggExemplarTraceIds [][][]string

func getCounterSearchMetadata(
	ctx context.Context,
	logger *zap.Logger,
	db clickhouse.Conn,
	projectID string,
	query *Query,
) (interface{}, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.SearchMetadataQuery", trace.SpanKindClient)
	defer span.End()

	qb, err := buildSearchMetadataQuery(projectID, query)
	if err != nil {
		return nil, fmt.Errorf("building metrics query: %w", err)
	}

	logger.Debug(
		"executing search metadata query",
		zap.String("query string", qb.SQL()),
		zap.Any("query args", qb.Params()),
	)

	var rows []MetricSearchMetadataRow
	if err := db.Select(
		qb.Context(ctx),
		&rows,
		qb.SQL(),
	); err != nil {
		return nil, fmt.Errorf("executing metrics search metadata query: %w", err)
	}

	response := MetricSearchMetadataResponse{}
	for _, r := range rows {
		response.MetricName = r.MetricName
		response.MetricType = query.TargetType
		response.MetricDescription = r.MetricDescription
		response.AttributeKeys = r.AttributeKeys
		response.LastIngestedAt = r.LastIngestedAt.UnixNano()
	}
	response.SupportedAggregations = SupportedTimeAggregations
	response.SupportedFunctions = []string{
		string(AggregateFunctionAvg),
		string(AggregateFunctionSum),
		string(AggregateFunctionMin),
		string(AggregateFunctionMax),
		string(AggregateFunctionCount),
	}
	response.DefaultGroupByAttributes = []string{GroupByAttributesAll}
	response.DefaultGroupByFunction = string(AggregateFunctionSum)
	return response, nil
}
