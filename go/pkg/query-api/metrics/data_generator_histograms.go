package metrics

import (
	"context"
	"fmt"
	"math"
	"math/rand"
	"slices"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/google/uuid"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/metrics"
	chinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	testutilscommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	conventions "go.opentelemetry.io/collector/semconv/v1.25.0"
	"go.uber.org/zap"
)

type HistogramDatapoint struct {
	MetricName         string
	MetricDescription  string
	MetricUnit         string
	StartTimestampUnix time.Time
	TimeUnix           time.Time
	Fingerprint        string
	Attributes         map[string]string
	AggTemp            int32
	ExplicitBounds     []float64
	BucketCounts       []uint64
	Sum                float64
	Count              uint64
	Min                float64
	Max                float64
	ExemplarTraceIds   uuid.UUIDs
	// computed when datapoints are aggregated
	AggTimeUnix  time.Time
	SamplesCount uint64
	BoundsHash   uint64
	Delta        []uint64
	Cumulative   []uint64
}

type HistogramDataGenerator struct {
	raw    []HistogramTimeseries
	db     clickhouse.Conn
	logger *zap.Logger
}

func NewHistogramDataGenerator(db clickhouse.Conn, logger *zap.Logger) *HistogramDataGenerator {
	return &HistogramDataGenerator{
		raw:    make([]HistogramTimeseries, 0),
		db:     db,
		logger: logger,
	}
}

func (d *HistogramDataGenerator) GenerateMetrics(
	ctx context.Context,
	startTimestamp time.Time,
	endTimestamp time.Time,
	ingestionTimestamp time.Time,
	opts MetricOptions,
) error {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, RandomServiceName)
	rm.Resource().SetDroppedAttributesCount(0)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(0)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	if opts.DatapointsCount == 0 {
		opts.DatapointsCount = 2 // default number of datapoints per aggregation window
	}

	generatedTimestamps, err := GenerateTimestamps(startTimestamp, endTimestamp, opts.DatapointsCount)
	if err != nil {
		return fmt.Errorf("generating timestamps: %w", err)
	}
	generatedTimeseries := d.generate(generatedTimestamps, opts)
	d.raw = append(d.raw, generatedTimeseries...)

	for _, ts := range generatedTimeseries {
		for _, gdp := range ts.datapoints {
			m := sm.Metrics().AppendEmpty()
			m.SetName(gdp.MetricName)
			m.SetDescription(gdp.MetricDescription)
			m.SetUnit(gdp.MetricUnit)

			dp := m.SetEmptyHistogram().DataPoints().AppendEmpty()
			dp.SetCount(gdp.Count)
			dp.SetSum(gdp.Sum)
			dp.SetMin(gdp.Min)
			dp.SetMax(gdp.Max)
			dp.ExplicitBounds().FromRaw(gdp.ExplicitBounds)
			dp.BucketCounts().FromRaw(gdp.BucketCounts)

			for k, v := range gdp.Attributes {
				dp.Attributes().PutStr(k, v)
			}

			dp.SetStartTimestamp(pcommon.NewTimestampFromTime(gdp.StartTimestampUnix))
			dp.SetTimestamp(pcommon.NewTimestampFromTime(gdp.TimeUnix))

			exemplars := dp.Exemplars().AppendEmpty()
			exemplars.SetIntValue(10)
			exemplars.FilteredAttributes().PutStr("key", "value")
			exemplars.SetTraceID(pcommon.TraceID(gdp.ExemplarTraceIds[0]))
			exemplars.SetSpanID(RandomSpanID)

			m.Histogram().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)
		}
	}
	// ingest data
	if err := testutilscommon.PersistOTELMetrics(ctx, d.db, d.logger, metrics, ingestionTimestamp); err != nil {
		return err
	}
	return nil
}

func (d *HistogramDataGenerator) Raw() []HistogramTimeseries {
	return d.raw
}

//nolint:unparam
func (d *HistogramDataGenerator) RawDataAggregated(q *Query) ([]HistogramDatapoint, error) {
	datapoints := make([]HistogramDatapoint, 0)
	for _, ts := range d.raw {
		lenDatapoints := len(ts.datapoints)
		if lenDatapoints == 0 {
			continue
		}
		anyDP := ts.datapoints[0] // pick a datapoint
		boundsHash, err := computeBoundsHash(context.Background(), d.db, anyDP.ExplicitBounds)
		if err != nil {
			return nil, err
		}

		lenBuckets := len(ts.datapoints[0].BucketCounts)
		traces := make([]uuid.UUID, 0)
		samplesCount := uint64(0)

		// build cumulative, traces, count of samples
		cumulative := make([]uint64, lenBuckets)
		for _, dp := range ts.datapoints {
			samplesCount++
			traces = append(traces, dp.ExemplarTraceIds...)
			for i, cnt := range dp.BucketCounts {
				cumulative[i] += cnt
			}
		}
		// build delta
		delta := make([]uint64, lenBuckets)
		if lenDatapoints == 1 {
			delta = ts.datapoints[0].BucketCounts
		} else {
			lastDP := ts.datapoints[lenDatapoints-1]
			for i, cnt := range lastDP.BucketCounts {
				delta[i] += cnt
			}
			firstDP := ts.datapoints[0]
			for i, cnt := range firstDP.BucketCounts {
				delta[i] -= cnt
			}
		}
		anyDP.AggTimeUnix = ts.pivot
		anyDP.Delta = delta
		anyDP.Cumulative = cumulative
		anyDP.SamplesCount = samplesCount
		anyDP.ExemplarTraceIds = traces
		anyDP.BoundsHash = boundsHash
		anyDP.ExplicitBounds = append(anyDP.ExplicitBounds, math.Inf(1))
		datapoints = append(datapoints, anyDP)
	}
	return datapoints, nil
}

type HistogramTimeseries struct {
	pivot      time.Time
	datapoints []HistogramDatapoint
}

//nolint:funlen
func (d *HistogramDataGenerator) generate(timestamps []GeneratedTimestamp, opts MetricOptions) []HistogramTimeseries {
	var (
		totalMin int64
		totalMax int64
		totalSum int64
	)

	startTime := timestamps[0].Pivot // pick first always!
	explicitBounds := []float64{1, 2, 3, 4, 5}
	explicitBoundsMax := slices.Max(explicitBounds)
	cumulativeCounts := make([]uint64, len(explicitBounds)+1)

	computeBucket := func(r int64) int {
		for idx, upperBound := range explicitBounds {
			if upperBound >= float64(r) {
				return idx
			}
		}
		return len(explicitBounds) // +Inf bucket
	}

	resetFn := func() {
		totalMin = 0
		totalMax = 0
		totalSum = 0
	}

	recordFn := func(r int64) {
		if totalMin == 0 {
			totalMin = r
		} else if r < totalMin {
			totalMin = r
		}
		if totalMax == 0 {
			totalMax = r
		} else if r > totalMax {
			totalMax = r
		}
		totalSum += r
		// check which bucket this reading belongs to
		bucketIdx := computeBucket(r)
		// sum up cumulative counts
		cumulativeCounts[bucketIdx]++
	}

	attrs := make(map[string]string)
	attrs[common.TenantIDHeader] = RandomTenantID
	attrs[common.ProjectIDHeader] = RandomProjectID
	attrs[common.NamespaceIDHeader] = RandomNamespaceID
	attrs[common.MetricAggregationTemporalityHeader] = pmetric.AggregationTemporalityCumulative.String()
	for k, v := range opts.Attributes {
		attrs[k] = v
	}
	fp := metrics.GetMetricFingerprint(opts.MetricName, attrs)

	data := make([]HistogramTimeseries, 0)
	rng := rand.New(rand.NewSource(time.Now().Unix())) //nolint:gosec
	for _, gts := range timestamps {
		resetFn()
		datapoints := make([]HistogramDatapoint, 0)
		for _, t := range gts.Generated {
			traceID, err := GenerateRandomTraceID()
			if err != nil {
				panic(err)
			}
			traceIDUUID, err := uuid.FromBytes(traceID)
			if err != nil {
				panic(err)
			}
			// generate recordings, atleast one
			dpCounts := rng.Intn(10) + 1
			for r := 0; r < dpCounts; r++ {
				reading := rng.Int63() % int64(explicitBoundsMax+5)
				recordFn(reading)
			}
			dp := HistogramDatapoint{
				MetricName:         opts.MetricName,
				MetricDescription:  "randomDescription",
				MetricUnit:         "randomUnit",
				StartTimestampUnix: startTime,
				TimeUnix:           t,
				Fingerprint:        fp,
				Attributes:         attrs,
				AggTemp:            int32(pmetric.AggregationTemporalityCumulative),
				ExplicitBounds:     explicitBounds,
				Sum:                float64(totalSum),
				Count:              uint64(dpCounts),
				Min:                float64(totalMin),
				Max:                float64(totalMax),
				ExemplarTraceIds:   []uuid.UUID{traceIDUUID},
			}
			dp.BucketCounts = make([]uint64, len(cumulativeCounts))
			copy(dp.BucketCounts, cumulativeCounts)
			datapoints = append(datapoints, dp)
		}
		data = append(data, HistogramTimeseries{
			pivot:      gts.Pivot,
			datapoints: datapoints,
		})
	}
	return data
}

func computeBoundsHash(
	ctx context.Context,
	db clickhouse.Conn,
	bounds []float64,
) (uint64, error) {
	qb := chinternal.QueryBuilder{}
	qb.Build(
		"SELECT sipHash64(arrayPushBack(?, +inf)) AS BoundsHash",
		chinternal.Array[float64](bounds),
	)
	var rows []struct {
		BoundsHash uint64 `ch:"BoundsHash"`
	}
	if err := db.Select(qb.Context(ctx), &rows, qb.SQL()); err != nil {
		return 0, fmt.Errorf("computing bounds hash: %w", err)
	}
	return rows[0].BoundsHash, nil
}
