package metrics

import (
	"context"
	cryptorand "crypto/rand"
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/google/uuid"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/metrics"
	testutilscommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics/instruments"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	conventions "go.opentelemetry.io/collector/semconv/v1.25.0"
	"go.uber.org/zap"
)

const (
	RandomProjectID   string = "12345"
	RandomTenantID    string = "orgfoo:123"
	RandomNamespaceID string = "123"
	RandomServiceName string = "test-service"
	RandomMetricName  string = "random-metric-name"
)

var (
	RandomTraceID = [16]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4}
	RandomSpanID  = [8]byte{0, 0, 0, 0, 1, 2, 3, 4}
)

type DataGenerator struct {
	raw    []instruments.Datapoint
	db     clickhouse.Conn
	logger *zap.Logger
}

func NewDataGenerator(db clickhouse.Conn, logger *zap.Logger) *DataGenerator {
	return &DataGenerator{
		raw:    make([]instruments.Datapoint, 0),
		db:     db,
		logger: logger,
	}
}

type MetricOptions struct {
	MetricName             string
	MetricType             string
	AggregationTemporality pmetric.AggregationTemporality
	Attributes             map[string]string
	DatapointsCount        int
}

//nolint:funlen,cyclop
func (d *DataGenerator) GenerateMetrics(
	ctx context.Context,
	startTimestamp time.Time,
	endTimestamp time.Time,
	ingestionTimestamp time.Time,
	opts MetricOptions,
) error {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, RandomServiceName)
	rm.Resource().SetDroppedAttributesCount(0)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(0)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	period := endTimestamp.Sub(startTimestamp)
	aggregateBucket, _ := getAggregateBucket(period)

	if opts.DatapointsCount == 0 {
		opts.DatapointsCount = 5 // default number of datapoints to generate every aggregated period
	}

	var generated []GeneratedTimestamp
	//nolint:exhaustive
	switch aggregateBucket {
	case Raw:
		generated = GenerateTimestampsAggregatedPerSecond(startTimestamp, endTimestamp, opts.DatapointsCount)
	case Minutely:
		generated = GenerateTimestampsAggregatedPerMinute(startTimestamp, endTimestamp, opts.DatapointsCount)
	case Hourly:
		generated = GenerateTimestampsAggregatedPerHour(startTimestamp, endTimestamp, opts.DatapointsCount)
	case Daily:
		generated = GenerateTimestampsAggregatedPerDay(startTimestamp, endTimestamp, opts.DatapointsCount)
	default:
		return fmt.Errorf("unhandled bucket when generating sample data")
	}

	// make sure to add exemplars when supported
	addExemplars := aggregateBucket == Raw
	// add some jitter to when it's generated
	if addExemplars {
		addExemplars = rand.Intn(100)%2 == 0 //nolint:gosec
	}

	var value float64 // initialize values to be ingested to account for aggregation temporality
	for _, g := range generated {
		for _, ts := range g.Generated {
			switch opts.MetricType {
			case pmetric.MetricTypeSum.String():
				if opts.AggregationTemporality == pmetric.AggregationTemporalityDelta {
					value = GenerateRandomFloat64(5.0, 10.0)
				} else {
					value += GenerateRandomFloat64(5.0, 10.0)
				}
				g := instruments.Datapoint{
					AggTimeUnix:       g.Pivot.UTC(),
					TimeUnix:          ts.UTC(),
					MetricName:        opts.MetricName,
					MetricDescription: "Random description for a random sum metric",
					MetricUnit:        "count",
					Attributes:        opts.Attributes,
					Value:             value,
					AggTemp:           opts.AggregationTemporality,
					ExemplarTraceIds:  make([]string, 0),
				}

				traceID, err := GenerateRandomTraceID()
				if err != nil {
					return err
				}
				traceIDUUID, err := uuid.FromBytes(traceID)
				if err != nil {
					return fmt.Errorf("constructing trace uuid from bytes: %w", err)
				}
				if addExemplars {
					g.ExemplarTraceIds = append(g.ExemplarTraceIds, traceIDUUID.String())
				}
				d.raw = append(d.raw, g)

				m := sm.Metrics().AppendEmpty()
				m.SetName(opts.MetricName)
				m.SetUnit("count")
				m.SetDescription("Random description for a random sum metric")

				dp := m.SetEmptySum().DataPoints().AppendEmpty()
				dp.SetDoubleValue(value)
				dp.Attributes().PutStr(common.TenantIDHeader, RandomTenantID)
				dp.Attributes().PutStr(common.ProjectIDHeader, RandomProjectID)
				dp.Attributes().PutStr(common.NamespaceIDHeader, RandomNamespaceID)
				for k, v := range opts.Attributes {
					dp.Attributes().PutStr(k, v)
				}
				dp.SetStartTimestamp(pcommon.NewTimestampFromTime(startTimestamp))
				dp.SetTimestamp(pcommon.NewTimestampFromTime(ts))

				m.Sum().SetAggregationTemporality(opts.AggregationTemporality)

				if addExemplars {
					exemplars := dp.Exemplars().AppendEmpty()
					exemplars.SetIntValue(10)
					exemplars.FilteredAttributes().PutStr("key", "value")
					exemplars.SetTraceID(pcommon.TraceID(traceID))
					exemplars.SetSpanID(RandomSpanID)
				}
			case pmetric.MetricTypeGauge.String():
				value = GenerateRandomFloat64(5.0, 10.0)
				g := instruments.Datapoint{
					AggTimeUnix:       g.Pivot.UTC(),
					TimeUnix:          ts.UTC(),
					MetricName:        opts.MetricName,
					MetricDescription: "Random description for a random gauge metric",
					MetricUnit:        "count",
					Attributes:        opts.Attributes,
					Value:             value,
					ExemplarTraceIds:  make([]string, 0),
				}

				traceID, err := GenerateRandomTraceID()
				if err != nil {
					return err
				}
				traceIDUUID, err := uuid.FromBytes(traceID)
				if err != nil {
					return fmt.Errorf("constructing trace uuid from bytes: %w", err)
				}
				if addExemplars {
					g.ExemplarTraceIds = append(g.ExemplarTraceIds, traceIDUUID.String())
				}
				d.raw = append(d.raw, g)

				m := sm.Metrics().AppendEmpty()
				m.SetName(opts.MetricName)
				m.SetUnit("count")
				m.SetDescription("Random description for a random gauge metric")

				dp := m.SetEmptyGauge().DataPoints().AppendEmpty()
				dp.SetDoubleValue(value)
				dp.Attributes().PutStr(common.TenantIDHeader, RandomTenantID)
				dp.Attributes().PutStr(common.ProjectIDHeader, RandomProjectID)
				dp.Attributes().PutStr(common.NamespaceIDHeader, RandomNamespaceID)
				for k, v := range opts.Attributes {
					dp.Attributes().PutStr(k, v)
				}
				dp.SetStartTimestamp(pcommon.NewTimestampFromTime(startTimestamp))
				dp.SetTimestamp(pcommon.NewTimestampFromTime(ts))

				if addExemplars {
					exemplars := dp.Exemplars().AppendEmpty()
					exemplars.SetIntValue(10)
					exemplars.FilteredAttributes().PutStr("key", "value")
					exemplars.SetTraceID(pcommon.TraceID(traceID))
					exemplars.SetSpanID(RandomSpanID)
				}
			}
		}
	}
	// ingest data
	if err := testutilscommon.PersistOTELMetrics(ctx, d.db, d.logger, metrics, ingestionTimestamp); err != nil {
		return err
	}
	return nil
}

func (d *DataGenerator) RawData() []instruments.Datapoint {
	return d.raw
}

func isFiltered(m instruments.Datapoint, filters []Filter) bool {
	for _, filter := range filters {
		switch filter.Operator {
		case string(FilterOpEqual):
			if m.Attributes[filter.Key] != filter.Value {
				return true
			}
		case string(FilterOpNotEqual):
			if m.Attributes[filter.Key] == filter.Value {
				return true
			}
		}
	}
	return false
}

// getFilteredAttributes helps narrow down a given set of attributes like
// we find within our metrics for the aggregation being composed as needed
// by the query being processed.
func getFilteredAttributes(attrs map[string]string, query *Query) map[string]string {
	filteredAttrs := make(map[string]string)
	groupingType := query.GetGroupingType()
	switch groupingType {
	case GroupingOverAttributes:
		// when grouping with passed attributes, only those attributes
		// need to be used for aggregation
		for _, k := range query.GroupBy.Attributes {
			if v, ok := attrs[k]; ok {
				filteredAttrs[k] = v
			}
		}
	case GroupingOverMetricName:
		// filter out all attributes since the aggregation should be
		// over the metric name only
	case GroupingOverNone:
		// do not filter out any attributes since we're fetching all
		// data as is, i.e. without any grouping
		for k, v := range attrs {
			filteredAttrs[k] = v
		}
	}
	return filteredAttrs
}

const (
	fetchFingerprintsQueryTmpl string = `
SELECT
  arraySort(groupUniqArray(Fingerprint)) AS Fingerprints
FROM %s.%s
WHERE
  MetricName = '%s'
GROUP BY %s
ORDER BY %s
`
)

//nolint:funlen,cyclop
func (d *DataGenerator) RawDataAggregated(q *Query) []*instruments.TimeSeries {
	// apply filters as necessary
	filteredData := make([]instruments.Datapoint, 0)
	if len(q.Filters) > 0 {
		for _, m := range d.raw {
			if isFiltered(m, q.Filters) {
				continue
			}
			filteredData = append(filteredData, m)
		}
	} else {
		filteredData = append(filteredData, d.raw...)
	}
	// apply grouping by fingerprint
	groupedData := make(map[string][]instruments.Datapoint)
	for _, m := range filteredData {
		m.FilteredAttributes = getFilteredAttributes(m.Attributes, q)
		attrs := m.Attributes
		attrs[common.TenantIDHeader] = RandomTenantID
		attrs[common.ProjectIDHeader] = RandomProjectID
		attrs[common.NamespaceIDHeader] = RandomNamespaceID
		// add aggregation temporality to fingerprinting only when it's
		// defined for the given metric type, e.g. it's unspecified for
		// gauges
		if m.AggTemp != 0 {
			attrs[common.MetricAggregationTemporalityHeader] = m.AggTemp.String()
		}
		fp := metrics.GetMetricFingerprint(m.MetricName, attrs)
		m.Fingerprint = fp
		if _, ok := groupedData[fp]; !ok {
			groupedData[fp] = make([]instruments.Datapoint, 0)
		}
		groupedData[fp] = append(groupedData[fp], m)
	}
	// apply time-aggregation
	data := make(map[string]map[int64][]instruments.Datapoint)
	for fp, metrics := range groupedData {
		for _, m := range metrics {
			t := m.AggTimeUnix.UnixNano()
			if _, ok := data[fp]; !ok {
				data[fp] = make(map[int64][]instruments.Datapoint)
			}
			if _, ok := data[fp][t]; !ok {
				data[fp][t] = make([]instruments.Datapoint, 0)
			}
			data[fp][t] = append(data[fp][t], m)
		}
	}
	tss := make([]*instruments.TimeSeries, 0)
	// group data as requested in the query
	groupingType := q.GetGroupingType()
	switch groupingType {
	case GroupingOverMetricName:
		// sort fingerprints
		fps := make([]string, 0)
		for fp := range data {
			fps = append(fps, fp)
		}
		sort.Strings(fps)
		// merge all fingerprints in a single timeseries
		ts := instruments.NewTimeSeries("randomfp")
		for _, fp := range fps {
			ts.AddDatapoints(data[fp])
		}
		tss = append(tss, ts)
	case GroupingOverAttributes:
		dbName := "metrics"
		tableName, err := getTableForMetricType(getMetricTypeFromString(q.TargetType))
		if err != nil {
			return tss
		}
		var groupByComponents []string
		for _, attr := range q.GroupBy.Attributes {
			groupByComponents = append(groupByComponents, fmt.Sprintf("Attributes['%s']", attr))
		}
		groupByStr := strings.Join(groupByComponents, ",")
		var rows []struct {
			Fingerprints []string `ch:"Fingerprints"`
		}
		if err := d.db.Select(
			context.Background(),
			&rows,
			fmt.Sprintf(fetchFingerprintsQueryTmpl,
				dbName, tableName,
				q.TargetMetric,
				groupByStr,
				groupByStr,
			),
		); err != nil {
			return tss
		}
		for _, r := range rows {
			ts := instruments.NewTimeSeries("random-fp")
			for _, fp := range r.Fingerprints {
				ts.AddDatapoints(data[fp])
			}
			tss = append(tss, ts)
		}
	case GroupingOverNone:
		// sort fingerprints
		fps := make([]string, 0)
		for fp := range data {
			fps = append(fps, fp)
		}
		sort.Strings(fps)
		// create a new timeseries for each fingerprint
		for _, fp := range fps {
			ts := instruments.NewTimeSeries(fp)
			ts.AddDatapoints(data[fp])
			tss = append(tss, ts)
		}
	}
	return tss
}

// Generate a random float64 value within the range [min .. max)
func GenerateRandomFloat64(min, max float64) float64 {
	return min + rand.Float64()*(max-min) //nolint:gosec
}

func GenerateRandomTraceID() ([]byte, error) {
	bytes := make([]byte, 16)
	if _, err := cryptorand.Read(bytes); err != nil {
		return nil, fmt.Errorf("reading random bytes: %w", err)
	}
	return bytes, nil
}
