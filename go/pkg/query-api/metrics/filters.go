package metrics

import (
	"context"
	"fmt"
	"time"

	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

const (
	noFiltersQueryQualifier           = "(1 = 1)"
	matchedFingerprintsQueryQualifier = "Fingerprint IN matched_fingerprints"
)

const filteredMetricsQueryTmpl string = `
WITH matched_fingerprints AS (
SELECT DISTINCT Fingerprint FROM %s.%s
`

func buildFilteredMetricsQuery(
	ctx context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
	qb *clickhouse.QueryBuilder,
) (*clickhouse.QueryBuilder, error) {
	if qb == nil || query == nil {
		return nil, fmt.Errorf("query builder or query cannot be nil")
	}
	if query.Filters == nil {
		return qb, nil // nothing to do
	}
	if len(query.Filters) == 0 {
		return qb, nil // nothing to do
	}

	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	period := endTimestamp.Sub(startTimestamp)
	aggregateBucket, _ := getAggregateBucket(period)

	switch query.TargetType {
	case pmetric.MetricTypeGauge.String(), pmetric.MetricTypeSum.String():
		if aggregateBucket == Raw {
			return buildFilteredMetricsQueryFromSource(
				ctx,
				projectID,
				startTimestamp,
				endTimestamp,
				query,
				qb,
			)
		}
		return buildFilteredMetricsQueryFromAggregates(
			ctx,
			projectID,
			startTimestamp,
			endTimestamp,
			query,
			qb,
		)
	case pmetric.MetricTypeHistogram.String(), pmetric.MetricTypeExponentialHistogram.String():
		if aggregateBucket == Raw {
			return buildFilteredMetricsQueryFromSource(
				ctx,
				projectID,
				startTimestamp,
				endTimestamp,
				query,
				qb,
			)
		}
		return buildFilteredMetricsQueryForHistograms(
			ctx,
			projectID,
			startTimestamp,
			endTimestamp,
			query,
			qb,
		)
	default:
		return nil, fmt.Errorf("couldn't handle filtering for metric type %s", query.TargetType)
	}
}

func buildFilteredMetricsQueryFromSource(
	_ context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
	qb *clickhouse.QueryBuilder,
) (*clickhouse.QueryBuilder, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	tableName, err := getTableForMetricType(mtype)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	qb.Build(
		fmt.Sprintf(
			filteredMetricsQueryTmpl,
			constants.MetricsDatabaseName,
			tableName,
		),
	)
	qb.Build("WHERE")
	qb.Build(" ProjectId = ?", projectID)
	qb.Build(" AND MetricName = ?", query.TargetMetric)
	qb.Build(" AND TimeUnix >= ?", startTimestamp)
	qb.Build(" AND TimeUnix <= ?", endTimestamp)
	qb, err = addAttributeFilters(qb, query.Filters)
	if err != nil {
		return qb, err
	}
	qb.Build(`)`)
	return qb, nil
}

const filteredHistogramMetricsQueryTmpl string = `
WITH matched_fingerprints AS (
  SELECT Fingerprint FROM %s.%s
`

func buildFilteredMetricsQueryForHistograms(
	_ context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
	qb *clickhouse.QueryBuilder,
) (*clickhouse.QueryBuilder, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	tableName, err := getTableForMetricType(mtype)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	qb.Build(
		fmt.Sprintf(
			filteredHistogramMetricsQueryTmpl,
			constants.MetricsDatabaseName,
			tableName,
		),
	)
	qb.Build("WHERE")
	qb.Build(" ProjectId = ?", projectID)
	qb.Build(" AND MetricName = ?", query.TargetMetric)
	qb.Build(" AND TimeUnix >= ?", startTimestamp)
	qb.Build(" AND TimeUnix <= ?", endTimestamp)
	if query.Correlate.TraceID != "" {
		qb.Build(
			" AND has(arrayMap((x) -> UUIDNumToString(x), Exemplars.TraceId), ?)",
			clickhouse.String(query.Correlate.TraceID),
		)
	}
	qb, err = addAttributeFilters(qb, query.Filters)
	if err != nil {
		return qb, err
	}
	qb.Build("ORDER BY TimeUnix DESC LIMIT 1")
	qb.Build(`)`) // end query
	return qb, nil
}

const filteredAggregatedMetricsQueryTmpl = `
WITH matched_fingerprints AS (
SELECT
  DISTINCT Fingerprint
FROM (
  SELECT
    ProjectId,
    MetricName,
    Fingerprint,
    maxMapMerge(Attributes) AS Attributes
  FROM
    %s.%s
  WHERE
    ProjectId = ?
    AND MetricName = ?
    AND AggTimeUnix >= %s(?)
    AND AggTimeUnix <= %s(?)
  GROUP BY ProjectId, MetricName, Fingerprint, AggTimeUnix
)
`

func buildFilteredMetricsQueryFromAggregates(
	_ context.Context,
	projectID string,
	startTimestamp time.Time,
	endTimestamp time.Time,
	query *Query,
	qb *clickhouse.QueryBuilder,
) (*clickhouse.QueryBuilder, error) {
	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	period := endTimestamp.Sub(startTimestamp)
	aggregateBucket, timeFunc := getAggregateBucket(period)
	tableName, err := getAggTableNameForMetricType(mtype, aggregateBucket)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	qb.Build(
		fmt.Sprintf(
			filteredAggregatedMetricsQueryTmpl,
			constants.MetricsDatabaseName,
			tableName,
			timeFunc,
			timeFunc,
		),
		projectID,
		query.TargetMetric,
		startTimestamp,
		endTimestamp,
	)
	qb.Build("WHERE")
	qb.Build(" ProjectId = ?", projectID)
	qb.Build(" AND MetricName = ?", query.TargetMetric)
	qb, err = addAttributeFilters(qb, query.Filters)
	if err != nil {
		return qb, err
	}
	qb.Build(`)`)
	return qb, nil
}

type FilterOp string

const (
	FilterOpNoOp         FilterOp = "noop"
	FilterOpEqual        FilterOp = "eq"
	FilterOpNotEqual     FilterOp = "neq"
	FilterOpLike         FilterOp = "re"
	FilterOpNotLike      FilterOp = "nre"
	FilterOpHasAttribute FilterOp = "hasAttr"
)

func addAttributeFilters(qb *clickhouse.QueryBuilder, filters []Filter) (*clickhouse.QueryBuilder, error) {
	for _, f := range filters {
		switch f.Operator {
		case string(FilterOpNoOp):
			// do nothing
		case string(FilterOpEqual):
			qb.Build(" AND Attributes[?] = ?", f.Key, f.Value)
		case string(FilterOpNotEqual):
			qb.Build(" AND Attributes[?] != ?", f.Key, f.Value)
		case string(FilterOpLike):
			qb.Build(" AND Attributes[?] LIKE ?", f.Key, f.Value)
		case string(FilterOpNotLike):
			qb.Build(" AND Attributes[?] NOT LIKE ?", f.Key, f.Value)
		case string(FilterOpHasAttribute):
			qb.Build(" AND mapContains(Attributes, ?)", f.Value)
		default:
			return nil, fmt.Errorf("unsupported filter operator")
		}
	}
	return qb, nil
}
