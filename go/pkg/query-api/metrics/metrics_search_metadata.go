package metrics

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.opentelemetry.io/collector/pdata/pmetric"
)

type MetricSearchMetadataResponse struct {
	MetricName               string   `json:"name"`
	MetricType               string   `json:"type"`
	MetricDescription        string   `json:"description"`
	AttributeKeys            []string `json:"attribute_keys"`
	LastIngestedAt           int64    `json:"last_ingested_at"`
	SupportedAggregations    []string `json:"supported_aggregations"`
	SupportedFunctions       []string `json:"supported_functions"`
	DefaultGroupByAttributes []string `json:"default_group_by_attributes"`
	DefaultGroupByFunction   string   `json:"default_group_by_function"`
}

type MetricSearchMetadataRow struct {
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	AttributeKeys     []string  `ch:"AttributeKeys"`
	LastIngestedAt    time.Time `ch:"LastIngestedAt"`
}

const baseSearchMetadataTmpl string = `
SELECT
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS AttributeKeys,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
GROUP BY ProjectId, MetricName`

var (
	errMissingTargetMetricName = errors.New("target metric name missing")
	errMissingTargetMetricType = errors.New("target metric type missing")
)

func buildSearchMetadataQuery(projectID string, query *Query) (*clickhouse.QueryBuilder, error) {
	if query.TargetMetric == "" {
		return nil, errMissingTargetMetricName
	}
	if query.TargetType == "" {
		return nil, errMissingTargetMetricType
	}

	mtype := getMetricTypeFromString(query.TargetType)
	if mtype == pmetric.MetricTypeEmpty {
		return nil, errUnknownMetricType
	}

	tableName, err := getTableForMetricType(mtype)
	if err != nil {
		return nil, fmt.Errorf("getting table for metric type %s: %w", query.TargetType, err)
	}

	builder := clickhouse.NewQueryBuilder()
	builder.Build(
		fmt.Sprintf(
			baseSearchMetadataTmpl,
			constants.MetricsDatabaseName,
			tableName+"_metadata",
		),
		projectID,
		query.TargetMetric,
	)
	return builder, nil
}
