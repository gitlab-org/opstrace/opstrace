package metrics

import (
	"context"
	"fmt"
	"slices"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type dataRow struct {
	ProjectID         string    `ch:"ProjectId"`
	MetricName        string    `ch:"MetricName"`
	MetricDescription string    `ch:"MetricDescription"`
	Attributes        []string  `ch:"Attributes"`
	LastIngestedAt    time.Time `ch:"LastIngestedAt"`
}

const queryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  %s.%s
WHERE
  ProjectId = ?
  AND MetricName = ?
GROUP BY ProjectId, MetricName
`

var _ = Context("metrics metadata", Ordered, Serial, func() {
	var (
		ctx     context.Context
		cancel  context.CancelFunc
		testEnv *testutils.ClickHouseServer
		conn    driver.Conn
	)

	BeforeAll(func() {
		ctx, cancel = context.WithTimeout(context.Background(), 180*time.Second)
		DeferCleanup(cancel)

		var err error
		testEnv, conn, err = testutils.NewClickHouseServerAndConnection(ctx)
		Expect(err).ToNot(HaveOccurred())
		DeferCleanup(func(ctx SpecContext) {
			testEnv.Terminate(ctx)
		})

		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.MetricsDatabaseName)).To(Succeed())
		Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.AnalyticsDatabaseName)).To(Succeed())
	})

	DescribeTable(
		"querying metrics metadata via materialized view",
		func(mtype pmetric.MetricType) {
			// insert data into raw table, then query it back via the MV
			refTime := time.Now().UTC()
			m := common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
				"keyone": "valone",
				"keytwo": "valtwo",
			}, 10, common.RandomTraceID[:])
			err := common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, refTime)
			Expect(err).ToNot(HaveOccurred())

			tableName, err := getTableForMetricType(mtype)
			Expect(err).ToNot(HaveOccurred())
			query := fmt.Sprintf(queryTmpl, constants.MetricsDatabaseName, tableName+"_metadata")

			var rows []dataRow
			err = conn.Select(ctx, &rows, query, common.RandomProjectID, common.RandomMetricName)
			Expect(err).ToNot(HaveOccurred())
			{
				Expect(rows).To(HaveLen(1))
				Expect(rows[0].LastIngestedAt).To(Equal(refTime))

				attrKeys := make([]string, 0)
				for _, r := range rows {
					attrKeys = append(attrKeys, r.Attributes...)
				}
				Expect(attrKeys).To(HaveLen(2))
				expectedKeys := []string{"keyone", "keytwo"}
				slices.Sort(expectedKeys)
				Expect(attrKeys).To(Equal(expectedKeys))
			}
			// insert new metrics with new attributes, verify attributes again
			refTime = refTime.Add(1 * time.Minute).UTC() // move time ahead
			m = common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
				"keyone":   "valone",
				"keytwo":   "valtwo",
				"keythree": "valthree",
				"keyfour":  "valfour",
			}, 10, common.RandomTraceID[:])
			err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, refTime)
			Expect(err).ToNot(HaveOccurred())
			rows = []dataRow{}
			err = conn.Select(ctx, &rows, query, common.RandomProjectID, common.RandomMetricName)
			Expect(err).ToNot(HaveOccurred())
			{
				Expect(rows).To(HaveLen(1))
				Expect(rows[0].LastIngestedAt).To(Equal(refTime))

				attrKeys := make([]string, 0)
				for _, r := range rows {
					attrKeys = append(attrKeys, r.Attributes...)
				}
				Expect(attrKeys).To(HaveLen(4))
				expectedKeys := []string{"keyone", "keytwo", "keythree", "keyfour"}
				slices.Sort(expectedKeys)
				Expect(attrKeys).To(Equal(expectedKeys))
			}
			// insert new datapoints with some attributes removed, verify attributes again
			refTime = refTime.Add(2 * time.Minute).UTC() // move time ahead
			m = common.GenerateOTELMetrics(mtype, time.Now(), time.Now(), map[string]string{
				"keyone":   "valone",
				"keytwo":   "valtwo",
				"keythree": "valthree",
			}, 10, common.RandomTraceID[:])
			err = common.PersistOTELMetrics(ctx, conn, logger.Desugar(), m, refTime)
			Expect(err).ToNot(HaveOccurred())
			rows = []dataRow{}
			err = conn.Select(ctx, &rows, query, common.RandomProjectID, common.RandomMetricName)
			Expect(err).ToNot(HaveOccurred())
			{
				Expect(rows).To(HaveLen(1))
				Expect(rows[0].LastIngestedAt).To(Equal(refTime))

				attrKeys := make([]string, 0)
				for _, r := range rows {
					attrKeys = append(attrKeys, r.Attributes...)
				}
				Expect(attrKeys).To(HaveLen(4))
				expectedKeys := []string{"keyone", "keytwo", "keythree", "keyfour"}
				slices.Sort(expectedKeys)
				Expect(attrKeys).To(Equal(expectedKeys))
			}
		},
		Entry(
			"list sum metrics metadata",
			pmetric.MetricTypeSum,
		),
		Entry(
			"list gauge metrics metadata",
			pmetric.MetricTypeGauge,
		),
		Entry(
			"list histogram metrics metadata",
			pmetric.MetricTypeHistogram,
		),
		Entry(
			"list exponential histogram metrics metadata",
			pmetric.MetricTypeExponentialHistogram,
		),
	)
})
