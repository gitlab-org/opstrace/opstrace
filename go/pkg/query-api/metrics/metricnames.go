package metrics

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

type MetricNameRow struct {
	ProjectID      string    `ch:"ProjectId"`
	Name           string    `ch:"MetricName"`
	Description    string    `ch:"MetricDescription"`
	Attributes     []string  `ch:"Attributes"`
	LastIngestedAt time.Time `ch:"LastIngestedAt"`
}

func (q *chQuerier) getSources(
	projectID string, filters *MetricNameFilters,
) map[string]MetricNameGetFn {
	getters := make(map[string]MetricNameGetFn)
	// process all registered OTEL metric-types
	for _, t := range registeredOTELMetricTables {
		t := t // get loop variable
		getters[t.tableName] = func(ctx context.Context) ([]MetricName, error) {
			qb, err := buildQuery(t, projectID, filters)
			if err != nil {
				return nil, fmt.Errorf("building metrics list query: %w", err)
			}

			var rows []MetricNameRow
			if err := q.db.Select(
				qb.Context(ctx),
				&rows,
				qb.SQL(),
			); err != nil {
				return nil, fmt.Errorf("executing metrics list query: %w", err)
			}

			response := make([]MetricName, 0)
			for _, r := range rows {
				response = append(response, MetricName{
					Name:           r.Name,
					Description:    r.Description,
					Attributes:     r.Attributes,
					Type:           t.typeName,
					LastIngestedAt: r.LastIngestedAt.UnixNano(),
				})
			}
			return response, nil
		}
	}
	return getters
}

const baseQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes,
  maxMerge(LastIngestedAtState) AS LastIngestedAt
FROM
  %s.%s
`

func buildQuery(t metricTable, projectID string, f *MetricNameFilters) (*clickhouse.QueryBuilder, error) {
	if projectID == "" {
		return nil, fmt.Errorf("project ID cannot be empty")
	}
	builder := clickhouse.NewQueryBuilder()
	builder.Build(
		fmt.Sprintf(
			baseQueryTmpl,
			constants.MetricsDatabaseName,
			t.tableName+"_metadata",
		),
	)
	builder.Build("WHERE ProjectId = ?", projectID)
	// if no filtering is requested, we can return early
	if f == nil {
		builder.Build("GROUP BY ProjectId, MetricName")
		builder.Build("ORDER BY ProjectId, MetricName")
		return builder, nil
	}
	// search by exact metric names
	if f.ExactMatch != nil && len(f.ExactMatch) > 0 {
		builder.Build("AND (")
		for idx, s := range f.ExactMatch {
			if idx == 0 {
				builder.Build("MetricName = ?", s)
			} else {
				builder.Build("OR MetricName = ?", s)
			}
		}
		builder.Build(")")
	}
	// search when metric names start with given text
	if f.StartsWith != "" {
		builder.Build("AND MetricName LIKE ?", f.StartsWith+"%")
	}
	// search text within metric names
	if f.Search != nil && len(f.Search) > 0 {
		builder.Build("AND (")
		for idx, s := range f.Search {
			searchString := fmt.Sprintf("%%%s%%", s)
			if idx == 0 {
				builder.Build("MetricName LIKE ?", searchString)
			} else {
				builder.Build("OR MetricName LIKE ?", searchString)
			}
		}
		builder.Build(")")
	}
	// process grouping
	builder.Build("GROUP BY ProjectId, MetricName")
	// search by attributes
	if f.Attributes != nil && len(f.Attributes) > 0 {
		builder.Build(
			"HAVING hasAll(Attributes, ?)",
			clickhouse.Array[string](f.Attributes),
		)
	}
	// process order
	builder.Build("ORDER BY ProjectId, MetricName")
	// process limit
	if f.Limit > 0 {
		builder.Build("LIMIT ?", f.Limit)
	}
	return builder, nil
}

type MetricAttributesRow struct {
	ProjectID  string   `ch:"ProjectId"`
	Name       string   `ch:"MetricName"`
	Attributes []string `ch:"Attributes"`
}

func (q *chQuerier) getAttributesSources(projectID string) map[string]MetricAttributesGetFn {
	getters := make(map[string]MetricAttributesGetFn)
	// process all registered OTEL metric-types
	for _, t := range registeredOTELMetricTables {
		t := t // get loop variable
		getters[t.tableName] = func(ctx context.Context) (map[string]interface{}, error) {
			qb := buildAttributesQuery(t, projectID)

			var rows []MetricAttributesRow
			if err := q.db.Select(
				qb.Context(ctx),
				&rows,
				qb.SQL(),
			); err != nil {
				return nil, fmt.Errorf("executing metrics attributes query: %w", err)
			}

			response := make(map[string]interface{})
			if len(rows) > 0 {
				for _, r := range rows {
					for _, attr := range r.Attributes {
						response[attr] = struct{}{}
					}
				}
			}
			return response, nil
		}
	}
	return getters
}

const baseAttributesQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  arraySort(groupUniqArrayArrayMerge(AttributeKeysState)) AS Attributes
FROM
  %s.%s
WHERE ProjectId = ? GROUP BY ProjectId, MetricName
`

func buildAttributesQuery(t metricTable, projectID string) *clickhouse.QueryBuilder {
	builder := clickhouse.NewQueryBuilder()
	builder.Build(
		fmt.Sprintf(
			baseAttributesQueryTmpl,
			constants.MetricsDatabaseName,
			t.tableName+"_metadata",
		),
		projectID,
	)
	return builder
}

type MetricCorrelationMetadataRow struct {
	ProjectID  string `ch:"ProjectId"`
	MetricName string `ch:"MetricName"`
	TraceIDOccurence
}

type TraceIDOccurence struct {
	ExemplarTraceID         string    `ch:"ExemplarTraceId"`
	LatestDatapointTimeUnix time.Time `ch:"LatestDatapointTimeUnix"`
}

func (q *chQuerier) getCorrelationMetadataSources(
	pid string, filters *MetricNameFilters,
) map[string]MetricCorrelationMetadataGetFn {
	getters := make(map[string]MetricCorrelationMetadataGetFn)
	// process all registered OTEL metric-types
	for _, t := range registeredOTELMetricTables {
		t := t // get loop variable
		getters[t.typeName] = func(ctx context.Context) (map[string]time.Time, error) {
			qb := buildMetricsCorrelationMetadataQuery(t, pid, filters)

			var rows []MetricCorrelationMetadataRow
			if err := q.db.Select(
				qb.Context(ctx),
				&rows,
				qb.SQL(),
			); err != nil {
				return nil, fmt.Errorf("executing metrics attributes query: %w", err)
			}

			response := make(map[string]time.Time)
			if len(rows) > 0 {
				for _, r := range rows {
					response[r.MetricName] = r.LatestDatapointTimeUnix
				}
			}
			return response, nil
		}
	}
	return getters
}

const baseMetricsCorrelationMetadataQueryTmpl = `
SELECT
  ProjectId,
  MetricName,
  UUIDNumToString(ExemplarTraceId) AS ExemplarTraceId,
  max(LatestDatapointTimeUnix) AS LatestDatapointTimeUnix
FROM
  {metricsDB:Identifier}.{correlationMetadataTable:Identifier}
WHERE ProjectId = ? AND ExemplarTraceId = ?
GROUP BY ProjectId, MetricName, ExemplarTraceId
ORDER BY ProjectId, MetricName, ExemplarTraceId;
`

func buildMetricsCorrelationMetadataQuery(
	t metricTable, projectID string, filters *MetricNameFilters,
) *clickhouse.QueryBuilder {
	builder := clickhouse.NewQueryBuilder().WithParams(
		map[string]clickhouse.Literal{
			"metricsDB":                clickhouse.String(constants.MetricsDatabaseName),
			"correlationMetadataTable": clickhouse.String(t.tableName + "_correlation_metadata"),
		},
	)
	builder.Build(baseMetricsCorrelationMetadataQueryTmpl, projectID, filters.TraceID)
	return builder
}
