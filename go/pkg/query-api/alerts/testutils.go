package alerts

import (
	"context"
	"time"
)

type MockQuerier struct{}

var _ Querier = (*MockQuerier)(nil)

var TS = time.Unix(1701264345, 0).UTC()

func (*MockQuerier) GetAlerts(_ context.Context, _ *QueryFilters) (interface{}, error) {
	return []Result{
		{
			TenantID:     "1",
			ProjectID:    "1",
			AggTimestamp: TS,
			AlertType:    "TraceRateLimitEvent",
			Description:  "rate limit exceeded on path: /v1/traces with method: POST",
			Reason:       "value of 1000 goes over the limit set to 100",
			Count:        4,
		},
	}, nil
}
