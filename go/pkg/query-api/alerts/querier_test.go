package alerts_test

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	alertsquery "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/alerts"
)

const (
	alertTenantID   = "1"
	alertProjectID1 = "1"
	alertProjectID2 = "2"
)

func generateAlerts(writer alerts.AlertWriter, tenantID string, projectID string) error {
	var (
		err     error
		y, m, d = time.Now().Date()
	)
	for i := 0; i < 10; i += 1 {
		a := alerts.AlertEvent{
			ProjectID:   projectID,
			TenantID:    tenantID,
			Description: "desc",
			Timestamp:   time.Date(y, m, d, 1, 1, i, 0, time.UTC),
			Reason:      "reason",
			Type:        alerts.TraceRateLimitEvent,
			IsSaaSEvent: 1,
		}

		err = writer.WriteAlertEvent(context.Background(), &a)
		if err != nil {
			break
		}
	}
	if err != nil {
		return err
	}
	return nil
}

var _ = Describe("AlertQuerier", func() {

	var (
		err error
	)
	Context("By querying", func() {
		It("should return alerts", func() {
			err = generateAlerts(writer, alertTenantID, alertProjectID1)
			Expect(err).NotTo(HaveOccurred())
			err = generateAlerts(writer, alertTenantID, alertProjectID2)
			Expect(err).NotTo(HaveOccurred())

			results, err := querier.GetAlerts(context.Background(), &alertsquery.QueryFilters{
				TenantID:   alertTenantID,
				IsSaaS:     true,
				PeriodFrom: time.Now().Add(-1 * 48 * time.Hour),
			})
			Expect(err).NotTo(HaveOccurred())

			GinkgoWriter.Println(results)
			Expect(results).To(HaveLen(2))
		})
	})
})
