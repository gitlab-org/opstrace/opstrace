package alerts

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"go.uber.org/zap"

	internalclickhouse "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

type QueryFilters struct {
	TenantID   string
	PeriodFrom time.Time
	IsSaaS     bool
}

type Querier interface {
	GetAlerts(
		ctx context.Context,
		filters *QueryFilters,
	) (interface{}, error)
}

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		err error
	)
	if clickHouseCloudDSN != "" {
		db, err = internalclickhouse.GetDatabaseConnection(clickHouseCloudDSN, opts, logger, 30*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting cloud database handle: %w", err)
		}
	} else {
		db, err = internalclickhouse.GetDatabaseConnection(clickHouseDSN, opts, logger, 3*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting database handle: %w", err)
		}
	}
	return &chQuerier{
		db:     db,
		logger: logger,
	}, nil
}

type chQuerier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

var _ Querier = (*chQuerier)(nil)

type Result struct {
	TenantID     string    `json:"tenant_id" ch:"TenantId"`
	ProjectID    string    `json:"project_id" ch:"ProjectId"`
	AggTimestamp time.Time `json:"agg_timestamp" ch:"AggTimestamp"`
	AlertType    string    `json:"alert_type" ch:"AlertType"`
	Description  string    `json:"description" ch:"Description"`
	Reason       string    `json:"reason" ch:"Reason"`
	Count        uint64    `json:"count" ch:"Count"`
}

func (q *chQuerier) GetAlerts(ctx context.Context, filters *QueryFilters) (interface{}, error) {
	var (
		result []Result
	)
	query := alertQuery(filters)

	err := q.db.Select(query.Context(ctx), &result, query.SQL())
	if err != nil {
		return nil, fmt.Errorf("error getting alerts from clickhouse: %w", err)
	}
	return result, nil
}

const baseAlertQuery = `
SELECT
  TenantId, ProjectId, AggTimestamp,
  AlertType, argMinMerge(Description) AS Description, argMinMerge(Reason) AS Reason, sum(EventCount) AS Count
FROM {alertDB:Identifier}.{alertsTable:Identifier}
`

func alertQuery(filters *QueryFilters) *internalclickhouse.QueryBuilder {
	qb := internalclickhouse.NewQueryBuilder().WithParams(
		map[string]internalclickhouse.Literal{
			"alertDB":     internalclickhouse.String(constants.AlertsDatabaseName),
			"alertsTable": internalclickhouse.String(constants.AlertsTableName),
		})

	qb.Build(baseAlertQuery)

	addWhereClause(filters, qb)

	qb = qb.Build(`
GROUP BY TenantId, ProjectId, AlertType, AggTimestamp;`)

	return qb
}

func addWhereClause(filters *QueryFilters, qb *internalclickhouse.QueryBuilder) {
	if filters == nil {
		return
	}

	isSaaS := 0
	if filters.IsSaaS {
		isSaaS = 1
	}

	qb.Build(`WHERE`)
	qb.Build(` IsSaaS = {isSaaS:UInt8}`)
	qb.WithParam("isSaaS", internalclickhouse.String(strconv.Itoa(isSaaS)))

	if filters.TenantID != "" {
		qb.Build(` AND TenantId = {tenantId:String}`)
		qb.WithParam("tenantId", internalclickhouse.String(filters.TenantID))
	}

	qb.Build(` AND AggTimestamp > {windowStart:DateTime64(9, 'UTC')}`)

	if !filters.PeriodFrom.IsZero() {
		qb.WithParam("windowStart", internalclickhouse.TimestampNano(filters.PeriodFrom.UnixNano()))
	} else {
		// if no period is provided, use last 24 hours as default
		qb.WithParam("windowStart", internalclickhouse.TimestampNano(time.Now().Add(-24*time.Hour).UnixNano()))
	}
}
