package logs

import (
	"context"
	"errors"
	"fmt"
	"maps"
	"slices"
	"strings"
	"time"

	"go.opentelemetry.io/collector/pdata/plog"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"

	ch "github.com/ClickHouse/clickhouse-go/v2"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	qcommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
)

const baseLogsSearchTmpl string = `
SELECT
  Timestamp,
  UUIDNumToString(TraceId) AS TraceIdBody,
  hex(SpanId) AS SpanIdBody,
  TraceFlags,
  SeverityText,
  SeverityNumber,
  ServiceName,
  Body,
  ResourceAttributes,
  LogAttributes,
  Fingerprint
FROM ?.?
WHERE
  ProjectId = ?
`

const baseLogsMetadataTmpl string = `
SELECT
    %s,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    %s as SumCount
FROM ?.?
WHERE
  ProjectId = ?
`

type chQuerier struct {
	db     clickhouse.Conn
	logger *zap.Logger

	queryTimeout float32
}

// ascertain interface implementation
var _ Querier = (*chQuerier)(nil)

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	queryTimeout float32,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		err error
	)
	if opts == nil {
		opts = &clickhouse.Options{}
	}
	if opts.Settings == nil {
		opts.Settings = make(ch.Settings)
	}
	maps.Copy(opts.Settings, map[string]any{
		"max_execution_time":                      queryTimeout,
		"timeout_before_checking_execution_speed": 0,
		"timeout_overflow_mode":                   "throw",
	})
	if clickHouseCloudDSN != "" {
		db, err = clickhouse.GetDatabaseConnection(clickHouseCloudDSN, opts, logger, 30*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting cloud database handle: %w", err)
		}
	} else {
		db, err = clickhouse.GetDatabaseConnection(clickHouseDSN, opts, logger, 3*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting database handle: %w", err)
		}
	}

	return &chQuerier{
		db:     db,
		logger: logger,

		queryTimeout: queryTimeout,
	}, nil
}

func (chq *chQuerier) GetLogs(ctx context.Context, qc *QueryContext) (*LogsResponse, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.GetLogs", trace.SpanKindClient)
	defer span.End()

	if qc == nil {
		return nil, nil // nothing to do
	}

	if qc.ProjectID == "" {
		return nil, qcommon.ErrUnknownProjectID
	}

	chq.sanitizeQueryTimeParameters(qc)

	if len(qc.Queries) > 1 {
		// NOTE(prozlach): we return the result of the first query that
		// iteration loop starts with. In case there are more than one query
		// and we do not adjust this loop, the bug may not be imediatelly
		// obvious as the iteration order for maps is randomized AFAIK.<F3>
		panic("multiple queries are not supported yet")
	}

	var pageToken *qcommon.LogsPage
	var err error
	if qc.PageToken != "" {
		pageToken, err = qcommon.DecodePage[qcommon.LogsPage](qc.PageToken)
		if err != nil {
			return nil, fmt.Errorf("page token decoding: %w", err)
		}
	}

	for alias, query := range qc.Queries {
		chq.logger.Debug(
			"processing query",
			zap.String("query alias", alias),
			zap.Any("query", query),
		)

		qb, err := chq.buildQuery(
			qc.ProjectID,
			qc.StartTimestamp, qc.EndTimestamp,
			qc.PageSize, pageToken,
			qc.SortByTimeAsc,
			query,
		)
		if err != nil {
			return nil, fmt.Errorf("building query: %w", err)
		}

		response, err := chq.executeQuery(
			ctx,
			qc.StartTimestamp, qc.EndTimestamp,
			qc.PageSize,
			qb,
		)
		if err != nil {
			return nil, fmt.Errorf("executing query: %w", err)
		}

		return response, nil
	}

	return nil, nil
}

func (chq *chQuerier) buildPageTokenConditions(
	pageToken *qcommon.LogsPage,
	builder *clickhouse.QueryBuilder,
	sortByTimeAsc bool,
	startTimestamp, endTimestamp time.Time,
) error {
	lastSeenTime, err := time.Parse(time.RFC3339Nano, pageToken.LastSeenTimestamp)
	if err != nil {
		return fmt.Errorf("unable to parse lastSeenTime %q: %w", lastSeenTime, err)
	}

	if sortByTimeAsc && endTimestamp.Compare(lastSeenTime) < 0 {
		// Time sorted in ascending order, requested time window is before
		// the last seen timestamp
		return fmt.Errorf("lastSeenTime %s is after endTimestamp %s", lastSeenTime, endTimestamp)
	}
	if !sortByTimeAsc && startTimestamp.Compare(lastSeenTime) > 0 {
		// Time sorted in descending order, requested time window is after
		// the last seen timestamp
		return fmt.Errorf("lastSeenTime %s is before startTimestamp %s", lastSeenTime, endTimestamp)
	}
	if startTimestamp.Equal(endTimestamp) && !startTimestamp.Equal(lastSeenTime) {
		// Specific time was requested, but the page cookie points different
		// time
		return fmt.Errorf(
			"lastSeenTime %s is not equal to request time of the signal(s) %s",
			lastSeenTime, startTimestamp,
		)
	}

	// NOTE(prozlach): We need to handle the use case when a group of log
	// entries has the same timestamp, thus making pagination based on
	// timestamp-only impossible. Hence we compare other fields in this case
	// as well and rely on them being sorted in ASC order.
	switch {
	case startTimestamp.Equal(endTimestamp):
		builder.Build(
			"AND Timestamp == ? AND ServiceName >= ? AND TraceIdBody >= ? AND SpanIdBody >= ? AND Body > ?",
			lastSeenTime,
			pageToken.LastSeenServiceName,
			pageToken.LastSeenTraceID,
			pageToken.LastSeenSpanID,
			pageToken.LastSeenBodyPrefix,
		)
	case sortByTimeAsc:
		builder.Build(
			"AND (Timestamp > ? or (Timestamp == ? AND ServiceName >= ? AND TraceIdBody >= ? AND SpanIdBody >= ? AND Body > ?))",
			lastSeenTime,
			lastSeenTime,
			pageToken.LastSeenServiceName,
			pageToken.LastSeenTraceID,
			pageToken.LastSeenSpanID,
			pageToken.LastSeenBodyPrefix,
		)
		builder.Build("AND Timestamp < ?", endTimestamp)
	case !sortByTimeAsc:
		builder.Build(
			"AND (Timestamp < ? OR (Timestamp == ? AND ServiceName >= ? AND TraceIdBody >= ? AND SpanIdBody >= ? AND Body > ?))",
			lastSeenTime,
			lastSeenTime,
			pageToken.LastSeenServiceName,
			pageToken.LastSeenTraceID,
			pageToken.LastSeenSpanID,
			pageToken.LastSeenBodyPrefix,
		)
		builder.Build("AND Timestamp > ?", startTimestamp)
	}

	return nil
}

func (chq *chQuerier) addWhereFilters(builder *clickhouse.QueryBuilder, q *Query) {
	if q.ServiceNames != nil {
		builder.Build(
			"AND ( ServiceName IN ? )",
			clickhouse.Array[string](q.ServiceNames),
		)
	}

	if q.NotServiceNames != nil {
		builder.Build(
			"AND ( ServiceName NOT IN ? )",
			clickhouse.Array[string](q.NotServiceNames),
		)
	}

	if q.SeverityNumbers != nil {
		builder.Build(
			"AND ( SeverityNumber IN ? )",
			clickhouse.Array[int32](q.SeverityNumbers),
		)
	}

	if q.NotSeverityNumbers != nil {
		builder.Build(
			"AND ( SeverityNumber NOT IN ? )",
			clickhouse.Array[int32](q.NotSeverityNumbers),
		)
	}

	if q.TraceIDs != nil {
		appliedFunc, transformed := clickhouse.FuncMapToString("UUIDStringToNum", q.TraceIDs)
		builder.Build("AND ( TraceId IN "+appliedFunc+" )", transformed...)
	}

	if q.SpanIDs != nil {
		appliedFunc, transformed := clickhouse.FuncMapToString("unhex", q.SpanIDs)
		builder.Build("AND ( SpanId IN "+appliedFunc+" )", transformed...)
	}

	if q.Fingerprints != nil {
		builder.Build(
			"AND ( Fingerprint IN ? )",
			clickhouse.Array[string](q.Fingerprints),
		)
	}

	if q.TraceFlags != nil {
		builder.Build(
			"AND ( TraceFlags IN ? )",
			clickhouse.Array[uint32](q.TraceFlags),
		)
	}

	if q.NotTraceFlags != nil {
		builder.Build(
			"AND ( TraceFlags NOT IN ? )",
			clickhouse.Array[uint32](q.NotTraceFlags),
		)
	}

	if q.Body != nil {
		builder.Build("AND (")
		wildcards := make([]any, len(q.Body))
		queryStrings := make([]string, len(q.Body))
		for i, b := range q.Body {
			wildcards[i] = "%" + b + "%"
			queryStrings[i] = "(Body ILIKE ?)"
		}
		builder.Build(
			strings.Join(queryStrings, " OR "),
			wildcards...,
		)
		builder.Build(")")
	}

	for i, k := range q.LogAttributeNames {
		v := q.LogAttributeValues[i]
		builder.Build("AND ( LogAttributes[?] = ?)", k, v)
	}

	for i, k := range q.ResourceAttributeNames {
		v := q.ResourceAttributeValues[i]
		builder.Build("AND ( ResourceAttributes[?] = ?)", k, v)
	}
}

func (chq *chQuerier) buildQuery(
	projectID string,
	startTimestamp, endTimestamp time.Time,
	pageSize int, pageToken *qcommon.LogsPage,
	sortByTimeAsc bool,
	q *Query,
) (*clickhouse.QueryBuilder, error) {
	builder := clickhouse.NewQueryBuilder()
	builder.Build(
		baseLogsSearchTmpl,
		clickhouse.Identifier(constants.LoggingDatabaseName),
		clickhouse.Identifier(constants.LoggingTableName),
		projectID,
	)

	if len(q.LogAttributeNames) != len(q.LogAttributeValues) {
		return nil, errors.New("log attributes query is malformed, unequal number of keys and values")
	}

	if len(q.ResourceAttributeNames) != len(q.ResourceAttributeValues) {
		return nil, errors.New("resource attributes query is malformed, unequal number of keys and values")
	}

	chq.addWhereFilters(builder, q)

	if pageToken != nil {
		err := chq.buildPageTokenConditions(
			pageToken, builder, sortByTimeAsc, startTimestamp, endTimestamp,
		)
		if err != nil {
			return nil, fmt.Errorf("parsing pageToken: %w", err)
		}
	} else {
		switch {
		case startTimestamp.Equal(endTimestamp):
			builder.Build("AND Timestamp == ?", startTimestamp)
		case sortByTimeAsc:
			builder.Build("AND Timestamp >= ? AND Timestamp < ?",
				startTimestamp, endTimestamp,
			)
		case !sortByTimeAsc:
			// sort by desc order:
			builder.Build("AND Timestamp > ? AND Timestamp <= ?",
				startTimestamp, endTimestamp,
			)
		}
	}

	// NOTE(prozlach): The MergeTree table engine family organizes data in
	// 'parts' and runs queries in parallel across them even on single nodes.
	// By default it returns the results from each part as they emerge from
	// query processing. Data within the parts will typically return in the
	// sort order from the table but the results from each part can return in
	// any order.
	// This is especially important when we have log entries that have the same
	// timestamp. In this case, we need to be able to rely on ordering of
	// remaining collumns, in order to properly implement pagination.
	switch {
	case startTimestamp.Equal(endTimestamp):
		builder.Build("ORDER BY ServiceName ASC, TraceId ASC, SpanId ASC, Body ASC")
	case sortByTimeAsc:
		builder.Build("ORDER BY Timestamp ASC, ServiceName ASC, TraceId ASC, SpanId ASC, Body ASC")
	case !sortByTimeAsc:
		builder.Build("ORDER BY Timestamp DESC, ServiceName ASC, TraceIdBody ASC, SpanIdBody ASC, Body ASC")
	}

	builder.Build(fmt.Sprintf("LIMIT %d", pageSize))

	return builder, nil
}

func (chq *chQuerier) executeQuery(
	ctx context.Context,
	startTimestamp, endTimestamp time.Time,
	pageSize int,
	qb *clickhouse.QueryBuilder,
) (*LogsResponse, error) {
	sql := qb.SQL()
	chq.logger.Debug(
		"executing query",
		zap.Any("query string", sql),
		zap.Any("query args", qb.Params),
	)

	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.CHQuery", trace.SpanKindClient)
	defer span.End()

	res := &LogsResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),

		Results: make([]LogsRow, 0),
	}
	err := chq.db.Select(
		qb.Context(
			ctx,
			ch.WithSpan(span.SpanContext()),
		),
		&res.Results, sql,
	)
	if err != nil {
		return nil, fmt.Errorf("executing logs query: %w", err)
	}

	if len(res.Results) < pageSize {
		// There are no more pages, return empty page token:
		return res, nil
	}

	lastRow := res.Results[len(res.Results)-1]

	var bodyTrimmed string
	if len(lastRow.Body) > qcommon.LogBodyMaxPrefixSize {
		bodyTrimmed = lastRow.Body[:qcommon.LogBodyMaxPrefixSize]
	} else {
		bodyTrimmed = lastRow.Body
	}

	page := &qcommon.LogsPage{
		LastSeenTimestamp:   lastRow.Timestamp.Format(time.RFC3339Nano),
		LastSeenTraceID:     lastRow.TraceID,
		LastSeenSpanID:      lastRow.SpanID,
		LastSeenServiceName: lastRow.ServiceName,
		LastSeenBodyPrefix:  bodyTrimmed,
	}
	token, err := qcommon.EncodePage[qcommon.LogsPage](page)
	if err != nil {
		return nil, fmt.Errorf("encoding next token page: %w", err)
	}
	res.NextPageToken = token
	return res, nil
}

//nolint:funlen,cyclop // business logic, half of the function are code comments.
func (chq *chQuerier) buildMetadataQuery(
	startTimestamp time.Time, projectID string, timeSpan time.Duration, q *Query,
) (*clickhouse.QueryBuilder, time.Duration, error) {
	if len(q.LogAttributeNames) != len(q.LogAttributeValues) {
		return nil, 0, errors.New("log attributes query is malformed, unequal number of keys and values")
	}

	if len(q.ResourceAttributeNames) != len(q.ResourceAttributeValues) {
		return nil, 0, errors.New("resource attributes query is malformed, unequal number of keys and values")
	}

	var sourceTable string
	var timeFunction string
	var summingFunction string
	var interval time.Duration

	// The idea is to split queries into low-cardinality and high-cardinality
	// queries which get served from materialized view and source table
	// respectively. Analytics scans always scan whole time range (no
	// pagination/LIMIT clause), and low cardinality queries result in lots of
	// granules full-scanned whereas high-cardinality queries with few.
	//
	// There are currently three MVs: "25920s", "259200ms", "2592ms". Time
	// ranges are calculated basing on few simple facts:
	// * we do not store more than 30 days of data
	// * UI chart for logs analytics is able to handle between 10 and 100
	//   data points
	//
	// Low cardinality queries are queries that either do not have any filters
	// or filter with:
	//   * ServiceName
	//   * TraceFlags
	//   * SererityNumbers
	//   * time ranges greater than 25.92 seconds (chosen arbitrary)
	// High-cardinality queries are those that filter by:
	//   * TraceID
	//   * SpanIDs
	//   * Body
	//   * Log and Resource attributes
	//
	// NOTE(prozlach): There are corner-cases where supposedly high-cardinality
	// query results in full-scan of the main table anyway (e.g. query for a
	// very common `Body` term). We handle these cases by setting query timeout
	// and returning a predefined error (HTTP status 504) to UI which in turn
	// notifies the user to change the query.
	//
	// NOTE(prozlach): In case of low-cardinality queries we are grouping data
	// from MV for some timeRange lengths in order to not to create too many
	// MVs. The idea is that the scale of grouping is small enough to not to
	// cause full scan of the MV table on one hand and still keep the number of
	// data points for the ui between 10 and 100.
	lowCardinalityQuery := q.TraceIDs == nil &&
		q.SpanIDs == nil &&
		q.Fingerprints == nil &&
		q.Body == nil &&
		q.LogAttributeNames == nil &&
		q.ResourceAttributeNames == nil &&
		timeSpan > 25920*time.Millisecond
	if lowCardinalityQuery {
		summingFunction = "sumMerge(SumCount)"
		switch {
		case timeSpan > 72*time.Hour:
			sourceTable = constants.LoggingTableQAnaliticsTableName + "_25920s"
			//nolint:goconst
			timeFunction = "Timestamp" // Interval is 7.2h
			interval = 432 * time.Minute
		case timeSpan > 432*time.Minute: // 7.2h
			sourceTable = constants.LoggingTableQAnaliticsTableName + "_259200ms"
			timeFunction = "toStartOfInterval(Timestamp, INTERVAL 2592 second) AS Timestamp" // Interval is 43.2 minutes/0.72h
			interval = 2592 * time.Second
		case timeSpan > 2592*time.Second: // 0.72h/43.2m
			sourceTable = constants.LoggingTableQAnaliticsTableName + "_259200ms"
			timeFunction = "Timestamp" // Interval is 4.32 minutes/0.072h
			interval = 259200 * time.Millisecond
		case timeSpan > 259200*time.Millisecond: // 0.072h/4.32m
			sourceTable = constants.LoggingTableQAnaliticsTableName + "_2592ms"
			timeFunction = "toStartOfInterval(Timestamp, INTERVAL 25920 millisecond) AS Timestamp"
			interval = 25920 * time.Millisecond
		case timeSpan > 25920*time.Millisecond: // 25.92s
			sourceTable = constants.LoggingTableQAnaliticsTableName + "_2592ms"
			timeFunction = "Timestamp"
			interval = 2592 * time.Millisecond
		}
	} else {
		sourceTable = constants.LoggingTableName
		summingFunction = "count(*)"
		// NOTE(prozlach): 2592 nanoseconds is the lowest interval that divides
		// 30 days completely, which we can express. Anything lower than that
		// and we return less than 10 datapoints to the UI.
		for timeThreshold := 72 * time.Hour; timeThreshold >= 25920*time.Nanosecond; timeThreshold /= 10 {
			if timeSpan > timeThreshold || timeThreshold == 25920*time.Nanosecond {
				interval = time.Duration(timeThreshold.Nanoseconds() / 10)
				timeFunction = fmt.Sprintf(
					"toStartOfInterval(Timestamp, INTERVAL %d nanoseconds) AS Timestamp",
					timeThreshold.Nanoseconds()/10,
				)
				break
			}
		}
	}
	queryTmpl := fmt.Sprintf(baseLogsMetadataTmpl, timeFunction, summingFunction)

	builder := clickhouse.NewQueryBuilder()
	builder.Build(
		queryTmpl,
		clickhouse.Identifier(constants.LoggingDatabaseName),
		clickhouse.Identifier(sourceTable),
		projectID,
	)

	chq.addWhereFilters(builder, q)

	if timeSpan == constants.LoggingSmallestAggregationInterval {
		// Special case - in order to get metadata for a sinle datapoint, we
		// need to round it to the start of smallest possible interval first
		builder.Build("AND Timestamp = toStartOfInterval(?, INTERVAL 2592 nanoseconds)",
			startTimestamp,
		)
	} else {
		builder.Build("AND Timestamp >= ? AND Timestamp < ?",
			startTimestamp, startTimestamp.Add(timeSpan),
		)
	}
	builder.Build(`
GROUP BY Timestamp, SeverityNumber`)
	builder.Build(`
ORDER BY Timestamp ASC`)

	return builder, interval, nil
}

func (chq *chQuerier) executeMetadataQuery(
	ctx context.Context,
	qb *clickhouse.QueryBuilder,
) ([]LogsRawMetadataRawResponse, error) {
	sql := qb.SQL()
	chq.logger.Debug(
		"executing metadata query",
		zap.Any("query string", sql),
		zap.Any("query args", qb.Params),
	)

	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.CHMetadataQuery", trace.SpanKindClient)
	defer span.End()

	rawRes := make([]LogsRawMetadataRawResponse, 0)

	err := chq.db.Select(
		qb.Context(
			ctx,
			ch.WithSpan(span.SpanContext()),
		),
		&rawRes, sql,
	)
	if err != nil {
		return nil, fmt.Errorf("executing logs metadata query: %w", err)
	}

	return rawRes, nil
}

func (chq *chQuerier) preCreateSeverityNumbersCounts(
	startTimestamp time.Time, timeSpan time.Duration, interval time.Duration,
) []SeverityNumbersCount {
	rangeStart := common.TruncateTimeUsingCHLogic(startTimestamp, interval)
	if rangeStart.Before(startTimestamp) {
		rangeStart = rangeStart.Add(interval)
	}

	// NOTE(prozlach): depending whether timeSpan is a multiple of intervals or
	// not, we will have timeSpan/interval or timeSpan/interval-1 elements (due
	// to <= inequality in the WHERE condition). To keep things simple, we
	// pre-allocate larger slice and simply append.
	intervalsMaxCount := timeSpan / interval
	res := make([]SeverityNumbersCount, 0, intervalsMaxCount)
	for t := rangeStart; t.Before(startTimestamp.Add(timeSpan)); t = t.Add(interval) {
		res = append(res, SeverityNumbersCount{
			Time:   t.UnixNano(),
			Counts: make(map[uint8]uint64),
		})
	}

	return res
}

func (chq *chQuerier) processRawResponse(
	startTimestamp, endTimestamp time.Time,
	rawResponse []LogsRawMetadataRawResponse,
	emptySeverityNumbersCounts []SeverityNumbersCount,
) (*LogsMetadataResponse, error) {
	res := &LogsMetadataResponse{
		StartTimestamp: startTimestamp.UnixNano(),
		EndTimestamp:   endTimestamp.UnixNano(),

		SeverityNumbersCounts: emptySeverityNumbersCounts,
	}

	serviceNamesSet := make(map[string]struct{})
	traceFlagsSet := make(map[uint32]struct{})
	severityNumbersSet := make(map[uint8]struct{})

	severityNumbersIdx := 0
	for _, row := range rawResponse {
		for _, sn := range row.ServiceNames {
			serviceNamesSet[sn] = struct{}{}
		}
		for _, tf := range row.TraceFlags {
			traceFlagsSet[tf] = struct{}{}
		}
		severityNumbersSet[row.SeverityNumber] = struct{}{}

		rowTimeStr := row.Timestamp.UnixNano()
		// NOTE(prozlach): Both arrays are sorted by time ascending. The goal
		// here is to advance severityNumbersIdx forward each time there is a
		// gap in data returned from CH.
		//nolint:lll // Is there a sane way to break this line?
		for ; res.SeverityNumbersCounts[severityNumbersIdx].Time != rowTimeStr && severityNumbersIdx < len(res.SeverityNumbersCounts); severityNumbersIdx++ {
			if severityNumbersIdx >= len(res.SeverityNumbersCounts) {
				return nil, fmt.Errorf(
					"timestamp mismatch between pre-generated series "+
						"and results we got from CH. rowTimeStr: %s, startTIme: %s, endTime: %s",
					row.Timestamp.Format(time.RFC3339Nano),
					startTimestamp.Format(time.RFC3339Nano), endTimestamp.Format(time.RFC3339Nano),
				)
			}
		}
		res.SeverityNumbersCounts[severityNumbersIdx].Counts[row.SeverityNumber] += row.SumCount
	}

	// Fill in the time points with zeros where applicable:
	for i := range res.SeverityNumbersCounts {
		for j := range severityNumbersSet {
			if _, ok := res.SeverityNumbersCounts[i].Counts[j]; !ok {
				res.SeverityNumbersCounts[i].Counts[j] = 0
			}
		}
	}

	res.Summary = LogsSummary{
		ServiceNames:    make([]string, 0, len(serviceNamesSet)),
		TraceFlags:      make([]uint32, 0, len(traceFlagsSet)),
		SeverityNumbers: make(JSONableSlice, 0, len(severityNumbersSet)),
		SeverityNames:   make([]string, len(severityNumbersSet)),
	}

	for k := range serviceNamesSet {
		res.Summary.ServiceNames = append(res.Summary.ServiceNames, k)
	}
	slices.Sort(res.Summary.ServiceNames)

	for k := range traceFlagsSet {
		res.Summary.TraceFlags = append(res.Summary.TraceFlags, k)
	}
	slices.Sort(res.Summary.TraceFlags)

	for k := range severityNumbersSet {
		res.Summary.SeverityNumbers = append(res.Summary.SeverityNumbers, k)
	}
	slices.Sort(res.Summary.SeverityNumbers)

	// Convenience mapping:
	for i, v := range res.Summary.SeverityNumbers {
		res.Summary.SeverityNames[i] = strings.ToLower(plog.SeverityNumber(v).String())
	}

	return res, nil
}

// sanitizeQueryTimeParameters makes sure that we do not query beyond the data
// TTL limits that we store in CH as this could result in weird results and
// edge cases. The backed in such a case silently narrows down the query
// period.
func (chq *chQuerier) sanitizeQueryTimeParameters(qc *QueryContext) {
	latestPermittedTimestamp := qc.ReferenceTime
	earliestPermitedTimestamp := latestPermittedTimestamp.Add(-constants.LoggingDataTTLDays * 24 * time.Hour)

	if qc.StartTimestamp.Before(earliestPermitedTimestamp) {
		qc.StartTimestamp = earliestPermitedTimestamp
	}

	if qc.EndTimestamp.After(latestPermittedTimestamp) {
		qc.EndTimestamp = latestPermittedTimestamp
	}
}

func (chq *chQuerier) GetLogsMetadata(ctx context.Context, qc *QueryContext) (*LogsMetadataResponse, error) {
	var span trace.Span
	ctx, span = instrumentation.NewSubSpan(ctx, "chQuerier.GetLogsMetadata", trace.SpanKindClient)
	defer span.End()

	if qc == nil {
		return nil, nil // nothing to do
	}

	if qc.ProjectID == "" {
		return nil, qcommon.ErrUnknownProjectID
	}

	if len(qc.Queries) > 1 {
		// NOTE(prozlach): we return the result of the first query that
		// iteration loop starts with. In case there are more than one query
		// and we do not adjust this loop, the bug may not be mediately
		// obvious as the iteration order for maps is randomized AFAIK.<F3>
		panic("multiple queries are not supported yet")
	}

	chq.sanitizeQueryTimeParameters(qc)

	timeSpan := qc.EndTimestamp.Sub(qc.StartTimestamp)
	if timeSpan < constants.LoggingSmallestAggregationInterval {
		// NOTE(prozlach): For queries with very short time span (i.e.
		// <2592ns), or point-in-time queries (i.e. startTs==endTs), there
		// are some edge-cases that require special handling in order to
		// still return meaningfull results. In order to simplify the code,
		// we cheat a little, but adjusting the time window so that the
		// requested time range/time point is always contained in the
		// smallest interval we can return (i.e. 2592ns).
		qc.StartTimestamp = common.TruncateTimeUsingCHLogic(
			qc.StartTimestamp, constants.LoggingSmallestAggregationInterval,
		)
		// NOTE(prozlach): In case when we have a very short time span that
		// overlaps two intervals, we expand the time window by two
		// intervals instead of just one.
		if qc.EndTimestamp.After(qc.StartTimestamp.Add(constants.LoggingSmallestAggregationInterval)) {
			timeSpan = constants.LoggingSmallestAggregationInterval * 2
		} else {
			timeSpan = constants.LoggingSmallestAggregationInterval
		}
		qc.EndTimestamp = qc.StartTimestamp.Add(timeSpan)
	}

	for alias, query := range qc.Queries {
		chq.logger.Debug(
			"processing query",
			zap.String("query alias", alias),
			zap.Any("query", query),
		)

		// NOTE(prozlach): data points interval is dependant of the time range
		// only, so in theory we could do it outside of the loop query. In
		// practice, keeping the code DRY would be non-trivial as very similar
		// code is needed to build the query itself and it is not clear how the
		// support for multiple queries for logs query analytics would look
		// like. Hence we keep things simple and do it here.
		qb, interval, err := chq.buildMetadataQuery(
			qc.StartTimestamp, qc.ProjectID, timeSpan, query,
		)
		if err != nil {
			return nil, fmt.Errorf("building metadata query: %w", err)
		}

		// NOTE(prozlach): In order to determine timestamps for the
		// `SeverityNumbersCounts` field, we need the interval, hence we
		// calculate the list of data points here. See explanation above for
		// more details why we do not put it outside of the loop.
		emptySeverityNumbersCounts := chq.preCreateSeverityNumbersCounts(
			qc.StartTimestamp, timeSpan, interval,
		)

		rawResponse, err := chq.executeMetadataQuery(ctx, qb)
		if err != nil {
			return nil, fmt.Errorf("executing metadata query: %w", err)
		}

		response, err := chq.processRawResponse(
			qc.StartTimestamp, qc.EndTimestamp,
			rawResponse,
			emptySeverityNumbersCounts,
		)

		if err != nil {
			return nil, fmt.Errorf("raw metadata processing: %w", err)
		}
		return response, nil
	}

	return nil, nil
}
