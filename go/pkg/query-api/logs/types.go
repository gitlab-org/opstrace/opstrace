package logs

import (
	"context"
	"fmt"
	"strings"
	"time"
)

type QueryContext struct {
	ProjectID string `json:"project_id"`

	Queries    map[string]*Query
	Expression string

	StartTimestamp time.Time
	EndTimestamp   time.Time
	ReferenceTime  time.Time

	PageSize  int
	PageToken string
	// NOTE(prozlach): Simple sorting by timestamp, ascending or descending.
	//
	// In order to implement anything more useful than this, we would also
	// need multi-level sorting - e.g. sort by level and then serviceID and
	// then timestamp. Plain sorting by service ID or loglevel is not very
	// useful.
	//
	// We can do it iteratively in next iterations.
	SortByTimeAsc bool
}

type Query struct {
	ServiceNames       []string
	NotServiceNames    []string
	SeverityNumbers    []int32
	NotSeverityNumbers []int32
	TraceIDs           []string
	SpanIDs            []string
	Fingerprints       []string
	TraceFlags         []uint32
	NotTraceFlags      []uint32
	Body               []string

	LogAttributeNames  []string
	LogAttributeValues []string

	ResourceAttributeNames  []string
	ResourceAttributeValues []string
}

type Querier interface {
	GetLogs(context.Context, *QueryContext) (*LogsResponse, error)
	GetLogsMetadata(context.Context, *QueryContext) (*LogsMetadataResponse, error)
}

type LogsResponse struct {
	StartTimestamp int64 `json:"start_ts"`
	EndTimestamp   int64 `json:"end_ts"`

	Results []LogsRow `json:"results"`

	// NextPageToken is the token for next page if the results are greater than page size.
	NextPageToken string `json:"next_page_token,omitempty"`
}

type LogsRow struct {
	Timestamp          time.Time         `json:"timestamp"`
	TraceID            string            `json:"trace_id" ch:"TraceIdBody"`
	SpanID             string            `json:"span_id" ch:"SpanIdBody"`
	TraceFlags         uint32            `json:"trace_flags"`
	SeverityText       string            `json:"severity_text"`
	SeverityNumber     uint8             `json:"severity_number"`
	ServiceName        string            `json:"service_name"`
	Body               string            `json:"body"`
	ResourceAttributes map[string]string `json:"resource_attributes"`
	LogAttributes      map[string]string `json:"log_attributes"`
	Fingerprint        string            `json:"fingerprint"`
}

type LogsMetadataResponse struct {
	StartTimestamp int64 `json:"start_ts"`
	EndTimestamp   int64 `json:"end_ts"`

	Summary               LogsSummary            `json:"summary"`
	SeverityNumbersCounts []SeverityNumbersCount `json:"severity_numbers_counts"`
}

type JSONableSlice []uint8

//nolint:unparam
func (u JSONableSlice) MarshalJSON() ([]byte, error) {
	var result string
	if u == nil {
		result = "null"
	} else {
		result = strings.Join(strings.Fields(fmt.Sprintf("%d", u)), ",")
	}
	return []byte(result), nil
}

type LogsSummary struct {
	ServiceNames    []string      `json:"service_names"`
	TraceFlags      []uint32      `json:"trace_flags"`
	SeverityNames   []string      `json:"severity_names"`
	SeverityNumbers JSONableSlice `json:"severity_numbers"`
}

type SeverityNumbersCount struct {
	Time   int64            `json:"time"`
	Counts map[uint8]uint64 `json:"counts"`
}

type LogsRawMetadataRawResponse struct {
	Timestamp      time.Time `json:"timestamp"`
	SeverityNumber uint8     `json:"severity_number"`
	TraceFlags     []uint32  `json:"trace_flags" ch:"TraceFlagsArr"`
	ServiceNames   []string  `json:"service_name" ch:"ServiceNamesArr"`
	SumCount       uint64    `json:"sum_count"`
}
