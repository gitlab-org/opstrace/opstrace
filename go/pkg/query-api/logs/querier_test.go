package logs_test

import (
	"context"
	"errors"
	"fmt"
	"math"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/plog"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

type SeverityNumbersCountsTestData struct {
	ServiceNames    []string
	TraceFlags      []uint32
	SeverityNames   []string
	SeverityNumbers []uint8

	Counts map[int]map[uint8]uint64
}

func createLogData(
	ctx context.Context,
	testServer *testutils.ClickHouseServer,
	startTS time.Time, count int, spacing time.Duration,
	serviceNameF func(int) string,
	severityF func(int) (string, int32),
	traceIDSpanIDF func(int) (string, string),
	traceFlagsF func(int) uint32,
	projectIDNamespaceIDF func(int) (int, int),
	budyFunc func(int) string,
	logAttrsF func(int) map[string]string,
	resourceAttrsF func(int) map[string]string,
) {
	GinkgoHelper()

	queryFmt := `INSERT INTO %s.%s (
    ProjectId,
	NamespaceId,
    Timestamp,
    ObservedTimestamp,
    TraceId,
    SpanId,
    TraceFlags,
    SeverityText,
    SeverityNumber,
    ServiceName,
    Body,
    ResourceAttributes,
    LogAttributes,
	Fingerprint
) values %s`

	args := make([]string, count)
	for i := 0; i < count; i++ {
		ts := startTS.Add(spacing * time.Duration(i)).UnixNano()
		projectID, namespaceID := projectIDNamespaceIDF(i)
		serviceName := serviceNameF(i)
		traceID, spanID := traceIDSpanIDF(i)
		traceFlags := traceFlagsF(i)
		severityText, severityNumber := severityF(i)
		body := budyFunc(i)

		elems := make([]string, 0, len(logAttrsF(i)))
		for k, v := range logAttrsF(i) {
			elems = append(elems, fmt.Sprintf("'%s', '%s'", k, v))
		}
		logAttrs := fmt.Sprintf("map(%s)", strings.Join(elems, ", "))

		elems = make([]string, 0, len(resourceAttrsF(i)))
		for k, v := range resourceAttrsF(i) {
			elems = append(elems, fmt.Sprintf("'%s', '%s'", k, v))
		}
		resourceAttrs := fmt.Sprintf("map(%s)", strings.Join(elems, ", "))

		args[i] = fmt.Sprintf(
			"(%d, %d, %d, %d, UUIDStringToNum('%s'), unhex('%s'), %d, '%s', %d, '%s', '%s', %s, %s, '%s')",
			projectID,
			namespaceID,
			ts, ts,
			traceID, spanID,
			traceFlags,
			severityText, severityNumber,
			serviceName,
			body,
			logAttrs,
			resourceAttrs,
			fmt.Sprintf("fingerprint-%04d", i),
		)
	}

	query := fmt.Sprintf(
		queryFmt,
		constants.LoggingDatabaseName, constants.LoggingTableName,
		strings.Join(args, ", "),
	)
	By(fmt.Sprintf("Submitting query %s", query))

	err := testServer.Exec(ctx, query)
	Expect(err).ToNot(HaveOccurred())

	By("Test log entries created")
}

func timeSpacedLogs(
	ctx context.Context,
	testServer *testutils.ClickHouseServer,
	startTS time.Time, spacing time.Duration, count int,
) {
	GinkgoHelper()

	createLogData(
		ctx,
		testServer,
		startTS, count, spacing,
		// serviceNameF
		func(i int) string {
			// Just a single service
			return fmt.Sprintf("%02d:fooService", i/11)
		},
		// severityF
		func(i int) (string, int32) {
			res := plog.SeverityNumber(i % 25)
			return res.String(), int32(res)
		},
		// traceIDSpanIDF
		func(i int) (string, string) {
			return fmt.Sprintf("%02daaaaaa-5cb3-11e7-907b-a6006ad3dba0", i), fmt.Sprintf("%02daaaabb", i/3)
		},
		// traceFlagsF
		func(i int) uint32 {
			return uint32(i % 2)
		},
		// projectIDNamespaceIDF
		func(i int) (int, int) {
			return 1, 1
		},
		// budyFunc
		func(i int) string {
			return fmt.Sprintf("%02d:Maryna Boryna raz two drei", i)
		},
		// resourceAttrsF
		func(i int) map[string]string {
			return map[string]string{
				"resAttrA": fmt.Sprintf("fooBar %02d-res AttrAVal", i/7),
				"resAttrB": fmt.Sprintf("bazBud %02d-res BttrAVal", i/7),
			}
		},
		// logAttrsF
		func(i int) map[string]string {
			return map[string]string{
				"logAttrA": fmt.Sprintf("fooBar %02d-log AttrAVal", i/7),
				"logAttrB": fmt.Sprintf("bazBud %02d-log BttrAVal", i/7),
			}
		},
	)
}

var _ = Describe("logs", Ordered, Serial, func() {
	var testServer *testutils.ClickHouseServer
	var dsn string

	BeforeAll(func(ctx SpecContext) {
		var err error

		testServer, _, err = testutils.NewClickHouseServerAndConnection(ctx)
		Expect(err).ToNot(HaveOccurred())

		DeferCleanup(func(ctx SpecContext) {
			if err := testServer.Terminate(ctx); err != nil {
				fmt.Fprintf(GinkgoWriter, "terminating container: %v\n", err)
			}
		})

		dsn, err = testServer.GetDSN(ctx, constants.LoggingDatabaseName)
		Expect(err).ToNot(HaveOccurred())

		Expect(testServer.CreateDatabasesAndRunMigrations(ctx, constants.LoggingDatabaseName)).To(Succeed())
	})

	BeforeEach(func(ctx SpecContext) {
		err := testServer.Exec(ctx, "truncate table logging.logs_main")
		Expect(err).ToNot(HaveOccurred())

		err = testServer.Exec(ctx, "truncate table logging.logs_quantity_analytics_2592ms")
		Expect(err).ToNot(HaveOccurred())

		err = testServer.Exec(ctx, "truncate table logging.logs_quantity_analytics_259200ms")
		Expect(err).ToNot(HaveOccurred())

		err = testServer.Exec(ctx, "truncate table logging.logs_quantity_analytics_25920s")
		Expect(err).ToNot(HaveOccurred())
	})

	Context("data quering", func() {
		DescribeTable("by time",
			func(
				ctx SpecContext,
				sortByTimeAsc bool,
				startTime, endTime time.Duration,
				timeSpacing time.Duration,
			) {
				referenceTime := time.Now()
				timeSpacedLogs(ctx, testServer, referenceTime, timeSpacing, 20)

				q, err := logs.NewQuerier(
					dsn, "",
					constants.LoggingCHQueryTimeout,
					nil,
					logger.Desugar(),
				)
				Expect(err).ToNot(HaveOccurred())
				qc := &logs.QueryContext{
					ProjectID: "1",
					Queries: map[string]*logs.Query{
						"main": {},
					},
					StartTimestamp: referenceTime.Add(startTime),
					EndTimestamp:   referenceTime.Add(endTime),
					ReferenceTime:  referenceTime.Add(endTime),
					PageSize:       100,
					SortByTimeAsc:  sortByTimeAsc,
				}
				res, err := q.GetLogs(ctx, qc)
				Expect(err).ToNot(HaveOccurred())

				if startTime != endTime {
					Expect(res.Results).To(HaveLen(6))
				} else {
					Expect(res.Results).To(HaveLen(20))
				}
				for i, row := range res.Results {
					var index int
					switch {
					case sortByTimeAsc && startTime != endTime:
						index = i + 2
					case !sortByTimeAsc && startTime != endTime:
						index = len(res.Results) + 1 - i
					case startTime == endTime:
						index = i
					}
					Expect(row.Body).To(HavePrefix(fmt.Sprintf("%02d:", index)))
				}
			},
			Entry("ascending - time range", true, 2987*time.Millisecond, 15*time.Second, 2*time.Second),
			Entry("descending - time range", false, 2987*time.Millisecond, 15*time.Second, 2*time.Second),
			Entry("ascending - exact time", true, time.Duration(0), time.Duration(0), time.Duration(0)),
			Entry("descending - exact time", false, time.Duration(0), time.Duration(0), time.Duration(0)),
		)

		DescribeTable("by attributes",
			func(ctx SpecContext, mutateF func(*logs.Query), expectedIndexes []int) {
				referenceTime := time.Now()
				timeSpacedLogs(ctx, testServer, referenceTime.Add(-51*time.Second), time.Second, 50)

				q, err := logs.NewQuerier(
					dsn, "",
					constants.LoggingCHQueryTimeout,
					nil,
					logger.Desugar(),
				)
				Expect(err).ToNot(HaveOccurred())
				qc := &logs.QueryContext{
					ProjectID: "1",
					Queries: map[string]*logs.Query{
						"main": {},
					},
					StartTimestamp: referenceTime.Add(-51 * time.Second),
					EndTimestamp:   referenceTime,
					ReferenceTime:  referenceTime,
					PageSize:       100,
					SortByTimeAsc:  true,
				}
				mutateF(qc.Queries["main"])
				res, err := q.GetLogs(ctx, qc)
				Expect(err).ToNot(HaveOccurred())

				Expect(res.Results).To(HaveLen(len(expectedIndexes)))
				for i, idx := range expectedIndexes {
					Expect(res.Results[i].Body).To(HavePrefix(fmt.Sprintf("%02d:", idx)))
				}
			},
			Entry(
				"ServiceNames and not severity numbers",
				func(q *logs.Query) {
					q.ServiceNames = []string{"00:fooService", "02:fooService"}
					q.NotSeverityNumbers = []int32{0, 1, 2, 3, 4, 5, 6}
				},
				[]int{7, 8, 9, 10, 22, 23, 24, 32},
			),
			Entry(
				"not serviceNames and severity numbers",
				func(q *logs.Query) {
					q.NotServiceNames = []string{"00:fooService", "02:fooService"}
					q.SeverityNumbers = []int32{7, 8, 9, 10}
				},
				[]int{33, 34, 35},
			),
			Entry(
				"trace ids and span ids",
				func(q *logs.Query) {
					q.TraceIDs = []string{
						"00aaaaaa-5cb3-11e7-907b-a6006ad3dba0",
						"03aaaaaa-5cb3-11e7-907b-a6006ad3dba0",
						"06aaaaaa-5cb3-11e7-907b-a6006ad3dba0",
					}
					q.SpanIDs = []string{
						"01AAAABB00000000",
						"02AAAABB00000000",
					}
				},
				[]int{3, 6},
			),
			Entry(
				"span ids and fingerprints",
				func(q *logs.Query) {
					q.Fingerprints = []string{
						"fingerprint-0005",
						"fingerprint-0006",
						"fingerprint-0007",
					}
					q.SpanIDs = []string{
						"01AAAABB00000000",
						"03AAAABB00000000",
					}
				},
				[]int{5},
			),
			Entry(
				"trace flags and body",
				func(q *logs.Query) {
					q.TraceFlags = []uint32{0}
					q.Body = []string{"15:Maryna", "02:Maryna"}
				},
				[]int{2},
			),
			Entry(
				"not trace flags and body",
				func(q *logs.Query) {
					q.NotTraceFlags = []uint32{0}
					q.Body = []string{"15:Maryna", "02:Maryna"}
				},
				[]int{15},
			),
			Entry(
				"resource and log attributes align",
				func(q *logs.Query) {
					q.LogAttributeNames = []string{"logAttrA"}
					q.LogAttributeValues = []string{"fooBar 04-log AttrAVal"}
					q.ResourceAttributeNames = []string{"resAttrA"}
					q.ResourceAttributeValues = []string{"fooBar 04-res AttrAVal"}
				},
				[]int{28, 29, 30, 31, 32, 33, 34},
			),
			Entry(
				"resource and log attributes contradict",
				func(q *logs.Query) {
					q.LogAttributeNames = []string{"logAttrA"}
					q.LogAttributeValues = []string{"fooBar 04-log AttrAVal"}
					q.ResourceAttributeNames = []string{"resAttrA"}
					q.ResourceAttributeValues = []string{"fooBar 05-res AttrAVal"}
				},
				[]int{},
			),
		)

		DescribeTable("with results pagination",
			func(ctx SpecContext, sortByTimeAsc bool, timeSpacing time.Duration) {
				referenceTime := time.Now()
				timeSpacedLogs(ctx, testServer, referenceTime, timeSpacing, 49)

				q, err := logs.NewQuerier(
					dsn, "",
					constants.LoggingCHQueryTimeout,
					nil,
					logger.Desugar(),
				)
				Expect(err).ToNot(HaveOccurred())

				var pageToken string
				for i := 0; i < 5; i++ {
					qc := &logs.QueryContext{
						ProjectID: "1",
						Queries: map[string]*logs.Query{
							"main": {},
						},
						StartTimestamp: referenceTime.Add(-15 * time.Millisecond),
						EndTimestamp:   referenceTime.Add(50 * time.Second),
						ReferenceTime:  referenceTime.Add(50 * time.Second),
						PageSize:       10,
						PageToken:      pageToken,
						SortByTimeAsc:  sortByTimeAsc,
					}
					res, err := q.GetLogs(ctx, qc)
					Expect(err).ToNot(HaveOccurred())

					if i < 4 {
						Expect(res.Results).To(HaveLen(10))
					} else {
						Expect(res.Results).To(HaveLen(9))
						Expect(res.NextPageToken).To(BeEmpty())
					}
					for j, row := range res.Results {
						var index int
						if sortByTimeAsc || !sortByTimeAsc && timeSpacing == 0 {
							index = i*10 + j
						} else {
							index = 48 - 10*i - j
						}
						Expect(row.Body).To(HavePrefix(fmt.Sprintf("%02d:", index)))
					}
					pageToken = res.NextPageToken
				}
			},
			Entry("ascending, proper time-spacing", true, time.Second),
			Entry("descending, proper time-spacing", false, time.Second),
			Entry("ascending, null time-spacing", true, time.Duration(0)),
			Entry("descending, null time-spacing", false, time.Duration(0)),
		)
	})

	Context("metadata quering", func() {
		fullServicesList := []string{
			"00:fooService",
			"01:fooService",
			"02:fooService",
			"03:fooService",
			"04:fooService",
			"05:fooService",
			"06:fooService",
			"07:fooService",
			"08:fooService",
			"09:fooService",
		}
		fullSeverityNamesList := []string{
			"unspecified",
			"trace",
			"trace2",
			"trace3",
			"trace4",
			"debug",
			"debug2",
			"debug3",
			"debug4",
			"info",
			"info2",
			"info3",
			"info4",
			"warn",
			"warn2",
			"warn3",
			"warn4",
			"error",
			"error2",
			"error3",
			"error4",
			"fatal",
			"fatal2",
			"fatal3",
			"fatal4",
		}

		fullSeverityNumbersList := []uint8{
			0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
		}

		DescribeTable("with time range and enough data points",
			func(
				ctx SpecContext,
				timeRange time.Duration,
				interval time.Duration,
				sampleOffset time.Duration,
			) {
				const aggregationPeriods = 100
				startTime := common.TruncateTimeUsingCHLogic(time.Now(), interval)
				endTime := startTime.Add(timeRange)
				timeSpacedLogs(ctx, testServer, startTime.Add(sampleOffset), interval, aggregationPeriods)

				q, err := logs.NewQuerier(
					dsn, "",
					constants.LoggingCHQueryTimeout,
					nil,
					logger.Desugar(),
				)
				Expect(err).ToNot(HaveOccurred())

				qc := &logs.QueryContext{
					ProjectID: "1",
					Queries: map[string]*logs.Query{
						"main": {},
					},
					StartTimestamp: startTime,
					EndTimestamp:   endTime,
					ReferenceTime:  endTime,
				}
				res, err := q.GetLogsMetadata(ctx, qc)
				Expect(err).ToNot(HaveOccurred())

				Expect(res.StartTimestamp).To(Equal(startTime.UnixNano()))
				Expect(res.EndTimestamp).To(Equal(endTime.UnixNano()))
				Expect(res.Summary.ServiceNames).To(Equal(fullServicesList))
				Expect(res.Summary.TraceFlags).To(Equal([]uint32{0, 1}))
				Expect(res.Summary.SeverityNames).To(Equal(fullSeverityNamesList))
				Expect(res.Summary.SeverityNumbers).To(BeEquivalentTo(fullSeverityNumbersList))

				Expect(res.SeverityNumbersCounts).To(HaveLen(aggregationPeriods))
				ts := res.SeverityNumbersCounts[0].Time
				for i, v := range res.SeverityNumbersCounts {
					Expect(v.Time).To(
						Equal(ts),
						fmt.Sprintf("entry %d has mismatched timestamp: %s vs %s",
							i,
							time.Unix(0, ts).Format(time.RFC3339Nano),
							time.Unix(0, v.Time).Format(time.RFC3339Nano),
						),
					)
					ts = ts + interval.Nanoseconds()

					for j := uint8(0); j < 25; j++ {
						Expect(v.Counts).To(HaveKey(j))
						c := v.Counts[j]
						if j == uint8(i%25) {
							Expect(c).To(BeEquivalentTo(1))
						} else {
							Expect(c).To(BeEquivalentTo(0))
						}
					}
				}
			},
			Entry(
				"MV-7h12, aligned entries, period 30d/720h",
				30*24*time.Hour, 432*time.Minute, time.Duration(0)),
			Entry(
				"MV-7h12, unaligned entries, period 30d/720h",
				30*24*time.Hour, 432*time.Minute, 3*time.Hour),
			Entry(
				"MV-259200ms downsampled, aligned entries, period 72h",
				72*time.Hour, 2592*time.Second, time.Duration(0)),
			Entry(
				"MV-259200ms downsampled, unaligned entries, period 72h",
				72*time.Hour, 2592*time.Second, 15*time.Minute),
			Entry(
				"MV-259200ms, aligned entries, period 7.2h",
				432*time.Minute, 259200*time.Millisecond, time.Duration(0)),
			Entry(
				"MV-259200ms, unaligned entries, period 7.2h",
				432*time.Minute, 259200*time.Millisecond, 15*time.Second),
			Entry(
				"MV-2592ms downsampled, aligned entries, period 0.72h/43.2m",
				2592*time.Second, 25920*time.Millisecond, time.Duration(0)),
			Entry(
				"MV-2592ms downsampled, unaligned entries, period 0.72h/43.2m",
				2592*time.Second, 25920*time.Millisecond, 2*time.Second),
			Entry(
				"MV-2592ms, aligned entries, period 0.072h/4.32m",
				259200*time.Millisecond, 2592*time.Millisecond, time.Duration(0)),
			Entry(
				"MV-2592ms, unaligned entries, period 0.072h/4.32m",
				259200*time.Millisecond, 2592*time.Millisecond, 200*time.Millisecond),
			Entry(
				"Main table, aligned entries, period 0.0072h/25.92s",
				25920*time.Millisecond, 259200*time.Microsecond, time.Duration(0)),
			Entry(
				"Main table, unaligned entries, period 0.0072h/25.92s",
				25920*time.Millisecond, 259200*time.Microsecond, 20*time.Millisecond),
			Entry(
				"Main table, aligned entries, period 0.00072h/2.592s",
				2592*time.Millisecond, 25920*time.Microsecond, time.Duration(0)),
			Entry(
				"Main table, unaligned entries, period 0.00072h/2.592s",
				2592*time.Millisecond, 25920*time.Microsecond, 2*time.Millisecond),
			Entry(
				"Main table, aligned entries, period 0.000072h/0.2592s",
				259200*time.Microsecond, 2592*time.Microsecond, time.Duration(0)),
			Entry(
				"Main table, unaligned entries, period 0.000072h/0.2592s",
				259200*time.Microsecond, 2592*time.Microsecond, 200*time.Microsecond),
			Entry(
				"Main table, aligned entries, period 0.0000072h/0.02592s",
				25920*time.Microsecond, 259200*time.Nanosecond, time.Duration(0)),
			Entry(
				"Main table, unaligned entries, period 0.0000072h/0.02592s",
				25920*time.Microsecond, 259200*time.Nanosecond, 20*time.Microsecond),
			Entry(
				"Main table, aligned entries, period 0.00000072h/0.002592s",
				2592*time.Microsecond, 25920*time.Nanosecond, time.Duration(0)),
			Entry(
				"Main table, unaligned entries, period 0.00000072h/0.002592s",
				2592*time.Microsecond, 25920*time.Nanosecond, 2*time.Microsecond),
			Entry(
				"Main table, aligned entries, period 0.000000072h/0.0002592s",
				2592*time.Microsecond, 25920*time.Nanosecond, time.Duration(0)),
			Entry(
				"Main table, unaligned entries, period 0.000000072h/0.0002592s",
				100*constants.LoggingSmallestAggregationInterval, constants.LoggingSmallestAggregationInterval, 200*time.Nanosecond),
			// NOTE(prozlach): 2592*time.Nanosecond is the lowerst interval
			// that we can handle. For periods lower that 10x2592ns we start
			// returning less than 10 items. Next test covers this use case.
		)

		DescribeTable("with time range and not enough data points",
			func(ctx SpecContext, sampleOffset time.Duration) {
				// 2592ns/12=216 - 12 samples for every aggregation period. In
				// total 8 full aggregation periods of 2592ns plus one partial
				// (4 samples)
				const samplesPerInterval = 12
				const interval = (2592 / samplesPerInterval) * time.Nanosecond
				const numberOfSamples = 100

				referenceTime := time.Now()
				startTime := common.TruncateTimeUsingCHLogic(referenceTime, constants.LoggingSmallestAggregationInterval)
				endTime := startTime.Add(numberOfSamples * interval)
				timeSpacedLogs(ctx, testServer, startTime.Add(sampleOffset), interval, numberOfSamples)

				q, err := logs.NewQuerier(
					dsn, "",
					constants.LoggingCHQueryTimeout,
					nil,
					logger.Desugar(),
				)
				Expect(err).ToNot(HaveOccurred())

				qc := &logs.QueryContext{
					ProjectID: "1",
					Queries: map[string]*logs.Query{
						"main": {},
					},
					StartTimestamp: startTime,
					EndTimestamp:   endTime,
					ReferenceTime:  endTime,
				}
				res, err := q.GetLogsMetadata(ctx, qc)
				Expect(err).ToNot(HaveOccurred())

				Expect(res.StartTimestamp).To(Equal(startTime.UnixNano()))
				Expect(res.EndTimestamp).To(Equal(endTime.UnixNano()))
				Expect(res.Summary.ServiceNames).To(Equal(fullServicesList))
				Expect(res.Summary.TraceFlags).To(Equal([]uint32{0, 1}))
				Expect(res.Summary.SeverityNames).To(Equal(fullSeverityNamesList))
				Expect(res.Summary.SeverityNumbers).To(BeEquivalentTo(fullSeverityNumbersList))

				Expect(
					res.SeverityNumbersCounts,
				).To(
					HaveLen(
						int(math.Ceil(float64(numberOfSamples) / samplesPerInterval)),
					),
				)
				ts := res.SeverityNumbersCounts[0].Time
				for i := 0; i < numberOfSamples; i++ {
					v := res.SeverityNumbersCounts[i/12]
					Expect(v.Time).To(
						Equal(
							ts+(constants.LoggingSmallestAggregationInterval.Nanoseconds()*int64(i/12)),
						),
						fmt.Sprintf("entry %d has mismatched timestamp: %s vs %s",
							i,
							time.Unix(0, ts).Format(time.RFC3339Nano),
							time.Unix(0, v.Time).Format(time.RFC3339Nano),
						),
					)

					for j := uint8(0); j < 25; j++ {
						Expect(v.Counts).To(HaveKey(j))
						v.Counts[uint8(i%25)] = 0
					}
				}

				// Verify that we found all 1s in the counts table:
				for _, vsc := range res.SeverityNumbersCounts {
					for _, sc := range vsc.Counts {
						Expect(sc).To(BeZero())
					}
				}
			},
			Entry("aligned", time.Duration(0)),
			Entry("unaligned", 50*time.Nanosecond),
		)

		It("with time being a single point", func(ctx SpecContext) {
			const numberOfSamples = 100

			referenceTime := time.Now()
			startTime := referenceTime
			endTime := referenceTime
			chTime := common.TruncateTimeUsingCHLogic(startTime, constants.LoggingSmallestAggregationInterval)
			timeSpacedLogs(ctx, testServer, startTime, 0, numberOfSamples)

			q, err := logs.NewQuerier(
				dsn, "",
				constants.LoggingCHQueryTimeout,
				nil,
				logger.Desugar(),
			)
			Expect(err).ToNot(HaveOccurred())

			qc := &logs.QueryContext{
				ProjectID: "1",
				Queries: map[string]*logs.Query{
					"main": {},
				},
				StartTimestamp: startTime,
				EndTimestamp:   endTime,
				ReferenceTime:  referenceTime,
			}
			res, err := q.GetLogsMetadata(ctx, qc)
			Expect(err).ToNot(HaveOccurred())

			Expect(res.StartTimestamp).To(Equal(chTime.UnixNano()))
			Expect(res.EndTimestamp).To(Equal(chTime.Add(constants.LoggingSmallestAggregationInterval).UnixNano()))
			Expect(res.Summary.ServiceNames).To(Equal(fullServicesList))
			Expect(res.Summary.TraceFlags).To(Equal([]uint32{0, 1}))
			Expect(res.Summary.SeverityNames).To(Equal(fullSeverityNamesList))
			Expect(res.Summary.SeverityNumbers).To(BeEquivalentTo(fullSeverityNumbersList))

			Expect(res.SeverityNumbersCounts).To(HaveLen(1))
			Expect(res.SeverityNumbersCounts[0].Time).To(Equal(chTime.UnixNano()))
			for _, sc := range res.SeverityNumbersCounts[0].Counts {
				Expect(sc).To(BeEquivalentTo(4))
			}
		},
		)

		// There are two cases here. One is where time span fits into single
		// aggregation period and the other being where it starts in one and
		// finishes in the other
		DescribeTable("with time range being a fraction of smallest agregation interval",
			func(ctx SpecContext, spanTwoPeriods bool) {
				const numberOfSamples = 100
				const logSpacing = constants.LoggingSmallestAggregationInterval / numberOfSamples // 25ns

				referenceTime := common.TruncateTimeUsingCHLogic(time.Now(), constants.LoggingSmallestAggregationInterval).Add(2 * constants.LoggingSmallestAggregationInterval)
				var startTime time.Time
				if spanTwoPeriods {
					// We want samples to be equally distributed across both
					// time intervals
					startTime = referenceTime.Add(-constants.LoggingSmallestAggregationInterval - logSpacing*numberOfSamples/2)
				} else {
					startTime = referenceTime.Add(-2*constants.LoggingSmallestAggregationInterval + 10*time.Nanosecond)
				}
				endTime := startTime.Add(logSpacing*100 + time.Nanosecond)
				timeSpacedLogs(ctx, testServer, startTime, logSpacing, numberOfSamples)

				q, err := logs.NewQuerier(
					dsn, "",
					constants.LoggingCHQueryTimeout,
					nil,
					logger.Desugar(),
				)
				Expect(err).ToNot(HaveOccurred())

				qc := &logs.QueryContext{
					ProjectID: "1",
					Queries: map[string]*logs.Query{
						"main": {},
					},
					StartTimestamp: startTime,
					EndTimestamp:   endTime,
					ReferenceTime:  referenceTime,
				}
				res, err := q.GetLogsMetadata(ctx, qc)
				Expect(err).ToNot(HaveOccurred())

				Expect(res.StartTimestamp).To(Equal(referenceTime.Add(-2 * constants.LoggingSmallestAggregationInterval).UnixNano()))
				if spanTwoPeriods {
					Expect(res.EndTimestamp).To(Equal(referenceTime.UnixNano()))
				} else {
					Expect(res.EndTimestamp).To(Equal(referenceTime.Add(-1 * constants.LoggingSmallestAggregationInterval).UnixNano()))
				}
				Expect(res.Summary.ServiceNames).To(Equal(fullServicesList))
				Expect(res.Summary.TraceFlags).To(Equal([]uint32{0, 1}))
				Expect(res.Summary.SeverityNames).To(Equal(fullSeverityNamesList))
				Expect(res.Summary.SeverityNumbers).To(BeEquivalentTo(fullSeverityNumbersList))

				if spanTwoPeriods {
					Expect(res.SeverityNumbersCounts).To(HaveLen(2))
				} else {
					Expect(res.SeverityNumbersCounts).To(HaveLen(1))
				}
				Expect(res.SeverityNumbersCounts[0].Time).To(Equal(res.StartTimestamp))
				if spanTwoPeriods {
					Expect(res.SeverityNumbersCounts[1].Time).To(Equal(res.StartTimestamp + constants.LoggingSmallestAggregationInterval.Nanoseconds()))
				}
				for _, scs := range res.SeverityNumbersCounts {
					for _, c := range scs.Counts {
						if spanTwoPeriods {
							Expect(c).To(BeEquivalentTo(2))
						} else {
							Expect(c).To(BeEquivalentTo(4))
						}
					}
				}
			},
			Entry("spanning one aggregation period", false),
			Entry("spanning two aggregation periods", true),
		)

		It("with no logging data", func(ctx SpecContext) {
			aggregationInterval := 432 * time.Minute
			aggregationIntervalsCount := 50

			referenceTime := time.Now()
			startTime := referenceTime.Add(aggregationInterval * time.Duration(-1*aggregationIntervalsCount))
			endTime := referenceTime
			firstIntervalTime := common.TruncateTimeUsingCHLogic(startTime, aggregationInterval)
			if firstIntervalTime.Before(startTime) {
				firstIntervalTime = firstIntervalTime.Add(aggregationInterval)
			}

			q, err := logs.NewQuerier(
				dsn, "",
				constants.LoggingCHQueryTimeout,
				nil,
				logger.Desugar(),
			)
			Expect(err).ToNot(HaveOccurred())

			qc := &logs.QueryContext{
				ProjectID: "1",
				Queries: map[string]*logs.Query{
					"main": {},
				},
				StartTimestamp: startTime,
				EndTimestamp:   endTime,
				ReferenceTime:  referenceTime,
			}
			res, err := q.GetLogsMetadata(ctx, qc)
			Expect(err).ToNot(HaveOccurred())

			Expect(res.StartTimestamp).To(Equal(startTime.UnixNano()))
			Expect(res.EndTimestamp).To(Equal(endTime.UnixNano()))
			Expect(res.Summary.ServiceNames).To(BeEmpty())
			Expect(res.Summary.TraceFlags).To(BeEmpty())
			Expect(res.Summary.SeverityNames).To(BeEmpty())
			Expect(res.Summary.SeverityNumbers).To(BeEmpty())

			Expect(res.SeverityNumbersCounts).To(HaveLen(aggregationIntervalsCount))
			for i, sc := range res.SeverityNumbersCounts {
				Expect(sc.Time).To(
					Equal(firstIntervalTime.UnixNano()+aggregationInterval.Nanoseconds()*int64(i)),
					fmt.Sprintf("i: %d", i),
				)
				for j, c := range sc.Counts {
					Expect(c).To(BeEquivalentTo(0), fmt.Sprintf("i: %d, j:%d, c:%d", i, j, c))
				}
			}
		},
		)

		It("with time range longer than 30d", func(ctx SpecContext) {
			aggregationInterval := 432 * time.Minute
			aggregationIntervalsCount := 110

			referenceTime := common.TruncateTimeUsingCHLogic(time.Now(), aggregationInterval)
			startTime := referenceTime.Add(aggregationInterval * time.Duration(-1*aggregationIntervalsCount))
			endTime := referenceTime.Add(aggregationInterval)

			q, err := logs.NewQuerier(
				dsn, "",
				constants.LoggingCHQueryTimeout,
				nil,
				logger.Desugar(),
			)
			Expect(err).ToNot(HaveOccurred())

			qc := &logs.QueryContext{
				ProjectID: "1",
				Queries: map[string]*logs.Query{
					"main": {},
				},
				StartTimestamp: startTime,
				EndTimestamp:   endTime,
				ReferenceTime:  referenceTime,
			}
			res, err := q.GetLogsMetadata(ctx, qc)
			Expect(err).ToNot(HaveOccurred())

			expectedStartTime := startTime.Add(10 * aggregationInterval)
			Expect(res.StartTimestamp).To(Equal(expectedStartTime.UnixNano()))
			// Verify that the results time window was narrowed down to just
			// 100 periods, down from 110
			Expect(res.EndTimestamp).To(Equal(expectedStartTime.Add(100 * aggregationInterval).UnixNano()))

			Expect(res.SeverityNumbersCounts).To(HaveLen(100))
			for i, sc := range res.SeverityNumbersCounts {
				Expect(sc.Time).To(
					Equal(expectedStartTime.Add(aggregationInterval*time.Duration(i)).UnixNano()),
					fmt.Sprintf("i: %d", i),
				)
			}
		},
		)

		It("with single data point", func(ctx SpecContext) {
			aggregationInterval := 2592 * time.Second
			referenceTime := common.TruncateTimeUsingCHLogic(time.Now(), aggregationInterval)
			startTime := referenceTime.Add(-100 * aggregationInterval)
			endTime := referenceTime
			timeSpacedLogs(ctx, testServer, startTime.Add(50*aggregationInterval+10*time.Minute), 0, 1)

			q, err := logs.NewQuerier(
				dsn, "",
				constants.LoggingCHQueryTimeout,
				nil,
				logger.Desugar(),
			)
			Expect(err).ToNot(HaveOccurred())

			qc := &logs.QueryContext{
				ProjectID: "1",
				Queries: map[string]*logs.Query{
					"main": {},
				},
				StartTimestamp: startTime,
				EndTimestamp:   endTime,
				ReferenceTime:  referenceTime,
			}
			res, err := q.GetLogsMetadata(ctx, qc)
			Expect(err).ToNot(HaveOccurred())

			Expect(res.StartTimestamp).To(Equal(startTime.UnixNano()))
			Expect(res.EndTimestamp).To(Equal(endTime.UnixNano()))
			Expect(res.Summary.ServiceNames).To(Equal([]string{"00:fooService"}))
			Expect(res.Summary.TraceFlags).To(Equal([]uint32{0}))
			Expect(res.Summary.SeverityNames).To(Equal([]string{"unspecified"}))
			Expect(res.Summary.SeverityNumbers).To(BeEquivalentTo([]uint8{0}))

			Expect(res.SeverityNumbersCounts).To(HaveLen(100))
			Expect(res.SeverityNumbersCounts[0].Time).To(Equal(startTime.UnixNano()))
			for i, scs := range res.SeverityNumbersCounts {
				Expect(scs.Time).To(
					Equal(startTime.Add(aggregationInterval*time.Duration(i)).UnixNano()),
					fmt.Sprintf("i: %d", i),
				)
				Expect(scs.Counts).To(HaveLen(1), fmt.Sprintf("i: %d", i))
				if i == 50 {
					Expect(scs.Counts[0]).To(BeEquivalentTo(1), fmt.Sprintf("i: %d", i))
				} else {
					Expect(scs.Counts[0]).To(BeEquivalentTo(0), fmt.Sprintf("i: %d", i))
				}
			}
		},
		)

		It("with query timeout", func(ctx SpecContext) {
			referenceTime := time.Now()
			startTime := referenceTime.Add(-72 * time.Hour)
			endTime := referenceTime
			timeSpacedLogs(ctx, testServer, startTime, 1*time.Second, 10000)

			q, err := logs.NewQuerier(
				dsn, "",
				0.0001, // ridiculously low in order to trigger a timeout
				nil,
				logger.Desugar(),
			)
			Expect(err).ToNot(HaveOccurred())

			qc := &logs.QueryContext{
				ProjectID: "1",
				Queries: map[string]*logs.Query{
					"main": {},
				},
				StartTimestamp: startTime,
				EndTimestamp:   endTime,
				ReferenceTime:  referenceTime,
			}
			_, err = q.GetLogsMetadata(ctx, qc)

			var exceptionErr *clickhouse.Exception
			ok := errors.As(err, &exceptionErr)
			Expect(ok).To(BeTrue())

			// source: https://github.com/ClickHouse/ClickHouse/blob/master/src/Common/ErrorCodes.cpp
			const clickHouseTimeoutExceededErrorCode = int32(159)
			Expect(exceptionErr.Code).To(Equal(clickHouseTimeoutExceededErrorCode))
		},
		)

		DescribeTable("with query parameters",
			func(
				ctx SpecContext,
				queryMutateF func(*logs.Query),
				sncTestData SeverityNumbersCountsTestData,
			) {
				const numberOfSamples = 100
				const interval = 2592 * time.Second

				referenceTime := common.TruncateTimeUsingCHLogic(time.Now(), 2592*time.Second)
				startTime := referenceTime.Add(-1 * interval * numberOfSamples)
				endTime := referenceTime
				timeSpacedLogs(ctx, testServer, startTime, interval, numberOfSamples)

				q, err := logs.NewQuerier(
					dsn, "",
					constants.LoggingCHQueryTimeout,
					nil,
					logger.Desugar(),
				)
				Expect(err).ToNot(HaveOccurred())

				qc := &logs.QueryContext{
					ProjectID: "1",
					Queries: map[string]*logs.Query{
						"main": {},
					},
					StartTimestamp: startTime,
					EndTimestamp:   endTime,
					ReferenceTime:  referenceTime,
				}
				queryMutateF(qc.Queries["main"])
				res, err := q.GetLogsMetadata(ctx, qc)
				Expect(err).ToNot(HaveOccurred())

				Expect(res.Summary.ServiceNames).To(Equal(sncTestData.ServiceNames))
				Expect(res.Summary.TraceFlags).To(Equal(sncTestData.TraceFlags))
				Expect(res.Summary.SeverityNames).To(Equal(sncTestData.SeverityNames))
				Expect(res.Summary.SeverityNumbers).To(BeEquivalentTo(sncTestData.SeverityNumbers))

				Expect(res.SeverityNumbersCounts).To(HaveLen(100))

				for i, scs := range res.SeverityNumbersCounts {
					Expect(scs.Time).To(
						Equal(startTime.Add(interval*time.Duration(i)).UnixNano()),
						fmt.Sprintf("i: %d", i),
					)
					Expect(scs.Counts).To(
						HaveLen(
							len(sncTestData.SeverityNumbers),
						),
						fmt.Sprintf("i: %d", i),
					)

					for sn, c := range scs.Counts {
						if v, ok := sncTestData.Counts[i][sn]; ok {
							Expect(c).To(BeEquivalentTo(v), fmt.Sprintf("sn: %d", sn))
						} else {
							Expect(c).To(BeZero(), fmt.Sprintf("sn: %d", sn))
						}
					}
				}
			},
			Entry(
				"ServiceNames and not severity numbers",
				func(q *logs.Query) {
					q.ServiceNames = []string{"00:fooService", "02:fooService"}
					q.NotSeverityNumbers = []int32{0, 1, 2, 3, 4, 5, 6}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{"00:fooService", "02:fooService"},
					TraceFlags:      []uint32{0, 1},
					SeverityNames:   []string{"debug3", "debug4", "info", "info2", "fatal2", "fatal3", "fatal4"},
					SeverityNumbers: []uint8{7, 8, 9, 10, 22, 23, 24},

					Counts: map[int]map[uint8]uint64{
						7:  {7: 1},
						8:  {8: 1},
						9:  {9: 1},
						10: {10: 1},
						22: {22: 1},
						23: {23: 1},
						24: {24: 1},
						32: {7: 1},
					},
				},
			),
			Entry(
				"not serviceNames and severity numbers",
				func(q *logs.Query) {
					q.NotServiceNames = []string{"00:fooService", "02:fooService", "03:fooService", "09:fooService"}
					q.SeverityNumbers = []int32{9, 10, 24}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{"04:fooService", "05:fooService", "06:fooService", "07:fooService"},
					TraceFlags:      []uint32{0, 1},
					SeverityNames:   []string{"info", "info2", "fatal4"},
					SeverityNumbers: []uint8{9, 10, 24},

					Counts: map[int]map[uint8]uint64{
						49: {24: 1},
						59: {9: 1},
						60: {10: 1},
						74: {24: 1},
						84: {9: 1},
						85: {10: 1},
					},
				},
			),
			Entry(
				"trace ids and span ids",
				func(q *logs.Query) {
					q.TraceIDs = []string{
						"00aaaaaa-5cb3-11e7-907b-a6006ad3dba0",
						"03aaaaaa-5cb3-11e7-907b-a6006ad3dba0",
						"06aaaaaa-5cb3-11e7-907b-a6006ad3dba0",
					}
					q.SpanIDs = []string{
						"01AAAABB00000000",
						"02AAAABB00000000",
					}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{"00:fooService"},
					TraceFlags:      []uint32{0, 1},
					SeverityNames:   []string{"trace3", "debug2"},
					SeverityNumbers: []uint8{3, 6},

					Counts: map[int]map[uint8]uint64{
						3: {3: 1},
						6: {6: 1},
					},
				},
			),
			Entry(
				"span ids and fingerprints",
				func(q *logs.Query) {
					q.Fingerprints = []string{
						"fingerprint-0005",
						"fingerprint-0006",
						"fingerprint-0007",
					}
					q.SpanIDs = []string{
						"01AAAABB00000000",
						"03AAAABB00000000",
					}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{"00:fooService"},
					TraceFlags:      []uint32{1},
					SeverityNames:   []string{"debug"},
					SeverityNumbers: []uint8{5},

					Counts: map[int]map[uint8]uint64{
						5: {5: 1},
					},
				},
			),
			Entry(
				"trace flags and body",
				func(q *logs.Query) {
					q.TraceFlags = []uint32{0}
					q.Body = []string{"15:Maryna", "02:Maryna"}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{"00:fooService"},
					TraceFlags:      []uint32{0},
					SeverityNames:   []string{"trace2"},
					SeverityNumbers: []uint8{2},

					Counts: map[int]map[uint8]uint64{
						2: {2: 1},
					},
				},
			),
			Entry(
				"not trace flags and body",
				func(q *logs.Query) {
					q.NotTraceFlags = []uint32{0}
					q.Body = []string{"15:Maryna", "02:Maryna"}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{"01:fooService"},
					TraceFlags:      []uint32{1},
					SeverityNames:   []string{"warn3"},
					SeverityNumbers: []uint8{15},

					Counts: map[int]map[uint8]uint64{
						15: {15: 1},
					},
				},
			),
			Entry(
				"resource and log attributes align",
				func(q *logs.Query) {
					q.LogAttributeNames = []string{"logAttrA"}
					q.LogAttributeValues = []string{"fooBar 04-log AttrAVal"}
					q.ResourceAttributeNames = []string{"resAttrA"}
					q.ResourceAttributeValues = []string{"fooBar 04-res AttrAVal"}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{"02:fooService", "03:fooService"},
					TraceFlags:      []uint32{0, 1},
					SeverityNames:   []string{"trace3", "trace4", "debug", "debug2", "debug3", "debug4", "info"},
					SeverityNumbers: []uint8{3, 4, 5, 6, 7, 8, 9},

					Counts: map[int]map[uint8]uint64{
						28: {3: 1},
						29: {4: 1},
						30: {5: 1},
						31: {6: 1},
						32: {7: 1},
						33: {8: 1},
						34: {9: 1},
					},
				},
			),
			Entry(
				"resource and log attributes contradict",
				func(q *logs.Query) {
					q.LogAttributeNames = []string{"logAttrA"}
					q.LogAttributeValues = []string{"fooBar 04-log AttrAVal"}
					q.ResourceAttributeNames = []string{"resAttrA"}
					q.ResourceAttributeValues = []string{"fooBar 05-res AttrAVal"}
				},
				SeverityNumbersCountsTestData{
					ServiceNames:    []string{},
					TraceFlags:      []uint32{},
					SeverityNames:   []string{},
					SeverityNumbers: []uint8{},

					Counts: map[int]map[uint8]uint64{},
				},
			),
		)

	})
})
