package logs

import (
	"fmt"
	"strconv"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

const (
	defaultProjectID = "1"
)

var (
	refTime = time.Now().UTC()
)

// Group of tests that use internal interfaces of the logs package
var _ = Describe("logs internals test", Ordered, Serial, func() {
	DescribeTable(
		"building logs-metadata queries",
		func(
			timeSpan time.Duration,
			pid string,
			startTS time.Time,
			query *Query,
			expectedQuery string,
			expectedArgs clickhouse.Parameters,
			expectedInterval time.Duration,
		) {
			chq := new(chQuerier)

			qb, interval, err := chq.buildMetadataQuery(startTS, pid, timeSpan, query)
			Expect(err).ToNot(HaveOccurred())

			Expect(interval).To(Equal(expectedInterval))

			fmt.Fprintf(GinkgoWriter, "query: %s\n", qb.SQL())

			err = testutils.CompareSQLStrings(qb.SQL(), expectedQuery)
			Expect(err).ToNot(HaveOccurred())

			Expect(qb.StringifyParams()).To(Equal(expectedArgs))
		},
		Entry(
			"30d interval, no query params",
			30*24*time.Hour,
			defaultProjectID,
			refTime.Add(-1*30*24*time.Hour),
			&Query{},
			`
SELECT
    Timestamp,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    sumMerge(SumCount) as SumCount
FROM {p0:Identifier}.{p1:Identifier}
WHERE
  ProjectId = {p2:String}
AND Timestamp >= {p3:DateTime64(9, 'UTC')} AND Timestamp < {p4:DateTime64(9, 'UTC')}
GROUP BY Timestamp, SeverityNumber
ORDER BY Timestamp ASC`,
			clickhouse.Parameters{
				"p0": constants.LoggingDatabaseName,
				"p1": constants.LoggingTableQAnaliticsTableName + "_25920s",
				"p2": defaultProjectID,
				"p3": strconv.FormatInt(refTime.Add(-1*30*24*time.Hour).UnixNano(), 10),
				"p4": strconv.FormatInt(refTime.UnixNano(), 10),
			},
			432*time.Minute,
		),
		Entry(
			"72h interval, no query params",
			72*time.Hour,
			defaultProjectID,
			refTime.Add(-1*72*time.Hour),
			&Query{},
			`
SELECT
	toStartOfInterval(Timestamp, INTERVAL 2592 second) AS Timestamp,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    sumMerge(SumCount) as SumCount
FROM {p0:Identifier}.{p1:Identifier}
WHERE
  ProjectId = {p2:String}
AND Timestamp >= {p3:DateTime64(9, 'UTC')} AND Timestamp < {p4:DateTime64(9, 'UTC')}
GROUP BY Timestamp, SeverityNumber
ORDER BY Timestamp ASC`,
			clickhouse.Parameters{
				"p0": constants.LoggingDatabaseName,
				"p1": constants.LoggingTableQAnaliticsTableName + "_259200ms",
				"p2": defaultProjectID,
				"p3": strconv.FormatInt(refTime.Add(-72*time.Hour).UnixNano(), 10),
				"p4": strconv.FormatInt(refTime.UnixNano(), 10),
			},
			2592*time.Second,
		),
		Entry(
			"7.2h interval, no query params",
			432*time.Minute,
			defaultProjectID,
			refTime.Add(-432*time.Minute),
			&Query{},
			`
SELECT
	Timestamp,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    sumMerge(SumCount) as SumCount
FROM {p0:Identifier}.{p1:Identifier}
WHERE
  ProjectId = {p2:String}
AND Timestamp >= {p3:DateTime64(9, 'UTC')} AND Timestamp < {p4:DateTime64(9, 'UTC')}
GROUP BY Timestamp, SeverityNumber
ORDER BY Timestamp ASC`,
			clickhouse.Parameters{
				"p0": constants.LoggingDatabaseName,
				"p1": constants.LoggingTableQAnaliticsTableName + "_259200ms",
				"p2": defaultProjectID,
				"p3": strconv.FormatInt(refTime.Add(-432*time.Minute).UnixNano(), 10),
				"p4": strconv.FormatInt(refTime.UnixNano(), 10),
			},
			259200*time.Millisecond,
		),
		Entry(
			"0.72h interval, no query params",
			2592*time.Second,
			defaultProjectID,
			refTime.Add(-2592*time.Second),
			&Query{},
			`
SELECT
	toStartOfInterval(Timestamp, INTERVAL 25920 millisecond) AS Timestamp,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    sumMerge(SumCount) as SumCount
FROM {p0:Identifier}.{p1:Identifier}
WHERE
  ProjectId = {p2:String}
AND Timestamp >= {p3:DateTime64(9, 'UTC')} AND Timestamp < {p4:DateTime64(9, 'UTC')}
GROUP BY Timestamp, SeverityNumber
ORDER BY Timestamp ASC`,
			clickhouse.Parameters{
				"p0": constants.LoggingDatabaseName,
				"p1": constants.LoggingTableQAnaliticsTableName + "_2592ms",
				"p2": defaultProjectID,
				"p3": strconv.FormatInt(refTime.Add(-2592*time.Second).UnixNano(), 10),
				"p4": strconv.FormatInt(refTime.UnixNano(), 10),
			},
			25920*time.Millisecond,
		),
		Entry(
			"0.072h interval, no query params",
			259200*time.Millisecond,
			defaultProjectID,
			refTime.Add(-259200*time.Millisecond),
			&Query{},
			`
SELECT
	Timestamp,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    sumMerge(SumCount) as SumCount
FROM {p0:Identifier}.{p1:Identifier}
WHERE
  ProjectId = {p2:String}
AND Timestamp >= {p3:DateTime64(9, 'UTC')} AND Timestamp < {p4:DateTime64(9, 'UTC')}
GROUP BY Timestamp, SeverityNumber
ORDER BY Timestamp ASC`,
			clickhouse.Parameters{
				"p0": constants.LoggingDatabaseName,
				"p1": constants.LoggingTableQAnaliticsTableName + "_2592ms",
				"p2": defaultProjectID,
				"p3": strconv.FormatInt(refTime.Add(-259200*time.Millisecond).UnixNano(), 10),
				"p4": strconv.FormatInt(refTime.UnixNano(), 10),
			},
			2592*time.Millisecond,
		),
		Entry(
			"0.00072h interval, no query params",
			25920*time.Millisecond,
			defaultProjectID,
			refTime.Add(-25920*time.Millisecond),
			&Query{},
			`
SELECT
	toStartOfInterval(Timestamp, INTERVAL 259200000 nanoseconds) AS Timestamp,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    count(*) as SumCount
FROM {p0:Identifier}.{p1:Identifier}
WHERE
  ProjectId = {p2:String}
AND Timestamp >= {p3:DateTime64(9, 'UTC')} AND Timestamp < {p4:DateTime64(9, 'UTC')}
GROUP BY Timestamp, SeverityNumber
ORDER BY Timestamp ASC`,
			clickhouse.Parameters{
				"p0": constants.LoggingDatabaseName,
				"p1": constants.LoggingTableName,
				"p2": defaultProjectID,
				"p3": strconv.FormatInt(refTime.Add(-25920*time.Millisecond).UnixNano(), 10),
				"p4": strconv.FormatInt(refTime.UnixNano(), 10),
			},
			259200*time.Microsecond,
		),
		Entry(
			"0.00072h interval, with query params",
			25920*time.Millisecond,
			defaultProjectID,
			refTime.Add(-25920*time.Millisecond),
			&Query{
				Fingerprints: []string{
					"fingerprint-0005",
					"fingerprint-0006",
					"fingerprint-0007",
				},
				SpanIDs: []string{
					"01AAAABB00000000",
					"03AAAABB00000000",
				},
				NotTraceFlags:           []uint32{0},
				Body:                    []string{"15:Maryna", "02:Maryna"},
				LogAttributeNames:       []string{"logAttrA"},
				LogAttributeValues:      []string{"fooBar 04-log AttrAVal"},
				ResourceAttributeNames:  []string{"resAttrA"},
				ResourceAttributeValues: []string{"fooBar 04-res AttrAVal"},
			},
			`
SELECT
	toStartOfInterval(Timestamp, INTERVAL 259200000 nanoseconds) AS Timestamp,
    SeverityNumber,
    arrayCompact(arraySort(groupArray(TraceFlags))) as TraceFlagsArr,
    arrayCompact(arraySort(groupArray(ServiceName))) as ServiceNamesArr,
    count(*) as SumCount
FROM {p0:Identifier}.{p1:Identifier}
WHERE
  ProjectId = {p2:String}
AND ( SpanId IN [ unhex({p3:String}), unhex({p4:String}) ] ) AND ( Fingerprint IN {p5:Array(String)} ) AND ( TraceFlags NOT IN {p6:Array(UInt32)} ) AND ( (Body ILIKE {p7:String}) OR (Body ILIKE {p8:String}) ) AND ( LogAttributes[{p9:String}] = {p10:String}) AND ( ResourceAttributes[{p11:String}] = {p12:String}) AND Timestamp >= {p13:DateTime64(9, 'UTC')} AND Timestamp < {p14:DateTime64(9, 'UTC')}
GROUP BY Timestamp, SeverityNumber
ORDER BY Timestamp ASC`,
			clickhouse.Parameters{
				"p0":  constants.LoggingDatabaseName,
				"p1":  constants.LoggingTableName,
				"p2":  defaultProjectID,
				"p3":  "01AAAABB00000000",
				"p4":  "03AAAABB00000000",
				"p5":  "['fingerprint-0005','fingerprint-0006','fingerprint-0007']",
				"p6":  "[0]",
				"p7":  "%15:Maryna%",
				"p8":  "%02:Maryna%",
				"p9":  "logAttrA",
				"p10": "fooBar 04-log AttrAVal",
				"p11": "resAttrA",
				"p12": "fooBar 04-res AttrAVal",
				"p13": strconv.FormatInt(refTime.Add(-25920*time.Millisecond).UnixNano(), 10),
				"p14": strconv.FormatInt(refTime.UnixNano(), 10),
			},
			259200*time.Microsecond,
		),
	)

})
