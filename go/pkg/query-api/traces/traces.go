package traces

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

func (q *querier) GetTraces(
	ctx context.Context,
	projectID string,
	param *TracesParams,
) ([]*TraceItem, string, error) {
	// NOTE: short term redirection depending on query complexity.
	// First iteration essentially allows no customization.
	// We'll add these in gradually.

	// TODO: querier should use a time interface for unit testing
	refTime := time.Now().UTC()

	simpleFilters := len(param.TraceIDs) > 0 || len(param.NotTraceIDs) > 0

	if !simpleFilters && param.isFilterApplied() {
		return q.getTracesComplex(ctx, projectID, param, refTime)
	}

	return q.getTracesSimple(ctx, projectID, param, refTime)
}

// contains a CTE that will be used to filter span attributes
// and provide the matched span counts
const spanSearch = `
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	%s(?) AS r_start,
	%s(?) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
	count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = ?) AND (Timestamp >= r_start) AND (Timestamp <= r_end)`

// aggregates all spans and joins with the matched_traces CTE above
const traceSelect = `SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
			if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
	any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = ?) AND (Start >= r_start) AND (Start <= r_end)
`

const traceSelectID = `
WITH max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
			if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans
FROM tracing.gl_traces_rolled
WHERE (ProjectId = ?)`

func buildTraceMVQuery(
	projectID string,
	param *TracesParams,
	refTime time.Time,
) *clickhouse.QueryBuilder {
	b := clickhouse.NewQueryBuilder()
	// special case if traceIDs are provided in param but no period params
	if param.Period == "" && len(param.TraceIDs) > 0 {
		b.Build(traceSelectID, projectID)
		addWhereFilter(b, param)
		addGroupBy(b)
		addHavingFilter(b, param)
		addOrderBy(b, param)
		addLimit(b, param)

		return b
	}

	var (
		startTime  int64
		endTime    int64
		timeFunc   = "toStartOfHour"
		traceQuery string
	)
	if !param.StartTime.IsZero() && !param.EndTime.IsZero() {
		startTime = param.StartTime.UnixNano()
		endTime = param.EndTime.UnixNano()
		traceQuery = fmt.Sprintf(spanSearch, timeFunc, timeFunc)
	} else {
		startTime, endTime, _, timeFunc = common.InferTimelines(refTime, param.Period)
		traceQuery = fmt.Sprintf(spanSearch, timeFunc, timeFunc)
	}

	b.Build(
		traceQuery,
		clickhouse.TimestampNano(startTime),
		clickhouse.TimestampNano(endTime),
		projectID,
	)

	addWhereFilter(b, param)
	addAttributeFilters(b, param)
	addGroupBy(b)
	b.Build(`))
	`)
	b.Build(traceSelect, projectID)
	addGroupBy(b)
	addHavingFilter(b, param)
	addOrderBy(b, param)
	addLimit(b, param)

	return b
}

type DBItem struct {
	TraceID          string        `ch:"traceID" json:"trace_id"`
	TraceStart       time.Time     `ch:"TraceStart"`
	TraceDuration    time.Duration `ch:"TraceDuration"`
	TotalSpans       uint64        `ch:"total_spans"`
	Values           [][]any       `ch:"spans"`
	MatchedSpanCount uint64        `ch:"MatchedSpanCount"`
}

// Wrapper for TraceItem to hold spans temporarily for page processing
type traceItem struct {
	*TraceItem
	spans []spanItem
}

type spanItem struct {
	Timestamp time.Time
	SpanID    string
}

//nolint:funlen,cyclop
func (q *querier) getTracesComplex(
	ctx context.Context,
	projectID string,
	param *TracesParams,
	refTime time.Time,
) ([]*TraceItem, string, error) {
	q.logger.Debug("got param", zap.Any("params", param))
	results := []traceItem{}
	builder := buildTraceMVQuery(projectID, param, refTime)

	rows, err := q.db.Query(
		builder.Context(ctx),
		builder.SQL(),
	)
	if err != nil {
		return nil, "", fmt.Errorf("query traces: %w", err)
	}

	for rows.Next() {
		item := &DBItem{}
		err = rows.ScanStruct(item)
		if err != nil {
			return nil, "", fmt.Errorf("scan results from clickhouse: %w", err)
		}

		trace := traceItem{
			TraceItem: &TraceItem{
				TraceID:          item.TraceID,
				TotalSpans:       item.TotalSpans,
				Timestamp:        item.TraceStart,
				TimestampNano:    item.TraceStart.UnixNano(),
				DurationNano:     uint64(item.TraceDuration),
				MatchedSpanCount: item.MatchedSpanCount,
			},
			spans: make([]spanItem, len(item.Values)),
		}

		//nolint:errcheck
		for i, v := range item.Values {
			// TODO(Arun): Investigate if we can improve marshaling strategy.
			// This is a straight pass from DB results and should be fastest.
			spanItem := spanItem{}
			spanItem.SpanID = v[0].(string)
			spanItem.Timestamp = v[1].(time.Time)

			trace.spans[i] = spanItem

			if i == 0 {
				serviceName := v[2].(string)
				operation := v[3].(string)
				statusCode := v[4].(string)

				// The first span will always be root span as they are ordered by start timestamp.
				trace.ServiceName = serviceName
				trace.Operation = operation
				trace.StatusCode = statusCode

				// if parentSpanID for the first item is not "" then we don't have a root span
				parentSpanID := v[6].(string)
				if parentSpanID != "" {
					trace.InProgress = true
				}
			}

			// NOTE(prozlach) Type returned depends on the version of CH.
			switch hasError := v[8].(type) {
			case bool:
				if hasError {
					trace.ErrorSpanCount += 1
				}
			case uint8:
				if hasError == 1 {
					trace.ErrorSpanCount += 1
				}
			}
		}
		// Cases where direct spans are fetched so CTE is not used, instead just return total spans.
		if trace.MatchedSpanCount == 0 {
			trace.MatchedSpanCount = trace.TotalSpans
		}

		// Some heuristics to detect if the trace is in progress.
		// See discussion on https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2692#note_1791784111.
		if trace.MatchedSpanCount > trace.TotalSpans {
			trace.InProgress = true
		} else if trace.MatchedSpanCount != trace.TotalSpans && !param.isFilterApplied() {
			// Validate if there are any filters
			// In case of no filters, these should be the same.
			trace.InProgress = true
		}

		results = append(results, trace)
	}

	r := make([]*TraceItem, len(results))
	for i, v := range results {
		r[i] = v.TraceItem
	}

	nextPageToken, err := getNextPageToken(r, param)
	if err != nil {
		return nil, "", fmt.Errorf("encoding page Cursor: %w", err)
	}

	return r, nextPageToken, nil
}

func addWhereFilter(b *clickhouse.QueryBuilder, param *TracesParams) {
	if param.TraceIDs != nil {
		appliedFunc, transformed := clickhouse.FuncMapToString("UUIDStringToNum", param.TraceIDs)

		b.Build("AND ( TraceId IN "+appliedFunc+" )", transformed...)
	}

	if param.ServiceNames != nil {
		b.Build(
			"AND ( ServiceName IN ? )",
			clickhouse.Array[string](param.ServiceNames),
		)
	}
	if param.NotServiceNames != nil {
		b.Build(
			"AND ( ServiceName NOT IN ? )",
			clickhouse.Array[string](param.NotServiceNames),
		)
	}
	if param.Operations != nil {
		b.Build(
			"AND ( SpanName IN ? )",
			clickhouse.Array[string](param.Operations),
		)
	}
	if param.NotOperations != nil {
		b.Build(
			"AND ( SpanName NOT IN ? )",
			clickhouse.Array[string](param.NotOperations),
		)
	}
	if param.LtDuration != 0 {
		b.Build("AND ( Duration <= ? )", param.LtDuration)
	}
	if param.GtDuration != 0 {
		b.Build("AND ( Duration >= ? )", param.GtDuration)
	}
	if param.Statuses != nil {
		b.Build(
			"AND ( StatusCode IN ? )",
			clickhouse.Array[string](param.Statuses),
		)
	}
	if param.NotStatuses != nil {
		b.Build(
			"AND ( StatusCode NOT IN ? )",
			clickhouse.Array[string](param.NotStatuses),
		)
	}
}

// separate from the where filter as it's only applied to the main table
func addAttributeFilters(b *clickhouse.QueryBuilder, param *TracesParams) {
	for i, n := range param.AttributeNames {
		v := param.AttributeValues[i]
		b.Build("AND ( ResourceAttributes[?] = ? OR SpanAttributes[?] = ? )", n, v, n, v)
	}
	b.Build(`
	`)
}

func addGroupBy(b *clickhouse.QueryBuilder) {
	b.Build(`GROUP BY TraceId
	 `)
}

func addOrderBy(b *clickhouse.QueryBuilder, param *TracesParams) {
	switch param.Sort {
	case SortDurationDesc:
		b.Build(`ORDER BY TraceDuration DESC, TraceId DESC
	`)
	case SortDurationAsc:
		b.Build(`ORDER BY TraceDuration ASC, TraceId ASC
	`)
	case SortTimestampAsc:
		b.Build(`ORDER BY TraceStart ASC, TraceId ASC
	`)
	default:
		b.Build(`ORDER BY TraceStart DESC, TraceId DESC
	`)
	}
}

func addHavingFilter(b *clickhouse.QueryBuilder, param *TracesParams) {
	// last seen trace ID - might just be empty
	// TraceIds are used in sorting above (they are numbers)
	// so we get consistent pagination.
	lastTraceID := ""
	if param.Cursor != nil {
		lastTraceID = param.Cursor.LastSeenTraceID
	}

	if param.Cursor != nil && param.Cursor.LastSeenTimestamp > 0 {
		direction := "<"
		if param.Sort == SortTimestampAsc {
			direction = ">"
		}
		tvp := clickhouse.TypeValPair{
			ParamType: "DateTime64(9)",
			ParamVal:  strconv.FormatInt(int64(param.Cursor.LastSeenTimestamp), 10),
		}
		b.Build(fmt.Sprintf("HAVING (TraceStart %s ?) OR (TraceStart = ? AND TraceId %[1]s UUIDStringToNum(?))", direction),
			tvp, tvp, lastTraceID)
		b.Build(`
	`)

		return
	}

	if param.Cursor != nil && param.Cursor.LastSeenDuration > 0 {
		direction := "<"
		if param.Sort == SortDurationAsc {
			direction = ">"
		}
		b.Build(
			fmt.Sprintf("HAVING (TraceDuration %s ?) OR (TraceDuration = ? AND TraceId %[1]s UUIDStringToNum(?))", direction),
			param.Cursor.LastSeenDuration, param.Cursor.LastSeenDuration, lastTraceID)
		b.Build(`
	`)
	}
}

func addLimit(b *clickhouse.QueryBuilder, param *TracesParams) {
	const defaultPageSize = 100
	const queryLimit = `LIMIT %d
	`

	pageSize := int64(defaultPageSize)
	if param.PageSize > 0 {
		pageSize = param.PageSize
	}
	b.Build(fmt.Sprintf(queryLimit, pageSize))
}

// getNextPageToken returns a token that can be used to fetch the next page of results.
// For either timestamp or duration based queries, the token contains the last seen
// trace id as this is a numeric value that can be compared against other trace ids.
// All queries return results sorted by the sorting param, then by the trace id, in
// the same direction, e.g. ORDER BY Duration DESC, TraceId DESC.
// To get the next page, the query can then use the opposite condition, e.g.
// WHERE Duration > last_seen_duration OR (Duration = last_seen_duration AND TraceId > last_seen_trace_id).
func getNextPageToken(results []*TraceItem, param *TracesParams) (string, error) {
	n := len(results)
	if n == 0 {
		return "", nil
	}

	lastRes := results[n-1]
	// trace summary approach, all we need is one trace ID
	page := &Page{
		LastSeenTraceID: lastRes.TraceID,
	}

	switch param.Sort {
	case SortDurationDesc, SortDurationAsc:
		page.LastSeenDuration = lastRes.DurationNano
	default:
		page.LastSeenTimestamp = uint64(lastRes.TimestampNano)
	}

	return page.EncodePage()
}

func (q *querier) getTracesSimple(
	ctx context.Context,
	projectID string,
	param *TracesParams,
	refTime time.Time,
) ([]*TraceItem, string, error) {
	sb := param.buildSimpleTracesQuery(projectID, refTime)

	var traces []struct {
		TraceID          string    `ch:"TraceId"`
		Start            time.Time `ch:"TStart"`
		Duration         int64     `ch:"TDuration"`
		SpanCount        uint64    `ch:"SpanCount"`
		ErrorCount       uint64    `ch:"ErrorCount"`
		RootServiceName  string    `ch:"RootServiceName"`
		RootSpanName     string    `ch:"RootSpanName"`
		FirstServiceName string    `ch:"FirstServiceName"`
		FirstSpanName    string    `ch:"FirstSpanName"`
	}

	if err := q.db.Select(sb.Context(ctx), &traces, sb.SQL()); err != nil {
		return nil, "", fmt.Errorf("query traces simple: %w", err)
	}

	res := make([]*TraceItem, len(traces))
	for i, t := range traces {
		serviceName := t.RootServiceName
		if serviceName == "" {
			serviceName = t.FirstServiceName
		}
		spanName := t.RootSpanName
		inProgress := false
		if spanName == "" {
			spanName = t.FirstSpanName
			inProgress = true
		}
		status := "STATUS_CODE_OK"
		if t.ErrorCount > 0 {
			status = "STATUS_CODE_ERROR"
		}

		res[i] = &TraceItem{
			TraceID:       t.TraceID,
			Timestamp:     t.Start,
			TimestampNano: t.Start.UnixNano(),
			DurationNano:  uint64(t.Duration),
			ServiceName:   serviceName,
			Operation:     spanName,
			// status is not used in the frontend
			StatusCode:       status,
			TotalSpans:       t.SpanCount,
			MatchedSpanCount: t.SpanCount,
			ErrorSpanCount:   t.ErrorCount,
			InProgress:       inProgress,
		}
	}

	nextPageToken, err := getNextPageToken(res, param)
	if err != nil {
		return nil, "", fmt.Errorf("encode next page token: %w", err)
	}

	return res, nextPageToken, nil
}

func (r *TracesParams) buildSimpleTracesQuery(projectID string, refTime time.Time) *clickhouse.QueryBuilder {
	orderBy, orderDir, orderOp := r.orderBy()
	startTime, endTime := r.timeRange(refTime)
	qb := clickhouse.NewQueryBuilder().WithParams(map[string]clickhouse.Literal{
		"projectId":    clickhouse.String(projectID),
		"pageSize":     clickhouse.Value(r.pageSize()),
		"summaryTable": clickhouse.Identifier(constants.TracingSummaryTableName),
		"orderBy":      clickhouse.Identifier(orderBy),
		"startTime":    clickhouse.Value(startTime),
		"endTime":      clickhouse.Value(endTime),
	})

	qb.Build(`
SELECT
	UUIDNumToString(TraceId) AS TraceId,
	min(Start) AS TStart,
	max(Duration) AS TDuration,
	sum(SpanCount) AS SpanCount,
	sum(ErrorCount) AS ErrorCount,
	anyIfMerge(RootServiceName) AS RootServiceName,
	anyIfMerge(RootSpanName) AS RootSpanName,
	argMinMerge(FirstServiceName) AS FirstServiceName,
	argMinMerge(FirstSpanName) AS FirstSpanName
FROM {summaryTable:Identifier}
WHERE ProjectId = {projectId:String}
AND Start >= {startTime:DateTime64(9)}
AND Start <= {endTime:DateTime64(9)}`)

	if len(r.TraceIDs) > 0 {
		qb.Build(`
AND {summaryTable:Identifier}.TraceId IN {traceIds:Array(FixedString(16))}`).
			WithParam("traceIds", clickhouse.UUIDsAsFixedStringArray(r.traceIDs))
	}

	if len(r.NotTraceIDs) > 0 {
		qb.Build(`
AND {summaryTable:Identifier}.TraceId NOT IN {notTraceIds:Array(FixedString(16))}`).
			WithParam("notTraceIds", clickhouse.UUIDsAsFixedStringArray(r.notTraceIDs))
	}

	qb.Build(`
GROUP BY TraceId`)

	if r.Cursor != nil {
		qb.Build(`
	HAVING`).WithParam("lastSeenTraceId", clickhouse.Value(r.Cursor.LastSeenTraceID))

		if r.Cursor.LastSeenTimestamp > 0 {
			qb.Build(fmt.Sprintf(`
	TStart %s {lastSeenTimestamp:DateTime64(9)}
	OR (TStart = {lastSeenTimestamp:DateTime64(9)}
	AND TraceId %[1]s UUIDStringToNum({lastSeenTraceId:String}))`, orderOp)).WithParam(
				"lastSeenTimestamp", clickhouse.TimestampNano(r.Cursor.LastSeenTimestamp))
		} else if r.Cursor.LastSeenDuration > 0 {
			qb.Build(fmt.Sprintf(`
	TDuration %s {lastSeenDuration:Int64}
	OR (TDuration = {lastSeenDuration:Int64}
	AND TraceId %[1]s UUIDStringToNum({lastSeenTraceId:String}))`, orderOp)).WithParam(
				"lastSeenDuration", clickhouse.Value(r.Cursor.LastSeenDuration))
		}
	}

	qb.Build(fmt.Sprintf(`
ORDER BY {orderBy:Identifier} %[1]s, TraceId %[1]s
LIMIT {pageSize:Int64}`, orderDir))

	return qb
}
