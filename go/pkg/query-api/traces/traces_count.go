package traces

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

type CountDBItem struct {
	Interval          time.Time `ch:"Interval"`
	TraceCount        uint64    `ch:"TraceCount"`
	DurationQuantiles []float64 `ch:"DurationQuantiles"`
}

type ErrDBItem struct {
	Interval time.Time `ch:"Interval"`
	ErrCount uint64    `ch:"TraceCount"`
}

type MVItem struct {
	Interval          time.Time `ch:"AggTime"`
	TraceCount        int64     `ch:"TraceCount"`
	ErrCount          int64     `ch:"ErrCount"`
	DurationQuantiles []float64 `ch:"DurationQuantiles"`
}

const matchedTracesSQL = `
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
    %s(?) AS r_start,
    %s(?) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = ?)
`

const traceCountSQL = `
SELECT
    %s(Start) AS Interval,
    quantiles(0.95,0.90,0.75,0.5)(Duration) AS DurationQuantiles,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = ? AND ParentSpanId = '' AND TraceId IN matched_traces AND Interval >= %s(?)
`

const errCountSQL = `
SELECT
    %s(Start) AS Interval,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = ? AND ParentSpanId = '' AND TraceId IN matched_traces AND Interval >= %s(?)
`

const traceCountMVSQL = `
WITH
    %s({startTime:DateTime64(9)}) AS r_start,
    %[1]s({endTime:DateTime64(9)}) AS r_end
SELECT
    %[1]s(AggTime) AS AggTime,
    -- groupUniqArrayArray(ServiceNames),
    sum(TraceCount) AS TraceCount,
    sum(ErrCount) AS ErrCount,
    quantilesMerge(0.95,0.9,0.75,0.5)(DurationQuantiles) AS DurationQuantiles
FROM {aggTable:Identifier}
WHERE (AggTime >= r_start) AND (AggTime <= r_end) AND (ProjectId = {projectId:String})
GROUP BY
    ProjectId,
    AggTime
ORDER BY AggTime
`

func (q *querier) GetTraceAnalytics(
	ctx context.Context,
	projectID string,
	params *TracesParams,
) (*TracesCountResult, error) {
	q.logger.Debug("got param for rates", zap.Any("params", params))

	used, mvBuilder, stepSeconds := useAggregateMV(projectID, params, time.Now().UTC())
	if used {
		return q.getTraceAnalyticsFromMV(ctx, projectID, stepSeconds, mvBuilder)
	}

	builder, stepSeconds := buildCountQuery(projectID, params, time.Now().UTC())

	var (
		mappedResults = make(map[uint64]*IntervalItem)
		results       []*IntervalItem
	)
	rows, err := q.db.Query(
		builder.Context(ctx),
		builder.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("query count for traces: %w", err)
	}

	for rows.Next() {
		item := &CountDBItem{}
		err = rows.ScanStruct(item)
		if err != nil {
			return nil, fmt.Errorf("scan results from clickhouse: %w", err)
		}

		if len(item.DurationQuantiles) > 0 && len(item.DurationQuantiles) != 4 {
			return nil, fmt.Errorf("expect `DurationQuantiles` to be of length 4")
		}

		res := &IntervalItem{
			Interval:   uint64(item.Interval.Unix()),
			TraceCount: item.TraceCount,
			TraceRate:  float64(item.TraceCount) / float64(stepSeconds),
		}

		if item.DurationQuantiles != nil {
			res.P95Duration = item.DurationQuantiles[0]
			res.P90Duration = item.DurationQuantiles[1]
			res.P75Duration = item.DurationQuantiles[2]
			res.P50Duration = item.DurationQuantiles[3]
		}
		mappedResults[res.Interval] = res
		results = append(results, res)
	}

	errBuilder := buildErrorCountQuery(projectID, params, time.Now().UTC())
	errRows, err := q.db.Query(
		errBuilder.Context(ctx),
		errBuilder.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("query err count for traces: %w", err)
	}

	for errRows.Next() {
		item := &ErrDBItem{}
		err = errRows.ScanStruct(item)
		if err != nil {
			return nil, fmt.Errorf("scan results from clickhouse: %w", err)
		}
		interval := uint64(item.Interval.Unix())

		// This should always be true because if there is an error count then there should be a trace count too.
		if v, ok := mappedResults[interval]; ok {
			v.ErrorCount = item.ErrCount
			if v.TraceCount != 0 {
				v.ErrorRate = (float64(item.ErrCount) / float64(v.TraceCount)) * 100
			}
		} else {
			q.logger.Error(
				"err count aggregate present for interval without an existing count aggregate",
				zap.Any("item", item),
			)
		}
	}
	return &TracesCountResult{Results: results, ProjectID: projectID}, nil
}

func useAggregateMV(
	projectID string,
	params *TracesParams,
	refTime time.Time,
) (bool, *clickhouse.QueryBuilder, int64) {
	var (
		startTime   int64
		endTime     int64
		stepSeconds int64
		timeFunc    = "toStartOfHour"
		query       string
		b           = clickhouse.NewQueryBuilder()
	)

	if params.isFilterApplied() {
		return false, nil, 0
	}

	if !params.StartTime.IsZero() && !params.EndTime.IsZero() {
		startTime = params.StartTime.UnixNano()
		endTime = params.EndTime.UnixNano()
		stepSeconds = int64(params.EndTime.Sub(params.StartTime).Seconds())
	} else {
		startTime, endTime, stepSeconds, timeFunc = common.InferTimelines(refTime, params.Period)
	}

	if stepSeconds < 60*60 {
		return false, nil, 0
	}

	query = fmt.Sprintf(
		traceCountMVSQL,
		timeFunc,
	)

	b.WithParams(map[string]clickhouse.Literal{
		"projectId": clickhouse.String(projectID),
		"startTime": clickhouse.TimestampNano(startTime),
		"endTime":   clickhouse.TimestampNano(endTime),
		"aggTable":  clickhouse.Identifier(constants.TracingRED1hMVTableName),
	})
	b.Build(query)

	return true, b, stepSeconds
}

func buildMatchedTracesQuery(
	projectID string,
	params *TracesParams,
	refTime time.Time,
	onlyErrors bool,
) (*clickhouse.QueryBuilder, string, int64, int64) {
	b := clickhouse.NewQueryBuilder()
	var (
		startTime    int64
		endTime      int64
		startTimeSec int64
		timeFunc     = "toStartOfHour"
		query        string
		stepSeconds  int64
	)
	if !params.StartTime.IsZero() && !params.EndTime.IsZero() {
		startTime = params.StartTime.UnixNano()
		endTime = params.EndTime.UnixNano()
		stepSeconds = int64(params.EndTime.Sub(params.StartTime).Seconds())
		startTimeSec = params.StartTime.Unix()
		query = fmt.Sprintf(matchedTracesSQL, timeFunc, timeFunc)
	} else {
		startTimeSec, endTime, stepSeconds, timeFunc = common.InferTimelines(refTime, params.Period)
		query = fmt.Sprintf(matchedTracesSQL, timeFunc, timeFunc)
		startTime = startTimeSec
	}
	b.Build(
		query,
		clickhouse.TimestampNano(startTime),
		clickhouse.TimestampNano(endTime),
		projectID,
	)

	addWhereFilter(b, params)
	addAttributeFilters(b, params)
	if onlyErrors {
		b.Build(`AND StatusCode = 'STATUS_CODE_ERROR'
        `)
	}
	addGroupBy(b)
	b.Build(`))
    `)

	return b, timeFunc, stepSeconds, startTimeSec
}

func buildCountQuery(projectID string, params *TracesParams, refTime time.Time) (*clickhouse.QueryBuilder, int64) {
	b, timeFunc, stepSeconds, startTime := buildMatchedTracesQuery(projectID, params, refTime, false)
	query := fmt.Sprintf(traceCountSQL, timeFunc, timeFunc)
	b.Build(query, projectID, clickhouse.TimestampNano(startTime))
	addGroupByForCount(b)
	addOrderByForCount(b)

	return b, stepSeconds
}

func addGroupByForCount(b *clickhouse.QueryBuilder) {
	b.Build(`GROUP BY ProjectId, Interval
    `)
}

func addOrderByForCount(b *clickhouse.QueryBuilder) {
	b.Build(`ORDER BY Interval
`)
}

func buildErrorCountQuery(projectID string, params *TracesParams, refTime time.Time) *clickhouse.QueryBuilder {
	b, timeFunc, _, startTime := buildMatchedTracesQuery(projectID, params, refTime, true)
	query := fmt.Sprintf(errCountSQL, timeFunc, timeFunc)
	b.Build(query, projectID, clickhouse.TimestampNano(startTime))
	addGroupByForCount(b)
	addOrderByForCount(b)

	return b
}

func (q *querier) getTraceAnalyticsFromMV(
	ctx context.Context,
	projectID string,
	stepSeconds int64,
	builder *clickhouse.QueryBuilder,
) (*TracesCountResult, error) {
	var (
		rows    driver.Rows
		err     error
		results []*IntervalItem
	)
	rows, err = q.db.Query(
		builder.Context(ctx),
		builder.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("query count for traces: %w", err)
	}

	for rows.Next() {
		item := &MVItem{}
		err = rows.ScanStruct(item)
		if err != nil {
			return nil, fmt.Errorf("scan results from clickhouse: %w", err)
		}

		if len(item.DurationQuantiles) > 0 && len(item.DurationQuantiles) != 4 {
			return nil, fmt.Errorf("expect `DurationQuantiles` to be of length 4")
		}

		res := &IntervalItem{
			Interval:   uint64(item.Interval.Unix()),
			TraceCount: uint64(item.TraceCount),
			ErrorCount: uint64(item.ErrCount),
			TraceRate:  float64(item.TraceCount) / float64(stepSeconds),
			ErrorRate:  (float64(item.ErrCount) / float64(item.TraceCount)) * 100,
		}
		if item.DurationQuantiles != nil {
			res.P95Duration = item.DurationQuantiles[0]
			res.P90Duration = item.DurationQuantiles[1]
			res.P75Duration = item.DurationQuantiles[2]
			res.P50Duration = item.DurationQuantiles[3]
		}
		results = append(results, res)
	}

	return &TracesCountResult{Results: results, ProjectID: projectID}, nil
}
