package traces

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

// Page holds the details of the page token needed for the next page.
// Requests requesting the next pages should include this Cursor in the page token parameter.
type Page struct {
	// LastSeenTimestamp is in unix nanoseconds
	LastSeenTimestamp uint64 `json:"last_seen_timestamp,omitempty"`
	// LastSeenDuration is in nanoseconds
	LastSeenDuration uint64 `json:"last_seen_duration,omitempty"`
	// LastSeenTraceID is the last seen trace ID from the page
	LastSeenTraceID string `json:"last_seen_trace_id"`
}

func DecodePage(rawCursor string) (*Page, error) {
	result := &Page{}

	raw, err := base64.StdEncoding.DecodeString(rawCursor)
	if err != nil {
		return nil, fmt.Errorf("cursor decode: %w", err)
	}

	err = json.Unmarshal(raw, result)
	if err != nil {
		return nil, fmt.Errorf("unmarshal Cursor: %w", err)
	}

	return result, nil
}

func (p *Page) EncodePage() (string, error) {
	bts, err := json.Marshal(p)
	if err != nil {
		return "", fmt.Errorf("encode page :%w", err)
	}

	return base64.StdEncoding.EncodeToString(bts), nil
}
