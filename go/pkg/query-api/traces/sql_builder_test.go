package traces

import (
	"strconv"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/google/go-cmp/cmp"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Context("traces query builder", func() {
	var (
		refTime          = time.Now().UTC()
		defaultStartTime = refTime.Add(-1 * 24 * time.Hour).UnixNano()
		defaultEndTime   = refTime.Add(1 * time.Hour).UnixNano()
	)

	DescribeTable("builds queries",
		func(projectID string, params *TracesParams, expected string, args clickhouse.Parameters) {
			builder := buildTraceMVQuery(projectID, params, refTime)
			err := testutils.CompareSQLStrings(builder.SQL(), expected)
			Expect(err).ToNot(HaveOccurred())
			Expect(builder.StringifyParams()).To(Equal(args))
		},
		Entry(
			"list trace query with default period and no filters",
			"1",
			&TracesParams{},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfHour({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfHour({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end)
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p3:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(defaultStartTime, 10),
				"p1": strconv.FormatInt(defaultEndTime, 10),
				"p2": "1",
				"p3": "1",
			},
		),
		Entry(
			"list trace query with no period and 1 trace ID",
			"1",
			&TracesParams{
				TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
			},
			`
WITH max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) as TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
			if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
			if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans
FROM tracing.gl_traces_rolled
WHERE (ProjectId = {p0:String}) AND ( TraceId IN [ UUIDStringToNum({p1:String}) ] ) GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
	`,
			clickhouse.Parameters{
				"p0": "1",
				"p1": "b7e641ba-918b-3147-2cba-e93b13e7de2a",
			},
		),
		Entry(
			"list trace query with custom period and list of traceID",
			"1",
			&TracesParams{
				TraceIDs: []string{
					"b7e641ba-918b-3147-2cba-e93b13e7de2a",
					"b7e641ba-918b-3147-2dss-e13c13edd1x0",
				},
				Period: "1m",
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( TraceId IN [ UUIDStringToNum({p3:String}), UUIDStringToNum({p4:String}) ] )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "b7e641ba-918b-3147-2cba-e93b13e7de2a",
				"p4": "b7e641ba-918b-3147-2dss-e13c13edd1x0",
				"p5": "1",
			},
		),
		Entry(
			"list trace query with custom period and 1 service and operations",
			"1",
			&TracesParams{
				ServiceNames: []string{"service-1"},
				Operations:   []string{"op-1", "op-2"},
				Period:       "1m",
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( ServiceName IN {p3:Array(String)} ) AND ( SpanName IN {p4:Array(String)} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "['service-1']",
				"p4": "['op-1','op-2']",
				"p5": "1",
			},
		),
		Entry(
			"list trace query with custom period and multiple service and operations",
			"1",
			&TracesParams{
				ServiceNames: []string{"service-1", "service-2"},
				Operations:   []string{"op-1", "op-2"},
				Period:       "1m",
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( ServiceName IN {p3:Array(String)} ) AND ( SpanName IN {p4:Array(String)} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "['service-1','service-2']",
				"p4": "['op-1','op-2']",
				"p5": "1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter",
			"1",
			&TracesParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( Duration <= {p3:Int64} ) AND ( Duration >= {p4:Int64} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "300",
				"p4": "500",
				"p5": "1",
			},
		),
		Entry(
			"list trace query with custom period of 12h",
			"1",
			&TracesParams{
				Period: "12h",
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfHour({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfHour({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end)
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p3:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*12*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Hour).UnixNano(), 10),
				"p2": "1",
				"p3": "1",
			},
		),
		Entry(
			"list trace query with custom period of 4h",
			"1",
			&TracesParams{
				Period: "4h",
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end)
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p3:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*4*time.Hour).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "1",
			},
		),
		Entry(
			"list trace query with custom period of 5m",
			"1",
			&TracesParams{
				Period: "5m",
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end)
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p3:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter and page token",
			"1",
			&TracesParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				Cursor: &Page{
					LastSeenTimestamp: uint64(refTime.UnixNano()),
					LastSeenTraceID:   "b7e641ba-918b-3147-2cba-e93b13e7de2a",
				},
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( Duration <= {p3:Int64} ) AND ( Duration >= {p4:Int64} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
HAVING (TraceStart < {p6:DateTime64(9)}) OR (TraceStart = {p7:DateTime64(9)} AND TraceId < UUIDStringToNum({p8:String}))
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 50
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "300",
				"p4": "500",
				"p5": "1",
				"p6": strconv.FormatInt(refTime.UnixNano(), 10),
				"p7": strconv.FormatInt(refTime.UnixNano(), 10),
				"p8": "b7e641ba-918b-3147-2cba-e93b13e7de2a",
			},
		),
		Entry(
			"list trace query with custom period of 5m and sort by duration desc",
			"1",
			&TracesParams{
				Period: "5m",
				Sort:   SortDurationDesc,
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end)
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p3:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceDuration DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "1",
			},
		),
		Entry(
			"list trace query with custom period and duration filter, sort by duration desc and page token",
			"1",
			&TracesParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				Sort:       SortDurationDesc,
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				Cursor: &Page{
					LastSeenDuration: 123456789,
					LastSeenTraceID:  "b7e641ba-918b-3147-2cba-e93b13e7de2a",
				},
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( Duration <= {p3:Int64} ) AND ( Duration >= {p4:Int64} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
HAVING (TraceDuration < {p6:UInt64}) OR (TraceDuration = {p7:UInt64} AND TraceId < UUIDStringToNum({p8:String}))
ORDER BY TraceDuration DESC, TraceId DESC
LIMIT 50
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "300",
				"p4": "500",
				"p5": "1",
				"p6": strconv.FormatInt(123456789, 10),
				"p7": strconv.FormatInt(123456789, 10),
				"p8": "b7e641ba-918b-3147-2cba-e93b13e7de2a",
			},
		),
		Entry(
			"list trace query with custom period and duration filter, sort by duration asc and page token",
			"1",
			&TracesParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				Sort:       SortDurationAsc,
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				Cursor: &Page{
					LastSeenDuration: 123456789,
					LastSeenTraceID:  "b7e641ba-918b-3147-2cba-e93b13e7de2a",
				},
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( Duration <= {p3:Int64} ) AND ( Duration >= {p4:Int64} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
HAVING (TraceDuration > {p6:UInt64}) OR (TraceDuration = {p7:UInt64} AND TraceId > UUIDStringToNum({p8:String}))
ORDER BY TraceDuration ASC, TraceId ASC
LIMIT 50
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "300",
				"p4": "500",
				"p5": "1",
				"p6": strconv.FormatInt(123456789, 10),
				"p7": strconv.FormatInt(123456789, 10),
				"p8": "b7e641ba-918b-3147-2cba-e93b13e7de2a",
			},
		),
		Entry(
			"list trace query with custom period and duration filter, sort by timestamp asc and page token",
			"1",
			&TracesParams{
				LtDuration: 300,
				GtDuration: 500,
				Period:     "1m",
				Sort:       SortTimestampAsc,
				PageSize:   50,
				PageToken:  "eyJsYXN0X3NlZW5fdGltZXN0YW1wIjoiMjAyMy0wOS0wNCAxMToyNToyMy4xNTY5MjMwMDAifQ==",
				Cursor: &Page{
					LastSeenTimestamp: uint64(refTime.UnixNano()),
					LastSeenTraceID:   "b7e641ba-918b-3147-2cba-e93b13e7de2a",
				},
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( Duration <= {p3:Int64} ) AND ( Duration >= {p4:Int64} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p5:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
HAVING (TraceStart > {p6:DateTime64(9)}) OR (TraceStart = {p7:DateTime64(9)} AND TraceId > UUIDStringToNum({p8:String}))
ORDER BY TraceStart ASC, TraceId ASC
LIMIT 50
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*1*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "300",
				"p4": "500",
				"p5": "1",
				"p6": strconv.FormatInt(refTime.UnixNano(), 10),
				"p7": strconv.FormatInt(refTime.UnixNano(), 10),
				"p8": "b7e641ba-918b-3147-2cba-e93b13e7de2a",
			},
		),
		Entry(
			"list trace query with custom attributes searched by name and value",
			"1",
			&TracesParams{
				AttributeNames:  []string{"foo", "bar"},
				AttributeValues: []string{"baz", "qux"},
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfHour({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfHour({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end) AND ( ResourceAttributes[{p3:String}] = {p4:String} OR SpanAttributes[{p5:String}] = {p6:String} ) AND ( ResourceAttributes[{p7:String}] = {p8:String} OR SpanAttributes[{p9:String}] = {p10:String} )
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p11:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0":  strconv.FormatInt(defaultStartTime, 10),
				"p1":  strconv.FormatInt(defaultEndTime, 10),
				"p2":  "1",
				"p3":  "foo",
				"p4":  "baz",
				"p5":  "foo",
				"p6":  "baz",
				"p7":  "bar",
				"p8":  "qux",
				"p9":  "bar",
				"p10": "qux",
				"p11": "1",
			},
		),
		Entry(
			"list trace query with custom range params",
			"1",
			&TracesParams{
				StartTime: time.Date(2023, 11, 02, 0, 0, 0, 0, time.UTC),
				EndTime:   time.Date(2023, 11, 20, 0, 0, 0, 0, time.UTC),
			},
			`
WITH
	max(timestamp_add(Start, INTERVAL Duration NANOSECOND)) AS trace_end,
	toStartOfHour({p0:DateTime64(9, 'UTC')}) AS r_start,
    toStartOfHour({p1:DateTime64(9, 'UTC')}) AS r_end,
	matched_traces AS (
SELECT TraceId, matched_span_count FROM (
SELECT
	TraceId,
    count() AS matched_span_count
FROM tracing.gl_traces_main
WHERE (ProjectId = {p2:String}) AND (Timestamp >= r_start) AND (Timestamp <= r_end)
GROUP BY TraceId
))
SELECT
	UUIDNumToString(TraceId) AS traceID,
	min(Start) AS TraceStart,
	toUnixTimestamp64Nano(trace_end) - toUnixTimestamp64Nano(TraceStart) AS TraceDuration,
	arraySort((x) -> (x.8, x.2), groupArray(
		(
			hex(SpanId) AS spanID,
			Start AS start,
			ServiceName AS serviceName,
			SpanName AS operation,
			StatusCode AS statusCode,
			Duration AS duration,
			if(ParentSpanId = '', '', hex(ParentSpanId)) as parentSpanID,
            if(ParentSpanId = '', 1, 2) as rootSpanOrderKey,
            if(StatusCode = 'STATUS_CODE_ERROR', true, false) AS hasError
		)
	)) AS spans,
	length(spans) AS total_spans,
    any(matched_traces.matched_span_count) AS MatchedSpanCount
FROM tracing.gl_traces_rolled
INNER JOIN matched_traces ON matched_traces.TraceId = gl_traces_rolled.TraceId
WHERE (ProjectId = {p3:String}) AND (Start >= r_start) AND (Start <= r_end)
GROUP BY TraceId
ORDER BY TraceStart DESC, TraceId DESC
LIMIT 100
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(time.Date(2023, 11, 02, 0, 0, 0, 0, time.UTC).UnixNano(), 10),
				"p1": strconv.FormatInt(time.Date(2023, 11, 20, 0, 0, 0, 0, time.UTC).UnixNano(), 10),
				"p2": "1",
				"p3": "1",
			},
		),
	)
})

var _ = Context("traces rate count query builder", func() {
	var (
		refTime          = time.Now().UTC()
		defaultStartTime = refTime.Add(-1 * 24 * time.Hour).UnixNano()
		defaultEndTime   = refTime.Add(1 * time.Hour).UnixNano()
	)

	DescribeTable("builds queries",
		func(projectID string, params *TracesParams, expected string, args clickhouse.Parameters) {
			builder, _ := buildCountQuery(projectID, params, refTime)
			err := testutils.CompareSQLStrings(builder.SQL(), expected)
			if err != nil {
				GinkgoWriter.Print("diff: ", cmp.Diff(builder.SQL(), expected))
			}
			Expect(err).ToNot(HaveOccurred())
			Expect(builder.StringifyParams()).To(Equal(args))
		},
		Entry(
			"trace count query with default period and no filters",
			"1",
			&TracesParams{},
			`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
	toStartOfHour({p0:DateTime64(9, 'UTC')}) AS r_start,
	toStartOfHour({p1:DateTime64(9, 'UTC')}) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = {p2:String})

    GROUP BY TraceId
))

SELECT
    toStartOfHour(Start) AS Interval,
    quantiles(0.95,0.90,0.75,0.5)(Duration) AS DurationQuantiles,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = {p3:String} AND ParentSpanId = '' AND TraceId IN matched_traces AND Interval >= toStartOfHour({p4:DateTime64(9, 'UTC')})
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(defaultStartTime, 10),
				"p1": strconv.FormatInt(defaultEndTime, 10),
				"p2": "1",
				"p3": "1",
				"p4": strconv.FormatInt(defaultStartTime, 10),
			},
		),
		Entry(
			"trace count query with 5m period and no filters",
			"1",
			&TracesParams{
				Period: "5m",
			},
			`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
	toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = {p2:String})

    GROUP BY TraceId
))

SELECT
    toStartOfMinute(Start) AS Interval,
    quantiles(0.95,0.90,0.75,0.5)(Duration) AS DurationQuantiles,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = {p3:String} AND ParentSpanId = '' AND TraceId IN matched_traces AND Interval >= toStartOfMinute({p4:DateTime64(9, 'UTC')})
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
				"p1": strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2": "1",
				"p3": "1",
				"p4": strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
			},
		),
		Entry(
			"trace count query with 5m period and service name and span name filters",
			"1",
			&TracesParams{
				Period:          "5m",
				ServiceNames:    []string{"service-name"},
				Operations:      []string{"op-1"},
				AttributeNames:  []string{"key"},
				AttributeValues: []string{"value"},
			},
			`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
	toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = {p2:String})
 AND ( ServiceName IN {p3:Array(String)} ) AND ( SpanName IN {p4:Array(String)} ) AND ( ResourceAttributes[{p5:String}] = {p6:String} OR SpanAttributes[{p7:String}] = {p8:String} )
    GROUP BY TraceId
))

SELECT
    toStartOfMinute(Start) AS Interval,
    quantiles(0.95,0.90,0.75,0.5)(Duration) AS DurationQuantiles,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = {p9:String} AND ParentSpanId = '' AND TraceId IN matched_traces AND Interval >= toStartOfMinute({p10:DateTime64(9, 'UTC')})
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
			clickhouse.Parameters{
				"p0":  strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
				"p1":  strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2":  "1",
				"p3":  "['service-name']",
				"p4":  "['op-1']",
				"p5":  "key",
				"p6":  "value",
				"p7":  "key",
				"p8":  "value",
				"p9":  "1",
				"p10": strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
			},
		),
	)
})

var _ = Context("traces error count query builder", func() {
	var (
		refTime          = time.Now().UTC()
		defaultStartTime = refTime.Add(-1 * 24 * time.Hour).UnixNano()
		defaultEndTime   = refTime.Add(1 * time.Hour).UnixNano()
	)

	DescribeTable("builds queries",
		func(projectID string, params *TracesParams, expected string, args clickhouse.Parameters) {
			builder := buildErrorCountQuery(projectID, params, refTime)
			err := testutils.CompareSQLStrings(builder.SQL(), expected)
			if err != nil {
				GinkgoWriter.Print("diff: ", cmp.Diff(builder.SQL(), expected))
			}
			Expect(err).ToNot(HaveOccurred())
			Expect(builder.StringifyParams()).To(Equal(args))
		},
		Entry(
			"trace error count query with default period and no filters",
			"1",
			&TracesParams{},
			`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
	toStartOfHour({p0:DateTime64(9, 'UTC')}) AS r_start,
	toStartOfHour({p1:DateTime64(9, 'UTC')}) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = {p2:String})

    AND StatusCode = 'STATUS_CODE_ERROR'
    GROUP BY TraceId
))

SELECT
    toStartOfHour(Start) AS Interval,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = {p3:String} AND ParentSpanId = '' AND TraceId IN matched_traces AND Interval >= toStartOfHour({p4:DateTime64(9, 'UTC')})
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
			clickhouse.Parameters{
				"p0": strconv.FormatInt(defaultStartTime, 10),
				"p1": strconv.FormatInt(defaultEndTime, 10),
				"p2": "1",
				"p3": "1",
				"p4": strconv.FormatInt(defaultStartTime, 10),
			},
		),
		Entry(
			"trace count query with 5m period and service name and span name filters",
			"1",
			&TracesParams{
				Period:          "5m",
				ServiceNames:    []string{"service-name"},
				Operations:      []string{"op-1"},
				AttributeNames:  []string{"key"},
				AttributeValues: []string{"value"},
			},
			`
WITH matched_traces AS (
SELECT TraceId FROM (
WITH
	toStartOfMinute({p0:DateTime64(9, 'UTC')}) AS r_start,
	toStartOfMinute({p1:DateTime64(9, 'UTC')}) AS r_end
SELECT
    TraceId
FROM tracing.gl_traces_main
WHERE (Timestamp >= r_start) AND (Timestamp <= r_end) AND (ProjectId = {p2:String})
 AND ( ServiceName IN {p3:Array(String)} ) AND ( SpanName IN {p4:Array(String)} ) AND ( ResourceAttributes[{p5:String}] = {p6:String} OR SpanAttributes[{p7:String}] = {p8:String} )
    AND StatusCode = 'STATUS_CODE_ERROR'
    GROUP BY TraceId
))

SELECT
    toStartOfMinute(Start) AS Interval,
    count() AS TraceCount
FROM tracing.gl_traces_rolled
WHERE ProjectId = {p9:String} AND ParentSpanId = '' AND TraceId IN matched_traces AND Interval >= toStartOfMinute({p10:DateTime64(9, 'UTC')})
GROUP BY ProjectId, Interval
    ORDER BY Interval
`,
			clickhouse.Parameters{
				"p0":  strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
				"p1":  strconv.FormatInt(refTime.Add(1*time.Minute).UnixNano(), 10),
				"p2":  "1",
				"p3":  "['service-name']",
				"p4":  "['op-1']",
				"p5":  "key",
				"p6":  "value",
				"p7":  "key",
				"p8":  "value",
				"p9":  "1",
				"p10": strconv.FormatInt(refTime.Add(-1*5*time.Minute).UnixNano(), 10),
			},
		),
	)
})
