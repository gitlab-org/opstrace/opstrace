package traces

import (
	"testing"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func TestTraceQueryAPI(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "TraceQuery", suiteConfig, reporterConfig)
}

var (
	logger *zap.SugaredLogger
)

var _ = BeforeSuite(func() {
	config := zap.NewDevelopmentConfig()
	sink := zapcore.AddSync(GinkgoWriter)
	encoder := zapcore.NewConsoleEncoder(config.EncoderConfig)
	core := zapcore.NewCore(encoder, sink, zap.DebugLevel)
	log := zap.New(core)
	log = log.WithOptions(zap.ErrorOutput(sink))
	logger = log.Sugar()

	gin.SetMode(gin.DebugMode)
	gin.DefaultWriter = GinkgoWriter
})

var _ = AfterSuite(func() {
	err := logger.Sync()
	Expect(err).ToNot(HaveOccurred())
})
