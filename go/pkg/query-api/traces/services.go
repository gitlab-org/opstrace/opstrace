package traces

import (
	"context"
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func (q *querier) GetServices(ctx context.Context, projectID string) ([]ServiceItem, error) {
	const query = `
SELECT ServiceName FROM ` + constants.TracingDistTableName + `
WHERE ProjectId = ? GROUP BY ServiceName ORDER BY ServiceName ASC`

	var services []struct {
		ServiceName string
	}
	if err := q.db.Select(ctx, &services, query, projectID); err != nil {
		return nil, fmt.Errorf("query services: %w", err)
	}

	sns := make([]ServiceItem, len(services))
	for i, s := range services {
		sns[i].Name = s.ServiceName
	}

	return sns, nil
}

func (q *querier) GetServiceOperations(
	ctx context.Context, projectID string, serviceName string) (*OperationsResult, error) {
	const query = `
SELECT SpanName FROM ` + constants.TracingDistTableName + `
WHERE ProjectId = ? AND ServiceName = ? GROUP BY SpanName ORDER BY SpanName ASC`

	var operations []struct {
		SpanName string
	}
	if err := q.db.Select(ctx, &operations, query, projectID, serviceName); err != nil {
		return nil, fmt.Errorf("query operations: %w", err)
	}

	ops := make([]OperationItem, len(operations))
	for i, o := range operations {
		ops[i].Name = o.SpanName
	}

	return &OperationsResult{Operations: ops}, nil
}
