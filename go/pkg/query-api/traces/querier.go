package traces

import (
	"context"
	"fmt"
	"net/url"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	internalclickhouse "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

type Querier interface {
	GetTraces(ctx context.Context, projectID string, param *TracesParams) ([]*TraceItem, string, error)
	// GetTrace returns the detailed version of the trace that includes all attributes on all spans
	GetTrace(ctx context.Context, projectID string, traceID string) (*TraceDetailedItem, error)
	// GetServices returns span service names associated with a project.
	GetServices(ctx context.Context, projectID string) ([]ServiceItem, error)
	// GetServiceOperations returns span operation names associated with a project and service.
	GetServiceOperations(ctx context.Context, projectID string, serviceName string) (*OperationsResult, error)

	GetTraceAnalytics(ctx context.Context, projectID string, params *TracesParams) (*TracesCountResult, error)
}

type querier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		dsn string
		err error
	)

	if clickHouseCloudDSN != "" {
		u, err := url.Parse(clickHouseCloudDSN)
		if err != nil {
			return nil, fmt.Errorf("parse clickhouse cloud DSN: %w", err)
		}
		dsn = u.JoinPath(constants.TracingDatabaseName).String()
	} else {
		u, err := url.Parse(clickHouseDSN)
		if err != nil {
			return nil, fmt.Errorf("parse clickhouse DSN: %w", err)
		}
		dsn = u.JoinPath(constants.TracingDatabaseName).String()
	}

	db, err = internalclickhouse.GetDatabaseConnection(dsn, opts, logger, 3*time.Second)
	if err != nil {
		return nil, fmt.Errorf("getting database handle: %w", err)
	}
	return &querier{db: db, logger: logger}, nil
}

type MockQuerier struct {
	PassedFilter *TracesParams
	Err          error
}

var _ Querier = (*MockQuerier)(nil)

var testTraceTimestamp = time.Date(2020, time.August, 11, 5, 2, 12, 123456789, time.UTC)

func (m *MockQuerier) GetTraces(_ context.Context, _ string, filter *TracesParams) ([]*TraceItem, string, error) {
	traceID := uuid.New().String()
	m.PassedFilter = filter
	return []*TraceItem{
		{
			Timestamp:            testTraceTimestamp,
			TimestampNano:        testTraceTimestamp.UnixNano(),
			TraceID:              traceID,
			ServiceName:          "sample-service",
			Operation:            "sample-operation",
			StatusCode:           "OK",
			StatusCodeDeprecated: "OK",
			TotalSpans:           2,
		},
	}, "", nil
}

func (m *MockQuerier) GetTrace(_ context.Context, _ string, traceID string) (*TraceDetailedItem, error) {
	if traceID == "12b797ee-7c32-a226-bf56-d4b3af18af56" {
		// Mock the not-found-in-db case
		return nil, nil
	}
	return &TraceDetailedItem{
		Timestamp:     testTraceTimestamp,
		TimestampNano: testTraceTimestamp.UnixNano(),
		TraceID:       traceID,
		ServiceName:   "sample-service",
		Operation:     "sample-operation",
		StatusCode:    "OK",
		Spans: []SpanDetailedItem{
			{
				Timestamp:     testTraceTimestamp,
				TimestampNano: testTraceTimestamp.UnixNano(),
				SpanID:        "uuid",
				TraceID:       traceID,
				ServiceName:   "sample-service",
				Operation:     "sample-operation",
				DurationNano:  1000,
				ParentSpanID:  "",
				StatusCode:    "OK",
				StatusMessage: "foo",
				ScopeVersion:  "bar",
				ScopeName:     "baz",
				SpanKind:      "server",
				TraceState:    "foobar",
				SpanAttributes: map[string]string{
					"http.route":  "/test",
					"custom.attr": "my_value",
				},
				ResourceAttributes: map[string]string{
					"service.name": "sample-service",
				},
			},
			{
				Timestamp:     testTraceTimestamp.Add(time.Millisecond * 10),
				TimestampNano: testTraceTimestamp.Add(time.Millisecond * 10).UnixNano(),
				SpanID:        "uuid",
				TraceID:       traceID,
				ServiceName:   "sample-service",
				Operation:     "sample-operation",
				DurationNano:  1000,
				ParentSpanID:  "",
				StatusCode:    "OK",
				StatusMessage: "foo",
				ScopeVersion:  "bar",
				ScopeName:     "baz",
				SpanKind:      "server",
				TraceState:    "foobar",
				SpanAttributes: map[string]string{
					"http.route":  "/test",
					"custom.attr": "my_value",
				},
				ResourceAttributes: map[string]string{
					"service.name": "sample-service",
				},
			},
		},
	}, nil
}

func (m *MockQuerier) GetServices(_ context.Context, _ string) ([]ServiceItem, error) {
	if m.Err != nil {
		return nil, m.Err
	}
	return []ServiceItem{
		{
			Name: "sample-service",
		},
	}, nil
}

func (m *MockQuerier) GetServiceOperations(_ context.Context, _ string, _ string) (*OperationsResult, error) {
	if m.Err != nil {
		return nil, m.Err
	}
	return &OperationsResult{
		Operations: []OperationItem{
			{
				Name: "sample-operation",
			},
		},
	}, nil
}

func (m *MockQuerier) GetTraceAnalytics(_ context.Context, _ string, params *TracesParams) (*TracesCountResult, error) {
	if m.Err != nil {
		return nil, m.Err
	}
	m.PassedFilter = params
	return &TracesCountResult{
		Results: []*IntervalItem{
			{
				Interval:    1000000,
				TraceCount:  2,
				P90Duration: 1000,
				P95Duration: 1000,
				P75Duration: 1000,
				P50Duration: 1000,
				ErrorCount:  3,
			},
		},
		ProjectID: "1",
	}, nil
}
