package traces

import (
	"fmt"
	"slices"
	"time"

	"github.com/google/uuid"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

const (
	SortDurationDesc = "duration_desc"
	SortDurationAsc  = "duration_asc"

	SortTimestampDesc = "timestamp_desc"
	SortTimestampAsc  = "timestamp_asc"
)

var SupportedSortingDirectives = []string{
	SortTimestampDesc,
	SortDurationDesc,
	SortTimestampAsc,
	SortDurationAsc,
}

var ValidPeriods = map[string]bool{
	common.Period1m:  true,
	common.Period5m:  true,
	common.Period15m: true,
	common.Period30m: true,
	common.Period1h:  true,
	common.Period4h:  true,
	common.Period12h: true,
	common.Period24h: true,
	common.Period7d:  true,
	common.Period14d: true,
	common.Period30d: true,
}

var ValidStatus = map[string]string{
	"ok":    "STATUS_CODE_OK",
	"error": "STATUS_CODE_ERROR",
}

type TracesParams struct {
	TraceIDs []string `form:"trace_id"`
	Period   string   `form:"period"`
	// Custom range parameters only accept RFC3339Nano format.
	StartTime       time.Time `form:"start_time" time_format:"2006-01-02T15:04:05.999999999Z07:00"`
	EndTime         time.Time `form:"end_time" time_format:"2006-01-02T15:04:05.999999999Z07:00"`
	Operations      []string  `form:"operation"`
	ServiceNames    []string  `form:"service_name"`
	NotOperations   []string  `form:"not[operation]"`
	NotServiceNames []string  `form:"not[service_name]"`
	NotTraceIDs     []string  `form:"not[trace_id]"`
	Statuses        []string  `form:"status"`
	NotStatuses     []string  `form:"not[status]"`

	AttributeNames  []string `form:"attr_name"`
	AttributeValues []string `form:"attr_value"`

	LtDuration int64 `form:"lt[duration_nano]" binding:"numeric,gte=0"`
	GtDuration int64 `form:"gt[duration_nano]" binding:"numeric,gte=0"`

	Sort string `form:"sort"`

	PageSize  int64  `form:"page_size,default=100" binding:"numeric,gte=0,max=1000"`
	PageToken string `form:"page_token"`

	Cursor *Page

	// store validated uuids for later query use
	traceIDs    uuid.UUIDs
	notTraceIDs uuid.UUIDs
}

func (r *TracesParams) orderBy() (string, string, string) {
	switch r.Sort {
	case SortDurationDesc:
		return "TDuration", "DESC", "<"
	case SortDurationAsc:
		return "TDuration", "ASC", ">"
	case SortTimestampAsc:
		return "TStart", "ASC", ">"
	default:
		return "TStart", "DESC", "<"
	}
}

func (r *TracesParams) pageSize() int64 {
	if r.PageSize > 0 {
		return r.PageSize
	}

	return 100
}

func (r *TracesParams) timeRange(refTime time.Time) (int64, int64) {
	if !r.StartTime.IsZero() && !r.EndTime.IsZero() {
		return r.StartTime.UnixNano(), r.EndTime.UnixNano()
	}

	startTime, endTime, _, _ := common.InferTimelines(refTime, r.Period)
	return startTime, endTime
}

func (r *TracesParams) isFilterApplied() bool {
	if len(r.TraceIDs) > 0 || len(r.NotTraceIDs) > 0 {
		return true
	}
	if len(r.AttributeNames) > 0 {
		return true
	}
	if len(r.Operations) > 0 || len(r.NotOperations) > 0 {
		return true
	}
	if len(r.ServiceNames) > 0 || len(r.NotServiceNames) > 0 {
		return true
	}
	if r.GtDuration != 0 || r.LtDuration != 0 {
		return true
	}
	if len(r.Statuses) > 0 || len(r.NotStatuses) > 0 {
		return true
	}

	return false
}

//nolint:cyclop
func (r *TracesParams) Validate() error {
	tids, err := validateTraceIDs(r.TraceIDs)
	if err != nil {
		return err
	}
	r.traceIDs = tids

	nTids, err := validateTraceIDs(r.NotTraceIDs)
	if err != nil {
		return err
	}
	r.notTraceIDs = nTids

	if r.Period != "" {
		valid := ValidPeriods[r.Period]
		if !valid {
			return fmt.Errorf("invalid period: %s", r.Period)
		}
	}

	// Either period or a time range pair should be provided but not both.
	if r.Period != "" && (!r.StartTime.IsZero() || !r.EndTime.IsZero()) {
		return fmt.Errorf("both period and time range are provided")
	}
	if (!r.StartTime.IsZero() && r.EndTime.IsZero()) || (!r.EndTime.IsZero() && r.StartTime.IsZero()) {
		return fmt.Errorf("both ends for time range are not provided")
	}

	if r.Sort != "" {
		if !slices.Contains(SupportedSortingDirectives, r.Sort) {
			return fmt.Errorf("unsupported sort - %s", r.Sort)
		}
	}

	if len(r.AttributeNames) != len(r.AttributeValues) {
		return fmt.Errorf("attribute names and values must be the same length")
	}

	if err := validateStatus(r.Statuses); err != nil {
		return err
	}
	if err := validateStatus(r.NotStatuses); err != nil {
		return err
	}
	return nil
}

// Validate slice trace ids against uuid.Parse
// https://pkg.go.dev/github.com/google/uuid@v1.3.1#Parse
// Updates the ids with standard 36 char string representation that works with ClickHouse
// i.e. xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
func validateTraceIDs(ids []string) (uuid.UUIDs, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	uuids := make([]uuid.UUID, len(ids))

	for i, t := range ids {
		id, err := uuid.Parse(t)
		if err != nil {
			return nil, fmt.Errorf("invalid trace_id: %s - %w", t, err)
		}
		ids[i] = id.String()
		uuids[i] = id
	}

	return uuids, nil
}

func validateStatus(statuses []string) error {
	for i, s := range statuses {
		mapped, ok := ValidStatus[s]
		if !ok {
			return fmt.Errorf("unsupported status filter: %s", s)
		}
		statuses[i] = mapped
	}
	return nil
}

type TraceParams struct {
	TraceID string `uri:"id" binding:"required,uuid"`
}

type ServiceOperationsParams struct {
	ServiceName string `uri:"service_name" binding:"required"`
}
