package traces

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

func (q *querier) GetTrace(ctx context.Context, projectID string, traceID string) (*TraceDetailedItem, error) {
	qb := traceDetailQuery(projectID, traceID)

	var spans []SpanDetailedItem
	if err := q.db.Select(
		qb.Context(ctx),
		&spans,
		qb.SQL()); err != nil {
		return nil, fmt.Errorf("query spans: %w", err)
	}
	if len(spans) == 0 {
		return nil, nil
	}

	startTime := spans[0].Timestamp
	endTime := startTime.Add(time.Duration(spans[0].DurationNano))
	for i := range spans {
		ts := spans[i].Timestamp
		if ts.Before(startTime) {
			startTime = ts
		}
		es := ts.Add(time.Duration(spans[i].DurationNano))
		if es.After(endTime) {
			endTime = es
		}
		spans[i].TimestampNano = ts.UnixNano()
	}

	trace := TraceDetailedItem{}
	trace.Timestamp = startTime
	trace.TimestampNano = startTime.UnixNano()
	trace.DurationNano = endTime.Sub(startTime).Nanoseconds()
	trace.TraceID = spans[0].TraceID
	trace.ServiceName = spans[0].ServiceName
	trace.Operation = spans[0].Operation
	trace.StatusCode = spans[0].StatusCode
	trace.Spans = spans
	trace.TotalSpans = int64(len(spans))

	return &trace, nil
}

func traceDetailQuery(projectID string, traceID string) *clickhouse.QueryBuilder {
	qb := clickhouse.NewQueryBuilder().WithStringParams(
		map[string]string{
			"projectId":    projectID,
			"traceId":      traceID,
			"traceIdTable": constants.TracingIDTimestampTableName,
			"mainTable":    constants.TracingDistTableName,
		},
	)

	//nolint:lll
	// Query requires changes to concurrent read settings because in most cases, the amount of rows read will be under
	// 100K rows. Default values for enabling concurrent reads are much larger.
	// See https://clickhouse.com/docs/en/operations/settings/settings#merge-tree-min-rows-for-concurrent-read-for-remote-filesystem for defaults.
	// Subquery has rootSpanOrderKey and is included in the ORDER BY.
	// Outer SELECT drops this field.
	const query = `
WITH
UUIDStringToNum({traceId:String}) AS traceId,
(
	SELECT Start FROM {traceIdTable:Identifier}
	WHERE ProjectId = {projectId:String} AND TraceId = traceId
	ORDER BY Start LIMIT 1
) AS minTime,
(
	SELECT End FROM {traceIdTable:Identifier}
	WHERE ProjectId = {projectId:String} AND TraceId = traceId
	ORDER BY End DESC LIMIT 1
) AS maxTime
SELECT
    Timestamp,
    SpanID,
    Operation,
    DurationNano,
    TraceID,
    ParentSpanID,
    TraceState,
    SpanKind,
    ServiceName,
    ResourceAttributes,
    ScopeName,
    ScopeVersion,
    SpanAttributes,
    StatusCode,
    StatusMessage
FROM (SELECT
    Timestamp,
    hex(SpanId) as SpanID,
    SpanName as Operation,
    Duration as DurationNano,
    UUIDNumToString(TraceId) as TraceID,
    if(ParentSpanId = '', '', hex(ParentSpanId)) as ParentSpanID,
    TraceState,
    SpanKind,
    ServiceName,
    ResourceAttributes,
    ScopeName,
    ScopeVersion,
    SpanAttributes,
    StatusCode,
    StatusMessage,
    if(ParentSpanId = '', 1, 2) as rootSpanOrderKey
FROM {mainTable:Identifier}
WHERE ProjectId = {projectId:String} AND TraceId = traceId AND Timestamp >= minTime AND Timestamp <= maxTime
ORDER BY (rootSpanOrderKey, Timestamp) ASC)
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`
	return qb.Build(query)
}
