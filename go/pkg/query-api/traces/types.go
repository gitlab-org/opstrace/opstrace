package traces

import (
	"time"
)

// API response for list traces

type SpanDetailedItem struct {
	Timestamp          time.Time         `json:"timestamp"`
	TimestampNano      int64             `json:"timestamp_nano"`
	SpanID             string            `json:"span_id"`
	TraceID            string            `json:"trace_id"`
	ServiceName        string            `json:"service_name"`
	Operation          string            `json:"operation"`
	DurationNano       int64             `json:"duration_nano"`
	ParentSpanID       string            `json:"parent_span_id"`
	StatusCode         string            `json:"status_code"`
	StatusMessage      string            `json:"status_message"`
	TraceState         string            `json:"trace_state"`
	SpanKind           string            `json:"span_kind"`
	ScopeName          string            `json:"scope_name"`
	ScopeVersion       string            `json:"scope_version"`
	SpanAttributes     map[string]string `json:"span_attributes"`
	ResourceAttributes map[string]string `json:"resource_attributes"`
}

type TraceDetailedItem struct {
	Timestamp     time.Time          `json:"timestamp"`
	TimestampNano int64              `json:"timestamp_nano"`
	TraceID       string             `json:"trace_id"`
	ServiceName   string             `json:"service_name"`
	Operation     string             `json:"operation"`
	StatusCode    string             `json:"status_code"`
	DurationNano  int64              `json:"duration_nano"`
	TotalSpans    int64              `json:"total_spans"`
	Spans         []SpanDetailedItem `json:"spans"`
}

type TracesResult struct {
	ProjectID string `json:"project_id"`

	Traces      []*TraceItem `json:"traces"`
	TotalTraces int64        `json:"total_traces"`

	// NextPageToken is the token for next page if the results are greater than page size.
	NextPageToken string `json:"next_page_token,omitempty"`
}

type TraceItem struct {
	// Snake Case all attributes
	Timestamp        time.Time `json:"timestamp"`
	TimestampNano    int64     `json:"timestamp_nano"`
	TraceID          string    `json:"trace_id"`
	ServiceName      string    `json:"service_name"`
	Operation        string    `json:"operation"`
	StatusCode       string    `json:"status_code"`
	DurationNano     uint64    `json:"duration_nano"`
	TotalSpans       uint64    `json:"total_spans"`
	MatchedSpanCount uint64    `json:"matched_span_count"`
	ErrorSpanCount   uint64    `json:"error_span_count"`
	// InProgress is an estimate if the trace is in progress.
	// We don't have a perfect way to tell this, so we use some heuristics.
	// See discussion on https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2692#note_1791784111.
	InProgress           bool   `json:"in_progress"`
	StatusCodeDeprecated string `ch:"statusCode" json:"statusCode"`
}

type ServicesResult struct {
	Services []ServiceItem `json:"services"`
}

type ServiceItem struct {
	Name string `json:"name"`
}

type TracesCountResult struct {
	ProjectID string `json:"project_id"`

	Results []*IntervalItem `json:"results,omitempty"`
}

type IntervalItem struct {
	Interval    uint64  `json:"interval,omitempty"`
	TraceCount  uint64  `json:"count,omitempty"`
	P90Duration float64 `json:"p90_duration_nano,omitempty"`
	P95Duration float64 `json:"p95_duration_nano,omitempty"`
	P75Duration float64 `json:"p75_duration_nano,omitempty"`
	P50Duration float64 `json:"p50_duration_nano,omitempty"`
	ErrorCount  uint64  `json:"error_count,omitempty"`
	TraceRate   float64 `json:"trace_rate,omitempty"`
	ErrorRate   float64 `json:"error_rate,omitempty"`
}

type OperationsResult struct {
	Operations []OperationItem `json:"operations"`
}

type OperationItem struct {
	Name string `json:"name"`
}
