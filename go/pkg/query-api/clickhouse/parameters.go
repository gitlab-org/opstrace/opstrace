package clickhouse

import (
	"fmt"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/google/uuid"
)

// Parameters is alias for clickhouse parameters.
type Parameters = clickhouse.Parameters

// FormatArray formats supported type slice to a ClickHouse string
// representing a compound array literal.
func FormatArray[N Literal](n []N) string {
	var b strings.Builder

	b.WriteString("[")
	for i, v := range n {
		b.WriteString(v.String())
		if i < len(n)-1 {
			b.WriteString(",")
		}
	}
	b.WriteString("]")
	return b.String()
}

// clickhouse string literals require escaping \ and '
// must be double escaped as they are sent as strings which are escaped again in the server
var stringEscaper = strings.NewReplacer(`\`, `\\\\`, `'`, `\\'`)

func printString(s string) string {
	return stringEscaper.Replace(s)
}

func FuncMapToString(fName string, ids []string) (string, []any) {
	// TODO(Arun): This does not work as expected because of a bug in
	// ClickHouse https://github.com/ClickHouse/ClickHouse/issues/14980
	// b.Build("AND ( TraceId IN arrayMap(x -> UUIDStringToNum(x), ? ) ) ", param.TraceIDs)

	// Workaround to the above transformation to wrap the
	// list of traceIDs inside the conversion function
	// on the client side. This has the same effect of
	// the above query and uses the primary index.

	// TODO(prozlach): The switch to clickhouse native params should allow us
	// to use `FixedString(16)` as `ParamType` here directly, not a priority
	// though so leaving as is.
	transformed := make([]any, len(ids))
	for i, v := range ids {
		transformed[i] = v
	}
	// Note that we pass each string as a separate bind parameter instead of binding the whole array as
	// parameter. This is to avoid quoting the conversion function itself.
	// TrimRight is to remove an extra "," at the end of the string.
	return "[ " + strings.TrimRight(strings.Repeat(" "+fName+"(?),", len(transformed)), ",") + " ]", transformed
}

// BytesToFixedString converts a byte slice into a printable string
// that can be used as a ClickHouse FixedString(N) parameter.
// Uses hex encoding for each byte.
func BytesToFixedString(bytes []byte) string {
	var sb strings.Builder
	for _, b := range bytes {
		fmt.Fprintf(&sb, `\\x%02X`, b)
	}
	return sb.String()
}

// UUIDsAsFixedStringArray converts a slice of UUIDs into
// a ClickHouse Array(FixedString(16)) compatible Literal.
func UUIDsAsFixedStringArray(ids uuid.UUIDs) Array[FixedString16] {
	fs := make(Array[FixedString16], len(ids))
	for i, id := range ids {
		fs[i] = FixedString16(id)
	}
	return fs
}

// Value converts a Golang type to a Literal for use in the query API.
func Value[T GoTypes](val T) Literal {
	return toLiteral(val)
}

// StringParams converts a map of strings to a map of Literals.
// Quicker and more convenient than calling Value() for each string.
func StringParams(params map[string]string) map[string]Literal {
	ls := make(map[string]Literal, len(params))
	for k, v := range params {
		ls[k] = String(v)
	}
	return ls
}

// Builtin types cannot be made to match a particular interface.
// We have to use reflection to convert them to literals.
// First we check if the type is already a literal.
//
//nolint:cyclop // not sure there's a better way to do this
func toLiteral(val any) Literal {
	switch v := val.(type) {
	// many of SupportedTypes are already literals
	case Literal:
		return v
	// rest in order of most used
	case string:
		return String(v)
	case time.Time:
		return TimestampNano(v.UnixNano())
	// 64-bit numbers
	case int:
		return builtIn[int]{v, printInt[int]}
	case int64:
		return builtIn[int64]{v, printInt[int64]}
	case uint:
		return builtIn[uint]{v, printUint[uint]}
	case uint64:
		return builtIn[uint64]{v, printUint[uint64]}
	case float64:
		return builtIn[float64]{v, printFloat[float64]}
	// other numeric types
	case bool:
		return builtIn[bool]{v, printBool}
	case float32:
		return builtIn[float32]{v, printFloat[float32]}
	case int8:
		return builtIn[int8]{v, printInt[int8]}
	case int16:
		return builtIn[int16]{v, printInt[int16]}
	case int32:
		return builtIn[int32]{v, printInt[int32]}
	case uint8:
		return builtIn[uint8]{v, printUint[uint8]}
	case uint16:
		return builtIn[uint16]{v, printUint[uint16]}
	case uint32:
		return builtIn[uint32]{v, printUint[uint32]}
	default:
		panic(fmt.Sprintf("unsupported type %T", v))
	}
}
