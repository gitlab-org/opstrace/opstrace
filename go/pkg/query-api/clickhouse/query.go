package clickhouse

import (
	"context"
	"fmt"
	"strings"

	"github.com/ClickHouse/clickhouse-go/v2"
	"golang.org/x/exp/maps"
)

// QueryBuilder helps construct a ClickHouse SQL query.
// Do not copy a non-zero QueryBuilder.
// QueryBuilder is not thread-safe.
// Provides a fluent interface to construct the query.
type QueryBuilder struct {
	sql     strings.Builder
	params  map[string]Literal
	idx     int
	newLine bool
}

func NewQueryBuilder() *QueryBuilder {
	return &QueryBuilder{
		params: make(map[string]Literal),
	}
}

// Build takes the given sql string replaces any ? with the equivalent indexed parameter
// and appends elems to the args slice.
func (qb *QueryBuilder) Build(q string, elems ...any) *QueryBuilder {
	// add the query params to the args slice, if any
	// replace ? with corresponding parameter p<idx>
	for _, elem := range elems {
		paramName := fmt.Sprintf("p%d", qb.idx)
		l := toLiteral(elem)
		qb.WithParam(paramName, l)

		p := fmt.Sprintf("{%s:%s}", paramName, l.Type())
		q = strings.Replace(q, "?", p, 1)

		qb.idx += 1
	}
	// add the sanitized query statement to the current sql query
	q = strings.Trim(q, " ")
	newLine := q[len(q)-1] == '\n'

	// if the query ends in a newline, don't add a space before it
	// adding a space is a convenience for chaining expressions

	switch {
	case qb.newLine, qb.sql.Len() == 0:
		fallthrough
	case q == "\n":
		_, _ = qb.sql.WriteString(q)
	default:
		_, _ = fmt.Fprintf(&qb.sql, " %s", q)
	}

	qb.newLine = newLine
	return qb
}

// WithParam replaces a named parameter in the Params map.
func (qb *QueryBuilder) WithParam(name string, value Literal) *QueryBuilder {
	if qb.params == nil {
		qb.params = make(map[string]Literal)
	}

	qb.params[name] = value
	return qb
}

// WithParams adds or replaces all parameters named in the provided map.
func (qb *QueryBuilder) WithParams(params map[string]Literal) *QueryBuilder {
	if qb.params == nil {
		qb.params = make(map[string]Literal)
	}

	maps.Copy(qb.params, params)
	return qb
}

// WithStringParams is a convenience function for bulk adding string parameters.
func (qb *QueryBuilder) WithStringParams(params map[string]string) *QueryBuilder {
	ps := StringParams(params)
	return qb.WithParams(ps)
}

// SQL returns the rendered SQL query.
// Any named parameters in the query are replaced with their native {name:type} pairs.
func (qb *QueryBuilder) SQL() string {
	return qb.sql.String()
}

// Params returns the map of query literals to be used in the SQL query.
func (qb *QueryBuilder) Params() map[string]Literal {
	ls := make(map[string]Literal, len(qb.params))
	maps.Copy(ls, qb.params)
	return ls
}

// StringifyParams resolves each literal to a string representation.
// Ideally only run this once for each query.
func (qb *QueryBuilder) StringifyParams() clickhouse.Parameters {
	ps := make(clickhouse.Parameters, len(qb.params))
	for k, v := range qb.params {
		ps[k] = v.String()
	}
	return ps
}

// Context returns a context to use with the ClickHouse driver query.
// It adds the query parameters to the context by default.
func (qb *QueryBuilder) Context(ctx context.Context, options ...clickhouse.QueryOption) context.Context {
	return clickhouse.Context(ctx, append(options, clickhouse.WithParameters(qb.StringifyParams()))...)
}
