package clickhouse

import (
	"reflect"
	"strconv"
	"time"
)

// Literal holds a compatible ClickHouse type and string value.
type Literal interface {
	// String returns a ClickHouse compatible literal representation.
	String() string
	// Type returns the ClickHouse type name for this literal.
	Type() string
}

// SupportedType is a type that has a Literal implementation in this package.
// External Literal implementations can also be used in the API.
type SupportedType interface {
	GoTypes | TimestampNano | time.Time | TypeValPair | Identifier | FixedString16
}

// GoTypes is a Golang primitive type that can be used in ClickHouse.
// Uses direct types rather than ~underlying compatible types
// as inferring the underlying type is not always possible.
type GoTypes interface {
	string | bool | Signed | Unsigned | Float
}

// Unsigned is an unsigned integer type.
// constraints.Unsigned contains uintptr and we don't want that.
type Unsigned interface {
	uint | uint8 | uint16 | uint32 | uint64
}

// Signed integer types. Copy of constraints.Signed without ~.
type Signed interface {
	int | int8 | int16 | int32 | int64
}

// Float types. Copy of constraints.Float without ~.
type Float interface {
	float32 | float64
}

// builtIn handles ClickHouse type mapping for Golang primitives.
// Allows for representing all the Go types without lots of
// boilerplate code.
type builtIn[V GoTypes] struct {
	V V
	// Print is a function to convert a Golang type to a ClickHouse literal string.
	Print func(V) string
}

// Print functions for builtins follow here.

func printInt[N Signed](n N) string {
	return strconv.FormatInt(int64(n), 10)
}

func printUint[N Unsigned](n N) string {
	return strconv.FormatUint(uint64(n), 10)
}

func printFloat[N Float](n N) string {
	return strconv.FormatFloat(float64(n), 'f', -1, 64)
}

func printBool(b bool) string {
	if b {
		return "1"
	}
	return "0"
}

func (b builtIn[V]) String() string {
	if b.Print == nil {
		panic("BuiltIn Print is nil")
	}

	return b.Print(b.V)
}

// map Golang primitives to ClickHouse types.
// this is a sparse slice but has O(1) lookups.
var kinds = []string{
	reflect.Bool: "Bool",
	// Int is Int32 in ClickHouse, but we likely mean int64 here on 64-bit systems.
	reflect.Int:   "Int64",
	reflect.Int8:  "Int8",
	reflect.Int16: "Int16",
	reflect.Int32: "Int32",
	reflect.Int64: "Int64",
	// As above, we likely mean uint64 here on 64-bit systems.
	reflect.Uint:    "UInt64",
	reflect.Uint8:   "UInt8",
	reflect.Uint16:  "UInt16",
	reflect.Uint32:  "UInt32",
	reflect.Uint64:  "UInt64",
	reflect.Float32: "Float32",
	reflect.Float64: "Float64",
	reflect.String:  "String",
}

func (b builtIn[V]) Type() string {
	k := reflect.TypeOf(b.V).Kind()
	return kinds[k]
}

// String is a special case for strings as these are most used.
// No looking up types etc.
type String string

func (s String) String() string {
	return printString(string(s))
}

func (s String) Type() string {
	return "String"
}

// TimestampNano is a ClickHouse compatible nanosecond timestamp.
type TimestampNano int64

func (t TimestampNano) String() string {
	return printInt(int64(t))
}

func (t TimestampNano) Type() string {
	return "DateTime64(9, 'UTC')"
}

// Identifier is a ClickHouse Identifier, like a table name.
type Identifier string

func (i Identifier) String() string {
	return string(i)
}

func (i Identifier) Type() string {
	return "Identifier"
}

// FixedString16 is a ClickHouse FixedString(16) type.
// Used for representing UUIDs in various tables.
type FixedString16 [16]byte

func (f FixedString16) String() string {
	return BytesToFixedString(f[:])
}

func (f FixedString16) Type() string {
	return "FixedString(16)"
}

// TypeValPair is a pair of (type, value) to use explicit parameter type and value.
type TypeValPair struct {
	ParamType string
	ParamVal  string
}

func (t TypeValPair) String() string {
	return t.ParamVal
}

func (t TypeValPair) Type() string {
	return t.ParamType
}

// Quoted is a thing surrounded by single quotes.
// Typically used for string literals inside composite types.
type Quoted[T Literal] struct {
	V T
}

func (l Quoted[T]) String() string {
	return "'" + l.V.String() + "'"
}

func (l Quoted[T]) Type() string {
	return l.V.Type()
}

// Array is a ClickHouse Array(T) type.
// String array elements will be quoted.
type Array[T SupportedType] []T

func (a Array[T]) String() string {
	ls := make([]Literal, len(a))
	for i, v := range a {
		ls[i] = quoteIfNeeded(v)
	}

	return FormatArray(ls)
}

func (a Array[T]) Type() string {
	v := new(T)
	t := toLiteral(*v).Type()
	return "Array(" + t + ")"
}

func quoteIfNeeded[T SupportedType](v T) Literal {
	switch q := any(v).(type) {
	case string:
		return Quoted[String]{V: String(q)}
	case FixedString16:
		return Quoted[FixedString16]{q}
	}
	return toLiteral(v)
}
