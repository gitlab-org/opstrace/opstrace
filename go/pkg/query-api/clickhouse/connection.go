package clickhouse

import (
	"context"
	"fmt"
	"maps"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.uber.org/zap"
)

// Conn alias for ClickHouse connection for import convenience
type Conn = clickhouse.Conn

// Options alias for ClickHouse options for import convenience
type Options = clickhouse.Options

func GetDatabaseConnection(
	clickHouseDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
	pingDuration time.Duration,
) (clickhouse.Conn, error) {
	dbOpts, err := clickhouse.ParseDSN(clickHouseDSN)
	if err != nil {
		return nil, fmt.Errorf("parsing clickhouse DSN: %w", err)
	}
	if opts != nil {
		dbOpts.MaxOpenConns = opts.MaxOpenConns
		dbOpts.MaxIdleConns = opts.MaxIdleConns
	}
	dbOpts.ConnMaxLifetime = 1 * time.Hour
	dbOpts.Compression = &clickhouse.Compression{
		Method: clickhouse.CompressionLZ4,
	}
	// Enforce limits on query complexity.
	// See https://clickhouse.com/docs/en/operations/settings/query-complexity for an exhaustive list.
	// Keep in mind that ClickHouse checks the restrictions for data parts, not for each row.
	// It means that you can exceed the restrictions with the size of each data part.
	settings := clickhouse.Settings{
		"max_execution_time": 60 * 5, // 5 minutes
		// If query runs longer than max_execution_time, stop executing the query and return partial result.
		"timeout_overflow_mode": "break",
		"max_rows_to_read":      15000000, // 15 million rows
		// return partial result when reading more than max_rows_to_read
		"read_overflow_mode":   "break",
		"max_rows_to_group_by": 15000000, // 15 million rows
		// return approx result when grouping more than max_rows_to_group_by
		"group_by_overflow_mode": "any",
		"max_result_rows":        1000000,   // 1 million rows
		"max_result_bytes":       104857600, // 100 MB
		// return partial result when returning more than max_result_rows or max_result_bytes
		"result_overflow_mode": "break",
		"max_memory_usage":     2097152000, // 2 GB
	}
	if opts != nil {
		maps.Copy(settings, opts.Settings)
	}
	dbOpts.Settings = settings

	if logger.Level() == zap.DebugLevel {
		dbOpts.Debug = true
	}

	db, err := clickhouse.Open(dbOpts)
	if err != nil {
		return nil, fmt.Errorf("opening db: %w", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), pingDuration)
	defer cancel()

	if err := db.Ping(ctx); err != nil {
		return nil, fmt.Errorf("connecting to the database: %w", err)
	}
	return db, nil
}
