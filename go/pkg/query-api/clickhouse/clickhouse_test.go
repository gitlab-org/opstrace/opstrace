package clickhouse_test

import (
	"context"
	"fmt"
	"math"
	"strconv"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	testcontainersCH "github.com/testcontainers/testcontainers-go/modules/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.uber.org/zap"
)

func setupDB(ctx context.Context, t *testing.T) clickhouse.Conn {
	t.Helper()
	ch, err := testcontainersCH.RunContainer(ctx)
	require.NoError(t, err)
	//nolint:contextcheck // ensure container is terminated
	t.Cleanup(func() {
		ch.Terminate(context.Background())
	})

	dsn, err := ch.ConnectionString(ctx)
	require.NoError(t, err)

	l := zap.NewExample()

	//nolint:contextcheck // ping uses own context
	conn, err := clickhouse.GetDatabaseConnection(dsn, nil, l, time.Second*5)
	require.NoError(t, err)
	t.Cleanup(func() {
		conn.Close()
	})

	return conn
}

func Test_ClickHouse_Special_Literals(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	conn := setupDB(ctx, t)

	refTime := time.Now().UTC()
	refTimeFmt := refTime.Format("2006-01-02 15:04:05.000000000")

	queryTests := map[string]struct {
		literal      any
		expectString string
		expectType   string
	}{
		"String value equals text": {
			clickhouse.String("text"),
			"text",
			"String",
		},
		"String values are escaped": {
			clickhouse.String(`t\ex't`),
			`t\ex't`,
			"String",
		},
		"Array of strings is quoted": {
			clickhouse.Array[string]{"a", "b", "c"},
			"['a','b','c']",
			"Array(String)",
		},
		"Array of strings is quoted and escaped": {
			clickhouse.Array[string]{"foo\\", "b'ar", "b'a\\z"},
			"['foo\\\\','b\\'ar','b\\'a\\\\z']",
			"Array(String)",
		},
		"Array of ints becomes Array(Int64)": {
			clickhouse.Array[int64]{1, 2, 3},
			"[1,2,3]",
			"Array(Int64)",
		},
		"Empty Array is empty": {
			clickhouse.Array[string]{},
			"[]",
			"Array(String)",
		},
		"Time uses nanosecond precision DateTime64": {
			refTime,
			refTimeFmt,
			"DateTime64(9, 'UTC')",
		},
		"TypeValPair allows overriding custom types": {
			clickhouse.TypeValPair{
				ParamType: "Date",
				ParamVal:  refTime.Format(time.DateOnly),
			},
			refTime.Format(time.DateOnly),
			"Date",
		},
		"FixedString16 converts to bytes and back": {
			clickhouse.FixedString16([16]byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'}),
			"abcdefghijklmnop",
			"FixedString(16)",
		},
	}

	for name, test := range queryTests {
		t.Run(name, func(t *testing.T) {
			qb := clickhouse.NewQueryBuilder()
			got := []struct{ Val string }{}
			qb.Build("SELECT toString(?) as Val", test.literal)
			sql := qb.SQL()
			assert.Equal(t, sql, fmt.Sprintf("SELECT toString({p0:%s}) as Val", test.expectType))
			err := conn.Select(qb.Context(ctx), &got, qb.SQL())
			assert.NoError(t, err)
			require.NotEmpty(t, got)
			assert.Equal(t, test.expectString, got[0].Val)
		})
	}
}

func Test_ClickHouse_Primitive_Literals(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	conn := setupDB(ctx, t)

	tests := []struct {
		literal any
		expect  string
	}{
		{true, "true"},
		{false, "false"},
		{int(-15), "-15"},
		{uint(16), "16"},
		{int64(math.MaxInt64), strconv.FormatInt(math.MaxInt64, 10)},
		{int32(math.MaxInt32), strconv.Itoa(int(math.MaxInt32))},
		{int16(math.MaxInt16), strconv.Itoa(int(math.MaxInt16))},
		{int8(math.MaxInt8), strconv.Itoa(int(math.MaxInt8))},
		{uint64(math.MaxUint64), strconv.FormatUint(math.MaxUint64, 10)},
		{uint32(math.MaxUint32), strconv.Itoa(int(math.MaxUint32))},
		{uint16(math.MaxUint16), strconv.Itoa(int(math.MaxUint16))},
		{uint8(math.MaxUint8), strconv.Itoa(int(math.MaxUint8))},
		// Golang and ClickHouse float implementations are not the same.
		// Go math.MaxFloat64 results in Inf in ClickHouse
		{float64(0.12), "0.12"},
		{float32(1.24), "1.24"},
		{"text", "text"},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%T", test.literal), func(t *testing.T) {
			qb := clickhouse.NewQueryBuilder()
			got := []struct{ Val string }{}
			qb.Build("SELECT toString(?) as Val", test.literal)
			err := conn.Select(qb.Context(ctx), &got, qb.SQL())
			assert.NoError(t, err)
			require.NotEmpty(t, got)
			assert.Equal(t, test.expect, got[0].Val)
		})
	}
}

func Test_ClickHouse_Identifiers(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	conn := setupDB(ctx, t)

	qb := clickhouse.NewQueryBuilder()
	qb.Build(`
SELECT groupArrayArray(*) as Vals FROM
(SELECT array({orderCol:Identifier}) FROM numbers(3) ORDER BY {orderCol:Identifier} DESC)`).
		WithParam("orderCol", clickhouse.Identifier("number"))

	got := []struct{ Vals []int }{}
	err := conn.Select(qb.Context(ctx), &got, qb.SQL())
	assert.NoError(t, err)
	require.NotEmpty(t, got)
	assert.Equal(t, []int{2, 1, 0}, got[0].Vals)
}

func Test_ClickHouse_Params(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	conn := setupDB(ctx, t)

	qb := clickhouse.NewQueryBuilder()
	qb.WithStringParams(map[string]string{
		"a": "a",
		"b": "b",
	})
	qb.WithParams(map[string]clickhouse.Literal{
		"c": clickhouse.Value(true),
		"d": clickhouse.Value(false),
	})
	qb.WithParam("e", clickhouse.Array[int]{1, 2, 3})

	qb.Build(`SELECT {a:String} as A, {b:String} as B, {c:Boolean} as C, {d:Boolean} as D, {e:Array(Int64)} as E`)

	got := []struct {
		A, B string
		C, D bool
		E    []int
	}{}
	err := conn.Select(qb.Context(ctx), &got, qb.SQL())
	assert.NoError(t, err)
	require.NotEmpty(t, got)
	assert.Equal(t, []struct {
		A, B string
		C, D bool
		E    []int
	}{
		{"a", "b", true, false, []int{1, 2, 3}},
	}, got)
}

func Test_ClickHouse_UUIDs_As_FixedString16(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	conn := setupDB(ctx, t)

	// known problematic uuid if the \x00 pattern is not escaped properly.
	// without double escaping the hex it cannot be read back
	// and becomes 78395E0A177C?3D2?B2AC31CE5DB27F5 because the byte
	// \x3F is interpreted in the server as 0x3f which is ?.
	uid1 := uuid.MustParse("78395e0a-177c-43d2-bb2a-c31ce5db27f5")
	uid2 := uuid.New()

	qb := clickhouse.NewQueryBuilder()

	qb.Build("SELECT UUIDNumToString(?) as UID1, toString(arrayMap(x -> UUIDNumToString(x), ?)) as UUIDs",
		clickhouse.FixedString16(uid1),
		clickhouse.UUIDsAsFixedStringArray(uuid.UUIDs{uid1, uid2}),
	)

	got := []struct {
		UID1  string
		UUIDs string
	}{}
	err := conn.Select(qb.Context(ctx), &got, qb.SQL())
	assert.NoError(t, err)
	require.NotEmpty(t, got)
	assert.Equal(t, uid1.String(), got[0].UID1)
	assert.Equal(t, fmt.Sprintf("['%s','%s']", uid1, uid2), got[0].UUIDs)
}
