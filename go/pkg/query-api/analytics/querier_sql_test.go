package analytics

import (
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var _ = Describe("analytics", Ordered, Serial, func() {
	refTime := time.Now().UTC()
	month := int(refTime.Month())
	year := refTime.Year()

	DescribeTable(
		"building storage analytics queries",
		func(
			queryFilters *QueryFilters,
			expectedQuery string,
			expectedArgs clickhouse.Parameters,
		) {
			querier := chQuerier{db: conn, logger: logger.Desugar()}
			qb, err := querier.buildQuery(queryFilters)
			Expect(err).ToNot(HaveOccurred())

			err = testutils.CompareSQLStrings(qb.SQL(), expectedQuery)
			Expect(err).ToNot(HaveOccurred())

			Expect(qb.StringifyParams()).To(Equal(expectedArgs))
		},
		Entry(
			"for given project",
			&QueryFilters{
				ProjectIDs: []string{metrics.RandomProjectID},
			},
			`
SELECT
  TenantId,
  ProjectId,
  substringIndex(Dimension, '.', 1) AS Dimension,
  toStartOfDay(AggTime) AS AggTimeDay,
  toMonth(AggTimeDay) AS AggTimeMonth,
  toStartOfMonth(AggTimeDay) AS StartOfMonth,
  toLastDayOfMonth(AggTimeDay) AS EndOfMonth,
  sum(BytesCount) AS BytesCountTotal,
  sum(EventsCount) AS EventsCountTotal
FROM analytics.analytics_ingestion_main
WHERE ProjectId IN {p0:Array(String)} GROUP BY TenantId, ProjectId, Dimension, AggTimeDay ORDER BY TenantId, ProjectId, Dimension, AggTimeDay`,
			clickhouse.Parameters{
				"p0": "['" + metrics.RandomProjectID + "']",
			},
		),
		Entry(
			"for a given tenant, project",
			&QueryFilters{
				ProjectIDs: []string{metrics.RandomProjectID},
				TenantID:   metrics.RandomTenantID,
			},
			`
SELECT
  TenantId,
  ProjectId,
  substringIndex(Dimension, '.', 1) AS Dimension,
  toStartOfDay(AggTime) AS AggTimeDay,
  toMonth(AggTimeDay) AS AggTimeMonth,
  toStartOfMonth(AggTimeDay) AS StartOfMonth,
  toLastDayOfMonth(AggTimeDay) AS EndOfMonth,
  sum(BytesCount) AS BytesCountTotal,
  sum(EventsCount) AS EventsCountTotal
FROM analytics.analytics_ingestion_main
WHERE ProjectId IN {p0:Array(String)} AND TenantId = {p1:String} GROUP BY TenantId, ProjectId, Dimension, AggTimeDay ORDER BY TenantId, ProjectId, Dimension, AggTimeDay`,
			clickhouse.Parameters{
				"p0": "['" + metrics.RandomProjectID + "']",
				"p1": metrics.RandomTenantID,
			},
		),
		Entry(
			"for given month, year",
			&QueryFilters{
				ProjectIDs: []string{metrics.RandomProjectID},
				Month:      uint(month),
				Year:       uint(year),
			},
			`
SELECT
  TenantId,
  ProjectId,
  substringIndex(Dimension, '.', 1) AS Dimension,
  toStartOfDay(AggTime) AS AggTimeDay,
  toMonth(AggTimeDay) AS AggTimeMonth,
  toStartOfMonth(AggTimeDay) AS StartOfMonth,
  toLastDayOfMonth(AggTimeDay) AS EndOfMonth,
  sum(BytesCount) AS BytesCountTotal,
  sum(EventsCount) AS EventsCountTotal
FROM analytics.analytics_ingestion_main
WHERE ProjectId IN {p0:Array(String)} AND toMonth(AggTime) = {p1:UInt64} AND toYear(AggTime) = {p2:UInt64} GROUP BY TenantId, ProjectId, Dimension, AggTimeDay ORDER BY TenantId, ProjectId, Dimension, AggTimeDay`,
			clickhouse.Parameters{
				"p0": "['" + metrics.RandomProjectID + "']",
				"p1": fmt.Sprintf("%d", month),
				"p2": fmt.Sprintf("%d", year),
			},
		),
		Entry(
			"for a given feature",
			&QueryFilters{
				ProjectIDs: []string{metrics.RandomProjectID},
				Feature:    "metrics",
			},
			`
SELECT
  TenantId,
  ProjectId,
  substringIndex(Dimension, '.', 1) AS Dimension,
  toStartOfDay(AggTime) AS AggTimeDay,
  toMonth(AggTimeDay) AS AggTimeMonth,
  toStartOfMonth(AggTimeDay) AS StartOfMonth,
  toLastDayOfMonth(AggTimeDay) AS EndOfMonth,
  sum(BytesCount) AS BytesCountTotal,
  sum(EventsCount) AS EventsCountTotal
FROM analytics.analytics_ingestion_main
WHERE ProjectId IN {p0:Array(String)} AND Dimension = {p1:String} GROUP BY TenantId, ProjectId, Dimension, AggTimeDay ORDER BY TenantId, ProjectId, Dimension, AggTimeDay`,
			clickhouse.Parameters{
				"p0": "['" + metrics.RandomProjectID + "']",
				"p1": "metrics",
			},
		),
	)
})
