package analytics

import (
	"context"
	"math"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"go.opentelemetry.io/collector/pdata/pmetric"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

const (
	targetMetric string = "sample-metric-name"
)

const baseStorageQueryTmpl = `
SELECT
  disk_name,
  database,
  table,
  sum(data_compressed_bytes) AS compressed_size_bytes,
  sum(data_uncompressed_bytes) AS uncompressed_size_bytes,
  sum(primary_key_bytes_in_memory) AS primary_keys_size,
  round(uncompressed_size_bytes / compressed_size_bytes, 2) AS compression_rate,
  sum(rows) AS rows_count,
  count() AS parts_count
FROM system.parts
WHERE
  active = 1
  AND database IN ?
  AND table IN ?
GROUP BY disk_name, database, table
`

type storageQueryRows struct {
	DiskName              string  `ch:"disk_name"`
	DatabaseName          string  `ch:"database"`
	TableName             string  `ch:"table"`
	CompressedSizeBytes   uint64  `ch:"compressed_size_bytes"`
	UncompressedSizeBytes uint64  `ch:"uncompressed_size_bytes"`
	PrimaryKeysSize       uint64  `ch:"primary_keys_size"`
	CompressionRate       float64 `ch:"compression_rate"`
	RowsCount             uint64  `ch:"rows_count"`
	PartsCount            uint64  `ch:"parts_count"`
}

func generateRandomGaugeMetricsData(
	ctx context.Context,
	d *metrics.DataGenerator,
	startTimestamp time.Time,
	endTimestamp time.Time,
) error {
	// ensure ingestion time is aligned for ALL data to be aggregated correctly when querying
	refTime := time.Now()
	// machine 1
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, refTime, metrics.MetricOptions{
		MetricName: targetMetric,
		MetricType: pmetric.MetricTypeGauge.String(),
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-001",
			"host": "ams4-host-001",
		},
		DatapointsCount: 3,
	}); err != nil {
		return err
	}
	// machine 2
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, refTime, metrics.MetricOptions{
		MetricName: targetMetric,
		MetricType: pmetric.MetricTypeGauge.String(),
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-001",
			"host": "ams4-host-002",
		},
		DatapointsCount: 4,
	}); err != nil {
		return err
	}
	// machine 3
	if err := d.GenerateMetrics(ctx, startTimestamp, endTimestamp, refTime, metrics.MetricOptions{
		MetricName: targetMetric,
		MetricType: pmetric.MetricTypeGauge.String(),
		Attributes: map[string]string{
			"dc":   "ams4",
			"rack": "ams4-rack-002",
			"host": "ams4-host-003",
		},
		DatapointsCount: 5,
	}); err != nil {
		return err
	}
	return nil
}

var _ = Describe("analytics", Ordered, Serial, func() {
	var (
		ctx     context.Context
		cancel  context.CancelFunc
		testEnv *testutils.ClickHouseServer
		conn    driver.Conn
		err     error
	)

	BeforeAll(func() {
		// ensure the tests run within the stipulated timeout
		ctx, cancel = context.WithTimeout(context.Background(), 120*time.Second)
		// create test CH server
		testEnv, conn, err = testutils.NewClickHouseServerAndConnection(ctx)
		Expect(err).ToNot(HaveOccurred())
		DeferCleanup(func(ctx SpecContext) {
			testEnv.Terminate(ctx)
		})

		Expect(testEnv.CreateAllDatabasesAndRunMigrations(ctx)).To(Succeed())
	})

	AfterAll(func() {
		// cancel context
		cancel()
	})

	Context("for storage", func() {
		DescribeTable(
			"for metrics",
			func(mType pmetric.MetricType) {
				endTimestamp := time.Now().UTC()
				startTimestamp := endTimestamp.Add(-1 * 10 * time.Second).UTC()
				// create a data generator
				d := metrics.NewDataGenerator(conn, logger.Desugar())
				// generate test data
				err = generateRandomGaugeMetricsData(ctx, d, startTimestamp, endTimestamp)
				Expect(err).ToNot(HaveOccurred())
				// query actual CH storage post ingestion
				var (
					totalEventCount int64
					totalBytesCount int64
				)
				{
					qb := clickhouse.NewQueryBuilder()
					qb.Build(
						baseStorageQueryTmpl,
						clickhouse.Array[string]([]string{constants.MetricsDatabaseName}),
						clickhouse.Array[string]([]string{constants.MetricsGaugeTableName}),
					)

					var rows []storageQueryRows
					err = conn.Select(qb.Context(ctx), &rows, qb.SQL())
					Expect(err).ToNot(HaveOccurred())

					for _, r := range rows {
						totalEventCount += int64(r.RowsCount)
						totalBytesCount += int64(r.UncompressedSizeBytes)
					}
				}
				// compute expected response
				refTime := time.Now().UTC()
				month := refTime.Month()
				year := refTime.Year()

				querier := chQuerier{db: conn, logger: logger.Desugar()}
				qb, err := querier.buildQuery(&QueryFilters{
					TenantID:   metrics.RandomTenantID,
					ProjectIDs: []string{metrics.RandomProjectID},
					Month:      uint(month),
					Year:       uint(year),
				})
				Expect(err).ToNot(HaveOccurred())
				response, err := querier.executeQuery(ctx, qb)
				Expect(err).ToNot(HaveOccurred())

				r, ok := response.(*StorageAnalyticsResponse)
				Expect(ok).To(BeTrue())

				computedEvents := r.Events
				eventDetails, ok := computedEvents[int(month)]
				Expect(ok).To(BeTrue())
				Expect(eventDetails.AggregatedTotal).To(Equal(totalEventCount))

				computedStorage := r.Storage
				storageDetails, ok := computedStorage[int(month)]
				Expect(ok).To(BeTrue())

				computedBytes := storageDetails.AggregatedTotal
				expectedBytes := totalBytesCount
				delta := math.Abs(float64(expectedBytes-computedBytes) / float64(expectedBytes))

				Expect(delta).To(BeNumerically("<=", 2.0)) // less than 2% difference
			},
			Entry("for metric type: Gauge", pmetric.MetricTypeGauge),
		)
	})
})
