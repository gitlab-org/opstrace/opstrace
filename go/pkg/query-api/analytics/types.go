package analytics

import (
	"context"
	"time"
)

type QueryFilters struct {
	TenantID   string
	ProjectIDs []string
	Feature    string
	Month      uint
	Year       uint
}

type Querier interface {
	GetStorageAnalytics(
		context.Context,
		*QueryFilters,
		time.Time,
		time.Time,
	) (interface{}, error)
}
