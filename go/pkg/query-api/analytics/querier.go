package analytics

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.uber.org/zap"

	chinternal "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

type chQuerier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

// ascertain interface implementation
var _ Querier = (*chQuerier)(nil)

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		err error
	)
	if clickHouseCloudDSN != "" {
		db, err = chinternal.GetDatabaseConnection(clickHouseCloudDSN, opts, logger, 30*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting cloud database handle: %w", err)
		}
	} else {
		db, err = chinternal.GetDatabaseConnection(clickHouseDSN, opts, logger, 3*time.Second)
		if err != nil {
			return nil, fmt.Errorf("getting database handle: %w", err)
		}
	}

	return &chQuerier{
		db:     db,
		logger: logger,
	}, nil
}

func (q *chQuerier) GetStorageAnalytics(
	ctx context.Context,
	filters *QueryFilters,
	startTimestamp time.Time,
	endTimestamp time.Time,
) (interface{}, error) {
	qb, err := q.buildQuery(filters)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}
	return q.executeQuery(ctx, qb)
}

const baseStorageAnalyticsQueryTmpl = `
SELECT
  TenantId,
  ProjectId,
  substringIndex(Dimension, '.', 1) AS Dimension,
  toStartOfDay(AggTime) AS AggTimeDay,
  toMonth(AggTimeDay) AS AggTimeMonth,
  toStartOfMonth(AggTimeDay) AS StartOfMonth,
  toLastDayOfMonth(AggTimeDay) AS EndOfMonth,
  sum(BytesCount) AS BytesCountTotal,
  sum(EventsCount) AS EventsCountTotal
FROM analytics.analytics_ingestion_main
`

func (q *chQuerier) buildQuery(f *QueryFilters) (*chinternal.QueryBuilder, error) {
	builder := chinternal.NewQueryBuilder()
	builder.Build(baseStorageAnalyticsQueryTmpl)
	if len(f.ProjectIDs) == 0 {
		return nil, fmt.Errorf("unknown project(s)")
	}
	builder.Build("WHERE ProjectId IN ?", chinternal.Array[string](f.ProjectIDs))
	if f.TenantID != "" {
		builder.Build("AND TenantId = ?", chinternal.String(f.TenantID))
	}
	if f.Feature != "" {
		builder.Build("AND Dimension = ?", chinternal.String(f.Feature))
	}
	if f.Month != 0 {
		builder.Build("AND toMonth(AggTime) = ?", f.Month)
	}
	if f.Year != 0 {
		builder.Build("AND toYear(AggTime) = ?", f.Year)
	}
	builder.Build("GROUP BY TenantId, ProjectId, Dimension, AggTimeDay")
	builder.Build("ORDER BY TenantId, ProjectId, Dimension, AggTimeDay")
	return builder, nil
}

type StorageAnalyticsResponse struct {
	Events  map[int]AnalyticsDetail `json:"events"`
	Storage map[int]AnalyticsDetail `json:"storage"`
}

type AnalyticsDetail struct {
	StartTime            int64                  `json:"start_ts"`
	EndTime              int64                  `json:"end_ts"`
	AggregatedTotal      int64                  `json:"aggregated_total"`
	AggregatedPerFeature map[string]int64       `json:"aggregated_per_feature"`
	Data                 map[string][]Datapoint `json:"data"`
	DataBreakdown        string                 `json:"data_breakdown"`
	DataUnit             string                 `json:"data_unit"`
}

type Datapoint [2]int64 // time, value

type storageAnalyticsRow struct {
	TenantID         string    `ch:"TenantId"`
	ProjectID        string    `ch:"ProjectId"`
	Dimension        string    `ch:"Dimension"`
	AggTimeDay       time.Time `ch:"AggTimeDay"`
	AggTimeMonth     uint8     `ch:"AggTimeMonth"`
	StartOfMonth     time.Time `ch:"StartOfMonth"`
	EndOfMonth       time.Time `ch:"EndOfMonth"`
	BytesCountTotal  uint64    `ch:"BytesCountTotal"`
	EventsCountTotal uint64    `ch:"EventsCountTotal"`
}

func (q *chQuerier) executeQuery(
	ctx context.Context,
	qb *chinternal.QueryBuilder,
) (interface{}, error) {
	var rows []storageAnalyticsRow
	if err := q.db.Select(qb.Context(ctx), &rows, qb.SQL()); err != nil {
		return nil, fmt.Errorf("%w", err)
	}
	response := &StorageAnalyticsResponse{
		Events:  make(map[int]AnalyticsDetail),
		Storage: make(map[int]AnalyticsDetail),
	}
	for _, r := range rows {
		r := r
		response.update(&r)
	}
	return response, nil
}

func (s *StorageAnalyticsResponse) update(r *storageAnalyticsRow) {
	// dig details to be updated, initialize otherwise
	month := int(r.AggTimeMonth)
	if _, ok := s.Storage[month]; !ok {
		s.Storage[month] = AnalyticsDetail{
			StartTime:            r.StartOfMonth.UnixNano(),
			EndTime:              r.EndOfMonth.UnixNano(),
			AggregatedTotal:      0,
			AggregatedPerFeature: make(map[string]int64),
			DataUnit:             "bytes",
			Data:                 make(map[string][]Datapoint),
			DataBreakdown:        "daily",
		}
	}
	storage := s.Storage[month]

	if _, ok := s.Events[month]; !ok {
		s.Events[month] = AnalyticsDetail{
			StartTime:            r.StartOfMonth.UnixNano(),
			EndTime:              r.EndOfMonth.UnixNano(),
			AggregatedTotal:      0,
			AggregatedPerFeature: make(map[string]int64),
			DataUnit:             "",
			Data:                 make(map[string][]Datapoint),
			DataBreakdown:        "daily",
		}
	}
	events := s.Events[month]

	// total aggregate
	storage.AggregatedTotal += int64(r.BytesCountTotal)
	events.AggregatedTotal += int64(r.EventsCountTotal)

	// aggregates & breakdowns per feature
	key := r.Dimension
	storage.AggregatedPerFeature[key] += int64(r.BytesCountTotal)
	storage.Data[key] = append(storage.Data[key], Datapoint{
		r.AggTimeDay.UnixNano(), int64(r.BytesCountTotal),
	})
	events.AggregatedPerFeature[key] += int64(r.EventsCountTotal)
	events.Data[key] = append(events.Data[key], Datapoint{
		r.AggTimeDay.UnixNano(), int64(r.EventsCountTotal),
	})

	// update response
	s.Events[month] = events
	s.Storage[month] = storage
}
