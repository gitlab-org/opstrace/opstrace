package analytics

import (
	"context"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

var logger *zap.SugaredLogger
var testEnv *testutils.ClickHouseServer
var conn clickhouse.Conn

func TestAnalytics(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "Analytics", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func(ctx SpecContext) {
	config := zap.NewDevelopmentConfig()
	sink := zapcore.AddSync(GinkgoWriter)
	encoder := zapcore.NewConsoleEncoder(config.EncoderConfig)
	core := zapcore.NewCore(encoder, sink, zap.DebugLevel)
	log := zap.New(core).WithOptions(zap.ErrorOutput(sink))
	logger = log.Sugar()

	gin.SetMode(gin.DebugMode)
	gin.DefaultWriter = GinkgoWriter

	var err error
	testEnv, conn, err = testutils.NewClickHouseServerAndConnection(context.Background())
	Expect(err).ToNot(HaveOccurred())
	DeferCleanup(func(ctx SpecContext) {
		_ = testEnv.Terminate(ctx)
	})

	Expect(testEnv.CreateAllDatabasesAndRunMigrations(ctx)).To(Succeed())
})

var _ = AfterSuite(func() {
	err := logger.Sync()
	Expect(err).ToNot(HaveOccurred())
})
