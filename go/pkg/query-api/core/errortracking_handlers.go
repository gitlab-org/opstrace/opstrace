package core

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/errortracking"
)

func (e *QueryAPI) ListErrorsHandler(ctx *gin.Context) {
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	p, err := assertProjectIDIsAnInteger(projectID)
	if err != nil {
		e.logger.Error("parse projectID", zap.Error(err))
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	params := &errortracking.ListErrorsParams{
		ProjectID: p,
	}
	if err := ctx.BindQuery(params); err != nil {
		e.logger.Error("bind errors params", zap.Error(err))
		return
	}

	e.logger.Debug("list errors", zap.Any("params", params))

	items, err := e.errorsQuerier.ListErrors(ctx, params)
	if err != nil {
		e.logger.Error("list errors", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	link, cursor, err := buildListErrorsLink(ctx.Request.URL.String(), e.baseAddress, params)
	if err != nil {
		e.logger.Error("build link", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.Header("Link", link)
	result := &errortracking.ListErrorResult{
		NextPageToken: cursor,
		Body:          items,
	}
	ctx.JSON(http.StatusOK, result)
}

func (e *QueryAPI) GetErrorHandler(ctx *gin.Context) {
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	p, err := assertProjectIDIsAnInteger(projectID)
	if err != nil {
		e.logger.Error("parse projectID", zap.Error(err))
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	fp := &FingerprintParams{}
	if err := ctx.BindUri(fp); err != nil {
		e.logger.Error("bind error params", zap.Error(err))
		return
	}
	params := &errortracking.GetErrorParams{
		ProjectID:   p,
		Fingerprint: fp.Fingerprint,
	}
	if err := ctx.BindQuery(params); err != nil {
		e.logger.Error("bind error params", zap.Error(err))
		return
	}

	e.logger.Debug("get errors", zap.Any("params", params))

	items, err := e.errorsQuerier.GetError(ctx, params)
	if err != nil {
		e.logger.Error("get error", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	result := &errortracking.GetErrorResult{
		Body: items,
	}
	ctx.JSON(http.StatusOK, result)
}

type FingerprintParams struct {
	Fingerprint uint32 `uri:"fingerprint" binding:"required"`
}

func (e *QueryAPI) ListErrorEventsHandler(ctx *gin.Context) {
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	p, err := assertProjectIDIsAnInteger(projectID)
	if err != nil {
		e.logger.Error("parse projectID", zap.Error(err))
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	fp := &FingerprintParams{}
	if err := ctx.BindUri(fp); err != nil {
		e.logger.Error("bind list error event params", zap.Error(err))
		return
	}
	params := &errortracking.ListEventsParams{
		ProjectID:   p,
		Fingerprint: fp.Fingerprint,
	}
	if err := ctx.BindQuery(params); err != nil {
		e.logger.Error("bind list error event params", zap.Error(err))
		return
	}
	e.logger.Debug("list errors events", zap.Any("params", params))

	items, err := e.errorsQuerier.ListEvents(ctx, params)
	if err != nil {
		e.logger.Error("list error events", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	link, cursor, err := buildListEventsLink(ctx.Request.URL.String(), e.baseAddress, params)
	if err != nil {
		e.logger.Error("build link", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.Header("Link", link)
	result := &errortracking.ListErrorEventResult{
		NextPageToken: cursor,
		Body:          items,
	}

	ctx.JSON(http.StatusOK, result)
}

func (e *QueryAPI) ListMessages(ctx *gin.Context) {
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	p, err := assertProjectIDIsAnInteger(projectID)
	if err != nil {
		e.logger.Error("parse projectID", zap.Error(err))
		ctx.AbortWithStatus(http.StatusBadRequest)
		return
	}

	params := &errortracking.ListMessagesParams{
		ProjectID: p,
	}
	if err := ctx.BindQuery(params); err != nil {
		e.logger.Error("bind list messages params", zap.Error(err))
		return
	}
	e.logger.Debug("list messages", zap.Any("params", params))

	items, err := e.errorsQuerier.ListMessages(ctx, params)
	if err != nil {
		e.logger.Error("list messages", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	result := &errortracking.ListMessageEventResult{
		// TODO: Pagination for list message events is not implemented.
		NextPageToken: "",
		Body:          items,
	}

	ctx.JSON(http.StatusOK, result)
}

// Note(Arun): Temp workaround to assert that projectID is a valid integer before drafting queries.
// Error Tracking schemas are based on strict integer based projectIDs which will need to be migrated.
func assertProjectIDIsAnInteger(projectID string) (uint64, error) {
	p, err := strconv.ParseUint(projectID, 10, 64)
	if err != nil {
		return p, fmt.Errorf("project id is not an integer: %s", projectID)
	}
	return p, nil
}

func buildListErrorsLink(
	requestURL string,
	baseAddress string,
	params *errortracking.ListErrorsParams,
) (string, string, error) {
	page, err := errortracking.DecodePage(params.Cursor)
	if err != nil {
		return "", "", err
	}
	prev := page - 1
	prevLink := ""
	next := page + 1
	cursor := ""

	buildListErrorsLinkPage := func(page int) (string, error) {
		u, err := url.Parse(baseAddress + requestURL)
		if err != nil {
			return "", fmt.Errorf("failed to parsed url %w", err)
		}
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Query != nil {
			q.Set("query", *params.Query)
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		if params.Status != nil {
			q.Set("status", *params.Status)
		}
		cursor, err = errortracking.EncodePage(page)
		if err != nil {
			return "", err
		}
		q.Set("cursor", cursor)
		u.RawQuery = q.Encode()

		return u.String(), nil
	}

	if prev >= 1 {
		prevLink, err = buildListErrorsLinkPage(prev)
		if err != nil {
			return "", "", err
		}
	}
	nextLink, err := buildListErrorsLinkPage(next)
	if err != nil {
		return "", "", err
	}

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link, cursor, nil
}

func buildListEventsLink(
	requestURL string,
	baseAddress string,
	params *errortracking.ListEventsParams,
) (string, string, error) {
	page, err := errortracking.DecodePage(params.Cursor)
	if err != nil {
		return "", "", err
	}
	prev := page - 1
	prevLink := ""
	next := page + 1
	cursor := ""

	buildListEventsLinkPage := func(page int) (string, error) {
		u, err := url.Parse(baseAddress + requestURL)
		if err != nil {
			return "", fmt.Errorf("failed to parsed url %w", err)
		}
		q := u.Query()

		if params.Limit != nil {
			q.Set("limit", strconv.Itoa(int(*params.Limit)))
		}
		if params.Sort != nil {
			q.Set("sort", *params.Sort)
		}
		cursor, err = errortracking.EncodePage(page)
		if err != nil {
			return "", err
		}
		q.Set("cursor", cursor)
		u.RawQuery = q.Encode()

		return u.String(), nil
	}

	if prev >= 1 {
		prevLink, err = buildListEventsLinkPage(prev)
		if err != nil {
			return "", "", err
		}
	}
	nextLink, err := buildListEventsLinkPage(next)
	if err != nil {
		return "", "", err
	}

	link := ""
	if prevLink != "" {
		link = fmt.Sprintf(`<%s>; rel="prev", `, prevLink)
	}
	link += fmt.Sprintf(`<%s>; rel="next"`, nextLink)

	return link, cursor, nil
}
