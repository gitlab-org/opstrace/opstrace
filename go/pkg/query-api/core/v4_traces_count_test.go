package core

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
)

var _ = Context("V4 trace rate handler", func() {
	var (
		router *gin.Engine
	)

	type expected struct {
		dbErr      error
		statusCode int
		json       string
	}

	test := func(projectID string, url string, expected expected, expectedFilter *traces.TracesParams) {
		q := &traces.MockQuerier{
			Err: expected.dbErr,
		}

		qm := &metrics.MockQuerier{}
		ql := &logs.MockQuerier{}
		api := NewQueryAPI(logger.Desugar(), qm, ql, nil, nil, q, nil)
		api.SetRoutes(router)

		headers := map[string]string{}
		headers["X-Target-Projectid"] = projectID

		recorder := httptest.NewRecorder()
		testReq, err := http.NewRequest(http.MethodGet, url, nil)
		Expect(err).NotTo(HaveOccurred())
		for k, v := range headers {
			testReq.Header.Add(k, v)
		}
		router.ServeHTTP(recorder, testReq)
		Expect(recorder.Code).To(Equal(expected.statusCode))
		if expected.json != "" {
			Expect(recorder).To(HaveHTTPBody(MatchJSON(expected.json)))
		}

		Expect(q.PassedFilter).To(Equal(expectedFilter))
	}

	BeforeEach(func() {
		router = gin.New()
	})

	Context("traces RED metrics", func() {
		DescribeTable("list services", test,
			Entry("should return 200 with no parameters", "1", "/v4/query/traces/analytics",
				expected{
					statusCode: http.StatusOK,
					json: `{
					  "project_id": "1",
					  "results": [
						{
						  "interval": 1000000,
						  "count": 2,
						  "p90_duration_nano": 1000,
						  "p95_duration_nano": 1000,
						  "p75_duration_nano": 1000,
						  "p50_duration_nano": 1000,
						  "error_count": 3
						}
					  ]
					  }`,
				},
				&traces.TracesParams{
					PageSize: 100,
				},
			),
			Entry("should return 500 with db error", "1", "/v4/query/traces/analytics",
				expected{
					dbErr:      fmt.Errorf("test err"),
					statusCode: http.StatusInternalServerError,
				},
				nil,
			),
			Entry("should return 200 with period parameters", "1", "/v4/query/traces/analytics?period=5m",
				expected{
					statusCode: http.StatusOK,
					json: `{
					  "project_id": "1",
					  "results": [
						{
						  "interval": 1000000,
						  "count": 2,
						  "p90_duration_nano": 1000,
						  "p95_duration_nano": 1000,
						  "p75_duration_nano": 1000,
						  "p50_duration_nano": 1000,
						  "error_count": 3
						}
					  ]
					  }`,
				},
				&traces.TracesParams{
					PageSize: 100,
					Period:   "5m",
				},
			),

			Entry("should return 200 with start and end parameters", "1", "/v4/query/traces/analytics?start_time=2023-11-23T11:24:10Z&end_time=2023-11-23T13:24:10Z",
				expected{
					statusCode: http.StatusOK,
					json: `{
					  "project_id": "1",
					  "results": [
						{
						  "interval": 1000000,
						  "count": 2,
						  "p90_duration_nano": 1000,
						  "p95_duration_nano": 1000,
						  "p75_duration_nano": 1000,
						  "p50_duration_nano": 1000,
						  "error_count": 3
						}
					  ]
					  }`,
				},
				&traces.TracesParams{
					PageSize:  100,
					StartTime: time.Date(2023, 11, 23, 11, 24, 10, 0, time.UTC),
					EndTime:   time.Date(2023, 11, 23, 13, 24, 10, 0, time.UTC),
				},
			),
		)
	})
})
