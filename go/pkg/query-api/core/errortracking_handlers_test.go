package core

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp/cmpopts"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/errortracking"
)

var _ = Context("ErrorTracking Query API", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	Context("List Errors Handler", func() {
		defaultLimit := int64(20)
		defaultPeriod := "30d"
		defaultStatsPeriod := "24h"
		defaultSort := "last_seen_desc"
		defaultStatus := "unresolved"

		makeParams := func(
			projectID uint64,
			limit int64,
			period, statsPeriod, sort, status string,
		) *errortracking.ListErrorsParams {
			return &errortracking.ListErrorsParams{
				Cursor:      nil,
				Limit:       &limit,
				ProjectID:   projectID,
				Query:       nil,
				QueryPeriod: &period,
				Sort:        &sort,
				StatsPeriod: &statsPeriod,
				Status:      &status,
			}
		}

		DescribeTable("should return expected status",
			func(
				projectID string,
				url string,
				statusCode int,
				expectedFilter *errortracking.ListErrorsParams,
				expectErr error,
			) {
				eq := &errortracking.MockQuerier{
					Err: expectErr,
				}
				api := NewQueryAPI(logger.Desugar(), nil, nil, nil, nil, nil, eq)
				api.SetRoutes(router)

				headers := map[string]string{}
				headers["X-Target-Projectid"] = projectID

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				for k, v := range headers {
					testReq.Header.Add(k, v)
				}
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))
				if expectedFilter != nil {
					Expect(eq.PassedFilter).To(BeComparableTo(
						expectedFilter,
						cmpopts.IgnoreUnexported(errortracking.ListErrorsParams{}),
					))
				}
			},
			Entry(
				"returns 200 for a list of errors",
				"1",
				"/v4/query/errortracking/errors",
				http.StatusOK,
				makeParams(1, defaultLimit, defaultPeriod, defaultStatsPeriod, defaultSort, defaultStatus),
				nil,
			),
			Entry(
				"returns 200 with query params",
				"1",
				"/v4/query/errortracking/errors?limit=50&status=resolved",
				http.StatusOK,
				makeParams(1, 50, defaultPeriod, defaultStatsPeriod, defaultSort, "resolved"),
				nil,
			),
			Entry(
				"returns 400 with invalid sort",
				"1",
				"/v4/query/errortracking/errors?sort=invalid_desc",
				http.StatusBadRequest,
				nil,
				nil,
			),
			Entry(
				"returns 400 with invalid status",
				"1",
				"/v4/query/errortracking/errors?status=invalid",
				http.StatusBadRequest,
				nil,
				nil,
			),
			Entry(
				"returns 500 with database error",
				"1",
				"/v4/query/errortracking/errors",
				http.StatusInternalServerError,
				nil,
				errors.New("database error"),
			),
		)
	})

	Context("Get Errors Handler", func() {
		makeParams := func(
			projectID uint64,
			fingerprint uint32,
		) *errortracking.GetErrorParams {
			return &errortracking.GetErrorParams{
				Fingerprint: fingerprint,
				ProjectID:   projectID,
			}
		}

		DescribeTable("should return expected status",
			func(
				projectID string,
				url string,
				statusCode int,
				expectedFilter *errortracking.GetErrorParams,
				expectErr error,
			) {
				eq := &errortracking.MockQuerier{
					Err: expectErr,
				}
				api := NewQueryAPI(logger.Desugar(), nil, nil, nil, nil, nil, eq)
				api.SetRoutes(router)

				headers := map[string]string{}
				headers["X-Target-Projectid"] = projectID

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				for k, v := range headers {
					testReq.Header.Add(k, v)
				}
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))
				if expectedFilter != nil {
					Expect(eq.PassedFilter).To(BeComparableTo(
						expectedFilter,
						cmpopts.IgnoreUnexported(errortracking.GetErrorParams{}),
					))
				}
			},
			Entry(
				"returns 200 for a list of errors",
				"1",
				"/v4/query/errortracking/error/123",
				http.StatusOK,
				makeParams(1, 123),
				nil,
			),
			Entry(
				"returns 500 with a db error",
				"1",
				"/v4/query/errortracking/error/123",
				http.StatusInternalServerError,
				nil,
				errors.New("database error"),
			),
		)
	})

	Context("List Events Handler", func() {
		makeParams := func(
			projectID uint64,
			fingerprint uint32,
			limit int64,
			sort string,
		) *errortracking.ListEventsParams {
			return &errortracking.ListEventsParams{
				Cursor:      nil,
				Fingerprint: fingerprint,
				Limit:       &limit,
				ProjectID:   projectID,
				Sort:        &sort,
			}
		}

		DescribeTable("should return expected status",
			func(
				projectID string,
				url string,
				statusCode int,
				expectedFilter *errortracking.ListEventsParams,
				expectErr error,
			) {
				eq := &errortracking.MockQuerier{
					Err: expectErr,
				}
				api := NewQueryAPI(logger.Desugar(), nil, nil, nil, nil, nil, eq)
				api.SetRoutes(router)

				headers := map[string]string{}
				headers["X-Target-Projectid"] = projectID

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				for k, v := range headers {
					testReq.Header.Add(k, v)
				}
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))
				if expectedFilter != nil {
					Expect(eq.PassedFilter).To(BeComparableTo(
						expectedFilter,
						cmpopts.IgnoreUnexported(errortracking.ListEventsParams{}),
					))
				}
			},
			Entry(
				"returns 200 for a list of errors",
				"1",
				"/v4/query/errortracking/error/123/events",
				http.StatusOK,
				makeParams(1, 123, 20, "occurred_at_desc"),
				nil,
			),
			Entry(
				"returns 500 with a db error",
				"1",
				"/v4/query/errortracking/error/123/events",
				http.StatusInternalServerError,
				nil,
				errors.New("database error"),
			),
		)
	})

	stringPointer := func(s string) *string {
		return &s
	}
	int64Pointer := func(i int64) *int64 {
		return &i
	}

	Context("Building List Errors Link Header", func() {
		requestURL := "/v4/query/errortracking/errors"
		baseAddress := "localhost:8080"
		endpoint := baseAddress + requestURL
		DescribeTable(
			"should return expected link",
			func(
				params *errortracking.ListErrorsParams,
				expectedLink string,
				expectedErr error,
			) {

				link, _, err := buildListErrorsLink(requestURL, baseAddress, params)
				if expectedErr != nil {
					Expect(err).To(MatchError(expectedErr))
					return
				}
				Expect(link).To(Equal(expectedLink))
			},
			Entry(
				"should return valid next link",
				&errortracking.ListErrorsParams{},
				fmt.Sprintf(`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="next"`, endpoint),
				nil,
			),
			Entry("should return valid next and prev link",
				&errortracking.ListErrorsParams{
					// cursor is the base64 encoded json string: {"page":3}
					Cursor: stringPointer("eyJwYWdlIjozfQo="),
				},
				fmt.Sprintf(
					`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D>; rel="next"`,
					endpoint, endpoint),
				nil,
			),
			Entry("should return valid next link with all params",
				&errortracking.ListErrorsParams{
					Limit:  int64Pointer(2),
					Query:  stringPointer("foobar"),
					Sort:   stringPointer("first_seen_desc"),
					Status: stringPointer("resolved"),
				},
				fmt.Sprintf(
					`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2&query=foobar&sort=first_seen_desc&status=resolved>; rel="next"`,
					endpoint,
				),
				nil,
			),
		)
	})

	Context("Building List Error Events Link Header", func() {
		requestURL := "/v4/query/errortracking/error/1/events"
		baseAddress := "localhost:8080"
		endpoint := baseAddress + requestURL
		DescribeTable(
			"should return expected link",
			func(
				params *errortracking.ListEventsParams,
				expectedLink string,
				expectedErr error,
			) {

				link, _, err := buildListEventsLink(requestURL, baseAddress, params)
				if expectedErr != nil {
					Expect(err).To(MatchError(expectedErr))
					return
				}
				Expect(link).To(Equal(expectedLink))
			},
			Entry(
				"should return valid next link",
				&errortracking.ListEventsParams{},
				fmt.Sprintf(`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="next"`, endpoint),
				nil,
			),
			Entry("should return valid next and prev link",
				&errortracking.ListEventsParams{
					// cursor is the base64 encoded json string: {"page":3}
					Cursor: stringPointer("eyJwYWdlIjozfQo="),
				},
				fmt.Sprintf(
					`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D>; rel="prev", <%s?cursor=eyJwYWdlIjo0fQ%%3D%%3D>; rel="next"`,
					endpoint, endpoint),
				nil,
			),
			Entry("should return valid next link with all params",
				&errortracking.ListEventsParams{
					Limit: int64Pointer(2),
					Sort:  stringPointer("occurred_at_asc"),
				},
				fmt.Sprintf(
					`<%s?cursor=eyJwYWdlIjoyfQ%%3D%%3D&limit=2&sort=occurred_at_asc>; rel="next"`,
					endpoint,
				),
				nil,
			),
		)
	})
})
