package core

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/analytics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
)

type QueryAPI struct {
	logger           *zap.Logger
	metricsQuerier   metrics.Querier
	logsQuerier      logs.Querier
	analyticsQuerier analytics.Querier
	alertsQuerier    alerts.Querier
	tracesQuerier    traces.Querier
	errorsQuerier    errortracking.Querier
	// baseAddress is required for creating `Link` headers for pagination.
	// This is to keep backward compatibility for Error Tracking query API.
	baseAddress string
}

func NewQueryAPI(
	l *zap.Logger,
	mq metrics.Querier,
	lq logs.Querier,
	aq analytics.Querier,
	alertsQuerier alerts.Querier,
	tracesQuerier traces.Querier,
	errorsQuerier errortracking.Querier,
) *QueryAPI {
	return &QueryAPI{
		logger:           l,
		metricsQuerier:   mq,
		logsQuerier:      lq,
		analyticsQuerier: aq,
		alertsQuerier:    alertsQuerier,
		tracesQuerier:    tracesQuerier,
		errorsQuerier:    errorsQuerier,
	}
}

func (e *QueryAPI) SetRoutes(router *gin.Engine) {
	e.setVersionedRoutes(router.Group("/v3/query/:project_id"))
	// This new route is used for all OIDC requests coming from Workhorse.
	// The project ID is stored in the request header
	e.setVersionedRoutes(router.Group("/v4/query"))

	// `GET /readyz` is used by k8s for healtchecking the application.
	router.GET("/readyz", func(ginCtx *gin.Context) {
		ginCtx.String(http.StatusOK, "Success")
	})

	router.NoRoute(func(ginCtx *gin.Context) {
		ginCtx.AbortWithStatus(http.StatusNotFound)
	})
}

func (e *QueryAPI) SetBaseAddress(address string) {
	e.baseAddress = address
}

func (e *QueryAPI) setVersionedRoutes(routerGroup *gin.RouterGroup) {
	metricsGroup := routerGroup.Group("/metrics")
	{
		metricsGroup.GET("/autocomplete", e.MetricsAutoCompleteHandler)
		metricsGroup.GET("/search", e.MetricsSearchHandler)
		metricsGroup.GET("/searchmetadata", e.MetricsSearchMetadataHandler)
	}

	logsGroup := routerGroup.Group("/logs")
	{
		logsGroup.GET("/search", e.SearchLogsHandler)
		logsGroup.GET("/searchmetadata", e.SearchLogsMetadataHandler)
	}

	analyticsGroup := routerGroup.Group("/analytics")
	{
		analyticsGroup.GET("/storage", e.StorageAnalyticsHandler)
	}

	alertsGroup := routerGroup.Group("/alerts")
	{
		alertsGroup.GET("", e.AlertsHandler)
	}

	servicesGroup := routerGroup.Group("/services")
	{
		servicesGroup.GET("", e.ServicesHandler)
		servicesGroup.GET("/:service_name/operations", e.ServiceOperationsHandler)
	}

	tracesGroup := routerGroup.Group("/traces")
	{
		tracesGroup.GET("", e.TracesHandler)
		tracesGroup.GET("/:id", e.TraceHandler)
		tracesGroup.GET("/analytics", e.TraceAnalyticsHandler)
		tracesGroup.GET("/services", e.ServicesHandler)
		tracesGroup.GET("/services/:service_name/operations", e.ServiceOperationsHandler)
	}

	errortrackingGroup := routerGroup.Group("/errortracking")
	{
		errortrackingGroup.GET("/errors", e.ListErrorsHandler)
		errortrackingGroup.GET("/error/:fingerprint", e.GetErrorHandler)
		errortrackingGroup.GET("/error/:fingerprint/events", e.ListErrorEventsHandler)
		errortrackingGroup.GET("/messages", e.ListMessages)
	}
}
