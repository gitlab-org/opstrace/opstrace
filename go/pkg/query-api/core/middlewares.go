package core

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"

	authproxy "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/auth-proxy"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

func OIDCAuthMiddleware(logger *zap.Logger, authenticator *authproxy.Authenticator) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		// check if readyz request and return early if so
		if ctx.Request.URL.Path == "/readyz" {
			ctx.Status(http.StatusOK)
			return
		}

		// requests with `/v3/query/:project_id` are deprecated and to be handled by gatekeeper for auth
		// skip checking such requests because they would've been auth already and will not have JWT token
		if strings.HasPrefix(ctx.Request.URL.Path, "/v3") {
			ctx.Next()
			return
		}

		if authenticator == nil {
			logger.Error("no authenticator provided")
			ctx.Status(http.StatusUnauthorized)
			return
		}

		logger := logger.With(
			zap.String("method", ctx.Request.Method),
			zap.String("host", ctx.Request.Host),
			zap.String("path", ctx.Request.URL.Path),
			zap.String("X-Gitlab-Realm", ctx.GetHeader("X-Gitlab-Realm")),
			zap.String("X-Gitlab-Host-Name", ctx.GetHeader("X-Gitlab-Host-Name")),
			zap.String("X-Gitlab-Version", ctx.GetHeader("X-Gitlab-Version")),
			zap.String("X-Gitlab-Instance-Id", ctx.GetHeader("X-Gitlab-Instance-Id")),
			zap.String("X-Gitlab-Global-User-Id", ctx.GetHeader("X-Gitlab-Global-User-Id")),
			zap.String("X-GitLab-Namespace-id", ctx.GetHeader("X-GitLab-Namespace-id")),
			zap.String("X-GitLab-Project-id", ctx.GetHeader("X-GitLab-Project-id")),
			zap.String("client-ip", ctx.GetHeader("X-Forwarded-For")),
			zap.String("user-agent", ctx.GetHeader("User-Agent")))

		ok, errors := authenticator.Authenticate(ctx.Request)
		if !ok {
			logger.Error("deny", zap.Errors("reasons -->", errors))
			ctx.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		common.RewriteHeaders(ctx.Request.Header)
		logger.Debug("allowed")
		ctx.Next()
	}
}
