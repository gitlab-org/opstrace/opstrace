package core

import (
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	authproxy "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/auth-proxy"
)

var _ = Context("Query API Middlewares", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	Context("OIDC Auth Middleware", func() {
		testFunc := func(url string, statusCode int, authenticator *authproxy.Authenticator) {
			router.Use(OIDCAuthMiddleware(logger.Desugar(), authenticator))
			router.GET("/*path", func(c *gin.Context) {
				c.Status(http.StatusOK)
			})
			recorder := httptest.NewRecorder()
			testReq, err := http.NewRequest(http.MethodGet, url, nil)
			Expect(err).NotTo(HaveOccurred())
			router.ServeHTTP(recorder, testReq)
			Expect(recorder.Code).To(Equal(statusCode))
		}

		DescribeTable(
			"route requests to OIDC auth middleware",
			testFunc,
			Entry("returns 401 if no authenticator is set",
				"v4/query/traces",
				http.StatusUnauthorized,
				nil,
			),
			Entry(
				"returns 200 on /readyz",
				"/readyz",
				http.StatusOK,
				nil,
			),
			Entry(
				"returns 200 on /v3 paths",
				"/v3/query/123/traces",
				http.StatusOK,
				nil,
			),
		)
	})
})
