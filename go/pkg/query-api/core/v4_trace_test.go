package core

import (
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
)

var _ = Context("V4 trace handler", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	type expected struct {
		statusCode int
		json       string
	}

	test := func(projectID string, url string, expected expected) {
		q := &traces.MockQuerier{}

		qm := &metrics.MockQuerier{}
		ql := &logs.MockQuerier{}
		api := NewQueryAPI(logger.Desugar(), qm, ql, nil, nil, q, nil)
		api.SetRoutes(router)

		headers := map[string]string{}
		headers["X-Target-Projectid"] = projectID

		recorder := httptest.NewRecorder()
		testReq, err := http.NewRequest(http.MethodGet, url, nil)
		Expect(err).NotTo(HaveOccurred())

		for k, v := range headers {
			testReq.Header.Add(k, v)
		}

		router.ServeHTTP(recorder, testReq)
		Expect(recorder).To(HaveHTTPStatus(expected.statusCode))
		if expected.json != "" {
			Expect(recorder).To(HaveHTTPBody(MatchJSON(expected.json)))
		}
	}

	DescribeTable("get trace", test,
		Entry("should return 200 with trace", "1", "/v4/query/traces/12b797ee-7c32-a226-bf56-d4b3af18af55",
			expected{
				statusCode: http.StatusOK,
				json: `{
					"timestamp":   "2020-08-11T05:02:12.123456789Z",
					"timestamp_nano": 1597122132123456789,
					"trace_id":     "12b797ee-7c32-a226-bf56-d4b3af18af55",
					"service_name": "sample-service",
					"operation":   "sample-operation",
					"duration_nano": 0,
					"status_code":  "OK",
					"total_spans": 0,
					"spans": [
						{
							"timestamp":     "2020-08-11T05:02:12.123456789Z",
							"timestamp_nano": 1597122132123456789,
							"span_id":        "uuid",
							"trace_id":       "12b797ee-7c32-a226-bf56-d4b3af18af55",
							"service_name":   "sample-service",
							"operation":     "sample-operation",
							"duration_nano":  1000,
							"parent_span_id":  "",
							"status_code":    "OK",
							"status_message": "foo",
							"scope_version":  "bar",
							"scope_name":     "baz",
							"span_kind":      "server",
							"trace_state":    "foobar",
							"span_attributes": {
								"http.route":  "/test",
								"custom.attr": "my_value"
							},
							"resource_attributes": {
								"service.name": "sample-service"
							}
						},
						{
							"timestamp":     "2020-08-11T05:02:12.133456789Z",
							"timestamp_nano": 1597122132133456789,
							"span_id":        "uuid",
							"trace_id":       "12b797ee-7c32-a226-bf56-d4b3af18af55",
							"service_name":   "sample-service",
							"operation":     "sample-operation",
							"duration_nano":  1000,
							"parent_span_id":  "",
							"status_code":    "OK",
							"status_message": "foo",
							"scope_version":  "bar",
							"scope_name":     "baz",
							"span_kind":      "server",
							"trace_state":    "foobar",
							"span_attributes": {
								"http.route":  "/test",
								"custom.attr": "my_value"
							},
							"resource_attributes": {
								"service.name": "sample-service"
							}
						}
					]
				}`,
			}),
		Entry("should return 400 with invalid trace id", "1", "/v4/query/traces/12",
			expected{
				statusCode: http.StatusBadRequest,
			}),
		Entry("should return 404 when traceId not found in db", "1", "/v4/query/traces/12b797ee-7c32-a226-bf56-d4b3af18af56",
			expected{
				statusCode: http.StatusNotFound,
			}),
	)

})
