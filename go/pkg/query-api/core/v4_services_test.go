package core

import (
	"errors"
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
)

var _ = Context("V4 services handler", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	type expected struct {
		dbErr      error
		statusCode int
		json       string
	}

	test := func(projectID string, url string, expected expected) {
		q := &traces.MockQuerier{
			Err: expected.dbErr,
		}

		qm := &metrics.MockQuerier{}
		ql := &logs.MockQuerier{}
		api := NewQueryAPI(logger.Desugar(), qm, ql, nil, nil, q, nil)
		api.SetRoutes(router)

		headers := map[string]string{}
		headers["X-Target-Projectid"] = projectID

		recorder := httptest.NewRecorder()
		testReq, err := http.NewRequest(http.MethodGet, url, nil)
		Expect(err).NotTo(HaveOccurred())

		for k, v := range headers {
			testReq.Header.Add(k, v)
		}

		router.ServeHTTP(recorder, testReq)
		Expect(recorder).To(HaveHTTPStatus(expected.statusCode))
		if expected.json != "" {
			Expect(recorder).To(HaveHTTPBody(MatchJSON(expected.json)))
		}
	}

	DescribeTable("list services", test,
		Entry("should return 200 with service name", "1", "/v4/query/services",
			expected{
				statusCode: http.StatusOK,
				json: `{
				"services": [{
					"name": "sample-service"
				}]}`,
			}),
		Entry("should return 500 with db error", "1", "/v4/query/services",
			expected{
				dbErr:      errors.New("test err"),
				statusCode: http.StatusInternalServerError,
			}),
	)

	DescribeTable("list service operations", test,
		Entry("should return 200 with service operations", "1", "/v4/query/services/sample-service/operations",
			expected{
				statusCode: http.StatusOK,
				json: `{
					"operations": [{
						"name": "sample-operation"
					}]
				}`}),
		Entry("should return 500 with db error", "1", "/v4/query/services/sample-service/operations",
			expected{
				dbErr:      errors.New("test err"),
				statusCode: http.StatusInternalServerError,
			}),
	)

})
