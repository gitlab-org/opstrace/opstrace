package core

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/gin-gonic/gin"
	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/analytics"
	qcommon "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
)

type timeQueryParams struct {
	Period string `form:"period"`

	// Time format is RFC3339Nano
	StartTime time.Time `form:"start_time" time_format:"2006-01-02T15:04:05.999999999Z07:00"`
	EndTime   time.Time `form:"end_time" time_format:"2006-01-02T15:04:05.999999999Z07:00"`
}

var (
	errMalformedQueryFilters error = errors.New("malformed query filters")
	errMalformedGroupbyAttrs error = errors.New("malformed groupby attributes")
)

type MetricFiltersStr []string

func (m *MetricFiltersStr) Parsed() ([]metrics.Filter, error) {
	f := []metrics.Filter{}
	for _, filterStr := range *m {
		filterComps := strings.Split(filterStr, ",")
		if len(filterComps) != 3 {
			return nil, errMalformedQueryFilters
		}
		f = append(f, metrics.Filter{
			Key:      filterComps[0],
			Operator: filterComps[1],
			Value:    filterComps[2],
		})
	}
	return f, nil
}

type MetricGroupbyAttrsStr string

func (m *MetricGroupbyAttrsStr) Parsed() ([]string, error) {
	attrComps := make([]string, 0)
	passedAttrs := string(*m)
	if passedAttrs != "" {
		attrComps = strings.Split(passedAttrs, ",")
		if len(attrComps) == 0 {
			return nil, errMalformedGroupbyAttrs
		}
	}
	return attrComps, nil
}

type metricsQueryParams struct {
	timeQueryParams
	MetricName               string                `form:"mname" binding:"required"`
	MetricType               string                `form:"mtype" binding:"required"`
	MetricVisual             string                `form:"mvisual"`
	MetricFilters            MetricFiltersStr      `form:"attrs"`
	MetricGroupbyFunc        string                `form:"groupby_fn"`
	MetricGroupbyAttrs       MetricGroupbyAttrsStr `form:"groupby_attrs"`
	MetricCorrelationTraceID string                `form:"trace_id"`
}

type logsQueryParams struct {
	// NOTE(prozlach): 256 chars limits for serviceName is arbitrary - I have
	// not found any info in the otel spec specifying the bounds. The limit
	// itself is meant to enfoce sane length for queries.
	ServiceNames       []string `form:"service_name" binding:"dive,min=1,max=256"`
	NotServiceNames    []string `form:"not[service_name]" binding:"dive,min=1,max=256"`
	SeverityNumbers    []int32  `form:"severity_number" binding:"dive,numeric,min=0,max=24"`
	NotSeverityNumbers []int32  `form:"not[severity_number]" binding:"dive,numeric,min=0,max=24"`
	// NOTE(prozlach): trace ids have high cardinality and bloom filters do not
	// work for `!=` queries making it possible for the user to query for e.g.
	// `notTraceID=foobar` which results in full scan of the dataset. It is
	// even worse for analytics queries where we do not have `limit` clause
	// added to queries (i.e. no pagination), resulting in queries usually
	// scanning whole dataset. We may re-add it after rethinking how to
	// implement it if there is a request for it from product.
	TraceIDs      []string `form:"trace_id" binding:"dive,uuid"`
	SpanIDs       []string `form:"span_id" binding:"dive,hexadecimal,len=16"`
	Fingerprints  []string `form:"fingerprint" binding:"dive,hexadecimal,len=16"`
	TraceFlags    []uint32 `form:"trace_flags" binding:"dive,numeric,oneof=0 1"`
	NotTraceFlags []uint32 `form:"not[trace_flags]" binding:"dive,numeric,oneof=0 1"`
	Body          []string `form:"body" binding:"dive,min=1"`

	// NOTE(prozlach): otel spec defines text labels for log levels but
	// frameworks do not really have to adhere. For example, otel demo alone
	// sends:
	// ```
	//    ┌─SeverityText─┬─count(SeverityText)─┐
	// 1. │ WARN         │                 473 │
	// 2. │ Information  │              885139 │
	// 3. │ Warning      │                   1 │
	// 4. │ INFO         │             1615341 │
	// 5. │ ERROR        │              110666 │
	//    └──────────────┴─────────────────────┘
	// ```
	// otel defines `warn` and `info`, not `Warning` and `Information`. How
	// would we parse it to e.g. assign coloring in UI? The idea is to rely on
	// the numerical level which is hard to get wrong and unambiguously
	// identifies the log level and treat `SeverityText` as free-form field.
	//
	// Query is also much more efficient and effective when run against
	// numerical values hence we do not foresee querying by `SeverityText`.
	// SeverityNames and NotSeverityNames become convince aliases for
	// SeverityNumbers and NotSeverityNumbers fields respectively.
	//nolint:lll
	SeverityNames []string `form:"severity_name" binding:"dive,oneof=trace trace2 trace3 trace4 debug debug2 debug3 debug4 info info2 info3 info4 warn warn2 warn3 warn4 error error2 error3 error4 fatal fatal2 fatal3 fatal4"`
	//nolint:lll
	NotSeverityNames []string `form:"not[severity_name]" binding:"dive,oneof=trace trace2 trace3 trace4 debug debug2 debug3 debug4 info info2 info3 info4 warn warn2 warn3 warn4 error error2 error3 error4 fatal fatal2 fatal3 fatal4"`

	LogAttributeNames  []string `form:"log_attr_name" binding:"dive,min=1"`
	LogAttributeValues []string `form:"log_attr_value" binding:"dive,min=1"`

	ResourceAttributeNames  []string `form:"res_attr_name" binding:"dive,min=1"`
	ResourceAttributeValues []string `form:"res_attr_value" binding:"dive,min=1"`
}

type logsMetadataCriterias struct {
	timeQueryParams
	logsQueryParams
}

type logsQueryCriterias struct {
	timeQueryParams
	logsQueryParams

	Sort string `form:"sort,default=timestamp_desc" binding:"required,oneof=timestamp_desc timestamp_asc"`

	PageSize  int     `form:"page_size,default=100" binding:"numeric,gte=0,max=1000"`
	PageToken *string `form:"page_token" binding:"omitempty,base64"`
}

func (e *QueryAPI) MetricsAutoCompleteHandler(ctx *gin.Context) {
	// get project ID
	pid, err := qcommon.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	// bind other filters
	filters := &metrics.MetricNameFilters{}
	if err := ctx.BindQuery(filters); err != nil {
		e.logger.Error("autocomplete form bind error", zap.Error(err))
		return
	}
	e.logger.Debug("filters", zap.Any("filters", filters))
	results, err := e.metricsQuerier.GetMetricNames(ctx, pid, filters)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, results)
}

func (e *QueryAPI) MetricsSearchHandler(ctx *gin.Context) {
	pid, err := qcommon.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	// bind query params
	qp := new(metricsQueryParams)
	if err := ctx.BindQuery(qp); err != nil {
		e.logger.Error("search form bind error", zap.Error(err))
		return
	}

	query := &metrics.Query{
		TargetMetric: qp.MetricName,
		TargetType:   qp.MetricType,
		TargetVisual: strings.ToLower(qp.MetricVisual),
		Filters:      make([]metrics.Filter, 0),
		GroupBy: metrics.GroupBy{
			Function:   "",
			Attributes: make([]string, 0),
		},
		Correlate: metrics.Correlate{
			TraceID: qp.MetricCorrelationTraceID,
		},
	}

	if len(qp.MetricFilters) > 0 {
		parsedFilters, err := qp.MetricFilters.Parsed()
		if err != nil {
			ctx.AbortWithError(400, err) // bad request
			return
		}
		query.Filters = append(query.Filters, parsedFilters...)
	}

	// if passed as arguments, ignore any grouping/aggregation for Histogram-types since
	// they cannot be aggregated like sums or gauges.
	switch query.TargetType {
	case pmetric.MetricTypeSum.String(), pmetric.MetricTypeGauge.String():
		if qp.MetricGroupbyFunc != "" || qp.MetricGroupbyAttrs != "" {
			parsedGroupbyFunc := metrics.GetAggregationFunctionFromString(qp.MetricGroupbyFunc)
			if parsedGroupbyFunc == metrics.AggregateFunctionUnknown {
				ctx.AbortWithError(400, fmt.Errorf("unknown groupby function")) // bad request
				return
			}

			parsedGroupbyAttrs, err := qp.MetricGroupbyAttrs.Parsed()
			if err != nil {
				ctx.AbortWithError(400, err) // bad request
				return
			}
			// parse groupby parameters into necessary filters as upstream as possible
			if len(parsedGroupbyAttrs) > 0 {
				for _, attr := range parsedGroupbyAttrs {
					query.Filters = append(query.Filters, metrics.Filter{
						Key:      "",
						Operator: string(metrics.FilterOpHasAttribute),
						Value:    attr,
					})
				}
			}
			query.GroupBy = metrics.GroupBy{
				Function:   parsedGroupbyFunc,
				Attributes: parsedGroupbyAttrs,
			}
		}
	default:
		// do nothing
	}

	queries := make(map[string]*metrics.Query)
	queries["alias"] = query

	refTime := time.Now().UTC() // always query from now
	startTime, endTime, err := parseQueryTimes(refTime, qp.timeQueryParams)
	if err != nil {
		ctx.AbortWithError(400, err) // bad request
		return
	}

	qc := &metrics.QueryContext{
		ProjectID:      pid,
		StartTimestamp: startTime.UTC(),
		EndTimestamp:   endTime.UTC(),
		Queries:        queries,
	}
	e.logger.Debug("query context", zap.Any("parsed object", qc))
	results, err := e.metricsQuerier.GetMetrics(ctx, qc)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, results)
}

func (e *QueryAPI) MetricsSearchMetadataHandler(ctx *gin.Context) {
	pid, err := qcommon.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	// bind query params
	qp := new(metricsQueryParams)
	if err := ctx.BindQuery(qp); err != nil {
		e.logger.Error("searchmetadata query bind error", zap.Error(err))
		return
	}
	queries := make(map[string]*metrics.Query)
	queries["alias"] = &metrics.Query{
		TargetMetric: qp.MetricName,
		TargetType:   qp.MetricType,
	}
	qc := &metrics.QueryContext{
		ProjectID: pid,
		Queries:   queries,
	}
	e.logger.Debug("search metadata query", zap.Any("parsed object", qc))
	results, err := e.metricsQuerier.GetSearchMetadata(ctx, qc)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, results)
}

func severityTextsToNumbers(in []string) []int32 {
	res := make([]int32, len(in))
	for i, v := range in {
		res[i] = int32(common.SeverityTextToNumber(v))
	}

	return res
}

func (e *QueryAPI) SearchLogsHandler(ctx *gin.Context) {
	pid, err := qcommon.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	// bind query params
	queryParameters := new(logsQueryCriterias)
	if err := ctx.BindQuery(queryParameters); err != nil {
		e.logger.Error("search form bind error", zap.Error(err))
		return
	}

	refTime := time.Now().UTC() // always query from now
	startTime, endTime, err := parseQueryTimes(refTime, queryParameters.timeQueryParams)
	if err != nil {
		ctx.AbortWithError(400, err) // bad request
		return
	}

	qc := &logs.QueryContext{
		ProjectID:      pid,
		StartTimestamp: startTime.UTC(),
		EndTimestamp:   endTime.UTC(),
		ReferenceTime:  refTime,

		PageSize:      queryParameters.PageSize,
		SortByTimeAsc: queryParameters.Sort == "timestamp_asc",

		// NOTE(prozlach): CH does deduplication of query args during query execution.
		Queries: map[string]*logs.Query{
			"main": {
				ServiceNames:    queryParameters.ServiceNames,
				NotServiceNames: queryParameters.NotServiceNames,
				SeverityNumbers: append(
					queryParameters.SeverityNumbers,
					severityTextsToNumbers(queryParameters.SeverityNames)...,
				),
				NotSeverityNumbers: append(
					queryParameters.NotSeverityNumbers,
					severityTextsToNumbers(queryParameters.NotSeverityNames)...,
				),
				TraceIDs:      queryParameters.TraceIDs,
				SpanIDs:       queryParameters.SpanIDs,
				Fingerprints:  queryParameters.Fingerprints,
				TraceFlags:    queryParameters.TraceFlags,
				NotTraceFlags: queryParameters.NotTraceFlags,
				Body:          queryParameters.Body,

				LogAttributeNames:  queryParameters.LogAttributeNames,
				LogAttributeValues: queryParameters.LogAttributeValues,

				ResourceAttributeNames:  queryParameters.ResourceAttributeNames,
				ResourceAttributeValues: queryParameters.ResourceAttributeValues,
			},
		},
	}
	if queryParameters.PageToken != nil {
		qc.PageToken = *queryParameters.PageToken
	}

	e.logger.Debug("query context", zap.Any("parsed object", qc))
	results, err := e.logsQuerier.GetLogs(ctx, qc)
	if err != nil {
		var exceptionErr *clickhouse.Exception

		// source: https://github.com/ClickHouse/ClickHouse/blob/master/src/Common/ErrorCodes.cpp
		const clickHouseTimeoutExceededErrorCode = int32(159)

		if errors.As(err, &exceptionErr) && exceptionErr.Code == clickHouseTimeoutExceededErrorCode {
			// Query timeout
			ctx.AbortWithError(http.StatusGatewayTimeout, err)
		} else {
			ctx.AbortWithError(http.StatusInternalServerError, err)
		}
		return
	}
	ctx.JSON(200, results)
}

func (e *QueryAPI) SearchLogsMetadataHandler(ctx *gin.Context) {
	// bind project ID
	pid, err := qcommon.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	// bind query params
	queryParameters := new(logsMetadataCriterias)
	if err := ctx.BindQuery(queryParameters); err != nil {
		e.logger.Error("search form bind error", zap.Error(err))
		return
	}

	refTime := time.Now().UTC() // always query from now
	startTime, endTime, err := parseQueryTimes(refTime, queryParameters.timeQueryParams)
	if err != nil {
		ctx.AbortWithError(400, err) // bad request
		return
	}

	qc := &logs.QueryContext{
		ProjectID: pid,

		StartTimestamp: startTime.UTC(),
		EndTimestamp:   endTime.UTC(),
		ReferenceTime:  refTime,

		// NOTE(prozlach): CH does deduplication of query args during query execution.
		Queries: map[string]*logs.Query{
			"main": {
				ServiceNames:    queryParameters.ServiceNames,
				NotServiceNames: queryParameters.NotServiceNames,
				SeverityNumbers: append(
					queryParameters.SeverityNumbers,
					severityTextsToNumbers(queryParameters.SeverityNames)...,
				),
				NotSeverityNumbers: append(
					queryParameters.NotSeverityNumbers,
					severityTextsToNumbers(queryParameters.NotSeverityNames)...,
				),
				TraceIDs:      queryParameters.TraceIDs,
				SpanIDs:       queryParameters.SpanIDs,
				Fingerprints:  queryParameters.Fingerprints,
				TraceFlags:    queryParameters.TraceFlags,
				NotTraceFlags: queryParameters.NotTraceFlags,
				Body:          queryParameters.Body,

				LogAttributeNames:  queryParameters.LogAttributeNames,
				LogAttributeValues: queryParameters.LogAttributeValues,

				ResourceAttributeNames:  queryParameters.ResourceAttributeNames,
				ResourceAttributeValues: queryParameters.ResourceAttributeValues,
			},
		},
	}

	e.logger.Debug("query context", zap.Any("parsed object", qc))
	results, err := e.logsQuerier.GetLogsMetadata(ctx, qc)
	if err != nil {
		var exceptionErr *clickhouse.Exception

		// source: https://github.com/ClickHouse/ClickHouse/blob/master/src/Common/ErrorCodes.cpp
		const clickHouseTimeoutExceededErrorCode = int32(159)

		if errors.As(err, &exceptionErr) && exceptionErr.Code == clickHouseTimeoutExceededErrorCode {
			// Query timeout
			ctx.AbortWithError(http.StatusGatewayTimeout, err)
		} else {
			ctx.AbortWithError(http.StatusInternalServerError, err)
		}
		return
	}
	ctx.JSON(200, results)
}

type analyticsUsageQueryParams struct {
	timeQueryParams

	TenantID string `header:"x-target-tenantid"`
	Feature  string `form:"feature"`
	Month    int    `form:"month"`
	Year     int    `form:"year"`
}

func (e *QueryAPI) StorageAnalyticsHandler(ctx *gin.Context) {
	// get project ID
	pid, err := qcommon.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	// bind query params
	qp := new(analyticsUsageQueryParams)
	if err := ctx.BindQuery(qp); err != nil {
		e.logger.Error("analytics query bind error", zap.Error(err))
		return
	}

	if err := ctx.BindHeader(qp); err != nil {
		e.logger.Error("analytics headers bind error", zap.Error(err))
		return
	}

	refTime := time.Now().UTC() // always query from now
	startTime, endTime, err := parseQueryTimes(refTime, qp.timeQueryParams)
	if err != nil {
		ctx.AbortWithError(400, err) // bad request
		return
	}

	// default to current month & year unless specified
	if qp.Month == 0 {
		qp.Month = int(time.Now().Month())
	}
	if qp.Year == 0 {
		qp.Year = time.Now().Year()
	}

	results, err := e.analyticsQuerier.GetStorageAnalytics(
		ctx,
		&analytics.QueryFilters{
			TenantID:   qp.TenantID,
			ProjectIDs: []string{pid},
			Feature:    qp.Feature,
			Month:      uint(qp.Month),
			Year:       uint(qp.Year),
		},
		startTime.UTC(),
		endTime.UTC(),
	)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, results)
}

var validPeriods = map[string]struct{}{
	common.Period1m:  {},
	common.Period5m:  {},
	common.Period15m: {},
	common.Period30m: {},
	common.Period1h:  {},
	common.Period4h:  {},
	common.Period12h: {},
	common.Period24h: {},
	common.Period7d:  {},
	common.Period14d: {},
	common.Period30d: {},
}

const defaultTimePeriodHours int = 1 // 1h

var (
	errBothPeriodAndTimeProvided     = errors.New("both period and time range are provided")
	errBothTimeBoundariesNotProvided = errors.New("both ends for time range are not provided")
)

func parseQueryTimes(refTime time.Time, p timeQueryParams) (time.Time, time.Time, error) {
	var (
		startTime, endTime time.Time
	)
	// either a period or a time range pair should be provided but not both.
	if p.Period != "" && (!p.StartTime.IsZero() || !p.EndTime.IsZero()) {
		return startTime, endTime, errBothPeriodAndTimeProvided
	}
	if (!p.StartTime.IsZero() && p.EndTime.IsZero()) || (!p.EndTime.IsZero() && p.StartTime.IsZero()) {
		return startTime, endTime, errBothTimeBoundariesNotProvided
	}
	// when a predefined interval is provided, use that and compute
	// start time and end time accordingly
	if p.Period != "" {
		_, ok := validPeriods[p.Period]
		if !ok {
			return startTime, endTime, fmt.Errorf("invalid period: %s", p.Period)
		}
		startTime, _, _, _ := common.InferTimelines(refTime, p.Period)
		return time.Unix(0, startTime), refTime, nil
	}
	// otherwise return the custom time range provided via params OR use default
	// timestamps when not provided via the query
	if p.StartTime.IsZero() {
		startTime = refTime.Add(-1 * time.Duration(defaultTimePeriodHours) * time.Hour)
	} else {
		startTime = p.StartTime
	}
	if p.EndTime.IsZero() {
		endTime = refTime
	} else {
		endTime = p.EndTime
	}
	return startTime, endTime, nil
}

type alertQueryParams struct {
	Period string `form:"period"`

	// Time format is RFC3339Nano
	StartTime time.Time `form:"start_time" time_format:"2006-01-02T15:04:05.999999999Z07:00"`

	// TenantID is optional parameter
	TenantID string `header:"x-target-tenantid"`
}

func (e *QueryAPI) AlertsHandler(ctx *gin.Context) {
	qp := new(alertQueryParams)
	if err := ctx.BindQuery(qp); err != nil {
		e.logger.Error("alerts query bind error", zap.Error(err))
		return
	}

	if err := ctx.BindHeader(qp); err != nil {
		e.logger.Error("alerts headers bind error", zap.Error(err))
		return
	}

	refTime := time.Now().UTC() // always query from now
	startTime, err := parseStartTimeOrPeriod(refTime, qp)
	if err != nil {
		ctx.AbortWithError(400, err) // bad request
		return
	}

	results, err := e.alertsQuerier.GetAlerts(
		ctx,
		&alerts.QueryFilters{
			TenantID:   qp.TenantID,
			PeriodFrom: startTime,
			IsSaaS:     ctx.GetHeader(constants.RealmHeader) == constants.GitlabRealmSaas,
		},
	)
	if err != nil {
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, results)
}

func parseStartTimeOrPeriod(refTime time.Time, p *alertQueryParams) (time.Time, error) {
	var (
		startTime time.Time
	)
	// either a period or a time range pair should be provided but not both.
	if p.Period != "" && (!p.StartTime.IsZero()) {
		return startTime, errBothPeriodAndTimeProvided
	}

	// when a predefined interval is provided, use that and compute
	// start time and end time accordingly
	if p.Period != "" {
		_, ok := validPeriods[p.Period]
		if !ok {
			return startTime, fmt.Errorf("invalid period: %s", p.Period)
		}
		startTime, _, _, _ := common.InferTimelines(refTime, p.Period)
		return time.Unix(0, startTime), nil
	}
	// otherwise return the custom time range provided via params OR use default
	// timestamps when not provided via the query.
	// Current default look period for alerts is last 24 hours.
	if p.StartTime.IsZero() {
		startTime = refTime.Add(-1 * 24 * time.Hour)
	} else {
		startTime = p.StartTime
	}

	return startTime, nil
}
