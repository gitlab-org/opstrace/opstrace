package core

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
	"go.uber.org/zap"
)

func (e *QueryAPI) TracesHandler(ctx *gin.Context) {
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}

	logger := e.logger.With(
		zap.String("project_id", projectID))

	params := &traces.TracesParams{}
	if err := ctx.BindQuery(params); err != nil {
		logger.Error("bind trace params", zap.Error(err))
		return
	}

	logger.Debug("trace request params", zap.Any("params", params))

	if err := params.Validate(); err != nil {
		ctx.AbortWithError(400, err)
		return
	}

	if params.PageToken != "" {
		cursor, err := traces.DecodePage(params.PageToken)
		if err != nil {
			logger.Error("decoding page:", zap.Error(err))
			ctx.AbortWithError(http.StatusBadRequest, err)
			return
		}
		params.Cursor = cursor
	}

	results, nextPageToken, err := e.tracesQuerier.GetTraces(ctx, projectID, params)
	if err != nil {
		logger.Error("get traces", zap.Error(err))
		ctx.AbortWithError(500, err)
		return
	}

	resp := &traces.TracesResult{
		ProjectID:     projectID,
		TotalTraces:   int64(len(results)),
		Traces:        results,
		NextPageToken: nextPageToken,
	}
	ctx.JSON(200, resp)
}

func (e *QueryAPI) TraceHandler(ctx *gin.Context) {
	handlerParams := &traces.TraceParams{}
	if err := ctx.BindUri(handlerParams); err != nil {
		e.logger.Error("bind error", zap.Error(err))
		// context already canceled by BindUri

		return
	}
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	traceID := handlerParams.TraceID

	trace, err := e.tracesQuerier.GetTrace(ctx, projectID, traceID)
	if err != nil {
		e.logger.With(
			zap.String("trace_id", traceID),
			zap.String("project_id", projectID)).
			Error("get trace", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	if trace != nil {
		ctx.JSON(200, trace)
	} else {
		ctx.AbortWithStatus(404)
	}
}

func (e *QueryAPI) ServicesHandler(ctx *gin.Context) {
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}

	services, err := e.tracesQuerier.GetServices(ctx, projectID)
	if err != nil {
		e.logger.Error("get services", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(200, traces.ServicesResult{
		Services: services,
	})
}

func (e *QueryAPI) ServiceOperationsHandler(ctx *gin.Context) {
	handlerParams := &traces.ServiceOperationsParams{}
	if err := ctx.BindUri(handlerParams); err != nil {
		e.logger.Error("bind error", zap.Error(err))
		// context already canceled by BindUri
		return
	}
	projectID, err := common.GetProjectIDFromRequest(ctx)
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}
	service := handlerParams.ServiceName

	operations, err := e.tracesQuerier.GetServiceOperations(ctx, projectID, service)
	if err != nil {
		e.logger.With(
			zap.String("service_name", service),
			zap.String("project_id", projectID)).
			Error("get service operations", zap.Error(err))
		ctx.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	ctx.JSON(200, operations)
}

func (e *QueryAPI) TraceAnalyticsHandler(ctx *gin.Context) {
	params := &traces.TracesParams{}
	if err := ctx.BindQuery(params); err != nil {
		e.logger.Error("bind traces params", zap.Error(err))
		return
	}

	projectID, err := common.GetProjectIDFromRequest(ctx)
	logger := e.logger.With(
		zap.String("project_id", projectID))
	if err != nil {
		ctx.AbortWithError(400, err)
		return
	}

	logger.Debug("trace request params", zap.Any("params", params))

	if err := params.Validate(); err != nil {
		ctx.AbortWithError(400, err)
		return
	}

	result, err := e.tracesQuerier.GetTraceAnalytics(ctx, projectID, params)
	if err != nil {
		logger.Error("get rate for traces", zap.Error(err))
		ctx.AbortWithError(500, err)
		return
	}
	ctx.JSON(200, result)
}
