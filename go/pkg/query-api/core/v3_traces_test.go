package core

import (
	"encoding/base64"
	"net/http"
	"net/http/httptest"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/go-cmp/cmp/cmpopts"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/traces"
)

var _ = Context("V3 traces handler", func() {
	var (
		router *gin.Engine
	)

	BeforeEach(func() {
		router = gin.New()
	})

	encodePageToken := func(json string) string {
		return base64.URLEncoding.EncodeToString([]byte(json))
	}

	Context("List Traces", func() {
		DescribeTable("query API",
			func(url string, statusCode int, expectedFilter *traces.TracesParams) {
				q := &traces.MockQuerier{}

				qm := &metrics.MockQuerier{}
				ql := &logs.MockQuerier{}
				api := NewQueryAPI(logger.Desugar(), qm, ql, nil, nil, q, nil)
				api.SetRoutes(router)

				recorder := httptest.NewRecorder()
				testReq, err := http.NewRequest(http.MethodGet, url, nil)
				Expect(err).NotTo(HaveOccurred())
				router.ServeHTTP(recorder, testReq)
				Expect(recorder.Code).To(Equal(statusCode))

				Expect(q.PassedFilter).To(BeComparableTo(expectedFilter, cmpopts.IgnoreUnexported(traces.TracesParams{})))
				if q.PassedFilter != nil {
					Expect(q.PassedFilter.Cursor).To(Equal(expectedFilter.Cursor))
				}
			},
			Entry(
				"returns 200 for a list of traces",
				"/v3/query/123/traces",
				http.StatusOK,
				&traces.TracesParams{
					PageSize: 100,
				},
			),
			Entry(
				"gets 400 on invalid traceID",
				"/v3/query/123/traces?trace_id=1222",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"gets 400 on invalid period",
				"/v3/query/123/traces?period=10m",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with valid params",
				"/v3/query/123/traces?trace_id=b7e641ba-918b-3147-2cba-e93b13e7de2a&period=1m",
				http.StatusOK,
				&traces.TracesParams{
					TraceIDs: []string{"b7e641ba-918b-3147-2cba-e93b13e7de2a"},
					Period:   "1m",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with valid operation and service name",
				"/v3/query/123/traces?service_name=blah&operation=fake",
				http.StatusOK,
				&traces.TracesParams{
					ServiceNames: []string{"blah"},
					Operations:   []string{"fake"},
					PageSize:     100,
				},
			),
			Entry(
				"returns 200 with repeated params name",
				"/v3/query/123/traces?service_name=blah&operation=fake&operation=boo&service_name=fiver",
				http.StatusOK,
				&traces.TracesParams{
					ServiceNames: []string{"blah", "fiver"},
					Operations:   []string{"fake", "boo"},
					PageSize:     100,
				},
			),
			Entry(
				"returns 200 with not operator in params name",
				"/v3/query/123/traces?not[service_name]=blah&not[operation]=fake&not[operation]=boo&not[service_name]=fiver",
				http.StatusOK,
				&traces.TracesParams{
					NotServiceNames: []string{"blah", "fiver"},
					NotOperations:   []string{"fake", "boo"},
					PageSize:        100,
				},
			),
			Entry(
				"returns 400 with incorrect duration params",
				"/v3/query/123/traces?lt[duration_nano]=400s&gt[duration_nano]=200s&period=1h",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 400 with negative duration param",
				"/v3/query/123/traces?lt[duration_nano]=-400&gt[duration_nano]=200&period=1h",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with proper duration params",
				"/v3/query/123/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h",
				http.StatusOK,
				&traces.TracesParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					PageSize:   100,
				},
			),
			Entry(
				"returns 200 with custom period of 4h",
				"/v3/query/123/traces?period=4h",
				http.StatusOK,
				&traces.TracesParams{
					Period:   "4h",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with custom period of 5m",
				"/v3/query/123/traces?period=5m",
				http.StatusOK,
				&traces.TracesParams{
					Period:   "5m",
					PageSize: 100,
				},
			),
			Entry(
				"returns 200 with page size parameter",
				"/v3/query/123/traces?lt[duration_nano]=400&gt[duration_nano]=200&period=1h&page_size=20",
				http.StatusOK,
				&traces.TracesParams{
					Period:     "1h",
					LtDuration: 400,
					GtDuration: 200,
					PageSize:   20,
				},
			),
			Entry(
				"returns 200 with page size and page token parameter",
				"/v3/query/123/traces?page_size=20&page_token="+encodePageToken(`{"last_seen_timestamp": 1714734027180362118}`),
				http.StatusOK,
				&traces.TracesParams{
					PageSize:  20,
					PageToken: encodePageToken(`{"last_seen_timestamp": 1714734027180362118}`),
					Cursor: &traces.Page{
						LastSeenTimestamp: 1714734027180362118,
					},
				},
			),
			Entry(
				"returns 200 with page size and page token parameter and sort",
				"/v3/query/123/traces?page_size=20&sort=duration_desc&page_token="+encodePageToken(`{"last_seen_duration": 123456789, "last_seen_trace_id": "b7e641ba-918b-3147-2cba-e93b13e7de2a"}`),
				http.StatusOK,
				&traces.TracesParams{
					PageSize:  20,
					PageToken: encodePageToken(`{"last_seen_duration": 123456789, "last_seen_trace_id": "b7e641ba-918b-3147-2cba-e93b13e7de2a"}`),
					Sort:      traces.SortDurationDesc,
					Cursor: &traces.Page{
						LastSeenDuration: 123456789,
						LastSeenTraceID:  "b7e641ba-918b-3147-2cba-e93b13e7de2a",
					},
				},
			),
			Entry(
				"returns 400 with wrong page token parameter",
				"/v3/query/123/traces?page_size=20&page_token=full",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 400 with unmatched attribute names and values",
				"/v3/query/123/traces?attr_name=foo&attr_value=bar&attr_name=baz",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with matched attribute names and values",
				"/v3/query/123/traces?attr_name=foo&attr_value=bar&attr_name=baz&attr_value=qux",
				http.StatusOK,
				&traces.TracesParams{
					AttributeNames:  []string{"foo", "baz"},
					AttributeValues: []string{"bar", "qux"},
					PageSize:        100,
				},
			),
			Entry(
				"returns 400 with mixed period and range params",
				"/v3/query/123/traces?period=1h&start_time=2023-11-02&end_time=2023-11-20",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 400 when missing one of range params",
				"/v3/query/123/traces?period=1h&start_time=2023-11-02",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 400 when missing one of range params",
				"/v3/query/123/traces?period=1h&end_time=2023-11-20",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"returns 200 with provided range params in RFC3339 format",
				"/v3/query/123/traces?start_time=2023-11-23T11:24:10Z&end_time=2023-11-23T13:24:10Z",
				http.StatusOK,
				&traces.TracesParams{
					StartTime: time.Date(2023, 11, 23, 11, 24, 10, 0, time.UTC),
					EndTime:   time.Date(2023, 11, 23, 13, 24, 10, 0, time.UTC),
					PageSize:  100,
				},
			),
			Entry(
				"returns 400 with provided range params in non RFC3339 format",
				"/v3/query/123/traces?start_time=2023-11-02&end_time=2023-11-20",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"gets 400 on invalid status",
				"/v3/query/123/traces?status=invalid",
				http.StatusBadRequest,
				nil,
			),
			Entry(
				"gets 200 on filtering with status",
				"/v3/query/123/traces?status=ok&status=error",
				http.StatusOK,
				&traces.TracesParams{
					PageSize: 100,
					Statuses: []string{"STATUS_CODE_OK", "STATUS_CODE_ERROR"},
				},
			),
		)
	})
})
