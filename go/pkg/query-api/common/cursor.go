package common

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
)

const LogBodyMaxPrefixSize = 128

// LogsPage holds the details of the page token needed for the next page.
//
// Requests for the next page should include this cursor in the page token
// parameter. Even though we use nanosecond precision internally, we still need
// to account for other fields when accurately determining last entry on the
// given page, as some tooling may simply round the timestamp to the closests
// second (or simply have 1-second precision in the first place), resulting in
// multiple log entries having the same timestamp.
//
// These fields do not 100% guarantee that we will not display a log entry
// twice (i.e. on previous page and then on the next), but should greatly
// reduce the risk.
//
// Fields below belong to the index CH is using.
type LogsPage struct {
	LastSeenTimestamp   string `json:"last_seen_timestamp,omitempty"`
	LastSeenTraceID     string `json:"trace_id" ch:"TraceId"`
	LastSeenSpanID      string `json:"span_id" ch:"SpanId"`
	LastSeenServiceName string `json:"service_name"`
	// We can't really put the whole body into the URL, but OTOH some logs may
	// different by only the logs body. A compro
	LastSeenBodyPrefix string `json:"body"`
}

type pageType interface {
	LogsPage
	// NOTE(prozlach): This can be extended to:
	// LogsPage | TracesPage | MetricsPage
}

func DecodePage[T pageType](rawCursor string) (*T, error) {
	result := &T{}

	raw, err := base64.StdEncoding.DecodeString(rawCursor)
	if err != nil {
		return nil, fmt.Errorf("cursor decode: %w", err)
	}

	err = json.Unmarshal(raw, result)
	if err != nil {
		return nil, fmt.Errorf("unmarshal cursor: %w", err)
	}

	return result, nil
}

func EncodePage[T pageType](p *T) (string, error) {
	bts, err := json.Marshal(p)
	if err != nil {
		return "", fmt.Errorf("encode page :%w", err)
	}

	return base64.StdEncoding.EncodeToString(bts), nil
}
