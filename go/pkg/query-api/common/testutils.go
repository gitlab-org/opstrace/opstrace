package common

import (
	"context"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/pmetric"
	conventions "go.opentelemetry.io/collector/semconv/v1.25.0"
	"go.uber.org/zap"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/otel/exporter/gitlabobservabilityexporter/metrics"
)

const (
	RandomProjectID   string = "12345"
	RandomNamespaceID string = "123"
	RandomServiceName string = "test-service"
	RandomMetricName  string = "random-metric-name"
)

var (
	RandomTraceID = [16]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4}
	RandomSpanID  = [8]byte{0, 0, 0, 0, 1, 2, 3, 4}
)

func GenerateOTELMetrics(
	mtype pmetric.MetricType,
	startTimestamp time.Time,
	timestamp time.Time,
	attributes map[string]string,
	value float64,
	traceID []byte,
) pmetric.Metrics {
	//nolint:exhaustive
	switch mtype {
	case pmetric.MetricTypeSum:
		return GenerateOTELSumMetrics(
			startTimestamp,
			timestamp,
			attributes,
			value,
			traceID,
		)
	case pmetric.MetricTypeGauge:
		return GenerateOTELGaugeMetrics(
			startTimestamp,
			timestamp,
			attributes,
			value,
			traceID,
		)
	case pmetric.MetricTypeHistogram:
		return GenerateOTELHistogramMetrics(
			startTimestamp,
			timestamp,
			attributes,
			[]float64{0, 0, 0, 0, 0},
			[]uint64{0, 0, 0, 1, 0},
			traceID,
		)
	case pmetric.MetricTypeExponentialHistogram:
		return GenerateOTELExponentialHistogramMetrics(
			startTimestamp,
			timestamp,
			attributes,
			traceID,
		)
	}
	return pmetric.Metrics{}
}

func GenerateOTELSumMetrics(
	startTimestamp time.Time,
	timestamp time.Time,
	attributes map[string]string,
	value float64,
	traceID []byte,
) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, RandomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(RandomMetricName)
	m.SetUnit("count")
	m.SetDescription("Random description for a random sum metric")

	dp := m.SetEmptySum().DataPoints().AppendEmpty()
	dp.SetDoubleValue(value)
	dp.Attributes().PutStr(common.ProjectIDHeader, RandomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(startTimestamp))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(timestamp))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(pcommon.TraceID(traceID))
	exemplars.SetSpanID(RandomSpanID)

	m.Sum().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)
	return metrics
}

func GenerateOTELGaugeMetrics(
	startTimestamp time.Time,
	timestamp time.Time,
	attributes map[string]string,
	value float64,
	traceID []byte,
) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, RandomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(RandomMetricName)
	m.SetUnit("count")
	m.SetDescription("Random description for a random gauge metric")

	dp := m.SetEmptyGauge().DataPoints().AppendEmpty()
	dp.SetDoubleValue(value)
	dp.Attributes().PutStr(common.ProjectIDHeader, RandomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(startTimestamp))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(timestamp))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(pcommon.TraceID(traceID))
	exemplars.SetSpanID(RandomSpanID)

	return metrics
}

func GenerateOTELHistogramMetrics(
	startTimestamp time.Time,
	timestamp time.Time,
	attributes map[string]string,
	explicitBounds []float64,
	bucketCounts []uint64,
	traceID []byte,
) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, RandomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(RandomMetricName)
	m.SetDescription("Random description for a random histogram metric")

	dp := m.SetEmptyHistogram().DataPoints().AppendEmpty()
	dp.SetCount(1)
	dp.SetSum(1)
	dp.ExplicitBounds().FromRaw(explicitBounds)
	dp.BucketCounts().FromRaw(bucketCounts)
	dp.SetMin(0)
	dp.SetMax(1)
	dp.Attributes().PutStr(common.ProjectIDHeader, RandomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(startTimestamp))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(timestamp))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(pcommon.TraceID(traceID))
	exemplars.SetSpanID(RandomSpanID)

	m.Histogram().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)

	return metrics
}

func GenerateOTELExponentialHistogramMetrics(
	startTimestamp time.Time,
	timestamp time.Time,
	attributes map[string]string,
	traceID []byte,
) pmetric.Metrics {
	metrics := pmetric.NewMetrics()
	rm := metrics.ResourceMetrics().AppendEmpty()
	rm.Resource().Attributes().PutStr(conventions.AttributeServiceName, RandomServiceName)
	rm.Resource().SetDroppedAttributesCount(10)
	rm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")

	sm := rm.ScopeMetrics().AppendEmpty()
	sm.Scope().SetName("io.opentelemetry.contrib.clickhouse")
	sm.Scope().SetVersion("1.0.0")
	sm.SetSchemaUrl("https://opentelemetry.io/schemas/1.4.0")
	sm.Scope().SetDroppedAttributesCount(10)
	sm.Scope().Attributes().PutStr("lib", "clickhouse")

	m := sm.Metrics().AppendEmpty()
	m.SetName(RandomMetricName)
	m.SetDescription("Random description for a random exponential histogram metric")

	dp := m.SetEmptyExponentialHistogram().DataPoints().AppendEmpty()
	dp.SetCount(1)
	dp.SetSum(1)
	dp.SetMin(0)
	dp.SetMax(1)
	dp.SetZeroCount(0)
	dp.Negative().SetOffset(1)
	dp.Negative().BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
	dp.Positive().SetOffset(1)
	dp.Positive().BucketCounts().FromRaw([]uint64{0, 0, 0, 1, 0})
	dp.Attributes().PutStr(common.ProjectIDHeader, RandomProjectID)
	for k, v := range attributes {
		dp.Attributes().PutStr(k, v)
	}
	dp.SetStartTimestamp(pcommon.NewTimestampFromTime(startTimestamp))
	dp.SetTimestamp(pcommon.NewTimestampFromTime(timestamp))

	exemplars := dp.Exemplars().AppendEmpty()
	exemplars.SetIntValue(10)
	exemplars.FilteredAttributes().PutStr("key", "value")
	exemplars.SetTraceID(pcommon.TraceID(traceID))
	exemplars.SetSpanID(RandomSpanID)

	m.ExponentialHistogram().SetAggregationTemporality(pmetric.AggregationTemporalityCumulative)

	return metrics
}

func PersistOTELMetrics(
	ctx context.Context,
	db clickhouse.Conn,
	logger *zap.Logger,
	md pmetric.Metrics,
	ingestionTimestamp time.Time,
) error {
	ingester := metrics.NewClickHouseIngester(db, logger)
	ingester.SetWriteTimeout(time.Second * 15)
	for i := 0; i < md.ResourceMetrics().Len(); i++ {
		metrics := md.ResourceMetrics().At(i)
		resAttr := common.AttributesToMap(metrics.Resource().Attributes())
		for j := 0; j < metrics.ScopeMetrics().Len(); j++ {
			rs := metrics.ScopeMetrics().At(j).Metrics()
			scopeInstr := metrics.ScopeMetrics().At(j).Scope()
			scopeURL := metrics.ScopeMetrics().At(j).SchemaUrl()
			for k := 0; k < rs.Len(); k++ {
				ingester.Add(resAttr, metrics.SchemaUrl(), scopeInstr, scopeURL, rs.At(k))
			}
		}
	}
	return ingester.Write(ctx, ingestionTimestamp)
}
