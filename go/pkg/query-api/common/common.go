package common

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

var ErrUnknownProjectID = errors.New("unknown project ID")

type projectIDRequestParams struct {
	ProjectIDUri    int64  `uri:"project_id"`
	ProjectIDHeader string `header:"x-target-projectid"`
}

// GetProjectIDFromRequest retrieves the project ID from the request context.
//
// It prioritizes the header value over the path parameter to satisfy both v3 and v4
// versions of the query API.
// Returns the project ID as a string.
func GetProjectIDFromRequest(ctx *gin.Context) (string, error) {
	p := new(projectIDRequestParams)
	if err := ctx.ShouldBindUri(p); err != nil {
		return "", fmt.Errorf("uri bind error: %w", err)
	}
	if err := ctx.ShouldBindHeader(p); err != nil {
		return "", fmt.Errorf("header bind error: %w", err)
	}
	// Prioritize the header value over the URI value because
	// the auth-proxy will edit the project ID for requests
	// originating from self-managed instances and set the header
	if p.ProjectIDHeader != "" {
		return p.ProjectIDHeader, nil
	}
	if p.ProjectIDUri != 0 {
		return strconv.FormatInt(p.ProjectIDUri, 10), nil
	}
	return "", fmt.Errorf("project id not found in uri or header")
}
