package errortracking

type ListErrorsParams struct {
	/*Base64 encoded information for pagination
	  In: query
	*/
	Cursor *string `form:"cursor"`
	/*Number of entries to return
	  Maximum: 100
	  Minimum: 1
	  In: query
	  Default: 20
	*/
	Limit *int64 `form:"limit,default=20" binding:"numeric,gte=0,max=100"`
	/*ID of the project where the error was created
	  Required: true
	  In: path
	*/
	ProjectID uint64
	/*
	  In: query
	*/
	Query *string `form:"query"`
	/*
	  In: query
	  Default: "30d"
	*/
	QueryPeriod *string `form:"query_period,default=30d" binding:"oneof=15m 30m 1h 1d 7d 14d 30d"`
	/*
	  In: query
	  Default: "last_seen_desc"
	*/
	Sort *string `form:"sort,default=last_seen_desc" binding:"oneof=last_seen_desc first_seen_desc frequency_desc"`
	/*
	  In: query
	  Default: "24h"
	*/
	StatsPeriod *string `form:"stats_period,default=24h" binding:"oneof=15m 30m 1h 24h 7d 14d 30d"`
	/*
	  In: query
	  Default: "unresolved"
	*/
	Status *string `form:"status,default=unresolved" binding:"oneof=resolved unresolved ignored"`
}

type ListEventsParams struct {
	/*Base64 encoded information for pagination
	  In: query
	*/
	Cursor *string `form:"cursor"`
	/*ID of the error within the project
	  Required: true
	  In: path
	*/
	Fingerprint uint32
	/*Number of entries to return
	  Maximum: 100
	  Minimum: 1
	  In: query
	  Default: 20
	*/
	Limit *int64 `form:"limit,default=20" binding:"numeric,gte=0,max=100"`
	/*ID of the project where the error was created
	  Required: true
	  In: path
	*/
	ProjectID uint64
	/*
	  In: query
	  Default: "occurred_at_desc"
	*/
	Sort *string `form:"sort,default=occurred_at_desc" binding:"oneof=occurred_at_desc occurred_at_asc"`
}

type GetErrorParams struct {
	/*ID of the error that needs to be updated deleted
	  Required: true
	  In: path
	*/
	Fingerprint uint32
	/*ID of the project where the error was created
	  Required: true
	  In: path
	*/
	ProjectID uint64
}

type ListMessagesParams struct {
	/*Number of entries to return
	  Maximum: 100
	  Minimum: 1
	  In: query
	  Default: 20
	*/
	Limit *int64 `form:"limit,default=20" binding:"numeric,gte=0,max=100"`
	/*ID of the project where the message was created
	  Required: true
	  In: path
	*/
	ProjectID uint64
}
