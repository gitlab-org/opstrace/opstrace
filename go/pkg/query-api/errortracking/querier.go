package errortracking

import (
	"context"
	"fmt"
	"net/url"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	internalclickhouse "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.uber.org/zap"
)

type Querier interface {
	ListErrors(ctx context.Context, params *ListErrorsParams) ([]*Error, error)
	ListEvents(ctx context.Context, params *ListEventsParams) ([]*ErrorEvent, error)
	ListMessages(ctx context.Context, params *ListMessagesParams) ([]*MessageEvent, error)

	GetError(ctx context.Context, params *GetErrorParams) (*Error, error)
}

type querier struct {
	db     clickhouse.Conn
	logger *zap.Logger
}

var _ Querier = (*querier)(nil)

func NewQuerier(
	clickHouseDSN string,
	clickHouseCloudDSN string,
	opts *clickhouse.Options,
	logger *zap.Logger,
) (Querier, error) {
	var (
		db  clickhouse.Conn
		dsn string
		err error
	)

	if clickHouseCloudDSN != "" {
		u, err := url.Parse(clickHouseCloudDSN)
		if err != nil {
			return nil, fmt.Errorf("parse clickhouse cloud DSN: %w", err)
		}
		dsn = u.JoinPath(constants.ErrorTrackingAPIDatabaseName).String()
	} else {
		u, err := url.Parse(clickHouseDSN)
		if err != nil {
			return nil, fmt.Errorf("parse clickhouse DSN: %w", err)
		}
		dsn = u.JoinPath(constants.ErrorTrackingAPIDatabaseName).String()
	}

	db, err = internalclickhouse.GetDatabaseConnection(dsn, opts, logger, 3*time.Second)
	if err != nil {
		return nil, fmt.Errorf("getting database handle: %w", err)
	}
	return &querier{db: db, logger: logger}, nil
}
