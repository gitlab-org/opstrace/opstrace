package errortracking

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
	"go.uber.org/zap"
)

func (q *querier) ListErrors(ctx context.Context, params *ListErrorsParams) ([]*Error, error) {
	refTime := time.Now()
	qb, err := buildListErrorsQuery(params, refTime)
	if err != nil {
		return nil, err
	}

	rows, err := q.db.Query(
		qb.Context(ctx),
		qb.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to query clickhouse: %w", err)
	}

	var fingerprints []uint32
	var result []*Error
	for rows.Next() {
		// Scan the row as a ErrorTrackingError struct
		e := &et.ErrorTrackingError{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingError: %w", err)
		}
		// Check if the error has been marked ignored first, then update status
		// value accordingly.
		var status string
		if e.Ignored {
			status = errorIgnoredStr
		} else {
			if _, ok := errorStatusToStr[errorStatus(e.Status)]; !ok {
				return nil, fmt.Errorf("unexpected error status value %v", e.Status)
			}
			status = string(errorStatusToStr[errorStatus(e.Status)])
		}
		// Convert the ErrorTrackingError struct to a models.Error. This was
		// done because golang doesn't allow adding tags dinamically at runtime
		// to structs to be able to decode a models.Error which only defines
		// json tags.
		result = append(result, &Error{
			Actor:                 e.Actor,
			ApproximatedUserCount: e.ApproximatedUserCount,
			Description:           e.Description,
			EventCount:            e.EventCount,
			Fingerprint:           e.Fingerprint,
			FirstSeenAt:           e.FirstSeenAt,
			LastSeenAt:            e.LastSeenAt,
			Name:                  e.Name,
			ProjectID:             e.ProjectID,
			Status:                status,
		})
		// Keep a note of all fingerprints to fetch stats for
		fingerprints = append(fingerprints, e.Fingerprint)
	}
	rows.Close()

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read errors from clickhouse: %w", err)
	}

	// Get error stats for all the errors fetched
	if len(fingerprints) > 0 {
		statsPeriod := "24h" // swagger defn defaults to 24h, but just in case
		if params.StatsPeriod != nil {
			statsPeriod = *params.StatsPeriod
		}
		stats, err := q.getErrorStats(ctx, fingerprints, statsPeriod, params.ProjectID)
		if err != nil {
			return nil, fmt.Errorf("getting error stats: %w", err)
		}
		// For all errors we have...
		for idx, e := range result {
			// Check if we got corresponding stats...
			if _, ok := stats[e.Fingerprint]; ok {
				// If we did, update it in-place
				result[idx].Stats = &ErrorStats{
					Frequency: map[string]interface{}{
						statsPeriod: stats[e.Fingerprint],
					},
				}
			}
		}
	}

	// Note(Arun): Crude way of determining if list errors took more than 1 second/
	// Our current tracing solution is not yet capable to listing such operations.
	timeTaken := time.Since(refTime)
	if timeTaken > time.Second*1 {
		q.logger.
			With(zap.Uint64("projectID", params.ProjectID)).
			Info("list errors queries took longer than 1 second, took: ",
				zap.Float64("seconds", timeTaken.Seconds()),
			)
	}
	return result, nil
}

const baseQuery = `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64}
`

func buildListErrorsQuery(params *ListErrorsParams, refTime time.Time) (*clickhouse.QueryBuilder, error) {
	q := clickhouse.NewQueryBuilder().WithParams(map[string]clickhouse.Literal{
		"projectId":          clickhouse.Value(params.ProjectID),
		"errorsTable":        clickhouse.Identifier(constants.ErrorTrackingErrorsTableName),
		"errorsStatusTable":  clickhouse.Identifier(constants.ErrorTrackingErrorStatusTableName),
		"errorsIgnoredTable": clickhouse.Identifier(constants.ErrorTrackingErrorIgnoreTableName),
	}).Build(baseQuery)

	// Setup time filtering
	queryPeriod := "30d" // default to 30 days of data
	if params.QueryPeriod != nil {
		queryPeriod = *params.QueryPeriod
	}
	startTime, endTime, _, _ := common.InferTimelines(refTime, queryPeriod)
	q.Build(
		"AND last_seen_at >= {startTime:DateTime64(9)} AND last_seen_at <= {endTime:DateTime64(9)}",
	)
	q.WithParam("startTime", clickhouse.TimestampNano(startTime))
	q.WithParam("endTime", clickhouse.TimestampNano(endTime))
	q.Build("\n")

	// Status default value is unresolved, so we can skip the nil check.
	status, ok := errorStatusToInt[errorStatusStr(*params.Status)]
	if !ok {
		return nil, fmt.Errorf("unexpected error status %v", *params.Status)
	}
	if status == errorIgnored {
		q.Build("AND COALESCE(ignored_errors.ignored, FALSE) = TRUE")
	} else {
		q.Build("AND COALESCE(ignored_errors.ignored, FALSE) = FALSE ")
		q.Build("AND errors_status.status = {eStatus:UInt8}")
		q.WithParam("eStatus", clickhouse.Value(uint8(status)))
	}
	q.Build("\n")

	if params.Query != nil && len(*params.Query) > 2 {
		wildcard := "%" + *params.Query + "%"
		q.Build(
			"AND (errors.name ILIKE ? OR errors.description ILIKE ?)",
			wildcard, wildcard,
		)
	}

	// Sort default value is last_seen_desc so we can skip the nil check.
	orderBy := "ORDER BY"
	switch *params.Sort {
	case "first_seen_desc":
		orderBy += " first_seen_at DESC, fingerprint DESC"
	case "frequency_desc":
		orderBy += " event_count DESC, fingerprint DESC"
	default:
		orderBy += " last_seen_at DESC, fingerprint DESC"
	}
	q.Build(orderBy)
	q.Build("LIMIT {lim:Int64}")
	q.WithParam("lim", clickhouse.Value(*params.Limit))
	page, err := DecodePage(params.Cursor)
	if err != nil {
		return nil, err
	}
	// Limit default value is 20, so we can skip the nil check.
	offset := (page - 1) * int(*params.Limit)
	q.Build("OFFSET {oft:Int64}")
	q.WithParam("oft", clickhouse.Value(int64(offset)))

	q.Build(`
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`)
	return q, nil
}

func (q *querier) GetError(ctx context.Context, params *GetErrorParams) (*Error, error) {
	var result *Error

	qb := buildGetErrorQuery(params)

	row := q.db.QueryRow(
		qb.Context(ctx),
		qb.SQL(),
	)
	if err := row.Err(); err != nil {
		return nil, fmt.Errorf("failed to query error : %w", err)
	}

	// Scan the row as a ErrorTrackingError struct
	e := &et.ErrorTrackingError{}
	err := row.ScanStruct(e)

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to scan struct into ErrorTrackingError: %w", err)
	}
	// Check if the error has been marked ignored first, then update status
	// value accordingly.
	var status string
	if e.Ignored {
		status = errorIgnoredStr
	} else {
		if _, ok := errorStatusToStr[errorStatus(e.Status)]; !ok {
			return nil, fmt.Errorf("unexpected error status value %v", e.Status)
		}
		status = string(errorStatusToStr[errorStatus(e.Status)])
	}
	// Get error stats for the error fetched
	// We align the response here with Sentry's which returns 24h & 30d stats by default,
	// see: https://docs.sentry.io/api/events/retrieve-an-issue/
	perPeriodStats := make(map[string]interface{})
	for _, statsPeriod := range []string{common.Period24h, common.Period30d} {
		stats, err := q.getErrorStats(ctx, []uint32{e.Fingerprint}, statsPeriod, params.ProjectID)
		if err != nil {
			return nil, fmt.Errorf("getting error stats: %w", err)
		}
		perPeriodStats[statsPeriod] = stats[e.Fingerprint]
	}

	// Convert the ErrorTrackingError struct to a models.Error. This was
	// done because golang doesn't allow adding tags dynamically at runtime
	// to structs to be able to decode a models.Error which only defines
	// json tags.
	result = &Error{
		Actor:                 e.Actor,
		ApproximatedUserCount: e.ApproximatedUserCount,
		Description:           e.Description,
		EventCount:            e.EventCount,
		Fingerprint:           e.Fingerprint,
		FirstSeenAt:           e.FirstSeenAt,
		LastSeenAt:            e.LastSeenAt,
		Name:                  e.Name,
		ProjectID:             e.ProjectID,
		Status:                status,
		Stats:                 &ErrorStats{Frequency: perPeriodStats},
	}

	if err := row.Err(); err != nil {
		return nil, fmt.Errorf("failed to read error from clickhouse: %w", err)
	}
	return result, nil
}

const baseGetQuery = `
SELECT
	errors.project_id AS project_id,
	errors.fingerprint AS fingerprint,
	errors.name AS name,
	errors.description AS description,
	errors.actor AS actor,
	errors.event_count AS event_count,
	errors.approximated_user_count AS approximated_user_count,
	errors.last_seen_at AS last_seen_at,
	errors.first_seen_at AS first_seen_at,
	COALESCE(errors_status.status, 1) AS status,
	COALESCE(ignored_errors.ignored, FALSE) AS ignored
FROM (
	SELECT
		project_id,
		fingerprint,
		any(name) as name,
		any(description) as description,
		any(actor) as actor,
		sum(event_count) as event_count,
		uniqMerge(approximated_user_count) as approximated_user_count,
		max(last_seen_at) as last_seen_at,
		min(first_seen_at) as first_seen_at
    FROM {errorsTable:Identifier}
    WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
    GROUP BY project_id, fingerprint
) as errors
LEFT JOIN (
	SELECT project_id, argMax(status, updated_at) as status, fingerprint
	FROM {errorsStatusTable:Identifier}
	WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
	GROUP BY project_id, fingerprint
) errors_status ON errors_status.project_id = errors.project_id AND errors_status.fingerprint = errors.fingerprint
LEFT JOIN (
	SELECT project_id, fingerprint, TRUE AS ignored
	FROM {errorsIgnoredTable:Identifier}
	WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
	GROUP BY project_id, fingerprint
) ignored_errors ON ignored_errors.project_id = errors.project_id AND ignored_errors.fingerprint = errors.fingerprint
WHERE project_id = {projectId:UInt64} AND fingerprint = {fingerprint:UInt32}
`

func buildGetErrorQuery(params *GetErrorParams) *clickhouse.QueryBuilder {
	q := clickhouse.NewQueryBuilder().WithParams(map[string]clickhouse.Literal{
		"projectId":          clickhouse.Value(params.ProjectID),
		"errorsTable":        clickhouse.Identifier(constants.ErrorTrackingErrorsTableName),
		"errorsStatusTable":  clickhouse.Identifier(constants.ErrorTrackingErrorStatusTableName),
		"errorsIgnoredTable": clickhouse.Identifier(constants.ErrorTrackingErrorIgnoreTableName),
		"fingerprint":        clickhouse.Value(params.Fingerprint),
	})
	q.Build(baseGetQuery)
	q.Build(`
SETTINGS merge_tree_min_rows_for_concurrent_read_for_remote_filesystem = 8192,
merge_tree_min_bytes_for_concurrent_read_for_remote_filesystem = 100000;
`)
	return q
}
