package errortracking

import (
	"context"
	"time"
)

type MockQuerier struct {
	PassedFilter interface{}
	Err          error
}

var _ Querier = (*MockQuerier)(nil)

var TS = time.Unix(1701264345, 0).UTC()

func (m *MockQuerier) ListErrors(_ context.Context, passed *ListErrorsParams) ([]*Error, error) {
	if m.Err != nil {
		return nil, m.Err
	}
	m.PassedFilter = passed
	return []*Error{
		{
			Actor:                 "actor",
			ApproximatedUserCount: 1,
			Description:           "description",
			EventCount:            1,
			Fingerprint:           1234,
			Name:                  "name",
			ProjectID:             1,
			Status:                "unresolved",
		},
	}, nil
}

func (m *MockQuerier) ListEvents(_ context.Context, passed *ListEventsParams) ([]*ErrorEvent, error) {
	if m.Err != nil {
		return nil, m.Err
	}
	m.PassedFilter = passed
	return []*ErrorEvent{
		{
			Actor:       "actor",
			Description: "description",
			Environment: "env",
			Fingerprint: 1234,
			Name:        "name",
			Payload:     "payload",
			Platform:    "platform",
			ProjectID:   1,
		},
	}, nil
}

func (m *MockQuerier) ListMessages(_ context.Context, passed *ListMessagesParams) ([]*MessageEvent, error) {
	if m.Err != nil {
		return nil, m.Err
	}
	m.PassedFilter = passed
	return []*MessageEvent{
		{
			Environment: "env",
			EventID:     "1",
			Level:       "info",
			Message:     "message",
			Platform:    "platform",
			ProjectID:   1,
			Release:     "release",
			Timestamp:   TS,
		},
	}, nil
}

func (m *MockQuerier) GetError(_ context.Context, passed *GetErrorParams) (*Error, error) {
	if m.Err != nil {
		return nil, m.Err
	}
	m.PassedFilter = passed
	return &Error{
		Actor:                 "actor",
		ApproximatedUserCount: 1,
		Description:           "description",
		EventCount:            1,
		Fingerprint:           1234,
		Name:                  "name",
		ProjectID:             1,
		Status:                "unresolved",
	}, nil
}
