package errortracking_test

import (
	"context"
	"testing"

	"github.com/ClickHouse/clickhouse-go/v2"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/errortracking"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var (
	logger  *zap.SugaredLogger
	testEnv *testutils.ClickHouseServer
	conn    clickhouse.Conn
	querier errortracking.Querier
)

func TestErrorTrackingQuerier(t *testing.T) {
	RegisterFailHandler(Fail)
	suiteConfig, reporterConfig := GinkgoConfiguration()
	RunSpecs(t, "Error Tracking Querier Suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func(ctx SpecContext) {
	config := zap.NewDevelopmentConfig()
	sink := zapcore.AddSync(GinkgoWriter)
	encoder := zapcore.NewConsoleEncoder(config.EncoderConfig)
	core := zapcore.NewCore(encoder, sink, zap.DebugLevel)
	log := zap.New(core).WithOptions(zap.ErrorOutput(sink))
	logger = log.Sugar()

	var err error
	testEnv, conn, err = testutils.NewClickHouseServerAndConnection(context.Background())
	Expect(err).ToNot(HaveOccurred())
	DeferCleanup(func(ctx SpecContext) {
		_ = testEnv.Terminate(ctx)
	})

	Expect(testEnv.CreateDatabasesAndRunMigrations(ctx, constants.ErrorTrackingAPIDatabaseName)).To(Succeed())
	dsn, err := testEnv.GetDSN(ctx, "")
	Expect(err).ToNot(HaveOccurred())
	querier, err = errortracking.NewQuerier(dsn, "", nil, logger.Desugar())
	Expect(err).ToNot(HaveOccurred())
})

var _ = AfterSuite(func() {
	err := logger.Sync()
	Expect(err).ToNot(HaveOccurred())
})
