package errortracking

import (
	"context"
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

func (q *querier) ListMessages(ctx context.Context, params *ListMessagesParams) ([]*MessageEvent, error) {
	var result []*MessageEvent

	qb := buildListMessageQuery(params)

	rows, err := q.db.Query(
		qb.Context(ctx),
		qb.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to list messages: %w", err)
	}
	for rows.Next() {
		e := &et.ErrorTrackingMessageEvent{}
		err := rows.ScanStruct(e)
		if err != nil {
			return nil, fmt.Errorf("failed to scan struct into ErrorTrackingMessageEvent: %w", err)
		}

		result = append(result, &MessageEvent{
			Environment: e.Environment,
			EventID:     e.EventID,
			Level:       e.Level,
			Message:     e.Message,
			Platform:    e.Platform,
			ProjectID:   e.ProjectID,
			Release:     e.Release,
			Timestamp:   e.Timestamp,
		})
	}
	err = rows.Close()
	if err != nil {
		return nil, fmt.Errorf("failed to close connection while reading from clickhouse: %w", err)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to read events from clickhouse: %w", err)
	}
	return result, nil
}

const baseQueryListMessages = `SELECT
	event_id,
	project_id,
	timestamp,
	environment,
	level,
	message,
	actor,
	platform,
	release,
	server_name
FROM {messagesTable:Identifier}
`

const defaultListIssueLimit = 20

func buildListMessageQuery(params *ListMessagesParams) *clickhouse.QueryBuilder {
	q := clickhouse.NewQueryBuilder().
		WithParams(map[string]clickhouse.Literal{
			"messagesTable": clickhouse.String(constants.ErrorTrackingMessageEventsTableName),
		})
	q.Build(baseQueryListMessages)

	q.Build("WHERE project_id IN ( ? )", params.ProjectID)

	if params.Limit != nil {
		q.Build("LIMIT ?", *params.Limit)
	} else {
		q.Build("LIMIT ?", int64(defaultListIssueLimit))
	}

	return q
}
