package errortracking_test

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	et "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/types"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/errortracking"
)

func getETConn(ctx context.Context) (clickhouse.Conn, error) {
	dsn, err := testEnv.GetDSN(ctx, "")
	if err != nil {
		return nil, fmt.Errorf("get dsn: %w", err)
	}
	opts, err := clickhouse.ParseDSN(dsn)
	if err != nil {
		return nil, fmt.Errorf("parse dsn: %w", err)
	}
	opts.Auth.Database = constants.ErrorTrackingAPIDatabaseName
	opts.Debug = true
	db, err := clickhouse.Open(opts)
	if err != nil {
		return nil, fmt.Errorf("open db: %w", err)
	}
	return db, nil
}

func generateErrorTrackingEvent(ctx context.Context, db clickhouse.Conn) (*et.ErrorTrackingErrorEvent, error) {
	payload, err := os.ReadFile("testdata/exceptions/python_repl_event.json")
	if err != nil {
		return nil, fmt.Errorf("could not read testdata/exceptions/python_repl_event.json: %w", err)
	}
	event := types.Event{}
	err = json.Unmarshal(payload, &event)
	if err != nil {
		return nil, fmt.Errorf("event unmarshal : %w", err)
	}

	expectedErrorEvent := et.NewErrorTrackingErrorEvent(1, &event, payload)
	// To make sure the data remains in range with TTL policy of tables
	expectedErrorEvent.OccurredAt = time.Now()

	err = db.Exec(ctx, expectedErrorEvent.AsInsertStmt(time.UTC))
	if err != nil {
		return nil, fmt.Errorf("insert error: %w", err)
	}
	s := &et.ErrorTrackingErrorStatus{
		ProjectID:   expectedErrorEvent.ProjectID,
		Fingerprint: expectedErrorEvent.Fingerprint,
		Status:      uint8(0),
		UserID:      uint64(0),
		Actor:       uint8(2),
		UpdatedAt:   time.Now(),
	}
	err = db.Exec(ctx, s.AsInsertStmt(time.UTC))
	if err != nil {
		return nil, fmt.Errorf("insert error: %w", err)
	}

	return expectedErrorEvent, nil
}

func generateMessageEvent(ctx context.Context, db clickhouse.Conn) (*et.ErrorTrackingMessageEvent, error) {
	payload, err := os.ReadFile("testdata/messages/python_message.json")
	if err != nil {
		return nil, fmt.Errorf("could not read testdata/messages/python_message.json: %w", err)
	}
	event := types.Event{}
	err = json.Unmarshal(payload, &event)
	if err != nil {
		return nil, fmt.Errorf("event unmarshal: %w", err)
	}
	messageEvent := et.NewErrorTrackingMessageEvent(1, &event, payload)
	messageEvent.Timestamp = time.Now()
	err = db.Exec(ctx, messageEvent.AsInsertStmt(time.UTC))
	if err != nil {
		return nil, fmt.Errorf("insert error: %w", err)
	}
	return messageEvent, nil
}

var _ = Describe("ErrorTrackingQuerier", Ordered, func() {
	var (
		expectedErrorEvent *et.ErrorTrackingErrorEvent
		err                error
		db                 clickhouse.Conn
	)
	Context("By querying", func() {
		BeforeAll(func(ctx SpecContext) {
			db, err = getETConn(ctx)
			Expect(err).ToNot(HaveOccurred())
			expectedErrorEvent, err = generateErrorTrackingEvent(ctx, db)
			Expect(err).ToNot(HaveOccurred())
			_, err = generateMessageEvent(ctx, db)
			Expect(err).ToNot(HaveOccurred())
		})

		It("returns list of errors", func(ctx SpecContext) {
			limit := int64(20)
			period := "24h"
			statsPeriod := "24h"
			sort := "last_seen_desc"
			status := "unresolved"

			Eventually(func(g Gomega) {
				items, err := querier.ListErrors(ctx, &errortracking.ListErrorsParams{
					Cursor:      nil,
					Limit:       &limit,
					ProjectID:   1,
					Query:       nil,
					QueryPeriod: &period,
					Sort:        &sort,
					StatsPeriod: &statsPeriod,
					Status:      &status,
				})
				g.Expect(err).ToNot(HaveOccurred())
				g.Expect(items).To(HaveLen(1))
			}).WithTimeout(time.Second * 10).WithPolling(time.Second * 2).Should(Succeed())
		})

		It("gets an error", func(ctx SpecContext) {
			Eventually(func(g Gomega) {
				item, err := querier.GetError(ctx, &errortracking.GetErrorParams{
					Fingerprint: expectedErrorEvent.Fingerprint,
					ProjectID:   1,
				})
				g.Expect(err).ToNot(HaveOccurred())
				g.Expect(item).ToNot(BeNil())
				g.Expect(item.Fingerprint).To(Equal(expectedErrorEvent.Fingerprint))
			}).WithTimeout(time.Second * 10).WithPolling(time.Second * 2).Should(Succeed())

		})

		It("gets error events", func(ctx SpecContext) {
			limit := int64(20)
			sort := "occurred_at_desc"
			Eventually(func(g Gomega) {
				items, err := querier.ListEvents(ctx, &errortracking.ListEventsParams{
					Cursor:      nil,
					Limit:       &limit,
					Sort:        &sort,
					Fingerprint: expectedErrorEvent.Fingerprint,
					ProjectID:   1,
				})
				g.Expect(err).ToNot(HaveOccurred())
				g.Expect(items).To(HaveLen(1))
			}).WithTimeout(time.Second * 10).WithPolling(time.Second * 2).Should(Succeed())
		})
	})

	It("gets message events", func(ctx SpecContext) {
		limit := int64(20)
		Eventually(func(g Gomega) {
			items, err := querier.ListMessages(ctx, &errortracking.ListMessagesParams{
				Limit:     &limit,
				ProjectID: 1,
			})
			g.Expect(err).ToNot(HaveOccurred())
			g.Expect(items).To(HaveLen(1))
		}).WithTimeout(time.Second * 10).WithPolling(time.Second * 2).Should(Succeed())
	})
})
