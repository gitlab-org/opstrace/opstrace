package errortracking

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/clickhouse"
)

type FrequencyPair [2]uint64

/*
Example aggregation query:
SELECT

	fingerprint,
	toUInt64(occurred_at) AS t,
	count(*)

FROM gl_error_tracking_error_events
WHERE fingerprint IN ('2698336012')
GROUP BY

	fingerprint,
	t

ORDER BY

	fingerprint ASC
	t DESC

LIMIT 10
*/
const baseQueryErrorFrequencyTmpl = `
SELECT
	fingerprint,
	toUInt64(%s(occurred_at)) AS t,
	count() AS cnt
FROM {errorEventsTable:Identifier}
`

const baseQueryTimeSeriesGeneratorTmpl = `
WITH
    %s(?) AS start,
    %s(?) AS end
SELECT arrayJoin(arrayMap(x -> toUInt64(toDateTime(x)), range(toUInt64(start), toUInt64(end), ?))) AS t
`

func (q *querier) getErrorStats(
	ctx context.Context,
	fingerprints []uint32,
	statsPeriod string,
	projectID uint64,
) (map[uint32][]FrequencyPair, error) {
	// the reference point in time around which we process statsPeriod against
	// to generate error stats data, for now, the current time.
	refTime := time.Now()

	qb := buildErrorStatsQuery(fingerprints, refTime, statsPeriod, projectID)

	rows, err := q.db.Query(
		qb.Context(ctx),
		qb.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("querying error stats: %w", err)
	}

	type queryColumns struct {
		Fingerprint uint32 `ch:"fingerprint"`
		Timestamp   uint64 `ch:"t"`
		Count       uint64 `ch:"cnt"`
	}

	queriedData := make(map[uint32]map[uint64]uint64)
	for rows.Next() {
		q := new(queryColumns)
		err := rows.ScanStruct(q)
		if err != nil {
			return nil, fmt.Errorf("reading stats output, scan struct: %w", err)
		}
		if _, ok := queriedData[q.Fingerprint]; !ok {
			queriedData[q.Fingerprint] = make(map[uint64]uint64)
		}
		queriedData[q.Fingerprint][q.Timestamp] = q.Count
	}

	result := make(map[uint32][]FrequencyPair)
	timeWindow, err := q.getQueryTimeWindow(ctx, refTime, statsPeriod)
	if err != nil {
		return nil, fmt.Errorf("getting query time window: %w", err)
	}
	// for all fingerprints we queried data for...
	for _, fp := range fingerprints {
		result[fp] = make([]FrequencyPair, 0)
		// generate non-sparse data for the entire time window we queried data for
		for _, ts := range timeWindow {
			if _, ok := queriedData[fp][ts]; ok {
				result[fp] = append(result[fp], FrequencyPair{ts, queriedData[fp][ts]})
			} else {
				result[fp] = append(result[fp], FrequencyPair{ts, 0})
			}
		}
	}

	return result, nil
}

func (q *querier) getQueryTimeWindow(ctx context.Context, refTime time.Time, statsPeriod string) ([]uint64, error) {
	qb := buildTimeSeriesQuery(refTime, statsPeriod)

	rows, err := q.db.Query(
		qb.Context(ctx),
		qb.SQL(),
	)
	if err != nil {
		return nil, fmt.Errorf("querying error stats: %w", err)
	}

	type timeSeriesColumns struct {
		Timestamp uint64 `ch:"t"`
	}

	window := []uint64{}
	for rows.Next() {
		t := new(timeSeriesColumns)
		err := rows.ScanStruct(t)
		if err != nil {
			return nil, fmt.Errorf("reading timeseries output, scan struct: %w", err)
		}
		window = append(window, t.Timestamp)
	}
	return window, nil
}

func buildTimeSeriesQuery(refTime time.Time, statsPeriod string) *clickhouse.QueryBuilder {
	startTime, endTime, stepSeconds, timeFunc := common.InferTimelines(refTime, statsPeriod)
	builder := clickhouse.NewQueryBuilder()
	builder.Build(
		fmt.Sprintf(baseQueryTimeSeriesGeneratorTmpl, timeFunc, timeFunc),
		clickhouse.TimestampNano(startTime), clickhouse.TimestampNano(endTime), uint64(stepSeconds),
	)
	// log.WithField("startTime", startTime).WithField("endTime", endTime).Debug("time series query")
	return builder
}

func buildErrorStatsQuery(
	fingerprints []uint32,
	refTime time.Time,
	statsPeriod string,
	projectID uint64,
) *clickhouse.QueryBuilder {
	startTime, endTime, _, timeFunc := common.InferTimelines(refTime, statsPeriod)

	builder := clickhouse.NewQueryBuilder().WithParams(
		map[string]clickhouse.Literal{
			"errorEventsTable": clickhouse.String(constants.ErrorTrackingEventsTableName),
		})
	builder.Build(
		fmt.Sprintf(baseQueryErrorFrequencyTmpl, timeFunc)+
			"WHERE fingerprint IN ? "+
			"AND project_id = ? "+
			"AND t >= ? AND t <= ? "+
			"GROUP BY fingerprint, t "+
			"ORDER BY fingerprint ASC, t DESC",
		clickhouse.Array[uint32](fingerprints),
		clickhouse.Value(projectID),
		clickhouse.TimestampNano(startTime), clickhouse.TimestampNano(endTime),
	)
	// log.WithField("startTime", startTime).WithField("endTime", endTime).Debug("error stats query")
	return builder
}
