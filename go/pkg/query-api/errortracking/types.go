package errortracking

import "time"

type ListErrorResult struct {
	NextPageToken string `json:"next_page_token,omitempty"`

	Body []*Error `json:"body,omitempty"`
}

type GetErrorResult struct {
	Body *Error `json:"body,omitempty"`
}

type Error struct {
	// actor
	// Example: PostsController#edit
	Actor string `json:"actor,omitempty"`

	// approximated user count
	ApproximatedUserCount uint64 `json:"approximated_user_count,omitempty"`

	// description
	// Example: Missing template posts/edit
	Description string `json:"description,omitempty"`

	// event count
	EventCount uint64 `json:"event_count,omitempty"`

	// fingerprint
	Fingerprint uint32 `json:"fingerprint,omitempty"`

	// first seen at
	// Format: date-time
	FirstSeenAt time.Time `json:"first_seen_at,omitempty"`

	// last seen at
	// Format: date-time
	LastSeenAt time.Time `json:"last_seen_at,omitempty"`

	// name
	// Example: ActionView::MissingTemplate
	Name string `json:"name,omitempty"`

	// project id
	ProjectID uint64 `json:"project_id,omitempty"`

	// stats
	Stats *ErrorStats `json:"stats,omitempty"`

	// Status of the error
	// Enum: [unresolved resolved ignored]
	Status string `json:"status,omitempty"`
}

type ErrorStats struct {

	// frequency
	Frequency interface{} `json:"frequency,omitempty"`
}

type ListErrorEventResult struct {
	NextPageToken string        `json:"next_page_token,omitempty"`
	Body          []*ErrorEvent `json:"body,omitempty"`
}

type ErrorEvent struct {
	// actor
	// Example: PostsController#edit
	Actor string `json:"actor,omitempty"`

	// description
	// Example: Missing template posts/edit
	Description string `json:"description,omitempty"`

	// environment
	// Example: production
	Environment string `json:"environment,omitempty"`

	// fingerprint
	Fingerprint uint32 `json:"fingerprint,omitempty"`

	// name
	// Example: ActionView::MissingTemplate
	Name string `json:"name,omitempty"`

	// JSON encoded string
	Payload string `json:"payload,omitempty"`

	// platform
	// Example: ruby
	Platform string `json:"platform,omitempty"`

	// project Id
	ProjectID uint64 `json:"projectId,omitempty"`
}

type errorStatus uint8

const (
	errorUnresolved errorStatus = iota
	errorResolved
	errorIgnored
)

type errorStatusStr string

const (
	errorUnresolvedStr = "unresolved"
	errorResolvedStr   = "resolved"
	errorIgnoredStr    = "ignored"
)

var errorStatusToInt = map[errorStatusStr]errorStatus{
	errorUnresolvedStr: errorUnresolved,
	errorResolvedStr:   errorResolved,
	errorIgnoredStr:    errorIgnored,
}

var errorStatusToStr = map[errorStatus]errorStatusStr{
	errorUnresolved: errorUnresolvedStr,
	errorResolved:   errorResolvedStr,
	errorIgnored:    errorIgnoredStr,
}

type MessageEvent struct {
	// environment
	// Example: production
	Environment string `json:"environment,omitempty"`

	// event Id
	EventID string `json:"eventId,omitempty"`

	// level
	// Example: info
	Level string `json:"level,omitempty"`

	// message
	// Example: some message from the SDK
	Message string `json:"message,omitempty"`

	// platform
	// Example: ruby
	Platform string `json:"platform,omitempty"`

	// project Id
	ProjectID uint64 `json:"projectId,omitempty"`

	// release
	// Example: v1.0.0
	Release string `json:"release,omitempty"`

	// timestamp
	Timestamp time.Time `json:"timestamp,omitempty"`
}

type ListMessageEventResult struct {
	NextPageToken string          `json:"next_page_token,omitempty"`
	Body          []*MessageEvent `json:"body,omitempty"`
}
