package migrations

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"net/url"
	"text/template"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/pressly/goose/v3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/store"
)

// TemplateData is has standard fields passed to every migration template.
// Embed this in a struct type to use these fields with other ones in a template.
// e.g.
//
//	type MyTemplateData struct {
//	    migrations.TemplateData
//	    MyField string
//	}
//
// You cannot embed any or interface{} types as the fields will not be
// accessible to the template.
type TemplateData struct {
	// DatabaseName is the name of the database where migrations are running.
	// All migrations should act on this database.
	DatabaseName string
	// SelfHostedVersion is true if the migrations are running on a self-hosted.
	// This will be removed when the tenant-operator is removed.
	SelfHostedVersion bool

	// If DevelopmentMode is true then the underlying migrations are executed on a non-replicated table engine.
	// This flag is mutually exclusive with `SelfHostedVersion` as we expect them to run on a replicated table engine.
	DevelopmentMode bool
}

// NewMigration creates a new migration from a Go template tmpl.
// The data is passed to the template as the arguments.
// The filename is used to name the template and build the migration number.
func NewMigration(tmpl string, data any, filename string) (*goose.Migration, error) {
	v, err := goose.NumericComponent(filename)
	if err != nil {
		return nil, fmt.Errorf("migration file numeric component: %w", err)
	}

	t, err := template.New(filename).Parse(tmpl)
	if err != nil {
		return nil, fmt.Errorf("parsing template %s: %w", filename, err)
	}
	var buf bytes.Buffer
	if err := t.Execute(&buf, data); err != nil {
		return nil, fmt.Errorf("executing template %s: %w", filename, err)
	}

	return goose.NewGoMigration(
		v,
		&goose.GoFunc{
			RunTx: func(ctx context.Context, tx *sql.Tx) error {
				_, err := tx.Exec(buf.String())
				//nolint:wrapcheck
				return err
			},
		},
		nil,
	), nil
}

type RenderFunc func(selfHosted bool, devel bool) (*goose.Migration, error)

type RenderFuncs []RenderFunc

func (r RenderFuncs) Render(selfHosted bool, devel bool) ([]*goose.Migration, error) {
	ms := make([]*goose.Migration, len(r))

	for i, f := range r {
		m, err := f(selfHosted, devel)
		if err != nil {
			return nil, err
		}
		ms[i] = m
	}

	return ms, nil
}

func RunGooseMigrations(ctx context.Context, dsn, dbName string, migrations []*goose.Migration, devMode bool) error {
	u, err := url.Parse(dsn)
	if err != nil {
		return fmt.Errorf("parsing dsn: %w", err)
	}
	u.Path = dbName

	db, err := sql.Open("clickhouse", u.String())
	if err != nil {
		return fmt.Errorf("sql open: %w", err)
	}

	db.SetConnMaxLifetime(time.Minute * 5)
	db.SetConnMaxIdleTime(time.Minute * 5)

	if err := db.Ping(); err != nil {
		return fmt.Errorf("clickhouse db ping: %w", err)
	}

	defer func() {
		_ = db.Close()
	}()

	clickhouseStore := &store.ClickHouseStore{
		TName: "goose_db_version_v2",
	}

	if devMode {
		// In development mode, there is no `KeeperMap` engine, so we have to use MergeTree engine for version table.
		// We rely on our own fork to add this option.
		clickhouseStore.AttachOptions(map[string]string{
			"NO_KEEPER_MAP": "true",
		})
	}

	if !devMode {
		clickhouseStore.AttachOptions(map[string]string{
			"ON_CLUSTER": "true",
		})
	}

	provider, err := goose.NewProvider(
		"",
		db,
		nil,
		goose.WithGoMigrations(migrations...),
		goose.WithAllowOutofOrder(true),
		goose.WithStore(clickhouseStore),
	)
	if err != nil {
		return fmt.Errorf("new provider: %w", err)
	}

	if _, err := provider.Up(ctx); err != nil {
		return fmt.Errorf("provider up: %w", err)
	}
	return nil
}

func CreateDB(ctx context.Context, conn clickhouse.Conn, dbName string) error {
	query := `CREATE DATABASE IF NOT EXISTS {dbName:Identifier}`
	if err := conn.Exec(
		ctx,
		query,
		clickhouse.Named("dbName", dbName),
	); err != nil {
		return fmt.Errorf("creating database %s: %w", dbName, err)
	}
	return nil
}
