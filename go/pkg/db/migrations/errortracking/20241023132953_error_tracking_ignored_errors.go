package errortracking

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const ignoredErrorsTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  project_id UInt64,
  fingerprint UInt32,
  user_id Nullable(UInt64) DEFAULT NULL,
  updated_at DateTime64(6, 'UTC')
)
{{if .DevelopmentMode}}ENGINE = ReplacingMergeTree{{else}}
ENGINE = ReplicatedReplacingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
ORDER BY (project_id, fingerprint)
TTL toDateTime(updated_at) + INTERVAL 90 day;
`

func renderIgnoredErrors(selfHosted, developmentMode bool) (*goose.Migration, error) {
	return renderMigration(ignoredErrorsTmpl, TemplateData{
		TableName: constants.ErrorTrackingErrorIgnoreTableName,
	}, selfHosted, developmentMode)
}
