package errortracking

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const errorStatusTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  project_id UInt64,
  fingerprint UInt32,
  status UInt8, -- 0 unresolved, 1 resolved
  user_id UInt64,
  actor UInt8, -- 0 status changed by user, 1 status changed by system (new event happened after resolve)
  updated_at DateTime64(6, 'UTC')
)
{{if .DevelopmentMode}}ENGINE = ReplacingMergeTree{{else}}
ENGINE = ReplicatedReplacingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
ORDER BY (project_id, fingerprint)
TTL toDateTime(updated_at) + INTERVAL 90 day;
`

func renderErrorStatus(selfHosted, developmentMode bool) (*goose.Migration, error) {
	return renderMigration(errorStatusTmpl, TemplateData{
		TableName: constants.ErrorTrackingErrorStatusTableName,
	}, selfHosted, developmentMode)
}
