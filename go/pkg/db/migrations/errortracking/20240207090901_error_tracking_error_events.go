package errortracking

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const errorEventsTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}} (
  project_id UInt64,
  fingerprint UInt32,
  name String,
  description String,
  actor String,
  environment LowCardinality(String),
  platform String,
  level LowCardinality(String),
  user_identifier String,
  payload String CODEC(LZ4HC(9)), -- AVG 20K bytes
  occurred_at DateTime64(6, 'UTC') -- Precision till microseconds
)
{{if .DevelopmentMode}}ENGINE = MergeTree{{else}}
ENGINE = ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
PARTITION BY toYYYYMM(occurred_at)
ORDER BY (project_id, fingerprint, occurred_at)
TTL toDateTime(occurred_at) + INTERVAL 90 day;
`

func renderErrorEvents(selfHosted, developmentMode bool) (*goose.Migration, error) {
	return renderMigration(errorEventsTmpl, TemplateData{
		TableName: constants.ErrorTrackingEventsTableName,
	}, selfHosted, developmentMode)
}
