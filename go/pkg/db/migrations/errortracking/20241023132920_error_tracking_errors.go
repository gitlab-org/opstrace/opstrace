package errortracking

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const errorsTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
    project_id UInt64,
    fingerprint UInt32,
    name String,
    description String,
    actor String,
    event_count UInt64,
    approximated_user_count AggregateFunction(uniq, String),
    last_seen_at SimpleAggregateFunction(max, DateTime64(6, 'UTC')),
    first_seen_at SimpleAggregateFunction(min, DateTime64(6, 'UTC'))
)
{{if .DevelopmentMode}}ENGINE = SummingMergeTree{{else}}
ENGINE = ReplicatedSummingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
ORDER BY (project_id, fingerprint)
TTL toDateTime(first_seen_at) + INTERVAL 90 day;
`

func renderErrors(selfHosted, developmentMode bool) (*goose.Migration, error) {
	return renderMigration(errorsTmpl, TemplateData{
		TableName: constants.ErrorTrackingErrorsTableName,
	}, selfHosted, developmentMode)
}
