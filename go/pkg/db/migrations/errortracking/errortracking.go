package errortracking

import (
	"runtime"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

// MigrationData defines all data we need to render errortracking-specific migrations.
type TemplateData struct {
	migrations.TemplateData
	TableName       string
	SourceTableName string
	TargetTableName string
}

func SetupMigrations(selfHostedVersion, developmentMode bool) ([]*goose.Migration, error) {
	return migrations.RenderFuncs{
		renderErrorEvents,
		renderErrors,
		renderErrorsMV,
		renderErrorStatus,
		renderIgnoredErrors,
		renderMessageEvents,
		renderSessions,
	}.Render(selfHostedVersion, developmentMode)
}

func renderMigration(
	tmpl string, data TemplateData, selfHosted, developmentMode bool) (*goose.Migration, error) {
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(1)

	data.TemplateData = migrations.TemplateData{
		DatabaseName:      constants.ErrorTrackingAPIDatabaseName,
		SelfHostedVersion: selfHosted,
		DevelopmentMode:   developmentMode,
	}

	return migrations.NewMigration(tmpl, data, filename)
}
