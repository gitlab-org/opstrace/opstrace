package errortracking

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const sessionsTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}} (
  project_id UInt64,
  session_id String,
  user_id Nullable(String),
  init UInt8,
  payload String CODEC(LZ4HC(9)), -- AVG 20K bytes
  started DateTime64(6, 'UTC'),
  occurred_at DateTime64(6, 'UTC'),
  duration DOUBLE,
  status  Enum('abnormal', 'exited', 'crashed', 'ok'),
  release String,
  environment Nullable(String)
)
{{if .DevelopmentMode}}ENGINE = MergeTree{{else}}
ENGINE = ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
PARTITION BY toYYYYMM(occurred_at)
ORDER BY (project_id, release, session_id, occurred_at)
TTL toDateTime(occurred_at) + INTERVAL 90 day;
`

func renderSessions(selfHosted, developmentMode bool) (*goose.Migration, error) {
	return renderMigration(sessionsTmpl, TemplateData{
		TableName: constants.ErrorTrackingSessionsTableName,
	}, selfHosted, developmentMode)
}
