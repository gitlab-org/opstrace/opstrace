package errortracking

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const messageEventsTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}} (
  event_id String,
  project_id UInt64,
  timestamp DateTime64(6, 'UTC'), -- Precision till microseconds
  is_deleted UInt8,
  fingerprint FixedString(32),

  environment Nullable(String),
  level Nullable(String),
  message String,
  actor Nullable(String),
  platform LowCardinality(Nullable(String)),
  release Nullable(String),
  sdk Nested
  (
    name String,
    version String
  ),
  server_name Nullable(String),
  stacktrace_frames Nested
  (
      abs_path Nullable(String),
      colno Nullable(UInt32),
      filename Nullable(String),
      function Nullable(String),
      lineno Nullable(UInt32),
      in_app Nullable(UInt8),
      package Nullable(String),
      module Nullable(String),
      context_line Nullable(String)
  ),
  payload String CODEC(LZ4HC(9)) -- AVG 20K bytes

)
{{if .DevelopmentMode}}ENGINE = ReplacingMergeTree{{else}}
ENGINE = ReplicatedReplacingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
PARTITION BY toYYYYMM(timestamp)
ORDER BY (project_id, timestamp, fingerprint, event_id)
TTL toDateTime(timestamp) + INTERVAL 90 day;
`

func renderMessageEvents(selfHosted, developmentMode bool) (*goose.Migration, error) {
	return renderMigration(messageEventsTmpl, TemplateData{
		TableName: constants.ErrorTrackingMessageEventsTableName,
	}, selfHosted, developmentMode)
}
