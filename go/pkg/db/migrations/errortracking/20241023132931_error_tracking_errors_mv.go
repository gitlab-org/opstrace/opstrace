package errortracking

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const errorsMV = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
    project_id,
    fingerprint,
    any(name) AS name,
    any(description) AS description,
    any(actor) AS actor,
    count() AS event_count,
    uniqState(user_identifier) AS approximated_user_count,
    max(occurred_at) AS last_seen_at,
    min(occurred_at) AS first_seen_at
FROM {{.DatabaseName}}.{{.SourceTableName}}
GROUP BY
    project_id,
    fingerprint;
`

func renderErrorsMV(selfHosted, developmentMode bool) (*goose.Migration, error) {
	return renderMigration(errorsMV, TemplateData{
		TableName:       constants.ErrorTrackingErrorsMv,
		SourceTableName: constants.ErrorTrackingEventsTableName,
		TargetTableName: constants.ErrorTrackingErrorsTableName,
	}, selfHosted, developmentMode)
}
