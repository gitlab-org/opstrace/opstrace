package migrations_test

import (
	"context"
	"testing"
	"time"

	"github.com/pressly/goose/v3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/analytics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/logging"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

func Test_Migrations(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*5)
	defer cancel()

	ch, err := testutils.NewClickHouseServer(ctx)
	require.NoError(t, err)
	t.Cleanup(func() {
		ch.Terminate(ctx)
	})

	// NOTE: we can only run self-hosted migrations against a local ClickHouse
	// at the moment. Cloud migrations are using replicated tables without
	// `ON CLUSTER` which breaks when running against a local ClickHouse.
	t.Run("self-hosted migrations", func(t *testing.T) {
		for _, migrationTest := range []struct {
			database      string
			migrationsFun func(bool, bool) ([]*goose.Migration, error)
		}{
			{
				constants.ErrorTrackingAPIDatabaseName,
				errortracking.SetupMigrations,
			},
			{
				constants.TracingDatabaseName,
				tracing.SetupMigrations,
			},
			{
				constants.MetricsDatabaseName,
				metrics.SetupMigrations,
			},
			{
				constants.LoggingDatabaseName,
				logging.SetupMigrations,
			},
			{
				constants.AlertsDatabaseName,
				alerts.SetupMigrations,
			},
			{
				constants.AnalyticsDatabaseName,
				analytics.SetupMigrations,
			},
		} {
			migrationTest := migrationTest
			t.Run(migrationTest.database, func(t *testing.T) {
				// Note(Arun): Parallel tests does not work with goose.Provider
				// due to connection(s) being closed/dropped with frequent errors:
				// such as `driver: bad connection; sql: connection is already closed`.
				// This is likely somewhere in how provider stitches connection but the cause remains elusive.
				// t.Parallel()

				require.NoError(t, ch.CreateDatabase(ctx, migrationTest.database))
				dsn, err := ch.GetDSN(ctx, migrationTest.database)
				require.NoError(t, err)

				// run migrations twice, they should be idempotent
				ms, err := migrationTest.migrationsFun(true, false)
				assert.NoError(t, err)
				err = migrations.RunGooseMigrations(ctx, dsn, migrationTest.database, ms, false)
				assert.NoError(t, err)
				err = migrations.RunGooseMigrations(ctx, dsn, migrationTest.database, ms, false)
				assert.NoError(t, err)
			})
		}
	})

	t.Run("development migrations", func(t *testing.T) {
		for _, migrationTest := range []struct {
			database      string
			migrationsFun func(bool, bool) ([]*goose.Migration, error)
		}{
			{
				constants.ErrorTrackingAPIDatabaseName,
				errortracking.SetupMigrations,
			},
			{
				constants.TracingDatabaseName,
				tracing.SetupMigrations,
			},
			{
				constants.MetricsDatabaseName,
				metrics.SetupMigrations,
			},
			{
				constants.LoggingDatabaseName,
				logging.SetupMigrations,
			},
			{
				constants.AlertsDatabaseName,
				alerts.SetupMigrations,
			},
			{
				constants.AnalyticsDatabaseName,
				analytics.SetupMigrations,
			},
		} {
			migrationTest := migrationTest
			t.Run(migrationTest.database, func(t *testing.T) {
				t.Skip("development tests can be enabled on local")
				t.Parallel()

				require.NoError(t, ch.CreateDatabase(ctx, migrationTest.database))
				dsn, err := ch.GetDSN(ctx, migrationTest.database)
				require.NoError(t, err)

				// run migrations twice, they should be idempotent
				ms, err := migrationTest.migrationsFun(false, true)
				assert.NoError(t, err)
				err = migrations.RunGooseMigrations(ctx, dsn, migrationTest.database, ms, true)
				assert.NoError(t, err)
				err = migrations.RunGooseMigrations(ctx, dsn, migrationTest.database, ms, true)
				assert.NoError(t, err)
			})
		}
	})
}
