package snowplow

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const enrichedEventsTmpl = `
CREATE TABLE IF NOT EXISTS snowplow.enriched_events (
  TenantId String CODEC(ZSTD(1)),
  ProjectId String CODEC(ZSTD(1)),
  IngestionTimestamp DateTime64(9) CODEC(Delta(8), ZSTD(1)),
  Message String CODEC(ZSTD(1))
)
ENGINE = MergeTree
ORDER BY (TenantId, ProjectId, IngestionTimestamp)
TTL toDateTime(IngestionTimestamp) + toIntervalDay(7)
SETTINGS index_granularity = 8192, ttl_only_drop_parts = 1;
`

func renderEnrichedEvents(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		enrichedEventsTmpl,
		TemplateData{
			TableName: constants.SnowplowEnrichedEventsTableName,
		},
		selfHosted,
		devel,
	)
}
