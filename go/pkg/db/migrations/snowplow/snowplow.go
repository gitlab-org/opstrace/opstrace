package snowplow

import (
	"runtime"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

type TemplateData struct {
	migrations.TemplateData
	TableName       string
	TargetTableName string
	SourceTableName string
}

// SetupMigrations renders and registers migrations for snowplow tables
func SetupMigrations(selfHostedVersion, developmentMode bool) ([]*goose.Migration, error) {
	return migrations.RenderFuncs{
		// enriched events table
		renderEnrichedEvents,
	}.Render(selfHostedVersion, developmentMode)
}

func renderMigration(
	tmpl string,
	data TemplateData,
	selfHosted bool,
	developmentMode bool,
) (*goose.Migration, error) {
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(1)

	data.TemplateData = migrations.TemplateData{
		DatabaseName:      constants.SnowplowDatabaseName,
		SelfHostedVersion: selfHosted,
		DevelopmentMode:   developmentMode,
	}

	return migrations.NewMigration(tmpl, data, filename)
}
