package analytics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const ingestionMainTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }} (
  TenantId String CODEC(ZSTD(1)),
  ProjectId String CODEC(ZSTD(1)),
  Dimension LowCardinality(String) CODEC(ZSTD(1)),
  AggTime DateTime64(9) CODEC(Delta(8), ZSTD(1)),
  BytesCount UInt64 CODEC(ZSTD(1)),
  EventsCount UInt64 CODEC(ZSTD(1))
)
{{if .DevelopmentMode}}ENGINE = SummingMergeTree{{else}}
ENGINE = ReplicatedSummingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
PARTITION BY toYYYYMM(AggTime)
ORDER BY (Dimension, TenantId, ProjectId, AggTime)
SETTINGS index_granularity = 8192, ttl_only_drop_parts = 1;
`

func renderIngestionMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		ingestionMainTmpl,
		TemplateData{
			TableName: constants.AnalyticsIngestionTableName,
		},
		selfHosted,
		devel,
	)
}
