package analytics

import (
	"runtime"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

// TemplateData defines all data we need to render logging-specific
// migrations.
type TemplateData struct {
	migrations.TemplateData
	TableName       string
	SourceTableName string
	TargetTableName string
}

func SetupMigrations(selfHostedVersion, developmentMode bool) ([]*goose.Migration, error) {
	steps := migrations.RenderFuncs{
		renderIngestionMain,
	}

	ms, err := steps.Render(selfHostedVersion, developmentMode)
	if err != nil {
		return nil, err
	}

	return ms, nil
}

func renderMigration(
	tmpl string,
	data TemplateData,
	selfHosted bool,
	developmentMode bool,
) (*goose.Migration, error) {
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(1)

	data.TemplateData = migrations.TemplateData{
		DatabaseName:      constants.AnalyticsDatabaseName,
		SelfHostedVersion: selfHosted,
		DevelopmentMode:   developmentMode,
	}

	return migrations.NewMigration(tmpl, data, filename)
}
