package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderExpHistogramMetricsCorrelationMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsCorrelationMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsExponentialHistogramCorrelationMetadataMVName,
			SourceTableName: constants.MetricsExponentialHistogramTableName,
			TargetTableName: constants.MetricsExponentialHistogramCorrelationMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
