package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderGaugeMetadataMVTarget(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsMetadataMVTargetTmpl,
		TemplateData{
			TableName: constants.MetricsGaugeMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
