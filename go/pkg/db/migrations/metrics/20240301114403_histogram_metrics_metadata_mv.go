package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderHistogramMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsHistogramMetadataMVName,
			SourceTableName: constants.MetricsHistogramTableName,
			TargetTableName: constants.MetricsHistogramMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
