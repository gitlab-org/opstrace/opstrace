package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderGaugeMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsGaugeMetadataMVName,
			SourceTableName: constants.MetricsGaugeTableName,
			TargetTableName: constants.MetricsGaugeMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
