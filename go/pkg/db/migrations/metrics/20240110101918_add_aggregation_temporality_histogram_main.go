package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addAggregationTemporalityHistogramMainTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS AggTemp Int32 CODEC(ZSTD(1)) AFTER Flags;
`

func renderAddAggregationTemporalityHistogramMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		addAggregationTemporalityHistogramMainTmpl,
		TemplateData{
			TableName: constants.MetricsHistogramTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
