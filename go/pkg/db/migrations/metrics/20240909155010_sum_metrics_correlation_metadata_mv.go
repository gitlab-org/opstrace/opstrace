package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderSumMetricsCorrelationMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsCorrelationMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsSumCorrelationMetadataMVName,
			SourceTableName: constants.MetricsSumTableName,
			TargetTableName: constants.MetricsSumCorrelationMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
