package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderExpHistogramMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsExponentialHistogramMetadataMVName,
			SourceTableName: constants.MetricsExponentialHistogramTableName,
			TargetTableName: constants.MetricsExponentialHistogramMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
