package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const expHistMainTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }} (
    ProjectId String CODEC(ZSTD(1)),
    NamespaceId Int64 CODEC(ZSTD(1)),
    Fingerprint String CODEC(ZSTD(1)),
    IngestionTimestamp DateTime64(9) CODEC(Delta, ZSTD(1)),
    ResourceAttributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    ResourceSchemaUrl String CODEC(ZSTD(1)),
    ScopeName String CODEC(ZSTD(1)),
    ScopeVersion String CODEC(ZSTD(1)),
    ScopeAttributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    ScopeDroppedAttrCount UInt32 CODEC(ZSTD(1)),
    ScopeSchemaUrl String CODEC(ZSTD(1)),
    MetricName String CODEC(ZSTD(1)),
    MetricDescription String CODEC(ZSTD(1)),
    MetricUnit String CODEC(ZSTD(1)),
    Attributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    StartTimeUnix DateTime64(9) CODEC(Delta, ZSTD(1)),
    TimeUnix DateTime64(9) CODEC(Delta, ZSTD(1)),
    Count UInt64 CODEC(Delta, ZSTD(1)),
    Sum Float64 CODEC(ZSTD(1)),
    Scale Int32 CODEC(ZSTD(1)),
    ZeroCount UInt64 CODEC(ZSTD(1)),
    PositiveOffset Int32 CODEC(ZSTD(1)),
    PositiveBucketCounts Array(UInt64) CODEC(ZSTD(1)),
    NegativeOffset Int32 CODEC(ZSTD(1)),
    NegativeBucketCounts Array(UInt64) CODEC(ZSTD(1)),
    Exemplars Nested (
        FilteredAttributes Map(LowCardinality(String), String),
        TimeUnix DateTime64(9),
        Value Float64,
        SpanId FixedString(8),
        TraceId FixedString(16)
    ) CODEC(ZSTD(1)),
    Flags UInt32  CODEC(ZSTD(1)),
    AggTemp Int32 CODEC(ZSTD(1)),
    Min Float64 CODEC(ZSTD(1)),
    Max Float64 CODEC(ZSTD(1)),
    INDEX idx_res_attr_key mapKeys(ResourceAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_res_attr_value mapValues(ResourceAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_scope_attr_key mapKeys(ScopeAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_scope_attr_value mapValues(ScopeAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_attr_key mapKeys(Attributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_attr_value mapValues(Attributes) TYPE bloom_filter(0.01) GRANULARITY 1
)
{{if .DevelopmentMode}}ENGINE = MergeTree{{else}}
ENGINE ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/metrics_main_exp_histogram', '{replica}'){{ end }}{{end}}
TTL toDateTime(IngestionTimestamp) + toIntervalDay(7)
PARTITION BY toDate(TimeUnix)
ORDER BY (ProjectId, MetricName, Fingerprint, toUnixTimestamp64Nano(TimeUnix))
SETTINGS index_granularity=8192, ttl_only_drop_parts = 1;
`

func renderExpHistMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	filename := ""
	// HACK: preserves cloud filename for backwards migrations compatibility
	if !selfHosted {
		filename = "20231120131524_exp_histogram_main.go"
	}

	return renderMigration(
		expHistMainTmpl,
		TemplateData{
			TableName: constants.MetricsExponentialHistogramTableName,
		},
		selfHosted,
		filename,
		devel,
	)
}
