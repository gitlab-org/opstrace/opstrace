package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderHistogramMetricsCorrelationMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsCorrelationMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsHistogramCorrelationMetadataMVName,
			SourceTableName: constants.MetricsHistogramTableName,
			TargetTableName: constants.MetricsHistogramCorrelationMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
