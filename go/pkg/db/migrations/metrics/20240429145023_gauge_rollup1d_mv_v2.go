package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const gaugeRollup1dMVTmplV2 = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
  ProjectId,
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  max(IngestionTimestamp) AS LastIngestedAt,
  toStartOfDay(TimeUnix) AS AggTimeUnix,
  sumState(Value) AS SumValueState,
  countState(Value) AS CountValueState,
  maxState(Value) AS MaxValueState,
  minState(Value) AS MinValueState,
  anyLastState(Value) AS LastValueState,
  maxMapState(Attributes) AS Attributes
FROM (
  SELECT
    ProjectId,
    MetricName,
    MetricDescription,
    Fingerprint,
    MetricUnit,
    IngestionTimestamp,
    TimeUnix,
    Value,
    Attributes
  FROM {{.DatabaseName}}.{{.SourceTableName}}
  ORDER BY ProjectId, Fingerprint, TimeUnix
)
GROUP BY ProjectId, Fingerprint, AggTimeUnix
ORDER BY ProjectId, Fingerprint, AggTimeUnix;
`

func renderGaugeRollup1dMVV2(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		gaugeRollup1dMVTmplV2,
		TemplateData{
			TableName:       constants.MetricsGauge1dMVNameV2,
			SourceTableName: constants.MetricsGaugeTableName,
			TargetTableName: constants.MetricsGauge1dTableNameV2,
		},
		selfHosted,
		"",
		devel,
	)
}
