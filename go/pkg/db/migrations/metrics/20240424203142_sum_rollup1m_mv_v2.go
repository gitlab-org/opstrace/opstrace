package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const sumRollup1mMVTmplV2 = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
  ProjectId,
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  Fingerprint,
  any(MetricUnit) AS MetricUnit,
  max(IngestionTimestamp) AS LastIngestedAt,
  toStartOfMinute(TimeUnix) AS AggTimeUnix,
  sumState(Value) AS SumValueState,
  countState(Value) AS CountValueState,
  maxState(Value) AS MaxValueState,
  minState(Value) AS MinValueState,
  maxMapState(Attributes) AS Attributes
FROM (
  SELECT
    ProjectId,
    MetricName,
    MetricDescription,
    Fingerprint,
    MetricUnit,
    IngestionTimestamp,
    TimeUnix,
    Value,
    Attributes
  FROM {{.DatabaseName}}.{{.SourceTableName}}
  ORDER BY ProjectId, Fingerprint, TimeUnix
)
GROUP BY ProjectId, Fingerprint, AggTimeUnix
ORDER BY ProjectId, Fingerprint, AggTimeUnix;
`

func renderSumRollup1mMVV2(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		sumRollup1mMVTmplV2,
		TemplateData{
			TableName:       constants.MetricsSum1mMVNameV2,
			SourceTableName: constants.MetricsSumTableName,
			TargetTableName: constants.MetricsSum1mTableNameV2,
		},
		selfHosted,
		"",
		devel,
	)
}
