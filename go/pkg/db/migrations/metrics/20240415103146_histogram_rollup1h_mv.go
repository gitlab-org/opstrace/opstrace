package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const histogramRollup1hMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
  any(ProjectId) AS ProjectId,
  any(MetricName) AS MetricName,
  any(MetricDescription) AS MetricDescription,
  any(MetricUnit) AS MetricUnit,
  Fingerprint,
  StartTimeUnix,
  AggTimeUnix,
  any(AggTemp) AS AggTemp,
  minState(Min) AS MinState,
  maxState(Max) AS MaxState,
  sumState(Count) AS CountState,
  sumState(Sum) AS SumState,
  any(Bounds) AS Bounds,
  sumState(SamplesCount) AS SamplesCountState,
  groupArrayState(BucketCountsAll[1]) AS BucketCountsMinState,
  groupArrayState(BucketCountsAll[-1]) AS BucketCountsMaxState,
  sumForEachState(BucketCountsSummed) AS BucketCountsSummedState
FROM
(
  SELECT
    any(ProjectId) AS ProjectId,
    any(MetricName) AS MetricName,
    any(MetricDescription) AS MetricDescription,
    any(MetricUnit) AS MetricUnit,
    Fingerprint,
    StartTimeUnix,
    toStartOfHour(TimeUnix) AS AggTimeUnix,
    any(AggTemp) AS AggTemp,
    max(Max) AS Max,
    min(Min) AS Min,
    sum(Count) AS Count,
    sum(Sum) AS Sum,
    any(arrayPushBack(ExplicitBounds, +inf)) AS Bounds,
    count() AS SamplesCount,
    arraySort(
      (x) -> (x.1),
      groupArray(
        (
          toDateTime64(TimeUnix, 9),
          BucketCounts
        )
      )
    ) AS BucketCountsAll,
    sumForEach(BucketCounts) AS BucketCountsSummed
  FROM {{.DatabaseName}}.{{.SourceTableName}}
  GROUP BY Fingerprint, StartTimeUnix, AggTimeUnix
  ORDER BY Fingerprint, StartTimeUnix, AggTimeUnix
)
GROUP BY Fingerprint, StartTimeUnix, AggTimeUnix
ORDER BY Fingerprint, StartTimeUnix, AggTimeUnix
`

func renderHistogramRollup1hMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		histogramRollup1hMVTmpl,
		TemplateData{
			TableName:       constants.MetricsHistogram1hMVName,
			SourceTableName: constants.MetricsHistogramTableName,
			TargetTableName: constants.MetricsHistogram1hTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
