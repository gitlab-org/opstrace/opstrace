package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderSumMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsSumMetadataMVName,
			SourceTableName: constants.MetricsSumTableName,
			TargetTableName: constants.MetricsSumMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
