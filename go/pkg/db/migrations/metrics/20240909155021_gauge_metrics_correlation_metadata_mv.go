package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderGaugeMetricsCorrelationMetadataMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsCorrelationMetadataMVTmpl,
		TemplateData{
			TableName:       constants.MetricsGaugeCorrelationMetadataMVName,
			SourceTableName: constants.MetricsGaugeTableName,
			TargetTableName: constants.MetricsGaugeCorrelationMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
