package metrics

import (
	"github.com/pressly/goose/v3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const histogramRollup1hMVTargetTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  ProjectId String CODEC(ZSTD(1)),
  MetricName String CODEC(ZSTD(1)),
  MetricDescription String CODEC(ZSTD(1)),
  MetricUnit String CODEC(ZSTD(1)),
  Fingerprint String CODEC(ZSTD(1)),
  AggTimeUnix DateTime64(9) CODEC(Delta, ZSTD(1)),
  StartTimeUnix DateTime64(9) CODEC(Delta, ZSTD(1)),
  AggTemp Int32 CODEC(ZSTD(1)),
  MinState AggregateFunction(min, Float64),
  MaxState AggregateFunction(max, Float64),
  CountState AggregateFunction(sum, UInt64),
  SumState AggregateFunction(sum, Float64),
  Bounds Array(Float64) CODEC(ZSTD(1)),
  SamplesCountState AggregateFunction(sum, UInt64),
  BucketCountsMinState AggregateFunction(groupArray, Tuple(DateTime64(9), Array(UInt64))),
  BucketCountsMaxState AggregateFunction(groupArray, Tuple(DateTime64(9), Array(UInt64))),
  BucketCountsSummedState AggregateFunction(sumForEach, Array(UInt64)),
)
{{if .DevelopmentMode}}ENGINE = AggregatingMergeTree{{else}}
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.TableName}}', '{replica}'){{ end }}{{end}}
ORDER BY (ProjectId, Fingerprint, AggTimeUnix, StartTimeUnix);
`

func renderHistogramRollup1hMVTarget(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		histogramRollup1hMVTargetTmpl,
		TemplateData{
			TableName: constants.MetricsHistogram1hTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
