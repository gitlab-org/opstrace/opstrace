package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderHistogramMetricsCorrelationMetadataMVTarget(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		metricsCorrelationMetadataMVTargetTmpl,
		TemplateData{
			TableName: constants.MetricsHistogramCorrelationMetadataTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
