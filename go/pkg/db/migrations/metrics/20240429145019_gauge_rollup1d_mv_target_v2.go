package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const gaugeRollup1dMVTargetTmplV2 = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  ProjectId String CODEC(ZSTD(1)),
  MetricName String CODEC(ZSTD(1)),
  MetricDescription String CODEC(ZSTD(1)),
  MetricUnit String CODEC(ZSTD(1)),
  Fingerprint String CODEC(ZSTD(1)),
  LastIngestedAt DateTime64(9) CODEC(Delta, ZSTD(1)),
  AggTimeUnix DateTime64(9) CODEC(Delta, ZSTD(1)),
  SumValueState AggregateFunction(sum, Float64),
  CountValueState AggregateFunction(count, Float64), 
  MaxValueState AggregateFunction(max, Float64),
  MinValueState AggregateFunction(min, Float64),
  LastValueState AggregateFunction(anyLast, Float64),
  Attributes AggregateFunction(maxMap, Map(String, String))
)
{{if .DevelopmentMode}}ENGINE = AggregatingMergeTree{{else}}
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/metrics_main_gauge_1d_v2', '{replica}'){{ end }}{{end}}
ORDER BY (ProjectId, Fingerprint, AggTimeUnix);
`

func renderGaugeRollup1dMVTargetV2(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		gaugeRollup1dMVTargetTmplV2,
		TemplateData{
			TableName: constants.MetricsGauge1dTableNameV2,
		},
		selfHosted,
		"",
		devel,
	)
}
