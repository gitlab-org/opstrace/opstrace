package metrics

//nolint:lll
const metricsMetadataMVTargetTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  ProjectId String CODEC(ZSTD(1)),
  MetricName String CODEC(ZSTD(1)),
  MetricDescription String CODEC(ZSTD(1)),
  AttributeKeysState AggregateFunction(groupUniqArrayArray, Array(String)),
  LastIngestedAtState AggregateFunction(max, DateTime64(9))
)
{{if .DevelopmentMode}}ENGINE = AggregatingMergeTree{{else}}
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.TableName}}', '{replica}'){{ end }}{{end}}
ORDER BY (ProjectId, MetricName);
`

//nolint:lll
const metricsMetadataMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
  ProjectId,
  MetricName,
  any(MetricDescription) AS MetricDescription,
  groupUniqArrayArrayState(mapKeys(Attributes)) AS AttributeKeysState,
  maxState(IngestionTimestamp) AS LastIngestedAtState
FROM {{.DatabaseName}}.{{.SourceTableName}}
GROUP BY ProjectId, MetricName
ORDER BY ProjectId, MetricName;
`

//nolint:lll
const metricsCorrelationMetadataMVTargetTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
  TenantId String CODEC(ZSTD(1)),
  ProjectId String CODEC(ZSTD(1)),
  MetricName String CODEC(ZSTD(1)),
  ExemplarTraceId FixedString(16),
  LatestDatapointTimeUnix SimpleAggregateFunction(max, DateTime64(9)),
)
{{if .DevelopmentMode}}ENGINE = AggregatingMergeTree{{else}}
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.TableName}}', '{replica}'){{ end }}{{end}}
TTL toDateTime(LatestDatapointTimeUnix) + toIntervalDay(7)
ORDER BY (TenantId, ProjectId, MetricName, ExemplarTraceId);
`

//nolint:lll
const metricsCorrelationMetadataMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
  TenantId,
  ProjectId,
  MetricName,
  arrayJoin(Exemplars.TraceId) AS ExemplarTraceId,
  max(TimeUnix) AS LatestDatapointTimeUnix
FROM {{.DatabaseName}}.{{.SourceTableName}}
WHERE notEmpty(Exemplars.TraceId)
GROUP BY TenantId, ProjectId, MetricName, ExemplarTraceId
ORDER BY TenantId, ProjectId, MetricName, ExemplarTraceId;
`

const addTenantIDTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS TenantId String CODEC(ZSTD(1)) AFTER NamespaceId;
`
