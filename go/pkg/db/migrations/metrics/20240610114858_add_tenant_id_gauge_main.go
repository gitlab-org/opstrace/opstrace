package metrics

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

func renderAddTenantIDGaugeMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		addTenantIDTmpl,
		TemplateData{
			TableName: constants.MetricsGaugeTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
