package metrics

import (
	"runtime"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

type TemplateData struct {
	migrations.TemplateData
	TableName       string
	TargetTableName string
	SourceTableName string
}

// SetupMigrations renders and registers migrations for metrics tables
func SetupMigrations(selfHostedVersion, developmentMode bool) ([]*goose.Migration, error) {
	return migrations.RenderFuncs{
		// main tables
		renderGaugeMain,
		renderSumMain,
		renderHistMain,
		renderExpHistMain,
		renderAddAggregationTemporalityHistogramMain,
		renderAddAggregationTemporalityExpHistogramMain,
		// metadata MVs
		renderSumMetadataMVTarget,
		renderSumMetadataMV,
		renderGaugeMetadataMVTarget,
		renderGaugeMetadataMV,
		renderHistogramMetadataMVTarget,
		renderHistogramMetadataMV,
		renderExpHistogramMetadataMVTarget,
		renderExpHistogramMetadataMV,
		// metrics correlation metadata MVs
		renderSumMetricsCorrelationMetadataMVTarget,
		renderSumMetricsCorrelationMetadataMV,
		renderGaugeMetricsCorrelationMetadataMVTarget,
		renderGaugeMetricsCorrelationMetadataMV,
		renderHistogramMetricsCorrelationMetadataMVTarget,
		renderHistogramMetricsCorrelationMetadataMV,
		renderExpHistogramMetricsCorrelationMetadataMVTarget,
		renderExpHistogramMetricsCorrelationMetadataMV,
		// gauge rollups
		renderGaugeRollup1mMVV2,
		renderGaugeRollup1mMVTargetV2,
		renderGaugeRollup1hMVV2,
		renderGaugeRollup1hMVTargetV2,
		renderGaugeRollup1dMVV2,
		renderGaugeRollup1dMVTargetV2,
		// sum rollups
		renderSumRollup1mMVTargetV2,
		renderSumRollup1mMVV2,
		renderSumRollup1hMVTargetV2,
		renderSumRollup1hMVV2,
		renderSumRollup1dMVTargetV2,
		renderSumRollup1dMVV2,
		// histogram rollups
		renderHistogramRollup1mMVTarget,
		renderHistogramRollup1mMV,
		renderHistogramRollup1hMVTarget,
		renderHistogramRollup1hMV,
		renderHistogramRollup1dMVTarget,
		renderHistogramRollup1dMV,
		// add tenant IDs
		renderAddTenantIDSumMain,
		renderAddTenantIDGaugeMain,
		renderAddTenantIDHistogramMain,
		renderAddTenantIDExpHistogramMain,
	}.Render(selfHostedVersion, developmentMode)
}

func renderMigration(
	tmpl string, data TemplateData,
	selfHosted bool, filenameOverride string,
	developmentMode bool,
) (*goose.Migration, error) {
	filename := filenameOverride
	if filename == "" {
		_, filename, _, _ = runtime.Caller(1)
	}

	data.TemplateData = migrations.TemplateData{
		DatabaseName:      constants.MetricsDatabaseName,
		SelfHostedVersion: selfHosted,
		DevelopmentMode:   developmentMode,
	}

	return migrations.NewMigration(tmpl, data, filename)
}
