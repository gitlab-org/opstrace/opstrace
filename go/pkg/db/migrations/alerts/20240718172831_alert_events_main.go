package alerts

import (
	"github.com/pressly/goose/v3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const alertEventsMainTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }} (
  IsSaaS UInt8 CODEC(ZSTD(1)),
  TenantId String CODEC(ZSTD(1)),
  ProjectId String CODEC(ZSTD(1)),
  Timestamp DateTime64(9) CODEC(Delta(8), ZSTD(1)),
  AlertType String CODEC(ZSTD(1)),
  Description String CODEC(ZSTD(1)),
  Reason String CODEC(ZSTD(1))
)
{{if .DevelopmentMode}}ENGINE = MergeTree{{else}}
ENGINE = ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
PARTITION BY toYYYYMM(Timestamp)
ORDER BY (IsSaaS, TenantId, ProjectId, Timestamp, AlertType)
TTL toDateTime(Timestamp) + toIntervalDay(30)
SETTINGS index_granularity = 8192, ttl_only_drop_parts = 1;
`

func renderAlertEvents(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(alertEventsMainTmpl, TemplateData{
		TableName: constants.AlertEventsTableName,
	}, selfHosted, devel)
}
