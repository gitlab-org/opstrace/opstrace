package alerts

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const alertMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}}
AS
SELECT
 IsSaaS,
 TenantId,
 ProjectId,
 toStartOfMinute(Timestamp) AS AggTimestamp,
 AlertType,
 argMinState(Description, Timestamp) AS Description,
 argMinState(Reason, Timestamp) AS Reason,
 count() AS EventCount
 FROM {{.DatabaseName}}.{{.SourceTableName}}
 GROUP BY TenantId, ProjectId, AlertType, IsSaaS, AggTimestamp
 ORDER BY AggTimestamp;
`

func renderAlertMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(alertMVTmpl, TemplateData{
		TableName:       constants.AlertsMVTableName,
		TargetTableName: constants.AlertsTableName,
		SourceTableName: constants.AlertEventsTableName,
	}, selfHosted, devel)
}
