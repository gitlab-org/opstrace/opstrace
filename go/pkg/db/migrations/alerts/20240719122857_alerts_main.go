package alerts

import (
	"github.com/pressly/goose/v3"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const alertsMainTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }} (
  IsSaaS UInt8 CODEC(ZSTD(1)),
  TenantId String CODEC(ZSTD(1)),
  ProjectId String CODEC(ZSTD(1)),
  AggTimestamp DateTime64(9) CODEC(Delta(8), ZSTD(1)),
  AlertType String CODEC(ZSTD(1)),
  Description AggregateFunction(argMin, String, DateTime64(9)) CODEC(ZSTD(1)),
  Reason AggregateFunction(argMin, String, DateTime64(9)) CODEC(ZSTD(1)),
  EventCount UInt64 CODEC(ZSTD(1))
)
{{if .DevelopmentMode}}ENGINE = SummingMergeTree{{else}}
ENGINE = ReplicatedSummingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
PARTITION BY toYYYYMM(AggTimestamp)
ORDER BY (IsSaaS, TenantId, ProjectId, AggTimestamp, AlertType)
TTL toDateTime(AggTimestamp) + toIntervalDay(30)
SETTINGS index_granularity = 8192, ttl_only_drop_parts = 1;
`

func renderAlerts(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(alertsMainTmpl, TemplateData{
		TableName: constants.AlertsTableName,
	}, selfHosted, devel)
}
