package alerts

import (
	"runtime"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

type TemplateData struct {
	migrations.TemplateData
	TableName       string
	TargetTableName string
	SourceTableName string
}

func renderMigration(tmpl string, data TemplateData, selfHosted, developmentMode bool) (*goose.Migration, error) {
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(1)

	data.TemplateData = migrations.TemplateData{
		DatabaseName:      constants.AlertsDatabaseName,
		SelfHostedVersion: selfHosted,
		DevelopmentMode:   developmentMode,
	}

	return migrations.NewMigration(tmpl, data, filename)
}

func SetupMigrations(selfHostedVersion bool, developmentMode bool) ([]*goose.Migration, error) {
	steps := migrations.RenderFuncs{
		renderAlertEvents,
		renderAlerts,
		renderAlertMV,
	}

	return steps.Render(selfHostedVersion, developmentMode)
}
