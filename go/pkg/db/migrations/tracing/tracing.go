package tracing

import (
	"runtime"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

// TemplateData defines all data we need to render tracing-specific
// migrations.
type TemplateData struct {
	migrations.TemplateData
	TableName       string
	DistTableName   string
	TargetTableName string
	SourceTableName string
}

// SetupMigrations renders and registers migrations for tracing tables
func SetupMigrations(selfHostedVersion bool, developmentMode bool) ([]*goose.Migration, error) {
	steps := migrations.RenderFuncs{
		renderTracesMain,
		renderTracesMVTargetLocal,
		renderTracesMV,
		renderAddNamespaceID,
		renderTraceSummary,
		renderTracesSummaryMV,
		renderTracesRED1hTargetV2,
		renderTracesRED1hMVV2,
		renderTraceIDTimestampRange,
		renderTraceIDsTimestampRangeMV,
		renderAddTenantIDTracesMain,
	}

	// backwards compatible self-hosted migrations for Distributed tables
	if selfHostedVersion {
		steps = append(steps,
			renderTracesMainDist,
			renderTracesMVTargetDist,
			renderAddNamespaceIDDist,
			renderAddTenantIDTracesDist,
		)
	}

	return steps.Render(selfHostedVersion, developmentMode)
}

func renderMigration(
	tmpl string,
	data TemplateData,
	selfHosted bool,
	filenameOverride string,
	developmentMode bool,
) (*goose.Migration, error) {
	filename := filenameOverride
	if filename == "" {
		_, filename, _, _ = runtime.Caller(1)
	}

	data.TemplateData = migrations.TemplateData{
		DatabaseName:      constants.TracingDatabaseName,
		SelfHostedVersion: selfHosted,
		DevelopmentMode:   developmentMode,
	}

	return migrations.NewMigration(tmpl, data, filename)
}
