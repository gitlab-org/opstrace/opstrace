package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const tracesMainDistributedTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.DistTableName}}
ON CLUSTER '{cluster}' AS {{.DatabaseName}}.{{.TableName}}
ENGINE = Distributed('{cluster}', {{.DatabaseName}}, {{.TableName}}, cityHash64(ProjectId, TraceId));
`

func renderTracesMainDist(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(tracesMainDistributedTmpl, TemplateData{
		TableName:     constants.TracingTableName,
		DistTableName: constants.TracingDistTableName,
	}, selfHosted, "", devel)
}
