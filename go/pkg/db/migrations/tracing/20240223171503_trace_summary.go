package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const traceSummaryTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
    ProjectId        String CODEC(ZSTD(1)),
    TraceId          FixedString(16) CODEC(ZSTD(1)),
    Start            SimpleAggregateFunction(min, DateTime64(9)) CODEC(Delta(8), ZSTD(1)),
    End              SimpleAggregateFunction(max, DateTime64(9)) CODEC(Delta(8), ZSTD(1)),
    Duration         SimpleAggregateFunction(max, Int64) CODEC(ZSTD(1)),
    SpanCount        UInt64 CODEC(ZSTD(1)),
    ErrorCount       UInt64 CODEC(ZSTD(1)),
    RootSpanId       AggregateFunction(anyIf, FixedString(8), UInt8) CODEC(ZSTD(1)),
    RootServiceName  AggregateFunction(anyIf, LowCardinality(String), UInt8) CODEC(ZSTD(1)),
    RootSpanName     AggregateFunction(anyIf, LowCardinality(String), UInt8) CODEC(ZSTD(1)),
    FirstSpanId      AggregateFunction(argMin, FixedString(8), DateTime64(9)) CODEC(ZSTD(1)),
    FirstServiceName AggregateFunction(argMin, LowCardinality(String), DateTime64(9)) CODEC(ZSTD(1)),
    FirstSpanName    AggregateFunction(argMin, LowCardinality(String), DateTime64(9)) CODEC(ZSTD(1)),
    ServiceNames     SimpleAggregateFunction(groupUniqArrayArray, Array(String)) CODEC(ZSTD(1)),
    INDEX idx_trace_id TraceId TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_duration Duration TYPE minmax GRANULARITY 1,
    INDEX idx_service_names ServiceNames TYPE bloom_filter(0.01) GRANULARITY 1
)
{{if .DevelopmentMode}}ENGINE = SummingMergeTree{{else}}
ENGINE = ReplicatedSummingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{end}}{{end}}
PARTITION BY toDate(Start)
ORDER BY (ProjectId, toUnixTimestamp(Start), TraceId)
TTL toDateTime(Start) + toIntervalDay(30);
`

func renderTraceSummary(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(traceSummaryTmpl, TemplateData{
		TableName: constants.TracingSummaryTableName,
	}, selfHosted, "", devel)
}
