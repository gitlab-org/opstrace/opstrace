package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const tracesRED1hMVTmplV2 = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
WITH CTE AS
    (
        SELECT
            ProjectId,
            1 AS TotalCount,
            min(Start) AS TraceStart,
            CAST(max(Duration), 'Float64') AS TraceDuration,
            groupUniqArrayArray(ServiceNames) AS ServiceNames,
            countIf(ErrorCount > 0) AS ErrCount
        FROM {{.DatabaseName}}.{{.SourceTableName}}
        GROUP BY
            ProjectId,
            TraceId
    )
SELECT
    ProjectId,
    sum(TotalCount) AS TraceCount,
    sumIf(1, ErrCount > 0) AS ErrCount,
    quantilesState(0.95,0.90,0.75,0.5)(TraceDuration) AS DurationQuantiles,
    groupUniqArrayArray(ServiceNames) AS ServiceNames,
    toStartOfHour(TraceStart) AS AggTime
FROM CTE
GROUP BY
    ProjectId,
    AggTime
ORDER BY
    ProjectId ASC,
    AggTime ASC
`

func renderTracesRED1hMVV2(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(tracesRED1hMVTmplV2, TemplateData{
		TableName:       constants.TracingRED1hMVName,
		TargetTableName: constants.TracingRED1hMVTableName,
		SourceTableName: constants.TracingSummaryTableName,
	}, selfHosted, "", devel)
}
