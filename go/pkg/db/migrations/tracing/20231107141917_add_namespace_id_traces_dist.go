package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const AddNamespaceIDDistTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.DistTableName}} ON CLUSTER '{cluster}'
ADD COLUMN IF NOT EXISTS NamespaceId Int64 CODEC(ZSTD(1)) AFTER ProjectId;
`

func renderAddNamespaceIDDist(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(AddNamespaceIDDistTmpl, TemplateData{
		DistTableName: constants.TracingDistTableName,
	}, selfHosted, "", devel)
}
