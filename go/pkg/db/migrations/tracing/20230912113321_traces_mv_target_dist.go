package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const tracesMVTargetDistributedTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.DistTableName}}
ON CLUSTER '{cluster}' AS {{.DatabaseName}}.{{.TableName}}
ENGINE = Distributed('{cluster}', {{.DatabaseName}}, {{.TableName}}, cityHash64(ProjectId, TraceId));
`

func renderTracesMVTargetDist(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(tracesMVTargetDistributedTmpl, TemplateData{
		TableName:     constants.TracingMVTargetTableName,
		DistTableName: constants.TracingMVTargetDistTableName,
	}, selfHosted, "", devel)
}
