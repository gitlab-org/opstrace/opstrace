package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const tracesMVTargetTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
	ProjectId String CODEC(ZSTD(1)),
	TraceId FixedString(16) CODEC(ZSTD(1)),
	SpanId FixedString(8) CODEC(ZSTD(1)),
	Start DateTime64(9) CODEC(Delta(8), ZSTD(1)),
	ServiceName String CODEC(ZSTD(1)),
	SpanName String CODEC(ZSTD(1)),
	StatusCode LowCardinality(String) CODEC(ZSTD(1)),
	ParentSpanId FixedString(8) CODEC(ZSTD(1)),
	Duration Int64 CODEC(ZSTD(1))
)
{{if .DevelopmentMode}}ENGINE = MergeTree{{else}}
ENGINE = ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{end}}{{end}}
PARTITION BY toDate(Start)
ORDER BY (ProjectId, TraceId, toUnixTimestamp(Start))
TTL toDateTime(Start) + toIntervalDay(30);
`

func renderTracesMVTargetLocal(selfHosted bool, devel bool) (*goose.Migration, error) {
	filename := ""
	tableName := constants.TracingMVTargetTableName
	if !selfHosted {
		// HACK: preserves cloud filename for backwards migrations compatibility
		filename = "20231004130953_traces_mv_target.go"
		tableName = constants.TracingMVTargetDistTableName
	}
	return renderMigration(tracesMVTargetTmpl, TemplateData{
		TableName: tableName,
	}, selfHosted, filename, devel)
}
