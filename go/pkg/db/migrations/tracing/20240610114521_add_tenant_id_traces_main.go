package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addTenantIDTracesMainTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS TenantId String CODEC(ZSTD(1)) AFTER NamespaceId;
`

func renderAddTenantIDTracesMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	tablename := constants.TracingDistTableName
	if selfHosted {
		tablename = constants.TracingTableName
	}

	return renderMigration(addTenantIDTracesMainTmpl, TemplateData{
		TableName: tablename,
	}, selfHosted, "", devel)
}
