package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addNamespaceIDTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS NamespaceId Int64 CODEC(ZSTD(1)) AFTER ProjectId;
`

func renderAddNamespaceID(selfHosted bool, devel bool) (*goose.Migration, error) {
	filename := ""
	tablename := constants.TracingTableName
	// HACK: preserves cloud filename for backwards migrations compatibility
	if !selfHosted {
		filename = "20231106121910_add_namespace_id_traces_main.go"
		tablename = constants.TracingDistTableName
	}

	return renderMigration(addNamespaceIDTmpl, TemplateData{
		TableName: tablename,
	}, selfHosted, filename, devel)
}
