package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const traceIDtsMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}}
AS
WITH timestamp_add(Timestamp, INTERVAL {{.DatabaseName}}.{{.SourceTableName}}.Duration NANOSECOND) as end_timestamp
SELECT
    ProjectId,
    TraceId,
    min(Timestamp) as Start,
    max(end_timestamp) as End
FROM {{.DatabaseName}}.{{.SourceTableName}}
WHERE TraceId != ''
GROUP BY ProjectId, TraceId, toUnixTimestamp(Timestamp)
ORDER BY toUnixTimestamp(Timestamp);
`

func renderTraceIDsTimestampRangeMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(traceIDtsMVTmpl, TemplateData{
		TableName:       constants.TracingIDTimestampMVTableName,
		TargetTableName: constants.TracingIDTimestampTableName,
		SourceTableName: constants.TracingDistTableName,
	}, selfHosted, "", devel)
}
