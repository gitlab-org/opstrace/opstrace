package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addTenantIDTracesDistTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.DistTableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS TenantId String CODEC(ZSTD(1)) AFTER NamespaceId;
`

func renderAddTenantIDTracesDist(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(addTenantIDTracesDistTmpl, TemplateData{
		DistTableName: constants.TracingDistTableName,
	}, selfHosted, "", devel)
}
