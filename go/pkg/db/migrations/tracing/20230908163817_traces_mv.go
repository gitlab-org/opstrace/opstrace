package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const tracesMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}} AS
SELECT
    ProjectId,
    TraceId,
    SpanId,
    min(Timestamp) AS Start,
    any(ServiceName) AS ServiceName,
    any(SpanName) AS SpanName,
    any(StatusCode) AS StatusCode,
    any(Duration) AS Duration,
    any(ParentSpanId) AS ParentSpanId
FROM {{.DatabaseName}}.{{.SourceTableName}}
WHERE TraceId != ''
GROUP BY
    ProjectId,
    TraceId,
    SpanId
ORDER BY Start;
`

func renderTracesMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	filename := ""
	targetTable := constants.TracingMVTargetTableName
	sourceTable := constants.TracingTableName
	if !selfHosted {
		filename = "20231004131042_traces_mv.go"
		targetTable = constants.TracingMVTargetDistTableName
		sourceTable = constants.TracingDistTableName
	}

	return renderMigration(tracesMVTmpl, TemplateData{
		TableName:       constants.TracingMVName,
		TargetTableName: targetTable,
		SourceTableName: sourceTable,
	}, selfHosted, filename, devel)
}
