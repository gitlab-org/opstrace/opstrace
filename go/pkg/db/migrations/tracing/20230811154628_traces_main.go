package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const tracesMainTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }} (
    ProjectId String CODEC(ZSTD(1)),
    NamespaceId Int64 CODEC(ZSTD(1)),
    Timestamp DateTime64(9) CODEC(Delta, ZSTD(1)),
    TraceId FixedString(16) CODEC(ZSTD(1)),
    SpanId FixedString(8) CODEC(ZSTD(1)),
    ParentSpanId FixedString(8) CODEC(ZSTD(1)),
    TraceState String CODEC(ZSTD(1)),
    SpanName LowCardinality(String) CODEC(ZSTD(1)),
    SpanKind LowCardinality(String) CODEC(ZSTD(1)),
    ServiceName LowCardinality(String) CODEC(ZSTD(1)),
    ResourceAttributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    ScopeName String CODEC(ZSTD(1)),
    ScopeVersion String CODEC(ZSTD(1)),
    SpanAttributes Map(LowCardinality(String), String) CODEC(ZSTD(1)),
    Duration Int64 CODEC(ZSTD(1)),
    StatusCode LowCardinality(String) CODEC(ZSTD(1)),
    StatusMessage String CODEC(ZSTD(1)),
    Events Nested (
        Timestamp DateTime64(9),
        Name LowCardinality(String),
        Attributes Map(LowCardinality(String), String)
    ) CODEC(ZSTD(1)),
    Links Nested (
        TraceId FixedString(16),
        SpanId FixedString(8),
        TraceState String,
        Attributes Map(LowCardinality(String), String)
    ) CODEC(ZSTD(1)),
    INDEX idx_trace_id TraceId TYPE bloom_filter(0.001) GRANULARITY 1,
    INDEX idx_res_attr_key mapKeys(ResourceAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_res_attr_value mapValues(ResourceAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_span_attr_key mapKeys(SpanAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_span_attr_value mapValues(SpanAttributes) TYPE bloom_filter(0.01) GRANULARITY 1,
    INDEX idx_duration Duration TYPE minmax GRANULARITY 1
)
{{if .DevelopmentMode}}ENGINE = MergeTree{{else}}
ENGINE ReplicatedMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{ end }}{{end}}
TTL toDateTime(Timestamp) + toIntervalDay(30)
PARTITION BY toDate(Timestamp)
ORDER BY (ProjectId, ServiceName, SpanName, toUnixTimestamp(Timestamp), TraceId)
SETTINGS index_granularity=8192, ttl_only_drop_parts = 1;
`

func renderTracesMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	filename := ""
	tableName := constants.TracingTableName

	if !selfHosted {
		tableName = constants.TracingDistTableName
		// HACK: preserves cloud filename for backwards migrations compatibility
		filename = "20231004130944_traces_main.go"
	}

	return renderMigration(tracesMainTmpl, TemplateData{
		TableName: tableName,
	}, selfHosted, filename, devel)
}
