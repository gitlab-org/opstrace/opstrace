package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const traceSummaryMVTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
TO {{.DatabaseName}}.{{.TargetTableName}}
AS
WITH timestamp_add(Timestamp, INTERVAL {{.DatabaseName}}.{{.SourceTableName}}.Duration NANOSECOND) as end_timestamp
SELECT
    ProjectId,
    TraceId,
    min(Timestamp) as Start,
    max(end_timestamp) as End,
    max(toUnixTimestamp64Nano(end_timestamp) - toUnixTimestamp64Nano(Timestamp)) as Duration,
    count() as SpanCount,
    countIf(StatusCode = 'STATUS_CODE_ERROR') as ErrorCount,
    anyIfState(SpanId, ParentSpanId = '') as RootSpanId,
    anyIfState(ServiceName, ParentSpanId = '') as RootServiceName,
    anyIfState(SpanName, ParentSpanId = '') as RootSpanName,
    argMinState(SpanId, Timestamp) as FirstSpanId,
    argMinState(SpanName, Timestamp) as FirstSpanName,
    argMinState(ServiceName, Timestamp) as FirstServiceName,
    groupUniqArray(ServiceName) as ServiceNames
FROM {{.DatabaseName}}.{{.SourceTableName}}
WHERE TraceId != ''
GROUP BY ProjectId, toUnixTimestamp(Timestamp), TraceId
ORDER BY toUnixTimestamp(Timestamp);
`

func renderTracesSummaryMV(selfHosted bool, devel bool) (*goose.Migration, error) {
	sourcetable := constants.TracingDistTableName
	if selfHosted {
		sourcetable = constants.TracingTableName
	}

	return renderMigration(traceSummaryMVTmpl, TemplateData{
		TableName:       constants.TracingSummaryMVName,
		TargetTableName: constants.TracingSummaryTableName,
		SourceTableName: sourcetable,
	}, selfHosted, "", devel)
}
