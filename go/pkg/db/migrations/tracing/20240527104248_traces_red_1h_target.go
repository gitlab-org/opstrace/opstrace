package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const tracesRED1hTargetTmplV2 = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
    ProjectId   String CODEC(ZSTD(1)),
    AggTime DateTime CODEC(Delta(8), ZSTD(1)),

    ServiceNames SimpleAggregateFunction(groupUniqArrayArray, Array(String)) CODEC(ZSTD(1)),

    TraceCount  Int64 CODEC(ZSTD(1)),
    ErrCount Int64 CODEC(ZSTD(1)),

    DurationQuantiles AggregateFunction(quantiles(0.95,0.90,0.75,0.5), Float64) CODEC(ZSTD(1)),

    INDEX idx_service_names ServiceNames TYPE bloom_filter(0.01) GRANULARITY 1
)
{{if .DevelopmentMode}}ENGINE = SummingMergeTree{{else}}
ENGINE = ReplicatedSummingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{end}}{{end}}
ORDER BY (ProjectId, AggTime)
PARTITION BY toYYYYMM(AggTime);
`

func renderTracesRED1hTargetV2(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		tracesRED1hTargetTmplV2,
		TemplateData{
			TableName: constants.TracingRED1hMVTableName,
		},
		selfHosted,
		"",
		devel,
	)
}
