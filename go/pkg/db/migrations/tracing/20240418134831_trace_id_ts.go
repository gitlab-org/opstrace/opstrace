package tracing

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const traceIDtsTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{end}}
(
    ProjectId        String CODEC(ZSTD(1)),
    TraceId          FixedString(16) CODEC(ZSTD(1)),
    Start            SimpleAggregateFunction(min, DateTime64(9)) CODEC(Delta(8), ZSTD(1)),
    End              SimpleAggregateFunction(max, DateTime64(9)) CODEC(Delta(8), ZSTD(1)),
    INDEX idx_trace_id TraceId TYPE bloom_filter(0.01) GRANULARITY 1
)
{{if .DevelopmentMode}}ENGINE = AggregatingMergeTree{{else}}
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.DatabaseName}}.{{.TableName}}', '{replica}'){{end}}{{end}}
PARTITION BY toDate(Start)
ORDER BY (ProjectId, TraceId, toUnixTimestamp(Start))
TTL toDateTime(Start) + toIntervalDay(30)
SETTINGS index_granularity=8192;
`

func renderTraceIDTimestampRange(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(traceIDtsTmpl, TemplateData{
		TableName: constants.TracingIDTimestampTableName,
	}, selfHosted, "", devel)
}
