package test

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/analytics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/logging"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/snowplow"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/store"
)

func RunGooseMigrations(ctx context.Context, dsn string, migrations []*goose.Migration) error {
	db, err := sql.Open("clickhouse", dsn)
	if err != nil {
		return fmt.Errorf("sql open: %w", err)
	}

	if err := db.Ping(); err != nil {
		return fmt.Errorf("db ping: %w", err)
	}

	defer func() {
		_ = db.Close()
	}()

	s := &store.ClickHouseStore{
		TName: "goose_db_version",
	}

	s.AttachOptions(map[string]string{
		"ON_CLUSTER": "true",
	})

	provider, err := goose.NewProvider(
		"",
		db,
		nil,
		goose.WithGoMigrations(migrations...),
		goose.WithAllowOutofOrder(true),
		goose.WithStore(s),
	)
	if err != nil {
		return fmt.Errorf("new provider: %w", err)
	}

	if _, err := provider.Up(ctx); err != nil {
		return fmt.Errorf("provider up: %w", err)
	}
	return nil
}

func RunErrorTrackingMigrations(ctx context.Context, dsn string) error {
	ms, err := errortracking.SetupMigrations(true, false)
	if err != nil {
		return fmt.Errorf("errortracking migrations: %w", err)
	}

	return RunGooseMigrations(ctx, dsn, ms)
}

func RunLoggingMigrations(ctx context.Context, dsn string) error {
	ms, err := logging.SetupMigrations(true, false)
	if err != nil {
		return fmt.Errorf("logging migrations: %w", err)
	}

	return RunGooseMigrations(ctx, dsn, ms)
}

func RunTracingMigrations(ctx context.Context, dsn string) error {
	ms, err := tracing.SetupMigrations(true, false)
	if err != nil {
		return fmt.Errorf("tracing migrations: %w", err)
	}

	return RunGooseMigrations(ctx, dsn, ms)
}

func RunMetricsMigrations(ctx context.Context, dsn string) error {
	ms, err := metrics.SetupMigrations(true, false)
	if err != nil {
		return fmt.Errorf("metrics migrations: %w", err)
	}

	return RunGooseMigrations(ctx, dsn, ms)
}

func RunAnalyticsMigrations(ctx context.Context, dsn string) error {
	ms, err := analytics.SetupMigrations(true, false)
	if err != nil {
		return fmt.Errorf("analytics migrations: %w", err)
	}

	return RunGooseMigrations(ctx, dsn, ms)
}

func RunAlertsMigrations(ctx context.Context, dsn string) error {
	ms, err := alerts.SetupMigrations(true, false)
	if err != nil {
		return fmt.Errorf("alerts migrations: %w", err)
	}
	return RunGooseMigrations(ctx, dsn, ms)
}

func RunSnowplowMigrations(ctx context.Context, dsn string) error {
	ms, err := snowplow.SetupMigrations(true, false)
	if err != nil {
		return fmt.Errorf("snowplow migrations: %w", err)
	}
	return RunGooseMigrations(ctx, dsn, ms)
}
