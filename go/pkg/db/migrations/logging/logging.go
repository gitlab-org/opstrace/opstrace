package logging

import (
	"runtime"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations"
)

// TemplateData defines all data we need to render logging-specific
// migrations.
type TemplateData struct {
	migrations.TemplateData
	TableName       string
	SourceTableName string
	TargetTableName string
	// TTLDays used to allow flexible TTL settings
	TTLDays string
	// Interval used in analytics MVs
	Interval string
}

func SetupMigrations(selfHostedVersion, developmentMode bool) ([]*goose.Migration, error) {
	steps := migrations.RenderFuncs{
		renderLogsMain,
		renderAddNamespaceIDLogsMain,
		renderAddFingerprintIdxIDLogsMain,
		renderAddSeverityNumberIdxIDLogsMain,
		renderAddTenantIDLogsMain,
	}

	qanalyticsQueries, err := renderQAnaliticsQueries(selfHostedVersion, developmentMode)
	if err != nil {
		return nil, err
	}

	ms, err := steps.Render(selfHostedVersion, developmentMode)
	if err != nil {
		return nil, err
	}

	return append(ms, qanalyticsQueries...), nil
}

func renderMigration(
	tmpl string, data TemplateData, selfHosted, developmentMode bool) (*goose.Migration, error) {
	//nolint:dogsled
	_, filename, _, _ := runtime.Caller(1)

	data.TemplateData = migrations.TemplateData{
		DatabaseName:      constants.LoggingDatabaseName,
		SelfHostedVersion: selfHosted,
		DevelopmentMode:   developmentMode,
	}

	return migrations.NewMigration(tmpl, data, filename)
}
