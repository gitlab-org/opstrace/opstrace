package logging

import (
	"fmt"
	"strconv"

	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

//nolint:lll
const (
	qAnaliticsMvTargetTmpl = `
CREATE TABLE IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
(
    ProjectId String CODEC(ZSTD(1)),
    NamespaceId Int64 CODEC(ZSTD(1)),
    Timestamp DateTime64(9) CODEC(Delta(8), ZSTD(1)),
    ObservedTimestamp  SimpleAggregateFunction(max, DateTime64(9)) CODEC(Delta(8), ZSTD(1)),
    TraceFlags UInt32 CODEC(ZSTD(1)),
    SeverityNumber UInt8 CODEC(ZSTD(1)),
    ServiceName String CODEC(ZSTD(1)),
    SumCount AggregateFunction(sum, UInt64),
    INDEX idx_severity_number SeverityNumber TYPE set(25) GRANULARITY 1,
    INDEX idx_trace_flags TraceFlags TYPE set(2) GRANULARITY 1
)
{{if .DevelopmentMode}}ENGINE = AggregatingMergeTree{{else}}
ENGINE = ReplicatedAggregatingMergeTree{{if .SelfHostedVersion}}('/clickhouse/{cluster}/tables/{shard}/{{.TableName}}', '{replica}'){{ end }}{{end}}
PARTITION BY toDate(Timestamp)
ORDER BY (ProjectId, ServiceName, SeverityNumber, toUnixTimestamp(Timestamp), TraceFlags)
TTL toDateTime(ObservedTimestamp) + toIntervalDay({{.TTLDays}})
SETTINGS index_granularity = 8192, ttl_only_drop_parts = 1`

	qAnaliticsMvTmpl = `
CREATE MATERIALIZED VIEW IF NOT EXISTS {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
TO {{.DatabaseName}}.{{.TargetTableName}}
AS
SELECT
    ProjectId,
    NamespaceId,
    Timestamp,
    ObservedTimestamp,
    TraceFlags,
    SeverityNumber,
    ServiceName,
    sumState(SumCount) AS SumCount
FROM
(
  SELECT
    ProjectId,
    NamespaceId,
    toStartOfInterval(Timestamp, INTERVAL {{ .Interval }}) AS Timestamp,
    max(ObservedTimestamp) as ObservedTimestamp,
    TraceFlags,
    SeverityNumber,
    ServiceName,
    count(*) AS SumCount
  FROM {{.DatabaseName}}.{{.SourceTableName}}
  GROUP BY
      ProjectId,
      NamespaceId,
      Timestamp,
      TraceFlags,
      SeverityNumber,
      ServiceName
)
GROUP BY
    ProjectId,
    NamespaceId,
    Timestamp,
    ObservedTimestamp,
    TraceFlags,
    SeverityNumber,
    ServiceName`
)

// NOTE(prozlach): see the comments in the code to understand how these values
// were chosen
var groupingSuffixes = [...]string{"25920s", "259200ms", "2592ms"}
var groupingSuffixToInterval = map[string]string{
	"25920s":   "432 minute",
	"259200ms": "259200 millisecond",
	"2592ms":   "2592 millisecond",
}

func renderQAnaliticsQueries(selfHosted, developmentMode bool) ([]*goose.Migration, error) {
	res := make([]*goose.Migration, len(groupingSuffixes)*2)

	for i, groupingSuffix := range groupingSuffixes {
		tableName := func(name string) string {
			return name + "_" + groupingSuffix
		}

		offset := i * 2
		{
			data := TemplateData{
				TableName: tableName(constants.LoggingTableQAnaliticsTableName),
				TTLDays:   strconv.Itoa(constants.LoggingDataTTLDays),
			}

			m, err := renderMigration(qAnaliticsMvTargetTmpl, data, selfHosted, developmentMode)
			if err != nil {
				return nil, fmt.Errorf("rendering qanalytics migration %s: %w", groupingSuffix, err)
			}
			m.Version += int64(offset)
			res[offset] = m
		}

		{
			data := TemplateData{
				TableName:       tableName(constants.LoggingTableQAnaliticsMVName),
				SourceTableName: constants.LoggingTableName,
				TargetTableName: tableName(constants.LoggingTableQAnaliticsTableName),
				Interval:        groupingSuffixToInterval[groupingSuffix],
			}

			m, err := renderMigration(qAnaliticsMvTmpl, data, selfHosted, developmentMode)
			if err != nil {
				return nil, fmt.Errorf("rendering qanalytics migration %s: %w", groupingSuffix, err)
			}
			m.Version += int64(offset + 1)
			res[offset+1] = m
		}
	}

	return res, nil
}
