package logging

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addTenantIDLogsMainTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS TenantId String CODEC(ZSTD(1)) AFTER NamespaceId;
`

func renderAddTenantIDLogsMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		addTenantIDLogsMainTmpl,
		TemplateData{
			TableName: constants.LoggingTableName,
		},
		selfHosted,
		devel,
	)
}
