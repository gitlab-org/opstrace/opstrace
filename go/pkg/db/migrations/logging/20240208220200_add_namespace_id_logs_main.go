package logging

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addNamespaceIDTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD COLUMN IF NOT EXISTS NamespaceId Int64 CODEC(ZSTD(1)) AFTER ProjectId;
`

func renderAddNamespaceIDLogsMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		addNamespaceIDTmpl,
		TemplateData{
			TableName: constants.LoggingTableName,
		},
		selfHosted,
		devel,
	)
}
