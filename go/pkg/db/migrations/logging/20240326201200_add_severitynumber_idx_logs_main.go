package logging

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addSeverityNumberIdxIDTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD INDEX IF NOT EXISTS idx_severity_number SeverityNumber TYPE set(25) GRANULARITY 1;
`

func renderAddSeverityNumberIdxIDLogsMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		addSeverityNumberIdxIDTmpl,
		TemplateData{
			TableName: constants.LoggingTableName,
		},
		selfHosted,
		devel,
	)
}
