package logging

import (
	"github.com/pressly/goose/v3"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
)

const addFingerprintIdxIDTmpl = `
ALTER TABLE {{.DatabaseName}}.{{.TableName}}{{if .SelfHostedVersion}} ON CLUSTER '{cluster}'{{ end }}
ADD INDEX IF NOT EXISTS idx_fingerprint Fingerprint TYPE BLOOM_FILTER(0.001) GRANULARITY 1;
`

func renderAddFingerprintIdxIDLogsMain(selfHosted bool, devel bool) (*goose.Migration, error) {
	return renderMigration(
		addFingerprintIdxIDTmpl,
		TemplateData{
			TableName: constants.LoggingTableName,
		},
		selfHosted,
		devel,
	)
}
