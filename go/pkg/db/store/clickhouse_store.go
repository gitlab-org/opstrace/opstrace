package store

import (
	"bytes"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"text/template"

	"github.com/pressly/goose/v3/database"
)

type ClickHouseStore struct {
	TName          string
	CreateTableSQL string
	Params         clusterParameters
}

var _ database.Store = (*ClickHouseStore)(nil)

const (
	paramOnCluster    = "ON_CLUSTER"
	paramClusterMacro = "CLUSTER_MACRO"
	paramNoKeeperMap  = "NO_KEEPER_MAP"
)

type clusterParameters struct {
	OnCluster    bool
	NoKeeperMap  bool
	ClusterMacro string
}

//nolint:lll
var createTableTmpl = `
CREATE TABLE IF NOT EXISTS {{.TName}}{{if .OnCluster}} ON CLUSTER '{{.ClusterMacro}}' {{end}}(
	version_id Int64,
	is_applied UInt8,
	date Date default now(),
	tstamp DateTime64(9, 'UTC') default now64(9, 'UTC')
)
ENGINE = {{if .NoKeeperMap}}MergeTree{{else}}{{if .OnCluster}}KeeperMap('/goose_version_repl'){{else}}KeeperMap('/goose_version'){{end}}{{end}}
PRIMARY KEY version_id
`

func (c *ClickHouseStore) CreateVersionTable(ctx context.Context, db database.DBTxConn) error {
	q := c.CreateTable(c.TName)
	if q == "" {
		return fmt.Errorf("no create table SQL")
	}

	if _, err := db.ExecContext(ctx, q); err != nil {
		return fmt.Errorf("failed to create version table %q: %w", c.TName, err)
	}
	return nil
}

func (c *ClickHouseStore) CreateTable(tableName string) string {
	tmpl, err := template.New("create_table").Parse(createTableTmpl)
	if err != nil {
		// Note(Arun): It is not ideal but the function signature
		// is part of an interface which will require rework if changed.
		return ""
	}
	buf := bytes.Buffer{}
	err = tmpl.Execute(&buf, struct {
		TName        string
		ClusterMacro string
		OnCluster    bool
		NoKeeperMap  bool
	}{
		tableName,
		c.Params.ClusterMacro,
		c.Params.OnCluster,
		c.Params.NoKeeperMap,
	})
	if err != nil {
		// Note(Arun): It is not ideal but the function signature
		// is part of an interface which will require rework if changed.
		return ""
	}
	return buf.String()
}

func (c *ClickHouseStore) Insert(ctx context.Context, db database.DBTxConn, req database.InsertRequest) error {
	q := c.InsertVersion(c.TName)
	if _, err := db.ExecContext(ctx, q, req.Version, true); err != nil {
		return fmt.Errorf("failed to insert version %d: %w", req.Version, err)
	}
	return nil
}

func (c *ClickHouseStore) InsertVersion(tableName string) string {
	q := `INSERT INTO %s (version_id, is_applied) VALUES ($1, $2)`
	return fmt.Sprintf(q, tableName)
}

func (c *ClickHouseStore) Delete(ctx context.Context, db database.DBTxConn, version int64) error {
	q := c.DeleteVersion(c.TName)
	if _, err := db.ExecContext(ctx, q, version); err != nil {
		return fmt.Errorf("failed to delete version %d: %w", version, err)
	}
	return nil
}

func (c *ClickHouseStore) DeleteVersion(tableName string) string {
	q := `ALTER TABLE %s DELETE WHERE version_id = $1 SETTINGS mutations_sync = 2`
	return fmt.Sprintf(q, tableName)
}

func (c *ClickHouseStore) GetMigration(
	ctx context.Context,
	db database.DBTxConn,
	version int64,
) (*database.GetMigrationResult, error) {
	q := c.GetMigrationByVersion(c.TName)
	var result database.GetMigrationResult
	if err := db.QueryRowContext(ctx, q, version).Scan(
		&result.Timestamp,
		&result.IsApplied,
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%w: %d", database.ErrVersionNotFound, version)
		}
		return nil, fmt.Errorf("failed to get migration %d: %w", version, err)
	}
	return &result, nil
}

func (c *ClickHouseStore) GetMigrationByVersion(tableName string) string {
	q := `SELECT tstamp, is_applied FROM %s WHERE version_id = $1 ORDER BY tstamp DESC LIMIT 1`
	return fmt.Sprintf(q, tableName)
}

func (c *ClickHouseStore) ListMigrations(
	ctx context.Context,
	db database.DBTxConn,
) ([]*database.ListMigrationsResult, error) {
	q := c.ListMigrationsSQL(c.TName)
	rows, err := db.QueryContext(ctx, q)
	if err != nil {
		return nil, fmt.Errorf("failed to list migrations: %w", err)
	}
	defer rows.Close()

	var migrations []*database.ListMigrationsResult
	for rows.Next() {
		var result database.ListMigrationsResult
		if err := rows.Scan(&result.Version, &result.IsApplied); err != nil {
			return nil, fmt.Errorf("failed to scan list migrations result: %w", err)
		}
		migrations = append(migrations, &result)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("failed to scan list migrations result: %w", err)
	}
	return migrations, nil
}

func (c *ClickHouseStore) ListMigrationsSQL(tableName string) string {
	q := `SELECT version_id, is_applied FROM %s ORDER BY tstamp DESC`
	return fmt.Sprintf(q, tableName)
}

func (c *ClickHouseStore) GetLatestVersion(ctx context.Context, db database.DBTxConn) (int64, error) {
	q := c.GetLatestVersionSQL(c.TName)
	var version sql.NullInt64
	if err := db.QueryRowContext(ctx, q).Scan(&version); err != nil {
		return -1, fmt.Errorf("failed to get latest version: %w", err)
	}
	if !version.Valid {
		return -1, fmt.Errorf("latest %w", database.ErrVersionNotFound)
	}
	return version.Int64, nil
}

func (c *ClickHouseStore) GetLatestVersionSQL(tableName string) string {
	q := `SELECT max(version_id) FROM %s`
	return fmt.Sprintf(q, tableName)
}
func (c *ClickHouseStore) Tablename() string {
	return c.TName
}

func (c *ClickHouseStore) TableExists(ctx context.Context, conn database.DBTxConn, tableName string) (bool, error) {
	res := conn.QueryRowContext(
		ctx,
		fmt.Sprintf(`EXISTS TABLE %s`, tableName),
	)
	if err := res.Err(); err != nil {
		return false, fmt.Errorf("check table exists: %w", res.Err())
	}

	ret := 0
	if err := res.Scan(&ret); err != nil {
		return false, fmt.Errorf("parse table exists: %w", err)
	}
	return ret == 1, nil
}

func (c *ClickHouseStore) AttachOptions(options map[string]string) {
	if val, ok := options[paramOnCluster]; ok {
		if val == "true" {
			clusterMacro, ok := options[paramClusterMacro]
			if !ok {
				clusterMacro = "{cluster}"
			}
			c.Params.ClusterMacro = clusterMacro
			c.Params.OnCluster = true
		}
	}
	if val, ok := options[paramNoKeeperMap]; ok {
		if val == "true" {
			c.Params.NoKeeperMap = true
		}
	}
}
