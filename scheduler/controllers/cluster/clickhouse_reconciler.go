package cluster

import (
	"context"
	"errors"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/pressly/goose/v3"
	"go.opentelemetry.io/otel/trace"
	"k8s.io/utils/ptr"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/analytics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/errortracking"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/logging"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/metrics"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/db/migrations/tracing"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/clickhouse"
)

type ClickHouseReconciler struct {
	Teardown bool
	Log      logr.Logger

	tracer         trace.Tracer
	tracingEnabled bool
}

func NewClickHouseReconciler(t trace.Tracer, teardown bool, logger logr.Logger) *ClickHouseReconciler {
	ch := &ClickHouseReconciler{
		Teardown: teardown,
		Log:      logger.WithName("clickhouse"),
	}
	ch.setupTracing(t)
	return ch
}

func (i *ClickHouseReconciler) setupTracing(tracer trace.Tracer) {
	if tracer != nil {
		i.tracer = tracer
		i.tracingEnabled = true
	}
}

func (i *ClickHouseReconciler) Reconcile(
	ctx context.Context,
	state *ClusterState,
	cr *v1alpha1.Cluster,
) common.DesiredState {
	if i.tracingEnabled {
		_, span := i.tracer.Start(ctx, "clickhouse-reconciler#Reconcile")
		defer span.End()
	}

	desired := common.DesiredState{}

	// when tearing down our desired state, it is important we follow a sequence of
	// actions which allows all components to be deleted correctly without creating
	// orphans OR causing a component to be gone before anything that still references
	// and/or needs it. Though not mandatory, a good sequence to follow is the reverse
	// order of how each component got provisioned when building the desired state
	// initially.
	if i.Teardown {
		desired = desired.AddAction(i.getServiceMonitorDesiredState(cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))
		desired = desired.AddActions(i.getReadiness(state))
	} else {
		desired = desired.AddAction(i.getCredentialsDesiredState(cr))

		desired = desired.AddActions(i.setupSelfhostedDatabases(state, cr))
		desired = desired.AddActions(i.setupCloudDatabases(state, cr))
	}

	return desired
}

func (i *ClickHouseReconciler) getReadiness(state *ClusterState) []common.Action {
	if i.Teardown {
		if state.ClickHouse.Cluster != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.ClickHouse.Cluster,
					Msg: "check clickhouse cluster is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}

	if !state.ClickHouse.isClusterReady() {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse cluster not ready",
				Error: errors.New("clickhouse cluster not ready"),
			},
		}
	}

	return []common.Action{
		common.LogAction{
			Msg: "clickhouse cluster is ready",
		},
	}
}

func (i *ClickHouseReconciler) setupSelfhostedDatabases(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	var actions []common.Action //nolint:prealloc

	// check if needed migrations have already been applied
	if cr.Status.LastMigrationApplied != nil {
		if *cr.Status.LastMigrationApplied == constants.DockerImageTag {
			return []common.Action{
				common.LogAction{
					Msg: "Self-hosted migrations were already applied",
				},
			}
		}
	}

	useRemoteStorageTracing, err := common.ParseFeatureAsBool(cr.Spec.Features, "TRACING_USE_REMOTE_STORAGE_BACKEND")
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to parse value for feature <TRACING_USE_REMOTE_STORAGE_BACKEND> as a boolean",
				Error: err,
			},
		}
	}

	endpoints, err := state.ClickHouse.GetSelfhostedEndpoints()
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "failed to obtain clickhouse scheduler endpoints, this should be a transient error that resolves on next reconcile",
				Error: err,
			},
		}
	}

	dbNames := []string{
		constants.JaegerDatabaseName,
		constants.ErrorTrackingAPIDatabaseName,
		constants.MetricsDatabaseName,
		constants.LoggingDatabaseName,
		constants.AnalyticsDatabaseName,
		constants.AlertsDatabaseName,
	}
	if useRemoteStorageTracing {
		switch cr.Spec.Target {
		case common.GCP:
			dbNames = append(dbNames, constants.JaegerGCSDatabaseName)
		case common.AWS:
			dbNames = append(dbNames, constants.JaegerS3DatabaseName)
		}
	}

	for _, dbName := range dbNames {
		actions = append(actions, common.ClickHouseAction{
			Msg: fmt.Sprintf("create clickhouse database if not exists: %s", dbName),
			SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s ON CLUSTER '{cluster}'", dbName),
			URL: endpoints.Native,
		})
	}

	actions = append(
		actions,
		processMigrations(
			errortracking.SetupMigrations, true,
			"clickhouse migrations for self-hosted errortracking",
			endpoints.Native.JoinPath(constants.ErrorTrackingAPIDatabaseName).String(),
			cr,
		),
		processMigrations(
			tracing.SetupMigrations, true,
			"clickhouse migrations for self-hosted tracing",
			endpoints.Native.JoinPath(constants.TracingDatabaseName).String(),
			cr,
		),
		processMigrations(
			metrics.SetupMigrations, true,
			"clickhouse migrations for self-hosted metrics",
			endpoints.Native.JoinPath(constants.MetricsDatabaseName).String(),
			cr,
		),
		processMigrations(
			logging.SetupMigrations, true,
			"clickhouse migrations for self-hosted logs",
			endpoints.Native.JoinPath(constants.LoggingDatabaseName).String(),
			cr,
		),
		processMigrations(
			analytics.SetupMigrations, true,
			"clickhouse migrations for self-hosted analytics",
			endpoints.Native.JoinPath(constants.AnalyticsDatabaseName).String(),
			cr,
		),
		processMigrations(
			alerts.SetupMigrations, true,
			"clickhouse migrations for self-hosted alerts",
			endpoints.Native.JoinPath(constants.AlertsDatabaseName).String(),
			cr,
		),
	)

	return actions
}

func (i *ClickHouseReconciler) setupCloudDatabases(state *ClusterState, cr *v1alpha1.Cluster) common.DesiredState {
	var actions []common.Action //nolint:prealloc

	// check if needed migrations have already been applied
	if cr.Status.LastCloudMigrationApplied != nil {
		if *cr.Status.LastCloudMigrationApplied == constants.DockerImageTag {
			return []common.Action{
				common.LogAction{
					Msg: "Cloud migrations were already applied",
				},
			}
		}
	}

	endpoint, err := state.ClickHouse.GetCloudEndpoints()
	if err != nil {
		actions = append(actions, common.LogAction{
			Msg:   "failed to obtain clickhouse cloud endpoint",
			Error: err,
		})
		return actions
	}

	if endpoint == nil {
		return []common.Action{
			common.LogAction{
				Msg: "ClickHouse Cloud DSN not provided",
			},
		}
	}

	for _, dbName := range []string{
		constants.ErrorTrackingAPIDatabaseName,
		constants.TracingDatabaseName,
		constants.MetricsDatabaseName,
		constants.LoggingDatabaseName,
		constants.AnalyticsDatabaseName,
		constants.AlertsDatabaseName,
	} {
		actions = append(actions, common.ClickHouseAction{
			Msg: fmt.Sprintf("create clickhouse database on CH Cloud if not exists: %s", dbName),
			SQL: fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s", dbName),
			URL: endpoint.Native,
		})
	}

	actions = append(
		actions,
		processMigrations(
			errortracking.SetupMigrations, false,
			"clickhouse cloud migrations for errortracking",
			endpoint.Native.JoinPath(constants.ErrorTrackingAPIDatabaseName).String(),
			cr,
		),
		processMigrations(
			tracing.SetupMigrations, false,
			"clickhouse cloud migrations for tracing",
			endpoint.Native.JoinPath(constants.TracingDatabaseName).String(),
			cr,
		),
		processMigrations(
			metrics.SetupMigrations, false,
			"clickhouse cloud migrations for metrics",
			endpoint.Native.JoinPath(constants.MetricsDatabaseName).String(),
			cr,
		),
		processMigrations(
			logging.SetupMigrations, false,
			"clickhouse cloud migrations for logs",
			endpoint.Native.JoinPath(constants.LoggingDatabaseName).String(),
			cr,
		),
		processMigrations(
			analytics.SetupMigrations, false,
			"clickhouse cloud migrations for analytics",
			endpoint.Native.JoinPath(constants.AnalyticsDatabaseName).String(),
			cr,
		),
		processMigrations(
			alerts.SetupMigrations, false,
			"clickhouse cloud migrations for alerts",
			endpoint.Native.JoinPath(constants.AlertsDatabaseName).String(),
			cr,
		),
	)

	return actions
}

func processMigrations(
	migrationsFunc func(bool, bool) ([]*goose.Migration, error),
	selfHostedVersion bool,
	name string,
	dsn string,
	cr *v1alpha1.Cluster,
) common.Action {
	// development mode is always set to false in orchestrated mode
	collectedMigrations, err := migrationsFunc(selfHostedVersion, false)
	if err != nil {
		panic(fmt.Sprintf("migration setup failed for %s: %s", name, err.Error()))
	}

	if len(collectedMigrations) == 0 {
		return common.LogAction{
			Msg: fmt.Sprintf("%q has no migrations to run", name),
		}
	}

	res := common.ClickHouseCloudMigrationAction{
		Msg:               "clickhouse migration",
		ClickHouseDSN:     dsn,
		GoMigrationsToRun: collectedMigrations,
	}

	if selfHostedVersion {
		res.SetLastMigrationApplied = func(version string) {
			cr.Status.LastMigrationApplied = ptr.To(version)
		}
	} else {
		res.SetLastMigrationApplied = func(version string) {
			cr.Status.LastCloudMigrationApplied = ptr.To(version)
		}
	}

	return res
}

func (i *ClickHouseReconciler) getCredentialsDesiredState(cr *v1alpha1.Cluster) common.Action {
	s := clickhouse.Credentials(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: s,
			Msg: "clickhouse credentials",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: s,
		Msg: "clickhouse credentials",
		Mutator: func() error {
			return clickhouse.CredentialsMutator(cr, s)
		},
	}
}

func (i *ClickHouseReconciler) getClickHouseDesiredState(state *ClusterState, cr *v1alpha1.Cluster) []common.Action {
	if state.ClickHouse.Credentials == nil {
		if i.Teardown {
			// if the secret does not exist now AND we're tearing down the CR, we're done here
			// because the CR should have been deleted in a previous reconcile loop. Just make
			// sure that has happened successfully.
			if state.ClickHouse.Cluster != nil {
				return []common.Action{
					common.CheckGoneAction{
						Ref: state.ClickHouse.Cluster,
						Msg: "check clickhouse CR is gone",
					},
				}
			} else {
				return []common.Action{} // nothing to do
			}
		}
		// if the secret does not exist yet BUT we're in a provisioning loop, report transient error
		// and come back again during the next reconcile.
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: fmt.Errorf("haven't read clickhouse scheduler user credentials yet, this should be a transient error that resolves on next reconcile"),
			},
		}
	}

	user, err := state.ClickHouse.GetSchedulerCredentials()
	if err != nil {
		return []common.Action{
			common.LogAction{
				Msg:   "clickhouse CR",
				Error: err,
			},
		}
	}
	current := clickhouse.ClickHouse(cr, user)

	if i.Teardown {
		actions := []common.Action{}
		actions = append(actions, common.GenericDeleteAction{
			Ref: current,
			Msg: "clickhouse CR",
		})
		// make sure the CR is correctly deleted after the operator is done
		// cleaning up all associated resources
		if state.ClickHouse.Cluster != nil {
			actions = append(actions, common.CheckGoneAction{
				Ref: state.ClickHouse.Cluster,
				Msg: "check clickhouse CR is gone",
			})
		}
		return actions
	}

	return []common.Action{
		common.GenericCreateOrUpdateAction{
			Ref: current,
			Msg: "Clickhouse CR",
			Mutator: func() error {
				return clickhouse.ClickHouseMutator(cr, current, user)
			},
		},
	}
}

func (i *ClickHouseReconciler) getServiceMonitorDesiredState(cr *v1alpha1.Cluster) common.Action {
	monitor := clickhouse.ServiceMonitor(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: monitor,
			Msg: "clickhouse cluster servicemonitor",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: monitor,
		Msg: "clickhouse cluster servicemonitor",
		Mutator: func() error {
			return clickhouse.ServiceMonitorMutator(cr, monitor)
		},
	}
}
