package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"go.opentelemetry.io/otel/trace"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	kustomize "sigs.k8s.io/kustomize/api/types"
)

type MonitoringReconciler struct {
	BaseReconciler

	chState *ClickHouseState
}

func NewMonitoringReconciler(
	t trace.Tracer,
	initialManifests map[string][]byte,
	teardown bool,
	chState *ClickHouseState,
) *MonitoringReconciler {
	res := &MonitoringReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.MonitoringInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.MonitoringInventoryID,
			reconcilerName: "monitoring",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.Monitoring
			},
		},

		chState: chState,
	}

	res.setupTracing(t)
	res.subclassApplyMethod = res.applyConfiguration
	return res
}

func (i *MonitoringReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}
	return res, nil
}
