package cluster

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"go.opentelemetry.io/otel/trace"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

const (
	queryPath     = `^/v3/query/(?P<projectID>[0-9]+)/(?P<endpoint>(metrics|logs|analytics|traces|services))`
	queryPathOIDC = `^/observability/v1/(?P<endpoint>(metrics|logs|analytics|alerts|traces|services))`
)

type QueryAPIReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewQueryAPIReconciler(
	t trace.Tracer,
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState,
) *QueryAPIReconciler {
	res := &QueryAPIReconciler{
		clusterState: clusterState,
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.QueryAPIInventoryID)),
			initialManifests: initialManifests,
			inventoryID:      constants.QueryAPIInventoryID,
			reconcilerName:   "query-api",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.QueryAPI
			},
			serviceAccountName: constants.QueryAPIName,
		},
	}
	res.setupTracing(t)
	res.subclassApplyMethod = res.applyConfiguration
	return res
}

//nolint:funlen
func (r *QueryAPIReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res, err := r.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}
	res.Images = []kustomize.Image{
		{
			Name:    "query-api-image-placeholder",
			NewName: constants.DockerImageName(constants.QueryAPIImageName),
			NewTag:  constants.DockerImageTag,
		},
	}
	err = r.applyDeploymentConfiguration(cr, res)
	if err != nil {
		return nil, err
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			// Ingress
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && PathRegexp(\"%s\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/3/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
				]`,
				cr.Spec.GetHost(), queryPath,
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: constants.QueryAPIName,
				},
			},
		},
		types.Patch{
			// Ingress
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && Method(\"GET\") && PathRegexp(\"%s\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/3/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
				]`,
				cr.Spec.GetHost(), queryPathOIDC,
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "IngressRoute",
					},
					Name: constants.QueryAPIOIDCName,
				},
			},
		},
		// Middleware
		types.Patch{
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/headers/accessControlAllowOriginList", "value": ["%s"]}]`,
				cr.Spec.GitLab.TrimInstanceURL(),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Middleware",
					},
					Name: "cors-headers-queryapi",
				},
			},
		},
		// PodMonitor
		types.Patch{
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "PodMonitor",
					},
					Name: constants.QueryAPIName,
				},
			},
			Patch: fmt.Sprintf(
				`[{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}]`,
				cr.Namespace(),
			),
		},
	)

	return res, nil
}

func (r *QueryAPIReconciler) applyDeploymentConfiguration(cr *v1alpha1.Cluster, k *kustomize.Kustomization) error {
	skipTLSInsecureVerify := "false"
	if cr.Spec.Target == common.KIND {
		// running on localhost domain and the loopback interface is not trusted
		skipTLSInsecureVerify = "true"
	}
	clickHouseEndpoints, err := r.clusterState.ClickHouse.GetSelfhostedEndpoints()
	if err != nil {
		return fmt.Errorf("retrieving clickhouse endpoints: %w", err)
	}
	clickHouseDSN := clickHouseEndpoints.Native.String()

	patches := []string{
		fmt.Sprintf(
			`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-clickhouse-dsn=%s" }`,
			clickHouseDSN,
		),
		fmt.Sprintf(
			`{
				"op": "add",
				"path": "/spec/template/spec/containers/0/args/-",
				"value": "-otlp-endpoint=http://%s-collector.%s.svc.cluster.local:4318/"
			}`,
			constants.OpenTelemetrySystemTracingCollector,
			cr.Namespace(),
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/metadata/annotations/secret.reloader.stakater.com~1reload", "value": "%s,redis" }`,
			cr.Spec.GitLab.AuthSecret.Name,
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/valueFrom/secretKeyRef/name", "value": "%s" }`,
			cr.Spec.GitLab.AuthSecret.Name,
		),
		fmt.Sprintf(
			`{"op": "replace", "path": "/spec/template/spec/containers/0/env/1/value", "value": "%s" }`,
			skipTLSInsecureVerify,
		),
		// Redis address
		fmt.Sprintf(`{"op": "add", "path": "/spec/template/spec/containers/0/env/2/value", "value": "%s" }`,
			fmt.Sprintf("rfs-%s.%s.svc.cluster.local:26379", constants.RedisName, cr.Namespace()),
		),
	}

	for _, provider := range cr.Spec.GitLab.OidcProviders {
		patches = append(patches, fmt.Sprintf(
			`{
				"op": "add",
				"path": "/spec/template/spec/containers/0/args/-",
				"value": "-gitlab-oidc-provider=%s"
			}`,
			provider,
		))
	}

	if r.clusterState.LogLevel != "" {
		patches = append(patches,
			fmt.Sprintf(
				`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-log-level=%s" }`,
				r.clusterState.LogLevel,
			),
		)
	}

	clickhouseCloudEndpoint, err := r.clusterState.ClickHouse.GetCloudEndpoints()
	if err != nil {
		return fmt.Errorf("failed to retrieve clickhouse cloud endpoints %w", err)
	}
	if clickhouseCloudEndpoint != nil {
		patches = append(patches, fmt.Sprintf(
			`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-clickhouse-cloud-dsn=%s" }`,
			clickhouseCloudEndpoint.Native.String(),
		))
	} else {
		patches = append(patches, fmt.Sprintf(
			`{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "-clickhouse-cloud-dsn=%s" }`,
			"",
		))
	}

	k.Patches = append(
		k.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[ %s ]`, strings.Join(patches, ","),
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "Deployment",
					},
					Name: constants.QueryAPIName,
				},
			},
		},
	)

	return nil
}
