package cluster

import (
	"fmt"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	"go.opentelemetry.io/otel/trace"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

type StorageReconciler struct {
	BaseReconciler
}

func NewStorageReconciler(
	t trace.Tracer,
	initialManifests map[string][]byte,
	teardown bool,
) *StorageReconciler {
	res := &StorageReconciler{
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.StorageInventoryID)),
			initialManifests: initialManifests,

			inventoryID:    constants.StorageInventoryID,
			reconcilerName: "storage",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.Storage
			},
		},
	}

	res.setupTracing(t)
	res.subclassApplyMethod = res.applyConfiguration

	return res
}

func (i *StorageReconciler) applyConfiguration(cr *v1alpha1.Cluster) (*kustomize.Kustomization, error) {
	res := &kustomize.Kustomization{
		TypeMeta: kustomize.TypeMeta{
			Kind:       kustomize.KustomizationKind,
			APIVersion: kustomize.KustomizationVersion,
		},
		Namespace: cr.Namespace(),
		Resources: []string{
			ctrlcommon.TmpResourceData,
		},
		Patches: make([]types.Patch, 0),
	}

	var storageType string
	var provisioner string
	switch cr.Spec.Target {
	case common.AWS:
		storageType = "gp2"
		provisioner = "kubernetes.io/aws-ebs"
	case common.GCP:
		storageType = "pd-ssd"
		provisioner = "pd.csi.storage.gke.io"
	case common.KIND:
		storageType = ""
		provisioner = "rancher.io/local-path"
	default:
		return nil, fmt.Errorf("unsupported cloud target %s", cr.Spec.Target)
	}

	res.Patches = append(
		res.Patches,
		types.Patch{
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/provisioner", "value": "%s"}
				]`,
				provisioner,
			),
			Target: &types.Selector{
				ResId: resid.ResId{
					Gvk: resid.Gvk{
						Kind: "StorageClass",
					},
					Name: constants.StorageClassName,
				},
			},
		},
	)

	if storageType != "" {
		res.Patches = append(
			res.Patches,
			types.Patch{
				Patch: fmt.Sprintf(
					`[
					{"op": "add", "path": "/parameters", "value": { "type": "%s"}}
	        			]`,
					storageType,
				),
				Target: &types.Selector{
					ResId: resid.ResId{
						Gvk: resid.Gvk{
							Kind: "StorageClass",
						},
						Name: constants.StorageClassName,
					},
				},
			},
		)
	}

	return res, nil
}
