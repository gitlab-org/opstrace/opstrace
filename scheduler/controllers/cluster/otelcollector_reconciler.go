package cluster

import (
	"fmt"
	"strings"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"go.opentelemetry.io/otel/trace"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/kustomize/api/types"
	kustomize "sigs.k8s.io/kustomize/api/types"
	"sigs.k8s.io/kustomize/kyaml/resid"
)

const (
	IngressBasePathOIDC = `^/observability/v1/(?P<endpoint>(traces|logs|metrics))`
)

type OTELCollectorReconciler struct {
	BaseReconciler
	clusterState *ClusterState
}

func NewOTELCollectorReconciler(
	t trace.Tracer,
	initialManifests map[string][]byte,
	teardown bool,
	clusterState *ClusterState,
) *OTELCollectorReconciler {
	res := &OTELCollectorReconciler{
		clusterState: clusterState,
		BaseReconciler: BaseReconciler{
			Teardown:         teardown,
			Log:              logf.Log.WithName(fmt.Sprintf("manifests/%s", constants.OtelCollectorInventoryID)),
			initialManifests: initialManifests,
			inventoryID:      constants.OtelCollectorInventoryID,
			reconcilerName:   "otel-collector",
			getOverridesF: func(cr *v1alpha1.Cluster) *v1alpha1.KustomizeOverridesSpec {
				return cr.Spec.Overrides.OtelCollector
			},
			serviceAccountName: constants.OtelCollectorComponentName,
		},
	}
	res.setupTracing(t)
	res.subclassApplyMethod = res.applyConfiguration
	return res
}

//nolint:funlen
func (i *OTELCollectorReconciler) applyConfiguration(
	cr *v1alpha1.Cluster,
) (*kustomize.Kustomization, error) {
	skipTLSInsecureVerify := "false"
	if cr.Spec.Target == common.KIND {
		// running on localhost domain and the loopback interface is not trusted
		skipTLSInsecureVerify = "true"
	}
	res, err := i.BaseReconciler.applyBaseConfiguration(cr)
	if err != nil {
		return nil, err
	}

	res.Images = []kustomize.Image{
		{
			Name:    "otel-collector-image-placeholder",
			NewName: constants.DockerImageName(constants.OtelCollectorComponentName),
			NewTag:  constants.DockerImageTag,
		},
		{
			Name:    "rate-limit-proxy-image-placeholder",
			NewName: constants.DockerImageName(constants.RateLimitProxyImageName),
			NewTag:  constants.DockerImageTag,
		},
	}

	patches := []types.Patch{}

	// deployment
	clickHouseEndpoints, err := i.clusterState.ClickHouse.GetSelfhostedEndpoints()
	if err != nil {
		return res, fmt.Errorf("retrieving clickhouse endpoints: %w", err)
	}
	clickHouseDSN := clickHouseEndpoints.Native.String()
	if clickHouseDSN != "" {
		patches = append(patches, types.Patch{
			Target: otelDeploymentSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"},
					{"op": "add", "path": "/spec/template/spec/containers/1/args/-", "value": "-clickhouse-dsn=%s"}
				]`,
				fmt.Sprintf(
					"--set=exporters.gitlabobservability.clickhouse_dsn=%s/tracing",
					clickHouseDSN,
				),
				clickHouseDSN,
			),
		})
	}

	clickhouseCloudEndpoint, err := i.clusterState.ClickHouse.GetCloudEndpoints()
	if err != nil {
		return res, fmt.Errorf("failed to retrieve clickhouse cloud endpoints %w", err)
	}
	if clickhouseCloudEndpoint != nil {
		patches = append(patches, types.Patch{
			Target: otelDeploymentSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "%s"},
					{"op": "add", "path": "/spec/template/spec/containers/1/args/-", "value": "-clickhouse-cloud-dsn=%s"}
				]`,
				fmt.Sprintf(
					"--set=exporters.gitlabobservability.clickhouse_cloud_dsn=%s",
					clickhouseCloudEndpoint.Native.String(),
				),
				clickhouseCloudEndpoint.Native.String(),
			),
		})
	}

	// Build the provider variable for passing it to otel collector configuration in format: `[ 'url1', 'url2' ]`
	providers := fmt.Sprintf(`[ '%s' ]`, strings.Join(cr.Spec.GitLab.OidcProviders, `', '`))

	patches = append(patches,
		types.Patch{
			Target: otelDeploymentSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/metadata/annotations/secret.reloader.stakater.com~1reload", "value": "%s,redis" },
					{"op": "replace", "path": "/spec/template/spec/containers/0/env/0/valueFrom/secretKeyRef/name", "value": "%s" },
					{"op": "replace", "path": "/spec/template/spec/containers/0/env/1/value", "value": "%s" },
					{"op": "add", "path": "/spec/template/spec/containers/0/env/2/value", "value": "%s" },
					{"op": "add", "path": "/spec/template/spec/containers/1/args/-", "value": "-otlp-endpoint=http://%s-collector.%s.svc.cluster.local:4318/"},
					{"op": "add", "path": "/spec/template/spec/containers/1/env/0/value", "value": "%s" }
				]`,
				cr.Spec.GitLab.AuthSecret.Name,
				cr.Spec.GitLab.AuthSecret.Name,
				skipTLSInsecureVerify,
				providers,
				constants.OpenTelemetrySystemTracingCollector,
				cr.Namespace(),
				// Redis address
				fmt.Sprintf("rfs-%s.%s.svc.cluster.local:26379", constants.RedisName, cr.Namespace()),
			),
		},
	)

	if i.clusterState.LogLevel != "" {
		patches = append(patches,
			types.Patch{
				Target: otelDeploymentSelector(),
				Patch: fmt.Sprintf(
					`[{"op": "add", "path": "/spec/template/spec/containers/1/args/-", "value": "-log-level=%s" }]`,
					i.clusterState.LogLevel,
				),
			},
		)
	}

	// ingress
	patches = append(patches,
		types.Patch{
			Target: otelIngressRouteSelector(),
			Patch: fmt.Sprintf(
				`[
					{"op": "replace", "path": "/spec/routes/0/match", "value": "Host(\"%s\") && Method(\"POST\") && PathRegexp(\"%s\")" },
					{"op": "replace", "path": "/spec/routes/0/services/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/0/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/1/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/routes/0/middlewares/2/namespace", "value": "%s" },
					{"op": "replace", "path": "/spec/tls/domains/0/main", "value": "%s" }
				]`,
				cr.Spec.GetHost(),
				IngressBasePathOIDC,
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Namespace(),
				cr.Spec.GetHost(),
			),
		},
	)

	// podmonitor
	patches = append(patches, types.Patch{
		Target: otelPodmonitorSelector(),
		Patch: fmt.Sprintf(
			`[
				{"op": "replace", "path": "/spec/namespaceSelector", "value": { "matchNames": ["%s"] }}
			]`,
			cr.Namespace(),
		),
	})
	res.Patches = patches

	return res, nil
}

func otelDeploymentSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "Deployment",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}

func otelPodmonitorSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "PodMonitor",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}

func otelIngressRouteSelector() *kustomize.Selector {
	return &types.Selector{
		ResId: resid.ResId{
			Gvk: resid.Gvk{
				Kind: "IngressRoute",
			},
			Name: constants.OtelCollectorComponentName,
		},
	}
}
