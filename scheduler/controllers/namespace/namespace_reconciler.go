package namespace

import (
	"fmt"

	"github.com/go-logr/logr"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

type GitLabNamespaceReconciler struct {
	Teardown bool
	Log      logr.Logger
}

func NewGitLabNamespaceReconciler(teardown bool, logger logr.Logger) *GitLabNamespaceReconciler {
	return &GitLabNamespaceReconciler{
		Teardown: teardown,
		Log:      logger.WithName("gitlab-namespace"),
	}
}

func (i *GitLabNamespaceReconciler) Reconcile(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) common.DesiredState {
	desired := common.DesiredState{}
	if i.Teardown {
		// remove group first
		desired = desired.AddAction(i.getGroupDesiredState(state.GetClusterState(), cr))
		// wait for group to be gone before we delete the clickhouse credentials
		// otherwise the tenant-operator has to do some magic to tear the group
		// down without access to these credentials
		desired = desired.AddActions(i.getGroupGone(state))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		// If there are no more active groups in this tenant then we teardown the
		// entire tenant
		if state.TenantEmpty() {
			// remove Tenant. This will cause all groups within the tenant to
			// be torn down too (via the tenant-operator)
			desired = desired.AddAction(i.getTenantDesiredState(state.GetClusterState(), cr))
			// make sure tenant is gone before we remove it's operator
			desired = desired.AddActions(i.getTenantGone(state))
			// remove Tenant operator next
			desired = desired.AddAction(i.getDeploymentDesiredState(state.GetClusterState(), cr))
			// block until tenant-operator is removed
			desired = desired.AddActions(i.getTenantReadiness(state))

			desired = desired.AddActions(i.getRBACDesiredState(cr))
			desired = desired.AddAction(i.getServiceAccountDesiredState(state.GetClusterState(), cr))
			// remove namespace last
			desired = desired.AddAction(i.getNamespaceDesiredState(cr))
			// block until namespace is removed
			desired = desired.AddActions(i.getNamespaceGone(state))
		}
	} else {
		// create the namespace first
		desired = desired.AddAction(i.getNamespaceDesiredState(cr))
		desired = desired.AddActions(i.getRBACDesiredState(cr))
		desired = desired.AddAction(i.getServiceAccountDesiredState(state.GetClusterState(), cr))
		desired = desired.AddActions(i.getClickHouseDesiredState(state, cr))
		desired = desired.AddAction(i.getDeploymentDesiredState(state.GetClusterState(), cr))
		desired = desired.AddActions(i.getTenantReadiness(state))
		// reconcile tenant/group last once we know the operator is ready
		desired = desired.AddAction(i.getTenantDesiredState(state.GetClusterState(), cr))
		desired = desired.AddAction(i.getGroupDesiredState(state.GetClusterState(), cr))
	}

	return desired
}

func (i *GitLabNamespaceReconciler) getTenantReadiness(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Operator != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Operator,
					Msg: "check tenant-operator is gone",
				},
			}
		} else {
			return []common.Action{} // nothing to do
		}
	}
	return []common.Action{
		common.DeploymentReadyAction{
			Ref: state.Operator,
			Msg: "check tenant-operator readiness",
		},
	}
}

func (i *GitLabNamespaceReconciler) getGroupGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Group != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Group,
					Msg: "check group is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getTenantGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Tenant != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Tenant,
					Msg: "check tenant is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getNamespaceGone(state *GitLabNamespaceState) []common.Action {
	if i.Teardown {
		if state.Namespace != nil {
			return []common.Action{
				common.CheckGoneAction{
					Ref: state.Namespace,
					Msg: "check namespace is gone",
				},
			}
		}
	}
	return []common.Action{}
}

func (i *GitLabNamespaceReconciler) getRBACDesiredState(cr *v1alpha1.GitLabNamespace) []common.Action {
	objects, err := GetRBACObjects(cr)
	if err != nil {
		return []common.Action{common.LogAction{
			Msg:   "failed to serialize tenant rbac resources",
			Error: err,
		}}
	}
	actions := []common.Action{}
	for _, obj := range objects {
		obj := obj
		//nolint:errcheck
		desired := obj.DeepCopyObject().(client.Object)

		if i.Teardown {
			actions = append(actions, common.GenericDeleteAction{
				Ref: obj,
				Msg: fmt.Sprintf("tenant %s", obj.GetObjectKind().GroupVersionKind().Kind),
			})
		} else {
			actions = append(actions, common.GenericCreateOrUpdateAction{
				Ref: obj,
				Msg: fmt.Sprintf("tenant %s", obj.GetObjectKind().GroupVersionKind().Kind),
				// Don't want a GitlabNamespace CR to own the whole this because multiple
				// GitlabNamespace CRs map to the same resource
				SkipOwnerRef: true,
				Mutator: func() error {
					return common.RBACObjectMutator(obj, desired)
				},
			})
		}
	}

	return actions
}

func (i *GitLabNamespaceReconciler) getNamespaceDesiredState(cr *v1alpha1.GitLabNamespace) common.Action {
	ns := Namespace(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: ns,
			Msg: "tenant namespace",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: ns,
		Msg: "tenant namespace",
		// Don't want a GitlabNamespace CR to own the whole namespace because multiple
		// GitlabNamespace CRs map to the same namespace
		SkipOwnerRef: true,
		Mutator: func() error {
			return NamespaceMutator(cr, ns)
		},
	}
}

func (i *GitLabNamespaceReconciler) getClickHouseDesiredState(
	state *GitLabNamespaceState,
	cr *v1alpha1.GitLabNamespace,
) []common.Action {
	desired := []common.Action{}
	return desired
}

func (i *GitLabNamespaceReconciler) getServiceAccountDesiredState(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	sa := ServiceAccount(clusterConfig, cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: sa,
			Msg: "tenant service account",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: sa,
		Msg: "tenant service account",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return ServiceAccountMutator(clusterConfig, cr, sa)
		},
	}
}

func (i *GitLabNamespaceReconciler) getGroupDesiredState(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	group := Group(clusterConfig, cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: group,
			Msg: "group cr",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: group,
		Msg: "group cr",
		Mutator: func() error {
			return GroupMutator(clusterConfig, cr, group)
		},
	}
}

func (i *GitLabNamespaceReconciler) getTenantDesiredState(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	tenant := Tenant(clusterConfig, cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: tenant,
			Msg: "tenant cr",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: tenant,
		Msg: "tenant cr",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return TenantMutator(clusterConfig, cr, tenant)
		},
	}
}

func (i *GitLabNamespaceReconciler) getDeploymentDesiredState(
	clusterConfig *v1alpha1.Cluster,
	cr *v1alpha1.GitLabNamespace,
) common.Action {
	deploy := Deployment(cr)

	if i.Teardown {
		return common.GenericDeleteAction{
			Ref: deploy,
			Msg: "tenant-operator deployment",
		}
	}

	return common.GenericCreateOrUpdateAction{
		Ref: deploy,
		Msg: "tenant-operator deployment",
		// Don't want a GitlabNamespace CR to own the whole this because multiple
		// GitlabNamespace CRs map to the same resource
		SkipOwnerRef: true,
		Mutator: func() error {
			return DeploymentMutator(clusterConfig, cr, deploy)
		},
	}
}
