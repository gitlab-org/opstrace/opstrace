package tenant

import "go.opentelemetry.io/otel/trace"

type BaseReconciler struct {
	reconcilerName string
	tracer         trace.Tracer
	tracingEnabled bool
}

func (b *BaseReconciler) setupTracing(t trace.Tracer) {
	if t != nil {
		b.tracer = t
		b.tracingEnabled = true
	}
}
