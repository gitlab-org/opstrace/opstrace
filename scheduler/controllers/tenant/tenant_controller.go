package tenant

import (
	"context"
	"errors"
	"net/http"
	"time"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	"github.com/fluxcd/pkg/runtime/patch"
	"github.com/fluxcd/pkg/ssa"
	"github.com/go-logr/logr"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	apimeta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

const (
	finalizerName = "gitlabobservabilitytenant.opstrace.com/finalizer"
)

type ReconcileTenant struct {
	Client                  client.Client
	Scheme                  *runtime.Scheme
	Transport               *http.Transport
	Recorder                record.EventRecorder
	Log                     logr.Logger
	InitialManifests        map[string]map[string][]byte
	StatusPoller            *polling.StatusPoller
	DriftPreventionInterval time.Duration
	LogLevel                string
	Tracer                  trace.Tracer
}

var _ reconcile.Reconciler = &ReconcileTenant{}

var errMigrationsNotApplied = errors.New("necessary migrations not applied yet")

// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabobservabilitytenant,verbs=get;list;watch
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabobservabilitytenant/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=opstrace.com,resources=gitlabobservabilitytenant/finalizers,verbs=update

func (r *ReconcileTenant) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		WithOptions(controller.Options{MaxConcurrentReconciles: 10}).
		For(&opstracev1alpha1.GitLabObservabilityTenant{}).
		Complete(r)
}

//nolint:funlen,cyclop
func (r *ReconcileTenant) Reconcile(
	ctx context.Context,
	request reconcile.Request,
) (result ctrl.Result, err error) {
	reconcileCtx, reconcileSpan := r.Tracer.Start(ctx, "tenant#Reconcile")
	defer reconcileSpan.End()

	tenant := &opstracev1alpha1.GitLabObservabilityTenant{}
	err = r.Client.Get(ctx, request.NamespacedName, tenant)
	if err != nil {
		if apierrors.IsNotFound(err) {
			r.Log.Info("Tenant has been removed from the API", "name", request.Name)
			return reconcile.Result{}, nil
		}
		return reconcile.Result{}, err
	}

	cr := tenant.DeepCopy()

	// Initialize the runtime patcher with the current version of the object.
	patcher := patch.NewSerialPatcher(cr, r.Client)

	// Finalize the reconciliation
	defer func() {
		// Configure the runtime patcher.
		patchOpts := []patch.Option{
			patch.WithFieldOwner(constants.GitLabObservabilityTenantFieldManagerIDString),
		}

		// Patch the object status, conditions and finalizers.
		if patchErr := patcher.Patch(ctx, cr, patchOpts...); patchErr != nil {
			if !cr.GetDeletionTimestamp().IsZero() {
				patchErr = kerrors.FilterOut(patchErr, apierrors.IsNotFound)
			}
			err = kerrors.NewAggregate([]error{err, patchErr})
		}
	}()

	// Add finalizer first if it doesn't exist to avoid the race condition
	// between init and delete.
	if !controllerutil.ContainsFinalizer(cr, finalizerName) {
		controllerutil.AddFinalizer(cr, finalizerName)
		return ctrl.Result{Requeue: true}, nil
	}

	if cr.Status.Inventory == nil {
		cr.Status.Inventory = make(map[string]*v1beta2.ResourceInventory)
	}

	// Read current state
	currentState := NewGitLabObservabilityTenantState()
	err = currentState.Read(ctx, cr, r.Client)
	if err != nil {
		r.Log.Error(err, "error reading state")
		r.manageError(cr, err)
		return reconcile.Result{}, err
	}

	// Check if the scheduler managed to apply all necessary migrations successfully, abort
	// early here and requeue for later otherwise
	var migrationsApplied bool
	clusterStatus := currentState.Cluster.Status
	if clusterStatus.LastMigrationApplied != nil {
		migrationsApplied = (*clusterStatus.LastMigrationApplied == constants.DockerImageTag)
	}
	if !migrationsApplied {
		r.Log.Error(
			errMigrationsNotApplied,
			"error checking migrations",
			"fromClusterStatus", clusterStatus.LastMigrationApplied,
			"dockerTag", constants.DockerImageTag,
		)
		r.manageError(cr, errMigrationsNotApplied)
		return reconcile.Result{RequeueAfter: r.DriftPreventionInterval}, nil
	}

	var (
		isCloudDSNprovided     = currentState.CloudCHCredentials != nil
		cloudMigrationsApplied bool
	)
	if clusterStatus.LastCloudMigrationApplied != nil {
		cloudMigrationsApplied = *clusterStatus.LastCloudMigrationApplied == constants.DockerImageTag
	}
	if isCloudDSNprovided && !cloudMigrationsApplied {
		r.Log.Error(
			errMigrationsNotApplied,
			"error checking cloud migrations",
			"fromClusterStatus", clusterStatus.LastCloudMigrationApplied,
			"dockerTag", constants.DockerImageTag,
		)
		r.manageError(cr, errMigrationsNotApplied)
		return reconcile.Result{RequeueAfter: r.DriftPreventionInterval}, nil
	}

	teardown := !cr.ObjectMeta.DeletionTimestamp.IsZero()
	desiredState := r.getDesiredState(reconcileCtx, teardown, cr, currentState)

	// Create the server-side apply manager.
	resourceManager := ssa.NewResourceManager(r.Client, r.StatusPoller, ssa.Owner{
		Field: constants.GitLabObservabilityTenantFieldManagerIDString,
		Group: cr.GetObjectKind().GroupVersionKind().Group,
	})

	// Run the actions to reach the desired state
	actionRunner := common.NewActionRunner(ctx, r.Client, r.Scheme, cr, resourceManager, r.Tracer)
	err = actionRunner.RunAll(reconcileCtx, desiredState)
	if err != nil {
		reconcileSpan.RecordError(err)
		reconcileSpan.SetStatus(codes.Error, "running all actions to reach desired state")
		r.Log.Error(err, "error executing action")
		r.manageError(cr, err)
		return reconcile.Result{RequeueAfter: constants.RequeueDelay}, nil
	}

	if teardown {
		if controllerutil.ContainsFinalizer(cr, finalizerName) {
			// Successfully deleted everything we care about.
			// Remove our finalizer from the list and update it
			controllerutil.RemoveFinalizer(cr, finalizerName)
		}
	} else {
		r.manageSuccess(cr)
	}

	return reconcile.Result{RequeueAfter: r.DriftPreventionInterval}, nil
}

func (r *ReconcileTenant) getDesiredState(
	ctx context.Context,
	teardown bool,
	cr *opstracev1alpha1.GitLabObservabilityTenant,
	currentState *GitLabObservabilityTenantState,
) common.DesiredState {
	var operation string
	if teardown {
		operation = "deprovisioning"
	} else {
		operation = "provisioning"
	}

	gdsCtx, gdsSpan := r.Tracer.Start(
		ctx,
		"tenant#getDesiredState",
		trace.WithAttributes([]attribute.KeyValue{
			{Key: "operation", Value: attribute.StringValue(operation)},
			{Key: "tenant_id", Value: attribute.Int64Value(cr.Spec.TopLevelNamespaceID)},
		}...),
	)
	defer gdsSpan.End()

	desiredState := common.DesiredState{}
	tenantID := cr.Spec.TopLevelNamespaceID

	if teardown {
		// OTEL collector
		otelcollector := NewOTELCollectorReconciler(
			r.Tracer,
			r.InitialManifests[constants.OtelCollectorComponentName],
			teardown,
			tenantID,
			currentState,
		)
		desiredState = append(desiredState, otelcollector.Reconcile(gdsCtx, cr)...)
		// CH credentials
		clickhouse := NewClickHouseReconciler(r.Tracer, teardown, currentState, tenantID)
		desiredState = append(desiredState, clickhouse.Reconcile(gdsCtx, cr)...)
		// Tenant namespace
		namespace := NewNamespaceReconciler(
			r.Tracer,
			r.InitialManifests[constants.OtelCollectorNamespaceComponentName],
			teardown,
			tenantID,
		)
		desiredState = append(desiredState, namespace.Reconcile(gdsCtx, cr)...)
	} else {
		// Tenant namespace
		namespace := NewNamespaceReconciler(
			r.Tracer,
			r.InitialManifests[constants.OtelCollectorNamespaceComponentName],
			teardown,
			tenantID,
		)
		desiredState = append(desiredState, namespace.Reconcile(gdsCtx, cr)...)
		// CH credentials
		clickhouse := NewClickHouseReconciler(r.Tracer, teardown, currentState, tenantID)
		desiredState = append(desiredState, clickhouse.Reconcile(gdsCtx, cr)...)
		// OTEL collector
		otelcollector := NewOTELCollectorReconciler(
			r.Tracer,
			r.InitialManifests[constants.OtelCollectorComponentName],
			teardown,
			tenantID,
			currentState,
		)
		desiredState = append(desiredState, otelcollector.Reconcile(gdsCtx, cr)...)
	}

	return desiredState
}

// Handle success case
func (r *ReconcileTenant) manageSuccess(tenant *opstracev1alpha1.GitLabObservabilityTenant) {
	condition := metav1.Condition{
		Status:             metav1.ConditionTrue,
		Reason:             common.ReconciliationSuccessReason,
		Message:            "All components are in ready state",
		Type:               common.ConditionTypeReady,
		ObservedGeneration: tenant.GetGeneration(),
	}
	apimeta.SetStatusCondition(&tenant.Status.Conditions, condition)
	r.Log.Info("tenant successfully reconciled", "tenant", tenant.Name)
}

// Handle error case: update tenant with error message and status
func (r *ReconcileTenant) manageError(tenant *opstracev1alpha1.GitLabObservabilityTenant, issue error) {
	condition := metav1.Condition{
		Status:             metav1.ConditionFalse,
		Reason:             common.ReconciliationFailedReason,
		Message:            issue.Error(),
		Type:               common.ConditionTypeReady,
		ObservedGeneration: tenant.GetGeneration(),
	}
	apimeta.SetStatusCondition(&tenant.Status.Conditions, condition)
}
