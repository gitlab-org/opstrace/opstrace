include ../Makefile.operator.mk

DOCKER_IMAGE_NAME ?= scheduler

.PHONY: unit-tests
unit-tests: test

# Build manager binary
build: manifests generate fmt vet
	go build -ldflags "$(GO_BUILD_LDFLAGS)" -o bin/manager main.go

kind:
	@# kind cluster without default kindnet CNI installed (see kind.yaml)
	kind create cluster --image=$(KIND_IMAGE) --config=kind.yaml
	@# install calico to provide network policies
	kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.1/manifests/calico.yaml
	@# turn off calico RPF check that is not supported with KIND nodes
	kubectl -n kube-system set env daemonset/calico-node FELIX_IGNORELOOSERPF=true
	kubectl -n kube-system rollout status daemonset/calico-node -w
	@# optionally install telepresence with options to match custom podCIDR settings (cannot auto detect this)
	-telepresence helm install --set podCIDRs=192.168.0.0/16,podCIDRStrategy=environment

kind-delete:
	kind delete cluster --name opstrace

# Run against the configured Kubernetes cluster in ~/.kube/config
# Ensure install & deploy-without-manager is run once first.
run: manifests generate fmt vet
	@echo "--- requires telepresence for cluster DNS routing (e.g. connections to ClickHouse)"
	telepresence connect
	go run -ldflags "$(GO_BUILD_LDFLAGS)" ./main.go

# Install CRDs into a cluster for the first time (https://medium.com/pareture/kubectl-install-crd-failed-annotations-too-long-2ebc91b40c7d)
install: manifests
	kubectl create -k config/crd || kubectl replace -k config/crd
	@echo "installing tenant-operator"
	cd ../tenant-operator && $(MAKE) install
	@echo "installing clickhouse-operator"
	cd ../clickhouse-operator && $(MAKE) install

# Uninstall CRDs from a cluster
uninstall: manifests
	kubectl delete -k config/crd
	@echo "uninstalling tenant-operator"
	cd ../tenant-operator && $(MAKE) uninstall
	@echo "uninstalling clickhouse-opeartor"
	cd ../clickhouse-operator && $(MAKE) uninstall


# variables for example dev secrets (see ./config/examplesecrets)
# Recommend using a .env file for these with https://direnv.net/
export gitlab_oauth_client_id ?= somesecret
export gitlab_oauth_client_secret ?= somesecret
export internal_endpoint_token ?= somesecret
export gitlab_otel_endpoint ?= https://fakeplaceholder.com
export gitlab_otel_endpoint_private_token ?= somesecret
export oidc_private_key_pem ?=

# Deploy everything in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests dev-secret
	kubectl apply -k config/rate-limits
	kubectl apply -k config/deploy


# UnDeploy everything from the configured Kubernetes cluster in ~/.kube/config
undeploy:
	kubectl delete -k config/deploy
	kubectl delete -k config/rate-limits
	kubectl delete -k config/examplesecrets

# Deploy all resources except controller
deploy-without-manager: manifests
	kubectl apply -k config/deploy-without-manager

# UnDeploy all resources
undeploy-without-manager:
	kubectl delete -k config/deploy-without-manager

# Apply the example Cluster and OpstraceNamespace resources.
# These work with the group https://gitlab.com/gitlab-org/opstrace assuming you have
# the correct OAuth credentials set up.
.PHONY: dev-namespace
dev-namespace:
	kubectl apply -k config/examples

# Apply the example Dev secret to be used by a Cluster CR.
# This is used to configure the GitLab OAuth client ID and secret and
# the internal endpoint token.
# Set the following environment variables before running this:
# gitlab_oauth_client_id
# gitlab_oauth_client_secret
# internal_endpoint_token
# oidc_private_key_pem
.PHONY: dev-secret
dev-secret:
	kubectl apply -k config/examplesecrets

# Generate manifests e.g. CRD, RBAC etc.
manifests:
	$(CONTROLLER_GEN) $(CRD_OPTIONS) rbac:roleName=manager-role crd:maxDescLen=0 webhook paths="./..." output:crd:artifacts:config=config/crd/bases

docker-ensure: docker-ensure-default

docker: docker-build docker-push

docker-build:
	docker build \
		--build-arg GO_BUILD_LDFLAGS \
		-f Dockerfile \
		-t ${DOCKER_IMAGE} \
			..

docker-push:
	docker push ${DOCKER_IMAGE}

.PHONY: print-docker-images
print-docker-images: print-docker-image-name-tag
