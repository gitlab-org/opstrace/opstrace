package v1alpha1

import (
	"fmt"

	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type GitLabObservabilityTenantSpec struct {
	// top level namespace in GitLab to which this tenant belongs
	TopLevelNamespaceID int64 `json:"topLevelNamespaceID"`
	// cluster to which this tenant belongs
	// +optional
	Cluster *string `json:"cluster,omitempty"`
	// set overrides for all tenant components and their configuration
	// +optional
	Overrides GitLabObservabilityTenantOverridesSpec `json:"overrides,omitempty"`
}

type GitLabObservabilityTenantOverridesSpec struct {
	// +optional
	OTELCollector *KustomizeOverridesSpec `json:"otelcollector,omitempty"`
}

type GitLabObservabilityTenantStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`

	// Inventory contains the list of Kubernetes resource object references that have been successfully applied.
	// +optional
	Inventory map[string]*v1beta2.ResourceInventory `json:"inventory,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:scope="Cluster"

// GitLabObservabilityTenant is the Schema for the GitLabObservabilityTenants API.
type GitLabObservabilityTenant struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   GitLabObservabilityTenantSpec   `json:"spec,omitempty"`
	Status GitLabObservabilityTenantStatus `json:"status,omitempty"`
}

func (t *GitLabObservabilityTenant) Namespace() string {
	return fmt.Sprintf("tenant-%d", t.Spec.TopLevelNamespaceID)
}

//+kubebuilder:object:root=true

// GitLabObservabilityTenantList contains a list of GitLabObservabilityTenant.
type GitLabObservabilityTenantList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []GitLabObservabilityTenant `json:"items"`
}

func init() {
	SchemeBuilder.Register(&GitLabObservabilityTenant{}, &GitLabObservabilityTenantList{})
}
