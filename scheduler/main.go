/*
Copyright 2021.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//nolint:gochecknoinits
package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"time"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap/zapcore"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling"
	"sigs.k8s.io/cli-utils/pkg/kstatus/polling/engine"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	"sigs.k8s.io/controller-runtime/pkg/webhook"

	"sigs.k8s.io/controller-runtime/pkg/log"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	apis "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster"
	clustermanifests "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/cluster/manifests"
	ctrlcommon "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/tenant"
	tenantmanifests "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/tenant/manifests"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/version"
	metricsserver "sigs.k8s.io/controller-runtime/pkg/metrics/server"

	_ "go.uber.org/automaxprocs"

	// Import all Kubernetes client auth plugins (e.g. Azure, GCP, OIDC, etc.)
	// to ensure that exec-entrypoint and run can make use of them.

	_ "k8s.io/client-go/plugin/pkg/client/auth"

	k8sruntime "k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"

	opstracev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
	"gitlab.com/gitlab-org/opstrace/opstrace/scheduler/controllers/namespace"
	// +kubebuilder:scaffold:imports
)

var (
	scheme                  = k8sruntime.NewScheme()
	setupLog                = ctrl.Log.WithName("setup")
	metricsAddr             string
	enableLeaderElection    bool
	probeAddr               string
	driftPreventionInterval time.Duration
	configFile              string
	err                     error
	logLevel                string
	zapOpts                 zap.Options

	otlpEndpoint        string
	otlpCACertificate   string
	otlpTokenSecretFile string
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(opstracev1alpha1.AddToScheme(scheme))
	// +kubebuilder:scaffold:scheme
}

func printVersion() {
	log.Log.Info(fmt.Sprintf("Go Version: %s", runtime.Version()))
	log.Log.Info(fmt.Sprintf("Go OS/Arch: %s/%s", runtime.GOOS, runtime.GOARCH))
	log.Log.Info(fmt.Sprintf("operator Version: %v", version.Version))
}

func assignOpts() {
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":7070", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":7071", "The address the probe endpoint binds to.")
	flag.DurationVar(
		&driftPreventionInterval,
		"drift-prevention-interval",
		constants.DefaultDriftPreventionInterval,
		"How often should the controller scan for drift (e.g. manual changes) in objects it created.",
	)
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	flag.StringVar(&configFile, "config", "",
		"The controller will load its initial configuration from this file. "+
			"Omit this flag to use the default configuration values. "+
			"Command-line flags override configuration from this file.")

	flag.StringVar(&otlpEndpoint,
		"otlp-endpoint", "", "otlp collector endpoint, without `/1/traces` or `/v1/metrics` suffix")
	flag.StringVar(&otlpCACertificate,
		"otlp-ca-cert", "", "otlp CA certificate to use while connecting over HTTPs")
	flag.StringVar(&otlpTokenSecretFile,
		"otlp-token-file", "", "file that contains Access Token to use when connecting to otel collector")

	zapOpts = zap.Options{
		Development: true,
		TimeEncoder: zapcore.ISO8601TimeEncoder,
	}
	zapOpts.BindFlags(flag.CommandLine)
	flag.Parse()
}

func main() {
	os.Exit(run()) // allow run() to run its deferred statements
}

//nolint:funlen,cyclop
func run() int {
	printVersion()
	assignOpts()

	ctrlConfig := opstracev1alpha1.ProjectConfig{}
	options := ctrl.Options{
		Scheme: scheme,
		Metrics: metricsserver.Options{
			BindAddress: metricsAddr,
		},
		WebhookServer: webhook.NewServer(webhook.Options{
			Port: 9443,
		}),
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "2c0156f1.opstrace.com",
	}

	if configFile != "" {
		//nolint:staticcheck // SA1019 TODO(prozlach): we need to remove it or write our own.
		options, err = options.AndFrom(ctrl.ConfigFile().AtPath(configFile).OfKind(&ctrlConfig))
		if err != nil {
			fmt.Printf("msg=unable to load the config file - exiting, error=%v\n", err)
			return 1
		}
	}

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), options)
	if err != nil {
		fmt.Printf("msg=unable to start manager - exiting, error=%v\n", err)
		return 1
	}

	if logLevel = string(ctrlConfig.LogLevel); logLevel == "" {
		logLevel = "info"
	}
	zapOpts.Level, err = zapcore.ParseLevel(logLevel)
	if err != nil {
		fmt.Printf("msg=unable to parse log level - exiting, error=%v\n", err)
		return 1
	}

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&zapOpts)))

	ctx := context.Background()
	resource, err := instrumentation.ConstructOTELResource(
		ctx,
		"scheduler",
		constants.DockerImageTag,
	)
	if err != nil {
		log.Log.Error(err, "defining otel resource failed")
		return 1
	}

	log.Log.Info("constructing tracer...")
	tracerProvider, _, traceProviderDoneF, err := instrumentation.ConstructOTELTracingTools(
		ctx,
		resource,
		otlpEndpoint,
		otlpCACertificate,
		otlpTokenSecretFile,
	)
	if err != nil {
		log.Log.Error(err, "constructing otel trace provider failed")
		return 1
	}
	defer func() {
		err := traceProviderDoneF()
		if err != nil {
			log.Log.Error(err, "shutting down trace provider failed")
		}
	}()
	otel.SetTracerProvider(tracerProvider)

	log.Log.Info("Registering Components")

	// Setup Scheme for all resources
	if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
		log.Log.Error(err, "setting up manager scheme")
		return 1
	}

	log.Log.Info("Processing embedded manifests...")
	log.Log.Info("Loading initial manifests for cluster")
	initialClusterManifests, err := ctrlcommon.InitialManifests(clustermanifests.Upstream)
	if err != nil {
		log.Log.Error(err, "error occurred while parsing embedded manifests for the cluster")
		return 1
	}
	log.Log.Info("Loading initial manifests for tenants")
	initialTenantManifests, err := ctrlcommon.InitialManifests(tenantmanifests.Upstream)
	if err != nil {
		log.Log.Error(err, "error occurred while parsing embedded manifests for the tenants")
		return 1
	}

	log.Log.Info("Starting reconcilers")

	jobStatusReader := common.NewCustomJobStatusReader(mgr.GetRESTMapper())
	pollingOpts := polling.Options{
		CustomStatusReaders: []engine.StatusReader{jobStatusReader},
	}

	if err = (&cluster.ReconcileCluster{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
		Log:    ctrl.Log.WithName("controllers").WithName("Cluster"),
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				//nolint:gosec
				InsecureSkipVerify: true,
			},
		},
		Recorder:                mgr.GetEventRecorderFor("Cluster"),
		InitialManifests:        initialClusterManifests,
		StatusPoller:            polling.NewStatusPoller(mgr.GetClient(), mgr.GetRESTMapper(), pollingOpts),
		DriftPreventionInterval: driftPreventionInterval,
		Tracer: otel.Tracer(
			"cluster-reconciler",
			trace.WithInstrumentationVersion(constants.DockerImageTag),
		),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Cluster")
		return 1
	}

	if err = (&namespace.ReconcileGitLabNamespace{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
		Log:    ctrl.Log.WithName("controllers").WithName("GitLabNamespace"),
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				//nolint:gosec
				InsecureSkipVerify: true,
			},
		},
		Recorder:     mgr.GetEventRecorderFor("GitLabNamespace"),
		StatusPoller: polling.NewStatusPoller(mgr.GetClient(), mgr.GetRESTMapper(), pollingOpts),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "GitLabNamespace")
		return 1
	}
	// +kubebuilder:scaffold:builder

	tenantReconciler := tenant.ReconcileTenant{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
		Log:    ctrl.Log.WithName("controllers").WithName("GitLabObservabilityTenant"),
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				//nolint:gosec
				InsecureSkipVerify: true,
			},
		},
		Recorder:                mgr.GetEventRecorderFor("GitLabObservabilityTenant"),
		InitialManifests:        initialTenantManifests,
		StatusPoller:            polling.NewStatusPoller(mgr.GetClient(), mgr.GetRESTMapper(), pollingOpts),
		DriftPreventionInterval: 30 * time.Second,
		Tracer: otel.Tracer(
			"tenant-reconciler",
			trace.WithInstrumentationVersion(constants.DockerImageTag),
		),
	}
	if err = (&tenantReconciler).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "GitLabObservabilityTenant")
		return 1
	}
	// +kubebuilder:scaffold:builder

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		return 1
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		return 1
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		return 1
	}

	return 0
}
