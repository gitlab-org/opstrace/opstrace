# Tracing

Tracing allows developers to ingest, view & discover the distributed traces that their application may be generating during their execution.

## Getting Started


### Feature Flags

The following feature flags must be enabled to utilize tracing on gitlab.com in its current state:

Project level feature flag where traces are to be stored/viewed (enables tracing for this project):

```bash
/chatops run feature set --project=<your_project> observability_tracing true
```

### Enable Tracing

Tracing is enabled at the top level namespace on gitlab.com. So if you haven't enabled tracing for your namespace,
then navigate to your project > Monitor > Tracing and click the enable button. You will only have to do this once
for the entire top level namespace.

### Auth Token
<!-- markdownlint-disable MD044 -->
From the project that was enabled above, navigate to the parent group. For the [gitlab.com/gitlab-org/opstrace/opstrace](https://gitlab.com/gitlab-org/opstrace/opstrace) project,
<!-- markdownlint-disable MD044 -->
the parent group is [gitlab.com/gitlab-org/opstrace](https://gitlab.com/gitlab-org/opstrace).

Next, navigate to Settings > Access Tokens. Create a new access token
with the following scopes:

* read_api
* read_observability
* write_observability

Set the value of the token to the following ENV variable in your terminal:

```bash
export PRIVATE_TOKEN_HEADER=private-token=<your_access_token>
```

### Tryout Tracing Project

Take a look at [tryout-tracing](https://gitlab.com/gitlab-org/opstrace/tryout-tracing) for a simple Rails CRM that can send traces to your project.

### Send Traces Demo

Set the following ENV variables in the same terminal where the PRIVATE_TOKEN_HEADER variable above is set:
<!-- markdownlint-disable MD044 -->
For the [gitlab.com/gitlab-org/opstrace/opstrace](https://gitlab.com/gitlab-org/opstrace/opstrace) project:

<your_project_id> is 32149347 and can be found under the project title on the project overview page
<your_top_level_group_id> is 9970 and can be found under the namespace title on the top level namespace overview page, i.e. [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org) in the case of this project.

```bash
export GITLAB_O11Y_ADDRESS=https://observe.gitlab.com
export PROJECT_ID=<your_project_id>
export NAMESPACE_ID=<your_top_level_group_id>
```

With these ENV vars set, the [following docker compose file](docker-compose.yaml) can be used to run a demo application that will generate
and send traces to your project:

```yaml
version: "2"
services:
  hotrod:
    image: registry.gitlab.com/gitlab-org/opstrace/opstrace/hotrod-demo:latest
    ports:
      - "8080:8080"
    command: ["all"]
    environment:
      - OTEL_EXPORTER_OTLP_TRACES_ENDPOINT=$GITLAB_O11Y_ADDRESS/v3/$NAMESPACE_ID/$PROJECT_ID/ingest/traces
      - OTEL_EXPORTER_OTLP_TRACES_HEADERS=$PRIVATE_TOKEN_HEADER
      - OTEL_EXPORTER="otlphttp"

```

Either use the linked docker-compose.yaml above or copy/paste the above snippet into your own docker-compose.yaml file.

**NOTE**: When executing docker-compose on Mac, you may find some docker alternatives like Podman don't pull the correct image arch.
In this case, you can pull the image before running docker-compose and the local image will be used.

```bash
docker pull --arch=arm64 registry.gitlab.com/gitlab-org/opstrace/opstrace/hotrod-demo:latest
```

Run the docker-compose services by executing the following from within the directory where the docker-compose.yaml resides:

```bash
docker compose up

# or for V1 docker users

docker-compose up
```

Now you can navigate to [http://localhost:8080](http://localhost:8080) and click some buttons to generate and send traces to your project.

![hotrod_screenshot](./hotrod_app.png)

Navigate to your GitLab project and view the traces under Monitor > Tracing:

![traces_screenshot](./traces.png)

Click on a trace to see the trace detail:

![trace](./trace.png)
