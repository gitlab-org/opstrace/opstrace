# Docs

The docs are split into these sections:

* Architecture
* Guides
  * Administrator
  * Contributor
  * User

## Getting Started

First, give our [Quick Start](./quickstart.md) a try.
It takes about half an hour to spin up the instance, but it's a great way to get a feel for how it works.
Furthermore, you don't often need to set up instances since once it's up and running it manages itself.

Missing something? Check out our [contributing guide](../CONTRIBUTING.md), thanks for your contributions!
