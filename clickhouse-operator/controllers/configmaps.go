package controllers

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ctrl "sigs.k8s.io/controller-runtime"
)

func newClickHouseConfigMaps(
	req ctrl.Request,
	ch *clickHouseCluster,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	kr = append(kr, newConfigMap(
		req.Namespace,
		req.Name,
		ch.labels(),
		map[string]string{
			"config.xml":        ch.ConfigXML,
			"users.xml":         ch.UsersXML,
			"client-config.xml": ch.ClientConfigXML,
			"keeper.xml":        ch.KeeperXML,
		},
	))

	return kr
}

func newConfigMap(namespace string, name string, labels map[string]string,
	data map[string]string) *KubernetesResource {
	cm := &corev1.ConfigMap{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: cm,
		mutator: func() error {
			cm.Labels = labels
			cm.Data = data
			return nil
		},
	}
}
