package controllers

import (
	"strconv"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
)

//nolint:funlen
func newClickHouseServices(
	req ctrl.Request,
	ch *clickHouseCluster,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	clientPorts := []corev1.ServicePort{
		{
			Name:       "http",
			Port:       8123,
			TargetPort: intstr.FromString("http"),
		},
		{
			Name:       "client",
			Port:       9000,
			TargetPort: intstr.FromString("client"),
		},
	}

	nodePorts := []corev1.ServicePort{
		{
			Name:       "interserver",
			Port:       9009,
			TargetPort: intstr.FromString("interserver"),
		},
		// Improvement: Remove extra keeper port if SSL is enabled
		{
			Name:       "keeper",
			Port:       2181,
			TargetPort: intstr.FromString("keeper"),
		},
		{
			Name:       "raft",
			Port:       9444,
			TargetPort: intstr.FromString("raft"),
		},
		{
			Name:       "metrics",
			Port:       8001,
			TargetPort: intstr.FromString("metrics"),
		},
		{
			Name:       "keeper-metrics",
			Port:       7000,
			TargetPort: intstr.FromString("keeper-metrics"),
		},
	}

	if ch.InternalSSL {
		clientPorts = append(clientPorts, []corev1.ServicePort{
			{
				Name:       "https",
				Port:       8443,
				TargetPort: intstr.FromString("https"),
			},
			{
				Name:       "client-secure",
				Port:       9440,
				TargetPort: intstr.FromString("client-secure"),
			},
		}...)
		nodePorts = append(nodePorts, []corev1.ServicePort{
			{
				Name:       "int-serv-secure",
				Port:       9010,
				TargetPort: intstr.FromString("int-serv-secure"),
			},
			{
				Name:       "keeper-secure",
				Port:       9281,
				TargetPort: intstr.FromString("keeper-secure"),
			},
		}...)
	}

	nodePorts = append(nodePorts, clientPorts...)

	// Create shared "<name>" Service for access by clients
	clusterLabels := ch.labels()
	kr = append(kr, newService(
		req.Namespace,
		req.Name,
		clusterLabels,
		clientPorts,
		clusterLabels,
		"",
		false,
	))

	// Create per-node "host" Services to allow direct addressing between nodes.
	// Note(joe): headless services using PublishNotReadyAddresses=true to allow pre-ready DNS access.
	for i, h := range ch.Hosts {
		nodeLabels := ch.labelsMerged(map[string]string{
			"replica": strconv.Itoa(i),
		})
		kr = append(kr, newService(
			req.Namespace,
			h,
			nodeLabels,
			nodePorts,
			nodeLabels,
			"None",
			true,
		))
	}

	return kr
}

func newService(namespace string, name string, labels map[string]string,
	ports []corev1.ServicePort, selector map[string]string, clusterIP string,
	publishNotReadyAddresses bool) *KubernetesResource {
	svc := &corev1.Service{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: svc,
		mutator: func() error {
			svc.Labels = labels
			svc.Spec.Ports = ports
			svc.Spec.Selector = selector
			if len(clusterIP) > 0 {
				// Avoid triggering a service change if the spec ClusterIP is already assigned
				svc.Spec.ClusterIP = clusterIP
			}
			svc.Spec.PublishNotReadyAddresses = publishNotReadyAddresses
			return nil
		},
	}
}
