<clickhouse>
  <remote_servers>
    <{{ .Name }}>
      <shard>
        <internal_replication>true</internal_replication>
        {{- range .Hosts }}
        <replica>
          <host>{{ . }}</host>
          {{- if $.InternalSSL }}
          <port>9440</port>
          <secure>1</secure>
          {{- else }}
          <port>9000</port>
          {{- end }}
          <user>replica</user>
        </replica>
        {{- end }}
      </shard>
    </{{ .Name }}>
  </remote_servers>

  <macros>
    <cluster>{{ .Name }}</cluster>
    <shard>0</shard>
    <!-- Replica is full replica service name, not just index -->
    <replica from_env="HOST"/>
  </macros>

  <default_replica_name>{replica}</default_replica_name>
  <default_replica_path>/clickhouse/{cluster}/tables/{shard}/{database}/{table}/{uuid}</default_replica_path>
  <keeper_map_path_prefix>/keeper_map_tables</keeper_map_path_prefix>

  <!-- Listen wildcard address to allow accepting connections from other containers and host network. -->
  <listen_host>::</listen_host>
  <listen_host>0.0.0.0</listen_host>
  {{- if .InternalSSL }}
  <!-- Not disabling all insecure port currently (http/tcp) -->
  <https_port>8443</https_port>
  <tcp_port_secure>9440</tcp_port_secure>
  <interserver_https_port>9010</interserver_https_port>
  <interserver_http_port remove="1"/>
  {{- end }}
  <listen_try>1</listen_try>

  <logger>
    <!-- Possible levels: https://github.com/pocoproject/poco/blob/develop/Foundation/include/Poco/Logger.h#L105 -->
    <level>information</level>
    <log>/var/log/clickhouse-server/clickhouse-server.log</log>
    <errorlog>/var/log/clickhouse-server/clickhouse-server.err.log</errorlog>
    <size>1000M</size>
    <count>2</count>
    <!-- Default behavior is autodetection (log to console if not daemon mode and is tty) -->
    <console>false</console>
  </logger>

  <prometheus>
    <endpoint>/metrics</endpoint>
    <port>8001</port>
    <metrics>true</metrics>
    <events>true</events>
    <asynchronous_metrics>true</asynchronous_metrics>
  </prometheus>

  <query_log replace="1">
    <database>system</database>
    <table>query_log</table>
    <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
    <flush_interval_milliseconds>7500</flush_interval_milliseconds>
  </query_log>

  <query_thread_log remove="1"/>

  <part_log replace="1">
    <database>system</database>
    <table>part_log</table>
    <engine>Engine = MergeTree PARTITION BY event_date ORDER BY event_time TTL event_date + interval 30 day</engine>
    <flush_interval_milliseconds>7500</flush_interval_milliseconds>
  </part_log>

  <query_views_log replace="1">
    <table>query_views_log</table>
    <partition_by>toStartOfWeek(event_date)</partition_by>
    <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
  </query_views_log>

  <metric_log replace="1">
    <table>metric_log</table>
    <partition_by>toStartOfWeek(event_date)</partition_by>
    <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
  </metric_log>

  <asynchronous_metric_log replace="1">
    <table>asynchronous_metric_log</table>
    <partition_by>toStartOfWeek(event_date)</partition_by>
    <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
  </asynchronous_metric_log>

  <trace_log replace="1">
    <table>trace_log</table>
    <partition_by>toStartOfWeek(event_date)</partition_by>
    <ttl>toStartOfWeek(event_date) + INTERVAL 30 DAY DELETE</ttl>
  </trace_log>

  <zookeeper>
    <node>
      <host>127.0.0.1</host>
      {{- if .InternalSSL }}
      <port>9281</port>
      <secure>1</secure>
      {{- else }}
      <port>2181</port>
      {{- end }}
    </node>
    <session_timeout_ms>30000</session_timeout_ms>
    <operation_timeout_ms>10000</operation_timeout_ms>
  </zookeeper>
  <distributed_ddl>
    <path>/clickhouse/cluster/task_queue/ddl</path>
  </distributed_ddl>

  {{- if .InternalSSL }}
    {{- template "openssl.tmpl.xml" }}
  {{- end }}

  {{- if and (.ObjectStorageEnabled) (eq .Spec.ObjectStorage.Backend "S3") }}
  <storage_configuration>
    <disks>
      <disk_s3>
        <type>s3</type>
        <endpoint>{{- .Spec.ObjectStorage.EndpointURL }}</endpoint>
        <use_environment_credentials>false</use_environment_credentials>
        <access_key_id from_env="ACCESS_KEY_ID"/>
        <secret_access_key from_env="SECRET_ACCESS_KEY"/>
        <region from_env="STORAGE_REGION"/>
        <!-- <server_side_encryption_customer_key_base64 from_env="SSE_CUSTOMER_KEY"/> -->
        <connect_timeout_ms>10000</connect_timeout_ms>
        <request_timeout_ms>5000</request_timeout_ms>
        <retry_attempts>10</retry_attempts>
        <single_read_retries>4</single_read_retries>
        <min_bytes_for_seek>1000</min_bytes_for_seek>
        <metadata_path>/var/lib/clickhouse/disks/s3/</metadata_path>
        <skip_access_check>false</skip_access_check>
      </disk_s3>
      <s3_cache>
        <type>cache</type>
        <disk>disk_s3</disk>
        <path>/var/lib/clickhouse/disks/s3_cache/</path>
        <max_size>10Gi</max_size>
      </s3_cache>
    </disks>
    <policies>
      <policy_s3_only>
        <volumes>
          <volume_s3>
            <disk>s3_cache</disk>
          </volume_s3>
        </volumes>
      </policy_s3_only>
    </policies>
  </storage_configuration>
  {{- end }}
  {{- if and (.ObjectStorageEnabled) (eq .Spec.ObjectStorage.Backend "GCS") }}
  <storage_configuration> 
    <disks>
      <gcs>
        <type>s3</type>
        <endpoint>{{- .Spec.ObjectStorage.EndpointURL }}</endpoint>
        <access_key_id from_env="ACCESS_KEY_ID" />
        <secret_access_key from_env="SECRET_ACCESS_KEY" />
        <region from_env="STORAGE_REGION" />
        <metadata_path>/var/lib/clickhouse/disks/gcs/</metadata_path>
        <support_batch_delete>false</support_batch_delete>
      </gcs>
      <gcs_cache>
        <type>cache</type>
        <disk>gcs</disk>
        <path>/var/lib/clickhouse/disks/gcs_cache/</path>
        <max_size>10Gi</max_size>
      </gcs_cache>
    </disks>
    <policies>
      <gcs_main>
        <volumes>
          <main>
            <disk>gcs_cache</disk>
          </main>
          <default>
            <disk>default</disk>
            <!-- keep_free_space_bytes !-->
          </default>
        </volumes>
      </gcs_main>
    </policies>
  </storage_configuration>
  {{- end }}
</clickhouse>
