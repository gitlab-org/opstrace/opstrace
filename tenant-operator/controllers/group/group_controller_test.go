package group

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	groupName      = "test-group"
	groupName2     = "test-group-2"
	groupNamespace = "default"
	groupID2       = groupID + 1
)

var _ = Describe("group controller", Ordered, func() {
	domain := "example.com"
	var group *v1alpha1.Group
	var group2 *v1alpha1.Group

	BeforeEach(func() {
		group = &v1alpha1.Group{
			ObjectMeta: metav1.ObjectMeta{
				Name:      groupName,
				Namespace: groupNamespace,
			},
			Spec: v1alpha1.GroupSpec{
				Domain: &domain,
				ID:     groupID,
			},
		}
	})

	AfterEach(func() {
		Expect(k8sClient.Delete(ctx, group)).To(Succeed())

		Eventually(func(g Gomega) {

			err := k8sClient.Get(ctx, client.ObjectKeyFromObject(group), group)
			g.Expect(err).To(HaveOccurred())
			g.Expect(apierrors.IsNotFound(err)).To(BeTrue())
		}, timeout, interval).Should(Succeed())
	})

	Context("when creating a group", func() {
		It("should reconcile group's resources", func() {
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())

			verifyGroupReconciled(group)
		})
	})

	Context("when creating two groups", func() {

		BeforeEach(func() {
			group2 = &v1alpha1.Group{
				ObjectMeta: metav1.ObjectMeta{
					Name:      groupName2,
					Namespace: groupNamespace,
				},
				Spec: v1alpha1.GroupSpec{
					Domain: &domain,
					ID:     groupID2,
				},
			}
		})

		It("should reconcile both group's resources", func() {
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())
			Expect(k8sClient.Create(ctx, group2)).Should(Succeed())
			verifyGroupReconciled(group)
			verifyGroupReconciled(group2)

			Expect(k8sClient.Delete(ctx, group2)).To(Succeed())

			Eventually(func(g Gomega) {

				err := k8sClient.Get(ctx, client.ObjectKeyFromObject(group2), group2)
				g.Expect(err).To(HaveOccurred())
				g.Expect(apierrors.IsNotFound(err)).To(BeTrue())
			}, timeout, interval).Should(Succeed())
		})

		It("should reconcile one group after deleting the other one", func() {
			// Create the two groups
			Expect(k8sClient.Create(ctx, group)).Should(Succeed())
			Expect(k8sClient.Create(ctx, group2)).Should(Succeed())
			verifyGroupReconciled(group)
			verifyGroupReconciled(group2)
			// Delete group 2 and make sure it is deleted
			Expect(k8sClient.Delete(ctx, group2)).To(Succeed())
			Eventually(func(g Gomega) {
				err := k8sClient.Get(ctx, client.ObjectKeyFromObject(group2), group2)
				g.Expect(err).To(HaveOccurred())
				g.Expect(apierrors.IsNotFound(err)).To(BeTrue())
			}, timeout, interval).Should(Succeed())
			// Verify that group1 reconciles correctly
			verifyGroupReconciled(group)
		})
	})
})

func verifyGroupReconciled(group *v1alpha1.Group) {
	By("check resources are eventually created")

	Eventually(func(g Gomega) {
		checkGroup := &v1alpha1.Group{}
		g.Expect(k8sClient.Get(ctx, client.ObjectKeyFromObject(group), checkGroup)).To(Succeed())
		g.Expect(
			meta.IsStatusConditionTrue(checkGroup.Status.Conditions, common.ConditionTypeReady)).
			To(BeTrue())
	}, timeout, interval).Should(Succeed())
}
