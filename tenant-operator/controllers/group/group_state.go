package group

import (
	"context"
	"fmt"

	"sigs.k8s.io/controller-runtime/pkg/client"
	cr_client "sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/tenant-operator/api/v1alpha1"
)

type GroupState struct {
	Tenant *v1alpha1.Tenant
}

func NewGroupState() *GroupState {
	return &GroupState{}
}

func (i *GroupState) Read(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	if err := i.readTenantState(ctx, cr, client); err != nil {
		return err
	}
	return nil
}

func (i *GroupState) readTenantState(ctx context.Context, cr *v1alpha1.Group, client client.Client) error {
	key := cr_client.ObjectKey{
		Namespace: cr.Namespace,
		Name:      constants.TenantName,
	}
	i.Tenant = new(v1alpha1.Tenant)

	err := client.Get(ctx, key, i.Tenant)
	if err != nil {
		return fmt.Errorf("unable to fetch tenant bound to group %s/%s: %w", cr.Namespace, cr.Name, err)
	}
	return nil
}
