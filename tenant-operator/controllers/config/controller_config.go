package config

import (
	"sync"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
)

type ControllerConfig struct {
	mu             *sync.Mutex
	gatekeeperURL  string
	platformTarget common.EnvironmentTarget
}

var instance *ControllerConfig
var once sync.Once

func Get() *ControllerConfig {
	once.Do(func() {
		instance = &ControllerConfig{
			mu: &sync.Mutex{},
		}
	})
	return instance
}

// SetGatekeeperURL sets the gatekeeper endpoint for all controllers to consume.
func (c *ControllerConfig) SetGatekeeperURL(gatekeeperURL string) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.gatekeeperURL = gatekeeperURL
}

// GetGatekeeperURL returns the gatekeeper endpoint.
func (c *ControllerConfig) GetGatekeeperURL() string {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.gatekeeperURL
}

// SetPlatformTarget sets the gatekeeper endpoint for all controllers to consume.
func (c *ControllerConfig) SetPlatformTarget(platformTarget common.EnvironmentTarget) {
	c.mu.Lock()
	defer c.mu.Unlock()

	c.platformTarget = platformTarget
}

// GetPlatformTarget returns the gatekeeper endpoint.
func (c *ControllerConfig) GetPlatformTarget() common.EnvironmentTarget {
	c.mu.Lock()
	defer c.mu.Unlock()

	return c.platformTarget
}
