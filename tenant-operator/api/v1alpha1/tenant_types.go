package v1alpha1

import (
	"github.com/fluxcd/kustomize-controller/api/v1beta2"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type StatusPhase string

var (
	NoPhase          StatusPhase
	PhaseReconciling StatusPhase = "reconciling"
	PhaseFailing     StatusPhase = "failing"
)

const (
	localhost    = "localhost"
	localhostURL = "http://" + localhost
)

type TenantSpec struct {
	// domain which this tenant is running under, e.g. observe.gitlab.com
	Domain *string `json:"domain,omitempty"`
	// +optional
	ImagePullSecrets []v1.LocalObjectReference `json:"imagePullSecrets,omitempty"`
}

type TenantStatus struct {
	Conditions []metav1.Condition `json:"conditions,omitempty"`

	// Inventory contains the list of Kubernetes resource object references that have been successfully applied.
	// +optional
	Inventory map[string]*v1beta2.ResourceInventory `json:"inventory,omitempty"`
}

// GrafanaPlugin contains information about a single plugin.
// +k8s:openapi-gen=true
type GrafanaPlugin struct {
	// +kubebuilder:validation:Required
	Name string `json:"name"`
	// +kubebuilder:validation:Required
	Version string `json:"version"`
}

// Tenant is the Schema for the tenants API.
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
type Tenant struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TenantSpec   `json:"spec,omitempty"`
	Status TenantStatus `json:"status,omitempty"`
}

// TenantList contains a list of Tenant.
// +kubebuilder:object:root=true
type TenantList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Tenant `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Tenant{}, &TenantList{})
}
