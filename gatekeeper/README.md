# Gatekeeper

Gatekeeper is responsible for authentication/authorization in an GOB instance.
Gatekeeper uses the attached GitLab instance as the trusted IAM.

Gatekeeper also creates `GitLabNamespace` resources when new namespaces are being provisioned.

We use the [Gin HTTP framework](https://gin-gonic.com/docs/) for route handling.

## Developing

Prerequisites:

1. Create a [GitLab Application](https://docs.gitlab.com/ee/integration/oauth_provider.html) in your instance of choice.
2. Ensure you have a k8s cluster and appropriate config/context set up (from root, `make kind deploy` as an example).

Run:

```sh
cp .env.dev.example .env.dev
```

and configure `.env.dev` with the credentials from the GitLab Application, and the instance URL of the GitLab instance where the credentials were created.

Populate your shell environment:

```sh
source .env.dev
```

Start dependent services (redis):

```sh
docker compose up
```

And then run `gatekeeper`:

```sh
make run
```

Start developing!

## Remote Development against a running GOB Instance

The following command will launch the telepresence agent in the GOB and proxy all gatekeeper traffic inside the remote GOB kubernetes cluster to the instance running on your local machine. All outbound traffic from the local gatekeeper instance running on your local machine will be proxied through telepresence to the kubernetes cluster.

`cd` into the root `/gatekeeper` directory and run:

```sh
make intercept-run
```

This starts the `intercept` and passes all environment variables from the `gatekeeper` pod to our local binary.

Ctrl+C this command and the intercept will be removed, returning the cluster to its default state.

> Note: this starts a local redis via `docker compose up -d`.
> Redis sentinel used in GOB uses pod IPs directly, which doesn't work with telepresence.

## Error Handling

The gatekeeper routes don't show error pages explicitly.
We rely on Traefik to handle HTTP error codes call the gatekeeper `/error_pages` endpoint.

This uses the [error pages Traefik middlewhare](https://doc.traefik.io/traefik/middlewares/http/errorpages/).

To signal an error in code, use one of the gin context error functions, e.g. `ctx.AbortWithError(401, errors.New("Unauthorized"))`.

The error handling middleware will log the error for you.

Errors are designed to be rendered as JSON or HTML, depending on the request's `Accept` header
and should be compatible with iframe embedding.
