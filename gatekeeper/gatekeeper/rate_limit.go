package gatekeeper

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/alerts"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/instrumentation"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
)

const (
	rateLimiterKey     = "rateLimiter"
	topLevelGroupIDKey = "topLevelGroupID"
	alertWriterKey     = "alertWriter"
)

type LimitIDRateLimitHandlerFormParams struct {
	LimitID *string `json:"limit_id" form:"limit_id" binding:"omitempty,oneof=tr_write"`
}

// Middleware to set the ratelimiter in the Gin's context for downstream
// handlers..
func SetRateLimiter(rl ratelimiting.RateLimiter) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(rateLimiterKey, rl)
	}
}

// Helper to get the ratelimiter from the context.
func GetRateLimiter(ctx *gin.Context) ratelimiting.RateLimiter {
	return ctx.MustGet(rateLimiterKey).(ratelimiting.RateLimiter)
}

func SetAlertWriter(writer alerts.AlertWriter) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(alertWriterKey, writer)
	}
}

func GetAlertWriter(ctx *gin.Context) (any, bool) {
	return ctx.Get(alertWriterKey)
}

// RateLimitingHandler checks if the request that belongs to given `limitID`
// group is permitted/below the quota limits for given topLevelGroupID. The
// return value signifies whether the request was rate limited or not, in case
// e.g. extra headers need to be added.
func RateLimitingHandler() gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		rl := GetRateLimiter(ginCtx)
		var (
			alertWriter alerts.AlertWriter
			ok          bool
		)
		aw, exists := GetAlertWriter(ginCtx)
		if exists && aw != nil {
			alertWriter, ok = aw.(alerts.AlertWriter)
			if !ok {
				ginCtx.AbortWithError(500, fmt.Errorf("failed to assert alertwriter"))
				return
			}
		}

		var formParams LimitIDRateLimitHandlerFormParams
		if err := ginCtx.ShouldBindQuery(&formParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("uri parameters validation failed: %w", err))
			return
		}
		if formParams.LimitID == nil {
			// limit_id param was not specified, this means that the endpoint
			// is not rate-limited.
			return
		}
		limitID := *formParams.LimitID
		logger, _, _ := instrumentation.SetupLogger("INFO")

		ratelimiting.DoRateLimiting(ginCtx, logger.Desugar(), rl, limitID, alertWriter)
	}
}
