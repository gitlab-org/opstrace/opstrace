package gatekeeper

import (
	"context"
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	cache "github.com/go-redis/cache/v9"
	"github.com/hashicorp/golang-lru/v2/expirable"
	"github.com/prometheus/client_golang/prometheus"
	redis "github.com/redis/go-redis/v9"
)

const (
	CacheClientKey = "gatekeeper/redisCache"
)

type CacheOptions struct {
	RedisAddr          string
	RedisPassword      string
	ConnectionPoolSize int
	Registry           prometheus.Registerer
}

type RedisCache interface {
	Set(item *cache.Item) error
	Get(ctx context.Context, key string, value interface{}) error
}

var _ RedisCache = (*CacheMetricsWrapper)(nil)

type CacheMetricsWrapper struct {
	c *cache.Cache

	localCacheEvictions *prometheus.CounterVec

	cacheGets *prometheus.CounterVec
	cacheSets *prometheus.CounterVec

	cacheGetTime prometheus.Histogram
	cacheSetTime prometheus.Histogram

	cacheHits   prometheus.Gauge
	cacheMisses prometheus.Gauge
}

func NewCacheMetricsWrapper(registry prometheus.Registerer) *CacheMetricsWrapper {
	res := &CacheMetricsWrapper{
		localCacheEvictions: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "local_cache_evictions",
				Help: "number of evictions done by in-memory layer of the cache",
			},
			// NOTE(prozlach):
			//   on_object_add==false - ALL evictions, including ones that
			//   happen on when a new element is added.
			//   on_object_add==true - only the evictions that happen when a
			//   new element is added.
			[]string{"on_object_add"},
		),
		cacheGets: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "cache_gets",
				Help: "number of Get() request to cache",
			},
			[]string{"object_type"},
		),
		cacheSets: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Name: "cache_sets",
				Help: "number of Set() request to cache",
			},
			[]string{"object_type"},
		),
		cacheGetTime: prometheus.NewHistogram(
			prometheus.HistogramOpts{
				Name:    "cache_get_time",
				Help:    "the time it took to fetch item from cache",
				Buckets: prometheus.DefBuckets,
			},
		),
		cacheSetTime: prometheus.NewHistogram(
			prometheus.HistogramOpts{
				Name:    "cache_set_time",
				Help:    "the time it took to set item in cache",
				Buckets: prometheus.DefBuckets,
			},
		),
		cacheHits: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Name: "cache_hits",
				Help: "number of hits when accesing cache",
			},
		),
		cacheMisses: prometheus.NewGauge(
			prometheus.GaugeOpts{
				Name: "cache_misses",
				Help: "number of misses when accesing cache",
			},
		),
	}

	registry.MustRegister(
		res.cacheGets,
		res.cacheSets,
		res.cacheGetTime,
		res.cacheSetTime,
		res.cacheHits,
		res.cacheMisses,
	)

	return res
}

func (cwm *CacheMetricsWrapper) EvictionCallback(isObjectAdd bool) {
	cwm.localCacheEvictions.WithLabelValues(fmt.Sprintf("%t", isObjectAdd)).Inc()
}

func (cwm *CacheMetricsWrapper) SetCacheObject(c *cache.Cache) {
	cwm.c = c
}

func (cwm *CacheMetricsWrapper) getObjectType(item interface{}) string {
	return fmt.Sprintf("%T", item)
}

func (cwm *CacheMetricsWrapper) Set(item *cache.Item) error {
	objectType := cwm.getObjectType(item.Value)
	cwm.cacheSets.WithLabelValues(objectType).Inc()

	startTime := time.Now()
	err := cwm.c.Set(item)
	latency := time.Since(startTime)

	cwm.cacheSetTime.Observe(latency.Seconds())

	return err
}

func (cwm *CacheMetricsWrapper) Get(
	ctx context.Context,
	key string,
	value interface{},
) error {
	startTime := time.Now()
	err := cwm.c.Get(ctx, key, value)
	latency := time.Since(startTime)

	objectType := cwm.getObjectType(value)
	cwm.cacheGets.WithLabelValues(objectType).Inc()

	cwm.cacheGetTime.Observe(latency.Seconds())
	stats := cwm.c.Stats()
	cwm.cacheHits.Set(float64(stats.Hits))
	cwm.cacheMisses.Set(float64(stats.Misses))

	return err
}

// Middleware to set the database clients on the context for downstream handlers.
func Cache(client redis.Cmdable, registry prometheus.Registerer) gin.HandlerFunc {
	// Cache heavily used items with an LFU locally so subsequent resource access
	// within this instance of gatekeeper does not require any network requests.
	// Caching up to 10000 items for 10 seconds seems reasonable to start with.
	//
	// The JSON response returned by /v4/groups/:id for example
	// is ~1.16KB, so 10000 of those is around 11MB.
	//
	// After that, the cache will lookup the key in redis (great for multiple instances
	// of gatekeeper to access a common cache layer).
	// If that returns redis.NIL then finally we'll hit the gitlab API.
	cm := NewCacheMetricsWrapper(registry)

	c := cache.New(&cache.Options{
		Redis:        client,
		LocalCache:   NewCacheAdapter(10000, 10*time.Second, cm.EvictionCallback),
		StatsEnabled: true,
	})

	cm.SetCacheObject(c)

	return func(ctx *gin.Context) {
		ctx.Set(CacheClientKey, cm)
	}
}

// Helper to get the Redis client from the context.
func GetCache(ctx *gin.Context) RedisCache {
	return ctx.MustGet(CacheClientKey).(RedisCache)
}

var _ cache.LocalCache = (*CacheAdapter)(nil)

type CacheAdapter struct {
	cacheImpl *expirable.LRU[string, []byte]
	ec        func(bool)
}

func NewCacheAdapter(size int, ttl time.Duration, evictionCallback func(bool)) *CacheAdapter {
	// evictionCallback function is called by the cache every time an element
	// is removed from cache. The bool argument signifies whether the eviction
	// happened during the addition of a new element or not. In this case we
	// are counting simply all evictions.
	f := func(_ string, _ []byte) {
		evictionCallback(false)
	}
	res := new(CacheAdapter)
	res.cacheImpl = expirable.NewLRU[string, []byte](size, f, ttl)
	res.ec = evictionCallback

	return res
}

func (ca *CacheAdapter) Set(key string, data []byte) {
	evicted := ca.cacheImpl.Add(key, data)
	if evicted {
		// LRU cache implementation does not distinguish if the eviction
		// happened due to plain removal/purge of old entries or if element was
		// replaced by a different one. We want to distinguish both cases in
		// the metrics so we count the latter case separately, hence the
		// `true` argument.
		ca.ec(true)
	}
}

func (ca *CacheAdapter) Get(key string) ([]byte, bool) {
	return ca.cacheImpl.Get(key)
}

func (ca *CacheAdapter) Del(key string) {
	ca.cacheImpl.Remove(key)
}
