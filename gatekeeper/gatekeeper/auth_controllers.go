package gatekeeper

import (
	"fmt"
	"strconv"
	"sync"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/ratelimiting"
)

// NOTE(prozlach): The caveat here is that our integration points to GitLab
// already require `read_api` scopes as we use it to perform some membership
// checks. We need to investigate whether we can simply extend
// `*_observability` scopes a bit so that `read_api` is no longer needed.

// ReadAccessTokenScopes returns scope sets. Token needs to have one or
// more of them in order for request to be granted.
func ReadAccessTokenScopes() []string {
	return []string{"read_api", "read_observability"}
}

func WriteAccessTokenScopes() []string {
	return []string{"read_api", "write_observability"}
}

func ReadWriteAccessTokenScopes() []string {
	return []string{"read_api", "read_observability", "write_observability"}
}

func actionToTokenScopes(action string) []string {
	switch action {
	case "read":
		return ReadAccessTokenScopes()
	case "write":
		return WriteAccessTokenScopes()
	case "readwrite":
		return ReadWriteAccessTokenScopes()
	default:
		// This is a programming error, not input error. The Gin's path params
		// input validation should have handled it before we reached this
		// point.
		panic("Unknown action passed")
	}
}

type GroupAuthHandlerFormParams struct {
	MinAccessLevel *int  `json:"min_accesslevel" form:"min_accesslevel" binding:"required,numeric,min=0,max=50"`
	AutoAuth       *bool `json:"auto_auth" form:"auto_auth" binding:"required,boolean"`
}

type GroupAuthHandlerPathParams struct {
	NamespaceID int    `uri:"namespace_id" binding:"required,numeric,min=1"`
	Action      string `uri:"action" binding:"required,oneof=read write readwrite"`
}

type ProjectAuthHandlerFormParams struct {
	MinAccessLevel *int  `json:"min_accesslevel" form:"min_accesslevel" binding:"required,numeric,min=0,max=50"`
	AutoAuth       *bool `json:"auto_auth" form:"auto_auth" binding:"required,boolean"`
}

type ProjectAuthHandlerPathParams struct {
	ProjectID int    `uri:"project_id" binding:"required,numeric,min=1"`
	Action    string `uri:"action" binding:"required,oneof=read write readwrite"`
}

type NamespacePathParam struct {
	NamespaceID int `uri:"namespace_id" binding:"required,numeric,min=1"`
}

type tokenStore struct {
	store     map[string]*sync.Mutex
	storeLock *sync.Mutex
}

func (s *tokenStore) GetCacheKeyMutex(cacheKey string) *sync.Mutex {
	s.storeLock.Lock()
	defer s.storeLock.Unlock()
	m, ok := s.store[cacheKey]
	if !ok {
		m = &sync.Mutex{}
		s.store[cacheKey] = m
	}
	return m
}

var (
	accessTokenStore = &tokenStore{
		store:     make(map[string]*sync.Mutex),
		storeLock: &sync.Mutex{},
	}
)

// ProjectAuthHandlerFactory returns an auth handler that will handle several
// cases of auth for project membership.
//
//nolint:funlen // This is mostly empty lines, and comments plus
func ProjectAuthHandlerFactory(namespacedPath bool) gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		var pathParams ProjectAuthHandlerPathParams
		if err := ginCtx.ShouldBindUri(&pathParams); err != nil {
			ginCtx.AbortWithError(
				403, fmt.Errorf("path parameters validation failed: %w", err),
			)
			return
		}

		namespaceID := -1
		if namespacedPath {
			var tmp NamespacePathParam
			if err := ginCtx.ShouldBindUri(&tmp); err != nil {
				ginCtx.AbortWithError(
					403, fmt.Errorf("namespace parameter validation failed: %w", err),
				)
				return
			}
			namespaceID = tmp.NamespaceID
		}

		var formParams ProjectAuthHandlerFormParams
		if err := ginCtx.ShouldBindQuery(&formParams); err != nil {
			ginCtx.AbortWithError(
				403, fmt.Errorf("uri parameters validation failed: %w", err),
			)
			return
		}

		// Check for Authorization header first
		bearerToken := ginCtx.GetHeader("private-token")
		if bearerToken != "" { //nolint:nestif
			// NOTE(prozlach): we differentiate "action" only for token
			// access/auth, user acesssing via cookie/browser is assumed to
			// have full access. There are only three possible actions as there
			// are only 2 scopes available for us:
			// * `read_observability`
			// * `write_observability`
			// Individual ingress objects may map these to URIs, but from the
			// perspective of Gatekeeper URIs have no meaning as they would be
			// mapped to one of these three "actions", hence we keep things
			// simple and configure this in the ingress object itself.
			HandleGLAccessTokenAuth(ginCtx, bearerToken, actionToTokenScopes(pathParams.Action), accessTokenStore)
			if ginCtx.IsAborted() {
				return
			}

			var topLevelNamespaceID int
			if namespacedPath {
				VerifyProjectGroupMembership(
					ginCtx,
					namespaceID,
					pathParams.ProjectID,
				)
				if ginCtx.IsAborted() {
					return
				}

				topLevelNamespaceID = GetTopLevelNamespaceFromGroup(ginCtx, namespaceID)
			} else {
				topLevelNamespaceID = GetTopLevelNamespaceFromProject(ginCtx, pathParams.ProjectID)
			}
			// If the request path does not contain the namespace of the
			// projects, we fetch the top-level namespace that the project
			// belongs to and set it as a HTTP header for downstream's
			// components consuption.
			ginCtx.Header("x-top-level-namespace", strconv.Itoa(topLevelNamespaceID))
			ginCtx.Set(topLevelGroupIDKey, topLevelNamespaceID)
			ginCtx.Set(ratelimiting.RootNamespaceKey, strconv.Itoa(topLevelNamespaceID))

			HandleProjectAccessAuth(
				ginCtx,
				pathParams.ProjectID,
				*formParams.MinAccessLevel,
			)
			return
		}

		// Fallback to authorization via session cookie. This request comes
		// from the browser and allows users to interact with the API in the
		// browser
		log.Debug("handling session cookie-based auth")
		AuthenticatedSessionRequired(*formParams.AutoAuth, func(_ *gin.Context) {
			var topLevelNamespaceID int

			if namespacedPath {
				VerifyProjectGroupMembership(
					ginCtx,
					namespaceID,
					pathParams.ProjectID,
				)
				if ginCtx.IsAborted() {
					return
				}

				topLevelNamespaceID = GetTopLevelNamespaceFromGroup(ginCtx, namespaceID)
			} else {
				topLevelNamespaceID = GetTopLevelNamespaceFromProject(ginCtx, pathParams.ProjectID)
			}
			// If the request path does not contain the namespace of the
			// projects, we fetch the top-level namespace that the project
			// belongs to and set it as a HTTP header for downstream's
			// components consuption.
			ginCtx.Header("x-top-level-namespace", strconv.Itoa(topLevelNamespaceID))
			ginCtx.Set(topLevelGroupIDKey, topLevelNamespaceID)
			ginCtx.Set(ratelimiting.RootNamespaceKey, strconv.Itoa(topLevelNamespaceID))

			HandleProjectAccessAuth(
				ginCtx,
				pathParams.ProjectID,
				*formParams.MinAccessLevel,
			)
		})(ginCtx)
	}
}

// GroupAuthHandlerFactory returns an auth handler that will handle several
// cases of authn/authz for Group/Namespace membership.
func GroupAuthHandlerFactory() gin.HandlerFunc {
	return func(ginCtx *gin.Context) {
		var pathParams GroupAuthHandlerPathParams
		if err := ginCtx.ShouldBindUri(&pathParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("path parameters validation failed: %w", err))
			return
		}

		var formParams GroupAuthHandlerFormParams
		if err := ginCtx.ShouldBindQuery(&formParams); err != nil {
			ginCtx.AbortWithError(403, fmt.Errorf("uri parameters validation failed: %w", err))
			return
		}

		// Check for Authorization header first
		bearerToken := ginCtx.GetHeader("private-token")
		if bearerToken != "" {
			// NOTE(prozlach): we differentiate "action" only for token
			// access/auth, user acesssing via cookie/browser is assumed to
			// have full access. There are only three possible actions as there
			// are only 2 scopes available for us:
			// * `read_observability`
			// * `write_observability`
			// Individual ingress objects may map these to URIs, but from the
			// perspective of Gatekeeper URIs have no meaning as they would be
			// mapped to one of these three "actions", hence we keep things
			// simple and configure this in the ingress object itself.
			HandleGLAccessTokenAuth(ginCtx, bearerToken, actionToTokenScopes(pathParams.Action), accessTokenStore)
			if ginCtx.IsAborted() {
				return
			}

			HandleGroupAccessAuth(
				ginCtx,
				pathParams.NamespaceID,
				*formParams.MinAccessLevel,
			)
			if ginCtx.IsAborted() {
				return
			}

			topLevelNamespaceID := GetTopLevelNamespaceFromGroup(ginCtx, pathParams.NamespaceID)
			ginCtx.Set(topLevelGroupIDKey, topLevelNamespaceID)
			ginCtx.Set(ratelimiting.RootNamespaceKey, strconv.Itoa(topLevelNamespaceID))
			return
		}

		// Fallback to authorization via session cookie. This request comes
		// from the browser and allows users to interact with the API in the
		// browser.
		log.Debug("handling session cookie-based auth")
		AuthenticatedSessionRequired(*formParams.AutoAuth, func(_ *gin.Context) {
			HandleGroupAccessAuth(
				ginCtx,
				pathParams.NamespaceID,
				*formParams.MinAccessLevel,
			)
		})(ginCtx)
		if ginCtx.IsAborted() {
			return
		}

		topLevelNamespaceID := GetTopLevelNamespaceFromGroup(ginCtx, pathParams.NamespaceID)
		ginCtx.Set(topLevelGroupIDKey, topLevelNamespaceID)
		ginCtx.Set(ratelimiting.RootNamespaceKey, strconv.Itoa(topLevelNamespaceID))
	}
}
