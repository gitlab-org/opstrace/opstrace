#!/usr/bin/env bash

set -euo pipefail

CURRENT_ASDF_DIR="${ASDF_DIR:-${HOME}/.asdf}"
CURRENT_ASDF_DATA_DIR="${ASDF_DATA_DIR:-${HOME}/.asdf}"

ASDF_VERSION_TO_INSTALL="v0.11.3"

CPU_TYPE=$(arch -arm64 uname -m 2> /dev/null || uname -m)

root_path="$(cd "$(dirname "$0")/.." || exit ; pwd -P)"

OPSTRACE_CACHE_DIR="${root_path}/.cache"
OPSTRACE_BOOTSTRAPPED_FILE="${OPSTRACE_CACHE_DIR}/.opstrace_bootstrapped"

asdf_ensure_plugins() {
  cut -d ' ' -f 1 .tool-versions | grep -Ev "^#|^$" | while IFS= read -r plugin
  do
    asdf plugin update "${plugin}" || asdf plugin add "${plugin}"
  done

  return 0
}

asdf_install_tools() {
  # Install all tools specified in .tool-versions
  local asdf_arch_opts

  asdf_arch_opts=""

  if [[ "${OSTYPE}" == "darwin"* ]]; then
    if [[ "${CPU_TYPE}" == "arm64" ]]; then
      # Support running brew under Rosetta 2 on Apple Silicon machines
      asdf_arch_opts="arch -x86_64"
    fi
  fi

  # NOTE(prozlach): We had cases where downloading nodejs tooling was taking
  # 20+ minutes and asdf did not timeout it out. 3 minutes should be more than
  # enough to download all the tooling on a reasonable network connection/in
  # cloud. SIGKILL after 3.5 minutes.
  timeout -k 210 180 bash -c "MAKELEVEL=0 ${asdf_arch_opts} asdf install"

  return $?
}

asdf_reshim() {
    asdf reshim
}

opstrace_mark_bootstrapped() {
    mkdir -p "${OPSTRACE_CACHE_DIR}"
    touch "${OPSTRACE_BOOTSTRAPPED_FILE}"

    echo
    echo "INFO: Bootstrap successful!"
    echo "INFO: To make sure all commands are available in this shell, run:"
    echo
    echo "source \"${CURRENT_ASDF_DIR}/asdf.sh\""
    echo
}

error() {
    echo
    echo "ERROR: ${1}" >&2
    echo
}

####################################################
echo "INFO: Bootstrapping dev dependencies..."

if [[ -f "${OPSTRACE_BOOTSTRAPPED_FILE}" ]]; then
    echo "INFO: Your environment has already been bootstrapped."
    echo "INFO: Delete '${OPSTRACE_BOOTSTRAPPED_FILE}' to re-bootstrap"
    exit 0
fi

asdf version >/dev/null 2>&1 || { echo >&2 "Need \"asdf\" installed to bootstrap dependencies, check \"make install-asdf\""; exit 1; }

echo "INFO: Installing/updating asdf plugins..."
if ! asdf_ensure_plugins; then
    error "Failed to install some asdf plugins." >&2
fi

echo "INFO: Installing asdf tools..."
if ! asdf_install_tools; then
    error "Failed to install some asdf tools." >&2
fi

echo "INFO: Reshimming asdf..."
if ! asdf_reshim; then
    error "Failed to reshim asdf." >&2
fi

opstrace_mark_bootstrapped
