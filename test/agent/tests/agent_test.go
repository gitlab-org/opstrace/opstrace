package tests

import (
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/testutils"
)

var (
	// we assume the GOB single-binary stack to be running already as a
	// prerequisite to running these tests, see go/cmd/all-in-one/README
	backendQueryAPIURL      string = "http://localhost:8080"
	backendOTELHTTPEndpoint string = "http://localhost:4318"

	httpClient   *http.Client
	oidcProvider *testutils.OidcServer
	jwtToken     string
)

func TestAgent(t *testing.T) {
	RegisterFailHandler(Fail)

	SetDefaultEventuallyTimeout(time.Minute * 15)
	SetDefaultEventuallyPollingInterval(time.Second)

	suiteConfig, reporterConfig := GinkgoConfiguration()
	reporterConfig.Verbose = true
	reporterConfig.FullTrace = true
	suiteConfig.Timeout = 2 * time.Hour
	suiteConfig.GracePeriod = 1 * time.Hour
	RunSpecs(t, "Agent test suite", suiteConfig, reporterConfig)
}

var _ = BeforeSuite(func(ctx SpecContext) {
	var err error
	httpClient = http.DefaultClient
	keyPath := os.Getenv("HOME") + "/sample_oidc_private_key.pem" // JWT signing key
	oidcProvider, err = testutils.NewOIDCServer(nil, &keyPath)
	if err != nil {
		panic(err)
	}
	claims := map[string]string{
		"gitlab_realm":        "self-managed",
		"gitlab_namespace_id": fmt.Sprintf("%d", gitlabGroupID),
		"sub":                 "instance1",
	}
	jwtToken, err = oidcProvider.Token(claims)
	if err != nil {
		panic(err)
	}
})

var _ = AfterSuite(func(ctx SpecContext) {
	if oidcProvider != nil {
		oidcProvider.Close()
	}
})
