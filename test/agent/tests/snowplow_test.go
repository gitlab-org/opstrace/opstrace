package tests

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	ch "github.com/ClickHouse/clickhouse-go/v2"
	storagememory "github.com/snowplow/snowplow-golang-tracker/v3/pkg/storage/memory"
	sp "github.com/snowplow/snowplow-golang-tracker/v3/tracker"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"

	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/constants"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/enricher/outputs"
	chexporter "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/snowplow/exporter/clickhouse"
)

const (
	debug         bool   = true
	collectorURL  string = "localhost:8082"
	natsURL       string = "nats://0.0.0.0:4222"
	clickhouseDSN string = "tcp://localhost:9000"
	tsFormat      string = "2006-01-02 15:04:05.000"
)

var (
	err             error
	actualCollector *sp.Tracker
	storageConn     ch.Conn
)

var _ = Describe("Snowplow", Ordered, Serial, func() {
	BeforeAll(func(ctx SpecContext) {
		actualCollector, err = setupActualCollector(ctx)
		Expect(err).ToNot(HaveOccurred())

		storageConn, err = chexporter.NewDB(&chexporter.Config{
			ClickHouseDSN:             clickhouseDSN,
			ClickHouseMaxOpenConns:    1,
			ClickHouseMaxIdleConns:    1,
			ClickHouseConnMaxLifetime: 30 * time.Second,
		})
		Expect(err).ToNot(HaveOccurred())
	})

	BeforeEach(func(ctx SpecContext) {
		// reset CH table content
		err := storageConn.Exec(
			ctx,
			fmt.Sprintf("TRUNCATE TABLE %s.%s", constants.SnowplowDatabaseName, constants.SnowplowEnrichedEventsTableName),
		)
		Expect(err).ToNot(HaveOccurred())
	})

	Context("Ingest", func() {
		It("can use the endpoint to track self-describing events", func(ctx SpecContext) {
			data := map[string]interface{}{"targetUrl": "https://foobar.com"}
			sdj := sp.InitSelfDescribingJson("iglu:com.snowplowanalytics.snowplow/link_click/jsonschema/1-0-1", data)
			sde := sp.SelfDescribingEvent{Event: sdj}

			actualCollector.TrackSelfDescribingEvent(sde)
			actualCollector.Emitter.Stop()
			actualCollector.BlockingFlush(5, 10000)

			actualEvents := buildActualEvents(ctx)

			refEvents, err := sendEventToReferenceCollector(ctx, func(t *sp.Tracker) {
				t.TrackSelfDescribingEvent(sde)
			})
			Expect(err).ToNot(HaveOccurred())

			Expect(len(actualEvents)).To(Equal(len(refEvents)))
			for i := 0; i < len(actualEvents); i++ {
				actualEvent := strings.Trim(actualEvents[i], "\n")
				referenceEvent := strings.Trim(refEvents[i], "\n")

				aComps := strings.Split(actualEvent, "\t")
				rComps := strings.Split(referenceEvent, "\t")
				fieldNames := strings.Split(outputs.ExpectedOrder, "\t")

				for j := 0; j < 131; j++ {
					switch fieldNames[j] {
					case "event_id", "v_collector", "v_etl", "user_ipaddress", "network_userid":
						continue // these fields cannot be matched across actual & reference implementations
					case "etl_tstamp", "collector_tstamp", "dvce_created_tstamp", "dvce_sent_tstamp", "derived_tstamp":
						// assert generated timestamps are within 5 seconds across implementations
						t1, err := time.Parse(tsFormat, aComps[j])
						Expect(err).ToNot(HaveOccurred())

						t2, err := time.Parse(tsFormat, rComps[j])
						Expect(err).ToNot(HaveOccurred())

						Expect(assertTimesWithinThreshold(t1, t2, 5)).To(BeTrue())
					default:
						if aComps[j] != rComps[j] {
							fmt.Printf("Comparing %s, actual: %s, reference: %s\n", fieldNames[j], aComps[j], rComps[j])
						}
						Expect(aComps[j]).To(Equal(rComps[j]))
					}
				}
			}
		})
	})
})

type TestLogConsumer struct {
	mtx  sync.Mutex
	msgs []string
}

func (g *TestLogConsumer) Accept(l testcontainers.Log) {
	s := string(l.Content)

	g.mtx.Lock()
	defer g.mtx.Unlock()
	g.msgs = append(g.msgs, s)
}

func (g *TestLogConsumer) Msgs() []string {
	g.mtx.Lock()
	defer g.mtx.Unlock()

	return g.msgs
}

func setupActualCollector(_ context.Context) (*sp.Tracker, error) {
	emitter := sp.InitEmitter(
		sp.RequireCollectorUri(collectorURL),
		sp.RequireStorage(*storagememory.Init()),
		sp.OptionRequestType("POST"),
		sp.OptionProtocol("http"),
		sp.OptionSendLimit(4),
	)
	subject := sp.InitSubject()
	subject.SetLanguage("en")
	tracker := sp.InitTracker(
		sp.RequireEmitter(emitter),
		sp.OptionSubject(subject),
	)
	return tracker, nil
}

type enrichedEventsRow struct {
	TenantID           string    `ch:"TenantId"`
	ProjectID          string    `ch:"ProjectId"`
	Message            string    `ch:"Message"`
	IngestionTimestamp time.Time `ch:"IngestionTimestamp"`
}

const readEnrichedEventsTmpl string = `
SELECT
  TenantId,
  ProjectId,
  Message,
  IngestionTimestamp
FROM
  %s.%s
`

func buildActualEvents(ctx context.Context) []string {
	var events []string
	Eventually(func(g Gomega) int {
		events = make([]string, 0) // reset over multiple runs, just in case

		rows, err := storageConn.Query(
			ctx,
			fmt.Sprintf(readEnrichedEventsTmpl, constants.SnowplowDatabaseName, constants.SnowplowEnrichedEventsTableName),
		)
		Expect(err).ToNot(HaveOccurred())

		for rows.Next() {
			var r enrichedEventsRow
			err := rows.ScanStruct(&r)
			Expect(err).ToNot(HaveOccurred())
			events = append(events, r.Message)
		}

		return len(events)
	}).WithTimeout(time.Minute).Should(BeNumerically(">", 0))
	return events
}

type sendEventFn func(*sp.Tracker)

func sendEventToReferenceCollector(ctx context.Context, sender sendEventFn) ([]string, error) {
	g := TestLogConsumer{
		msgs: make([]string, 0),
	}

	// Create the container request
	req := testcontainers.ContainerRequest{
		Image:        "snowplow/snowplow-micro:2.1.3",
		Cmd:          []string{"--output-tsv"},
		ExposedPorts: []string{"9090/tcp"},
		WaitingFor:   wait.ForListeningPort("9090"),
		LogConsumerCfg: &testcontainers.LogConsumerConfig{
			Consumers: []testcontainers.LogConsumer{&g},
		},
	}

	// Start the container
	container, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		return nil, err
	}

	mappedPort, err := container.MappedPort(ctx, "9090")
	if err != nil {
		return nil, err
	}

	collectorURL := fmt.Sprintf("localhost:%s", mappedPort.Port())
	emitter := sp.InitEmitter(
		sp.RequireCollectorUri(collectorURL),
		sp.RequireStorage(*storagememory.Init()),
		sp.OptionRequestType("POST"),
		sp.OptionProtocol("http"),
		sp.OptionSendLimit(4),
	)

	subject := sp.InitSubject()
	subject.SetLanguage("en")
	tracker := sp.InitTracker(
		sp.RequireEmitter(emitter),
		sp.OptionSubject(subject),
	)

	sender(tracker)
	tracker.Emitter.Stop()
	tracker.BlockingFlush(5, 10000)

	if err := container.Terminate(ctx); err != nil {
		panic(err)
	}

	events := make([]string, 0)
	for _, msg := range g.msgs {
		if strings.HasPrefix(msg, "SLF4J") {
			continue // snowplow-micro's internal logger
		}
		events = append(events, msg)
	}

	return events, nil
}

func assertTimesWithinThreshold(t1, t2 time.Time, nrSeconds int) bool {
	diff := t1.Sub(t2).Abs() // absolute difference
	return diff.Seconds() <= float64(nrSeconds)
}
