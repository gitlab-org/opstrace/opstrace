include ../../Makefile.go.mk

DOCKER_IMAGE_NAME ?= sentry-test-harness

export GO_BUILD_LDFLAGS = \
	-X main.release=${DOCKER_IMAGE_TAG} \

.PHONY: docker-ensure
docker-ensure: docker-ensure-default

.PHONY: docker-build
docker-build:
	docker build \
		--build-arg GO_BUILD_LDFLAGS \
		-f Dockerfile \
		-t ${DOCKER_IMAGE} \
		-t ${DOCKER_IMAGE_REGISTRY}/${DOCKER_IMAGE_NAME}:latest \
		../..

.PHONY: docker-push
docker-push:
	docker push ${DOCKER_IMAGE}

.PHONY: docker
docker: docker-build docker-push

.PHONY: docker-run
docker-run:
	docker run ${DOCKER_IMAGE}

.PHONY: build
build:
	go build -o harness ./cmd/main.go

.PHONY: run
run:
	./harness execute

.PHONY: unit-tests
unit-tests:
	go test -v ./...

.PHONY: deploy
deploy: build docker-build run

.PHONY: regenerate
regenerate: #noop

.PHONY: print-docker-images
print-docker-images: print-docker-image-name-tag

sdk_dockerfiles = $(wildcard ./testdata/supported-sdk-clients/*/Dockerfile)
sdk_dirs = $(dir $(sdk_dockerfiles))

.PHONY: docker-apps
docker-apps: docker-build-apps docker-push-apps

.PHONY: docker-build-app
docker-build-app: APP_DIR=.
docker-build-app:
	docker build \
		--build-arg GO_BUILD_LDFLAGS \
		-f ${APP_DIR}/Dockerfile \
		-t ${DOCKER_IMAGE} \
		-t ${DOCKER_IMAGE_REGISTRY}/${DOCKER_IMAGE_NAME}:latest \
		${APP_DIR}

.PHONY: docker-push-app
docker-push-app:
	docker push ${DOCKER_IMAGE}

.PHONY: docker-build-apps
docker-build-apps:
	@for d in ${sdk_dirs}; \
	do \
	    export DOCKER_IMAGE_NAME="test-with-sentry-sdk/sentry-$$(basename "$$d")"; \
		$(MAKE) APP_DIR=$$d docker-build-app; \
	done;

.PHONY: docker-push-apps
docker-push-apps:
	@for d in ${sdk_dirs}; \
	do \
		export DOCKER_IMAGE_NAME="test-with-sentry-sdk/sentry-$$(basename "$$d")"; \
		$(MAKE) docker-push-app; \
	done;
