SHELL=/bin/bash
.SHELLFLAGS := -eu -o pipefail -c
.ONESHELL:

export TEST_TERRAFORM_DIR=$(shell pwd)/../../terraform

# TODO(prozlach): Instead of hardcoding, we should fetch these IP from devvm
# somehow. This is not trivial though, and the likeness that these will change
# is miniscule.
GDK_IP := 10.15.17.1
export GDK_IP
GOB_IP := 10.15.16.129
export GOB_IP

.PHONY: e2e-test
e2e-test: setup-frontend setup-iptables-rules setup-host-file-entries
	go run github.com/onsi/ginkgo/v2/ginkgo -r --junit-report=.junit-ginkgo.xml -v ./...

.PHONY: lint
lint: ## Run golangci-lint against the project
	golangci-lint run --allow-parallel-runners --timeout 10m

.PHONY: destroy
destroy: ## Destroy all e2e infra resources
	go run ./cmd/destroy

.PHONY: setup-frontend
setup-frontend: ## Install frontend test dependencies
	make -C frontend setup-suite

.PHONY: printenv
printenv: ## Print internal provider environment variables
	@go run ./cmd/printenv

.PHONY:
setup-iptables-rules:
	scripts/setup-iptables-rules.sh

.PHONY:
prune-iptables-rules:
	scripts/prune-iptables-rules.sh

.PHONY:
setup-host-file-entries:
	scripts/setup-host-file-entries.sh

.PHONY:
prune-host-file-entries:
	scripts/prune-host-file-entries.sh
