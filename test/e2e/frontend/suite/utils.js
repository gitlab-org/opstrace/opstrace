import { METRIC_TYPE_HISTOGRAM } from "./constants";
import { v4 } from "uuid";

export function getTraceService(id) {
  return `test-${id}`;
}

export function generateTraceID() {
  return v4();
}

function sanitizeTraceId(traceId) {
  return traceId.replaceAll("-", "");
}

export function seedTraces(token, projectId, traces) {
  const endpoint = `${Cypress.env("TEST_GITLAB_ADDRESS")}/api/v4/projects/${projectId}/observability/v1/traces`;

  let cmd = `go run ../generators/trace.go -token ${token} -endpoint ${endpoint}`;
  traces.forEach((trace) => {
    const traceID = sanitizeTraceId(trace.id);
    const nowMs = Math.round(trace.start.getTime() / 1000);
    cmd += ` ${traceID}:${nowMs}:${trace.duration}:${getTraceService(trace.id)}`;
  });

  exec(cmd, "seedTraces");
}

export function seedMetrics(token, projectId, { name, type, datapoints }) {
  const endpoint = `${Cypress.env("TEST_GITLAB_ADDRESS")}/api/v4/projects/${projectId}/observability/v1/metrics`;

  let cmd = `go run ../generators/metrics/metrics.go -token ${token} -endpoint ${endpoint} -mname ${name} -mtype ${type}`;

  datapoints.forEach((datapoint) => {
    const attributes = datapoint.attributes || {};
    const attributesStr = Object.entries(attributes)
      .map(([key, value]) => `${key}=${value}`)
      .join(",");
    if (type === METRIC_TYPE_HISTOGRAM) {
      cmd += ` -datapoint ${datapoint.timestamp}:${datapoint.timestamp}:${datapoint.value.join(",")}:${attributesStr}:${sanitizeTraceId(datapoint.traceID)}`;
    } else {
      cmd += ` -datapoint ${datapoint.timestamp}:${datapoint.timestamp}:${datapoint.value}:${attributesStr}:${sanitizeTraceId(datapoint.traceID)}`;
    }
  });

  exec(cmd, "seedMetrics");
}

export function seedLogs(token, projectId, logs) {
  const endpoint = `${Cypress.env("TEST_GITLAB_ADDRESS")}/api/v4/projects/${projectId}/observability/v1/logs`;

  let cmd = `go run ../generators/logs/log.go -token ${token} -endpoint ${endpoint}`;
  logs.forEach(({ traceId, timestamp, appName, severity, body }) => {
    cmd += ` "${[sanitizeTraceId(traceId), timestamp, appName, severity, body].join("#")}"`;
  });

  exec(cmd, "seedLogs");
}

function exec(cmd, name) {
  cy.log(cmd);
  cy.exec(cmd, { failOnNonZeroExit: false }).then((res) => {
    if (res.code !== 0) {
      throw new Error(`${name} exit code ${res.code}: ${res.stderr}`);
    }
  });
}
