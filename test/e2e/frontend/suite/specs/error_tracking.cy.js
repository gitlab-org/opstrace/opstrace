import * as Sentry from "@sentry/browser";
import { defaultUser } from "../constants";

describe("error tracking", () => {
  const errorText = "My test failure";

  const errorTrackingTestProject =
    "error-tracking-e2e/error-tracking-" + Date.now();

  before(() => {
    cy.setupProjectPermissions(
      defaultUser,
      errorTrackingTestProject,
      "error-tracking-token",
    ).then(({ projectId }) => {
      cy.getProjectSentryDSN(projectId).then((dsn) => {
        Sentry.init({
          dsn,
          debug: true,
        });

        failureFromE2ETest(errorText);
      });
    });
  });

  beforeEach(() => {
    cy.loginWithUser(defaultUser);
  });

  it("shows error details", () => {
    visitError(errorTrackingTestProject, errorText);

    // sanity check for error details
    checkCard("user-count-card", "Users", /\d+/);
    checkCard("error-count-card", "Events", /\d+/);
    checkCard("first-release-card", "First seen", /(just now|\d+ \w+ ago)/);
    // can take a minute for page to be available
    checkCard("last-release-card", "Last seen", /(just now|1 minute ago)/);
    cy.contains("Stack trace").should("be.visible");
    cy.contains("error_tracking.cy.js").should("be.visible");
  });

  it("increments error count", () => {
    visitError(errorTrackingTestProject, errorText);

    cardBody("error-count-card").then((el) => {
      const count = parseInt(el.text());

      failureFromE2ETest(errorText);

      const expected = count + 1;

      cy.waitUntil(
        () => {
          cy.reload();
          return cardBody("error-count-card").then((el) => {
            const newCount = parseInt(el.text());
            return newCount === expected;
          });
        },
        {
          errorMsg: `expected ${expected} users`,
          timeout: 10000,
          interval: 1000,
        },
      );
    });
  });

  it("increments user count", () => {
    visitError(errorTrackingTestProject, errorText);

    cardBody("user-count-card").then((el) => {
      const count = parseInt(el.text());

      failureFromE2ETest(errorText, Date.now().toString());

      const expected = count + 1;

      cy.waitUntil(
        () => {
          cy.reload();
          return cardBody("user-count-card").then((el) => {
            const newCount = parseInt(el.text());
            return newCount === expected;
          });
        },
        {
          errorMsg: `expected ${expected} users`,
          timeout: 10000,
          interval: 1000,
        },
      );
    });
  });
});

function visitError(project, title) {
  cy.visit(`${project}/-/error_tracking`);

  // check error appears in the error list
  cy.contains(title, { timeout: 20000 }).should("be.visible").click();
}

function cardQuery(testid) {
  const q = `.gl-card[data-testid=${testid}]`;
  return cy.get(q);
}

function checkCard(testid, headerText, body) {
  cardQuery(testid).should("be.visible").contains(headerText);
  cardQuery(testid).contains(body);
}

function cardBody(testid) {
  return cardQuery(testid).children(".gl-card-body").first();
}

function failureFromE2ETest(errorText, userId) {
  Sentry.withScope(function (scope) {
    scope.setUser({
      id: userId,
    });
    scope.captureException(new Error(errorText));
    cy.wrap(Sentry.flush(2000)).should("be.true");
  });
}
