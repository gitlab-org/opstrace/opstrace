import { defaultUser } from "../constants";
import { seedMetrics, seedTraces, seedLogs, generateTraceID } from "../utils";

describe("features correlation", () => {
  const testProject = "features-correlation-e2e/test-project-" + Date.now();

  const now = new Date();
  now.setMilliseconds(0);

  const traceID = generateTraceID();

  const metric = {
    name: "test_metric",
    type: "Sum",
    datapoints: [
      {
        timestamp: now.getTime(),
        value: 50,
        attributes: {
          test_attr_one: "val-1",
        },
        traceID,
      },
    ],
  };

  const log = {
    traceId: traceID,
    timestamp: now.getTime(),
    appName: "shop-backend",
    severity: "info",
    body: "User logged in",
    serviceName: "test-service-1",
  };

  const trace = {
    id: traceID,
    duration: 400000000,
    formattedDuration: "400ms",
    start: now,
  };

  let metricsAPIUrl;

  before(() => {
    cy.setupProjectPermissions(defaultUser, testProject, "test-token").then(
      ({ token, projectId }) => {
        metricsAPIUrl = new RegExp(
          `/api/v4/projects/${projectId}/observability/v1/metrics/search(\\?.*)?$`,
        );

        seedTraces(token, projectId, [trace]);
        seedMetrics(token, projectId, metric);
        seedLogs(token, projectId, [log]);

        cy.wait(5000);
      },
    );
  });

  beforeEach(() => {
    cy.loginWithUser(defaultUser);
  });

  it("correlates traces, logs and metrics correctly", () => {
    // visit trace details page
    cy.visit(`${testProject}/-/tracing/${traceID}`);

    // check trace's logs
    cy.contains("View logs").click();

    cy.url().should("include", `/-/logs?traceId[]=${traceID}`);

    cy.contains(log.body).click();

    cy.get(".gl-drawer").as("drawer");
    cy.get("@drawer").contains(`trace_id ${traceID}`).should("exist");

    // go back to trace details page from logs
    cy.get("@drawer").contains(`${traceID}`).click();
    cy.url().should("include", `/-/tracing/${traceID}`);

    // go to trace's metrics page
    cy.contains("View metrics").click();
    cy.url().should("include", `/-/metrics?traceId[]=${traceID}`);

    // check from the API response that we are getting the exact trace's metric
    cy.intercept(
      {
        method: "GET",
        url: metricsAPIUrl,
      },
      (req) => {
        req.continue((res) => {
          const datapoints = res.body.results.find(
            (m) => m.name === metric.name,
          ).values;
          expect(datapoints[0][2][0]).to.eq(traceID);
        });
      },
    ).as("searchMetric");

    cy.contains(metric.name).click();
    cy.wait("@searchMetric");
  });
});
