import {
  defaultUser,
  METRIC_TYPE_HISTOGRAM,
  METRIC_TYPE_SUM,
} from "../constants";
import { gitlabCustomDateRangeFilterShouldMatch } from "../../cypress/support/gitlab";
import { seedMetrics, generateTraceID } from "../utils";

describe("metrics", () => {
  const metricsTestProject = "metrics-e2e/metrics-" + Date.now();

  const now = Date.now();

  let searchAPIUrl;

  const sumMetric = {
    name: "test_sum_metric",
    type: METRIC_TYPE_SUM,
    datapoints: [
      {
        timestamp: now - 1000,
        value: 50,
        attributes: {
          test_attr_one: "val-1",
        },
        traceID: generateTraceID(),
      },
      {
        timestamp: now,
        value: 60,
        attributes: {
          test_attr_two: "val-2",
        },
        traceID: generateTraceID(),
      },
    ],
  };

  const histogramMetric = {
    name: "test_histogram_metric",
    type: METRIC_TYPE_HISTOGRAM,
    datapoints: [
      {
        timestamp: now - 1000,
        value: [1, 1, 1, 1, 1, 1],
        attributes: {
          test_attr_one: "val-1",
        },
        traceID: generateTraceID(),
      },
      {
        timestamp: now,
        value: [1, 2, 3, 4, 5, 0],
        attributes: {
          test_attr_two: "val-2",
        },
        traceID: generateTraceID(),
      },
    ],
  };

  const metrics = [sumMetric, histogramMetric];

  before(() => {
    cy.setupProjectPermissions(
      defaultUser,
      metricsTestProject,
      "metrics-token",
    ).then(({ token, projectId }) => {
      searchAPIUrl = new RegExp(
        `/api/v4/projects/${projectId}/observability/v1/metrics/search(\\?.*)?$`,
      );

      metrics.forEach((metric) => {
        seedMetrics(token, projectId, metric);
      });
      cy.wait(5000);
    });
  });

  beforeEach(() => {
    cy.loginWithUser(defaultUser);
  });

  describe("metrics list page", () => {
    it("contains correct content for row", () => {
      cy.visit(`${metricsTestProject}/-/metrics`);

      metrics.forEach((metric) => {
        cy.contains(metric.name).parents("tr").as("row");

        cy.get("@row").contains(metric.type).should("exist");
        const attributes = metric.datapoints
          .map((dp) => Object.keys(dp.attributes))
          .flat();
        cy.get("@row").contains(attributes.join(", ")).should("exist");
      });
    });
  });

  describe("metrics details page", () => {
    metrics.forEach((metric) => {
      const startTimestamp = metric.datapoints[0].timestamp;
      const endTimestamp =
        metric.datapoints[metric.datapoints.length - 1].timestamp;
      const startDate = new Date(startTimestamp);
      const endDate = new Date(endTimestamp);

      const allAttributes = metric.datapoints
        .map((dp) =>
          Object.entries(dp.attributes).map(([key, value]) => ({
            key,
            value,
          })),
        )
        .flat();

      describe(`when viewing ${metric.type} metric`, () => {
        it("displays correct details", () => {
          cy.visit(`${metricsTestProject}/-/metrics`);

          cy.contains(metric.name).click();

          cy.url().should(
            "include",
            `/-/metrics/${metric.name}?type=${metric.type}&date_range=1h`,
          );
          cy.gitlabDateRangeFilter().contains("Last 1 hour").should("exist");

          cy.get("[data-testid=metric-chart]").should("exist");
        });

        describe("attributes filters", () => {
          it("filters correctly", () => {
            const filterParams = allAttributes
              .map(({ key, value }) => `${key}[]=${value}`)
              .join("&");

            // check that API query params are set correctly
            cy.intercept(
              {
                method: "GET",
                url: searchAPIUrl,
              },
              (req) => {
                allAttributes.forEach(({ key, value }) => {
                  // Note: here we are checking the url instead of req.query as that is not populated correctly with duplicated keys
                  expect(decodeURIComponent(req.url)).to.contain(
                    `attrs=${key},eq,${value}`,
                  );
                });
                req.continue();
              },
            ).as("searchMetric");

            cy.visit(
              `${metricsTestProject}/-/metrics/${metric.name}?type=${metric.type}&${filterParams}`,
            );

            cy.gitlabSearchFilters().then(($filters) => {
              cy.wrap($filters).should("have.length", allAttributes.length);
              [...$filters].forEach(($filter, index) => {
                const { key, value } = allAttributes[index];
                cy.wrap($filter).contains(key);
                cy.wrap($filter).contains("=");
                cy.wrap($filter).contains(value);
              });
            });
            cy.wait("@searchMetric");

            cy.contains("Failed to load").should("not.exist");
          });
        });

        describe("groupby filters", () => {
          it("filters correctly", () => {
            if (metric.type === METRIC_TYPE_HISTOGRAM) return; //histograms do not support group by attrs

            const filterParams = allAttributes
              .map(({ key }) => `group_by_attrs[]=${key}`)
              .join("&");

            const groupByFn = "avg";

            // check that API query params are set correctly
            cy.intercept(
              {
                method: "GET",
                url: searchAPIUrl,
              },
              (req) => {
                expect(req.query.groupby_fn).to.eq(groupByFn);
                expect(req.query.groupby_attrs).to.eq(
                  allAttributes.map(({ key }) => key).join(","),
                );
                req.continue();
              },
            ).as("searchMetric");

            cy.visit(
              `${metricsTestProject}/-/metrics/${metric.name}?type=${metric.type}&group_by_fn=${groupByFn}&${filterParams}`,
            );

            cy.get("[data-testid=group-by-function-dropdown]")
              .contains(groupByFn)
              .should("exist");

            cy.get("[data-testid=group-by-attributes-dropdown]")
              .contains(allAttributes[0].key)
              .should("exist");

            cy.wait("@searchMetric");

            cy.contains("Failed to load").should("not.exist");
          });
        });

        describe("period filters", () => {
          const filters = {
            "5m": "Last 5 minutes",
            "15m": "Last 15 minutes",
            "30m": "Last 30 minutes",
            "1h": "Last 1 hour",
            "4h": "Last 4 hours",
            "12h": "Last 12 hours",
            "24h": "Last 24 hours",
            "7d": "Last 7 days",
            "14d": "Last 14 days",
            "30d": "Last 30 days",
          };
          Object.entries(filters).forEach(([period, filterText]) => {
            it(`filters ${period} correctly`, () => {
              // check that API query params are set correctly
              cy.intercept(
                {
                  method: "GET",
                  url: searchAPIUrl,
                },
                (req) => {
                  expect(req.query.period).to.eq(period);
                  req.continue();
                },
              ).as("searchMetric");

              cy.visit(
                `${metricsTestProject}/-/metrics/${metric.name}?type=${metric.type}&date_range=${period}`,
              );

              cy.gitlabDateRangeFilter().contains(filterText).should("exist");

              cy.wait("@searchMetric");

              cy.contains("Failed to load").should("not.exist");
            });
          });

          it("handles custom date ranges", () => {
            // check that API query params are set correctly
            cy.intercept(
              {
                method: "GET",
                url: searchAPIUrl,
              },
              (req) => {
                expect(req.query.start_time).to.eq(startDate.toISOString());
                expect(req.query.end_time).to.eq(endDate.toISOString());
                req.continue();
              },
            ).as("searchMetric");
            cy.visit(
              `${metricsTestProject}/-/metrics/${metric.name}?type=${metric.type}&date_range=custom&date_start=${startDate.toISOString()}&date_end=${endDate.toISOString()}`,
            );
            cy.gitlabDateRangeFilter().contains("Custom").should("exist");

            gitlabCustomDateRangeFilterShouldMatch(startDate, endDate);

            cy.wait("@searchMetric");

            cy.contains("Failed to load").should("not.exist");
          });
        });

        describe("issues integration", () => {
          it("creates an issue from a metric", () => {
            cy.visit(
              `${metricsTestProject}/-/metrics/${metric.name}?type=${metric.type}&date_range=custom&date_start=${startDate.toISOString()}&date_end=${endDate.toISOString()}&${allAttributes[0].key}=${allAttributes[0].value}&group_by_fn=avg&group_by_attrs[]=${allAttributes[0].key}`,
            );

            // check that there are no linked issues
            cy.contains("View issues 0").should("exist");

            // start issue creation flow from metrics page
            cy.contains("Create issue").click();

            cy.contains("New issue").should("exist");

            // submit issue creation form, with prefilled values
            cy.contains("Create issue").click();

            // check that the created issue has the correct information
            const expectedIssueTitle = `Issue created from ${metric.name}`;
            cy.contains(expectedIssueTitle).should("exist");
            cy.contains(`Name: ${metric.name}`).should("exist");
            cy.contains(`Type: ${metric.type}`).should("exist");
            cy.contains("Snapshot").should("exist");
            cy.contains(
              `Timeframe: ${startDate.toUTCString()} - ${endDate.toUTCString()}`,
            ).should("exist");

            // go to the related metric
            cy.contains("Metric details").click();

            // check we are on the metric detail page
            cy.url().should(
              "include",
              `/-/metrics/${metric.name}?type=${metric.type}`,
            );

            gitlabCustomDateRangeFilterShouldMatch(startDate, endDate);
            cy.get("[data-testid=metric-chart]").should("exist");

            if (metric.type !== METRIC_TYPE_HISTOGRAM) {
              // Hisograms do not support group by attrs
              cy.get("[data-testid=group-by-function-dropdown]")
                .contains("avg")
                .should("exist");

              cy.get("[data-testid=group-by-attributes-dropdown]")
                .contains(allAttributes[0].key)
                .should("exist");
            }

            cy.gitlabSearchFilters().then(($filters) => {
              cy.wrap($filters).should("have.length", 1);
              cy.wrap($filters[0]).contains(allAttributes[0].key);
              cy.wrap($filters[0]).contains("=");
              cy.wrap($filters[0]).contains(allAttributes[0].value);
            });

            // check that the newly created issue is linked on the metric page
            cy.contains("View issues 1").should("exist");

            // click on the linked issue
            cy.contains(expectedIssueTitle).click();

            // check we are on the newly created issue page
            cy.url().should("include", `/-/issues/`);
            cy.contains(expectedIssueTitle).should("exist");
          });
        });
      });
    });
  });
});
