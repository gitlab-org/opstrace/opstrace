import { defaultUser } from "../constants";
import dateFormat from "dateformat";
import { gitlabCustomDateRangeFilterShouldMatch } from "../../cypress/support/gitlab";
import { seedLogs, generateTraceID } from "../utils";

describe("logs", () => {
  const now = Date.now();

  let searchAPIUrl;

  const logsTestProject = "logs-e2e/logs-" + Date.now();

  const logs = [
    {
      traceId: generateTraceID(),
      timestamp: now,
      appName: "shop-backend",
      severity: "info",
      body: "User logged in",
      serviceName: "test-service-1",
    },
    {
      traceId: generateTraceID(),
      timestamp: now - 1000,
      appName: "auth-service",
      severity: "debug",
      body: "Token validated",
      serviceName: "test-service-2",
    },
    {
      traceId: generateTraceID(),
      timestamp: now - 2000,
      appName: "article-service",
      severity: "error",
      body: "Failed to retrieve article",
      serviceName: "test-service-3",
    },
  ];

  before(() => {
    cy.setupProjectPermissions(defaultUser, logsTestProject, "logs-token").then(
      ({ token, projectId }) => {
        searchAPIUrl = new RegExp(
          `/api/v4/projects/${projectId}/observability/v1/logs/search(\\?.*)?$`,
        );
        seedLogs(token, projectId, logs);
        cy.wait(5000);
      },
    );
  });

  function overrideServiceName(req) {
    // Note: because of an SDK/log-seeder limitation the log service is not populated by the API, so we are injecting it into the response manually.
    // At the same we need to remove it from the API query params as it would cause an empty response otherwise
    delete req.query.service_name;
    req.continue((res) => {
      res.body.results?.forEach((log) => {
        log.service_name = logs.find((l) => l.body === log.body)?.serviceName;
      });
    });
  }

  beforeEach(() => {
    cy.loginWithUser(defaultUser);

    cy.intercept(
      {
        method: "GET",
        url: searchAPIUrl,
      },
      (req) => {
        overrideServiceName(req);
      },
    );
  });

  describe("logs list", () => {
    it("contains the correct content per row", () => {
      cy.visit(`${logsTestProject}/-/logs`);

      // charts with logs metrics should exists
      cy.get("[_echarts_instance_]").should("exist");

      cy.contains(`Showing ${logs.length} logs`).should("exist");

      logs.forEach((log) => {
        cy.contains(log.body).parents("tr").as("row");
        cy.get("@row").contains(log.severity).should("exist");
        cy.get("@row").contains(log.serviceName).should("exist");
        cy.get("@row")
          .contains(dateFormat(log.timestamp, "mmm dd yyyy HH:MM:ss.l Z"))
          .should("exist");
      });
    });

    it("shows the details drawer", () => {
      cy.visit(`${logsTestProject}/-/logs`);

      // default params
      cy.url().should("include", "/-/logs?search=&date_range=1h");
      cy.gitlabDateRangeFilter().contains("Last 1 hour").should("exist");

      const log = logs[0];

      cy.contains(log.body).click();

      cy.get(".gl-drawer").as("drawer");
      cy.get("@drawer").should("be.visible");

      cy.get("@drawer").contains(
        dateFormat(log.timestamp, "mmm dd yyyy HH:MM:ss.l Z"),
      );
      cy.get("@drawer").contains(`body ${log.body}`);
      cy.get("@drawer").contains(`service_name ${log.serviceName}`);
      cy.get("@drawer").contains(`severity_text ${log.severity}`);
      // Note that logs attribute are currently base64 encoded by the logs seeder because of an SDK limitation
      cy.get("@drawer").contains(`app ${btoa(log.appName)}`);
    });

    it("shares the log entry", () => {
      cy.visit(`${logsTestProject}/-/logs`);

      const log = logs[0];

      cy.contains(log.body).click();

      cy.get(".gl-drawer").as("drawer");

      cy.gitlabDrawerSectionLineValueByLabels([
        "fingerprint",
        "timestamp",
        "severity_number",
        "trace_id",
        "service_name",
      ]).then(
        ({
          fingerprint,
          timestamp,
          severity_number,
          trace_id,
          service_name: serviceName,
        }) => {
          cy.intercept(
            {
              method: "GET",
              url: searchAPIUrl,
            },
            (req) => {
              expect(req.query.start_time).to.eq(timestamp);
              expect(req.query.end_time).to.eq(timestamp);
              expect(req.query.fingerprint).to.eq(fingerprint);
              expect(req.query.severity_number).to.eq(severity_number);
              expect(req.query.service_name).to.eq(serviceName);
              expect(req.query.trace_id).to.eq(trace_id);

              overrideServiceName(req);
            },
          );
        },
      );

      cy.reload();

      cy.contains(`Showing 1 logs`).should("exist");
      cy.get("@drawer").should("be.visible");
      cy.get("@drawer").contains(`body ${log.body}`);
    });

    describe("logs search", () => {
      describe("period filters", () => {
        const filters = {
          "5m": "Last 5 minutes",
          "15m": "Last 15 minutes",
          "30m": "Last 30 minutes",
          "1h": "Last 1 hour",
          "4h": "Last 4 hours",
          "12h": "Last 12 hours",
          "24h": "Last 24 hours",
          "7d": "Last 7 days",
          "14d": "Last 14 days",
          "30d": "Last 30 days",
        };
        Object.entries(filters).forEach(([period, filterText]) => {
          it(`filters ${period} correctly`, () => {
            // check that API query params are set correctly
            cy.intercept(
              {
                method: "GET",
                url: searchAPIUrl,
              },
              (req) => {
                expect(req.query.period).to.eq(period);
                req.continue();
              },
            ).as("searchLogs");
            cy.visit(`${logsTestProject}/-/logs?date_range=${period}`);

            cy.gitlabDateRangeFilter().contains(filterText).should("exist");

            cy.wait("@searchLogs");

            cy.contains("Failed to load").should("not.exist");
          });
        });

        it("handles custom date ranges", () => {
          const startDate = new Date(logs[0].timestamp - 1000);
          const endDate = new Date(logs[logs.length - 1].timestamp + 1000);
          // check that API query params are set correctly
          cy.intercept(
            {
              method: "GET",
              url: searchAPIUrl,
            },
            (req) => {
              expect(req.query.start_time).to.eq(startDate.toISOString());
              expect(req.query.end_time).to.eq(endDate.toISOString());
              req.continue();
            },
          ).as("searchLogs");
          cy.visit(
            `${logsTestProject}/-/logs?date_range=custom&date_start=${startDate.toISOString()}&date_end=${endDate.toISOString()}`,
          );
          cy.gitlabDateRangeFilter().contains("Custom").should("exist");

          gitlabCustomDateRangeFilterShouldMatch(startDate, endDate);

          cy.wait("@searchLogs");

          cy.contains("Failed to load").should("not.exist");
        });
      });

      describe("attributes filters", () => {
        it("filters correctly", () => {
          const searchQuery = "foobar";
          const testFilters = {
            service: {
              key: "service_name",
              value: "test-service",
              description: "Service",
            },
            severityName: {
              key: "severity_name",
              value: "info",
              description: "Severity",
            },
            severityNumber: {
              key: "severity_number",
              value: "9",
              description: "Severity Number",
            },
            traceId: {
              key: "trace_id",
              value: generateTraceID(),
              description: "Trace ID",
            },
            spanId: {
              key: "span_id",
              value: "0000000000000001",
              description: "Span ID",
            },
            fingerprint: {
              key: "fingerprint",
              value: "b6c9c0f68bba4197",
              description: "Fingerprint",
            },
            traceFlags: {
              key: "trace_flags",
              value: "0",
              description: "Trace Flags",
            },
            attribute: {
              value: "foo=bar",
              description: "Attribute",
            },
            resourceAttribute: {
              value: "asd=baz",
              description: "Resource Attribute",
            },
          };

          let url = `${logsTestProject}/-/logs?search=${searchQuery}`;
          Object.entries(testFilters).forEach(([filterName, filter]) => {
            url += `&${filterName}[]=${encodeURIComponent(filter.value)}`;
          });

          cy.intercept(
            {
              method: "GET",
              url: searchAPIUrl,
            },
            (req) => {
              expect(req.query.body).to.eq(searchQuery);
              expect(req.query.log_attr_name).to.eq("foo");
              expect(req.query.log_attr_value).to.eq("bar");
              expect(req.query.res_attr_name).to.eq("asd");
              expect(req.query.res_attr_value).to.eq("baz");
              Object.values(testFilters).forEach(({ key, value }) => {
                if (key) {
                  expect(req.query[key]).to.eq(value);
                }
              });
              req.continue();
            },
          ).as("searchLogs");

          cy.visit(url);

          cy.gitlabSearchFilters().then(($filters) => {
            cy.wrap($filters).should(
              "have.length",
              Object.values(testFilters).length,
            );
            [...$filters].forEach(($filter, index) => {
              const { description, value } = Object.values(testFilters)[index];
              cy.wrap($filter).contains(description);
              cy.wrap($filter).contains("=");
              cy.wrap($filter).contains(value);
            });
          });
          cy.get("[data-testid=filtered-search-term]")
            .contains(searchQuery)
            .should("exist");

          cy.wait("@searchLogs");

          cy.contains("Failed to load").should("not.exist");
        });
      });
    });

    describe("issues integration", () => {
      it("creates an issue from a log", () => {
        cy.visit(`${logsTestProject}/-/logs`);

        const log = logs[0];

        cy.contains(log.body).click();

        cy.get(".gl-drawer").as("drawer");

        // check that there are no linked issues
        cy.get("@drawer").contains("View issues 0").should("exist");

        cy.gitlabDrawerSectionLineValueByLabels([
          "fingerprint",
          "timestamp",
          "severity_number",
          "trace_id",
          "service_name",
        ]).then(
          ({
            fingerprint,
            timestamp,
            severity_number: severityNumber,
            trace_id: traceId,
            service_name: serviceName,
          }) => {
            // start issue creation flow from metrics page
            cy.get("@drawer").contains("Create issue").click();

            // submit issue creation form, with prefilled values
            cy.contains("Create issue").click();

            // check that the created issue has the correct information
            const expectedIssueTitle = `Issue created from log of '${serviceName}' service at ${timestamp}`;
            cy.contains(expectedIssueTitle).should("exist");

            cy.get(".description").as("issueDescription");
            cy.get("@issueDescription")
              .contains(`Service: ${serviceName}`)
              .should("exist");
            cy.get("@issueDescription")
              .contains(`Trace ID: ${traceId}`)
              .should("exist");
            cy.get("@issueDescription")
              .contains(`Log Fingerprint: ${fingerprint}`)
              .should("exist");
            cy.get("@issueDescription")
              .contains(`Severity Number: ${severityNumber}`)
              .should("exist");
            cy.get("@issueDescription")
              .contains(`Timestamp: ${timestamp}`)
              .should("exist");
            cy.get("@issueDescription").contains(`${log.body}`).should("exist");

            // go to the related log
            cy.contains("Log details").click();

            // check we are on the log page
            cy.url().should(
              "include",
              `/-/logs?search=&service[]=${serviceName}&severityNumber[]=${severityNumber}&traceId[]=${traceId}&fingerprint[]=${fingerprint}&timestamp=${encodeURIComponent(timestamp)}&drawerOpen=true`,
            );

            gitlabCustomDateRangeFilterShouldMatch(timestamp, timestamp);

            cy.contains(`Showing 1 logs`).should("exist");
            cy.get("@drawer").should("be.visible");

            // check that the newly created issue is linked on the log page
            cy.get("@drawer").contains("View issues 1").should("exist");

            // click on the linked issue
            cy.get("@drawer").contains(expectedIssueTitle).click();

            // check we are on the newly created issue page
            cy.url().should("include", `/-/issues/`);
            cy.contains(expectedIssueTitle).should("exist");
          },
        );
      });
    });
  });
});
