import dateFormat from "dateformat";
import { defaultUser } from "../constants";
import { seedTraces, getTraceService, generateTraceID } from "../utils";

describe("tracing", () => {
  let tracingTestProject = "tracing-e2e/tracing-" + Date.now();

  const now = new Date();
  // for now our trace generator rounds to the
  // nearest second so this ensures we can test
  // for the right value to show on the trace detail page
  now.setMilliseconds(0);

  // the following values are hardcoded in the trace generator
  const testOperationName = "article-to-cart";
  const testNumberOfSpans = 10;

  let searchAPIUrl;

  const traces = [
    {
      id: generateTraceID(),
      duration: 400000000,
      formattedDuration: "400ms",
      start: now,
    },
    {
      id: generateTraceID(),
      duration: 450000000,
      formattedDuration: "450ms",
      start: new Date(now - 60000), // 1 min ago
    },
    {
      id: generateTraceID(),
      duration: 410000000,
      formattedDuration: "410ms",
      start: new Date(now - 120000), // 2 mins ago
    },
  ];

  before(() => {
    cy.setupProjectPermissions(
      defaultUser,
      tracingTestProject,
      "trace-token",
    ).then(({ token, projectId }) => {
      searchAPIUrl = `/api/v4/projects/${projectId}/observability/v1/traces*`;
      seedTraces(token, projectId, traces);
      cy.wait(5000);
    });
  });

  beforeEach(() => {
    cy.loginWithUser(defaultUser);
  });

  describe("tracing list page", () => {
    it("contains correct content for row", () => {
      cy.visit(`${tracingTestProject}/-/tracing`);

      cy.get(".analytics-chart").should("have.length", 3);

      // default params
      cy.url().should(
        "include",
        "/-/tracing?date_range=1h&sortBy=timestamp_desc",
      );

      // test filters show in filter bar
      cy.gitlabDateRangeFilter().contains("Last 1 hour").should("exist");
      // test correct sort directive rendered
      cy.gitLabSortBy().contains("Timestamp").should("exist");
      cy.gitLabSortBy().get("[data-testid=sort-highest-icon]").should("exist");

      traces.forEach((trace) => {
        const nowFormattedString = formatDate(trace.start);
        const traceService = getTraceService(trace.id);

        cy.contains(traceService).parents("tr").as("row");

        cy.get("@row").contains(nowFormattedString).should("exist");

        cy.get("@row").contains(testOperationName).should("exist");

        cy.get("@row").contains(trace.formattedDuration).should("exist");
      });
    });

    describe("attributes filters", () => {
      it("filters correctly", () => {
        const testFilters = {
          service: {
            key: "service_name",
            value: "test-service",
            description: "Service",
          },
          operation: {
            key: "operation",
            value: "info",
            description: "Operation",
          },
          trace_id: {
            key: "trace_id",
            value: "00000000-0000-0000-0000-000000000000",
            description: "Trace ID",
          },
          "gt[durationMs]": {
            value: "123",
            description: "Duration (ms)",
            operator: ">",
          },
          attribute: {
            value: "foo=bar",
            description: "Attribute",
          },
        };

        let url = `${tracingTestProject}/-/tracing?`;
        Object.entries(testFilters).forEach(([filterName, filter]) => {
          url += `&${filterName}[]=${encodeURIComponent(filter.value)}`;
        });

        cy.intercept(
          {
            method: "GET",
            url: searchAPIUrl,
          },
          (req) => {
            expect(req.query.attr_name).to.eq("foo");
            expect(req.query.attr_value).to.eq("bar");
            expect(req.query["gt[duration_nano]"]).to.eq("123000000");

            Object.values(testFilters).forEach(({ key, value }) => {
              if (key) {
                expect(req.query[key]).to.eq(value);
              }
            });
            req.continue();
          },
        ).as("searchTraces");

        cy.visit(url);

        cy.gitlabSearchFilters().then(($filters) => {
          cy.wrap($filters).should(
            "have.length",
            Object.values(testFilters).length,
          );
          [...$filters].forEach(($filter, index) => {
            const { description, value, operator } =
              Object.values(testFilters)[index];
            cy.wrap($filter).contains(description);
            cy.wrap($filter).contains(operator || "=");
            cy.wrap($filter).contains(value);
          });
        });

        cy.wait("@searchTraces");

        cy.contains("Failed to load").should("not.exist");
      });
    });

    describe("period filters", () => {
      const filters = {
        "5m": "Last 5 minutes",
        "15m": "Last 15 minutes",
        "30m": "Last 30 minutes",
        "1h": "Last 1 hour",
        "4h": "Last 4 hours",
        "12h": "Last 12 hours",
      };
      Object.entries(filters).forEach(([period, filterText]) => {
        it(`filters ${period} correctly`, () => {
          // intercept the API request
          cy.intercept(searchAPIUrl, (req) => {
            // expect the correct query param to be passed to the API
            expect(req.query.period).to.eq(period);
            req.continue();
          }).as("searchTraces");
          cy.visit(`${tracingTestProject}/-/tracing?date_range=${period}`);

          // test filters show in filter bar
          cy.gitlabDateRangeFilter().contains(filterText).should("exist");

          cy.wait("@searchTraces");
          cy.contains("Failed to load").should("not.exist");
        });
      });

      it("handles custom date ranges", () => {
        // intercept the API request
        cy.intercept(searchAPIUrl, (req) => {
          // expect the correct query param to be passed to the API
          expect(req.query.start_time).to.eq(now.toISOString());
          expect(req.query.end_time).to.eq(now.toISOString());
          req.continue();
        }).as("searchTraces");
        cy.visit(
          `${tracingTestProject}/-/tracing?date_range=custom&date_start=${now.toISOString()}&date_end=${now.toISOString()}`,
        );
        // test filters show in filter bar
        cy.gitlabDateRangeFilter().contains("Custom").should("exist");

        cy.wait("@searchTraces");

        cy.contains("Failed to load").should("not.exist");
      });
    });

    describe("sorting", () => {
      [
        {
          param: "timestamp_desc",
          text: "Timestamp",
          icon: "sort-highest-icon",
        },
        {
          param: "timestamp_asc",
          text: "Timestamp",
          icon: "sort-lowest-icon",
        },
        {
          param: "duration_desc",
          text: "Duration",
          icon: "sort-highest-icon",
        },
        {
          param: "duration_asc",
          text: "Duration",
          icon: "sort-lowest-icon",
        },
      ].forEach((sort) => {
        it(`sorts ${sort.param} correctly`, () => {
          // intercept the API request
          cy.intercept(searchAPIUrl, (req) => {
            // expect the correct query param to be passed to the API
            expect(req.query.sort).to.eq(sort.param);
            req.continue();
          }).as("searchTraces");

          cy.visit(`${tracingTestProject}/-/tracing?sortBy=${sort.param}`);
          // test sort renders correctly
          cy.gitLabSortBy().contains(sort.text).should("exist");

          cy.gitLabSortBy().get(`[data-testid=${sort.icon}]`).should("exist");

          cy.wait("@searchTraces");

          cy.contains("Failed to load").should("not.exist");
        });
      });
    });
  });

  describe("trace detail", () => {
    const testTrace = traces[0];
    const traceId = testTrace.id;
    const traceService = getTraceService(traceId);
    const traceFormattedDuration = testTrace.formattedDuration;

    it("opens trace details from trace list", () => {
      cy.visit(`${tracingTestProject}/-/tracing`);

      cy.contains(traceService).click();

      cy.url().should("include", `/-/tracing/${traceId}`);

      cy.get("[data-testid=trace-date-card]").contains(
        formatDate(testTrace.start, "mmm d, yyyy"),
      );

      cy.get("[data-testid=trace-date-card]").contains(
        formatDate(testTrace.start, "H:MM:ss.l Z"),
      );

      cy.get("[data-testid=trace-duration-card]").contains(
        traceFormattedDuration,
      );

      cy.get("[data-testid=trace-spans-card]").contains(testNumberOfSpans);
      cy.get("[data-testid=span-details]").should(
        "have.length",
        testNumberOfSpans,
      );
    });

    describe("issues integration", () => {
      it("creates an issue from a trace", () => {
        cy.visit(`${tracingTestProject}/-/tracing/${traceId}`);

        // check that there are no linked issues
        cy.contains("View issues 0").should("exist");

        // start issue creation flow from tracing page
        cy.contains("Create issue").click();

        cy.contains("New issue").should("exist");

        // submit issue creation form, with prefilled values
        cy.contains("Create issue").click();

        // check that the created issue has the correct information
        const expectedIssueTitle = `Issue created from trace '${traceService} : ${testOperationName}'`;
        cy.contains(expectedIssueTitle).should("exist");
        cy.contains(`Name: ${traceService} : ${testOperationName}`).should(
          "exist",
        );
        cy.contains(`Trace ID: ${traceId}`).should("exist");
        cy.contains(
          `Trace start: ${new Date(testTrace.start).toUTCString()}`,
        ).should("exist");
        cy.contains(`Duration: ${traceFormattedDuration}`).should("exist");
        cy.contains(`Total spans: ${testNumberOfSpans}`).should("exist");
        cy.contains(`Total errors: 0`).should("exist");

        // go to the related trace
        cy.contains("Trace details").click();

        // check we are on the trace detail page
        cy.url().should("include", `/-/tracing/${traceId}`);

        // check that the newly created issue is linked on the trace page
        cy.contains("View issues 1").should("exist");

        // click on the linked issue
        cy.contains(expectedIssueTitle).click();

        // check we are on the newly created issue page
        cy.url().should("include", `/-/issues/`);
        cy.contains(expectedIssueTitle).should("exist");
      });
    });
  });
});

// formats datetime to be same as GitLab UI
function formatDate(datetime, fmt = "mmm dd yyyy HH:MM:ss.l Z") {
  return dateFormat(datetime.toISOString(), fmt, false);
}
