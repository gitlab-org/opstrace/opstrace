import * as gitlab from "./gitlab";
import "cypress-wait-until";

Cypress.Commands.add("loginWithUser", (user) => {
  if (!user.password) {
    throw Error("user.password not specified");
  }
  if (!user.username) {
    throw Error("user.username not specified");
  }
  cy.wrap(null).then(() =>
    gitlab.getUser(user).then((u) => {
      cy.session([u.username, u.password], () => {
        cy.visit("/users/sign_in");
        cy.get("[data-testid=username-field]").type(u.username);
        cy.get("[data-testid=password-field]").type(u.password);
        cy.get("[data-testid=sign-in-button]").click();
        cy.url().should("eq", `${Cypress.env("TEST_GITLAB_ADDRESS")}/`);
      });
    }),
  );
});

Cypress.Commands.add("gitlabSearchFilters", () => {
  return cy.get("[data-testid=filtered-search-token]");
});

Cypress.Commands.add("gitlabDateRangeFilter", () => {
  return cy.get("[data-testid=date-range-filter]");
});

Cypress.Commands.add("gitlabCustomDateRangeFilter", () => {
  return cy.gitlabDateRangeFilter().then(($filter) => {
    return cy
      .wrap($filter)
      .find('[data-testid="gl-datepicker-input"]')
      .should("have.length", 2)
      .then(($inputs) => {
        const startDateInput = $inputs[0];
        const endDateInput = $inputs[1];
        return cy
          .wrap($filter)
          .find('input[type="time"]')
          .should("have.length", 2)
          .then(($times) => {
            const startTimeInput = $times[0];
            const endTimeInput = $times[1];
            return {
              startDateInput,
              startTimeInput,
              endDateInput,
              endTimeInput,
            };
          });
      });
  });
});

Cypress.Commands.add("gitlabDrawerSectionLineValueByLabels", (labels) => {
  const results = {};

  return cy
    .wrap(null)
    .then(() => {
      labels.forEach((labelText) => {
        cy.get(".gl-drawer")
          .contains('[data-testid="section-line-name"]', labelText)
          .parent()
          .find('[data-testid="section-line-value"]')
          .invoke("text")
          .then((value) => {
            results[labelText] = value.trim();
          });
      });
    })
    .then(() => results);
});

Cypress.Commands.add("gitLabSortBy", () => {
  return cy.get(".sort-dropdown-container");
});

Cypress.Commands.add("getUser", (user) => {
  cy.wrap(null).then(() => gitlab.getUser(user));
});

Cypress.Commands.add("getProject", (path) => {
  cy.wrap(null).then(() => gitlab.getProject(path));
});

Cypress.Commands.add("getGroup", (path) => {
  cy.wrap(null).then(() => gitlab.getGroup(path));
});

Cypress.Commands.add(
  "addProjectMembership",
  (projectID, userID, accessLevel) => {
    cy.wrap(null).then(() =>
      gitlab.addProjectMembership(projectID, userID, accessLevel),
    );
  },
);

Cypress.Commands.add("getProjectAccessToken", (projectID, name, scopes) => {
  cy.wrap(null).then(() =>
    gitlab.getProjectAccessToken(projectID, name, scopes),
  );
});

Cypress.Commands.add("getProjectSentryDSN", (projectID) => {
  cy.wrap(null).then(() => gitlab.getProjectSentryDSN(projectID));
});

Cypress.Commands.add(
  "setupProjectPermissions",
  (user, testProject, tokenName) => {
    cy.wrap(null).then(() =>
      cy.getUser(user).then(() => {
        return cy.getProject(testProject).then((project) => {
          return cy.addProjectMembership(project.id, user.id, 30).then(() => {
            return cy
              .getProjectAccessToken(project.id, tokenName, ["api"])
              .then((token) => ({
                token,
                projectId: project.id,
              }));
          });
        });
      }),
    );
  },
);
