//
// Sends metrics to ingest API with configurable attributes.
// This can be used both from Go or Cypress tests.
//
// For sums/gauges, pass datapoints in the format - starttimeunix:timeunix:value:attributes_list, e.g.
//
//	go run metrics.go \
//		-endpoint "https://gdk.devvm:3443/api/v4/projects/2/observability/v1/metrics" \
//		-token gl-pat-token \
//		-mname sample-metric-name \
//		-mtype sum|gauge \
//		-datapoint 1728288607668:1728288607668:1:k1=v1,k2=v2 \
//		-datapoint 1728288608668:1728288608668:2:k1=v1,k2=v2 \
//		-datapoint 1728288609668:1728288609668:3:k1=v1,k2=v2
//
// or for histograms, pass datapoints in the format - starttimeunix:timeunix:bucket_counts:attributes_list, e.g.
//
//	go run metrics.go \
//		-endpoint "https://gdk.devvm:3443/api/v4/projects/2/observability/v1/metrics" \
//		-token gl-pat-token \
//		-mname sample-metric-name \
//		-mtype histogram \
//		-datapoint 1728288607668:1728288607668:1,2,3,4,5,6:k1=v1,k2=v2 \
//		-datapoint 1728288608668:1728288608668:2,4,5,6,7,8:k1=v1,k2=v2 \
//		-datapoint 1728288609668:1728288609668:3,6,7,8,9,9:k1=v1,k2=v2
//
// note, we use default bounds for now [1.0, 2.0, 3.0, 4.0, 5.0]

package main

import (
	"context"
	"crypto/tls"
	"errors"
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"

	"go.opentelemetry.io/collector/pdata/pmetric"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/exporters/stdout/stdoutmetric"
	"go.opentelemetry.io/otel/sdk/instrumentation"
	"go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/metric/metricdata"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
	"go.opentelemetry.io/otel/trace"
)

var (
	errUnsupportedMetricType = errors.New("unsupported metric type")
	defaultHistogramBounds   = []float64{1, 2, 3, 4, 5}
)

func main() {
	debug := flag.Bool("debug", false, "Output generated metrics to STDOUT")
	token := flag.String("token", "", "GitLab access token")
	endpoint := flag.String("endpoint", "gob.local", "GOB endpoint for sending metrics to")
	mname := flag.String("mname", "sample_metric_name", "Name of the metric being generated")
	mtype := flag.String("mtype", "sum", "Type of the metric being generated")

	var datapoints arrayFlags
	flag.Var(&datapoints, "datapoint", "datapoint")

	flag.Parse()

	if *token == "" {
		panic("-token required")
	}

	t := getMetricTypeFromString(*mtype)
	if t == pmetric.MetricTypeEmpty {
		panic("metric type empty or unsupported")
	}

	mr := MetricsRequest{
		MetricName: *mname,
		MetricType: t,
	}

	rm, err := mr.generate(datapoints)
	if err != nil {
		panic(err)
	}

	err = mr.send(*debug, *endpoint, *token, rm)
	if err != nil {
		panic(err)
	}
}

type MetricsRequest struct {
	MetricName string
	MetricType pmetric.MetricType
}

func (mr *MetricsRequest) send(debug bool, endpoint, token string, metrics *metricdata.ResourceMetrics) error {
	var (
		exporter metric.Exporter
		err      error
	)
	ctx := context.Background()
	if debug {
		exporter, err = stdoutmetric.New(stdoutmetric.WithPrettyPrint())
	} else {
		exporter, err = otlpmetrichttp.New(
			ctx,
			otlpmetrichttp.WithTLSClientConfig(&tls.Config{InsecureSkipVerify: true}), //nolint:gosec
			otlpmetrichttp.WithEndpointURL(endpoint),
			otlpmetrichttp.WithHeaders(
				map[string]string{
					"PRIVATE-TOKEN": token,
				},
			),
		)
	}
	if err != nil {
		return fmt.Errorf("initializing exporter: %w", err)
	}
	defer func() {
		if err := exporter.Shutdown(ctx); err != nil {
			fmt.Println(err)
		}
	}()
	if err := exporter.Export(ctx, metrics); err != nil {
		return fmt.Errorf("exporting telemetry: %w", err)
	}
	return nil
}

func (mr *MetricsRequest) generate(dps []string) (*metricdata.ResourceMetrics, error) {
	var data metricdata.Aggregation

	switch mr.MetricType { //nolint:exhaustive
	case pmetric.MetricTypeSum:
		dpsArr := parseCounterDatapoints(dps)
		datapoints := make([]metricdata.DataPoint[float64], 0)
		for _, sumdp := range dpsArr {
			datapoints = append(datapoints, metricdata.DataPoint[float64]{
				Attributes: sumdp.Attributes,
				StartTime:  sumdp.StartTimeUnix,
				Time:       sumdp.TimeUnix,
				Value:      sumdp.Value,
				Exemplars: []metricdata.Exemplar[float64]{
					{
						TraceID: sumdp.TraceID,
					},
				},
			})
		}

		data = metricdata.Sum[float64]{
			Temporality: metricdata.DeltaTemporality,
			DataPoints:  datapoints,
		}
	case pmetric.MetricTypeGauge:
		dpsArr := parseCounterDatapoints(dps)
		datapoints := make([]metricdata.DataPoint[float64], 0)

		for _, gaugedp := range dpsArr {
			datapoints = append(datapoints, metricdata.DataPoint[float64]{
				Attributes: gaugedp.Attributes,
				StartTime:  gaugedp.StartTimeUnix,
				Time:       gaugedp.TimeUnix,
				Value:      gaugedp.Value,
				Exemplars: []metricdata.Exemplar[float64]{
					{
						TraceID: gaugedp.TraceID,
					},
				},
			})
		}
		data = metricdata.Gauge[float64]{
			DataPoints: datapoints,
		}
	case pmetric.MetricTypeHistogram:
		dpsArr := parseHistogramDatapoints(dps)
		datapoints := make([]metricdata.HistogramDataPoint[float64], 0)
		for _, hdp := range dpsArr {
			datapoints = append(datapoints, metricdata.HistogramDataPoint[float64]{
				Attributes:   hdp.Attributes,
				StartTime:    hdp.StartTimeUnix,
				Time:         hdp.TimeUnix,
				Count:        hdp.Count,
				Sum:          hdp.Sum,
				Min:          metricdata.NewExtrema(hdp.Min),
				Max:          metricdata.NewExtrema(hdp.Max),
				BucketCounts: hdp.BucketCounts,
				Bounds:       hdp.Bounds,
				Exemplars: []metricdata.Exemplar[float64]{
					{
						TraceID: hdp.TraceID,
					},
				},
			})
		}
		data = metricdata.Histogram[float64]{
			Temporality: metricdata.DeltaTemporality,
			DataPoints:  datapoints,
		}
	default:
		return nil, errUnsupportedMetricType
	}

	rm := &metricdata.ResourceMetrics{
		Resource: resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName("metrics-generator"),
			semconv.ServiceVersion("1.0.0"),
		),
		ScopeMetrics: []metricdata.ScopeMetrics{
			{
				Scope: instrumentation.Scope{
					Name:      "observe.gitlab.com",
					Version:   "v0.0.0",
					SchemaURL: "observe.gitlab.com/schema/0.0.0",
				},
				Metrics: []metricdata.Metrics{
					{
						Name:        mr.MetricName,
						Description: "sample_description",
						Unit:        "sample_metric_unit",
						Data:        data,
					},
				},
			},
		},
	}

	return rm, nil
}

func getMetricTypeFromString(t string) pmetric.MetricType {
	switch strings.ToLower(t) {
	case "gauge":
		return pmetric.MetricTypeGauge
	case "sum":
		return pmetric.MetricTypeSum
	case "histogram":
		return pmetric.MetricTypeHistogram
	case "exponentialhistogram":
		return pmetric.MetricTypeExponentialHistogram
	default:
		return pmetric.MetricTypeEmpty
	}
}

type CounterDatapoint struct {
	StartTimeUnix time.Time
	TimeUnix      time.Time
	Value         float64
	Attributes    attribute.Set
	TraceID       []byte
}

func parseCounterDatapoints(datapoints []string) []CounterDatapoint {
	counterDatapoints := make([]CounterDatapoint, 0)
	for _, dp := range datapoints {
		dpComps := strings.Split(dp, ":")
		if len(dpComps) != 5 {
			panic("malformed datapoint")
		}

		tid, err := trace.TraceIDFromHex(dpComps[4])
		if err != nil {
			panic("malformed trace ID")
		}

		counterDatapoints = append(counterDatapoints, CounterDatapoint{
			StartTimeUnix: parseTime(dpComps[0]),
			TimeUnix:      parseTime(dpComps[1]),
			Value:         parseFloat64(dpComps[2]),
			Attributes:    parseAttributes(dpComps[3]),
			TraceID:       tid[:],
		})
	}
	return counterDatapoints
}

type HistogramDatapoint struct {
	StartTimeUnix time.Time
	TimeUnix      time.Time
	Count         uint64
	Sum           float64
	Min           float64
	Max           float64
	BucketCounts  []uint64
	Bounds        []float64
	Attributes    attribute.Set
	TraceID       []byte
}

func parseHistogramDatapoints(datapoints []string) []HistogramDatapoint {
	histoDatapoints := make([]HistogramDatapoint, 0)
	for _, dp := range datapoints {
		dpComps := strings.Split(dp, ":")
		if len(dpComps) != 5 {
			panic("malformed datapoint")
		}
		// parse bucket counts
		bucketCountsStr := dpComps[2]
		bucketCountsComps := strings.Split(bucketCountsStr, ",")
		bucketCounts := make([]uint64, 0)
		for _, t := range bucketCountsComps {
			bucketCounts = append(bucketCounts, parseUInt64(t))
		}
		if len(bucketCounts) != len(defaultHistogramBounds)+1 {
			panic("malformed bucket counts")
		}
		tid, err := trace.TraceIDFromHex(dpComps[4])
		if err != nil {
			panic("malformed trace ID")
		}
		histoDatapoints = append(histoDatapoints, HistogramDatapoint{
			StartTimeUnix: parseTime(dpComps[0]),
			TimeUnix:      parseTime(dpComps[1]),
			Bounds:        defaultHistogramBounds,
			Count:         1, // random value
			Sum:           1, // random value
			Min:           0, // random value
			Max:           1, // random value
			BucketCounts:  bucketCounts,
			Attributes:    parseAttributes(dpComps[3]),
			TraceID:       tid[:],
		})
	}
	return histoDatapoints
}

type arrayFlags []string

func (i *arrayFlags) String() string {
	return strings.Join(*i, ", ")
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func parseTime(ts string) time.Time {
	ms, err := strconv.ParseInt(ts, 10, 64)
	if err != nil {
		panic(err)
	}
	return time.Unix(0, ms*int64(time.Millisecond))
}

func parseAttributes(attrs string) attribute.Set {
	attrsKv := make([]attribute.KeyValue, 0)
	kvPairs := strings.Split(attrs, ",")
	for _, kv := range kvPairs {
		parts := strings.SplitN(kv, "=", 2)
		if len(parts) == 2 {
			attrsKv = append(attrsKv, attribute.String(parts[0], parts[1]))
		}
	}
	return attribute.NewSet(attrsKv...)
}

func parseFloat64(val string) float64 {
	s, err := strconv.ParseFloat(val, 64)
	if err != nil {
		panic(err)
	}
	return s
}

func parseUInt64(val string) uint64 {
	s, err := strconv.ParseUint(val, 10, 64)
	if err != nil {
		panic(err)
	}
	return s
}
