package main

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"

	otlploghttp "go.opentelemetry.io/otel/exporters/otlp/otlplog/otlploghttp"
	log "go.opentelemetry.io/otel/log"
	logsdk "go.opentelemetry.io/otel/sdk/log"
	"go.opentelemetry.io/otel/trace"
)

// Can use this on the CLI
//
//	go run log.go \
//			-token TOKEN \
//			-endpoint "gitlab.com/api/v4/projects/xxxxxx/observability/v1/logs"
//			"08040201000000000000000000000000#1727740800000#shop-backend#INFO#User logged in" \
//			"08040201000000000000000000000000#1727740800000#auth-service#DEBUG#Token validated" \
//			"08040201000000000000000000000000#1727740800000#article-service#ERROR#Failed to retrieve article" | jq
func main() {
	token := flag.String("token", "", "GitLab access token")
	endpoint := flag.String("endpoint", "gob.local", "GOB endpoint for posting logs to")

	flag.Parse()

	if *token == "" {
		panic("-token required")
	}

	args := flag.Args()
	loglines := make([]string, 0)
	loglines = append(loglines, args...)

	records := generate(loglines)
	if err := send(*endpoint, *token, records); err != nil {
		panic(err)
	}
}

func generate(loglines []string) []logsdk.Record {
	records := make([]logsdk.Record, 0)

	for _, logline := range loglines {
		parts := strings.Split(logline, "#")
		if len(parts) != 5 {
			panic("log line must be of form traceId#timestamp#appName#severity#message")
		}

		t := parseTime(parts[1])
		record := logsdk.Record{}

		tid, err := trace.TraceIDFromHex(parts[0])
		if err != nil {
			panic("malformed trace ID")
		}
		record.SetTraceID(tid)
		record.SetTimestamp(t)
		record.SetAttributes(log.Bytes("app", []byte(parts[2])))
		record.SetSeverityText(parts[3])
		record.SetSeverity(log.Severity(getSeverityNumber(parts[3])))
		record.SetBody(log.StringValue(parts[4]))

		records = append(records, record)
	}

	return records
}

func send(endpoint, token string, records []logsdk.Record) error {
	var (
		exporter logsdk.Exporter
		err      error
	)
	ctx := context.TODO()
	exporter, err = otlploghttp.New(
		ctx,
		otlploghttp.WithTLSClientConfig(&tls.Config{InsecureSkipVerify: true}), //nolint:gosec
		otlploghttp.WithEndpointURL(endpoint),
		otlploghttp.WithHeaders(
			map[string]string{
				"PRIVATE-TOKEN": token,
			},
		),
	)

	if err != nil {
		return fmt.Errorf("initializing exporter: %w", err)
	}

	defer func() {
		if err := exporter.Shutdown(ctx); err != nil {
			fmt.Println(err)
		}
	}()

	if err := exporter.Export(ctx, records); err != nil {
		return fmt.Errorf("exporting logs: %w", err)
	}
	return nil
}

//nolint:funlen,cyclop
func getSeverityNumber(severityText string) int32 {
	switch strings.ToUpper(severityText) {
	case "TRACE":
		return 1
	case "TRACE2":
		return 2
	case "TRACE3":
		return 3
	case "TRACE4":
		return 4
	case "DEBUG":
		return 5
	case "DEBUG2":
		return 6
	case "DEBUG3":
		return 7
	case "DEBUG4":
		return 8
	case "INFO":
		return 9
	case "INFO2":
		return 10
	case "INFO3":
		return 11
	case "INFO4":
		return 12
	case "WARN":
		return 13
	case "WARN2":
		return 14
	case "WARN3":
		return 15
	case "WARN4":
		return 16
	case "ERROR":
		return 17
	case "ERROR2":
		return 18
	case "ERROR3":
		return 19
	case "ERROR4":
		return 20
	case "FATAL":
		return 21
	case "FATAL2":
		return 22
	case "FATAL3":
		return 23
	case "FATAL4":
		return 24
	default:
		return 0 // UNSPECIFIED
	}
}

func parseTime(ts string) time.Time {
	millis, err := strconv.ParseInt(ts, 10, 64)
	if err != nil {
		panic(err)
	}
	return time.Unix(0, millis*int64(time.Millisecond))
}
