package tests

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"math/big"
	"net/http"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/lestrrat-go/jwx/v2/jwt"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/metrics"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlpmetric/otlpmetrichttp"
	"go.opentelemetry.io/otel/metric"
	sdkmetric "go.opentelemetry.io/otel/sdk/metric"
	"go.opentelemetry.io/otel/sdk/metric/metricdata"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
)

var _ = Describe("Metrics", Ordered, Serial, func() {
	// tokens for accessing the group/project
	var writeOnlyToken string

	var jwtToken string
	var otherJWTToken string

	// common attributes for generated test-metrics
	commonAttrs := []attribute.KeyValue{
		{
			Key:   attribute.Key("foo"),
			Value: attribute.StringValue("bar"),
		},
	}
	// for testing backward compatibility by sending to old endpoint
	// and reading from OIDC query API
	makeDeprecatedMetricsExporter := func(ctx SpecContext, apiKey, urlPath string) *otlpmetrichttp.Exporter {
		opts := []otlpmetrichttp.Option{
			// #nosec G402
			otlpmetrichttp.WithTLSClientConfig(&tls.Config{InsecureSkipVerify: true}),
			otlpmetrichttp.WithEndpoint(testInfra.Configuration().GOBHost),
			otlpmetrichttp.WithURLPath(urlPath),
			otlpmetrichttp.WithHeaders(
				map[string]string{
					"Private-Token": apiKey,
				},
			),
		}

		exporter, err := otlpmetrichttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())

		return exporter
	}

	sendDeprecatedMetrics := func(ctx SpecContext, apiKey string, metrics *metricdata.ResourceMetrics) error {
		// use standard ingest URL by default here as opposed to /v1/metrics
		url := fmt.Sprintf("/v3/%d/%d/ingest/metrics", gitLab.group.ID, gitLab.project.ID)
		exporter := makeDeprecatedMetricsExporter(ctx, apiKey, url)
		defer exporter.Shutdown(ctx)
		return exporter.Export(ctx, metrics)
	}

	makeMetricsExporter := func(ctx SpecContext, token string) *otlpmetrichttp.Exporter {
		opts := []otlpmetrichttp.Option{
			// #nosec G402
			otlpmetrichttp.WithTLSClientConfig(&tls.Config{InsecureSkipVerify: true}),
			otlpmetrichttp.WithEndpoint(testInfra.Configuration().GOBHost),
			otlpmetrichttp.WithURLPath("/observability/v1/metrics"),
			otlpmetrichttp.WithHeaders(
				map[string]string{
					"Authorization":         fmt.Sprintf("Bearer %s", token),
					"X-Gitlab-Realm":        "saas",
					"X-Gitlab-Instance-Id":  "instance1",
					"X-GitLab-Namespace-id": fmt.Sprint(gitLab.group.ID),
					"X-GitLab-Project-id":   fmt.Sprint(gitLab.project.ID),
				},
			),
		}

		exporter, err := otlpmetrichttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())

		return exporter
	}

	sendMetrics := func(ctx SpecContext, token string, metrics *metricdata.ResourceMetrics) error {
		exporter := makeMetricsExporter(ctx, token)
		defer exporter.Shutdown(ctx)
		return exporter.Export(ctx, metrics)
	}

	makeMeterReader := func() (metric.Meter, sdkmetric.Reader) {
		const service = "test_otlp_metric_api"
		r, err := resource.Merge(
			resource.Default(),
			resource.NewWithAttributes(
				"",
				semconv.ServiceNameKey.String(service),
				semconv.ServiceVersionKey.String("v0.1.0"),
				attribute.String("environment", "e2e"),
			))
		Expect(err).ToNot(HaveOccurred())

		reader := sdkmetric.NewManualReader()
		mp := sdkmetric.NewMeterProvider(sdkmetric.WithResource(r), sdkmetric.WithReader(reader))
		return mp.Meter(service), reader
	}

	generateMetric := func(ctx SpecContext) *metricdata.ResourceMetrics {
		meter, reader := makeMeterReader()

		randStr, err := common.RandStringASCIIBytes(8)
		Expect(err).ToNot(HaveOccurred())

		c, err := meter.Int64Counter("test_counter_"+randStr, metric.WithDescription("int64 test counter"))
		Expect(err).ToNot(HaveOccurred())
		c.Add(ctx, 2, metric.WithAttributes(commonAttrs...))

		g, err := meter.Float64ObservableGauge(
			"test_float_gauge_"+randStr,
			metric.WithDescription("float64 gauge"),
		)
		Expect(err).ToNot(HaveOccurred())
		n, err := rand.Int(rand.Reader, big.NewInt(100))
		Expect(err).ToNot(HaveOccurred())
		_, err = meter.RegisterCallback(func(_ context.Context, o metric.Observer) error {
			o.ObserveFloat64(g, float64(n.Int64()), metric.WithAttributes(commonAttrs...))
			return nil
		}, g)
		Expect(err).ToNot(HaveOccurred())

		h, err := meter.Float64Histogram(
			"test_histogram_"+randStr,
			metric.WithDescription("float64 histogram"))
		Expect(err).ToNot(HaveOccurred())
		h.Record(ctx, float64(n.Int64()), metric.WithAttributes(commonAttrs...))

		rm := &metricdata.ResourceMetrics{}
		Expect(reader.Collect(ctx, rm)).To(Succeed())
		return rm
	}

	makeMetricsRequest := func(token string) *http.Request {
		By("building query for metrics names")

		req, err := http.NewRequest("GET",
			fmt.Sprintf("%s/observability/v1/metrics/autocomplete", testInfra.Configuration().GOBAddress()), nil)
		Expect(err).ToNot(HaveOccurred())
		for key, val := range map[string]string{
			"Authorization":         fmt.Sprintf("Bearer %s", token),
			"X-Gitlab-Realm":        "saas",
			"X-Gitlab-Instance-Id":  "instance1",
			"X-GitLab-Namespace-id": fmt.Sprint(gitLab.group.ID),
			"X-GitLab-Project-id":   fmt.Sprint(gitLab.project.ID),
		} {
			req.Header.Set(key, val)
		}

		return req
	}

	makeMetricSearchRequest := func(token, metricName, metricType string) *http.Request {
		By("building search query for metric names")

		req, err := http.NewRequest(
			"GET",
			fmt.Sprintf("%s/observability/v1/metrics/search?mname=%s&mtype=%s",
				testInfra.Configuration().GOBAddress(), metricName, metricType),
			nil)
		Expect(err).ToNot(HaveOccurred())
		for key, val := range map[string]string{
			"Authorization":         fmt.Sprintf("Bearer %s", token),
			"X-Gitlab-Realm":        "saas",
			"X-Gitlab-Instance-Id":  "instance1",
			"X-GitLab-Namespace-id": fmt.Sprint(gitLab.group.ID),
			"X-GitLab-Project-id":   fmt.Sprint(gitLab.project.ID),
		} {
			req.Header.Set(key, val)
		}

		return req
	}

	queryForMetrics := func(token string, rm *metricdata.ResourceMetrics) *metrics.MetricNameResponse {
		By("querying for metric names")

		req := makeMetricsRequest(token)

		var res *metrics.MetricNameResponse
		// wait for up to ~60s for metric names to appear.
		Eventually(func(g Gomega) {
			resp, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer resp.Body.Close()
			g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))

			names := &metrics.MetricNameResponse{}
			g.Expect(json.NewDecoder(resp.Body).Decode(names)).To(Succeed())

			// verify that all the named metrics are present
			for _, sm := range rm.ScopeMetrics {
				for _, m := range sm.Metrics {
					g.Expect(names.Metrics).To(ContainElement(
						gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
							"Name": Equal(m.Name),
						}),
					))
				}
			}

			res = names
		}).WithTimeout(time.Minute).Should(Succeed())

		return res
	}

	searchForMetrics := func(token string, rm *metricdata.ResourceMetrics) {
		By("searching for metrics")

		// verify that all the named metrics are present
		for _, sm := range rm.ScopeMetrics {
			for _, m := range sm.Metrics {
				var metricType string
				switch v := m.Data.(type) {
				case metricdata.Gauge[int64], metricdata.Gauge[float64]:
					metricType = "Gauge"
				case metricdata.Sum[int64], metricdata.Sum[float64]:
					metricType = "Sum"
				case metricdata.Histogram[int64], metricdata.Histogram[float64]:
					metricType = "Histogram"
				case metricdata.Summary:
					metricType = "Summary"
				default:
					metricType = fmt.Sprintf("%T is not handled", v)
				}
				req := makeMetricSearchRequest(token, m.Name, metricType)

				Eventually(func(g Gomega) {
					resp, err := httpClient.Do(req)
					g.Expect(err).NotTo(HaveOccurred())
					defer resp.Body.Close()
					g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))

					results := &metrics.MetricsResponse{}
					g.Expect(json.NewDecoder(resp.Body).Decode(results)).To(Succeed())

					g.Expect(results.Results).To(ContainElement(
						gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
							"MetricName": Equal(m.Name),
						}),
					))

				}).WithTimeout(time.Minute).Should(Succeed())
			}
		}
	}

	verifyMetricNames := func(rm *metricdata.ResourceMetrics, names *metrics.MetricNameResponse) {
		By("verifying metric details")

		ns := make(map[string]*metrics.MetricName, len(names.Metrics))
		for _, n := range names.Metrics {
			np := n
			ns[n.Name] = &np
		}

		for _, sm := range rm.ScopeMetrics {
			for _, m := range sm.Metrics {
				Expect(ns).To(HaveKey(m.Name), "metric name exists")
				n := ns[m.Name]
				var metricType string
				switch v := m.Data.(type) {
				case metricdata.Gauge[int64], metricdata.Gauge[float64]:
					metricType = "Gauge"
				case metricdata.Sum[int64], metricdata.Sum[float64]:
					metricType = "Sum"
				case metricdata.Histogram[int64], metricdata.Histogram[float64]:
					metricType = "Histogram"
				case metricdata.Summary:
					metricType = "Summary"
				default:
					metricType = fmt.Sprintf("%T is not handled", v)
				}
				Expect(n.Type).To(Equal(metricType), "metric type")
				Expect(n.Description).To(Equal(m.Description), "metric description")

				attrs := make([]string, 0)
				for _, a := range commonAttrs {
					attrs = append(attrs, string(a.Key))
				}
				Expect(n.Attributes).To(Equal(attrs))
			}
		}
	}

	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	BeforeAll(func(ctx SpecContext) {
		writeOnlyToken = gitLab.groupWriteOnlyToken.Token

		// create a jwt token
		claims := map[string]string{
			"gitlab_realm":        "saas",
			"gitlab_namespace_id": fmt.Sprint(gitLab.group.ID),
		}
		token := jwt.New()
		for k, v := range claims {
			token.Set(k, v)
		}
		signed, err := jwt.Sign(token, jwt.WithKey(jwa.RS256, *jwtSigningKey))
		Expect(err).NotTo(HaveOccurred())
		jwtToken = string(signed)

		otherKey, err := rsa.GenerateKey(rand.Reader, 2048)
		Expect(err).NotTo(HaveOccurred())
		otherJwtSigningKey, err := jwk.FromRaw(otherKey)
		Expect(err).ToNot(HaveOccurred())

		otherJwtSigningKey.Set(jwk.AlgorithmKey, jwa.RS256)
		otherSigned, err := jwt.Sign(token, jwt.WithKey(jwa.RS256, otherJwtSigningKey))
		Expect(err).NotTo(HaveOccurred())
		otherJWTToken = string(otherSigned)
	})

	Context("Auth handling", func() {
		Context("Ingest", func() {
			It("should not accept an empty jwt token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, "", m)).NotTo(Succeed())
			})

			It("should not accept an invalid jwt token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, otherJWTToken, m)).NotTo(Succeed())
			})
		})

		Context("Query", func() {
			It("should not accept an empty jwt token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, jwtToken, m)).To(Succeed())
				res, err := httpClient.Do(makeMetricsRequest(""))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusUnauthorized))
			})

			It("should not accept an invalid jwt token", func(ctx SpecContext) {
				m := generateMetric(ctx)
				Expect(sendMetrics(ctx, jwtToken, m)).To(Succeed())
				res, err := httpClient.Do(makeMetricsRequest(otherJWTToken))
				Expect(err).NotTo(HaveOccurred())
				Expect(res).To(HaveHTTPStatus(http.StatusUnauthorized))
			})
		})
	})

	Context("Storage", func() {
		It("can save metrics and retrieve names", func(ctx SpecContext) {
			m := generateMetric(ctx)
			Expect(sendMetrics(ctx, jwtToken, m)).To(Succeed())
			ns := queryForMetrics(jwtToken, m)
			verifyMetricNames(m, ns)
		})

		It("can save metrics in tenant ingest pipeline and retrieve names from OIDC query API", func(ctx SpecContext) {
			m := generateMetric(ctx)
			Expect(sendDeprecatedMetrics(ctx, writeOnlyToken, m)).To(Succeed())
			ns := queryForMetrics(jwtToken, m)
			verifyMetricNames(m, ns)
		})

		It("can use the /v1/metrics endpoint to save metrics", func(ctx SpecContext) {
			m := generateMetric(ctx)
			exporter := makeMetricsExporter(ctx, jwtToken)
			defer exporter.Shutdown(ctx)
			Expect(exporter.Export(ctx, m)).To(Succeed())
			ns := queryForMetrics(jwtToken, m)
			verifyMetricNames(m, ns)
		})
	})

	Context("Querying metrics", func() {
		var generatedMetrics *metricdata.ResourceMetrics
		BeforeEach(func(ctx SpecContext) {
			generatedMetrics = generateMetric(ctx)
			Expect(sendMetrics(ctx, jwtToken, generatedMetrics)).To(Succeed())
		})

		It("can search for metrics", func(ctx SpecContext) {
			searchForMetrics(jwtToken, generatedMetrics)
		})
	})
})
