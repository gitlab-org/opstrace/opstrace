package tests

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/agoda-com/opentelemetry-logs-go/exporters/otlp/otlplogs"
	"github.com/agoda-com/opentelemetry-logs-go/exporters/otlp/otlplogs/otlplogshttp"
	"github.com/agoda-com/opentelemetry-logs-go/logs"
	sdk "github.com/agoda-com/opentelemetry-logs-go/sdk/logs"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	qlogs "gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/logs"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/sdk/instrumentation"
	"go.opentelemetry.io/otel/sdk/resource"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.opentelemetry.io/otel/trace"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
)

var _ = Describe("DeprecatedLogging", func() {
	var errOtel error

	traceIDs := [3][2]string{
		{
			"a/<@];!~p{jTj={)",
			"612f3c40-5d3b-217e-707b-6a546a3d7b29",
		},
		{
			"b/<@];!~p{jTj={)",
			"622f3c40-5d3b-217e-707b-6a546a3d7b29",
		},
		{
			"c/<@];!~p{jTj={)",
			"632f3c40-5d3b-217e-707b-6a546a3d7b29",
		},
	}
	spanIDs := [3][2]string{
		{
			"sidsid01",
			"7369647369643031",
		},
		{
			"sidsid02",
			"7369647369643032",
		},
		{
			"sidsid03",
			"7369647369643033",
		},
	}
	logBodys := [3]string{
		"logbodyA",
		"logbodyB",
		"logbodyC",
	}
	logAttrs := [3]string{
		"logattrA",
		"logattrB",
		"logattrC",
	}

	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	BeforeEach(func() {
		errOtel = nil

		otel.SetErrorHandler(otel.ErrorHandlerFunc(
			func(err error) {
				errOtel = err
			}),
		)
	})

	timeNow := time.Now().UTC()

	fetchSDKLogger := func(ctx context.Context, token, serviceName string) logs.Logger {
		hostName, _ := os.Hostname()
		resource := resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(serviceName),
			semconv.ServiceVersion("1.0.0"),
			semconv.HostName(hostName),
		)

		logExporter, _ := otlplogs.NewExporter(
			ctx,
			otlplogs.WithClient(
				otlplogshttp.NewClient(
					otlplogshttp.WithProtobufProtocol(),
					otlplogshttp.WithTLSClientConfig(&tls.Config{
						//nolint:gosec
						InsecureSkipVerify: true,
					}),
					otlplogshttp.WithEndpoint(testInfra.Configuration().GOBHost),
					otlplogshttp.WithURLPath(fmt.Sprintf("/v3/%d/%d/ingest/logs", gitLab.aux1Group.ID, gitLab.aux1Project.ID)),
					otlplogshttp.WithHeaders(
						map[string]string{
							"Private-Token": token,
						},
					),
					otlplogshttp.WithTimeout(
						// GDK on devvm can be super slow sometimes
						1*time.Minute,
					),
				),
			),
		)
		loggerProvider := sdk.NewLoggerProvider(
			sdk.WithSyncer(logExporter),
			sdk.WithResource(resource),
		)

		DeferCleanup(func(ctx SpecContext) {
			loggerProvider.Shutdown(ctx)
		})

		sdkLogger := loggerProvider.Logger("gob-e2e-logger")
		return sdkLogger
	}

	emitSDKLog := func(
		ctx context.Context,
		logTime time.Time,
		body string, tid trace.TraceID, sid trace.SpanID, tf trace.TraceFlags, attrs []attribute.KeyValue,
		sdkLogger logs.Logger,
	) {
		// NOTE(prozlach): We are tapping directly into logging bridge here
		// to reduce the amount of boilerplate needed and force synchronous
		// operation
		lrc := logs.LogRecordConfig{
			Timestamp:         &logTime,
			ObservedTimestamp: logTime,
			TraceId:           &tid,
			SpanId:            &sid,
			TraceFlags:        &tf,
			SeverityText:      common.Ptr("INFO"),
			SeverityNumber:    common.Ptr(logs.INFO),
			Body:              common.Ptr(body),
			Resource:          nil,
			InstrumentationScope: &instrumentation.Scope{
				Name:      "gitlab.com/vespian",
				Version:   "1.0.0",
				SchemaURL: semconv.SchemaURL,
			},
			Attributes: &attrs,
		}
		lr := logs.NewLogRecord(lrc)
		sdkLogger.Emit(lr)
	}

	emitSimpleSDKLog := func(
		ctx context.Context,
		logTime time.Time,
		body, attrBody string,
		sdkLogger logs.Logger,
	) {
		tid := trace.TraceID{}
		copy(tid[:], traceIDString)
		sid := trace.SpanID{}
		copy(sid[:], spanIDString)
		tf := trace.TraceFlags(0)
		attrs := []attribute.KeyValue{
			attribute.String("gob-e2e-attrbody", attrBody),
		}

		emitSDKLog(ctx, logTime, body, tid, sid, tf, attrs, sdkLogger)
	}

	emitComplexSDKLog := func(
		ctx context.Context,
		endTime time.Time,
		interval time.Duration,
		sdkLogger logs.Logger,
	) {
		for i := 0; i < 3; i++ {
			tid := trace.TraceID{}
			copy(tid[:], traceIDs[i][0])
			sid := trace.SpanID{}
			copy(sid[:], spanIDs[i][0])
			tf := trace.TraceFlags(i % 2)
			attrs := []attribute.KeyValue{
				attribute.String("attr0", logAttrs[0]),
				attribute.String("attr1", "foobar"),
				attribute.String("attr2", logAttrs[2]),
			}

			emitSDKLog(ctx, endTime.Add(interval*time.Duration(i-2)), logBodys[i], tid, sid, tf, attrs, sdkLogger)
			Expect(errOtel).ToNot(HaveOccurred())
		}
	}

	fetchLogs := func(
		ctx context.Context,
		apiKey string,
		periodArg, startTimeArg, endTimeArg string,
	) (*http.Response, error) {
		By("quering logs API")

		req, err := http.NewRequest(
			"GET",
			fmt.Sprintf("%s/v3/query/%d/logs/search",
				testInfra.Configuration().GOBAddress(), gitLab.aux1Project.ID),
			nil)
		Expect(err).ToNot(HaveOccurred())
		req.Header.Set("Private-Token", apiKey)

		q := req.URL.Query()
		if periodArg != "" {
			q.Add("period", periodArg)
		}
		if startTimeArg != "" {
			q.Add("start_time", startTimeArg)
		}
		if endTimeArg != "" {
			q.Add("end_time", endTimeArg)
		}
		req.URL.RawQuery = q.Encode()

		cl := testInfra.GetHTTPClient()
		cl.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}

		return cl.Do(req)
	}

	Context("Auth handling", func() {
		Context("Ingestion", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				sdkLogger := fetchSDKLogger(ctx, "", "gob-e2e-logstest")
				emitSimpleSDKLog(ctx, time.Now().UTC(), "test body", "test attr", sdkLogger)
				Expect(errOtel).To(MatchError(ContainSubstring("401 Unauthorized")))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				sdkLogger := fetchSDKLogger(ctx, "__FOO_MARYNAAAAAAAA", "gob-e2e-logstest")
				emitSimpleSDKLog(ctx, time.Now().UTC(), "test body", "test attr", sdkLogger)
				Expect(errOtel).To(MatchError(ContainSubstring("403 Forbidden")))
			})

			It("should not accept a read-only token", func(ctx SpecContext) {
				sdkLogger := fetchSDKLogger(ctx, gitLab.aux1GroupReadOnlyToken.Token, "gob-e2e-logstest")
				emitSimpleSDKLog(ctx, time.Now().UTC(), "test body", "test attr", sdkLogger)
				Expect(errOtel).To(MatchError(ContainSubstring("403 Forbidden")))
			})
		})
		Context("Quering", func() {
			It("should not accept an empty api token", func(ctx SpecContext) {
				response, err := fetchLogs(ctx, "", "1m", "", "")
				defer func() { _ = response.Body.Close() }()

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusFound))
			})

			It("should not accept an invalid api token", func(ctx SpecContext) {
				response, err := fetchLogs(ctx, "__FOO_MARYNAAAAAAAA", "1m", "", "")
				defer func() { _ = response.Body.Close() }()

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusForbidden))
			})

			It("should not accept a write-only token", func(ctx SpecContext) {
				response, err := fetchLogs(ctx, gitLab.aux1GroupWriteOnlyToken.Token, "1m", "", "")
				defer func() { _ = response.Body.Close() }()

				Expect(err).NotTo(HaveOccurred())
				Expect(response.StatusCode).To(Equal(http.StatusForbidden))
			})
		})
	})

	Context("Ingestion", func() {
		It("log records can be ingested and then retrieved", func(ctx SpecContext) {
			uniqPart, err := common.RandStringASCIIBytes(10)
			Expect(err).ToNot(HaveOccurred())
			logBody := uniqPart + " body"
			logAttr := uniqPart + " attr"

			sdkLogger := fetchSDKLogger(ctx, gitLab.aux1GroupWriteOnlyToken.Token, "gob-e2e-logingestion")
			emitSimpleSDKLog(ctx, timeNow, logBody, logAttr, sdkLogger)
			Expect(errOtel).ToNot(HaveOccurred())

			var matchingLogRow qlogs.LogsRow

			Eventually(func(g Gomega) {
				// NOTE(prozlach): Do not make this filter too tight, as the
				// time between the query-api's pod host and the host that is
				// running e2e may be not in sync!
				response, err := fetchLogs(ctx, gitLab.aux1GroupReadWriteToken.Token, "4h", "", "")
				defer func() { _ = response.Body.Close() }()

				g.Expect(err).NotTo(HaveOccurred())
				g.Expect(response).To(HaveHTTPStatus(http.StatusOK))

				logsData := new(qlogs.LogsResponse)
				g.Expect(json.NewDecoder(response.Body).Decode(logsData)).To(Succeed())

				g.Expect(logsData.Results).To(ContainElement(
					gstruct.MatchFields(
						gstruct.IgnoreExtras,
						gstruct.Fields{
							"Body": Equal(logBody),
						},
					),
					&matchingLogRow,
				))

			}).WithTimeout(time.Minute).Should(Succeed())

			Expect(matchingLogRow.Timestamp).To(Equal(timeNow))
			Expect(matchingLogRow.TraceID).To(Equal(traceIDUUID))
			Expect(matchingLogRow.SpanID).To(Equal(spanIDHex))
			Expect(matchingLogRow.SeverityText).To(Equal("INFO"))
			Expect(matchingLogRow.SeverityNumber).To(Equal(uint8(logs.INFO)))
			Expect(matchingLogRow.TraceFlags).To(Equal(uint32(0)))
			Expect(matchingLogRow.LogAttributes).To(HaveKeyWithValue(
				"gob-e2e-attrbody", logAttr,
			))
			Expect(matchingLogRow.ResourceAttributes).To(HaveKeyWithValue(
				"service.name", "gob-e2e-logingestion",
			))
		})
	})

	Context("quering", func() {
		DescribeTable("filtering by time",
			func(ctx SpecContext,
				prePeriodArg, preStartTimeArg, preEndTimeArg string,
				postPeriodArg, postStartTimeArg, postEndTimeArg string,
			) {
				sdkLogger := fetchSDKLogger(ctx, gitLab.aux1GroupWriteOnlyToken.Token, "gob-e2e-logstest")
				uniqPart, err := common.RandStringASCIIBytes(10)
				Expect(err).ToNot(HaveOccurred())
				logBody := uniqPart + " body"

				emitSimpleSDKLog(ctx, timeNow.Add(-8*time.Hour), logBody, "", sdkLogger)
				Expect(errOtel).ToNot(HaveOccurred())

				emitSimpleSDKLog(ctx, timeNow, logBody, "", sdkLogger)
				Expect(errOtel).ToNot(HaveOccurred())

				var matchingLogRows []qlogs.LogsRow

				assertFunc := func(
					ctx context.Context,
					g Gomega,
					periodArg, startTimeArg, endTimeArg string,
					times int,
				) {
					response, err := fetchLogs(
						ctx,
						gitLab.aux1GroupReadWriteToken.Token,
						periodArg, startTimeArg, endTimeArg,
					)
					defer func() { _ = response.Body.Close() }()
					g.Expect(err).NotTo(HaveOccurred())
					g.Expect(response).To(HaveHTTPStatus(http.StatusOK))

					matchingLogRows = nil
					logsData := new(qlogs.LogsResponse)
					g.Expect(json.NewDecoder(response.Body).Decode(logsData)).To(Succeed())
					g.Expect(logsData.Results).To(ContainElement(
						gstruct.MatchFields(
							gstruct.IgnoreExtras,
							gstruct.Fields{
								"Body": Equal(logBody),
							},
						),
						&matchingLogRows,
					))

					g.Expect(matchingLogRows).To(HaveLen(times), fmt.Sprintf("found %d rows instead of %d", len(matchingLogRows), times))
				}

				Eventually(func(g Gomega) {
					assertFunc(ctx, g, prePeriodArg, preStartTimeArg, preEndTimeArg, 1)
					if postStartTimeArg != "" && postStartTimeArg == postEndTimeArg {
						assertFunc(ctx, g, postPeriodArg, postStartTimeArg, postEndTimeArg, 1)
					} else {
						assertFunc(ctx, g, postPeriodArg, postStartTimeArg, postEndTimeArg, 2)
					}
				}).WithTimeout(time.Minute).Should(Succeed())
			},
			Entry("symbolic time filters",
				// NOTE(prozlach): Do not make these filters too tight, as the
				// time between the query-api's pod host and the host that is
				// running e2e may be not in sync!
				"4h", "", "",
				"12h", "", "",
			),
			Entry("timestamp filters - range",
				"", timeNow.Add(-5*time.Hour).Format(time.RFC3339Nano), timeNow.Add(1*time.Second).Format(time.RFC3339Nano),
				"", timeNow.Add(-11*time.Hour).Format(time.RFC3339Nano), timeNow.Add(1*time.Second).Format(time.RFC3339Nano),
			),
			Entry("timestamp filters - single ts",
				"", timeNow.Format(time.RFC3339Nano), timeNow.Format(time.RFC3339Nano),
				"", timeNow.Add(-8*time.Hour).Format(time.RFC3339Nano), timeNow.Add(-8*time.Hour).Format(time.RFC3339Nano),
			),
		)

		// This use-case is used also by UI. UI specifies all possible
		// parameters to efficiently fetch single log entry.
		It("query args", func(ctx SpecContext) {
			sdkLogger := fetchSDKLogger(
				ctx, gitLab.aux1GroupWriteOnlyToken.Token, "gob-e2e-logs-query",
			)
			emitComplexSDKLog(ctx, timeNow, 0, sdkLogger)

			var logsData *qlogs.LogsResponse
			Eventually(func(g Gomega) {
				// NOTE(prozlach): Do not make this filter too tight, as the
				// time between the query-api's pod host and the host that is
				// running e2e may be not in sync!
				req, err := http.NewRequest(
					"GET",
					fmt.Sprintf(
						"%s/v3/query/%d/logs/search",
						testInfra.Configuration().GOBAddress(), gitLab.aux1Project.ID,
					),
					nil,
				)
				Expect(err).ToNot(HaveOccurred())
				req.Header.Set("Private-Token", gitLab.aux1GroupReadWriteToken.Token)

				q := req.URL.Query()
				q.Add("start_time", timeNow.Format(time.RFC3339Nano))
				q.Add("end_time", timeNow.Format(time.RFC3339Nano))
				q.Add("service_name", "gob-e2e-logs-query")
				q.Add("severity_name", "info")
				q.Add("trace_id", traceIDs[0][1])
				q.Add("trace_id", traceIDs[2][1])
				q.Add("not[trace_id]", traceIDs[1][1])
				q.Add("span_id", spanIDs[0][1])
				q.Add("span_id", spanIDs[2][1])
				//q.Add("fingerprint")
				q.Add("trace_flags", "0")
				q.Add("trace_flags", "1")
				q.Add("body", logBodys[0])
				q.Add("body", logBodys[2])
				q.Add("log_attr_name", "attr0")
				q.Add("log_attr_value", logAttrs[0])
				q.Add("log_attr_name", "attr2")
				q.Add("log_attr_value", logAttrs[2])
				req.URL.RawQuery = q.Encode()

				cl := testInfra.GetHTTPClient()
				cl.CheckRedirect = func(req *http.Request, via []*http.Request) error {
					return http.ErrUseLastResponse
				}

				By("fetching log entries using detailed query")
				response, err := cl.Do(req)
				defer func() { _ = response.Body.Close() }()

				g.Expect(err).NotTo(HaveOccurred())
				g.Expect(response).To(HaveHTTPStatus(http.StatusOK))

				logsData = new(qlogs.LogsResponse)
				g.Expect(json.NewDecoder(response.Body).Decode(logsData)).To(Succeed())

				g.Expect(logsData.Results).To(HaveLen(2))
			}).WithTimeout(time.Minute).Should(Succeed())

			for i, r := range logsData.Results {
				if i == 1 {
					i = 2
				}
				Expect(r.Timestamp).To(Equal(timeNow))
				Expect(r.TraceID).To(Equal(traceIDs[i][1]))
				Expect(r.SpanID).To(Equal(spanIDs[i][1]))
				Expect(r.SeverityText).To(Equal("INFO"))
				Expect(r.SeverityNumber).To(Equal(uint8(logs.INFO)))
				Expect(r.TraceFlags).To(Equal(uint32(i % 2)))
				Expect(r.LogAttributes).To(HaveKeyWithValue(
					"attr0", logAttrs[0],
				))
				Expect(r.LogAttributes).To(HaveKeyWithValue(
					"attr2", logAttrs[2],
				))
				Expect(r.ResourceAttributes).To(HaveKeyWithValue(
					"service.name", "gob-e2e-logs-query",
				))
			}
		})

		It("query metadata", func(ctx SpecContext) {
			// In order to avoid issues with e2e tests re-runs seeing data from
			// previous runs, the time interval needs to be very short so that
			// we can be sure to always query only the data that belongs to
			// this test run.
			interval := 1296 * time.Millisecond
			timeStep := interval * 2 / 10
			referenceTime := common.TruncateTimeUsingCHLogic(timeNow, interval)
			sdkLogger := fetchSDKLogger(
				ctx, gitLab.aux1GroupWriteOnlyToken.Token, "gob-e2e-logs-qanalitics",
			)
			emitComplexSDKLog(ctx, referenceTime, interval, sdkLogger)

			sendReqF := func(g Gomega, logsData *qlogs.LogsMetadataResponse) {
				req, err := http.NewRequest(
					"GET",
					fmt.Sprintf(
						"%s/v3/query/%d/logs/searchmetadata",
						testInfra.Configuration().GOBAddress(), gitLab.aux1Project.ID,
					),
					nil,
				)
				Expect(err).ToNot(HaveOccurred())
				req.Header.Set("Private-Token", gitLab.aux1GroupReadWriteToken.Token)

				q := req.URL.Query()
				q.Add("start_time", referenceTime.Add(-2*interval).Format(time.RFC3339Nano))
				q.Add("end_time", referenceTime.Add(time.Millisecond).Format(time.RFC3339Nano))
				q.Add("service_name", "gob-e2e-logs-qanalitics")
				q.Add("severity_name", "info")
				q.Add("trace_id", traceIDs[0][1])
				q.Add("trace_id", traceIDs[2][1])
				q.Add("not[trace_id]", traceIDs[1][1])
				q.Add("span_id", spanIDs[0][1])
				q.Add("span_id", spanIDs[2][1])
				q.Add("trace_flags", "0")
				q.Add("trace_flags", "1")
				q.Add("body", logBodys[0])
				q.Add("body", logBodys[2])
				q.Add("log_attr_name", "attr0")
				q.Add("log_attr_value", logAttrs[0])
				q.Add("log_attr_name", "attr2")
				q.Add("log_attr_value", logAttrs[2])
				req.URL.RawQuery = q.Encode()

				cl := testInfra.GetHTTPClient()
				cl.CheckRedirect = func(req *http.Request, via []*http.Request) error {
					return http.ErrUseLastResponse
				}

				response, err := cl.Do(req)
				defer func() { _ = response.Body.Close() }()

				g.Expect(err).NotTo(HaveOccurred())
				g.Expect(response).To(HaveHTTPStatus(http.StatusOK))

				g.Expect(json.NewDecoder(response.Body).Decode(logsData)).To(Succeed())
			}

			var logsData *qlogs.LogsMetadataResponse
			Eventually(func(g Gomega) {
				logsData = new(qlogs.LogsMetadataResponse)

				By("fetch log metadata information using query with arguments")
				sendReqF(g, logsData)

				g.Expect(logsData.Summary.SeverityNumbers).To(HaveLen(1))
				GinkgoWriter.Printf("SeverityNumbersCounts: %v\n", logsData.SeverityNumbersCounts)
				GinkgoWriter.Printf("ReferenceTime: %s\n", referenceTime.String())

				By("verify if we got proper data")
				g.Expect(logsData.SeverityNumbersCounts).To(HaveLen(11))
				ts := referenceTime.Add(-2 * interval).UnixNano()
				for i, v := range logsData.SeverityNumbersCounts {
					g.Expect(v.Time).To(
						Equal(ts),
						fmt.Sprintf("i: %d", i),
					)
					ts += timeStep.Nanoseconds()

					g.Expect(v.Counts).To(HaveLen(1))
					switch i {
					case 0:
						fallthrough
					case 10:
						g.Expect(v.Counts[9]).To(BeEquivalentTo(1))
					default:
						g.Expect(v.Counts[9]).To(BeEquivalentTo(0))
					}
				}
			}).WithTimeout(time.Minute).Should(Succeed())

			Expect(logsData.StartTimestamp).To(Equal(referenceTime.Add(-2 * interval).UnixNano()))
			Expect(logsData.EndTimestamp).To(Equal(referenceTime.Add(time.Millisecond).UnixNano()))

			Expect(logsData.Summary.ServiceNames).To(Equal([]string{"gob-e2e-logs-qanalitics"}))
			Expect(logsData.Summary.SeverityNames).To(Equal([]string{"info"}))
			Expect(logsData.Summary.SeverityNumbers).To(BeEquivalentTo([]uint8{9}))
			Expect(logsData.Summary.TraceFlags).To(Equal([]uint32{0}))

		})
	})
})
