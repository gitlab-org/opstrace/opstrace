package tests

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwt"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/query-api/alerts"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/trace"
)

var _ = Describe("Alerts", Ordered, Serial, func() {
	var (
		tracer              trace.Tracer
		rateLimitedJwtToken string
	)

	sendSpansRateLimited := func(ctx SpecContext, token string, spans ...trace.Span) error {
		opts := []otlptracehttp.Option{
			otlptracehttp.WithTLSClientConfig(&tls.Config{
				//nolint:gosec
				InsecureSkipVerify: true,
			}),
			otlptracehttp.WithEndpoint(testInfra.Configuration().GOBHost),
			otlptracehttp.WithURLPath("/observability/v1/traces"),
			otlptracehttp.WithHeaders(
				map[string]string{
					"Authorization":         fmt.Sprintf("Bearer %s", token),
					"X-Gitlab-Realm":        "saas",
					"X-Gitlab-Instance-Id":  "instance1",
					"X-GitLab-Namespace-id": strconv.Itoa(gitLab.rateLimitedGroup.ID),
					"X-GitLab-Project-id":   fmt.Sprint(gitLab.rateLimitedProject.ID),
				},
			),
		}

		exporter, err := otlptracehttp.New(ctx, opts...)
		Expect(err).NotTo(HaveOccurred())
		defer func() { exporter.Shutdown(ctx) }()
		return exporter.ExportSpans(ctx, readSpans(spans...))
	}

	makeAlertsQueryRequest := func() *http.Request {
		req, err := http.NewRequest("GET",
			fmt.Sprintf("%s/observability/v1/alerts", testInfra.Configuration().GOBAddress()), nil)
		Expect(err).NotTo(HaveOccurred())

		for key, val := range map[string]string{
			"Authorization":        fmt.Sprintf("Bearer %s", rateLimitedJwtToken),
			"X-Gitlab-Realm":       "saas",
			"X-Gitlab-Instance-Id": "instance1",
		} {
			req.Header.Set(key, val)
		}
		return req
	}

	BeforeAll(func() {
		By("creating a tracer instance")
		tracer = makeTracer("alert-tests")

		By("creating a rate limited jwt token")
		// create a jwt token
		rClaims := map[string]string{
			"gitlab_realm":        "saas",
			"gitlab_namespace_id": fmt.Sprint(gitLab.rateLimitedGroup.ID),
		}
		rToken := jwt.New()
		for k, v := range rClaims {
			rToken.Set(k, v)
		}
		rSigned, err := jwt.Sign(rToken, jwt.WithKey(jwa.RS256, *jwtSigningKey))
		Expect(err).NotTo(HaveOccurred())
		rateLimitedJwtToken = string(rSigned)
	})

	It("gets rate limited on some spans", func(ctx SpecContext) {
		_, span := getSingleSpan(ctx, tracer)
		span.End()
		err := sendSpansRateLimited(ctx, rateLimitedJwtToken, span)
		GinkgoWriter.Printf("rate limit error: %v", err)
		Expect(err).To(HaveOccurred())
	})

	It("gets alerts by querying", func(ctx SpecContext) {
		req := makeAlertsQueryRequest()

		Eventually(func(g Gomega) {
			resp, err := httpClient.Do(req)
			g.Expect(err).NotTo(HaveOccurred())
			defer resp.Body.Close()
			g.Expect(resp).To(HaveHTTPStatus(http.StatusOK))
			g.Expect(resp.Header.Get("Cache-Control")).To(Equal("no-cache"))

			var res []alerts.Result
			g.Expect(json.NewDecoder(resp.Body).Decode(&res)).To(Succeed())
			g.Expect(res).ToNot(BeEmpty())

		}).WithTimeout(time.Minute).Should(Succeed())
	})
})
