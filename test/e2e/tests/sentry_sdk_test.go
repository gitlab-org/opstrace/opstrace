package tests

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/docker/docker/api/types/container"
	"github.com/go-openapi/strfmt"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"

	"github.com/testcontainers/testcontainers-go"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/common"
	"gitlab.com/gitlab-org/opstrace/opstrace/go/pkg/errortracking/gen/models"
	"gitlab.com/gitlab-org/opstrace/opstrace/test/sentry-sdk/pkg/containers"
)

const (
	// Our tag for the sentry SDK example images generated from the test/sentry-sdk app.
	// Don't use "latest", it will produce inconsistent results.
	// Set env var TEST_SENTRY_IMAGE_TAG to override.
	TestSentryImageTag = "0.3.0-d92f83d6"
)

var _ = Describe("sentry sdk", Ordered, Serial, func() {
	var tag string
	var errors map[uint32]models.Error

	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	BeforeAll(func(ctx SpecContext) {
		// TEST_SENTRY_IMAGE_TAG can be set to override the default tag.
		// useful when developing the test/sentry-sdk apps.
		tag = common.GetEnv("TEST_SENTRY_IMAGE_TAG", TestSentryImageTag)
	})

	BeforeEach(func() {
		By("populating known errors")

		errors = map[uint32]models.Error{}
		for _, e := range queryProjectErrors(Default) {
			errors[e.Fingerprint] = e
		}
	})

	// verifies contents of errors from the query project endpoint
	// takes into account any prior events that may have been created
	verifyErrorModel := func(want models.Error, start, end time.Time, minUserCount, minEventCount int) {
		Eventually(func(g Gomega) {
			var old models.Error
			var got *models.Error
			for _, e := range queryProjectErrors(g) {
				m := e
				if m.Fingerprint == want.Fingerprint {
					got = &m
					break
				}
			}
			g.Expect(got).NotTo(BeNil(), "found error")

			if o, got := errors[want.Fingerprint]; got {
				old = o
			} else {
				// set some defaults
				old.Status = "unresolved"
			}

			timeTransform := func(t strfmt.DateTime) time.Time {
				return time.Time(t)
			}

			g.Expect(*got).To(gstruct.MatchAllFields(gstruct.Fields{
				"Fingerprint": Equal(want.Fingerprint),
				"Name":        Equal(want.Name),
				"ProjectID":   BeNumerically("==", gitLab.project.ID),
				"Actor":       Equal(want.Actor),
				"Description": Equal(want.Description),
				"EventCount": And(
					BeNumerically(">=", minEventCount),
					BeNumerically(">", old.EventCount)),
				"ApproximatedUserCount": And(
					BeNumerically(">=", minUserCount),
					BeNumerically(">=", old.ApproximatedUserCount)),
				"Status": Or(Equal("unresolved"), Equal(old.Status)),
				// some SDKs only send unix seconds, allow rounding down
				"FirstSeenAt": WithTransform(timeTransform, Or(
					And(
						BeTemporally(">=", start.Truncate(time.Second)),
						BeTemporally("<=", end),
					),
					BeTemporally("==", time.Time(old.FirstSeenAt)))),
				"LastSeenAt": WithTransform(timeTransform, And(
					BeTemporally(">=", start.Truncate(time.Second)),
					BeTemporally("<=", end))),
				// no detailed stats verification yet
				"Stats": gstruct.PointTo(gstruct.MatchAllFields(gstruct.Fields{
					"Frequency": HaveKeyWithValue("24h", And(
						HaveLen(25),
						// check one of the last stats has a non-zero value
						// if the test rolls over the hour mark the second-to-last value could be positive
						Or(
							gstruct.MatchElementsWithIndex(gstruct.IndexIdentity, gstruct.IgnoreExtras, gstruct.Elements{
								"23": HaveEach(BeNumerically(">", 0)),
							}),
							gstruct.MatchElementsWithIndex(gstruct.IndexIdentity, gstruct.IgnoreExtras, gstruct.Elements{
								"24": HaveEach(BeNumerically(">", 0)),
							}),
						),
					)),
				})),
			}))
		}, time.Second*10, time.Second).Should(Succeed())
	}

	DescribeTable("containers should successfully send error to GOB",
		func(ctx SpecContext, lang string, expect models.Error) {
			start := time.Now().UTC()
			for i, user := range []string{"", common.RandStringRunes(10)} {
				By(fmt.Sprintf("Starting Sentry client container %s with user '%s'", lang, user))

				cr := getContainerRequest(lang, tag)
				cr.Env["SENTRY_USER"] = user

				container, err := testcontainers.GenericContainer(
					ctx,
					testcontainers.GenericContainerRequest{
						ContainerRequest: cr,
						Started:          true,
					},
				)

				Expect(err).ToNot(HaveOccurred())

				logsReader, err := container.Logs(ctx)
				Expect(err).ToNot(HaveOccurred())
				allLogs, err := io.ReadAll(logsReader)
				Expect(err).ToNot(HaveOccurred())
				GinkgoWriter.Printf("container logs:\n%s", string(allLogs))

				state, err := container.State(ctx)
				Expect(err).ToNot(HaveOccurred())
				Expect(state.ExitCode).To(Equal(0), "non-zero exit code")
				end := time.Now().UTC()

				count := i + 1
				verifyErrorModel(expect, start, end, count, count)
			}
		},
		Entry("Python SDK", "python", models.Error{
			Fingerprint: 1651027619,
			Actor:       "<module>(__main__)",
			Description: "all is wrong",
			Name:        "Exception",
		}),
		Entry("Golang SDK", "go", models.Error{
			Fingerprint: 2131308631,
			Actor:       "main(main)",
			Description: "Get \"fake-url\": unsupported protocol scheme \"\"",
			Name:        "*url.Error",
		}),
		Entry("Ruby SDK", "ruby", models.Error{
			Fingerprint: 1530347183,
			Actor:       "<main>()",
			Description: "Hello from Ruby Sentry SDK (RubySdkTestError)",
			Name:        "RubySdkTestError",
		}),
		Entry("NodeJS SDK", "nodejs", models.Error{
			Fingerprint: 1802605102,
			Actor:       "Object.<anonymous>(app)",
			Description: "Hello from Sentry Javascript SDK",
			Name:        "Error",
		}),
		Entry("Java SDK", "java", models.Error{
			Fingerprint: 765652695,
			Actor:       "unsafeMethod(sentry.example.basic.Application)",
			Description: "You shouldn't call this!",
			Name:        "UnsupportedOperationException",
		}),
		Entry("Rust SDK", "rust", models.Error{
			Fingerprint: 791262578,
			Actor:       "ParseIntError()",
			Description: "invalid digit found in string",
			Name:        "ParseIntError",
		}),
		Entry("PHP SDK", "php", models.Error{
			Fingerprint: 1664506815,
			Actor:       "()",
			Description: "Some Method",
			Name:        "BadMethodCallException",
		}),
	)
})

func queryProjectErrors(g Gomega) []models.Error {
	GinkgoHelper()
	var errors []models.Error

	By("Listing Errors From GOB at " + gitLab.errorTrackingConfig.listEndpoint)
	req, err := http.NewRequest(http.MethodGet, gitLab.errorTrackingConfig.listEndpoint, nil)
	g.Expect(err).ToNot(HaveOccurred())
	req.Header.Add("Private-Token", gitLab.groupReadOnlyToken.Token)
	resp, err := httpClient.Do(req)
	g.Expect(err).ToNot(HaveOccurred())
	defer resp.Body.Close()
	g.Expect(resp.StatusCode).To(Equal(http.StatusOK))

	err = json.NewDecoder(resp.Body).Decode(&errors)
	g.Expect(err).ToNot(HaveOccurred())

	GinkgoWriter.Printf("list error response: %v\n", errors)

	return errors
}

func getContainerRequest(
	lang string,
	tag string,
) testcontainers.ContainerRequest {
	GinkgoHelper()

	cr := containers.GetTestContainer(
		lang,
		gitLab.errorTrackingConfig.clientKey.SentryDsn,
		true,
		tag,
	)
	if testTarget == common.DEVVM {
		cr.HostConfigModifier = func(hostConfig *container.HostConfig) {
			hostConfig.ExtraHosts = []string{
				"gob.devvm:10.15.16.129",
			}
			// SDK sample container do not really need their own ip stack, and
			// using the host's one simplifies debugging.
			hostConfig.NetworkMode = "host"
		}
	} else {
		cr.HostConfigModifier = func(hostConfig *container.HostConfig) {
			// SDK sample container do not really need their own ip stack, and
			// using the host's one simplifies debugging.
			hostConfig.NetworkMode = "host"
		}
	}

	return cr
}
