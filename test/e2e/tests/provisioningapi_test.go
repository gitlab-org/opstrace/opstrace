package tests

import (
	"encoding/json"
	"fmt"
	"net/http"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"gitlab.com/gitlab-org/opstrace/opstrace/provisioning-api/pkg/provisioningapi"
	schedulerv1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/scheduler/api/v1alpha1"
)

func provisioningAPIRequest(method, token string, projectID int) *http.Response {
	GinkgoHelper()

	req, err := http.NewRequest(
		method,
		fmt.Sprintf(
			"%s/v3/tenant/%d",
			testInfra.Configuration().GOBAddress(),
			projectID,
		),
		nil,
	)
	Expect(err).NotTo(HaveOccurred())
	req.Header.Set("private-token", token)

	cl := testInfra.GetHTTPClient()
	cl.CheckRedirect = func(req *http.Request, via []*http.Request) error {
		return http.ErrUseLastResponse
	}

	resp, err := cl.Do(req)
	Expect(err).NotTo(HaveOccurred())

	return resp
}

var _ = Describe("provisioning API", Ordered, func() {
	ReportAfterEach(func(report SpecReport) {
		e2eFailed = e2eFailed || report.Failed()
	})

	Context("Auth", func() {
		It("returns 302 with an empty token", func() {
			resp := provisioningAPIRequest("GET", "", gitLab.project.ID)
			Expect(resp.StatusCode).To(Equal(http.StatusFound))
		})

		It("returns 403 on GET with non-read-write tokens", func() {
			for _, t := range []string{
				gitLab.groupReadOnlyToken.Token,
				gitLab.groupWriteOnlyToken.Token,
			} {
				resp := provisioningAPIRequest("GET", t, gitLab.project.ID)
				Expect(resp.StatusCode).To(Equal(http.StatusForbidden))
			}
		})

		It("returns 403 on PUT with non-read-write tokens", func() {
			for _, t := range []string{
				gitLab.groupReadOnlyToken.Token,
				gitLab.groupWriteOnlyToken.Token,
			} {
				resp := provisioningAPIRequest("PUT", t, gitLab.project.ID)
				Expect(resp.StatusCode).To(Equal(http.StatusForbidden))
			}
		})
	})

	// NOTE(prozlach): Simple workflow test, more thorough tests are done as
	// part of unittesting.
	It("is able to query and create tenants", func(ctx SpecContext) {
		token := gitLab.auxGroupReadWriteToken.Token
		tenantName := fmt.Sprintf("tenant-%d", gitLab.auxGroup.ID)

		By("getting info about inexistant group/project's tenant returns 403")
		resp := provisioningAPIRequest("GET", token, 12342134)
		Expect(resp.StatusCode).To(Equal(http.StatusForbidden))

		By("getting info about tenant that has not been provisioned yet returns 404")
		resp = provisioningAPIRequest("GET", token, gitLab.auxProject.ID)
		Expect(resp.StatusCode).To(Equal(http.StatusNotFound))

		By("Create a tenant")
		resp = provisioningAPIRequest("PUT", token, gitLab.auxProject.ID)
		Expect(resp.StatusCode).To(Equal(http.StatusCreated))

		By("creation of tenant using k8s client")
		tenant := new(schedulerv1alpha1.GitLabObservabilityTenant)
		key := client.ObjectKey{
			Name: tenantName,
		}
		Expect(k8sClient.Get(ctx, key, tenant)).To(Succeed())
		Expect(tenant.Spec.TopLevelNamespaceID).To(Equal(int64(gitLab.auxGroup.ID)))

		DeferCleanup(func(ctx SpecContext) {
			By("Delete tenant, see if we get 404 again")
			Expect(k8sClient.Delete(ctx, tenant)).To(Succeed())
			// NOTE(prozlach): informer is eventually consistent
			Eventually(func(g Gomega) {
				resp = provisioningAPIRequest("GET", token, gitLab.auxProject.ID)
				g.Expect(resp.StatusCode).To(Equal(http.StatusNotFound))
			}).Should(Succeed())
		})

		By("getting info about existing tenant now returns 200")
		// NOTE(prozlach): informer is eventually consistent
		Eventually(func(g Gomega) {
			resp = provisioningAPIRequest("GET", token, gitLab.auxProject.ID)
			g.Expect(resp.StatusCode).To(Equal(http.StatusOK))

			tenantInfo := new(provisioningapi.TenantData)
			err := json.NewDecoder(resp.Body).Decode(tenantInfo)
			g.Expect(err).NotTo(HaveOccurred())

			g.Expect(*tenantInfo.Name).To(Equal(tenantName))
			g.Expect(*tenantInfo.TopLevelNamespaceID).To(Equal(int64(gitLab.auxGroup.ID)))
			g.Expect(*tenantInfo.Status).To(Equal("ready"))
		}).Should(Succeed())
	})
})
