#!/usr/bin/env bash

# For development purposes - this duplicates data in the main trace table.
# Per LIMIT number of traces x ITERATIONS each trace is duplicated with
# a new id and the start time is randomized within a DAY_RANGE window.
# This is useful for using existing data to populate materialized views
# and to be able to analyze query performance.

set -euo pipefail

clickhouse_pod=${CLICKHOUSE_POD:-cluster-0-0-0}
traces_db=${TRACES_DB:-tracing}
traces_table=${TRACES_TABLE:-gl_traces_main}
iterations=${ITERATIONS:-100}
limit=${LIMIT:-1000}
# default to 30 days as this is TTL
day_range=${DAY_RANGE:-30}

sql=$(cat << SQL
INSERT INTO $traces_table
SELECT
main.ProjectId,
main.NamespaceId,
date_add(MILLISECONDS, adjusted.AdjustTimestampMs, main.Timestamp),
UUIDStringToNum(toString(adjusted.NewTraceId)),
main.SpanId,
main.ParentSpanId,
main.TraceState,
main.SpanName,
main.SpanKind,
main.ServiceName,
main.ResourceAttributes,
main.ScopeName,
main.ScopeVersion,
main.SpanAttributes,
main.Duration,
main.StatusCode,
main.StatusMessage,
main.Events.Timestamp,
main.Events.Name,
main.Events.Attributes,
main.Links.TraceId,
main.Links.SpanId,
main.Links.TraceState,
main.Links.Attributes
FROM $traces_table AS main
INNER JOIN
(
	WITH 86400000 * $day_range AS milliseconds_in_days, date_sub(MILLISECONDS, round(randCanonical() * milliseconds_in_days), now64()) AS rand_date
	SELECT TraceId, generateUUIDv4() AS NewTraceId, age('ms', min(Timestamp), rand_date) AS AdjustTimestampMs
	FROM $traces_db.$traces_table GROUP BY TraceId LIMIT $limit
) AS adjusted
USING TraceId;
SQL
)

for ((i=1;i<=iterations;i++)); do
	printf 'iteration %s\n' "$i"

	kubectl exec "$clickhouse_pod" -c clickhouse-server -- clickhouse-client -d "$traces_db" -mq "$sql"
done