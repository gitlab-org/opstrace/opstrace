package provisioningapi

import "time"

const K8sRequestTimeout = 3 * time.Second
