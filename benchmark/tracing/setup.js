const GOB_ADDRESS = __ENV.GOB_ADDRESS
const GROUP_ID = __ENV.GROUP_ID
const PROJECT_ID = __ENV.PROJECT_ID
const API_KEY = __ENV.API_KEY

export default {
    urls: {
        otlphttp: `${GOB_ADDRESS}/v3/${GROUP_ID}/${PROJECT_ID}/ingest/traces`,
    },
    headers: {
        default: {
            'Private-Token': `${API_KEY}`,
        },
    }
}
